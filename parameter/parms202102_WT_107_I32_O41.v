// Root model parameter

[Main]
// Edge based parameter
GrowthRate: 0.001 // 1/0
GrowthRate: 0.001 // 1/1
GrowthRate: 0.0002 // 2/1
GrowthRate: 0.0002 // 2/2
GrowthRate: 0.001 // 3/2
GrowthRate: 0.001 // 3/3
GrowthRate: 0.001 // 4/3
GrowthRate: 0.005 // 4/4
GrowthRate: 0.005 // 5/4
GrowthRate: 0.005 // 5/5
GrowthRate: 0.001 // 7/0
GrowthRate: 0.001 // 7/1
GrowthRate: 0.0002 // 7/2
GrowthRate: 0.001 // 7/7

Stiffness: 18.33 // 1/0
Stiffness: 8.601 // 1/1
Stiffness: 5.217 // 2/1
Stiffness: 4.935 // 2/2
Stiffness: 11.88 // 3/2
Stiffness: 3.3 // 3/3
Stiffness: 5.148 // 4/3
Stiffness: 1.452 // 4/4
Stiffness: 1.452 // 5/4
Stiffness: 0.528 // 5/5
Stiffness: 18.33 // 7/0
Stiffness: 8.601 // 7/1
Stiffness: 5.217 // 7/2
Stiffness: 8.601 // 7/7

DivisionArea: 55555 // 1
DivisionArea: 55555 // 2
DivisionArea: 55555 // 3
DivisionArea: 70 // 4
DivisionArea: 30 // 5
DivisionArea: 55555 // 7
