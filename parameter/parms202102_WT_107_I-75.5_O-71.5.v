// Root model parameter

[Main]
// Edge based parameter
GrowthRate: 0.001 // 1/0
GrowthRate: 0.001 // 1/1
GrowthRate: 0.0002 // 2/1
GrowthRate: 0.0002 // 2/2
GrowthRate: 0.001 // 3/2
GrowthRate: 0.001 // 3/3
GrowthRate: 0.001 // 4/3
GrowthRate: 0.005 // 4/4
GrowthRate: 0.005 // 5/4
GrowthRate: 0.005 // 5/5
GrowthRate: 0.001 // 7/0
GrowthRate: 0.001 // 7/1
GrowthRate: 0.0002 // 7/2
GrowthRate: 0.001 // 7/7

Stiffness: 3.705 // 1/0
Stiffness: 1.7385 // 1/1
Stiffness: 1.0545 // 2/1
Stiffness: 0.9975 // 2/2
Stiffness: 2.205 // 3/2
Stiffness: 0.6125 // 3/3
Stiffness: 0.9555 // 4/3
Stiffness: 0.2695 // 4/4
Stiffness: 0.2695 // 5/4
Stiffness: 0.098 // 5/5
Stiffness: 3.705 // 7/0
Stiffness: 1.7385 // 7/1
Stiffness: 1.0545 // 7/2
Stiffness: 1.7385 // 7/7

DivisionArea: 55555 // 1
DivisionArea: 55555 // 2
DivisionArea: 55555 // 3
DivisionArea: 70 // 4
DivisionArea: 30 // 5
DivisionArea: 55555 // 7
