//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CELL_APEX_HPP
#define CELL_APEX_HPP

#include <Attributes.hpp>
#include <Function.hpp>

#include <Contour.hpp>

#include <Mesh.hpp>
#include <Process.hpp>
#include <Random.hpp>
#include <MeshProcessSystem.hpp>

#include <Solver.hpp>

#include <MDXProcessTissue.hpp>
#include <MDXProcessCellDivide.hpp>
#include <MDXProcessMorphogens.hpp>
#include <MeshProcessStructure.hpp>

using namespace mdx;

namespace CellDisk
{
  class CellDiskSubdivide : virtual public Subdivide
  {
  public:
    CellDiskSubdivide() {}
    void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct& ss, 
        CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(),  double interpPos = 0.5)
    {
      mdx->splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
      meinhardt.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
    }

    MDXSubdivide *mdx;
    MeinhardtAI::Subdivide meinhardt;
  };

  class CellDiskDivide;
  class CellDiskTissue;
  class CellDiskGrowth;
  class CellDiskMeinhardt;
  class CellDiskSolver;
  class CellDiskSplitEdges;
 
  class CellDisk : public Process
  {
  public:
    CellDisk(const Process &process) : Process(process) 
    {
      setName("Model/CCF/01 Cell Disk");
      setDesc("Growing cellular apex");
      setIcon(QIcon(":/images/CellDisk.png"));
      addParm("Solver Process", "Name of the process for the Meinhardt Solver", "Model/CCF/02 Meinhardt Solver");
      addParm("Tissue Process", "Name of process for Cell Tissue", "Model/CCF/10 Cell Tissue");
      addParm("Growth Process", "Name of the process for Growth", "Model/CCF/04 Uniform Growth");
      addParm("Divide Process", "Name of the process for Cell Division", "Model/CCF/05 Divide Cells");
      addParm("Split Edges Process", "Name of the process to split edges", "Model/CCF/06 Split Edges");
    }
    bool initialize(QWidget *parent);
    bool step();
    bool rewind(QWidget *parent);

  private:
    Mesh *mesh = 0;

    CellDiskSolver *solverProcess = 0;
    CellDiskTissue *tissueProcess = 0;
    CellDiskGrowth *growthProcess = 0;
    CellDiskDivide *divideProcess = 0;
    CellDiskSplitEdges *splitEdgesProcess = 0;
  };

  class CellDiskSolver : public MeinhardtSolver
  {
  public:
    CellDiskSolver(const Process &process): MeinhardtSolver(process) 
    {
      setName("Model/CCF/02 Meinhardt Solver");
      addParm("Meinhardt Process", "Name of process for Meinhardt simulation", "Model/CCF/03 Meinhardt AI");
      addParm("Tissue Process", "Name of process for Cell Tissue", "Model/CCF/10 Cell Tissue");
    }
  };

  class CellDiskMeinhardt : public MeinhardtAI
  {
  public:
    CellDiskMeinhardt(const Process &process): MeinhardtAI(process) 
    {
      setName("Model/CCF/03 Meinhardt AI");
    }
  };

  class CellDiskGrowth : public Process
  {
  public:
    CellDiskGrowth(const Process &process): Process(process) 
    {
      setName("Model/CCF/04 Uniform Growth");
      addParm("Rate", "The rates of growth", "1.0 1.0 0.0");
      addParm("Dt", "Time step for growth", "0.1");

      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/CCF/10 Cell Tissue");
    }

    // Initialize to grab subdivider
    bool initialize(QWidget *parent);

    // Run a step of cell division
    bool step();

  private:
    Mesh *mesh = 0;
    CellDiskTissue *tissueProcess = 0;
  };

  class CellDiskDivide : public CellTissueCell2dDivide
  {
  public:
    CellDiskDivide(const Process &process) : CellTissueCell2dDivide(process)
    {
      setName("Model/CCF/05 Divide Cells");
      addParm("Solver Process", "Name of the process for the Meinhardt solver", "Model/CCF/02 Meinhardt Solver");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/CCF/10 Cell Tissue");
    }

    // Initialize to grab subdivider
    bool initialize(QWidget *parent);

    // Run a step of cell division
    bool step();

    CellDiskSubdivide *subdivider() { return &subdiv; }

  private:
    CellDiskSubdivide subdiv;

    CellDiskSolver *solverProcess = 0;
    CellDiskTissue *tissueProcess = 0;
  };

  class CellDiskSplitEdges : public SplitEdges
  {
  public:
    CellDiskSplitEdges(const Process &process) : SplitEdges(process) 
    {
      setName("Model/CCF/06 Split Edges");
    }
  };

  class CellDiskTissue : public CellTissueProcess
  {
  public:
    CellDiskTissue(const Process &process) : CellTissueProcess(process) 
    {
      setName("Model/CCF/10 Cell Tissue");
    }
  };
}
#endif 
