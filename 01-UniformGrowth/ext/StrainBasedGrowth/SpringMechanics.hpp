#ifndef SPRING_MECHANICS_HPP
#define SPRING_MECHANICS_HPP

#include <Attributes.hpp>
#include <Mesh.hpp>
#include <Process.hpp>
#include <Solver.hpp>
#include <MDXSubdivide.hpp>

// #define WITH_AD
#ifdef WITH_AD
#  include <Solver_Conservative_CppAD.hpp>
#endif // WITH_AD

namespace mdx
{
  // spring data
  struct SpringData
  {
    double restLength;
    double stiffness;

    // vertices and cache of positions / derivatives
    CCIndex v[2];
    Point3d *x[2], *dx[2];
    Matrix3d *jac[2][2];

    SpringData(void) : restLength(0.0), stiffness(0.0)
    {
      for(uint i = 0 ; i < 2 ; i++) {
        x[i] = dx[i] = NULL;
        for(uint j = 0 ; j < 2 ; j++)
          jac[i][j] = NULL;
      }
    }

    bool setVertices(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex e);
    bool initCache(Solver<Point3d> &solver);
    bool setRestLengthToCurrent(const CCIndexDataAttr &indexAttr, double scale = 1.0);

    bool addEdges(Solver<Point3d> &solver) {
      solver.addEdge(v[0],v[1]);
      return true;
    }

    bool operator==(const SpringData &sd) {
      return (sd.v[0] == v[0]) && (sd.v[1] == v[1]) &&
             (sd.restLength == restLength) && (sd.stiffness == stiffness);
    }
  };
  typedef AttrMap<CCIndex,SpringData> SpringAttr;
  bool inline readAttr(SpringData &element, const QByteArray &ba, size_t &pos)
  {
    bool result = readAttr(element.v, ba, pos);
    if(result) result &= readAttr(element.restLength, ba, pos);
    if(result) result &= readAttr(element.stiffness, ba, pos);
    return result;
  }

  bool inline writeAttr(const SpringData &element, QByteArray &ba)
  {
    bool result = writeAttr(element.v, ba);
    if(result) result &= writeAttr(element.restLength, ba);
    if(result) result &= writeAttr(element.stiffness, ba);
    return result;
  }

  // subdivider
  struct SpringSubdivide : public Subdivide
  {
    CCIndexDataAttr *indexAttr;
    SpringAttr *springAttr;

    SpringSubdivide(void) {}
    SpringSubdivide(CCIndexDataAttr &_indexAttr, SpringAttr &_springAttr)
      : indexAttr(&_indexAttr), springAttr(&_springAttr) {}

    void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct &ss,
                         CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(), double interpPos = 0.5);
  };

  // derivs process
  struct SpringDerivs : public Process, public SolverDerivs<Point3d>
  {
    SpringDerivs(const Process &process) : Process(process)
    {
      setName("Spring Derivs");
      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
    }

    bool step(void) {
      throw QString("SpringDerivs: This process defines derivatives; run the solver process instead.");
    }

    bool initialize(Mesh &mesh);

    void setValues(const SolverT &solver, CCIndex v, const VectorT &values);
    void getValues(const SolverT &solver, CCIndex v, VectorT &values);
    void initDerivs(SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr);
    void calcDerivatives(const SolverT &solver, VertexAttr &vAttr);
    void calcJacobian(const SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr);

    CCIndexDataAttr *indexAttr = NULL;
    SpringAttr *springAttr = NULL;

    CCIndexVec edges;
  };

  struct AssignRestLength : public Process
  {
    AssignRestLength(const Process &process) : Process(process)
    {
      setName("Assign Rest Length");
      setDesc("Assign rest length to selected edges.");
      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
      addParm("Scale Factor", "Scale factor to apply to current length", "1.0");
    }

    bool run(void) {
      Mesh *mesh = currentMesh();
      if(!mesh)
        throw(QString("AssignRestLength::run: Invalid mesh."));

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw(QString("AssignRestLength::run: No current cell complex."));
  
      CCStructure &cs = mesh->ccStructure(ccName);

      if(parm("Spring Attribute").isEmpty())
        throw QString("AssignRestLength::run: Spring attribute name empty.");
      SpringAttr &springAttr =
        mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      bool valSet;
      double scale = parm("Scale Factor").toDouble(&valSet);
      if(!valSet) scale = 1.0;

      return run(cs, indexAttr, springAttr, scale);
    }

    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, SpringAttr &springAttr, double scale = 1.0);
  };

  struct AssignStiffness : public Process
  {
    AssignStiffness(const Process &process) : Process(process)
    {
      setName("Assign Stiffness");
      setDesc("Assign stiffness to selected edges.");
      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
      addParm("Stiffness", "Stiffness to assign to all edges", "1.0");
    }

    bool run(void) {
      Mesh *mesh = currentMesh();
      if(!mesh)
        throw(QString("AssignStiffness::run: Invalid mesh."));

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw(QString("AssignStiffness::run: No current cell complex."));
  
      CCStructure &cs = mesh->ccStructure(ccName);

      if(parm("Spring Attribute").isEmpty())
        throw QString("AssignStiffness::run: Spring attribute name empty.");
      SpringAttr &springAttr =
        mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      bool valSet;
      double stiffness = parm("Stiffness").toDouble(&valSet);
      if(!valSet) stiffness = 1.0;

      return run(cs, indexAttr, springAttr, stiffness);
    }

    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, SpringAttr &springAttr, double stiffness);
  };

  // not really springy, but here anywhere
  struct BoundaryPressure : public Process, public SolverDerivs<Point3d>
  {
    BoundaryPressure(const Process &process) : Process(process)
    {
      setName("Boundary Pressure");
      setDesc("Applies pressure to vertices on boundary of cell complex");
      insertParm("Pressure", "Pressure to apply (positive is outward)", "1.0", 0);
    }

    bool step(void) {
      throw QString("BoundaryPressure: This process defines derivatives; run the solver process instead.");
    }

    bool initialize(Mesh &mesh, CCStructure &cc);

    void setValues(const SolverT &solver, CCIndex v, const VectorT &values);
    void getValues(const SolverT &solver, CCIndex v, VectorT &values);
    void calcDerivatives(const SolverT &solver, VertexAttr &vAttr);
    void calcDerivatives(const SolverT &solver, CCIndex v, VectorT &dx); // for numerical Jacobian

    CCIndexDataAttr *indexAttr = NULL;
    CCStructure *cs = NULL;
    double pressure = 0.0;
  };

#ifdef WITH_AD
  struct BoundaryPressureAD : public Process, public SolverDerivs_Cons_CppAD_Sparse<Point3d>
  {
    BoundaryPressureAD(const Process &process) : Process(process)
    {
      setName("Boundary Pressure AD");
      setDesc("Applies pressure to vertices on boundary of cell complex (with automatic differentiation)");
      insertParm("Pressure", "Pressure to apply (positive is outward)", "1.0", 0);
    }

    bool step(void) {
      throw QString("BoundaryPressureAD: This process defines derivatives; run the solver process instead.");
    }

    bool initialize(Mesh &mesh, CCStructure &cc);

    void setValues(const SolverT &solver, CCIndex v, const VectorT &values);
    void getValues(const SolverT &solver, CCIndex v, VectorT &values);
    adouble calcPotential(const SolverT &solver, VertexAttrAD &vAttrAD);

    CCIndexDataAttr *indexAttr = NULL;
    CCStructure *cs = NULL;
    double pressure = 0.0;
  };
#endif // WITH_AD

} // end namespace mdx

#endif // SPRING_MECHANICS_HPP
