// Root model parameter

[Main]
// Edge based parameter
GrowthRate: 0.0012 // 0/0
GrowthRate: 0.0012 // 1/0
GrowthRate: 0.0006 // 1/1
GrowthRate: 0.0002 // 2/1
GrowthRate: 0.0001 // 2/2
GrowthRate: 0.0014 // 3/2
GrowthRate: 0.0014 // 3/3
GrowthRate: 0.0024 // 4/3
GrowthRate: 0.0144 // 4/4
GrowthRate: 0.008 // 5/4
GrowthRate: 0.048 // 5/5
GrowthRate: 0.0012 // 7/0
GrowthRate: 0.0006 // 7/1
GrowthRate: 0.0002 // 7/2
GrowthRate: 0.0006 // 7/7

Stiffness: 14.85 // 0/0
Stiffness: 14.85 // 1/0
Stiffness: 3.7125 // 1/1
Stiffness: 3.7125 // 2/1
Stiffness: 3.7125 // 2/2
Stiffness: 18.5625 // 3/2
Stiffness: 6.1875 // 3/3
Stiffness: 12.375 // 4/3
Stiffness: 6.1875 // 4/4
Stiffness: 6.1875 // 5/4
Stiffness: 6.1875 // 5/5
Stiffness: 14.85 // 7/0
Stiffness: 3.7125 // 7/1
Stiffness: 3.7125 // 7/2
Stiffness: 3.7125 // 7/7

DivisionArea: 4.19956e-322 // 0
DivisionArea: 55555 // 1
DivisionArea: 55555 // 2
DivisionArea: 55555 // 3
DivisionArea: 70 // 4
DivisionArea: 30 // 5
DivisionArea: 55555 // 7
