#include "SpringMechanics.hpp"

#include <MeshUtils.hpp>

using namespace mdx;

// Setting edge data
bool SpringData::setVertices(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex e)
{
  std::pair<CCIndex,CCIndex> endpoints = cs.edgeBounds(e);
  if(endpoints.first.isPseudocell() || endpoints.second.isPseudocell()) {
    mdxInfo << "SpringData::setVertices: Invalid edge " << e << endl;
    return false;
  }

  v[0] = endpoints.first;
  v[1] = endpoints.second;

  return true;
}

bool SpringData::initCache(Solver<Point3d> &solver)
{
  for(uint i = 0 ; i < 2 ; i++) {
    if(v[i] == CCIndex::UNDEF) {
      mdxInfo << "SpringData::initCache vertex undefined" << endl;
      return false;
    }

    Solver<Point3d>::SolverVertexData &vData = solver.vertexAttr[v[i]];

    // Pointers to positions + derivatives
    x[i] = &vData.x;
    dx[i] = &vData.dx;

    // Jacobian diagonals, stored in vertices
    jac[i][i] = &vData.j;

    // Off-diagonal Jacobians, stored in edges
    uint j = 1 - i;
    jac[i][j] = &solver.edgeAttr[solver.graph().orientedEdge(v[i], v[j])].j;
  }

  return true;
}

bool SpringData::setRestLengthToCurrent(const CCIndexDataAttr &indexAttr, double scale)
{
  if(v[0].isPseudocell() || v[1].isPseudocell()) {
    mdxInfo << "SpringData::setRestLengthToCurrent: Vertices not yet set." << endl;
    return false;
  }

  restLength = scale * norm(indexAttr[v[0]].pos - indexAttr[v[1]].pos);

  return true;
}

// first and second derivatives for spring data
bool calcDerivs(const SpringData &spr)
{
  // Derivation for first derivatives of energy:
  //         len^2 = (x[1] - x[0])^2 = sum (x[1][i] - x[0][i])^2
  // so      2 * len * dlen = 2 * sum (x[1][i] - x[0][i]) * (-1)^(j+1) * dx[j][i],
  // i.e.    dlen/dx[j][i] = (x[j][i] - x[1-j][i]) / len.
  // Now,    E = (1/2) * hooke * (len - restlen)^2
  // so      dE/dlen = hooke * (len - restlen)
  // and     dE/dx[j][i] = hooke * ((len - restlen) / len) * (x[j][i] - x[1-j][i]),
  // so      F[j] = -hooke * ((len - restlen) / len) * (x[j] - x[1-j])
  //              =  hooke * ((len - restlen) / len) * (x[1-j] - x[j]),
  // thus    F[0] = hooke * ((len - restlen) / len) * (x[1] - x[0])
  // and     F[1] = -F[0].

  Point3d vec = *spr.x[1] - *spr.x[0];
  double len = norm(vec);
  double hookeConstant = spr.stiffness / spr.restLength;
  Point3d f0 = vec * (hookeConstant * (len - spr.restLength) / len);

  *spr.dx[0] += f0;
  *spr.dx[1] -= f0;

  return true;
}

bool calcJac(const SpringData &spr)
{
   // Derivation for second derivatives of energy:
  // Nb that kl = hooke * (len - restlen) / len = hooke * (1 - (restlen / len))
  // so      dkl/dx[j][i] = (hooke * restlen / len^2) * dlen/dx[j][i]
  //                      = (hooke * restlen / len^3) *(x[j][i] - x[1-j][i]) 
  //                      = kl3 * (x[j][i] - x[1-j][i]).
  // Now,    dE/dx[j][i] = kl * (x[j][i] - x[1-j][i])
  // so      d^2E/(dx[j][i])^2 = kl + dkl/dx[j][i] * (x[j][i] - x[1-j][i])
  //                           = kl + kl3 * (x[j][i] - x[1-j][i])^2;
  //         d^2E/(dx[j][i]dx[1-j][i]) = -kl + dkl/dx[1-j][i] * (x[j][i] - x[1-j][i])
  //                                   = -kl + kl3 * (x[1-j][i] - x[j][i]) * (x[j][i] - x[1-j][i])
  //                                   = -d^2E/(dx[j][i])^2;
  //         d^2E/(dx[j][i]dx[j][k]) = dkl/dx[j][k] * (x[j][i] - x[1-j][i])
  //                                 = kl3 * (x[j][k] - x[1-j][k]) * (x[j][i] - x[1-j][i]);
  // and     d^2E/(dx[j][i]dx[1-j][k]) = dkl/dx[1-j][k] * (x[j][i] - x[1-j][i])
  //                                   = kl3 * (x[1-j][k] - x[j][k]) * (x[j][i] - x[1-j][i])
  //                                   = -d^2E/(dx[j][i]dx[j][k]).
  // We can thus create a "self" Jacobian
  //  (  kl + kl3 * vec[0]^2    kl3 * vec[1] * vec[0]   kl3 * vec[2] * vec[0] )
  //  ( kl3 * vec[0] * vec[1]    kl + kl3 * vec[1]^2    kl3 * vec[2] * vec[1] )
  //  ( kl3 * vec[0] * vec[2]   kl3 * vec[1] * vec[2]    kl + kl3 * vec[2]^2  );
  // this is the Jacobian for coordinates on the same vertex,
  // i.e. jac[0][0] and jac[1][1]; the Jacobian for coordinates on opposite vertices
  // is then the negative of this.

  Point3d vec = *spr.x[1] - *spr.x[0];
  double len = norm(vec);
  double hookeConstant = spr.stiffness / spr.restLength;
  double kl = hookeConstant * (len - spr.restLength) / len;
  double kl3 = hookeConstant * spr.restLength / (len * len * len);

  Matrix3d mat;
  for(uint x = 0 ; x < 3 ; x++) {
    mat[x][x] += kl;
    for(uint y = 0 ; y < 3 ; y++) {
      mat[x][y] += kl3 * vec[x] * vec[y];
    }
  }

  // Checking against AD-computed values shows that we got the signs wrong.
  // I still have to go back and check the derivation to see where.
  *spr.jac[0][0] -= mat;
  *spr.jac[1][1] -= mat;

  *spr.jac[0][1] += mat;
  *spr.jac[1][0] += mat;

  return true;
}

// subdivider
void SpringSubdivide::splitCellUpdate
  (Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct &ss,
   CCIndex otherP, CCIndex otherN, double interpPos)
{
  if(!indexAttr) {
    mdxInfo << "SpringSubdivide::splitCellUpdate error: indexAttr not set." << endl;
    return;
  }
//  mdxInfo << "spr: Splitting cell of dimension "<<dim<<endl;

  if(dim == 1) { // splitting an edge, i.e. possibly a spring
    if(!springAttr) {
      mdxInfo << "SpringSubdivide::splitCellUpdate error: springAttr not set." << endl;
      return;
    }

    SpringAttr::iterator iter = springAttr->find(ss.parent);
    if(iter == springAttr->end()) return;   // not a spring, do nothing.

//    mdxInfo << "spr: Splitting edge "<<ss.parent<<" at interp "<<interpPos
//            << " into ("<<ss.childP<<","<<ss.childN<<")"<<endl;

    SpringData &sprP = (*springAttr)[ss.childP];
    SpringData &sprN = (*springAttr)[ss.childN];

    mdxInfo << "split1 " << interpPos << "/" << endl;

    sprP.setVertices(cs, *indexAttr, ss.childP);
    sprP.restLength = interpPos * iter->second.restLength;
    sprP.stiffness = iter->second.stiffness;

    sprN.setVertices(cs, *indexAttr, ss.childN);
    sprN.restLength = (1.0 - interpPos) * iter->second.restLength;
    sprN.stiffness = iter->second.stiffness;

    springAttr->erase(ss.parent);
  }
  if(dim == 2){ // splitting a face (i.e. a cell)
    std::cout << "dim2" << std::endl;
    CCIndexPair ep = cs.edgeBounds(ss.membrane);
    CCIndexData &vIdx = ep.first->*indexAttr;
    CCIndexData &wIdx = ep.second->*indexAttr;

    CCIndex e = edgeBetween(cs, ep.first, ep.second);

    SpringData &eSD = e->*springAttr;

    eSD.setVertices(cs, *indexAttr, e);
    //eSD.stiffness = 1;
    eSD.restLength = norm(vIdx.pos - wIdx.pos);

    std::set<CCIndex> edges = cs.incidentCells(ep.first, 1);

    double stiffn = 0;
    int counter = 0;
    for(CCIndex edge : edges){
      SpringData &edgeSD = edge->*springAttr;
      if(edgeSD.stiffness > 0){
        stiffn += edgeSD.stiffness;
        counter++;
      } 
    }
    std::cout << "stiffness1 " << stiffn << "/" << edges.size() << std::endl;
    if(counter > 0)
      stiffn /= counter;
    if(stiffn == 0)
      stiffn = 2;
    eSD.stiffness = stiffn;
    std::cout << "stiffness2 " << stiffn << std::endl;
  }
}

// Derivs process for springs
bool SpringDerivs::initialize(Mesh &mesh)
{
  indexAttr = &mesh.indexAttr();

  if(parm("Spring Attribute").isEmpty())
    throw QString("SpringDerivs::initialize: Spring attribute name empty.");
  springAttr = &mesh.attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

  return true;
}

void SpringDerivs::setValues(const SolverT &solver, CCIndex v, const VectorT &values)
{
  if(!indexAttr) {
    mdxInfo << "SpringDerivs::setValues: CCIndexData attribute not set." << endl;
    return;
  }
  (*indexAttr)[v].pos = values;
}

void SpringDerivs::getValues(const SolverT &solver, CCIndex v, VectorT &values)
{
  if(!indexAttr) {
    mdxInfo << "SpringDerivs::setValues: CCIndexData attribute not set." << endl;
    return;
  }
  values = (*indexAttr)[v].pos;
}

void SpringDerivs::initDerivs(SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr)
{
  SolverDerivs::getValues(solver, vAttr);

  if(!indexAttr)
    throw QString("SpringDerivs::initDerivs: CCIndexData attribute not set.");
  if(!springAttr)
    throw QString("SpringDerivs::initDerivs: Spring attribute not set.");

  edges.clear();
  for(CCIndex edge : solver.graph().edges()) {
    SpringAttr::iterator iter = springAttr->find(edge);
    if(iter == springAttr->end()) continue;
    SpringData &spring = iter->second;

    // Check that the vertex list is initialized
    bool elementOK = true;
    for(int i = 0; i < 2 ; i++)
      if(spring.v[i].isPseudocell()) {
        mdxInfo << "SpringDerivs::initDerivs: Uninitialized element found:" << edge << endl;
        elementOK = false;
        break;
      }
    if(!elementOK) continue;
    spring.initCache(solver);

    // Add edge to list
    edges.push_back(edge);
  }
}

void SpringDerivs::calcDerivatives(const SolverT &solver, VertexAttr &vAttr)
{
  if(!indexAttr)
    throw QString("SpringDerivs::calcDerivatives: Index attribute empty.");
  if(!springAttr)
    throw QString("SpringDerivs::calcDerivatives: Spring attribute empty.");

  for(uint i = 0; i < edges.size(); i++)
    calcDerivs((*springAttr)[edges[i]]);
}

void SpringDerivs::calcJacobian(const SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr)
{
  if(!indexAttr)
    throw QString("SpringDerivs::calcJacobian: Index attribute empty.");
  if(!springAttr)
    throw QString("SpringDerivs::calcJacobian: Spring attribute empty.");

  for(uint i = 0; i < edges.size(); i++)
    calcJac((*springAttr)[edges[i]]);
}


// Assign rest length to selected springs.
bool AssignRestLength::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, SpringAttr &springAttr,
                           double scale)
{
  const CCIndexVec &edges = selectedEdges(cs, indexAttr);
  if(edges.size() == 0)
    throw QString("AssignRestLength::run: No edges selected.");

  #pragma omp parallel for
  for(uint i = 0 ; i < edges.size() ; i++) {
    CCIndex edge = edges[i];
    SpringData &spd = springAttr[edge];
    spd.setVertices(cs, indexAttr, edge);
    spd.setRestLengthToCurrent(indexAttr, scale);
  }

  return true;
}

// Assign stiffness to selected springs.
bool AssignStiffness::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, SpringAttr &springAttr,
                          double stiffness)
{
  const CCIndexVec &edges = selectedEdges(cs, indexAttr);
  if(edges.size() == 0)
    throw QString("AssignStiffness::run: No edges selected.");

  #pragma omp parallel for
  for(uint i = 0 ; i < edges.size() ; i++) {
    springAttr[edges[i]].stiffness = stiffness;
  }

  return true;
}


// boundary pressure derivs
bool BoundaryPressure::initialize(Mesh &mesh, CCStructure &cc)
{
  indexAttr = &mesh.indexAttr();
  cs = &cc;
  if(cc.maxDimension() != 2)
    throw QString("BoundaryPressure: Only works with 2D cell complexes.");

  bool valSet;
  pressure = parm("Pressure").toDouble(&valSet);
  if(!valSet) pressure = 0.0;

  return true;
}

void BoundaryPressure::setValues(const SolverT &solver, CCIndex v, const VectorT &values)
{
  if(!indexAttr) {
    mdxInfo << "BoundaryPressure::setValues: CCIndexData attribute not set." << endl;
    return;
  }
  (*indexAttr)[v].pos = values;
}

void BoundaryPressure::getValues(const SolverT &solver, CCIndex v, VectorT &values)
{
  if(!indexAttr) {
    mdxInfo << "BoundaryPressure::getValues: CCIndexData attribute not set." << endl;
    return;
  }
  values = (*indexAttr)[v].pos;
}

void BoundaryPressure::calcDerivatives(const SolverT &solver, VertexAttr &vAttr)
{
  // apply values for every edge on border of cell complex
  for(CCIndex edge : cs->bounds(CCIndex::INFTY)) {
    // get edge geometry
    std::pair<CCIndex,CCIndex> endpoints = cs->edgeBounds(edge);
    Point3d p0 = vAttr[endpoints.first].x, p1 = vAttr[endpoints.second].x;

    // find outward facing normal to edge
    CellTuple tuple(*cs,edge,CCIndex::INFTY);
    tuple.flip(2);
//    Point3d inward = (*indexAttr)[tuple[2]].pos - 0.5 * (p0 + p1);
//    Point3d normal = (p1 - p0) % ((p1 - p0) % inward);
    Point3d normal = (p1 - p0) % (*indexAttr)[tuple[2]].nrml;
    if(tuple.ro(1) == ccf::NEG) normal = -normal;

    // force is pressure * length * outward normal
    Point3d force = (pressure * norm(p1 - p0) / norm(normal)) * normal;

    // apply half to each vertex
    vAttr[endpoints.first].dx += 0.5 * force;
    vAttr[endpoints.second].dx += 0.5 * force;
  }
}

void BoundaryPressure::calcDerivatives(const SolverT &solver, CCIndex v, VectorT &dx)
{
  // get incident edges from a single flip
  CCStructure::FlipI flip = cs->matchFirst(v,CCIndex::Q,CCIndex::Q,CCIndex::INFTY);
  if(!flip.isValid()) return;
  
  for(CCIndex edge : flip.facet) {
    // get edge geometry
    std::pair<CCIndex,CCIndex> endpoints = cs->edgeBounds(edge);
    Point3d p0 = solver.vertexAttr[endpoints.first].x, p1 = solver.vertexAttr[endpoints.second].x;

    // find outward facing normal to edge
    CellTuple tuple(*cs,edge,CCIndex::INFTY);
    tuple.flip(2);
    Point3d normal = (p1 - p0) % (*indexAttr)[tuple[2]].nrml;
    if(tuple.ro(1) == ccf::NEG) normal = -normal;

    // force is pressure * length * outward normal
    Point3d force = (pressure * norm(p1 - p0) / norm(normal)) * normal;

    // apply half of force to our vertex
    dx += 0.5 * force;
  }
}

#ifdef WITH_AD
bool BoundaryPressureAD::initialize(Mesh &mesh, CCStructure &cc)
{
  indexAttr = &mesh.indexAttr();
  cs = &cc;
  if(cc.maxDimension != 2)
    throw QString("BoundaryPressureAD: Only works with 2D cell complexes.");

  bool valSet;
  pressure = parm("Pressure").toDouble(&valSet);
  if(!valSet) pressure = 0.0;

  return true;
}

void BoundaryPressureAD::setValues(const SolverT &solver, CCIndex v, const VectorT &values)
{
  if(!indexAttr) {
    mdxInfo << "BoundaryPressureAD::setValues: CCIndexData attribute not set." << endl;
    return;
  }
  (*indexAttr)[v].pos = values;
}

void BoundaryPressureAD::getValues(const SolverT &solver, CCIndex v, VectorT &values)
{
  if(!indexAttr) {
    mdxInfo << "BoundaryPressureAD::getValues: CCIndexData attribute not set." << endl;
    return;
  }
  values = (*indexAttr)[v].pos;
}

BoundaryPressureAD::adouble BoundaryPressureAD::calcPotential(const SolverT &solver, VertexAttrAD &vAttrAD)
{
  typedef Vector<3,adouble> aVec;

  // collect border vertices (we assume there is only one border piece)
  std::vector<CCIndex> vs;
  CCStructure::CellTuple tuple(*cs,CCIndex::INFTY);
  do {
    vs.push_back(tuple[0]);
    tuple.flip(0,1);
  } while(tuple[0] != vs[0]);

  // get average position
  Point3d center(0,0,0);
  for(CCIndex v : vs) center += (*indexAttr)[v].pos;
  center /= vs.size();
  aVec aCenter(center);

  // compute area
  adouble area = 0;
  for(uint i = 0 ; i < vs.size() ; i++) {
    uint i1 = (i + 1) % vs.size();
    aVec mPos = vAttrAD[vs[i]].x, nPos = vAttrAD[vs[i1]].x;
    aVec cross = (mPos - aCenter) ^ (nPos - aCenter);
    area += sqrt(cross * cross) / 2.0;
  }

  // now pressure potential is area * pressure
  mdxInfo << "Pressure potential is "<<adValue(pressure * fabs(area))<<endl;
  return pressure * fabs(area);
}
#endif // WITH_AD
