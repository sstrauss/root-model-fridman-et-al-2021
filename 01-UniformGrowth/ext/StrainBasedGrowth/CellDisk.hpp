//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CELL_APEX_HPP
#define CELL_APEX_HPP

#include <Attributes.hpp>
#include <Function.hpp>

#include <Contour.hpp>

#include <Mesh.hpp>
#include <Process.hpp>
#include <Random.hpp>
#include <MeshProcessSystem.hpp>

#include <Solver.hpp>

#include <MDXProcessTissue.hpp>
#include <MDXProcessCellDivide.hpp>
#include <MDXProcessMorphogens.hpp>
#include <MeshProcessStructure.hpp>

#include "SpringMechanics.hpp"

using namespace mdx;

namespace CellDisk
{

  // Store info in edges
  struct MassSpringEdgeData
  {
    double restLen; // Rest length
    double growthRate; // Growth rate
    double k; // Stiffness
    CCIndex f, g; // Attached faces, for fast lookup
    bool border;

    double stiffness;

    MassSpringEdgeData() :
      restLen(0), growthRate(0.0), k(0.0), f(0), g(0), border(false), stiffness(0) {}

    bool operator==(const MassSpringEdgeData &other) const
    {
      if(growthRate == other.growthRate) return true;
      return false;

     }

  };
  bool inline readAttr(MassSpringEdgeData &m, const QByteArray &ba, size_t &pos)
  {
    return readChar((char *)&m, sizeof(MassSpringEdgeData), ba, pos);
  }
  bool inline writeAttr(const MassSpringEdgeData &m, QByteArray &ba)
  {
    return writeChar((char *)&m, sizeof(MassSpringEdgeData), ba);
  }

  typedef AttrMap<CCIndex,MassSpringEdgeData> MSEdgeDataAttr;

  // Store info in edges
  struct MassSpringCellData
  {
    double divArea; // Rest length
    int type;

    MassSpringCellData() :
      divArea(0), type(0) {}

    bool operator==(const MassSpringCellData &other) const
    {
      if(divArea == other.divArea and type == other.type) return true;
      return false;

     }

  };
  bool inline readAttr(MassSpringCellData &m, const QByteArray &ba, size_t &pos)
  {
    return readChar((char *)&m, sizeof(MassSpringCellData), ba, pos);
  }
  bool inline writeAttr(const MassSpringCellData &m, QByteArray &ba)
  {
    return writeChar((char *)&m, sizeof(MassSpringCellData), ba);
  }

  typedef AttrMap<CCIndex,MassSpringCellData> MSCellDataAttr;


  class MassSpringSubdivide : virtual public Subdivide
  {
  public:
    MassSpringSubdivide(MSEdgeDataAttr *msEdgeAttr, CCIndexDataAttr *indexAttr, SpringAttr *springAttr, MSCellDataAttr *msCellAttr) : msEdgeData(msEdgeAttr), indexAttr(indexAttr), springAttr(springAttr), msCellData(msCellAttr) {}
    void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct& ss,
        CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(), double interpPos = 0.5)
    {
      std::cout << "Subdivide!" << std::endl;
      //     std::cout << "s" << (*springAttr).size()  << "/" << (*msEdgeData).size() << std::endl;

      // if(cs.dimensionOf(ss.childP) == 1) {
      //   MassSpringEdgeData &rMS = ss.parent->*msEdgeData;
      //   MassSpringEdgeData &pMS = ss.childP->*msEdgeData;
      //   MassSpringEdgeData &nMS = ss.childN->*msEdgeData;

      //   pMS.restLen = (1.0 - interpPos) * rMS.restLen;
      //   nMS.restLen = interpPos * rMS.restLen;

      //   pMS.growthRate = nMS.growthRate = rMS.growthRate;

      //   (*msEdgeData).erase(ss.parent);
      //   pMS.stiffness = nMS.stiffness = rMS.stiffness;
      //   pMS.k = nMS.k = rMS.k;

      //   // Spring Data
      //   // SpringData &rSD = ss.parent->*springAttr;
      //   // SpringData &pSD = ss.childP->*springAttr;
      //   // SpringData &nSD = ss.childN->*springAttr;

      //   // pSD.setVertices(cs, *indexAttr, ss.childP);
      //   // pSD.restLength = interpPos * rSD.restLength;
      //   // pSD.stiffness = rSD.stiffness;

      //   // nSD.setVertices(cs, *indexAttr, ss.childN);
      //   // nSD.restLength = (1.0 - interpPos) * rSD.restLength;
      //   // nSD.stiffness = rSD.stiffness;

      //   // (*springAttr).erase(ss.parent);

      // }
      if(cs.dimensionOf(ss.childP) == 2) {
        MassSpringCellData &rMS = ss.parent->*msCellData;
        MassSpringCellData &pMS = ss.childP->*msCellData;
        MassSpringCellData &nMS = ss.childN->*msCellData;

        pMS.divArea = nMS.divArea = rMS.divArea;

        // MassSpringEdgeData &rMSE = ss.parent->*msEdgeData;
        // MassSpringEdgeData &pMSE = ss.childP->*msEdgeData;
        // MassSpringEdgeData &nMSE = ss.childN->*msEdgeData;

        // pMSE.growthRate = nMSE.growthRate = rMSE.growthRate;

        CCIndexPair ep = cs.edgeBounds(ss.membrane);
        MassSpringEdgeData &eMS = ss.membrane->*msEdgeData;
        CCIndexData &vIdx = ep.first->*indexAttr;
        CCIndexData &wIdx = ep.second->*indexAttr;

        // eMS.restLen = norm(vIdx.pos - wIdx.pos);
        // eMS.growthRate = 0.01;

        // SpringData &pSD = ss.membrane->*springAttr;

        // pSD.stiffness = 1;
        // pSD.restLength = norm(vIdx.pos - wIdx.pos);


      }
      //           std::cout << "t" << (*springAttr).size()  << "/" << (*msEdgeData).size() << std::endl;

    }


    MSEdgeDataAttr *msEdgeData;
    MSCellDataAttr *msCellData;

    CCIndexDataAttr *indexAttr;
    SpringAttr *springAttr;

    //CCIndexDataAttr *indexData;
    //MDXSubdivide *mdx;
    //MeinhardtAI::Subdivide meinhardt;
    //SpringSubdivide spring;
  };

  class CellDiskSubdivide : virtual public Subdivide
  {
  public:
    CellDiskSubdivide() : ms(0,0,0,0) {}
    void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct& ss,
        CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(),  double interpPos = 0.5)
    {
      mdxInfo << "splitting cell of dimension "<<dim<<endl;
      mdx->splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
      meinhardt.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
      spring.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
      ms.splitCellUpdate(dim, cs, ss, otherP, otherN, interpPos);
    }

    MDXSubdivide *mdx;
    MeinhardtAI::Subdivide meinhardt;
    SpringSubdivide spring;
    MassSpringSubdivide ms;
  };

  class CellDiskMeinhardt;
  class CellDiskMeinhardtSolver;

  class CellDiskMechanics;
  class CellDiskPressure;
  class CellDiskMechanicsSolver;
  class CellDiskSetRestLength;
  class CellDiskSetMaterial;

  class CellDiskTissue;
  class CellDiskGrowth;
  class CellDiskDivide;
  class CellDiskSplitEdges;

  class CellDiskTissue : public CellTissueProcess
  {
  public:
    CellDiskTissue(const Process &process) : CellTissueProcess(process)
    {
      setName("Model/01 Cell Disk/Cell Tissue");
    }
  };

  class CellDisk : public Process
  {
  public:
    CellDisk(const Process &process) : Process(process)
    {
      setName("Model/01 Cell Disk/01 Main");
      setDesc("Growing cellular apex");
      setIcon(QIcon(":/images/CellDisk.png"));

      addParm("Meinhardt Solver Process", "Name of the process for the Meinhardt Solver",
              "Model/02 Meinhardt/01 Solver");

      addParm("Mechanics Solver Process", "Name of the process for the Mechanics Solver",
              "Model/03 Mechanics/01 Solver");

      addParm("Tissue Process", "Name of process for Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
      addParm("Growth Process", "Name of the process for Growth", "Model/01 Cell Disk/Growth");
      addParm("Divide Process", "Name of the process for Cell Division", "Model/01 Cell Disk/Divide");
      addParm("Split Edges Process", "Name of the process to split edges", "Model/01 Cell Disk/Split Edges");
      addParm("Stop Size", "Stop when size is lager than mesh 2", "Yes", booleanChoice());
      addParm("Max Time", "Stop when Max Time is reached", "10000");
    }
    bool initialize(QWidget *parent);
    bool step();
    bool rewind(QWidget *parent);

  private:
    Mesh *mesh = NULL;

    CellDiskMeinhardtSolver *meinhardtSolver = NULL;
    CellDiskMechanicsSolver *mechanicsSolver = NULL;

    CellDiskTissue *tissueProcess = NULL;
    CellDiskGrowth *growthProcess = NULL;
    CellDiskDivide *divideProcess = NULL;
    CellDiskSplitEdges *splitEdgesProcess = NULL;

    MSEdgeDataAttr *msEdgeData;
    MSCellDataAttr *msCellData;
  };

  // Meinhardt
  class CellDiskMeinhardtSolver : public MeinhardtSolver
  {
  public:
    CellDiskMeinhardtSolver(const Process &process): MeinhardtSolver(process)
    {
      setName("Model/02 Meinhardt/01 Solver");
      addParm("Meinhardt Process", "Name of process for Meinhardt simulation", "Model/02 Meinhardt/AI");
      addParm("Tissue Process", "Name of process for Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
    }
  };

  class CellDiskMeinhardt : public MeinhardtAI
  {
  public:
    CellDiskMeinhardt(const Process &process): MeinhardtAI(process)
    {
      setName("Model/02 Meinhardt/AI");
    }
  };

  // Mechanics
  struct CellDiskMechanics : public SpringDerivs
  {
    CellDiskMechanics(const Process &process) : SpringDerivs(process)
    {
      setName("Model/03 Mechanics/Springs");
    }
  };

  struct CellDiskPressure : public BoundaryPressure
  {
    CellDiskPressure(const Process &process) : BoundaryPressure(process)
    {
      setName("Model/03 Mechanics/Pressure");
    }
  };

#ifdef WITH_AD
  struct CellDiskPressureAD : public BoundaryPressureAD
  {
    CellDiskPressureAD(const Process &process) : BoundaryPressureAD(process)
    {
      setName("Model/03 Mechanics/PressureAD");
    }
  };
#endif // WITH_AD

  struct CellDiskSetRestLength : public AssignRestLength
  {
    CellDiskSetRestLength(const Process &process) : AssignRestLength(process)
    {
      setName("Model/03 Mechanics/Set Rest Lengths");
    }
  };

  struct CellDiskSetMaterial : public AssignStiffness
  {
    CellDiskSetMaterial(const Process &process) : AssignStiffness(process)
    {
      setName("Model/03 Mechanics/Set Material Properties");
    }
  };

  struct CellDiskMechanicsSolver : public Process, public Solver<Point3d>
  {
    CellDiskMechanicsSolver(const Process &process) : Process(process)
    {
      setName("Model/03 Mechanics/01 Solver");
      insertParm("Converge Threshold", "Threshold for convergence", "1e-5", 0);
      addParm("Spring Derivs", "Name of process for spring forces", "Model/03 Mechanics/Springs");
      addParm("Pressure Derivs", "Name of process for pressure forces", "Model/03 Mechanics/Pressure");
      addParm("Tissue Process", "Name of process for Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
    }

    bool initialize(QWidget *parent);
    bool step(void);
    bool finalize(QWidget *parent);

    Mesh *mesh = NULL;
    CCStructure *cs = NULL;
    QString ccName;
  };

  // Growth &c.
  class CellDiskGrowth : public Process
  {
  public:
    CellDiskGrowth(const Process &process): Process(process)
    {
      setName("Model/01 Cell Disk/Growth");
      addParm("Rate", "Yield rate", "1.0");
      addParm("Dt", "Time step for growth", "0.1");

      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
    }

    bool initialize(QWidget *parent);
    bool step();

    double totalTime;

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

  };


  class CellDiskGrowthSetAttr : public Process
  {
  public:
    CellDiskGrowthSetAttr(const Process &process): Process(process)
    {
      setName("Model/01 Cell Disk/Set Attr Growth");

      addParm("Attribute Name", "Name of attribute containing growth data", "MassSpringEdgeData");
      addParm("Growth Rate", "Growth Rate", "1");
    }

    bool run(){


      mesh = currentMesh();


      if(!mesh)
        throw(QString("MechanicsSolver::initialize: Invalid mesh."));
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("MechanicsSolver::initialize: Invalid cell complex.");
      CCStructure *cs = &mesh->ccStructure(ccName);
      if(!cs)
        throw QString("MechanicsSolver::initialize: Failed to get cell complex.");

      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      //auto &attr = mesh->attributes().attrMap<CCIndex,double>(parm("Attribute Name"));

      MSEdgeDataAttr *msEdgeAttr = &mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>(parm("Attribute Name"));

      double growthRate = parm("Growth Rate").toDouble();

      std::cout << "growth " << cs->edges().size() << std::endl;

      for(CCIndex edge : cs->edges()) {
        bool selected = true;
        for(CCIndex vtx : cs->incidentCells(edge,0)){
          if(!indexAttr[vtx].selected) selected = false;
        }

        MassSpringEdgeData &msed = edge->*msEdgeAttr;

        if(selected){
          //attr[edge] = growthRate;
          msed.growthRate = growthRate;
        }
      }



      return true;
    }

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;
  };


  // Growth &c.
  class CellDiskGetAttr : public Process
  {
  public:
    CellDiskGetAttr(const Process &process): Process(process)
    {
      setName("Model/04 Misc/Get Attrs");

      addParm("Attribute Name", "Name of attribute containing growth data", "MassSpringEdgeData");
      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
    }

    bool run(){


      mesh = currentMesh();


      if(!mesh)
        throw(QString("MechanicsSolver::initialize: Invalid mesh."));
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("MechanicsSolver::initialize: Invalid cell complex.");
      CCStructure *cs = &mesh->ccStructure(ccName);
      if(!cs)
        throw QString("MechanicsSolver::initialize: Failed to get cell complex.");

      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      //auto &attr = mesh->attributes().attrMap<CCIndex,double>(parm("Attribute Name"));

      MSEdgeDataAttr *msEdgeAttr = &mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>(parm("Attribute Name"));
      SpringAttr &springAttr = mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));


      //double growthRate = parm("Growth Rate").toDouble();

      //std::cout << "growth " << cs->edges().size() << std::endl;

      for(CCIndex edge : cs->edges()) {
        bool selected = true;
        for(CCIndex vtx : cs->incidentCells(edge,0)){
          if(!indexAttr[vtx].selected) selected = false;
        }

        MassSpringEdgeData &msed = edge->*msEdgeAttr;

        if(selected){
          //attr[edge] = growthRate;
          //msed.growthRate = growthRate;
          std::cout << "Growth Rate: " << msed.growthRate << " / Stiffness: " << springAttr[edge].stiffness << std::endl;
        }
      }

    MSCellDataAttr &msCellAttr = mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");

      for(CCIndex f : cs->faces()) {
        if(!indexAttr[f].selected) continue;
        std::cout << "Div Area: " << msCellAttr[f].divArea << std::endl;
      }

      return true;
    }

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;
  };


  // Growth &c.
  class CellDiskAreaQuant : public Process
  {
  public:
    CellDiskAreaQuant(const Process &process): Process(process)
    {
      setName("Model/04 Misc/Quantify Areas");

      //addParm("Attribute Name", "Name of attribute containing division data", "MassSpringCellData");
      //addParm("Div Area", "Div Area", "50");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
      addParm("Filename", "Filename", "");
      addParm("Sample", "Sample", "");
      addParm("Genotype", "Genotype", "");
    }

    bool run(){


      mesh = currentMesh();
      if(!mesh)
        throw(QString("CellDiskAreaQuant::initialize: Invalid mesh."));
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("CellDiskAreaQuant::initialize: Invalid cell complex.");
      CCStructure *cs = &mesh->ccStructure(ccName);
      if(!cs)
        throw QString("CellDiskAreaQuant::initialize: Failed to get cell complex.");

    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDiskAreaQuant::initialize Cannot make tissue process"));

    //TissueName = tissueProcess->parm("Tissue");
    //TissueDualName = tissueProcess->parm("Tissue Dual");

      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      Mesh *mesh2 = otherMesh();
      QString ccName2 = mesh2->ccName();
      CCStructure *cs2 = &mesh2->ccStructure(ccName2);

      CCIndexDataAttr &indexAttr2 = mesh2->indexAttr();

      // //std::cout << "div " << cs->edges().size() << std::endl;

      // //auto &attr = mesh->attributes().attrMap<CCIndex,double>(parm("Attribute Name"));
      // MSCellDataAttr &msCellAttr = mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");

      // double divArea = parm("Div Area").toDouble();

      // CellTissue* tissue = &(tissueProcess->tissue());

    //if(!tissue)
    //  throw(QString("CellDiskAreaQuant.step: Tissue not initialized"));


    // forall(CCIndex cell, tissue->cellStructure().faces()) {
    //   if(indexAttr[cell].selected) msCellAttr[cell].divArea = divArea;

    // }

      // quantify the cell areas of all cells, sum them up

      AttrMap<int,double> &sizeAttr = mesh->attributes().attrMap<int,double>("Root Area");
      sizeAttr.clear();

      std::map<int, double> cellTypeAreaMap, cellTypeAreaMap2;
      std::map<int, int> cellTypeCountMap, cellTypeCountMap2;
      auto &labelMap = mesh->labelMap("Parents");
      auto &labelMap2 = mesh2->labelMap("Parents");

      cellTypeAreaMap.clear();

      double total2 = 0;


      double total = 0;


      for(CCIndex f : cs2->faces()) {
        int parent = labelMap2[indexAttr2[f].label];

        cellTypeAreaMap2[parent] += indexAttr2[f].measure;
        total2 += indexAttr2[f].measure;

        cellTypeCountMap2[parent]++;


      }

      for(CCIndex f : cs->faces()) {
        int parent = labelMap[indexAttr[f].label];

        cellTypeAreaMap[parent] += indexAttr[f].measure;
        total += indexAttr[f].measure;

        cellTypeCountMap[parent]++;

      }

      if(total == 0) return false;

      for(auto p : cellTypeAreaMap){
        std::cout << "parent label " << p.first << std::endl;
        std::cout << "mesh1: " << " total area "
        << p.second << " / cell number " << cellTypeCountMap[p.first] << " / area per cell " << p.second / (double)cellTypeCountMap[p.first]
        << " / total area ratio " << (p.second / cellTypeAreaMap2[p.first] * total2/total) << std::endl;

        std::cout << "mesh2: cell number " << cellTypeCountMap2[p.first] << " / area per cell " << cellTypeAreaMap2[p.first] / (double)cellTypeCountMap2[p.first] << std::endl;

      }

      std::cout << "parent label 1 and 7 " << std::endl;
      std::cout << "mesh1: " << " total area "
      << cellTypeAreaMap[1]+cellTypeAreaMap[7] << " / cell number " << cellTypeCountMap[1]+cellTypeCountMap[7] << " / area per cell "
      << (cellTypeAreaMap[1]+cellTypeAreaMap[7]) / (double)(cellTypeCountMap[1]+cellTypeCountMap[7])
      << " / total area ratio " << ((cellTypeAreaMap[1]+cellTypeAreaMap[7]) / (cellTypeAreaMap2[1]+cellTypeAreaMap2[7]) * total2/total) << std::endl;

      std::cout << "mesh2: cell number " << cellTypeCountMap2[1]+cellTypeCountMap2[7];// << " / area per cell " << cellTypeAreaMap2[p.first] / (double)cellTypeCountMap2[p.first] << std::endl;


        // std::cout << "epidermis " << p.first << "/ area " << p.second << "/ area ratio " << (p.second / cellTypeAreaMap2[p.first] * total2/total)
        // << "/ cell number " << cellTypeCountMap[p.first] << "/ area per cell " << p.second / (double)cellTypeCountMap[p.first]
        // << "/ cell number mesh 2 " << cellTypeCountMap2[p.first] << "/ area per cell mesh 2 " << cellTypeAreaMap2[p.first] / (double)cellTypeCountMap2[p.first] << std::endl;


      std::cout << "total " << total << "/" << total2 << std::endl;

      sizeAttr[0] = total;
      sizeAttr[1] = total2;

      QString filename;

      if(parm("Filename") == ""){
        filename = parm("Sample") + ".csv";
      }
      if(filename != ""){

      QFile file(filename);
      if(!file.open(QIODevice::WriteOnly)) {
        setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
        return false;
      }
      QTextStream out(&file);

      out << "Sample,Genotype,Slice,CellType,CellNumber,CellArea,GrowthArea,Ratio";
      out << endl;

      int totalCell = 0;
      int innerCell = 0;
      int outerCell = 0;
      double totalArea = 0;
      double totalInner = 0;
      double totalOuter = 0;

      for(auto p : cellTypeAreaMap){
        if(p.first < 1 or p.first > 7) continue;
        totalCell += cellTypeCountMap[p.first];
        totalArea += p.second;
        if(p.first == 1 or p.first == 2 or p.first == 7){
          totalOuter += p.second;
          outerCell += cellTypeCountMap[p.first];
        }
        if(p.first == 3 or p.first == 4 or p.first == 5){
          totalInner += p.second;
          innerCell += cellTypeCountMap[p.first];
        }
      }

      int totalCell2 = 0;
      int innerCell2 = 0;
      int outerCell2 = 0;
      double totalArea2 = 0;
      double totalInner2 = 0;
      double totalOuter2 = 0;

      for(auto p : cellTypeAreaMap2){
        if(p.first < 1 or p.first > 7) continue;
        totalCell2 += cellTypeCountMap2[p.first];
        totalArea2 += p.second;
        if(p.first == 1 or p.first == 2 or p.first == 7){
          totalOuter2 += p.second;
          outerCell2 += cellTypeCountMap2[p.first];
        }
        if(p.first == 3 or p.first == 4 or p.first == 5){
          totalInner2 += p.second;
          innerCell2 += cellTypeCountMap2[p.first];
        }
      }

      for(auto p : cellTypeAreaMap){
        if(p.first < 1 or p.first > 7) continue;
        out << parm("Sample") << "," << parm("Genotype")  << ",8um," << p.first << "," << cellTypeCountMap[p.first] << "," << p.second << ",," << (p.second/total) / (cellTypeAreaMap2[p.first]/total2) << endl;
      }

      out << parm("Sample") << "," << parm("Genotype") << ",8um,inner," << innerCell << "," << totalInner << ",," << (totalInner/total) / (totalInner2/total2) << endl;
      out << parm("Sample") << "," << parm("Genotype")<< ",8um,outer," << outerCell << "," << totalOuter << ",," << (totalOuter/total) / (totalOuter2/total2) << endl;
      out << parm("Sample") << "," << parm("Genotype")<< ",8um,total," << totalCell << "," << totalArea << ",," << total/total2 << endl;


      for(auto p : cellTypeAreaMap2){
        if(p.first < 1 or p.first > 7) continue;
        out << parm("Sample") << "," << parm("Genotype") << ",100um," << p.first << "," << cellTypeCountMap2[p.first] << "," << p.second << "," << p.second/cellTypeAreaMap[p.first] << endl;
      }
      out << parm("Sample") << ","<< parm("Genotype")<< ",100um,inner," << innerCell2 << "," << totalInner2 << "," << totalInner2/totalInner << endl;
      out << parm("Sample") << ","<< parm("Genotype")<< ",100um,outer," << outerCell2 << "," << totalOuter2 << "," << totalOuter2/totalOuter << endl;
      out << parm("Sample") << ","<< parm("Genotype")<< ",100um,total," << totalCell2 << "," << totalArea2 << "," << totalArea2/totalArea << endl;

      }

      return true;
    }

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

    QString TissueName;
    QString TissueDualName;

  };


  // Growth &c.
  class CellDiskSelectWalls : public Process
  {
  public:
    CellDiskSelectWalls(const Process &process): Process(process)
    {
      setName("Model/04 Misc/Select Walls");
      addParm("Select Walls between Parent 1", "Select Walls of Parent", "1");
      addParm("Select Walls between Parent 2", "Select Walls of Parent", "2");
    }

    bool run(){


      mesh = currentMesh();
      if(!mesh)
        throw(QString("CellDiskAreaQuant::initialize: Invalid mesh."));
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("CellDiskAreaQuant::initialize: Invalid cell complex.");
      CCStructure *cs = &mesh->ccStructure(ccName);
      if(!cs)
        throw QString("CellDiskAreaQuant::initialize: Failed to get cell complex.");



      CCIndexDataAttr &indexAttr = mesh->indexAttr();



    if(!mesh)
      throw(QString("CellDiskAreaQuant.step: Mesh not initialized"));


    int label1 = parm("Select Walls between Parent 1").toInt();
    int label2 = parm("Select Walls between Parent 2").toInt();

    auto &labelMap = mesh->labelMap("Parents");

    if(label1 > label2){
      int h = label1;
      label1 = label2;
      label2 = h;
    }


      for(CCIndex e : cs->edges()) {
        CCIndexPair ep = cs->edgeBounds(e);

        std::set<CCIndex> cells = cs->incidentCells(e, 2);

        if(cells.size() == 2){
          if(label1 == 0) continue;

          CCIndex c1 = *(cells.begin());
          CCIndex c2 = *(++cells.begin());

          int labelCell1 = labelMap[indexAttr[c1].label];
          int labelCell2 = labelMap[indexAttr[c2].label];

          if(labelCell1 > labelCell2){
            int h = labelCell1;
            labelCell1 = labelCell2;
            labelCell2 = h;
          }

          if(label1 == labelCell1 and label2 == labelCell2){
            // select vertices of edge
            indexAttr[ep.first].selected = true;
            indexAttr[ep.second].selected = true;
          }

        } else if(cells.size() == 1) {
          if(label1 > 0) continue;

          CCIndex c1 = *(cells.begin());

          int labelCell1 = labelMap[indexAttr[c1].label];

          if(labelCell1 == label2){
            // select vertices of edge
            indexAttr[ep.first].selected = true;
            indexAttr[ep.second].selected = true;
          }


        } else {
          mdxInfo << "sth wrong" << endl;
        }



      }

      mesh->updateAll(ccName);

      return true;
    }

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

    QString TissueName;
    QString TissueDualName;

  };



  // Growth &c.
  class CellDiskLoadParameterFile : public Process
  {
  public:
    CellDiskLoadParameterFile(const Process &process): Process(process)
    {
      setName("Model/01 Cell Disk/File Load");
      //addParm("Select Walls between Parent 1", "Select Walls of Parent", "1");
      //addParm("Select Walls between Parent 2", "Select Walls of Parent", "2");
    }

    //bool initialize(QWidget *parent);

    bool run(){


      mesh = currentMesh();
      if(!mesh)
        throw(QString("CellDiskAreaQuant::initialize: Invalid mesh."));
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("CellDiskAreaQuant::initialize: Invalid cell complex.");
      CCStructure *cs = &mesh->ccStructure(ccName);
      if(!cs)
        throw QString("CellDiskAreaQuant::initialize: Failed to get cell complex.");



      CCIndexDataAttr &indexAttr = mesh->indexAttr();



    if(!mesh)
      throw(QString("CellDiskAreaQuant.step: Mesh not initialized"));


      return true;
    }

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

    QString TissueName;
    QString TissueDualName;

  };

  // Growth &c.
  class CellDiskSaveParameterFile : public Process
  {
  public:
    CellDiskSaveParameterFile(const Process &process): Process(process)
    {
      setName("Model/01 Cell Disk/File Save");
      //addParm("Select Walls between Parent 1", "Select Walls of Parent", "1");
      //addParm("Select Walls between Parent 2", "Select Walls of Parent", "2");
    }

    bool run(){


      mesh = currentMesh();
      if(!mesh)
        throw(QString("CellDiskAreaQuant::initialize: Invalid mesh."));
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("CellDiskAreaQuant::initialize: Invalid cell complex.");
      CCStructure *cs = &mesh->ccStructure(ccName);
      if(!cs)
        throw QString("CellDiskAreaQuant::initialize: Failed to get cell complex.");



      CCIndexDataAttr &indexAttr = mesh->indexAttr();



    if(!mesh)
      throw(QString("CellDiskAreaQuant.step: Mesh not initialized"));


      return true;
    }

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

    QString TissueName;
    QString TissueDualName;

  };



  // Growth &c.
  class CellDiskDivSetAttr : public Process
  {
  public:
    CellDiskDivSetAttr(const Process &process): Process(process)
    {
      setName("Model/01 Cell Disk/Set Attr Division");

      addParm("Attribute Name", "Name of attribute containing division data", "MassSpringCellData");
      addParm("Div Area", "Div Area", "50");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
    }

    bool run(){


      mesh = currentMesh();
      if(!mesh)
        throw(QString("MechanicsSolver::initialize: Invalid mesh."));
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("MechanicsSolver::initialize: Invalid cell complex.");
      CCStructure *cs = &mesh->ccStructure(ccName);
      if(!cs)
        throw QString("MechanicsSolver::initialize: Failed to get cell complex.");

    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDisk::initialize Cannot make tissue process"));

    //TissueName = tissueProcess->parm("Tissue");
    //TissueDualName = tissueProcess->parm("Tissue Dual");

      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      //std::cout << "div " << cs->edges().size() << std::endl;

      //auto &attr = mesh->attributes().attrMap<CCIndex,double>(parm("Attribute Name"));
      MSCellDataAttr &msCellAttr = mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");

      double divArea = parm("Div Area").toDouble();

      CellTissue* tissue = &(tissueProcess->tissue());

    if(!mesh)
      throw(QString("CellTissueCell2dDivide.step: Mesh not initialized"));
    if(!tissue)
      throw(QString("CellTissueCell2dDivide.step: Tissue not initialized"));


    forall(CCIndex cell, tissue->cellStructure().faces()) {
      if(indexAttr[cell].selected) msCellAttr[cell].divArea = divArea;

    }

      return true;
    }

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

    QString TissueName;
    QString TissueDualName;

  };


  class CellDiskDivide : public CellTissueCell2dDivide
  {
  public:
    CellDiskDivide(const Process &process) : CellTissueCell2dDivide(process)
    {
      setName("Model/01 Cell Disk/Divide");
      addParm("Meinhardt Solver Process", "Name of the process for the Meinhardt solver",
              "Model/02 Meinhardt/01 Solver");
      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
    }

    // Initialize to grab subdivider
    bool initialize(QWidget *parent);

    // Run a step of cell division
    bool step();

    CellDiskSubdivide *subdivider() { return &subdiv; }

  private:
    CellDiskSubdivide subdiv;

    CellDiskMeinhardtSolver *meinhardtSolver = NULL;
    CellDiskTissue *tissueProcess = NULL;

    QString TissueName;
    QString TissueDualName;

  };

  class CellDiskSplitEdges : public SplitEdges
  {
  public:
    CellDiskSplitEdges(const Process &process) : SplitEdges(process)
    {
      setName("Model/01 Cell Disk/Split Edges");
      addParm("Division Process", "Process for 2D cell division", "Model/01 Cell Disk/Divide");
    }

    // we need to use our custom subdivider!
    bool run()
    {

      std::cout << "subdivider" << std::endl;

      Mesh *mesh = currentMesh();
      if(!mesh)
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      CellDiskDivide *divProcess = dynamic_cast<CellDiskDivide*>(getProcess(parm("Division Process")));
      if(!divProcess)
        throw QString("CellDiskSplitEdges::run: Unable to find division process.");

      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updateAll(ccName);

      SplitEdges::run(*mesh, cs, parm("Max Length").toDouble(), divProcess->subdivider());

      setStatus(QString("Split Edges, vertices %1, edges %2, faces %3.")
          .arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()));

      return true;
    }
  };

  class ReadParmFile : public Process
  {
  public:
    ReadParmFile(const Process &process) : Process(process)
    {
      setName("Model/04 Misc/Parm File Load");
      setDesc("Read and load a parameter file");
      //addParm("Division Process", "Process for 2D cell division", "Model/01 Cell Disk/Divide");
      addParm("Filename", "Filename", "");
      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/01 Cell Disk/Cell Tissue");

    }
    QString filename;

    bool initialize(QWidget* parent);
    bool run();

  private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

  };

  class WriteParmFile : public Process
  {
  public:
    WriteParmFile(const Process &process) : Process(process)
    {
      setName("Model/04 Misc/Parm File Save");
      setDesc("Write and save a parameter file");
      //addParm("Division Process", "Process for 2D cell division", "Model/01 Cell Disk/Divide");
      addParm("Filename", "Filename", "");
      addParm("Spring Attribute", "Name of attribute containing spring data", "Spring Data");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/01 Cell Disk/Cell Tissue");

    }
    QString filename;

    bool initialize(QWidget* parent);
    bool run();

    private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

  };

  class VisualizeParms : public Process
  {
  public:
    VisualizeParms(const Process &process) : Process(process)
    {
      setName("Model/04 Misc/Visualize Parms");
      setDesc("Visualize Div Areas");
      addParm("Tissue Process", "Name of the process for the Cell Tissue", "Model/01 Cell Disk/Cell Tissue");
    }
    QString filename;

    bool run();

    private:
    Mesh *mesh = NULL;
    CellDiskTissue *tissueProcess = NULL;

  };


}
#endif
