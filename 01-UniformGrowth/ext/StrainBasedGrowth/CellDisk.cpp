//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CellDisk.hpp>

namespace CellDisk
{
  bool CellDisk::initialize(QWidget *parent)
  {
    // Get the current mesh
    mesh = currentMesh();
    if(!mesh)
      throw(QString("CellDisk::initialize No current mesh"));

    if(!getProcess(parm("Divide Process"), divideProcess))
      throw(QString("CellDisk::initialize: Cannot make divide cell process."));
    divideProcess->initialize(parent);

    if(!getProcess(parm("Growth Process"), growthProcess))
      throw(QString("CellDisk::initialize: Cannot make growth process."));
    growthProcess->initialize(parent);

    // Growth initializes the tissue process
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDisk::initialize: Cannot make tissue process."));

    // CellDivide initializes the Meinhardt solver
    if(!getProcess(parm("Meinhardt Solver Process"), meinhardtSolver))
      throw(QString("CellDisk::initialize: Cannot make Meinhardt solver process."));

    // Get the split edges process
    if(!getProcess(parm("Split Edges Process"), splitEdgesProcess))
      throw(QString("CellDisk::initialize: Cannot make splitEdges process."));

    // Get the mechanics solver process
    if(!getProcess(parm("Mechanics Solver Process"), mechanicsSolver))
      throw(QString("CellDisk::initialize: Cannot make mechanics solver process."));
    mechanicsSolver->initialize(parent);

    return true;
  }

  bool CellDisk::step()
  {
    // Run mechanics to equilibrium
    mechanicsSolver->initialize(0);
    mechanicsSolver->run();

    double xNorm = mechanicsSolver->calcXNorm();
    double dxNorm = mechanicsSolver->calcDXNorm();

std::cout << "after run " << xNorm << "/" << dxNorm << std::endl;

    // Grow
    growthProcess->step();

    // Split edges
    splitEdgesProcess->run();

    // Split large-enough cells
    //divideProcess->initialize(0);
    divideProcess->step();

    // Meinhardt morphogen process
    //meinhardtSolver->initSolver(&tissueProcess->tissue().dualGraph());
    //meinhardtSolver->step();

    //tissueProcess->tissue().tissueName()->createDual();
    //tissueProcess->tissue().tissueDualName()->updateGeometry();

    mesh->updateAll(tissueProcess->tissue().tissueName());
    mesh->updateAll(tissueProcess->tissue().tissueDualName());

    CellDiskAreaQuant cdaq(*this);
    cdaq.run();

    AttrMap<int,double> &sizeAttr = mesh->attributes().attrMap<int,double>("Root Area");

    if(sizeAttr[0] > sizeAttr[1] and stringToBool(parm("Stop Size"))) return false;

    if(growthProcess->totalTime > parm("Max Time").toDouble()) return false;

    return true;
  }

  bool CellDisk::rewind(QWidget *parent)
  {
    // To rewind, we'll reload the mesh
    mesh = currentMesh();
    if(!mesh or mesh->file().isEmpty())
      throw(QString("No current mesh, cannot rewind"));
    MeshLoad meshLoad(*this);
    meshLoad.setParm("File Name", mesh->file());

    return meshLoad.run();
  }

  bool CellDiskGrowth::initialize(QWidget *parent)
  {
    // Get the current mesh
    mesh = currentMesh();
    if(!mesh)
      throw(QString("CellDiskGrowth::initialize: No current mesh."));

    // Initialize the tissue
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDiskGrowth::initialize: Cannot make tissue process."));
    tissueProcess->initialize(parent);

    totalTime = 0;

    return true;
  }

  bool CellDiskGrowth::step()
  {
    std::cout << "growth!" << std::endl;

    if(!mesh)
      throw(QString("CellDiskGrowth::step: No current mesh."));
    if(!tissueProcess)
      throw(QString("CellDiskGrowth::step: No tissue process."));

    if(parm("Spring Attribute").isEmpty())
      throw QString("CellDiskGrowth::step: Spring attribute name empty.");
    SpringAttr &springAttr =
      mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

    bool hasVal;
    double dt = parm("Dt").toDouble(&hasVal);
    if(!hasVal) dt = 0.1;
    double rate = parm("Rate").toDouble(&hasVal);
    if(!hasVal) rate = 1.0;

    double mul = dt * rate;
    if(mul > 1.0) mul = 1.0; // no overshoot

    std::cout << "rates " << dt << "/" << rate << "/" << springAttr.size() << std::endl;

    CCStructure &cs = tissueProcess->tissue().cellStructure();
    CCIndexDataAttr &indexAttr = tissueProcess->tissue().indexAttr();

    //auto &attrGrowth = mesh->attributes().attrMap<CCIndex,double>("Growth Data");
    //auto &attrGrowthConst = mesh->attributes().attrMap<CCIndex,double>("Growth Data Const");

    // MSEdgeDataAttr *msEdgeAttr = &mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>("MassSpringEdgeData");

    MSEdgeDataAttr &msEdgeAttr2 = mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>("MassSpringEdgeData");

    //std::cout << "cs " << cs.edges().size() << "/" << attrGrowth.size() << std::endl;
    // std::cout << "loop " << msEdgeAttr2.size() << "/" << springAttr.size() << std::endl;
    for(CCIndex edge : cs.edges()) {

      SpringAttr::iterator sprIter = springAttr.find(edge);
      MSEdgeDataAttr::iterator it = msEdgeAttr2.find(edge);
      if(sprIter == springAttr.end()) continue;
      if(it == msEdgeAttr2.end()) continue;
      MassSpringEdgeData &msd = it->second;
      SpringData &spr = sprIter->second;

      Point3d p0 = indexAttr[spr.v[0]].pos, p1 = indexAttr[spr.v[1]].pos;
      double newLen = norm(p1 - p0);
      double mul = msd.growthRate * dt;
      //if(mul < 0.) mul = 0.;
      //if(mul > 1.) mul = 1.;

      spr.restLength += mul * (newLen - spr.restLength);
    }      
    
    totalTime += dt;
    std::cout << "total time passed:  " << totalTime << std::endl;
    return true;
  }

  // Initialize to grab subdivider
  bool CellDiskDivide::initialize(QWidget *parent)
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw(QString("CellDiskDivide::step No current mesh"));

    // Call base initialize
    if(!CellTissueCell2dDivide::initialize(parent))
      return false;

    if(!getProcess(parm("Meinhardt Solver Process"), meinhardtSolver))
      throw(QString("CellDiskDivide::initialize Cannot make Meinhardt solver process"));
    meinhardtSolver->initialize(parent);

    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDisk::initialize Cannot make tissue process"));

    TissueName = tissueProcess->parm("Tissue");
    TissueDualName = tissueProcess->parm("Tissue Dual");

    SpringAttr &sprAttr = mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

    // Setup subdivision objects
    subdiv.mdx = CellTissueCell2dDivide::subdivider();
    subdiv.meinhardt = MeinhardtAI::Subdivide(meinhardtSolver->attr());
    subdiv.spring = SpringSubdivide(mesh->indexAttr(), sprAttr);

    CCIndexDataAttr *indexAttr = &mesh->indexAttr();
    SpringAttr *sprAttr2 = &mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

    std::cout << "sprAttr b" << (*sprAttr2).size() << std::endl;

    // Setup subdivision objects
    MSEdgeDataAttr *msEdgeAttr = &mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>("MassSpringEdgeData");
    MSCellDataAttr *msCellAttr = &mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");
    subdiv.ms = MassSpringSubdivide(msEdgeAttr, indexAttr, sprAttr2, msCellAttr);
    std::cout << "sprAttr a" << (*sprAttr2).size() << std::endl;
    return true;
  }

  // Run a step of cell division
  bool CellDiskDivide::step() 
  { 

    std::cout << "div " << std::endl;

    Mesh *mesh = currentMesh();
    Subdivide *subdiv2 = &subdiv;
    CellTissue *tissue = &(tissueProcess->tissue());

    //auto &divData = mesh->attributes().attrMap<CCIndex,double>("Division Data");

    MSCellDataAttr &msCellAttr = mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");

    if(!mesh)
      throw(QString("CellTissueCell2dDivide.step: Mesh not initialized"));
    if(!tissue)
      throw(QString("CellTissueCell2dDivide.step: Tissue not initialized"));

    CCIndexDataAttr &indexAttr = tissue->indexAttr();

    // Check if we need to use selection
    // implement later

    // Find cells to divide
    std::vector<CCIndex> toDivide;
    forall(CCIndex cell, tissue->cellStructure().faces()) {
      double area = indexAttr[cell].measure;
//std::cout << "areas " << indexAttr[cell].label << "/" << indexAttr[cell].measure << std::endl;
      double maxArea = msCellAttr[cell].divArea;//divData[cell];
      if(maxArea < 1) continue;
      if(area > maxArea)
        toDivide.push_back(cell);
    }
    //if(toDivide.empty())
    //  return false;
    
    std::cout << "div cells " << toDivide.size() << std::endl;

    for(const CCIndex &cell : toDivide) {
      //if(Verbose)
      //  mdxInfo << "Dividing cell (" << to_string(cell).c_str() << "), with area "
      //          << indexAttr[cell].measure << endl;
      divideCell2d(tissue->cellStructure(), indexAttr, cell, *this,
                   subdiv2, Cell2dDivideParms::SHORTEST_WALL_THROUGH_CENTROID);
      break; // only 1 cell per round can divide
    }

    //CCStructure &cs = tissue->cellStructure();

    //auto &attrGrowth = mesh->attributes().attrMap<CCIndex,double>("Growth Data");
    //auto &attrSpring = mesh->attributes().attrMap<CCIndex,SpringData>("Spring Data");

    //std::cout << "bla " << attrGrowth.size() << "/" << attrSpring.size() << "/" << cs.edges().size() << std::endl;

    // go through all edges, check which don't have values
    //for(CCIndex edge : cs.edges()) {
     // if(attrGrowth.find(edge) != attrGrowth.end() and attrSpring.find(edge) != attrSpring.end()) continue;
     // std::cout << "edge found" << std::endl;

      // look for neighbor edges

    CCStructure &cs = tissueProcess->tissue().cellStructure();

    SpringAttr &springAttr =
      mesh->attributes().attrMap<CCIndex,SpringData>("Spring Data");
    MSEdgeDataAttr &msEdgeAttr2 = mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>("MassSpringEdgeData");

    AttrMap<std::pair<int,int>, double> &edgeTypeStiffn = mesh->attributes().attrMap<std::pair<int,int>, double>("Edge Type Stiffness");
    AttrMap<std::pair<int,int>, double> &edgeTypeGrowth = mesh->attributes().attrMap<std::pair<int,int>, double>("Edge Type Growth");

    auto &labelMap = mesh->labelMap("Parents");

    if(!edgeTypeStiffn.empty() and !edgeTypeGrowth.empty()){

      for(CCIndex e : cs.edges()){
        std::pair<int,int> edgeLabels = std::make_pair(0,0);
        for(CCIndex f : cs.incidentCells(e,2)){
          int currentLabel = labelMap[indexAttr[f].label];
          if(edgeLabels.second == 0) edgeLabels.second = currentLabel;
          else edgeLabels.first = currentLabel;

          if(edgeLabels.first < edgeLabels.second){
            int h = edgeLabels.second;
            edgeLabels.second = edgeLabels.first;
            edgeLabels.first = h;
          }
        }

        springAttr[e].stiffness = edgeTypeStiffn[edgeLabels];
        msEdgeAttr2[e].growthRate = edgeTypeGrowth[edgeLabels];
      }
    }


    //}

    tissue->createDual();
    tissue->updateGeometry();

    // Update mesh points, edges, surfaces
    mesh->updateAll(TissueName);
    mesh->updateAll(TissueDualName);   

    std::cout << "done" << std::endl;

    return true; 


    // Pass our subdivide
    //return CellTissueCell2dDivide::step(currentMesh(), &subdiv);
  }

  REGISTER_PROCESS(CellDisk);

  REGISTER_PROCESS(CellDiskMeinhardt);
  REGISTER_PROCESS(CellDiskMeinhardtSolver);

  REGISTER_PROCESS(CellDiskDivide);
  REGISTER_PROCESS(CellDiskGrowth);
  REGISTER_PROCESS(CellDiskTissue);
  REGISTER_PROCESS(CellDiskSplitEdges);

  REGISTER_PROCESS(CellDiskGrowthSetAttr);
//REGISTER_PROCESS(CellDiskGrowthSetAttrConst);
  REGISTER_PROCESS(CellDiskDivSetAttr);

  // mechanics stuff
  bool CellDiskMechanicsSolver::initialize(QWidget *parent)
  {
    mesh = currentMesh();
    if(!mesh)
      throw(QString("MechanicsSolver::initialize: Invalid mesh."));
    ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw QString("MechanicsSolver::initialize: Invalid cell complex.");
    cs = &mesh->ccStructure(ccName);
    if(!cs)
      throw QString("MechanicsSolver::initialize: Failed to get cell complex.");
  
    // Get the derivatives processes.
    clearDerivs();
#ifdef WITH_AD
    BoundaryPressureAD *bpProcess = dynamic_cast<BoundaryPressureAD*>(getProcess(parm("Pressure Derivs")));
#else // !WITH_AD
    BoundaryPressure *bpProcess = dynamic_cast<BoundaryPressure*>(getProcess(parm("Pressure Derivs")));
#endif // WITH_AD
    if(!bpProcess)
      throw QString("MechanicsSolver::initialize: Unable to find pressure derivs process.");
    bpProcess->initialize(*mesh,*cs);
    addDerivs(bpProcess);

    SpringDerivs *spProcess = dynamic_cast<SpringDerivs*>(getProcess(parm("Spring Derivs")));
    if(!spProcess)
      throw QString("MechanicsSolver::initialize: Unable to find spring derivs process.");
    spProcess->initialize(*mesh);
    addDerivs(spProcess);

    // Set up the solver.
    initSolver(cs);
    return true;
  }

  bool CellDiskMechanicsSolver::finalize(QWidget *parent)
  {
    finalizeSolver();
    return true;
  }

  bool CellDiskMechanicsSolver::step(void)
  {
    if(!mesh || !cs)
      throw(QString("CellDiskMechanicsSolver::run: Invalid mesh."));

    // Run the solver.
    solve();

SpringAttr *sprAttr2 = &mesh->attributes().attrMap<CCIndex,SpringData>("Spring Data");

    std::cout << "sprAttr s" << (*sprAttr2).size() << std::endl;

  
    // Update the GUI.
    mesh->updatePositions(ccName);

    if(!progressAdvance())
      userCancel();

    double xNorm = calcXNorm();
    double dxNorm = calcDXNorm();
    if(dxNorm < parm("Converge Threshold").toDouble()) {
      mdxInfo << "Converged: x-norm = " << xNorm << ", dx-norm = " << dxNorm << endl;
      return false;
    }
    mdxInfo << "Not converged: x-norm = " << xNorm << ", dx-norm = " << dxNorm << endl;
    return true;
  }

// file dialogue for save data
bool ReadParmFile::initialize(QWidget* parent)
 {
   mesh = currentMesh();

    if(!mesh)
      throw(QString("CellDiskGrowth::initialize: No current mesh."));

    // Initialize the tissue
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDiskGrowth::initialize: Cannot make tissue process."));


   filename = parm("Filename");
   if(parent)
     filename = QFileDialog::getOpenFileName(0, "Choose spreadsheet file to load", QDir::currentPath(), "parm files (*.v)");
   if(filename.isEmpty())
     return false;
   if(!filename.endsWith(".v", Qt::CaseInsensitive))
     filename += ".v";

   setParm("Filename", filename);


   if(!parent) return true;

   return true;
 }

  bool ReadParmFile::run()
  {


    // Read parameters
    QFileInfo fileInfo(filename);
    if(!fileInfo.exists() or !fileInfo.isFile() or !fileInfo.isReadable())
      throw(QString("Unable to read parameter file: %1").arg(filename));

    // no effect
    // CellDiskTissue cdt(*this);
    // cdt.step();
    // cdt.run();
  
    Parms parms(filename);
  
    std::vector<double> growthRate;
    std::vector<double> stiffness;
    std::vector<double> divArea;

  
    parms.all("Main", "GrowthRate", growthRate);
    parms.all("Main", "Stiffness", stiffness);
    parms.all("Main", "DivisionArea", divArea);

    auto &labelMap = mesh->labelMap("Parents");

    if(growthRate.size() != 14)
      throw(QString("Error: not all wall types specified for GrowthRate. Need 14 in total."));

    if(stiffness.size() != 14)
      throw(QString("Error: not all wall types specified for Stiffness. Need 14 in total."));

    if(divArea.size() != 6)
      throw(QString("Error: not all cell types specified for DivisionArea. Need 6 in total."));

    SpringAttr &springAttr =
      mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

    CCStructure &cs = tissueProcess->tissue().cellStructure();
    CCIndexDataAttr &indexAttr = tissueProcess->tissue().indexAttr();

    //springAttr.clear();


    MSEdgeDataAttr &msEdgeAttr2 = mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>("MassSpringEdgeData");
    MSCellDataAttr &msCellAttr = mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");

    AttrMap<std::pair<int,int>, double> &edgeTypeStiffn = mesh->attributes().attrMap<std::pair<int,int>, double>("Edge Type Stiffness");
    edgeTypeStiffn.clear();
    AttrMap<std::pair<int,int>, double> &edgeTypeGrowth = mesh->attributes().attrMap<std::pair<int,int>, double>("Edge Type Growth");
    edgeTypeGrowth.clear();

    //msEdgeAttr2.clear();

    std::map<std::pair<int,int>, int> wallTypeIndexMap;

    for(int i = 0; i < 5; i++){
      std::pair<int,int> w1 = std::make_pair(i+1,i);
      std::pair<int,int> w2 = std::make_pair(i+1,i+1); 
      wallTypeIndexMap[w1] = 2*i;
      wallTypeIndexMap[w2] = 2*i+1;
    }
      std::pair<int,int> w07 = std::make_pair(7,0);
      std::pair<int,int> w17 = std::make_pair(7,1); 
      std::pair<int,int> w27 = std::make_pair(7,2);
      std::pair<int,int> w77 = std::make_pair(7,7); 
      wallTypeIndexMap[w07] = 10;
      wallTypeIndexMap[w17] = 11;
      wallTypeIndexMap[w27] = 12;
      wallTypeIndexMap[w77] = 13;

    for(CCIndex e : cs.edges()){
      std::pair<int,int> edgeLabels = std::make_pair(0,0);
      for(CCIndex f : cs.incidentCells(e,2)){
        int currentLabel = labelMap[indexAttr[f].label];
        if(edgeLabels.second == 0) edgeLabels.second = currentLabel;
        else edgeLabels.first = currentLabel;

        if(edgeLabels.first < edgeLabels.second){
          int h = edgeLabels.second;
          edgeLabels.second = edgeLabels.first;
          edgeLabels.first = h;
        }
      }

      springAttr[e].stiffness = stiffness[wallTypeIndexMap[edgeLabels]];
      msEdgeAttr2[e].growthRate = growthRate[wallTypeIndexMap[edgeLabels]];
      edgeTypeStiffn[edgeLabels] = stiffness[wallTypeIndexMap[edgeLabels]];
      edgeTypeGrowth[edgeLabels] = growthRate[wallTypeIndexMap[edgeLabels]];
    }

    for(CCIndex f : cs.faces()){
      int currentLabel = labelMap[indexAttr[f].label];
      if(currentLabel == 7) currentLabel--;
      msCellAttr[f].divArea = divArea[currentLabel-1];
    }

    // set rest lengths
    for(CCIndex e : cs.edges()){
      springAttr[e].setVertices(cs, indexAttr, e);
      springAttr[e].setRestLengthToCurrent(indexAttr, 1.0);
    }

    std::cout << "Loaded growth rates" << std::endl;
    for(int i = 0; i < growthRate.size(); i++){
      std::cout << growthRate[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Loaded stiffness" << std::endl;
    for(int i = 0; i < stiffness.size(); i++){
      std::cout << stiffness[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Loaded division areas" << std::endl;
    for(int i = 0; i < divArea.size(); i++){
      std::cout << divArea[i] << " ";
    }
    std::cout << std::endl;

    return true;
  }
  REGISTER_PROCESS(ReadParmFile);

// file dialogue for save data
bool WriteParmFile::initialize(QWidget* parent)
 {
   mesh = currentMesh();

    if(!mesh)
      throw(QString("CellDiskGrowth::initialize: No current mesh."));

    // Initialize the tissue
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDiskGrowth::initialize: Cannot make tissue process."));

   filename = parm("Filename");
   if(parent)
     filename = QFileDialog::getSaveFileName(0, "Choose spreadsheet file to save", QDir::currentPath(), "parm files (*.v)");
   if(filename.isEmpty())
     return false;
   if(!filename.endsWith(".v", Qt::CaseInsensitive))
     filename += ".v";

   setParm("Filename", filename);


   if(!parent) return true;

   return true;
 }

  bool WriteParmFile::run()
  {

    // Read parameters
    // QFileInfo fileInfo(filename);
    // if(!fileInfo.exists() or !fileInfo.isFile() or !fileInfo.isReadable())
    //   throw(QString("Unable to read parameter file: %1").arg(filename));
  
   QFile file(filename);
   if(!file.open(QIODevice::WriteOnly)) {
     throw(QString("File '%1' cannot be opened for writing").arg(filename));
   }

    QTextStream out(&file);

    mesh = currentMesh();
    auto &labelMap = mesh->labelMap("Parents");

    SpringAttr &springAttr =
      mesh->attributes().attrMap<CCIndex,SpringData>(parm("Spring Attribute"));

    MSEdgeDataAttr &msEdgeAttr2 = mesh->attributes().attrMap<CCIndex,MassSpringEdgeData>("MassSpringEdgeData");
    MSCellDataAttr &msCellAttr = mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");

    //auto &divData = mesh->attributes().attrMap<CCIndex,double>("Division Data");

    CCStructure &cs = tissueProcess->tissue().cellStructure();
    CCIndexDataAttr &indexAttr = tissueProcess->tissue().indexAttr();

    std::map<std::pair<int,int>, double> wallTypeStiffnessMap, wallTypeGrowthMap;
    std::map<int, double> cellTypeDivMap;

    for(CCIndex e : cs.edges()){
      std::pair<int,int> edgeLabels = std::make_pair(0,0);
      for(CCIndex f : cs.incidentCells(e,2)){
        int currentLabel = labelMap[indexAttr[f].label];
        if(edgeLabels.second == 0) edgeLabels.second = currentLabel;
        else edgeLabels.first = currentLabel;

        if(edgeLabels.first < edgeLabels.second){
          int h = edgeLabels.second;
          edgeLabels.second = edgeLabels.first;
          edgeLabels.first = h;
        }

      }

      if(wallTypeStiffnessMap.find(edgeLabels) == wallTypeStiffnessMap.end()){
        wallTypeStiffnessMap[edgeLabels] = springAttr[e].stiffness;
      } else {
        if(wallTypeStiffnessMap[edgeLabels] != springAttr[e].stiffness) std::cout << "Warning Edge not initialized: Stiffness " << edgeLabels.first << "/" << edgeLabels.second << "//" << wallTypeStiffnessMap[edgeLabels] << "/" << springAttr[e].stiffness << std::endl;
      }

      if(wallTypeGrowthMap.find(edgeLabels) == wallTypeGrowthMap.end()){
        wallTypeGrowthMap[edgeLabels] = msEdgeAttr2[e].growthRate;
      } else {
        if(wallTypeGrowthMap[edgeLabels] != msEdgeAttr2[e].growthRate) std::cout << "Warning Edge not initialized: Growth"  << edgeLabels.first << "/" << edgeLabels.second  << "//" << wallTypeGrowthMap[edgeLabels] << "/" << msEdgeAttr2[e].growthRate  << std::endl;
      }


    }

    std::vector<std::pair<int,int> > allWallTypes;

    allWallTypes.push_back(std::make_pair(1,0));
    allWallTypes.push_back(std::make_pair(1,1));
    allWallTypes.push_back(std::make_pair(2,1));
    allWallTypes.push_back(std::make_pair(2,2));
    allWallTypes.push_back(std::make_pair(3,2));
    allWallTypes.push_back(std::make_pair(3,3));
    allWallTypes.push_back(std::make_pair(4,3));
    allWallTypes.push_back(std::make_pair(4,4));
    allWallTypes.push_back(std::make_pair(5,4));
    allWallTypes.push_back(std::make_pair(5,5));
    allWallTypes.push_back(std::make_pair(7,0));
    allWallTypes.push_back(std::make_pair(7,1));
    allWallTypes.push_back(std::make_pair(7,2));
    allWallTypes.push_back(std::make_pair(7,7));


    std::vector<int> allCellTypes;

    allCellTypes.push_back(1);
    allCellTypes.push_back(2);
    allCellTypes.push_back(3);
    allCellTypes.push_back(4);
    allCellTypes.push_back(5);
    allCellTypes.push_back(7);



    for(auto p : allWallTypes){
      if(wallTypeGrowthMap.find(p) == wallTypeGrowthMap.end() or wallTypeStiffnessMap.find(p) == wallTypeStiffnessMap.end()){
        std::cout << "Warning: Wall type " << p.first << "/" << p.second << " not found, writing 0!" << std::endl;
        wallTypeStiffnessMap[p] = 0;
        wallTypeGrowthMap[p] = 0;
      }
    }


    for(CCIndex f : cs.faces()){
      int currentLabel = labelMap[indexAttr[f].label];

      if(cellTypeDivMap.find(currentLabel) == cellTypeDivMap.end()){
        cellTypeDivMap[currentLabel] = msCellAttr[f].divArea;// divData[f];
      } else {
        if(cellTypeDivMap[currentLabel] != msCellAttr[f].divArea) std::cout << "Warning" << std::endl;
      }

    }

    for(int t : allCellTypes){
      if(cellTypeDivMap.find(t) == cellTypeDivMap.end()){
        std::cout << "Warning: Cell type " << t << " not found, writing 0!" << std::endl;
        cellTypeDivMap[t] = 0;
      }
    }

    out << "// Root model parameter" << endl;
    out << endl;
    out << "[Main]" << endl;
    out << "// Edge based parameter" << endl;
    for(auto p : wallTypeGrowthMap){
      out << "GrowthRate: " << p.second << " // " << p.first.first << "/" << p.first.second << endl;
    }

    out << endl;
    for(auto p : wallTypeStiffnessMap){
      out << "Stiffness: " << p.second << " // " << p.first.first << "/" << p.first.second << endl;
    }

    out << endl;
    for(auto p : cellTypeDivMap){
      out << "DivisionArea: " << p.second << " // " << p.first << endl;
    }

    return true;
  }
  REGISTER_PROCESS(WriteParmFile);


  bool VisualizeParms::run()
  {

    // Read parameters
    // QFileInfo fileInfo(filename);
    // if(!fileInfo.exists() or !fileInfo.isFile() or !fileInfo.isReadable())
    //   throw(QString("Unable to read parameter file: %1").arg(filename));

    std::cout << "vis 1" << std::endl;

    mesh = currentMesh();

    if(!mesh)
      throw(QString("CellDiskGrowth::initialize: No current mesh."));

    QString ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw QString("%1::run No cell complex").arg(name());



    // Initialize the tissue
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDiskGrowth::initialize: Cannot make tissue process."));


    CCStructure &cs = tissueProcess->tissue().cellStructure();
    CCIndexDataAttr &indexAttr = tissueProcess->tissue().indexAttr();
  
   // QFile file(filename);
   // if(!file.open(QIODevice::WriteOnly)) {
   //   throw(QString("File '%1' cannot be opened for writing").arg(filename));
   // }

    // QTextStream out(&file);
std::cout << "vis 2" << std::endl;
    auto &labelMap = mesh->labelMap("Parents");

    MSCellDataAttr &msCellAttr = mesh->attributes().attrMap<CCIndex,MassSpringCellData>("MassSpringCellData");

    auto &heatAttr = mesh->heatAttr<double>("Div Area");
    heatAttr.clear();

    for(CCIndex f : cs.faces()){
      if(msCellAttr.find(f) == msCellAttr.end()){
        std::cout << "not found " << indexAttr[f].label << std::endl;
        continue;
      }
      heatAttr[labelMap[indexAttr[f].label]] = msCellAttr[f].divArea;
    }

    mesh->setHeatBounds(calcBounds(heatAttr), "Div Area");
    mesh->setHeatUnit("UM2", "Div Area");
    mesh->updateProperties(ccName);
    mesh->setHeat("Div Area");


    return true;
  }
  REGISTER_PROCESS(VisualizeParms);

  REGISTER_PROCESS(CellDiskGetAttr);


  REGISTER_PROCESS(CellDiskMechanics);
  REGISTER_PROCESS(CellDiskPressure);
  REGISTER_PROCESS(CellDiskMechanicsSolver);
  REGISTER_PROCESS(CellDiskSetRestLength);
  REGISTER_PROCESS(CellDiskSetMaterial);

  REGISTER_PROCESS(CellDiskAreaQuant);
  REGISTER_PROCESS(CellDiskSelectWalls);

#ifdef WITH_AD
  REGISTER_PROCESS(CellDiskPressureAD);
#endif // WITH_AD
}
