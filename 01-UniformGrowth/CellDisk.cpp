//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CellDisk.hpp>

namespace CellDisk
{
  bool CellDisk::initialize(QWidget *parent)
  {
    // Get the current mesh
    mesh = currentMesh();
    if(!mesh)
      throw(QString("CellDisk::initialize No current mesh"));

    if(!getProcess(parm("Divide Process"), divideProcess))
      throw(QString("CellDisk::initialize Cannot make divide cell process"));
    divideProcess->initialize(parent);

    if(!getProcess(parm("Growth Process"), growthProcess))
      throw(QString("CellDisk::initialize Cannot make growth process"));
    growthProcess->initialize(parent);

    // Growth initializes the tissue process
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDisk::initialize Cannot make tissue process"));

    // CellDivide initializeis the Meinhardt solver
    if(!getProcess(parm("Solver Process"), solverProcess))
      throw(QString("CellDisk::initialize Cannot make Meinhardt solver process"));

    // Get the split edges process
    if(!getProcess(parm("Split Edges Process"), splitEdgesProcess))
      throw(QString("CellDisk::initialize Cannot make splitEdges process"));

    return true;
  }

  bool CellDisk::step()
  {
    // Grow
    growthProcess->step();

    // Split large-enough cells
    divideProcess->step();

    // Split edges
    splitEdgesProcess->run();

    // Meinhardt morphogen process
    solverProcess->initSolver(&tissueProcess->tissue().dualGraph());
    solverProcess->step();

    mesh->updateAll(tissueProcess->tissue().tissueName());
    mesh->updateAll(tissueProcess->tissue().tissueDualName());

    return true;
  }

  bool CellDisk::rewind(QWidget *parent)
  {
    // To rewind, we'll reload the mesh
    mesh = currentMesh();
    if(!mesh or mesh->file().isEmpty())
      throw(QString("No current mesh, cannot rewind"));
    MeshLoad meshLoad(*this);
    meshLoad.setParm("File Name", mesh->file());
    return meshLoad.run();
  }

  bool CellDiskGrowth::initialize(QWidget *parent)
  {
    // Get the current mesh
    mesh = currentMesh();
    if(!mesh)
      throw(QString("CellDisk::initialize No current mesh"));

    // Initialize the tissue
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDiskGrowth::initialize Cannot make tissue process"));
    tissueProcess->initialize(parent);

    return true;
  }

  bool CellDiskGrowth::step()
  {
    if(!mesh)
      throw(QString("CellDiskGrowth::step No current mesh"));

    if(!tissueProcess)
      throw(QString("CellDiskGrowth::step No tissue process"));

    Point3d rate = stringToPoint3d(parm("Rate"));
    double dt = parm("Dt").toDouble();

    CCStructure &cs = tissueProcess->tissue().cellStructure();
    CCIndexDataAttr &indexAttr = tissueProcess->tissue().indexAttr();

    for(CCIndex v : cs.vertices())
      for(uint i = 0; i < 3; i++)
        indexAttr[v].pos[i] *= 1.0 + dt * rate[i];

    tissueProcess->tissue().updateGeometry();

    mesh->updatePositions(tissueProcess->tissue().tissueName());
    
    return true;
  }

  // Initialize to grab subdivider
  bool CellDiskDivide::initialize(QWidget *parent)
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw(QString("CellDiskDivide::step No current mesh"));

    // Call base initialize
    if(!CellTissueCell2dDivide::initialize(parent))
      return false;

    if(!getProcess(parm("Solver Process"), solverProcess))
      throw(QString("CellDiskDivide::initialize Cannot make Meinhardt solver process"));
    solverProcess->initialize(parent);

    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("CellDisk::initialize Cannot make tissue process"));

    // Setup subdivision objects
    subdiv.mdx = CellTissueCell2dDivide::subdivider();
    subdiv.meinhardt = MeinhardtAI::Subdivide(solverProcess->attr());

    return true;
  }

  // Run a step of cell division
  bool CellDiskDivide::step() 
  { 
    // Pass our subdivide
    return CellTissueCell2dDivide::step(currentMesh(), &subdiv);
  }

  REGISTER_PROCESS(CellDisk);
  REGISTER_PROCESS(CellDiskDivide);
  REGISTER_PROCESS(CellDiskGrowth);
  REGISTER_PROCESS(CellDiskMeinhardt);
  REGISTER_PROCESS(CellDiskTissue);
  REGISTER_PROCESS(CellDiskSolver);
  REGISTER_PROCESS(CellDiskSplitEdges);
}
