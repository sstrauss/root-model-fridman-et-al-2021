# Root Model Fridman et al 2021

To run the model first install MorphoDynamX (MDX). The MDX source can be found in the folder with the same name. Cmake is needed to build and compile MDX. There is also an installation package for Ubuntu 20 attached (MDX-2.0.f451b17-Ubuntu20.04-Cuda10.deb).

Once MDX is installed open the folder 01-UniformGrowth/ext/StrainBasedGrowth in a terminal.
Type "make clean" + ENTER
Type "make run" + ENTER

Now the MDX window should open with the model processes. Open a 8um template in Mesh1 (from the templates folder) and a 100um template in Mesh2. Run the process 01 Cell Disk/Cell Tissue and then open a parameter file using the process 04 Misc/Parm File Load (from the parameters folder). Now run the model using the 01 Cell Disk/01 Main process.
