# Include Add-ons in build
# 
# Each Add-on must have its own directory in AddOns/
# and have a CMakeLists.txt file in that directory
# 
Macro(SubDirList result curdir)
  File(GLOB dirs ${curdir}/*)
  Set(dirlist "")
  ForEach(dir ${dirs})
    If(IS_DIRECTORY ${dir} AND EXISTS ${dir}/CMakeLists.txt)
        List(APPEND dirlist ${dir})
    EndIf()
  EndForEach()
  Set(${result} ${dirlist})
EndMacro(SubDirList)

SubDirList(ADDON_DIRS "${MorphoDynamX_SOURCE_DIR}/AddOns")

ForEach(ADDON ${ADDON_DIRS})
  Message(STATUS "Found Add-on: ${ADDON}")
  Add_Subdirectory(${ADDON})
EndForEach()
