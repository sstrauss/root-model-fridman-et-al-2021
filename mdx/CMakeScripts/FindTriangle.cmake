Find_Path(TRIANGLE_INCLUDE_DIR triangle.h
          DOC "Directory containing triangle.h")

Set(TRIANGLE_NAMES ${TRIANGLE_NAMES} triangle libtriangle)
Find_Library(TRIANGLE_LIBRARY NAMES ${TRIANGLE_NAMES}
             DOC "Name of the triangle library")

Include(FindPackageHandleStandardArgs)
Find_Package_Handle_Standard_Args(TRIANGLE
   Required_Vars TRIANGLE_LIBRARY TRIANGLE_INCLUDE_DIR)

If(TRIANGLE_FOUND)
  Set(TRIANGLE_LIBRARIES ${TRIANGLE_LIBRARY})
  Set(TRIANGLE_INCLUDE_DIRS ${TRIANGLE_INCLUDE_DIR})
EndIf()

Mark_As_Advanced(TRIANGLE_INCLUDE_DIR TRIANGLE_LIBRARY)
