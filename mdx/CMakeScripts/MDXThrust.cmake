Macro(Fake_CUDA outfiles)  # Macro to be used when Thrust backend is not CUDA
  ForEach(CUFILE ${ARGN})
    Get_Filename_Component(CUFILE ${CUFILE} ABSOLUTE)
    Get_Filename_Component(CFILE ${CUFILE} NAME_WE)
    Set(CXXFILE ${CMAKE_CURRENT_BINARY_DIR}/${CFILE}.gen.cxx)
    Configure_File(${CUFILE} ${CXXFILE} COPYONLY)
    Set(${outfiles} ${${outfiles}} ${CXXFILE})
  EndForEach(CUFILE)
EndMacro(Fake_CUDA)

# Shorthand for adding libraries when they use thrust (e.g. by importing a target)
# It will automatically choose between CUDA_Add_Library and Add_Library with special .cu handling
# Usage: Just like CUDA_Add_Library
Function(MDX_Add_Library LIB_TARGET)
  If("${THRUST_DEVICE_SYSTEM}" STREQUAL "CUDA")
    Cuda_Add_Library(${LIB_TARGET} ${ARGN})
    Target_Link_Libraries(${LIB_TARGET} ${CUDA_LIBRARIES} ${CUDA_CUBLAS_LIBRARIES})
  Else()
    Set(NORMAL_ARGS "")
    Set(CUFILES "")
    ForEach(ARG ${ARGN})
      String(TOLOWER "${ARG}" LARG)
      String(REGEX MATCH "\\.cu$" IS_CUDA "${LARG}")
      If(IS_CUDA)
        List(APPEND CUFILES ${ARG})
      Else()
        List(APPEND NORMAL_ARGS ${ARG})
      EndIf()
    EndForEach()
    Fake_CUDA(CXX_CUFILES ${CUFILES})
    Add_Library(${LIB_TARGET} ${NORMAL_ARGS} ${CXX_CUFILES})
  EndIf()
EndFunction()

