Find_Path(TETGEN_INCLUDE_DIR tetgen.h
          DOC "Directory containing tetgen.h")

Set(TETGEN_NAMES ${TETGEN_NAMES} tetgen libtetgen libtet)
Find_Library(TETGEN_LIBRARY NAMES ${TETGEN_NAMES}
             DOC "Name of the TetGen library")

Include(FindPackageHandleStandardArgs)
Find_Package_Handle_Standard_Args(TETGEN
   Required_Vars TETGEN_LIBRARY TETGEN_INCLUDE_DIR)

If(TETGEN_FOUND)
  Set(TETGEN_LIBRARIES ${TETGEN_LIBRARY})
  Set(TETGEN_INCLUDE_DIRS ${TETGEN_INCLUDE_DIR})
EndIf()

Mark_As_Advanced(TETGEN_INCLUDE_DIR TETGEN_LIBRARY)
