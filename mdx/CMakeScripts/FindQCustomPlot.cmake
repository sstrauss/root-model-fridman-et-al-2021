# Taken from the roboticslibrary project rl: https://github.com/roboticslibrary/
# Copyright (c) 2009, Markus Rickert
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Sets the same variables as the Debian bundled version.

include(FindPackageHandleStandardArgs)

find_path(
        QCustomPlot_INCLUDE_DIR
        NAMES
        qcustomplot.h
        HINTS
        /usr/local/include
        /usr/include
)

file(READ "${QCustomPlot_INCLUDE_DIR}/qcustomplot.h" HDR_CONTENTS)

string(REGEX MATCH "Version: [^ ]*" QCustomPlot_VERSION "${HDR_CONTENTS}")

string(REGEX REPLACE
       "Version: \(([0-9]+)\\.([0-9]+)\\.([0-9]+))" "\\1"
       QCustomPlot_VERSION "${QCustomPlot_VERSION}"
)

# For compatibility with Fedora, qcustomplot-qt5 is included
find_library(
        QCustomPlot_LIBRARIES
        NAMES
        qcustomplot-qt5
        qcustomplot
        HINTS
        /usr/local/lib
        /usr/lib
)

find_package_handle_standard_args(
        QCustomPlot
        REQUIRED_VARS
        QCustomPlot_INCLUDE_DIR
        QCustomPlot_LIBRARIES
        VERSION_VAR QCustomPlot_VERSION
)

mark_as_advanced(
        QCustomPlot_INCLUDE_DIR
        QCustomPlot_LIBRARIES
)
