# Find RInside and set variables needed for R linking
#
# Defines:
#  RINSIDELIBS
#  RINSIDEINCL
#  RCPPLIBS
#  RCPPINCL
#  RCPPLIBS_L
#  RLDFLAGS_L
#
# RINSIDE_FOUND
#
find_program(R_PATH R DOC "R executable.")
if(R_COMMAND)
  set(NUM_TRUNC_CHARS 2)

  set (RPATH "R")
  set (RSCRIPT_PATH "Rscript")

  set (RCPPFLAGS_CMD " ${RPATH} " " CMD " " config " " --cppflags ") 

  execute_process(COMMAND ${RPATH} CMD config --cppflags
                  OUTPUT_VARIABLE RCPPFLAGS)

  string(SUBSTRING ${RCPPFLAGS} ${NUM_TRUNC_CHARS} -1 RCPPFLAGS)
  include_directories(${RCPPFLAGS})

  execute_process(COMMAND ${RPATH} CMD config --ldflags
                  OUTPUT_VARIABLE RLDFLAGS)
  string(LENGTH ${RLDFLAGS} RLDFLAGS_LEN)

  if (${RLDFLAGS} MATCHES "[-][L]([^ ;])+")
      string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RLDFLAGS_L)
      string(STRIP ${RLDFLAGS_L} RLDFLAGS_L )
      link_directories(${RLDFLAGS_L} )
  endif()

  if (${RLDFLAGS} MATCHES "[-][l]([^;])+")
      string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RLDFLAGS_l)
      string(STRIP ${RLDFLAGS_l} RLDFLAGS_l )
  endif()

  execute_process(COMMAND ${RSCRIPT_PATH} -e "Rcpp:::CxxFlags()"
                  OUTPUT_VARIABLE RCPPINCL)
  string(SUBSTRING ${RCPPINCL} ${NUM_TRUNC_CHARS} -1 RCPPINCL)
  include_directories(${RCPPINCL})

  execute_process(COMMAND ${RSCRIPT_PATH} -e "Rcpp:::LdFlags()"
                  OUTPUT_VARIABLE RCPPLIBS)


  execute_process(COMMAND ${RSCRIPT_PATH} -e "RInside:::CxxFlags()"
                  OUTPUT_VARIABLE RINSIDEINCL)
  string(SUBSTRING ${RINSIDEINCL} ${NUM_TRUNC_CHARS} -1 RINSIDEINCL)
  include_directories(${RINSIDEINCL})

  message( ${RINSIDEINCL} )

  execute_process(COMMAND ${RSCRIPT_PATH} -e "RInside:::LdFlags()"
                  OUTPUT_VARIABLE RINSIDELIBS)

  if (${RCPPLIBS} MATCHES "[-][L]([^ ;])+")
      string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RCPPLIBS_L)
      link_directories(${RCPPLIBS_L} )
  endif()

  if (${RCPPLIBS} MATCHES "[-][l][R]([^;])+")
      string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RCPPLIBS_L)
  endif()

  if (${RINSIDELIBS} MATCHES "[-][L]([^ ;])+")
      string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RINSIDELIBS_L)
      link_directories(${RINSIDELIBS_L})
  endif()

  if (${RINSIDELIBS} MATCHES "[-][l][R]([^;])+")
      string(SUBSTRING ${CMAKE_MATCH_0} ${NUM_TRUNC_CHARS} -1 RINSIDELIBS_L)
  endif()

  message( ${RINSIDELIBS} )

  execute_process(COMMAND ${RPATH} CMD config CXXFLAGS
                  OUTPUT_VARIABLE RCXXFLAGS)

  execute_process(COMMAND ${RPATH} CMD config BLAS_LIBS
                  OUTPUT_VARIABLE RBLAS)

  execute_process(COMMAND ${RPATH} CMD config LAPACK_LIBS
                  OUTPUT_VARIABLE RLAPACK)

  set(CMAKE_CXX_FLAGS "-W -Wall -pedantic -Wextra ${CMAKE_CXX_FLAGS}")

  add_definitions("-O3")

  message(STATUS "RCPPLIBS_L:" ${RCPPLIBS_L})
  message(STATUS "RLDFLAGS_L:" ${RLDFLAGS_L})
  message(STATUS "RINSIDELIBS:" ${RINSIDELIBS})
  message(STATUS "RINSIDEINCL:" ${RINSIDEINCL})
  message(STATUS "RCPPINCL:" ${RCPPINCL})
  message(STATUS "RCPPLIBS:" ${RCPPLIBS})


  if(RINSIDEINCL)
    if(RCPPINCL)
      set(RINSIDE_FOUND TRUE)
    else()
      message(SEND_ERROR, "Package RInside not found")
    endif()
  else()
    message(SEND_ERROR, "Package Rcpp not found")
  endif()
else()
  message(SEND_ERROR "FindRInside.cmake cannot find R")
endif()


