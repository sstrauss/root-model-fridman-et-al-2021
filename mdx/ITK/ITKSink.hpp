//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ITKSINK_HPP
#define ITKSINK_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

namespace mdx
{
  class mdxITK_EXPORT ITKVTKWriter : public Process 
  {
  public:
    ITKVTKWriter(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/System/ITK VTK Writer");
      setDesc("Write a VTK Image File");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "File name to write image", "");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack")); 
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack")); 
      bool res = run(input, parm("File Name"));
      return res;
    }
  
    bool run(const Store* input, QString filename);
  };
}

#endif
