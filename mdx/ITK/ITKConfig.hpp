//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ITKCONFIG_HPP
#define ITKCONFIG_HPP

#if defined(WIN32) || defined(WIN64)
  #ifdef mdxITKutil_EXPORTS
    #define mdxITKutil_EXPORT __declspec(dllexport)
  #else
    #define mdxITKutil_EXPORT __declspec(dllimport)
  #endif
  #ifdef mdxITK_EXPORTS
    #define mdxITK_EXPORT __declspec(dllexport)
  #else
    #define mdxITK_EXPORT __declspec(dllimport)
  #endif
#else
  #define mdxITK_EXPORT
  #define mdxITKutil_EXPORT
#endif

#endif
