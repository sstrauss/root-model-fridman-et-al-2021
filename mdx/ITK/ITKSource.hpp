//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ITKSOURCE_HPP
#define ITKSOURCE_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>
#include <ui_ImportStack.h>
#include <itkImageFileReader.h>
#include <itkSCIFIOImageIO.h>

namespace mdx
{
  typedef itk::Image<unsigned short int, 5> U5ImageType;
  typedef itk::ImageFileReader<U5ImageType> ReaderType;
  
  class mdxITK_EXPORT ITKImageReader : public Process 
  {
    Q_OBJECT
  public:
    ITKImageReader(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/System/ITK Image Reader");
      setDesc("Read an image with ITK and SCIFIO (Loci Tools)");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "File Name", "");
      addParm("Store", "Store", "Main", storeChoice());
      addParm("Stack Number", "Stack Number", "0");
    }
    bool initialize(QWidget* parent);
  
    bool run();
    bool run(Stack* stack, Store* store, QString filename);
  
  protected:
    void LoadSeries(bool next);
  
  protected slots:
    void NextSeriesSlot() { LoadSeries(true); };
    void PrevSeriesSlot() { LoadSeries(false); };
  
  protected:
    QString fileName;
    Point5i size;
    Point5f step;
  
    QString pixelType;
    int componentSize;
    int components;
    int seriesCount;
  
    int selectSeries;
    int selectComponent;
    int selectChannel;
    int selectTimePoint;
  
    Ui_ImportStackDialog* ui;
    itk::SCIFIOImageIO::Pointer imageIO;
  };
}

#endif
