//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "ITKSink.hpp"
#include <itkVTKImageIO.h>

namespace mdx
{
  bool ITKVTKWriter::run(const Store* input, QString filename)
  {
    UImageConverter::Pointer converter = UImageConverter::New();
  
    converter->SetStore(input);
  
    converter->Update();
  
    UImageType::Pointer img = converter->GetOutput();
  
    typedef itk::ImageFileWriter<UImageType> WriterType;
  
    WriterType::Pointer filter = WriterType::New();
  
    QByteArray ba = filename.toLocal8Bit();
    filter->SetFileName(ba.data());
  
    itk::VTKImageIO::Pointer ImageIO = itk::VTKImageIO::New();
  
    filter->SetImageIO(ImageIO);
  
    filter->SetInput(img);
  
    filter->Write();
  
    return true;
  }
  
  REGISTER_PROCESS(ITKVTKWriter);
}
