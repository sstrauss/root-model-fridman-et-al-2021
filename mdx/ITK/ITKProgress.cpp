//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "ITKProgress.hpp"
#include "Process.hpp"

namespace mdx 
{
  ITKProgress::ITKProgress(const QString& text) : eventTag(0)
  {
    progressStart(text, 100, false);
    eventHandler = Receptor::New();
    eventHandler->SetCallbackFunction(this, &ITKProgress::advance);
  }
  
  void ITKProgress::setFilter(itk::ProcessObject::Pointer f)
  {
    if(filter.IsNotNull())
      eventHandler->RemoveAllObservers();
    filter = f;
    if(filter.IsNotNull())
      eventTag = filter->AddObserver(itk::ProgressEvent(), eventHandler);
  }
  
  ITKProgress::~ITKProgress() 
  {
    if(filter.IsNotNull())
      eventHandler->RemoveAllObservers();
  }
  
  void ITKProgress::advance(const itk::EventObject& event)
  {
    if(progressEvent.CheckEvent(&event))
      if(!progressAdvance(int(100 * filter->GetProgress())))
        ; // RSS what can we do here?
  }
}
