//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ITKREGISTRATION_HPP
#define ITKREGISTRATION_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

namespace mdx
{
  
  class mdxITK_EXPORT ITKDemonsRegistration : public Process 
	{
  public:
    ITKDemonsRegistration(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/Registration/ITK Demons Registration");
      setDesc("Register the current image with the current one on the other stack");
      setIcon(QIcon(":/images/Blur.png"));
  
      addParm("Deformation File", "Deformation File", "deformation.vtk");
      addParm("Threshold at Mean", "Threshold at mean intensity", "Yes", booleanChoice());
      addParm("Histogram Levels", "Num Histogram levels", "1024");
      addParm("Match Points", "Num Match points", "7");
      addParm("Iterations", "Num Iterations", "150");
      addParm("Sigma", "Sigma", "1.0");
    }
  
    bool run()
    {
      Stack* stk = currentStack();
      Stack* ref_stk = getStack(stk->id() == 0 ? 1 : 0);
      Store* ref = ref_stk->currentStore();
      return run(ref, stk, parm("Deformation File"), stringToBool(parm("Threshold at Mean")), parm("Histogram Levels").toInt(), 
         parm("MatchPoints").toInt(), parm("Iterations").toInt(), parm("Sigma").toFloat());
    }
  
    bool run(const Store* reference, Stack* image, QString output, bool thresholdAtMean, 
                        int nbHistLevels, int nbMatchPoints, int nbIterations, float sigma);
  };
}

#endif
