//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ITKFILTER_HPP
#define ITKFILTER_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

namespace mdx 
{
  class mdxITK_EXPORT ITKSmoothingRecursiveGaussianImageFilter : public Process 
  {
  public:
    ITKSmoothingRecursiveGaussianImageFilter(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/Filters/ITK Smoothing Recursive Gaussian Image Filter");
      setDesc("Smoothing Recursive Gaussian Blur");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Radius", "Radius in microns", "1.0");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack"));
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));

      bool res = run(input, output, parm("Radius").toFloat());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
  
    bool run(const Store* input, Store* output, float radius);
  };
  
  class ITKCurvatureFlowImageFilter : public Process 
  {
  public:
    ITKCurvatureFlowImageFilter(const Process& process) : Process(process) 
    {
		  setName("Stack/ITK/Filters/ITK Curvature Flow Image Filter");
		  setDesc("Curvature Flow Edge Preserving Smoothing");
		  setIcon(QIcon(":/images/Blur.png"));

		  addParm("Time Step", "Time step for flow", ".0.0625");
      addParm("Steps", "Number of steps to flow", "10");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack"));
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));

      bool res = run(input, output, parm("Time Step").toFloat(), parm("Steps").toInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
  
    bool run(const Store* input, Store* output, float timestep, int steps);
  };
  
  class ITKCurvatureAnisotropicDiffusionImageFilter : public Process {
  public:
    ITKCurvatureAnisotropicDiffusionImageFilter(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/Filters/ITK Curvature Anisotropic Diffusion Image Filter");
      setDesc("Curvature Anisotropic Diffusion");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Time Step", "Time step for diffusion", "0.03");
      addParm("Iterations", "Numbers of steps of diffusion", "5");
      addParm("Conductance", "Conductance parameter", "9.0");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack"));
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));

      bool res = run(input, output, parm("Time Step").toFloat(), parm("Iterations").toInt(), parm("Conductance").toFloat());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
  
    bool run(const Store* input, Store* output, float timestep, int iterations, float conductance);
  };
  
  class ITKGradientMagnitudeRecursiveGaussianImageFilter : public Process {
  public:
    ITKGradientMagnitudeRecursiveGaussianImageFilter(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/Filters/ITK Gradient Magnitude Recursive Gaussian Image Filter");
      setDesc("Gradient Magnitude Recursive Gaussian");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Sigma", "Sigma in microns", "0.5");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack"));
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));

      bool res = run(input, output, parm("Sigma").toFloat());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
  
    bool run(const Store* input, Store* output, float sigma);
  };
  
  class ITKSigmoidImageFilter : public Process {
  public:
    ITKSigmoidImageFilter(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/Filters/ITK Sigmoid Image Filter");
      setDesc("Sigmoid Filter");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Alpha", "Alpha", "1000.0");
      addParm("Beta", "Beta", "2000.0");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack"));
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));;

      bool res = run(input, output, parm("Alpha").toFloat(), parm("Beta").toFloat());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
  
    bool run(const Store* input, Store* output, float alpha, float beta);
  };
}

#endif
