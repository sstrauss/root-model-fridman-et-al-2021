//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef EDGE_DETECT_ANGLE_HPP
#define EDGE_DETECT_ANGLE_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

#include <StackProcessMorphology.hpp>

namespace mdx
{
  mdxITK_EXPORT class EdgeDetectAngle : public Process {
  public:
    EdgeDetectAngle(const Process& process) : Process(process) 
    {
      setName("Stack/Morphology/Edge Detect Angle");
      setDesc("Do another pass of edge detection with the stack rotated to a new Z direction\n"
              "This process requires a standard Edge Detect result in the work stacki and uses\n"
              "the parameters from that process");
      setIcon(QIcon(":/images/EdgeDetectAngle.png"));

      addParm("Fill Holes", "Also run Fill Holes Process", "No", booleanChoice());
    }

    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack"));
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));
      
      bool res = run(stack, input, output, stringToBool(parm("Fill Holes")));
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(Stack* s1, Store* input, Store* output, bool fillHoles);
  };
}
#endif
