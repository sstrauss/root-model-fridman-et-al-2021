//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ITKSEGMENTATION_HPP
#define ITKSEGMENTATION_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

namespace mdx 
{
  class mdxITK_EXPORT ITKWatershed : public Process {
  public:
    ITKWatershed(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/Segmentation/ITK Watershed");
      setDesc("Seeded watershed segmentation, image in main store, labels in work");
      setIcon(QIcon(":/images/SegmentMesh.png"));

      addParm("Fully Connected", "Fully connected", "No", booleanChoice());
      addParm("Mark Watershed Line", "Mark Watershed Line", "No", booleanChoice());
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->main();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));

      bool connect8 = stringToBool(parm("Fully Connected"));
      bool markLine = stringToBool(parm("Mark Watershed Line"));
      return run(input, output, connect8, markLine);
    }
  
    bool run(const Store* image, Store* labels, bool connect8, bool markLine);
  };
  
  class mdxITK_EXPORT ITKWatershedAutoSeeded : public Process 
  {
  public:
    ITKWatershedAutoSeeded(const Process& process) : Process(process) 
    {
      setName("Stack/ITK/Segmentation/ITK Watershed Auto Seeded");
      setDesc("Auto Seeded Watershed segmentation");
      setIcon(QIcon(":/images/SegmentMesh.png"));

      addParm("Fully Connected", "Fully connected", "No", booleanChoice());
      addParm("Mark Watershed Line", "Mark Watershed Line", "No", booleanChoice());
      addParm("Level", "Threshold level for region merging", "1500");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw(QString("Invalid stack"));
      if(stack->empty())
        throw(QString("Stack empty"));
      Store* input = stack->currentStore();
      if(!input)
        throw(QString("Invalid input stack"));
      Store* output = stack->work();
      if(!output)
        throw(QString("Invalid output stack"));

      bool connect8 = stringToBool(parm("Fully Connected"));
      bool markLine = stringToBool(parm("Mark Watershed Line"));
      ushort level = parm("Level").toUInt();

      if(run(stack, input, output, level, connect8, markLine)) {
        input->hide();
        output->show();
        return true;
      }
      return false;
    }
  
    bool run(Stack* stack, const Store* image, Store* labels, ushort level, 
                                                               bool connect8, bool markLine);
  };
}

#endif
