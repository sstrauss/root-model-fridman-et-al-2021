//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ITKPROGRESS_HPP
#define ITKPROGRESS_HPP

#include <ITKConfig.hpp>

#include <Progress.hpp>
#include <itkCommand.h>
#include <itkProcessObject.h>
#include <itkEventObject.h>

namespace mdx 
{
  class mdxITKutil_EXPORT ITKProgress 
  {
  public:
    ITKProgress(const QString& text);
    ~ITKProgress();
  
    void setFilter(itk::ProcessObject::Pointer filter);
  
    template <typename FilterPointer> void setFilter(const FilterPointer& filter)
    {
      setFilter(itk::ProcessObject::Pointer(filter));
    }
  
    void advance(const itk::EventObject& event);
  
    typedef itk::ReceptorMemberCommand<ITKProgress> Receptor;
    Receptor::Pointer eventHandler;
  
    unsigned long eventTag;
    itk::ProcessObject::Pointer filter;
    itk::ProgressEvent progressEvent;
  };
}

#endif
