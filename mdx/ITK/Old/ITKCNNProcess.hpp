//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef ITK_CNN_Process_HPP
#define ITK_CNN_Process_HPP

#include <ITKProcess.hpp>

namespace mdx
{
  ///\addtogroup Process
  ///@{
  /**
   * \class UNet3DPredict CNNProcess.hpp <CNNProcess.hpp>
   *
   * Use CNN cell wall predictions to (hopefully) improve the stack
   */
  class ITK_EXPORT UNet3DPredict2 : public Process 
  {
  public:
    UNet3DPredict2(const Process &process) : Process(process) 
    {
      setName("Stack/CNN/UNet3D Prediction2");
      setDesc("Improve stack with CNN cell wall predictions.\n"
              "This process is uses the tools developed in Eschweiler et al.\n"
              "2019 IEEE 16th International Symposium on Biomedical Imaging (ISBI 2019)\n");
      setIcon(QIcon(":/images/CNN.png"));
      
      addParm("Model Path","Path to network model", "/home/rismith/Desktop/MDX/mdx_private/ITK/3ClassUNet.pt");						
      addParm("Patch Size","Maximum patch size for processing", "64 64 64");						
      addParm("Use Cuda","Use cuda if available", "Yes", booleanChoice());						
	  }
  
    bool run()
    {
      Stack* stack = currentStack();
      Store* input = stack->currentStore();
      Store* output = stack->work();
      bool result = return run(stack, input, output, parm("Model Path"), stringToPoint3u(parm("Patch Size")), stringToBool(parm("Use Cuda")));
      if(result)
        output->show(); {
        output->setLabels(false);
      }
      return result;
    }
  
    bool run(Stack *stack, Store* input, Store *output, QString modelPath, Point3u patchSize, bool useCuda = true);
  };

  ///@}
}
#endif
