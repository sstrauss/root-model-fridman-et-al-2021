//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2021 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <ITKCNNProcess.hpp>
#include <StackProcessSystem.hpp>
#include <StackProcessCanvas.hpp>
#include <cuda/CudaExport.hpp>

#undef slots
#include <torch/script.h>
#include <ATen/cuda/CUDAUtils.h>
#define slots Q_SLOTS
#define USE_CUDA
namespace mdx 
{
  // Class to manage memory hold for cuda, need to release it before calling CNN, hopefully we get some back after.
  struct HoldMem
  {
    HoldMem() { freeMem(); }
    ~HoldMem() { holdMem(); }
  };


  bool UNet3DPredict2::run(Stack *stack, Store* input, Store* output, QString modelPath, Point3u patchSz, bool useCuda)
  {
    HoldMem hm;

    auto sz = stack->size();

    // get the parameter values
    const int patchWidth = min(sz.x(), patchSz.x()); // 128; //processObjectSettings->GetSettingValue( "PatchWidth" ).toInt();
    const int patchHeight = min(sz.y(), patchSz.y()); // 128; //processObjectSettings->GetSettingValue( "PatchHeight" ).toInt();
    const int patchDepth = min(sz.z(), patchSz.z()); // 64; // processObjectSettings->GetSettingValue( "PatchDepth" ).toInt();
    const int numInputChannels = 1; // processObjectSettings->GetSettingValue( "NumInputChannels" ).toInt();
    const float patchStrideParm = .5; // processObjectSettings->GetSettingValue( "PatchStride" ).toFloat();

    // load the torch model
    //torch::jit::script::Module torchModel;

    // deserialize the ScriptModule from the provided model file
    mdxInfo << endl << endl;
    if(!at::cuda::is_available() and useCuda) {
      useCuda = false;
      mdxInfo << "Cuda is not available, using CPU" << endl;
    }
    torch::jit::script::Module torchModel = torch::jit::load(modelPath.toStdString().c_str(), useCuda ? at::kCUDA : at::kCPU);

    // Get the images
    FImageConverter::Pointer converter1 = FImageConverter::New();
    FImageConverter::Pointer converter2 = FImageConverter::New();
    FImageConverter::Pointer converter3 = FImageConverter::New();
    FImageConverter::Pointer converter4 = FImageConverter::New();
    converter1->SetStore(input);
    converter2->SetStore(input);
    converter3->SetStore(input);
    converter4->SetStore(input);
    converter1->Update();
    converter2->Update();
    converter3->Update();
    converter4->Update();
    FImageType::Pointer inputImage1 = converter1->GetOutput();
    FImageType::Pointer inputImage2 = converter2->GetOutput();
    FImageType::Pointer inputImage3 = converter3->GetOutput();
    FImageType::Pointer inputImage4 = converter4->GetOutput();
 mdxDebug;  
  //	  typename FImageType::Pointer inputImage1 = mInputImages.at(0)->template GetImage<FImageType>();
  //    typename FImageType::Pointer inputImage2 = mInputImages.at(1)->template GetImage<FImageType>();
  //    typename FImageType::Pointer inputImage3 = mInputImages.at(2)->template GetImage<FImageType>();
  //    typename FImageType::Pointer inputImage4 = mInputImages.at(3)->template GetImage<FImageType>();
  
    // perform tile-based processing
    int imageDimension = FImageType::ImageDimension;
    typename FImageType::RegionType inputRegion = inputImage1->GetLargestPossibleRegion();
    typename FImageType::SpacingType inputSpacing = inputImage1->GetSpacing();
    typename FImageType::SizeType imageSize = inputRegion.GetSize();
    typename FImageType::SizeType patchSize;
    typename FImageType::SizeType patchStride;
    typename FImageType::SizeType numSteps;

    // set the patch size used for processing
    patchSize[0] = patchWidth;
    patchSize[1] = patchHeight;
    if (imageDimension > 2)
        patchSize[2] = patchDepth;
        
 mdxDebug;  
    // compute the strides and number of steps used for processing
    int numPatchVoxels = 1;
    for (int i=0; i<imageDimension; ++i)
    {
        numPatchVoxels *= imageSize[i];
        patchStride[i] = (patchStrideParm >= 1) ? patchStrideParm : int(patchStrideParm * (float)patchSize[i]);
        numSteps[i] = floor((float)imageSize[i] / (float)patchStride[i]);
        
        if (patchSize[i] == imageSize[i])
            numSteps[i] = 1;
    }
    
    // print processing information for debugging
    std::cout << "Image Dimension: " << imageDimension << std::endl;
    if (imageDimension > 2)
    {
        std::cout << "Image Size: [" << imageSize[0] << ", " << imageSize[1] << ", " << imageSize[2] << "]" << std::endl;
        std::cout << "Patch Size: [" << patchSize[0] << ", " << patchSize[1] << ", " << patchSize[2] << "]" << std::endl;
        std::cout << "Patch Stride: [" << patchStride[0] << ", " << patchStride[1] << ", " << patchStride[2] << "]" << std::endl;
        std::cout << "Num Steps: [" << numSteps[0] << ", " << numSteps[1] << ", " << numSteps[2] << "]" << std::endl;
    }
    else
    {
        std::cout << "Image Size: [" << imageSize[0] << ", " << imageSize[1] << "]" << std::endl;
        std::cout << "Patch Size: [" << patchSize[0] << ", " << patchSize[1] << "]" << std::endl;
        std::cout << "Patch Stride: [" << patchStride[0] << ", " << patchStride[1] << "]" << std::endl;
        std::cout << "Num Steps: [" << numSteps[0] << ", " << numSteps[1] << "]" << std::endl;
    }
   
    // initialize the patch region as well as the patch center for distance computations
    typename FImageType::RegionType patchRegion;
    vnl_vector<float> patchCenter(imageDimension);
    for (int i=0; i<imageDimension; ++i)
    {
        patchRegion.SetSize(i, patchSize[i]);
        patchRegion.SetIndex(i, 0);
        patchCenter[i] = (float)(patchSize[i]/2.0f);
    }
    float patchCenterDistance = patchCenter.two_norm();
            
    // create output and weight images
    typename FImageType::Pointer patchWeightMap = FImageType::New();
    typename FImageType::Pointer outputImage1 = FImageType::New();
    typename FImageType::Pointer outputImage2 = FImageType::New();
    typename FImageType::Pointer outputImage3 = FImageType::New();
    typename FImageType::Pointer outputImage4 = FImageType::New();
    typename FImageType::Pointer weightImage = FImageType::New();
    outputImage1->SetRegions( inputRegion );
    outputImage1->SetSpacing( inputSpacing );
    outputImage1->Allocate();
    outputImage1->FillBuffer( 0 );
    outputImage2->SetRegions( inputRegion );
    outputImage2->SetSpacing( inputSpacing );
    outputImage2->Allocate();
    outputImage2->FillBuffer( 0 );
    outputImage3->SetRegions( inputRegion );
    outputImage3->SetSpacing( inputSpacing );
    outputImage3->Allocate();
    outputImage3->FillBuffer( 0 );
    outputImage4->SetRegions( inputRegion );
    outputImage4->SetSpacing( inputSpacing );
    outputImage4->Allocate();
    outputImage4->FillBuffer( 0 );
    weightImage->SetRegions( inputRegion );
    weightImage->SetSpacing( inputSpacing );
    weightImage->Allocate();
    weightImage->FillBuffer( 0 );
    patchWeightMap->SetRegions( patchRegion );
    patchWeightMap->SetSpacing( inputSpacing );
    patchWeightMap->Allocate();
    patchWeightMap->FillBuffer( 0 );

 mdxDebug;  
    // initialize the temporary patch
    typename FImageType::IndexType currentIndex;
    typename FImageType::SizeType currentSize;
    typename FImageType::RegionType currentRegion;
    for (int i=0; i<imageDimension; ++i)
    {
        currentRegion.SetSize(i, patchSize[i]);
        currentRegion.SetIndex(i, 0);
    }
    
    // generate the weight map
    itk::ImageRegionIterator<FImageType> patchWeightMapIterator( patchWeightMap, patchRegion );
    patchWeightMapIterator.GoToBegin();
    vnl_vector<float> currentPosition(imageDimension);
    while (patchWeightMapIterator.IsAtEnd() == false)
    {
        vnl_vector<float> currentPosition(imageDimension);
        for (int i=0; i<imageDimension; ++i)
            currentPosition[i] = (float)patchWeightMapIterator.GetIndex()[i];
         
        patchWeightMapIterator.Set((patchCenterDistance - (currentPosition - patchCenter).two_norm()) / patchCenterDistance);
        ++patchWeightMapIterator;
    }
    
    // initialize counters and temporary patch
    int counter = 0;
    int numStepsZ = (imageDimension > 2) ? std::max<int>(1, numSteps[2]) : 1;
    float* currentPatch = new float[numInputChannels * numPatchVoxels];
    
 mdxDebug;  
    // process all patches separately
    for (int i=0; i<numSteps[0]; ++i)
    {
        currentRegion.SetIndex(0, std::min<int>(imageSize[0]-patchSize[0], i*patchStride[0]));
        
        for (int j=0; j<numSteps[1]; ++j)
        {
            currentRegion.SetIndex(1, std::min<int>(imageSize[1]-patchSize[1], j*patchStride[1]));
            
            for (int k=0; k<numStepsZ; ++k)
            {
                if (imageDimension > 2)
                    currentRegion.SetIndex(2, std::min<int>(imageSize[2]-patchSize[2], k*patchStride[2]));
                
                // print information about the current patch to be processed
                if (imageDimension > 2)
                {
                    std::cout << "Processing region with index [" << currentRegion.GetIndex(0) << ", "  << currentRegion.GetIndex(1) << ", "  << currentRegion.GetIndex(2) << "]";
                    std::cout << " and size [" << currentRegion.GetSize(0) << ", "  << currentRegion.GetSize(1) << ", "  << currentRegion.GetSize(2) << "]" << std::endl;
                }
                else
                {
                    std::cout << "Processing region with index [" << currentRegion.GetIndex(0) << ", "  << currentRegion.GetIndex(1) << "]";
                    std::cout << " and size [" << currentRegion.GetSize(0) << ", "  << currentRegion.GetSize(1) << "]" << std::endl;
                }
                                
                // extract information from the inputs at the current patch location
                itk::ImageRegionIterator<FImageType> inputIterator1( inputImage1, currentRegion );
                itk::ImageRegionIterator<FImageType> inputIterator2( inputImage2, currentRegion );
                itk::ImageRegionIterator<FImageType> inputIterator3( inputImage3, currentRegion );
                itk::ImageRegionIterator<FImageType> inputIterator4( inputImage4, currentRegion );
                inputIterator1.GoToBegin();
                inputIterator2.GoToBegin();
                inputIterator3.GoToBegin();
                inputIterator4.GoToBegin();

mdxDebug;  
                // initialize temporary input tensor
//                torch::Tensor inputTensor;
//                if (imageDimension > 2)
//                    inputTensor = torch::zeros({1, numInputChannels, patchDepth, patchHeight, patchWidth});
//                else
//                    inputTensor = torch::zeros({1, numInputChannels, patchHeight, patchWidth});
                
                // fill the current patch input
                counter = 0;
                while (inputIterator1.IsAtEnd() == false)
                {
                    currentPatch[counter] = inputIterator1.Value();
                    
                    if (numInputChannels > 1)
                        currentPatch[counter+(numPatchVoxels)] = inputIterator2.Value();
                    
                    if (numInputChannels > 2)
                        currentPatch[counter+(2*numPatchVoxels)] = inputIterator3.Value();
                    
                    if (numInputChannels > 3)
                        currentPatch[counter+(3*numPatchVoxels)] = inputIterator4.Value();
                    
                    // increment iterators
                    ++counter;
                    ++inputIterator1;
                    ++inputIterator2;
                    ++inputIterator3;
                    ++inputIterator4;
                }

                // convert the float array to a torch tensor
                torch::Tensor tensorImage;
                if (imageDimension > 2)
                    tensorImage = torch::from_blob(currentPatch, {1, numInputChannels, patchDepth, patchHeight, patchWidth}, torch::kFloat).clone();
                else
                    tensorImage = torch::from_blob(currentPatch, {1, numInputChannels, patchHeight, patchWidth}, torch::kFloat).clone();
                
                tensorImage = tensorImage.toType(torch::kFloat);
                tensorImage.set_requires_grad(0);
                               
                // Create a vector of inputs.
                std::vector<torch::jit::IValue> inputs;
mdxDebug;  
                inputs.push_back(tensorImage);
mdxDebug;  
                // Execute the model and turn its output into a tensor.
                torch::jit::IValue networkPrediction;
                torch::Tensor outputTensor;
                std::vector<torch::jit::IValue> outputTuple;
                                
                int numOutputChannels = 0;
                try
                {
mdxDebug;  
                    if(useCuda) {
                      cudaThreadSynchronize();
                      networkPrediction = torchModel.forward({tensorImage.to(at::kCUDA)});
                      cudaThreadSynchronize();
                    } else
                      networkPrediction = torchModel.forward({tensorImage});
mdxDebug;  
                                        
                    if (networkPrediction.isTuple() == false)
                    {
                        outputTensor = networkPrediction.toTensor();
                        numOutputChannels = outputTensor.size(1);
                        
                        // print result tensor size
                        std::cout << "Result tensor dtype: " << outputTensor.dtype() << ", size: " << outputTensor.sizes() << std::endl;
                    }
                    else
                    {
                        outputTuple = networkPrediction.toTuple()->elements();
                        numOutputChannels = outputTuple.size();
                        outputTensor = outputTuple[0].toTensor();
                        
                        std::cout << "Processing tuple output with " << numOutputChannels << " tuple elements." 
                                  << outputTensor.dtype() << ", size: " << outputTensor.sizes() << std::endl;
                    }
                }
                catch (const c10::Error& e)
                {
                    std::cout << e.what() << std::endl;
                }
                

                // convert tensor output back to a float array
                outputTensor = outputTensor.toType(torch::kFloat);
                float* resultArray = outputTensor.data_ptr<float>();
                float currentValue = 0.0;
                
                // create output iterators
                itk::ImageRegionIterator<FImageType> outputIterator1( outputImage1, currentRegion );
                itk::ImageRegionIterator<FImageType> outputIterator2( outputImage2, currentRegion );
                itk::ImageRegionIterator<FImageType> outputIterator3( outputImage3, currentRegion );
                itk::ImageRegionIterator<FImageType> outputIterator4( outputImage4, currentRegion );
                itk::ImageRegionIterator<FImageType> weightIterator( weightImage, currentRegion );

                // reset output iterators to beginning
                outputIterator1.GoToBegin();
                weightIterator.GoToBegin();
                patchWeightMapIterator.GoToBegin();
                
                // fill the output region of the current patch for the first channel
                while (outputIterator1.IsAtEnd() == false)
                {
                    // get the current value and set it to the respective output position
                    currentValue = *resultArray++;
                    outputIterator1.Set(outputIterator1.Value() + patchWeightMapIterator.Value() * currentValue);
                   
                    // increment the weight iterator. As weights for all channels are identical, this only has to be done once.
                    weightIterator.Set(weightIterator.Value() + patchWeightMapIterator.Value());
                    
                    // increment the iterators
                    ++outputIterator1;
                    ++weightIterator;
                    ++patchWeightMapIterator;
                }
                
                // fill the output region of the current patch for the second channel
                if (numOutputChannels > 1)
                {
                    if (networkPrediction.isTuple())
                    {
                        outputTensor = outputTuple[1].toTensor();
                        outputTensor = outputTensor.toType(torch::kFloat);
                        resultArray = outputTensor.data_ptr<float>();
                    }
                    
                    // reset iterators to beginning
                    outputIterator2.GoToBegin();
                    patchWeightMapIterator.GoToBegin();
                    
                    while (outputIterator2.IsAtEnd() == false)
                    {
                        // get the current value and set it to the respective output position
                        currentValue = *resultArray++;
                        outputIterator2.Set(outputIterator2.Value() + patchWeightMapIterator.Value() * currentValue);

                        // increment the iterators
                        ++outputIterator2;
                        ++patchWeightMapIterator;
                    }
                }
                
                // fill the output region of the current patch for the third channel
                if (numOutputChannels > 2)
                {
                    if (networkPrediction.isTuple())
                    {
                        outputTensor = outputTuple[2].toTensor();
                        outputTensor = outputTensor.toType(torch::kFloat);
                        resultArray = outputTensor.data_ptr<float>();
                    }
                    
                    // reset iterators to beginning
                    outputIterator3.GoToBegin();
                    patchWeightMapIterator.GoToBegin();
                    
                    while (outputIterator3.IsAtEnd() == false)
                    {
                        // get the current value and set it to the respective output position
                        currentValue = *resultArray++;
                        outputIterator3.Set(outputIterator3.Value() + patchWeightMapIterator.Value() * currentValue);
                       
                        // increment the iterators
                        ++outputIterator3;
                        ++patchWeightMapIterator;
                    }
                }
                
                // fill the output region of the current patch for the third channel
                if (numOutputChannels > 3)
                {
                    if (networkPrediction.isTuple())
                    {
                        outputTuple = networkPrediction.toTuple()->elements();
                        numOutputChannels = outputTuple.size();
                        outputTensor = outputTuple[3].toTensor();
                        outputTensor = outputTensor.toType(torch::kFloat);
                        resultArray = outputTensor.data_ptr<float>();
                    }
                    
                    // reset iterators to beginning
                    outputIterator4.GoToBegin();
                    patchWeightMapIterator.GoToBegin();
                    
                    while (outputIterator4.IsAtEnd() == false)
                    {
                        // get the current value and set it to the respective output position
                        currentValue = *resultArray++;
                        outputIterator4.Set(outputIterator4.Value() + patchWeightMapIterator.Value() * currentValue);
                       
                        // increment the iterators
                        ++outputIterator4;
                        ++patchWeightMapIterator;
                    }
                }
            }
        }
    }
    
    // remove temporary input image
    delete[] currentPatch;
  
 mdxDebug;  
    // perform weight normalization for correct outputs
    itk::ImageRegionIterator<FImageType> outputIterator1( outputImage1, inputRegion );
    itk::ImageRegionIterator<FImageType> outputIterator2( outputImage2, inputRegion );
    itk::ImageRegionIterator<FImageType> outputIterator3( outputImage3, inputRegion );
    itk::ImageRegionIterator<FImageType> outputIterator4( outputImage4, inputRegion );
    itk::ImageRegionIterator<FImageType> weightIterator( weightImage, inputRegion );
    outputIterator1.GoToBegin();
    outputIterator2.GoToBegin();
    outputIterator3.GoToBegin();
    outputIterator4.GoToBegin();
    weightIterator.GoToBegin();

    while (outputIterator1.IsAtEnd() == false)
    {
        // normalize pixels with the associated weights
        outputIterator1.Set(outputIterator1.Value() / weightIterator.Value());
        outputIterator2.Set(outputIterator2.Value() / weightIterator.Value());
        outputIterator3.Set(outputIterator3.Value() / weightIterator.Value());
        outputIterator4.Set(outputIterator4.Value() / weightIterator.Value());
     
        // increment iterators
        ++outputIterator1;
        ++outputIterator2;
        ++outputIterator3;
        ++outputIterator4;
        ++weightIterator;
    }
  
 mdxDebug;  
  // set the outputs
//	  ImageWrapper* outputImageWrapper1 = new ImageWrapper();
//    ImageWrapper* outputImageWrapper2 = new ImageWrapper();
//    ImageWrapper* outputImageWrapper3 = new ImageWrapper();
//    ImageWrapper* outputImageWrapper4 = new ImageWrapper();
//    
//    /*
//    outputImageWrapper1->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
//    outputImageWrapper2->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
//    outputImageWrapper3->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
//    outputImageWrapper4->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
//    */
//    
//    outputImageWrapper1->SetImage<FImageType>( outputImage1 );
//    outputImageWrapper2->SetImage<FImageType>( outputImage2 );
//    outputImageWrapper3->SetImage<FImageType>( outputImage3 );
//    outputImageWrapper4->SetImage<FImageType>( outputImage4 );
//    
//    mOutputImages.append( outputImageWrapper1 );
//    mOutputImages.append( outputImageWrapper2 );
//    mOutputImages.append( outputImageWrapper3 );
//    mOutputImages.append( outputImageWrapper4 );
//
//    // update the process widget
//    ProcessObjectBase::Update();
//
//    // log the performance
//    ProcessObjectBase::LogPerformance();


    if(!FImageConverter::TransferImage(output->stack(), output, outputImage3)) {
      setErrorMessage("ITK Process tried to change the size of the stack");
      return false;
    }
    output->changed();


    return true;
  }
  REGISTER_PROCESS(UNet3DPredict2);
  
}
