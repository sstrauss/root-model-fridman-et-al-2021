//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef VTKPROGRESS_HPP
#define VTKPROGRESS_HPP

#include <VTKConfig.hpp>
#include <Progress.hpp>

class vtkCallbackCommand;
class vtkAlgorithm;

namespace mdx {
namespace process {
class vtk_EXPORT VTKProgress {
public:
  VTKProgress(const QString& text);
  ~VTKProgress();

  void setFilter(vtkAlgorithm* filter);

  Progress progress;
  vtkCallbackCommand* progressHandler, *startHandler, *endHandler;
};
}
}

#endif // VTKPROGRESS_HPP
