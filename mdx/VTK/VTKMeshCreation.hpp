//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef VTKMESHCREATION_HPP
#define VTKMESHCREATION_HPP

#include <VTKProcess.hpp>
#include <VTKProgress.hpp>

namespace mdx {
namespace process {
class vtk_EXPORT VTKDiscreteMarchingCube : public MeshProcess {
public:
  VTKDiscreteMarchingCube(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const QStringList& )
  {
    if(!checkState().store(STORE_LABEL).mesh())
      return false;
    const Store* store = currentStack()->currentStore();
    Mesh* mesh = currentMesh();
    return (*this)(store, mesh);
  }

  bool operator()(const Store* store, Mesh* mesh);

  QString folder() const {
    return "VTK/Creation";
  }
  QString name() const {
    return "Discrete Marching Cube";
  }
  QString description() const {
    return "Extract all the labelled cells using VTK marching cube algorithm";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon();
  }
};
} // namespace process
} // namespace mdx

#endif // VTKMESHCREATION_HPP
