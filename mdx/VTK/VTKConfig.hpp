//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef VTKCONFIG_HPP
#define VTKCONFIG_HPP

#if defined(WIN32) || defined(WIN64)

#  ifdef mdxVTKutil_EXPORTS
#    define vtk_EXPORT __declspec(dllexport)
#  else
#    define vtk_EXPORT __declspec(dllimport)
#  endif

#else

#  define vtk_EXPORT

#endif

#endif // VTKCONFIG_HPP
