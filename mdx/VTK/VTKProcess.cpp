//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "VTKProcess.hpp"
#include "vtkPolyData.h"
#include <vtkCellArray.h>

namespace mdx {
namespace process {
VTKImageConverter::VTKImageConverter()
  : vtkImageImport()
  , store(0)
{
}

VTKImageConverter::~VTKImageConverter() {
}

void VTKImageConverter::SetStore(const Store* s)
{
  store = s;
  const Stack* stack = s->stack();
  Point3u size = stack->size();
  Point3f step = stack->step();
  Point3f origin = stack->origin();
  this->SetDataScalarTypeToUnsignedShort();
  this->SetNumberOfScalarComponents(1);
  this->SetDataExtent(0, size.x() - 1, 0, size.y() - 1, 0, size.z() - 1);
  this->SetWholeExtent(0, size.x() - 1, 0, size.y() - 1, 0, size.z() - 1);
  this->SetDataSpacing(step.x(), step.y(), step.z());
  this->SetDataOrigin(origin.x(), origin.y(), origin.z());
  this->SetImportVoidPointer((void*)(&(s->data()[0])));
}

VTKImageConverter* VTKImageConverter::New() {
  return new VTKImageConverter;
}
}
}
