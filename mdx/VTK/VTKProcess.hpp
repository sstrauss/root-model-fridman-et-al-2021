//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef VTKPROCESS_HPP
#define VTKPROCESS_HPP

#include <VTKConfig.hpp>
#include <Process.hpp>
#include <vtkImageImport.h>

class vtkPolyData;

namespace mdx {
namespace process {

class vtk_EXPORT VTKImageConverter : public vtkImageImport {
public:
  static VTKImageConverter* New();

  typedef vtkImageImport SuperClass;

  void SetStore(const Store* store);
  const Store* GetStore() const {
    return store;
  }

protected:
  VTKImageConverter();
  ~VTKImageConverter();

  const Store* store;
};

bool triangulatedPolyDataToMesh(vtkPolyData* data, Mesh* mesh, bool replace = true);
}
}

#endif // VTKPROCESS_HPP
