#! /bin/bash

if [ "$1" == "--help" ]; then
  echo "Usage: $0 [VERSION]"
  echo "    Inform the morphographx build system to build the package for the version VERSION."
  echo "    This should trigger your editor to add the usual subversion copy line."
  exit 0
fi

VERSION="$1"

if [ ! -z $VERSION ]; then
  VERSION="-$VERSION"
fi

TIMESTAMP=$(date +%Y%m%d%H%M)

svn copy https://systemsx02.ethz.ch/svn/morphographx$VERSION/trunk https://systemsx02.ethz.ch/svn/morphographx$VERSION/triggers/$TIMESTAMP

