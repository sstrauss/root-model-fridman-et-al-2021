#!/bin/sh
# installs packages for building on Mint 17.2

set -eu

base_pkgs='
  g++
  cmake
  make
  qtbase5-dev
  libqt5-opengl-dev
  libglu1-mesa-dev
  libtbb-dev
  libtiff4-dev
  cimg-dev
  doxygen
  libglew-dev
  libthrust-dev
  libgsl0-dev
  gsl-bin
  libcgal-dev
'

versioned_pkgs='
  libcublas5.5
'

apt-get install $base_pkgs $versioned_pkgs
