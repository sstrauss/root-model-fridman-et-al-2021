#!/bin/bash

find_clang_format() {
if [ -z "$CLANG_FORMAT"]; then
    CLANG_FORMAT=$(which clang-format)
    local minor=20
    while [ -z "$CLANG_FORMAT" ]; do
        if (( minor < 5 )); then
            break  # We need clang-format-3.5 at least
        fi
        (( minor = minor - 1 ))
        CLANG_FORMAT=$(which clang-format-3.$minor)
    done
fi

if [ -z "$CLANG_FORMAT" ]; then
    echo "Error, couldn't find clang-format executable. Please set the CLANG_FORMAT environment variable." >&2
    exit 2
fi

export CLANG_FORMAT
}

export UNCRUSTIFY=$(which uncrustify)
export FOLDER=$(dirname "$0")

uncrustify_file() {
    local filename="$1"
    local output="$2"
    local show_info=""
    declare -a args=(-c "$FOLDER/uncrustify.cfg" -l cpp -q)
    if [[ -n "$output" && "$output" != "-" ]]; then
        show_info="yes"
        args+=(-o "$output")
    fi
    if [[ -n "$filename" && "$filename" != "-" ]]; then
        if [ -z "$output" ]; then
            show_info="yes"
            args+=(--replace --no-backup "$filename")
        else
            args+=(-f "$filename")
        fi
    fi
    if [ -n "$show_info" ]; then
        echo "Processing file '$filename'"
    fi
    "$UNCRUSTIFY" "${args[@]}"
}

process_file() {
    local filename="$1"
    local output="$2"
    if [ -z "$output" ]; then
        echo "Processing file '$filename'"
        "$CLANG_FORMAT" -i -style=file "$filename"
    elif [[ "$output" == "-" ]]; then
        "$CLANG_FORMAT" -style=file "$filename"
    else
        echo "Processing file '$filename'"
        "$CLANG_FORMAT" -style=file "$filename" > "$output"
    fi
}

process_folder() {
    local folder="$1"
    export -f process_file uncrustify_file
    echo "@@@ Processing folder $f ..."
    find "$folder" \( -name \*.cpp -o -name \*.hpp -o -name \*.h -o -name \*.cu \) \
        -exec bash -c "$PROCESS_FILE \"{}\"" ';'
}

process_all() {
    declare -a FOLDERS=(src ITK VTK libMGXViewer AddOns)

    for folder in "${FOLDERS[@]}"; do
        process_folder "$folder"
    done
}

find_clang_format

usage() {
    echo "Usage: $0 [-o|--output FILENAME] [-a|--all] [-u|--uncrustify] [TO_PROCESS]"
    echo "   -a|-all          Process all the folders with sources."
    echo "                    Other options are invalid if this is present."
    echo
    echo "   -o|--output      Specify the file to write the output to."
    echo "                    If this option is specified, at most a single file"
    echo "                    must be specified, or the standard input must be read."
    echo "                    '-' indicate the standard output. If not specified, "
    echo "                    the input is overwritten."
    echo
    echo "   -u|--uncrustify  Use Uncrustify instead for clang-format. Uncrustify has "
    echo "                    been configure to only act on indentation, not on line breaks."
    echo
    echo "   -h|--help        Print this message."
    echo
    echo "   TO_PROCESS       List of files and folders to process. If empty or '-', then"
    echo "                    the standard input is read."
}

ARGS=$(getopt --options "o:ahu" --long "output:,all,help,uncrustify" --name "$0" -- "$@")
if [ $? -ne 0 ]; then
    usage
    exit 1
fi

eval set -- "$ARGS"

PROCESS_FILE=process_file
OUTPUT=""
ALL=""
while true ; do
    case "$1" in
        -o|--output)
            if [ -z "$2" ]; then
                echo "No output specified in --output parameter"
                usage
                exit 1
            fi
            OUTPUT="$2"
            shift 2
            ;;
        -a|--all)
            ALL="all"
            shift
            ;;
        -u|--uncrustify)
            PROCESS_FILE=uncrustify_file
            shift
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        --)
            shift
            break
            ;;
    esac
done

export PROCESS_FILE

if [[ -n "$ALL" && ! ( -z "$*" && -z "$OUTPUT" ) ]]; then
    echo "If --all is specified, no other option can be specified."
    exit 1
fi

if [[ -n "$OUTPUT" && "$OUTPUT" != "-" &&
    ( $# -ne 1 || ! ( "$1" == "-" || -f "$1" ) ) ]]; then
    echo "ERROR: If you specify an output, you must provide exactly one, existing, input file."
    echo
    usage
    exit 1
fi

if [ -n "$ALL" ]; then
    process_all
elif [ -z "$OUTPUT" ]; then
    if [[ "$*" == "-" || -z "$*" ]]; then
        $PROCESS_FILE
    else
        for input in "$@"; do
            if [[ "$input" == "-" ]]; then
                echo "WARNING: cannot specify '-' within the list of files ... ignored!"
            elif [ -d "$input" ]; then
                process_folder "$input"
            elif [ -f "$input" ]; then
                $PROCESS_FILE "$input"
            else
                echo "WARNING: Cannot find input '$input' ... ignored!"
            fi
        done
    fi
else
    process_file "$*" "$OUTPUT"
fi
