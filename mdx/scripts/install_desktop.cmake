EXECUTE_PROCESS(
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --size 64 ${CMAKE_INSTALL_PREFIX}/share/MorphoDynamX/Icon64.png morphodynamx-MorphoDynamX
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --size 22 ${CMAKE_INSTALL_PREFIX}/share/MorphoDynamX/Icon22.png morphodynamx-MorphoDynamX
  COMMAND ${XDG_DESKTOP_MENU} install --mode system ${CMAKE_INSTALL_PREFIX}/share/MorphoDynamX/morphodynamx-MorphoDynamX.desktop
  COMMAND ${XDG_MIME} install --mode system ${CMAKE_INSTALL_PREFIX}/share/MorphoDynamX/morphodynamx-MorphoDynamX.xml
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --context mimetypes --size 48 ${CMAKE_INSTALL_PREFIX}/share/MorphoDynamX/FileIcon.png application-morphodynamx.mdxv
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --context mimetypes --size 48 ${CMAKE_INSTALL_PREFIX}/share/MorphoDynamX/StackIcon.png application-morphodynamx.mdxs
  COMMAND ${XDG_ICON_RESOURCE} install --mode system --context mimetypes --size 48 ${CMAKE_INSTALL_PREFIX}/share/MorphoDynamX/MeshIcon.png application-morphodynamx.mdxm
  )

