#include <CCF.hpp>
#include <CCIndex.hpp>
#include <vector>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN CCIndex
#include <boost/test/unit_test.hpp>

using namespace mdx;

BOOST_AUTO_TEST_CASE( pseudocell_comparisons )
{
  // They should be distinct
  BOOST_CHECK( CCIndex::UNDEF != CCIndex::INFTY );
  BOOST_CHECK( CCIndex::UNDEF != CCIndex::TOP );
  BOOST_CHECK( CCIndex::UNDEF != CCIndex::BOTTOM );
  BOOST_CHECK( CCIndex::UNDEF != CCIndex::Q );

  BOOST_CHECK( CCIndex::INFTY != CCIndex::TOP );
  BOOST_CHECK( CCIndex::INFTY != CCIndex::BOTTOM );
  BOOST_CHECK( CCIndex::INFTY != CCIndex::Q );

  BOOST_CHECK( CCIndex::TOP != CCIndex::BOTTOM );
  BOOST_CHECK( CCIndex::TOP != CCIndex::Q );

  BOOST_CHECK( CCIndex::BOTTOM != CCIndex::Q );

  // We need Q < (TOP,BOTTOM) < INFTY
  BOOST_CHECK( CCIndex::Q < CCIndex::BOTTOM );
  BOOST_CHECK( CCIndex::Q < CCIndex::TOP );
  BOOST_CHECK( CCIndex::BOTTOM < CCIndex::INFTY );
  BOOST_CHECK( CCIndex::TOP < CCIndex::INFTY );

  // Check match
  BOOST_CHECK( CCIndex::UNDEF.match(CCIndex::UNDEF) );
  BOOST_CHECK( !(CCIndex::UNDEF.match(CCIndex::INFTY)) );
  BOOST_CHECK( !(CCIndex::UNDEF.match(CCIndex::TOP)) );
  BOOST_CHECK( !(CCIndex::UNDEF.match(CCIndex::BOTTOM)) );
  BOOST_CHECK( CCIndex::UNDEF.match(CCIndex::Q) );

  BOOST_CHECK( !(CCIndex::INFTY.match(CCIndex::UNDEF)) );
  BOOST_CHECK( CCIndex::INFTY.match(CCIndex::INFTY) );
  BOOST_CHECK( !(CCIndex::INFTY.match(CCIndex::TOP)) );
  BOOST_CHECK( !(CCIndex::INFTY.match(CCIndex::BOTTOM)) );
  BOOST_CHECK( CCIndex::INFTY.match(CCIndex::Q) );

  BOOST_CHECK( !(CCIndex::TOP.match(CCIndex::UNDEF)) );
  BOOST_CHECK( !(CCIndex::TOP.match(CCIndex::INFTY)) );
  BOOST_CHECK( CCIndex::TOP.match(CCIndex::TOP) );
  BOOST_CHECK( !(CCIndex::TOP.match(CCIndex::BOTTOM)) );
  BOOST_CHECK( CCIndex::TOP.match(CCIndex::Q) );

  BOOST_CHECK( !(CCIndex::BOTTOM.match(CCIndex::UNDEF)) );
  BOOST_CHECK( !(CCIndex::BOTTOM.match(CCIndex::INFTY)) );
  BOOST_CHECK( !(CCIndex::BOTTOM.match(CCIndex::TOP)) );
  BOOST_CHECK( CCIndex::BOTTOM.match(CCIndex::BOTTOM) );
  BOOST_CHECK( CCIndex::BOTTOM.match(CCIndex::Q) );

  BOOST_CHECK( CCIndex::Q.match(CCIndex::UNDEF) );
  BOOST_CHECK( CCIndex::Q.match(CCIndex::INFTY) );
  BOOST_CHECK( CCIndex::Q.match(CCIndex::TOP) );
  BOOST_CHECK( CCIndex::Q.match(CCIndex::BOTTOM) );
  BOOST_CHECK( CCIndex::Q.match(CCIndex::Q) );

  // Check isPseudocell
  BOOST_CHECK( CCIndex::UNDEF.isPseudocell() );
  BOOST_CHECK( CCIndex::Q.isPseudocell() );
  BOOST_CHECK( CCIndex::TOP.isPseudocell() );
  BOOST_CHECK( CCIndex::BOTTOM.isPseudocell() );
  BOOST_CHECK( CCIndex::INFTY.isPseudocell() );
}

BOOST_AUTO_TEST_CASE( allocated_index_comparisons )
{
  std::vector<CCIndex> idx(10);
  for(unsigned int i = 0 ; i < idx.size() ; i++)
    idx[i] = CCIndexFactory.getIndex();

  for(unsigned int i = 0 ; i < idx.size() ; i++)
  {
    // None are pseudocells
    BOOST_CHECK( !(idx[i].isPseudocell()) );

    // All greater than any pseudocell
    BOOST_CHECK( idx[i] > CCIndex::UNDEF );
    BOOST_CHECK( idx[i] > CCIndex::Q );
    BOOST_CHECK( idx[i] > CCIndex::TOP );
    BOOST_CHECK( idx[i] > CCIndex::BOTTOM );
    BOOST_CHECK( idx[i] > CCIndex::INFTY );
    // All match Q
    BOOST_CHECK( idx[i].match(CCIndex::Q) );
    // and vice versa
    BOOST_CHECK( CCIndex::Q.match(idx[i]) );
    // and don't match any other pseudocell
    BOOST_CHECK( !idx[i].match(CCIndex::UNDEF) );
    BOOST_CHECK( !idx[i].match(CCIndex::TOP) );
    BOOST_CHECK( !idx[i].match(CCIndex::BOTTOM) );
    BOOST_CHECK( !idx[i].match(CCIndex::INFTY) );
    BOOST_CHECK( !CCIndex::UNDEF.match(idx[i]) );
    BOOST_CHECK( !CCIndex::TOP.match(idx[i]) );
    BOOST_CHECK( !CCIndex::BOTTOM.match(idx[i]) );
    BOOST_CHECK( !CCIndex::INFTY.match(idx[i]) );

    // All different, except the ones that are the same
    for(unsigned int j = 0 ; j < idx.size() ; j++)
    {
      if(i == j)
      {
	BOOST_CHECK( idx[i] == idx[j] );
	BOOST_CHECK( idx[i].match(idx[j]) );
      }
      else
      {
	BOOST_CHECK( idx[i] != idx[j] );
	BOOST_CHECK( !(idx[i].match(idx[j])) );
      }
    }
  }
}
