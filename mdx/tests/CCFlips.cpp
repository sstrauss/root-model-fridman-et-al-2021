#include <CCF.hpp>
#include <CCIndex.hpp>
#include <vector>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN CCFlips
#include <boost/test/unit_test.hpp>

#define BOOST_CHECK_FALSE(x) BOOST_CHECK(!(x))

using namespace mdx;
typedef CCStructure::FlipI Flip;
typedef ccf::FlipTable<CCIndex> FlipTable;

BOOST_AUTO_TEST_CASE( flip_class )
{
  const int NUM_VS = 10;
  std::vector<CCIndex> vs(NUM_VS);
  for(int i = 0 ; i < NUM_VS ; i++)
    vs[i] = CCIndexFactory.getIndex();

  BOOST_CHECK( vs[0] != vs[1] ); // just to make sure

//  SECTION( "Test constructors" )
  {
    CCIndex defaultIndex;  // uninitialized = default

    // Uninitialized => all cells default
    Flip f1;
    BOOST_CHECK( f1.interior == defaultIndex );
    BOOST_CHECK( f1.facet[0] == defaultIndex );
    BOOST_CHECK( f1.facet[1] == defaultIndex );
    BOOST_CHECK( f1.joint == defaultIndex );

    // One argument => all cells the same
    Flip f2(vs[0]);
    BOOST_CHECK( f2.interior == vs[0] );
    BOOST_CHECK( f2.facet[0] == vs[0] );
    BOOST_CHECK( f2.facet[1] == vs[0] );
    BOOST_CHECK( f2.joint == vs[0] );

    // All four cells specified: joint, f0, f1, interior
    Flip f3(vs[0],vs[1],vs[2],vs[3]);
    BOOST_CHECK( f3.interior == vs[3] );
    BOOST_CHECK( f3.facet[0] == vs[1] );
    BOOST_CHECK( f3.facet[1] == vs[2] );
    BOOST_CHECK( f3.joint == vs[0] );

    // Copy constructor
    Flip f4(f3);
    BOOST_CHECK( f4.interior == vs[3] );
    BOOST_CHECK( f4.facet[0] == vs[1] );
    BOOST_CHECK( f4.facet[1] == vs[2] );
    BOOST_CHECK( f4.joint == vs[0] );

    // Set equal
    Flip f5 = f4;
    BOOST_CHECK( f5.interior == vs[3] );
    BOOST_CHECK( f5.facet[0] == vs[1] );
    BOOST_CHECK( f5.facet[1] == vs[2] );
    BOOST_CHECK( f5.joint == vs[0] );
  }

//  SECTION( "Test orientations" )
  {
    Flip f1(vs[0],vs[1],vs[2],vs[3]);

//    SECTION( "Negation operator" )
    {
      Flip f2 = -f1;

      BOOST_CHECK( f2.joint == f1.joint );
      BOOST_CHECK( f2.facet[0] == f1.facet[1] );
      BOOST_CHECK( f2.facet[1] == f1.facet[0] );
      BOOST_CHECK( f2.interior == f1.interior );
    }

//    SECTION( "Premult operator" )
    {
      Flip f2 = ccf::POS * f1, f3 = ccf::NEG * f1;

      BOOST_CHECK( f2.joint == f1.joint );
      BOOST_CHECK( f2.facet[0] == f1.facet[0] );
      BOOST_CHECK( f2.facet[1] == f1.facet[1] );
      BOOST_CHECK( f2.interior == f1.interior );

      BOOST_CHECK( f3.joint == f1.joint );
      BOOST_CHECK( f3.facet[0] == f1.facet[1] );
      BOOST_CHECK( f3.facet[1] == f1.facet[0] );
      BOOST_CHECK( f3.interior == f1.interior );
    }

//    SECTION( "Postmult operator" )
    {
      Flip f2 = f1 * ccf::POS, f3 = f1 * ccf::NEG;

      BOOST_CHECK( f2.joint == f1.joint );
      BOOST_CHECK( f2.facet[0] == f1.facet[0] );
      BOOST_CHECK( f2.facet[1] == f1.facet[1] );
      BOOST_CHECK( f2.interior == f1.interior );

      BOOST_CHECK( f3.joint == f1.joint );
      BOOST_CHECK( f3.facet[0] == f1.facet[1] );
      BOOST_CHECK( f3.facet[1] == f1.facet[0] );
      BOOST_CHECK( f3.interior == f1.interior );
    }
    
//    SECTION( "Postmult with assignment operator" )
    {
      Flip f2 = f1, f3 = f1;

      f2 *= ccf::POS;
      BOOST_CHECK( f2.joint == f1.joint );
      BOOST_CHECK( f2.facet[0] == f1.facet[0] );
      BOOST_CHECK( f2.facet[1] == f1.facet[1] );
      BOOST_CHECK( f2.interior == f1.interior );

      f3 *= ccf::NEG;
      BOOST_CHECK( f3.joint == f1.joint );
      BOOST_CHECK( f3.facet[0] == f1.facet[1] );
      BOOST_CHECK( f3.facet[1] == f1.facet[0] );
      BOOST_CHECK( f3.interior == f1.interior );
    }
  }

//  SECTION( "Test ordering" )
  {
    Flip f1(vs[0],vs[1],vs[2],vs[3]), f2(vs[4],vs[1],vs[2],vs[3]),
      f3(vs[0],vs[4],vs[2],vs[3]), f4(vs[0],vs[1],vs[4],vs[3]), f5(vs[0],vs[1],vs[2],vs[4]);

    // just to make sure...
    BOOST_CHECK( vs[0] < vs[1] );
    BOOST_CHECK( vs[1] < vs[2] );
    BOOST_CHECK( vs[2] < vs[3] );
    BOOST_CHECK( vs[3] < vs[4] );

    // f1 should be less than any other
    BOOST_CHECK_FALSE( f1 < f1 );
    BOOST_CHECK( f1 < f2 );
    BOOST_CHECK_FALSE( f2 < f1 );
    BOOST_CHECK( f1 < f3 );
    BOOST_CHECK_FALSE( f3 < f1 );
    BOOST_CHECK( f1 < f4 );
    BOOST_CHECK_FALSE( f4 < f1 );
    BOOST_CHECK( f1 < f5 );
    BOOST_CHECK_FALSE( f5 < f1 );

    // then f5
    BOOST_CHECK_FALSE( f5 < f5 );
    BOOST_CHECK( f5 < f2 );
    BOOST_CHECK_FALSE( f2 < f5 );
    BOOST_CHECK( f5 < f3 );
    BOOST_CHECK_FALSE( f3 < f5 );
    BOOST_CHECK( f5 < f4 );
    BOOST_CHECK_FALSE( f4 < f5 );

    // then f4
    BOOST_CHECK_FALSE( f4 < f4 );
    BOOST_CHECK( f4 < f2 );
    BOOST_CHECK_FALSE( f2 < f4 );
    BOOST_CHECK( f4 < f3 );
    BOOST_CHECK_FALSE( f3 < f4 );

    // then f3
    BOOST_CHECK_FALSE( f3 < f3 );
    BOOST_CHECK( f3 < f2 );
    BOOST_CHECK_FALSE( f2 < f3 );

    BOOST_CHECK_FALSE( f2 < f2 );
  }

//  SECTION( "Test otherFacet" )
  {
    Flip f1(vs[0],vs[1],vs[2],vs[3]);

    // otherFacet for joint / interior / nonexistent
    BOOST_CHECK_THROW( f1.otherFacet(vs[0]) , std::domain_error );
    BOOST_CHECK_THROW( f1.otherFacet(vs[3]) , std::domain_error );
    BOOST_CHECK_THROW( f1.otherFacet(vs[4]) , std::domain_error );

    // otherFacet for facets
    BOOST_CHECK( f1.otherFacet(vs[1]) == vs[2] );
    BOOST_CHECK( f1.otherFacet(vs[2]) == vs[1] );
  }

//  SECTION( "Test realFacet" )
  {
    Flip f1(vs[0],vs[1],vs[2],vs[3]), f2(vs[0],CCIndex::UNDEF,vs[1],vs[2]),
      f3(vs[0],vs[1],CCIndex::UNDEF,vs[2]), f4(vs[0],CCIndex::TOP,CCIndex::BOTTOM,vs[1]);

    BOOST_CHECK( f1.realFacet() == vs[1] || f1.realFacet() == vs[2] );
    BOOST_CHECK( f2.realFacet() == vs[1] );
    BOOST_CHECK( f3.realFacet() == vs[1] );
    BOOST_CHECK_THROW( f4.realFacet() , std::domain_error );
  }

//  SECTION( "Test replaceIndex" )
  {
    Flip f1(vs[0],vs[1],vs[2],vs[3]);

//    SECTION( "Replace nonexistent index" )
    {
      Flip f2 = f1.replaceIndex(vs[4],vs[5]);
      BOOST_CHECK( f2.joint == vs[0] );
      BOOST_CHECK( f2.facet[0] == vs[1] );
      BOOST_CHECK( f2.facet[1] == vs[2] );
      BOOST_CHECK( f2.interior == vs[3] );
    }

//    SECTION( "Replace joint" )
    {
      Flip f2 = f1.replaceIndex(vs[0],vs[4]);
      BOOST_CHECK( f2.joint == vs[4] );
      BOOST_CHECK( f2.facet[0] == vs[1] );
      BOOST_CHECK( f2.facet[1] == vs[2] );
      BOOST_CHECK( f2.interior == vs[3] );
    }

//    SECTION( "Replace interior" )
    {
      Flip f2 = f1.replaceIndex(vs[3],vs[4]);
      BOOST_CHECK( f2.joint == vs[0] );
      BOOST_CHECK( f2.facet[0] == vs[1] );
      BOOST_CHECK( f2.facet[1] == vs[2] );
      BOOST_CHECK( f2.interior == vs[4] );
    }

//    SECTION( "Replace facet[0]" )
    {
      Flip f2 = f1.replaceIndex(vs[1],vs[4]);
      BOOST_CHECK( f2.joint == vs[0] );
      BOOST_CHECK( f2.facet[0] == vs[4] );
      BOOST_CHECK( f2.facet[1] == vs[2] );
      BOOST_CHECK( f2.interior == vs[3] );
    }

//    SECTION( "Replace facet[1]" )
    {
      Flip f2 = f1.replaceIndex(vs[2],vs[4]);
      BOOST_CHECK( f2.joint == vs[0] );
      BOOST_CHECK( f2.facet[0] == vs[1] );
      BOOST_CHECK( f2.facet[1] == vs[4] );
      BOOST_CHECK( f2.interior == vs[3] );
    }
  }

//  SECTION( "Test match" )
  {
    Flip f1(vs[0],vs[1],vs[2],vs[3]), f2 = f1, fQ(CCIndex::Q);

    BOOST_CHECK( f1.match(f1) );
    BOOST_CHECK( f1.match(f2) );
    BOOST_CHECK( f2.match(f1) );
    BOOST_CHECK( fQ.match(f1) );
    BOOST_CHECK( f1.match(fQ) );

    BOOST_CHECK( f1.match(-f1) );
    BOOST_CHECK( (-f1).match(f1) );

//    SECTION( "Q in each position" )
    {
      Flip fJ = f1, fF0 = f1, fF1 = f1, fI = f1;
      fJ.joint = CCIndex::Q;
      fF0.facet[0] = CCIndex::Q;
      fF1.facet[1] = CCIndex::Q;
      fI.interior = CCIndex::Q;

      BOOST_CHECK( f1.match(fJ) );
      BOOST_CHECK( f1.match(fF0) );
      BOOST_CHECK( f1.match(fF1) );
      BOOST_CHECK( f1.match(fI) );

      BOOST_CHECK( fJ.match(f1) );
      BOOST_CHECK( fF0.match(f1) );
      BOOST_CHECK( fF1.match(f1) );
      BOOST_CHECK( fI.match(f1) );
    }

//    SECTION( "Differs in each position" )
    {
      Flip fJ = f1, fF0 = f1, fF1 = f1, fI = f1;
      fJ.joint = vs[4];
      fF0.facet[0] = vs[4];
      fF1.facet[1] = vs[4];
      fI.interior = vs[4];

      BOOST_CHECK_FALSE( f1.match(fJ) );
      BOOST_CHECK_FALSE( f1.match(fF0) );
      BOOST_CHECK_FALSE( f1.match(fF1) );
      BOOST_CHECK_FALSE( f1.match(fI) );

      BOOST_CHECK_FALSE( fJ.match(f1) );
      BOOST_CHECK_FALSE( fF0.match(f1) );
      BOOST_CHECK_FALSE( fF1.match(f1) );
      BOOST_CHECK_FALSE( fI.match(f1) );
    }
  }
}

BOOST_AUTO_TEST_CASE( fliptable_class )
{
  const int NUM_VS = 10;
  std::vector<CCIndex> vs(NUM_VS);
  for(int i = 0 ; i < NUM_VS ; i++)
    vs[i] = CCIndexFactory.getIndex();

  Flip f1(vs[0],vs[1],vs[2],vs[3]), f2(vs[4],vs[5],vs[6],vs[7]),
    f3(vs[8],vs[9],vs[0],vs[1]), f4(vs[2],vs[3],vs[4],vs[5]), f5(vs[6],vs[7],vs[8],vs[9]);

//  SECTION( "Normal set operations" )
  {
    FlipTable fs;

    BOOST_CHECK( fs.empty() );

    fs.insert(f1);
    fs.insert(f2);
    fs.insert(f3);

    BOOST_CHECK( fs.size() == 3 );

    // add two more
    fs.insert(f4);
    fs.insert(f5);

    BOOST_CHECK( fs.size() == 5 );

    // these are already there => no effect
    fs.insert(f3);
    fs.insert(f1);

    // erase two
    fs.erase(f4);
    fs.erase(f2);

    BOOST_CHECK( fs.size() == 3 );

    // erase again
    fs.erase(f2);
    fs.erase(f4);

    BOOST_CHECK( fs.size() == 3 );

    // count and find
    BOOST_CHECK( fs.count(f1) == 1 );
    BOOST_CHECK( fs.count(f2) == 0 );
    BOOST_CHECK( fs.count(f3) == 1 );
    BOOST_CHECK( fs.count(f4) == 0 );
    BOOST_CHECK( fs.count(f5) == 1 );

    BOOST_CHECK( fs.find(f2) == fs.end() );
    BOOST_CHECK( fs.find(f4) == fs.end() );

    // clear
    fs.clear();
    BOOST_CHECK( fs.size() == 0 );
    BOOST_CHECK( fs.begin() == fs.end() );
  }

//  SECTION( "Test insertion limits" )
  {
    FlipTable fs;
    fs.insert(f1);
    fs.insert(-f2);
    fs.insert(f3);
    fs.insert(-f4);

    BOOST_CHECK( fs.count(f1) == 1 );
    //BOOST_CHECK( fs.count(f2) == 0 );
    BOOST_CHECK( fs.count(f3) == 1 );
    //BOOST_CHECK( fs.count(f4) == 0 );

    BOOST_CHECK( fs.size() == 4 );
  }

//  SECTION( "Test match()" )
  {
    FlipTable fs;
    fs.insert(f1);     // (vs[0],vs[1],vs[2],vs[3])
    fs.insert(-f2);    // (vs[4],vs[6],vs[5],vs[7])
    fs.insert(f3);     // (vs[8],vs[9],vs[0],vs[1])

    fs.insert(Flip(vs[0],vs[4],vs[5],vs[7]));
    fs.insert(Flip(vs[7],vs[1],vs[4],vs[3]));
    fs.insert(Flip(vs[8],vs[4],vs[6],vs[9]));

    BOOST_CHECK( fs.matchV(f1).size() == 1 );
    BOOST_CHECK( fs.matchV(f2).size() == 1 );
    BOOST_CHECK( fs.matchV(f3).size() == 1 );
    BOOST_CHECK( fs.matchV(f4).size() == 0 );
    BOOST_CHECK( fs.matchV(f5).size() == 0 );

    BOOST_CHECK( fs.matchFirst(f1).isValid() );
    BOOST_CHECK( fs.matchFirst(f2).isValid() );
    BOOST_CHECK( fs.matchFirst(f3).isValid() );
    BOOST_CHECK_FALSE( fs.matchFirst(f4).isValid() );
    BOOST_CHECK_FALSE( fs.matchFirst(f5).isValid() );

    std::vector<Flip>
      vqJ  = fs.matchV(Flip(vs[0],CCIndex::Q,CCIndex::Q,CCIndex::Q)),
      vqFa = fs.matchV(Flip(CCIndex::Q,vs[4],CCIndex::Q,CCIndex::Q)),
      vqFb = fs.matchV(Flip(CCIndex::Q,CCIndex::Q,vs[4],CCIndex::Q)),
      vqI  = fs.matchV(Flip(CCIndex::Q,CCIndex::Q,CCIndex::Q,vs[3]));
    BOOST_CHECK( vqJ.size()  == 2 );
    BOOST_CHECK( vqFa.size() == 3 );
    BOOST_CHECK( vqFb.size() == 3 );
    BOOST_CHECK( vqI.size()  == 2 );

    Flip
      qJ1 = fs.matchFirst(Flip(vs[0],CCIndex::Q,CCIndex::Q,CCIndex::Q)),
      qJ2 = fs.matchFirst(Flip(vs[9],CCIndex::Q,CCIndex::Q,CCIndex::Q));
    BOOST_CHECK( qJ1.isValid() );
    BOOST_CHECK_FALSE( qJ2.isValid() );

    Flip
      qF1a = fs.matchFirst(Flip(CCIndex::Q,vs[6],CCIndex::Q,CCIndex::Q)),
      qF1b = fs.matchFirst(Flip(CCIndex::Q,CCIndex::Q,vs[1],CCIndex::Q)),
      qF2a = fs.matchFirst(Flip(CCIndex::Q,vs[3],CCIndex::Q,CCIndex::Q)),
      qF2b = fs.matchFirst(Flip(CCIndex::Q,CCIndex::Q,vs[7],CCIndex::Q));
    BOOST_CHECK( qF1a.isValid() );
    BOOST_CHECK( qF1b.isValid() );
    BOOST_CHECK_FALSE( qF2a.isValid() );
    BOOST_CHECK_FALSE( qF2b.isValid() );

    Flip
      qI1 = fs.matchFirst(Flip(CCIndex::Q,CCIndex::Q,CCIndex::Q,vs[9])),
      qI2 = fs.matchFirst(Flip(CCIndex::Q,CCIndex::Q,CCIndex::Q,vs[4]));
    BOOST_CHECK( qI1.isValid() );
    BOOST_CHECK_FALSE( qI2.isValid() );
  }
}
