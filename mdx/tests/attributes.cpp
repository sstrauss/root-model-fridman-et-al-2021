#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN attributes
#include <boost/test/unit_test.hpp>


#include <vector>
#include <Mesh.hpp>
#include <Features.hpp>

#if __cplusplus < 201103L
#define nullptr 0
#endif


const std::vector<int>& getAttribute(mdx::Mesh &mesh, const QString &name, unsigned idx)
{
  // If the map called "name" is not present, it will be created
  mdx::AttrMap<unsigned, std::vector<int> >& myAttributeMap = mesh.attributes().attrMap<unsigned, std::vector<int> >(name);
  // This is also guaranteed to work - if key "idx" doesn't exist, default constructor of vector<int> will be used.
  return myAttributeMap[idx];
}

void setAttribute(mdx::Mesh &mesh, const QString &name, unsigned idx, std::vector<int>& input)
{
  // If the map called "name" is not present, it will be created
  mdx::AttrMap<unsigned, std::vector<int> >& myAttributeMap = mesh.attributes().attrMap<unsigned, std::vector<int> >(name);
  // This is also guaranteed to work - if key "idx" doesn't exist, default constructor of vector<int> will be used.
  myAttributeMap[idx] = input;
}

BOOST_AUTO_TEST_CASE( access_map ) {
  mdx::Attributes attrs;
  mdx::AttrMap<unsigned, std::vector<int> > &amap = attrs.attrMap<unsigned, std::vector<int> >("foo");
  BOOST_CHECK(amap.size() == 0);
}

// GCC <= 5.4 breaks on C++11 code by messing up Mesh::_attributes and deadlocking
BOOST_AUTO_TEST_CASE( mesh_access_map ) {
  mdx::Mesh mesh(0, nullptr);
  mdx::Attributes &attrs = mesh.attributes();
  BOOST_CHECK(&(attrs.attrMap<unsigned, std::vector<int> >("foo")) != nullptr);
}

BOOST_AUTO_TEST_CASE( create_map ) {
  mdx::Mesh m(0, nullptr);
  const std::vector<int>& out = getAttribute(m, "test", 0);
  BOOST_CHECK(out.size() == 0);
}

BOOST_AUTO_TEST_CASE( filled_attribute ) {
  mdx::Mesh m(0, nullptr);
  std::vector<int>& vec = m.attributes().attrMap<unsigned, std::vector<int> >("testmap")[99];
  vec.push_back(1);
  vec.push_back(2);

  const std::vector<int>& saved = getAttribute(m, "testmap", 99);
  BOOST_CHECK_EQUAL_COLLECTIONS(vec.begin(), vec.end(), saved.begin(), saved.end());
}

BOOST_AUTO_TEST_CASE( set_attribute ) {
  mdx::Mesh m(0, nullptr);
  int initials[] = {1, 2, 3};
  std::vector<int> vec(initials, initials + sizeof(initials) / sizeof(int));

  setAttribute(m, "test", 19, vec);  
  
  std::vector<int>& saved = m.attributes().attrMap<unsigned, std::vector<int> >("test")[19];
  BOOST_CHECK_EQUAL_COLLECTIONS(vec.begin(), vec.end(), saved.begin(), saved.end());
}

BOOST_AUTO_TEST_CASE( make_vector ) {
  mdx::Mesh mesh(0, nullptr);
  mdx::AttrMap<unsigned, std::vector<int> > &m = mesh.attributes().attrMap<unsigned, std::vector<int> >("foo");
  BOOST_CHECK(m[0].size() == 0);
}

BOOST_AUTO_TEST_CASE( use_vector ) {
  mdx::Mesh mesh(0, nullptr);
  mdx::AttrMap<unsigned, std::vector<int> > &m = mesh.attributes().attrMap<unsigned, std::vector<int> >("foo");
  m[0].push_back(1);
  BOOST_CHECK(m[0][0] == 1);
}
