#ifndef LASSOSELECT_H
#define LASSOSELECT_H
#include<vector>
#include<Geometry.hpp>
using mdx::Point3d;

class LassoSelect{
public:
  bool LassoContains(Point3d v){

    long int_count = 0;
    for(size_t i=0;i<contour.size();i++){
      Point3d p1 = contour[i];
      Point3d p2 = contour[(i+1)%contour.size()];
      if( (v.y()-p1.y())*(v.y()-p2.y())<0){
        if(v.x()>p1.x() && v.x()>p2.x())
          int_count++;
        else if( (v.x()-p1.x())*(v.x()-p2.x())<0 ){
          double side = ((p2-p1)%(v-p1)).z();
          side *= (p2-p1).y()>0? -1:1;
          if(side>0)
            int_count++;
        }
      }

    }
    return int_count%2;
  }

  void AddPoint(Point3d v){contour.push_back(v);};
  void Clear(){contour.clear();};
  //member variables
  std::vector<Point3d> contour;

};


#endif
