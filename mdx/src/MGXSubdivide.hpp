//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#ifndef MDX_SUBDIVIDE_HPP
#define MDX_SUBDIVIDE_HPP

/**
 * \file MDXSubdivide.hpp
 *
 * Subdivide object for built in MDX data
 */

#include <Subdivide.hpp>
#include <Mesh.hpp>

namespace mdx
{
  // Class to subdivide MDX data
  class mdx_EXPORT MDXSubdivide : virtual public Subdivide
  {
    public:
      MDXSubdivide(Mesh *m) : mesh(m) {}

    protected:
      virtual bool updateCellData(cell c, cell cl, cell cr);
      virtual bool updateEdgeData(vertex l, vertex v, vertex r, double s);
    
    private: 
      Mesh *mesh;
  };
}

#endif

