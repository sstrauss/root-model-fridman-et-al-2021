//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <ColorEditDlg.hpp>
#include <ui_ColorEditDlg.h>
#include <QAbstractButton>
#include <QColorDialog>
#include <QFileDialog>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QMenu>
#include <QPoint>
#include <QItemSelectionModel>
#include <QCheckBox>

#include "Dir.hpp"
#include <stdlib.h>
#include <Random.hpp>
#include <Geometry.hpp>
#include <Information.hpp>

using mdx::Colorb;
using mdx::ColorMap;
using mdx::Point2f;
using mdx::Point3f;
using mdx::Point4f;
using mdx::ran;

// Color Model class methods

ColorModel::ColorModel(ColorMap &colorMap)
  : colorMapDest(colorMap), colorsLocal(colorMap.colors),
    channelsLocal(colorMap.numChannels()),
    usingIndices(colorMap.numChannels())
{
  for(uint i = 0 ; i < colorMap.numChannels() ; i++)
  {
    channelsLocal[i] = colorMap.channelMap(i);
    usingIndices[i] = (channelsLocal[i].indices[0] >= 0) && (channelsLocal[i].indices[1] >= 0);
    if(!usingIndices[i])
    {
      channelsLocal[i].indices[0] = 0;
      channelsLocal[i].indices[1] = colorMap.colors.size() - 1;
    }
  }
}

int ColorModel::rowCount(const QModelIndex& parent) const
{
  if(parent.isValid())
    return 0;
  return colorsLocal.size();
}

Qt::ItemFlags ColorModel::flags(const QModelIndex& ) const 
{
  return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant ColorModel::data(const QModelIndex& index, int role) const
{
  if(index.column() > 0)
    return QVariant();
  if(index.row() >= (int)colorsLocal.size())
    return QVariant();
  int n = index.row();
  switch(role) {
  case Qt::DisplayRole:
    return QString::number(n);
  case Qt::DecorationRole:
    return (QColor)colorsLocal[n];
  }
  return QVariant();
}

void ColorModel::reset()
{
  loadPresetColors("Uniform Jet");
}

void ColorModel::presetColors(const QString &preset)
{
  loadPresetColors(preset);
  emit dataChanged(index(0), index(colorsLocal.size() - 1));
}

void ColorModel::loadPresetColors(const QString &preset)
{
  if(preset == "Preset Color Maps")
    return;
  beginResetModel();
  uint oldSize = colorsLocal.size();
  ColorMap colorMap;
  colorMap.setColors(preset, oldSize);
  uint newSize = colorMap.size();
  colorsLocal = colorMap.colors;
  setNbColors(newSize);
  endResetModel();
}

void ColorModel::apply() 
{
  colorMapDest.colors = colorsLocal;
  for(uint i = 0 ; i < channelsLocal.size() ; i++)
  {
    colorMapDest.channelMap(i) = channelsLocal[i];
    if(!usingIndices[i])
      colorMapDest.channelMap(i).indices = mdx::Point2i(-1,-1);
  }
}

bool ColorModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
  if(index.column() > 0)
    return false;
  if(index.row() >= (int)colorsLocal.size())
    return false;
  if(role == Qt::DecorationRole) {
    colorsLocal[index.row()] = (Colorb)value.value<QColor>();
    emit dataChanged(index, index);
  }
  return false;
}

void ColorModel::setNbColors(int n)
{
  int prev = rowCount();
  if(n > prev)
    insertRows(prev, n - prev);
  else if(n < prev)
    removeRows(n, prev - n);
  emit nbColorsChanged(n);
}

bool ColorModel::insertRows(int row, int count, const QModelIndex& parent)
{
  int prev = rowCount();
  if(row < prev or parent.isValid())
    return false;
  int n = prev + count;
  beginInsertRows(QModelIndex(), prev, n - 1);
  colorsLocal.resize(n);
  for(int i = prev; i < n; ++i)
    colorsLocal[i] = colorsLocal[i - prev];
  endInsertRows();
  return true;
}

bool ColorModel::removeRows(int row, int count, const QModelIndex& parent)
{
  int prev = rowCount();
  if(row != prev - count or parent.isValid())
    return false;
  beginRemoveRows(QModelIndex(), row, prev - 1);
  colorsLocal.resize(row);
  endRemoveRows();
  return true;
}

// Color Edit Dialog class methods
ColorEditDlg::ColorEditDlg(ColorMap &cMap, QWidget* parent)
  : QDialog(parent), colorMap(cMap)
{
  ui = new Ui::ColorEditDlg();
  ui->setupUi(this);
  model = new ColorModel(cMap);
 
  ui->colorsView->setModel(model);
  connect(model, SIGNAL(rowsRemoved(const QModelIndex &, int, int)), this, SLOT(changeNbItems()));
  connect(model, SIGNAL(rowsInserted(const QModelIndex &, int, int)), this, SLOT(changeNbItems()));
  if(colorMap.size() == 0)
    model->reset();
  ui->nbColors->setValue(model->rowCount());

  ui->presetColors->addItems(colorMap.getColorMapNames());

  // Get a width for index spinboxes based on 3-digit numbers
  int iBoxMinWidth;
  {
    QSpinBox iBox;
    iBox.setMaximum(999);
    iBoxMinWidth = iBox.minimumSizeHint().width();
  }

  // Create a row of buttons for each channel map in the signal
  for(uint i = 0 ; i < colorMap.numChannels() ; i++)
  {
    ColorMap::ChannelMap &chmap = colorMap.channelMap(i);
    QHBoxLayout *layout = new QHBoxLayout;

    // Checkbox to select if channel is visible
    QCheckBox *visibleBox = new QCheckBox("Visible");
    visibleBox->setChecked(chmap.visible);
    visibleBox->setLayoutDirection(Qt::RightToLeft);
    connect(visibleBox,&QCheckBox::toggled,
            [i,this](bool val) { model->setVisible(i,val); });
    layout->addWidget(visibleBox);
    layout->addStretch(10);

    // If name is there use it, otherwise use channel number
    QString name = chmap.name;
    if(name.isEmpty())
      name = QString("Channel %1").arg(i+1);
    QLabel *nameLabel = new QLabel(name);
    layout->addWidget(nameLabel);
    layout->addStretch(100);

    if(chmap.cmType == ColorMap::Range) {
      // Range min and max spinners
      QLabel *rangeLabel = new QLabel("Range");

      QDoubleSpinBox *minBox = new QDoubleSpinBox;
      minBox->setRange(-1e10,1e10);
      minBox->setValue(chmap.bounds[0]);
      minBox->setDecimals(4);
      connect(minBox,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
              [i,this](double val) { model->setMinBound(i,val); });

      QDoubleSpinBox *maxBox = new QDoubleSpinBox;
      maxBox->setRange(-1e10,1e10);
      maxBox->setValue(chmap.bounds[1]);
      maxBox->setDecimals(4);
      connect(maxBox,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),
              [i,this](double val) { model->setMaxBound(i,val); });

      // Add to layout
      layout->addWidget(rangeLabel);
      layout->addWidget(minBox);
      layout->addWidget(maxBox);
      layout->addStretch(10);
    }

    if(chmap.cmType != ColorMap::Color) {
      // Min and max indices
      bool usingIndices = chmap.indices[0] >= 0 && chmap.indices[1] >= 0;
      QSpinBox *minIBox = new QSpinBox;
      minIBox->setMaximum(model->rowCount() - 1);
      minIBox->setEnabled(usingIndices);
      minIBox->setMinimumWidth(iBoxMinWidth);
      connect(minIBox,static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
              [i,this](double val) { model->setMinIndex(i,val); });
      connect(model,&ColorModel::nbColorsChanged,
              [minIBox](int n) { minIBox->setMaximum(n-1); });

      QSpinBox *maxIBox = new QSpinBox;
      maxIBox->setMaximum(model->rowCount() - 1);
      maxIBox->setEnabled(usingIndices);
      maxIBox->setMinimumWidth(iBoxMinWidth);
      connect(maxIBox,static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
              [i,this](double val) { model->setMaxIndex(i,val); });
      connect(model,&ColorModel::nbColorsChanged,
              [maxIBox](int n) { maxIBox->setMaximum(n-1); });

      if(usingIndices) {
        minIBox->setValue(chmap.indices[0]);
        maxIBox->setValue(chmap.indices[1]);
      } else {
        minIBox->setValue(0);
        maxIBox->setValue(model->rowCount() - 1);
      }

      // Checkbox to determine whether we are using indices at all
      QCheckBox *indicesBox = new QCheckBox("Indices");
      indicesBox->setChecked(usingIndices);
      connect(indicesBox,&QCheckBox::toggled,
              [i,minIBox,maxIBox,this](bool val) {
                model->setUsingIndices(i,val);
                minIBox->setEnabled(val);
                maxIBox->setEnabled(val);
              });

      // Add to layout
      layout->addWidget(indicesBox);
      layout->addWidget(minIBox);
      layout->addWidget(maxIBox);
    }

    ui->channelControls->addLayout(layout);
  }
}

ColorEditDlg::~ColorEditDlg()
{
  ui->colorsView->setModel(0);
  delete model;
  delete ui;
}

void ColorEditDlg::on_presetColors_currentIndexChanged(QString preset)
{
  if(preset.size() == 0)
    return;
  model->presetColors(preset);
  ui->presetColors->setCurrentIndex(0);
  ui->nbColors->setValue(model->rowCount());
}

void ColorEditDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui->buttonBox->buttonRole(btn)) {
  case QDialogButtonBox::ApplyRole:
  case QDialogButtonBox::AcceptRole:
    model->apply();
    emit update();
    break;
  case QDialogButtonBox::ResetRole:
    model->reset();
    emit update();
    break;
  default:
    break;
  }
}

void ColorEditDlg::on_importColors_clicked()
{
  QString filename = QFileDialog::getOpenFileName(this, "Select labels file name", mdx::currentPath(),
                                                  "Palette files (*.mdxc);;All files (*.*)");

  if(!QFile::exists(filename) and !filename.endsWith(".mdxc"))
    filename += ".mdxc";
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    mdxInfo << "Error, cannot open file '" << filename << "' for reading" << endl;
    return;
  }
  QTextStream ts(&file);
  QString header = ts.readLine();
  if(!(header.startsWith("MDX COLORS") or header.startsWith("MGX COLORS"))) {
    mdxInfo << "Error, file '" << filename << "' is not a MorphoDynamX label file" << endl;
    return;
  }
  QStringList hv = header.split(" ");
  if(hv.size() != 3 or hv[2] != "1.0") {
    mdxInfo << "Error, file '" << filename << "' has an unknown version number" << endl;
    return;
  }
  QString cntString = ts.readLine();
  int cnt;
  bool ok;
  cnt = cntString.trimmed().toInt(&ok);
  if(!ok or cnt < 0) {
    mdxInfo << "Error, in file '" << filename << "': Invalid number of colors" << endl;
    return;
  }
  QAbstractItemModel* m = ui->colorsView->model();
  int n = m->rowCount();
  if(n > cnt)
    m->removeRows(cnt, n - cnt);
  else if(n < cnt)
    m->insertRows(n, cnt - n);
  for(int i = 0; i < cnt; ++i) {
    QString color = ts.readLine().trimmed();
    if(color.isEmpty()) {
      mdxInfo << "Error, in file '" << filename << "': Color " << i
                                   << " is invalid or non-existent (expected: " << cnt << " colors)" << endl;
      return;
    }
    QColor col = QColor(color);
    QModelIndex idx = m->index(i, 0);
    m->setData(idx, col, Qt::DecorationRole);
  }
  ui->nbColors->setValue(cnt);
}

void ColorEditDlg::on_exportColors_clicked()
{
  QString filename = QFileDialog::getSaveFileName(this, "Select labels file name", "",
                                                  "Palette files (*.mdxc);;All files (*.*)");

  if(!filename.endsWith(".mdxc"))
    filename += ".mdxc";
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    mdxInfo << "Error, cannot open file '" << filename << "' for writing" << endl;
    return;
  }
  QTextStream ts(&file);
  QAbstractItemModel* m = ui->colorsView->model();
  ts << "MDX COLORS 1.0" << endl;
  int cnt = m->rowCount();
  ts << cnt << endl;
  for(int i = 0; i < cnt; ++i) {
    QModelIndex idx = m->index(i, 0);
    QColor col = m->data(idx, Qt::DecorationRole).value<QColor>();
    ts << col.name() << endl;
  }
  file.close();
}

void ColorEditDlg::on_colorsView_doubleClicked(const QModelIndex& idx)
{
  QColor col = model->data(idx, Qt::DecorationRole).value<QColor>();
  if(col.isValid()) {
    col = QColorDialog::getColor(col, this, "Change color");
    if(col.isValid()) {
      model->setData(idx, col, Qt::DecorationRole);
    }
  }
}

void ColorEditDlg::on_setNbColors_clicked() 
{
  model->setNbColors(ui->nbColors->value());
}

//void ColorEditDlg::on_colorsView_customContextMenuRequested(const QPoint& pos)
//{
//  QModelIndex idx = ui->colorsView->indexAt(pos);
//  if(idx.isValid()) {
//    selectedColor = idx.row();
//    QMenu* menu = new QMenu(this);
//    menu->addAction(interpolateAction);
//    menu->popup(ui->colorsView->mapToGlobal(pos));
//  }
//}

void ColorEditDlg::changeNbItems() 
{
  ui->nbColors->setValue(ui->colorsView->model()->rowCount());
}
