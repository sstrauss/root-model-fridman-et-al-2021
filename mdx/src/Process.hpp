//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PROCESS_HPP
#define PROCESS_HPP

/**
 * \file Process.hpp
 *
 * File containing the definition of a Process.
 */

#include <Config.hpp>
#include <GL.hpp>

#include <QIcon>
#include <QSharedPointer>
#include <QStringList>
#include <QTextStream>

#include <MDXCamera.hpp>
#include <ProcessParms.hpp>
#include <Forall.hpp>
#include <Vector.hpp>
#include <Attributes.hpp>
#include <Information.hpp>
#include <Clip.hpp>
#include <CuttingSurface.hpp>
#include <Mesh.hpp>
#include <Stack.hpp>
#include <Store.hpp>
#include <Quaternion.hpp>
#include <SystemCommand.hpp>

#include <algorithm>
#include <iostream>
#include <typeinfo>
#include <tr1/memory>

class QWidget;
class MorphoDynamX;

/**
 * \def PROCESS_VERSION
 *
 * Number identifying the running version of the process API. This makes sure the loaded 
 * processes will run with the current API. Otherwise, an error informing correctly the user
 * will be shown in the standard error.
 *
 * This is set to the SVN revision number that triggered a change in the process interface 
 */
#define PROCESS_VERSION 1380

/**
 * \mainpage MorphoDynamX Plug-in documentation
 *
 * Plug-ins in MorphoDynamX are called processes. Most features in MorphoDynamX are implemented
 * internally as processes.
 *
 * All processes are inherited from the Process class.
 *
 * Plug-ins are compiled into shared object (.so) files and are loaded when MorphoDynamX starts.
 * They can be installed in a system area for all users, or in the user's home directory. Run
 * the command:
 *
 * \verbatim $ MorphoDynamX --all-process \endverbatim
 *
 * to print the plug-in directories.
 *
 * The best way to start is from a sample plug-in available from the MorphoDynamX
 * website: www.MorphoDynamX.org. 
 *
 * If you write a useful plug-in, please let us know so that we can incorporate it
 * into MorphoDynamX.
 *
 * \defgroup StackProcess Stack Processes
 *
 * List of Stack processes
 *
 * \defgroup MeshProcess Mesh Processes
 *
 * List of Mesh processes
 *
 * \defgroup ToolsProcess Tools Processes
 *
 * List of tools processes
 *
 * \defgroup ModelProcess Tools Processes
 *
 * List of processes for modeling
 *
 * \defgroup ProcessUtils Process utilities
 *
 * Classes and functions needed to create your own processes or call other processes.
 */

/**
 * \namespace mdx
 *
 * This namespace contains all the API of MorphoDynamX
 */
namespace mdx 
{
  /**
	 * Here is a template of a minimal process:
   *
   * \section Basics
   *
   * \code
   * class DoNothing : public Process
   * {
   * public:
   *   // Processes can only be created with the copy constructor
   *   DoNothing(const StackProcess& proc) : Process(proc) 
   *   {
   *     setName("Nothing");
   *     setDescription("Do nothing");
   *   }
   *   // Define what the class do ... here it's nothing
   *   bool run() { return true; }
   * };
   * \endcode
   *
   * Then, in the implementation file, you need to add:
   * \code
   * REGISTER_PROCESS(DoNothing);
   * \endcode
   * Which will take care of registering the process to the system when the
   * library is loaded.
   *
   * \section Recommendations
   *
   * Beside the minimal process, it is recommended to structure the process in two functions:
   * the first one taking generic arguments (i.e. a string list and a float list), the other
   * one taking specific arguments. This way, your process will
   * be easier to use from another C++ process.
   *
   * Also, to help providing meaningful (and uniform) error messages, and to help you
   * testing the current state of the process, the checkState() method is provided.
   *
   * The structure then typically becomes:
   *
   * \code
   * class DoSomething : public Process
   * {
   * public:
   *   // Add parameters in the constructor as follows:
   *   //   addParm(name, description, default, pick-list);
   *   //
   *   // Name, description, and default are all QStrings, pick-list is a QStringList.
   *   // 
   *   DoSomething(const StackProcess& proc) : Process(proc) 
   *   {
   *     setName("Nothing");
   *     setDescription("Do nothing");
   *     // This time, we have four parameters
   *     addParm("Store", "The store to process", "Work", storeChoice);
   *     addParm("Amount", "The amount to process", "2.0");
   *     addParm("Percent", "The store to process", "Work", booleanChoice);
   *     addParm("User Parm", "A dropdown defined by the user", "Case1", QStringList << "Case1" << "Case2");
   *   }
   *
   *   // Run the process
   *   bool run()
   *   {
   *     Store *store = currentStack()->work();
   *     bool isneeded = stringToBool(parms[1]);
   *     QString aname = parms[2];
   *     float param = parms[3].toFloat();
   *     bool result = 
   *        run(stringToStore(parm("Store")), parm("Amount").toDouble(), stringToBool(parm("Precent")), parm("User Parm"));
   *     // Update store if successful
   *     if(result)
   *       store->changed();
   *     return result;
   *   }
   *
   *   // run() with specialized arguments is recommended to be used by other C++ processes
   *   bool run(Store *store, double amount, bool percent, const QString &userParm)
   *   {
   *     // Do what is needed, return false if failed
   *     return true;
   *   }
   * };
   * \endcode
   */
 
  #ifndef DOXYGEN
  class PrivateProcess;
  class SetupProcess;
  #endif
  
  /**
   * \class UserCancelException Process.hpp <Process.hpp>
   *
   * Exception launched when a user clicks the Cancel button.
   * When writing your own processes, you should use the userCancel() method.
   * \ingroup ProcessUtils
   */
  class mdx_EXPORT UserCancelException : public std::exception {
  public:
    UserCancelException() : std::exception() {}
  
    const char* what() const throw() {
      return "UserCancel";
    }
  };
  
  /**
   * \class Process Process.hpp <Process.hpp>
   *
   * This is the main process class, the one all process inherit from.
   * \ingroup ProcessUtils
   */
  class mdx_EXPORT Process : public QObject, public virtual ProcessParms
  {
    Q_OBJECT
    friend class SetupProcess;
  public:
    static unsigned int processVersion;
  
    typedef std::vector<Stack*>::iterator stack_iterator;
    typedef std::vector<Stack*>::const_iterator const_stack_iterator;
  
    typedef std::vector<Mesh*>::iterator mesh_iterator;
    typedef std::vector<Mesh*>::const_iterator const_mesh_iterator;

  private:
    /**
     * Constructor used only to initialize the first instance in SetupProcess.
     */
    Process();
  public:
    /**
     * Copy constructor.
     *
     * Use when inheriting from Process.
     * Normally getProcess is used to create new processes.
     */
    Process(const Process& p);
  public:
    /**
     * Virtual destructor
     */
    virtual ~Process() {}
  
    /**
     * Method to be called anytime a file is acted on (i.e. saved/loaded).
     *
     * If needed, it will set the current folder to the one containing the file and start a session
     *
     * If it is a project file (i.e. ending in mdxv), then project_file should be set to true to force the change in
     * folder.
     */
    void actingFile(const QString& filename, bool project_file = false);
  
    /**
     * Get the file currently defining the path of the system
     */
    QString actingFile() const;
  
    /**
     * Return the python call describing the current process
     */
    QString pythonCall(const QStringList &parms) const;
  
    /**
     * Number of stacks available to the process
     */
    int stackCount() const;
    /**
     * Returns the ith stack, or 0 if there is no such stack.
     */
    Stack* getStack(int i);
    /**
     * Get the stack by name
     */
    Stack *getStack(const QString &stack);
    /**
     * Returns the current stack (i.e. the stack currently selected by the user).
     */
    Stack* currentStack();
    /**
     * Returns the other stack (i.e. the stack not currently selected by the user).
     */
    Stack* otherStack();
    /**
     * Return the id (i.e. number) of the current stack.
     */
    int currentStackId() const;
    /**
     * Return the id (i.e. number) of the other stack.
     */
    int otherStackId() const;
    /**
     * Change which stack is current.
     */
    void setCurrentStackId(int i);
    bool setCurrentStack(int id, bool is_main);
  
    //bool resetProject();
  
    /**
     * Save screen shot to a file
     */
    bool takeSnapshot(QString filename, float overSampling = 1.0f, int width = 0, int height = 0, int quality = 95,
                      bool expand_frustum = false);
    /**
     * Display a message in the status bar  
     */
    bool setStatus(const QString &msg, bool alsoPrint = true);

    /**
     * Run a system command
     */
    bool systemCommand(int command, const QStringList &parms);

    /**
     * Get the camera
     */
    MDXCamera *camera();

    //
    /// Iterate over all the stacks
    std::pair<stack_iterator, stack_iterator> stacks();
    /// Iterate over all the stacks
    std::pair<const_stack_iterator, const_stack_iterator> stacks() const;
  
    /**
     * Add a new stack to the process
     */
    Stack* addStack();
  
    /**
     * Delete the stack of given id
     *
     * \note The current implementation assumes there are at least two stacks always available
     */
    bool deleteStack(int i);
  
    /**
     * Returns the number of mesh available to the process
     */
    int meshCount() const;
    /**
     * Returns the ith mesh
     */
    Mesh* getMesh(int i);
    /**
     * Returns the mesh by name
     */
    Mesh* getMesh(const QString &mesh);
    /**
     * Returns the current mesh (i.e. the mesh currently selected by the user)
     */
    Mesh* currentMesh();
    /**
     * Returns the other mesh (i.e. the mesh not currently selected by the user)
     */
    Mesh* otherMesh();
    /**
     * Returns the id (i.e. number) of the current mesh
     */
    int currentMeshId() const;
    /**
     * Returns the id (i.e. number) of the other mesh
     */
    int otherMeshId() const;
    /**
     * Change which mesh is current
     */
    void setCurrentMeshId(int i);
  
    /// Iterate over all the meshs
    std::pair<mesh_iterator, mesh_iterator> meshes();
    /// Iterate over all the meshs
    std::pair<const_mesh_iterator, const_mesh_iterator> meshes() const;
  
    /**
     * Add a mesh to the process for the given stack
     */
    Mesh* addMesh(Stack* stack);
  
    /**
     * Remove a mesh from the process
     */
    bool deleteMesh(int i);
  
    /**
     * Get the current selected label
     */
    int selectedLabel() const;
  
    /**
     * Change the current selected label
     */
    void setSelectedLabel(int label);
  
    /**
     * Return the current setting for the global brightness level
     */
    double globalBrightness();
  
    /**
     * Return the current setting for the global contrast level
     */
    double globalContrast();
  
    /**
     * Return the current setting for the global shininess level
     */
    double globalShininess();

    /**
     * Return the current setting for the global specular level
     */
    double globalSpecular();
  
    /**
     * Change the current setting for the global brightness level
     *
     * The brightness is clamped to the range [-1, 1]
     */
    void setGlobalBrightness(double value);
  
    /**
     * Change the current setting for the global contrast level
     *
     * The contrast is clamped to the range [0, 2]
     */
    void setGlobalContrast(double value);
  
    /**
     * Change the current setting for the global shininess level
     *
     * The shininess is clamped to the range [0, 128]
     */
    void setGlobalShininess(double value);
  
    /**
     * Change the current setting for the global specular level
     *
     * The specular is clamped to the range [0, 1]
     */
    void setGlobalSpecular(double value);
  
    /**
     * Returns if the user has mesh selection active
     */
    bool meshSelection() const;
    /**
     * Returns is the user has line border selection active
     */
    bool lineBorderSelection() const;
  
    /**
     * Update the Gui of MorphoDynamX
     */
    bool updateState();
    /**
     * Tell the viewer to redraw
     */
    bool updateViewer();
    /**
     * Load viewfile
     */
    bool loadView(const QString &fileName);
     /**
     * Save viewfile
     */
    bool saveView(const QString &fileName); 
     /**
     * Set the visualization for a frame
     */
    bool setFrameVis(const QString &name, ulong flags, const Point3d &pos, const Quaternion &orient); 
     /**
     * Set the visualization for camera
     */
    bool setCameraVis(const Point3d &pos, const Quaternion &orient, const Point3d &center, double zoom); 
    /**
     * Set an error message that will be displayed if the process returns false.
     */
    bool setErrorMessage(const QString& str);
  
    /**
     * Get the current error message
     */
    QString errorMessage() const;
  
    /**
     * Throw an exception informing the system that the user canceled the current process.
     */
    void userCancel() const { throw UserCancelException(); }
  
    /**
     * Get the name of the file that was used to load the current process 
     * (i.e. the mdxv file), if any.
     */
    const QString& file() const;
  
    /**
     * Return the object defining the first clipping region
     */
    Clip *clip1();
    /**
     * Return the object defining the second clipping region
     */
    Clip *clip2();
    /**
     * Return the object defining the third clipping region
     */
    Clip *clip3();
  
    /**
     * Return the object defining the first clipping region
     */
    const Clip *clip1() const;
    /**
     * Return the object defining the second clipping region
     */
    const Clip *clip2() const;
    /**
     * Return the object defining the third clipping region
     */
    const Clip *clip3() const;
  
    /**
     * Return the cutting surface.
     */
    CuttingSurface *cuttingSurface();

    /**
     * Return the cutting surface.
     */
    const CuttingSurface *cuttingSurface() const;
  
    //@{
    //\name Methods needed to launch other processes
  
    /**
     * Gets a process object by name
     */
    Process *getProcess(const QString &processName);
    /**
     * Get process 
     */
    template <typename ProcessT>
    bool getProcess(const QString &processName, ProcessT* &p) 
    {
      p = dynamic_cast<ProcessT*>(getProcess(processName));
      if(p)
        return true;
      else
        return false;
    } 
    /**
     * Set the parms from the GUI
     * */
    bool setCurrentParms();

    /**
     * Enumeration for process flow control
     */
    enum ProcessAction { PROCESS_RUN, PROCESS_STEP, PROCESS_REWIND, PROCESS_FINALIZE } ;

    /**
     * Launch a process with generic arguments.
     *
     * Note that exceptions are filtered and converted into an error message and
     * the process returning false.
     */
    bool runProcess(Process &proc, QStringList &parms) throw();
  
    /**
     * Launch a process by name
     *
     * Note that exceptions are filtered and converted into an error message and the
     * process returning false.
     */
    bool runProcess(const QString &processName, QStringList &parms) throw();

    //@}
  
    // @{ Functions to be defined by the user

    /**
     * Initialize the process. Called from the main (GUI) thread before the process
     * is run. If there is an active GUI, then parent will be set.
     *
     * The process can use the method to update the argument list with a dialog. If the arguments
     * are updates, the process should return true
     *
     * \param parent Pointer to the parent widget if there is an active GUI.
     * @returns false if the process is not to run afterwards
     */
    virtual bool initialize(QWidget * /*parent*/) { return true; }

    /**
     * @brief Starts the process.
     * If your process is implemented in steps, step() will be
     * called until step() returns false of the user cancels. If the process is a one-step, simply override this function.
     *
     * @returns false if process failed, indicating that the should pop up the error message
     */
    virtual bool run()
    {
      bool result = true;
      do {
        result = step(); 
        updateState();
        updateViewer();
      } while(result and progressAdvance());

      return progressAdvance(); 
    }

    /**
     * Make one step of the process
     */
    virtual bool step() { return false; };

    /**
     * Make one step of the process
     */
    virtual bool rewind(QWidget * /*parent*/) { return true; };

    /**
     * This is an optional method that is called from the main thread before the process
     * is run. If there is an active GUI, then parent will be set.
     *
     * \param parent Pointer to the parent widget if there is an active GUI.
     */
    virtual bool finalize(QWidget * /*parent*/) { return true; }

    /**
     * Run a process with its current parms
     */
    bool runWithCurrentParms()
    {
      setCurrentParms();
      return run();
    }

    // @}
  
    /**
     * Returns the number of named string for this process
     */
    virtual uint numParms() const { return parmDefaults().size(); }

    static std::vector<Colorf> &LabelColors;

  private:
    friend MorphoDynamX;
    PrivateProcess *p = 0;

  };
  
  #ifndef DOXYGEN
  
  struct mdx_EXPORT ProcessFactory 
  {
    ProcessFactory() {}
    virtual ~ProcessFactory() {}
  
    virtual Process* operator()(const Process& process) = 0;
    virtual void reload() = 0;
  };
  
  //#  ifdef WIN32
  //struct mdx_EXPORT ProcessFactory;
  //#  endif
  
  template<typename ProcessName> struct ClassProcessFactory : public ProcessFactory 
  {
    ClassProcessFactory() : ProcessFactory(), pPtr(0) {}
  
    virtual Process* operator()(const Process& process)
    {
      // Recycle the process object to allows variable persistence
      if(!pPtr)
        pPtr = new ProcessName(process);

      // Grab the parms from the GUI
      pPtr->setCurrentParms();

      return pPtr;
    }
    void reload()
    {
      // Recycle the process object to allows variable persistence
      if(pPtr) {
        delete pPtr;
        pPtr = 0;
      }
    }
    virtual ~ClassProcessFactory()
    {
      if(pPtr)
        delete pPtr;
    }

    ProcessName *pPtr;
  };
  
  // Smart pointer to factory
  typedef std::shared_ptr<ProcessFactory> ProcessFactoryPtr;
  
  struct mdx_EXPORT Registration {
    typedef QList<ProcessFactoryPtr> factoryList;
  
    Registration(ProcessFactoryPtr f, const char* class_name, unsigned int compiledVersion);
    ~Registration();
    static factoryList& processFactories();
  
    ProcessFactoryPtr factory;
    const char* classname;
  
  private:
    static factoryList factories;
  };
  
  inline QList<ProcessFactoryPtr>& Registration::processFactories()
  {
    return factories;
  }
  
  inline QList<ProcessFactoryPtr>& processFactories() 
  {
    return Registration::processFactories();
  }
  #endif
  
  /**
   * \def REGISTER_PROCESS(ClassName)
   *
   * Register \c ClassName as a process. It must inherit Process and have a constructor 
   * accepting a single Process.
   * \ingroup ProcessUtils
   */
  #define REGISTER_PROCESS(ClassName) \
            mdx::Registration ClassName ## registration( \
            mdx::ProcessFactoryPtr(new mdx::ClassProcessFactory<ClassName>()), \
                                     typeid(ClassName).name(), PROCESS_VERSION)
  /**
   * \struct ProcessDefinition Process.hpp <Process.hpp>
   *
   * Definition of a process
   *  \ingroup ProcessUtils
   */
  struct mdx_EXPORT ProcessDefinition 
  {
    QString name;            ///< Name of the process
    QString description;     ///< Description of the process
    QStringList parmNames;   ///< List of named parameters of the process
    QStringList parmDescs;   ///< List of descriptions of named parameters
    QStringList parms;       ///< List of parameters/parameter defaults
    QIcon icon;              ///< Icon of the process
    ParmChoiceList parmChoice;   ///< List of choices for parameters

    /**
     * \typedef process_t
     *
     * Type of the generated process.
     */
    typedef Process process_t;
    /**
     * Process factory
     */
    ProcessFactoryPtr factory;
  };
  
  #ifndef DOXYGEN
    typedef QMap<QString, ProcessDefinition> processesMap_t;
    
    mdx_EXPORT extern processesMap_t processes;
  #endif
  
  ///\addtogroup ProcessUtils
  ///@{
  /**
   * Retrieves the process definition from the name of the process
   */
  mdx_EXPORT ProcessDefinition* getProcessDefinition(const QString& processName);
  
  /// Get the parameters for a given process
  mdx_EXPORT bool getProcessParms(const Process& proc, QStringList& parms);
  /// Get the default parameters for a given process (i.e. the ones defined by the process)
  mdx_EXPORT bool getDefaultParms(const Process& proc, QStringList& parms);
  /// Save the default parameters in memory
  mdx_EXPORT bool saveProcessParms(const Process& proc, const QStringList &parms);
  /// Check if the parameters have enough defaults
  mdx_EXPORT bool checkProcessParms(const Process& proc, const QStringList &parms, size_t* nbParms = 0);
  /// Get the parameters for a given process by name
  mdx_EXPORT bool getProcessParms(const QString& processName, QStringList& parms);
  /// Get the default parameters for a given process (i.e. the ones defined by the process)
  mdx_EXPORT bool getDefaultParms(const QString& processName, QStringList& parms);
  /// Save the default parameters in memory
  mdx_EXPORT bool saveProcessParms(const QString& processName, const QStringList &parms);
  /// Check if the parameters have enough defaults
  mdx_EXPORT bool checkProcessParms(const QString& processName, const QStringList &parms,
                                    size_t* nbParms = 0);
  /// Get the definition of a process
  mdx_EXPORT ProcessDefinition* getProcessDefinition(const QString& processThread, const QString& processName);
  /// Returns the list of names of the processes
  mdx_EXPORT QStringList listProcesses();
  /// Split process name into Tab/Folder/Name
  mdx_EXPORT bool getProcessText(const QString &text, QString &tab, QString &folder, QString &name);

  /**
   * Check if the \c processName exist in the list of processes.
   */
  mdx_EXPORT bool validProcessName(const QString& processName);
  
  ///@}
}
#endif
