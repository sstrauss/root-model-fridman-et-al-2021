//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <Process.hpp>
#include <Mangling.hpp>
#include <Version.hpp>
#include <ImageData.hpp>
#include <PrivateProcess.hpp>
#include <Forall.hpp>
#include <QTextStream>
#include <stdio.h>
#include <ProcessThread.hpp>
#include <QMutexLocker>
#include <Information.hpp>
#include <memory>
#include <exception>
#include <Dir.hpp>
#include <Misc.hpp>

static QTextStream err(stderr);

namespace mdx 
{
  std::vector<Colorf> &Process::LabelColors = ImgData::LabelColors;

  unsigned int Process::processVersion = PROCESS_VERSION;
  
  Process::Process() : p(0) {}
  
  Process::Process(const Process &p) : p(p.p)  {}

  bool Process::setCurrentParms()
  {
    ProcessDefinition* def = getProcessDefinition(name());
    if(def) {
      setParms(def->parms);
      return true;
    }
    return false;
  }
 
  QString Process::pythonCall(const QStringList &parms) const
  {
    QStringList all_parms;
    forall(QString s, parms)
      all_parms << QString("'%1'").arg(shield_python(s));
    QString name = this->name();
    name.replace('/', "__");
    name.replace(' ', "_");
    return QString("Process.%1(%2)").arg(name).arg(all_parms.join(", "));
  }
  
  QString Process::actingFile() const 
  {
    return p->actingFile;
  }
  
  void Process::actingFile(const QString& filename, bool project_file)
  {
    if(!filename.isEmpty() and (project_file or file().isEmpty())) {
      QString dir = getDir(filename);
      bool changed_dir = (dir != currentPath());
      setCurrentPath(dir);
      p->actingFile = filename;
      if((project_file and file().isEmpty()) or changed_dir) {
        QFile f("MorphoDynamX.py");
        if(f.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
          QTextStream ss(&f);
          QDateTime now = QDateTime::currentDateTime();
          ss << "######## New MorphoDynamX session v" << VERSION << " r" << REVISION << ": "
             << now.toString("yyyy-MM-dd hh:mm:ss") << endl;
          ss << p->currentPythonCall << endl;
          p->changedFolder = true;
          f.close();
        }
      }
    }
  }
  
  int Process::stackCount() const 
  {
    return p->_stacks.size();
  }
  
  std::pair<Process::stack_iterator, Process::stack_iterator> Process::stacks()
  {
    return std::make_pair(p->_stacks.begin(), p->_stacks.end());
  }
  
  std::pair<Process::const_stack_iterator, Process::const_stack_iterator> Process::stacks() const
  {
    return std::make_pair(p->_stacks.begin(), p->_stacks.end());
  }
  
  Stack* Process::getStack(int i)
  {
    if(i == -1)
      return currentStack();
    if(i < 0 or i >= (int)p->_stacks.size())
      return 0;
    return p->_stacks[i];
  }

  Stack* Process::getStack(const QString &stack)
  {
    if(stack == "Current")
      return currentStack();
    else if(stack == "Other")
      return otherStack();
    else if(stack == "Stack 1")
      return p->_stacks[0];
    else if(stack == "Stack 2")
      return p->_stacks[1];
    else
      return 0;
  } 
  Stack* Process::currentStack()
  {
    if(currentStackId() == -1)
      return 0;
    return p->_stacks[currentStackId()];
  }
  
  Stack* Process::otherStack()
  {
    if(otherStackId() < 0)
      return 0;
    return p->_stacks[otherStackId()];
  }
  
  int Process::currentStackId() const 
  {
    return p->current_stack;
  }
  
  int Process::otherStackId() const 
  {
    if(p->current_stack < 0)
			return -1;
		else if(p->current_stack == 0)
		  return 1;
		else if(p->current_stack == 1)
		  return 0;
		
		return -1;
  }
  
  void Process::setCurrentStackId(int i)
  {
    if(i >= -1 and i < (int)p->_stacks.size())
      p->current_stack = i;
  }
  
  Stack* Process::addStack()
  {
    Stack* s = new Stack((int)p->_stacks.size());
    p->_stacks.push_back(s);
    return s;
  }
  
  bool Process::deleteStack(int i)
  {
    if(i >= 0 and i < (int)p->_stacks.size()) {
      delete p->_stacks[i];
      p->_stacks[i] = 0;
      p->_stacks.erase(p->_stacks.begin() + i);
      for(int j = 0; j < (int)p->_stacks.size(); ++j)
        p->_stacks[j]->setId(j);
      return true;
    }
    return false;
  }
  
  int Process::meshCount() const {
    return p->_meshes.size();
  }
  
  std::pair<Process::mesh_iterator, Process::mesh_iterator> Process::meshes()
  {
    return std::make_pair(p->_meshes.begin(), p->_meshes.end());
  }
  
  std::pair<Process::const_mesh_iterator, Process::const_mesh_iterator> Process::meshes() const
  {
    return std::make_pair(p->_meshes.begin(), p->_meshes.end());
  }
  
  Mesh* Process::getMesh(int i)
  {
    if(i < 0 or uint(i) >= p->_meshes.size())
      return 0;
    return p->_meshes[i];
  }
   
  Mesh* Process::getMesh(const QString &mesh)
  {
    if(mesh == "Current")
      return currentMesh();
    else if(mesh == "Other")
      return otherMesh();
    else if(mesh == "Mesh 1")
      return p->_meshes[0];
    else if(mesh == "Mesh 2")
      return p->_meshes[1];
    else
      return 0;
  }  

  Mesh* Process::currentMesh()
  {
    if(currentMeshId() == -1)
      return 0;
    return p->_meshes[currentMeshId()];
  }
  
  Mesh* Process::otherMesh()
  {
    if(otherMeshId() < 0)
      return 0;
    return p->_meshes[otherMeshId()];
  }
  
  int Process::currentMeshId() const {
    return p->current_mesh;
  }
  
  int Process::otherMeshId() const {
    if(p->current_mesh < 0)
			return -1;
		else if(p->current_mesh == 0)
		  return 1;
		else if(p->current_mesh == 1)
		  return 0;

    return -1;
  }
  
  void Process::setCurrentMeshId(int i)
  {
    if(i >= -1 and i < (int)p->_meshes.size())
      p->current_mesh = i;
  }
  
  Mesh* Process::addMesh(Stack* stack)
  {
    Mesh* m = new Mesh((int)p->_meshes.size(), stack);
    p->_meshes.push_back(m);
    return m;
  }
  
  bool Process::deleteMesh(int i)
  {
    if(i >= 0 and i < (int)p->_meshes.size()) {
      delete p->_meshes[i];
      p->_meshes[i] = 0;
      p->_meshes.erase(p->_meshes.begin() + i);
      for(int j = 0; j < (int)p->_meshes.size(); ++j)
        p->_meshes[j]->setId(j);
      return true;
    }
    return false;
  }
  
  int Process::selectedLabel() const {
    return p->selected_label;
  }
  
  void Process::setSelectedLabel(int label) {
    p->selected_label = label;
  }
  
  double Process::globalBrightness() {
    return p->globalBrightness;
  }
  
  double Process::globalContrast() {
    return p->globalContrast;
  }

  double Process::globalShininess() {
    return p->globalShininess;
  }
  
  double Process::globalSpecular() {
    return p->globalSpecular;
  }
  
  void Process::setGlobalBrightness(double value)
  {
    if(value > 1)
      p->globalBrightness = 1.0f;
    else if(value < -1)
      p->globalBrightness = -1.0f;
    else
      p->globalBrightness = value;
  }
  
  void Process::setGlobalContrast(double value)
  {
    if(value > 2)
      p->globalContrast = 2.0f;
    else if(value < 0)
      p->globalContrast = 0.0f;
    else
      p->globalContrast = value;
  }

  void Process::setGlobalShininess(double value)
  {
    if(value > 128.0)
      p->globalShininess = 128.0f;
    else if(value < 0)
      p->globalShininess = 0.0f;
    else
      p->globalShininess = value;
  }
  
  void Process::setGlobalSpecular(double value)
  {
    if(value > 1.0)
      p->globalSpecular = 1.0f;
    else if(value < 0)
      p->globalSpecular = 0.0f;
    else
      p->globalSpecular = value;
  } 

  bool Process::meshSelection() const {
    return p->mesh_selection;
  }
  
  bool Process::lineBorderSelection() const {
    return p->line_border_selection;
  }
  
  bool Process::setErrorMessage(const QString& str)
  {
    p->error = str;
    return false;
  }
  
  QString Process::errorMessage() const { return p->error; }
  
  bool Process::updateState()
  {
    return systemCommand(UPDATE_STATE, QStringList());
  }

  bool Process::updateViewer() 
  {
    return systemCommand(UPDATE_VIEWER, QStringList());
  }

  bool Process::loadView(const QString &fileName) 
  {
    return systemCommand(LOAD_VIEW, QStringList() << fileName);
  } 

  bool Process::saveView(const QString &fileName) 
  {
    return systemCommand(SAVE_VIEW, QStringList() << fileName);
  } 

  bool Process::setCurrentStack(int id, bool isMain)
  {
    if(id >= stackCount()) {
      setErrorMessage(
        QString("There are only %1 stacks, cannot made stack %2 the current one")
                                                       .arg(stackCount()).arg(id));
      return false;
    }
    return systemCommand(SET_CURRENT_STACK, QStringList() << boolToString(isMain) << QString::number(id));
  }
  
  bool Process::takeSnapshot(QString fileName, float overSampling, int width, int height, 
                                                            int quality, bool expandFrustum)
  {
    return systemCommand(TAKE_SNAPSHOT, QStringList() << fileName
       << QString::number(overSampling) << QString::number(width) << QString::number(height) 
       << QString::number(quality) << boolToString(expandFrustum));
  }

  // Put a message on the status bar
  bool Process::setStatus(const QString &msg, bool alsoPrint)
  {
    return systemCommand(SET_STATUS, QStringList() << msg << boolToString(alsoPrint));
  }

  // Set the visualization for a frame
  bool Process::setFrameVis(const QString &name, ulong flags, const Point3d &pos, const Quaternion &orient)
  {
    return systemCommand(SET_FRAME_VIS, QStringList() << name << QString::number(flags)
        << QString("%1 %2 %3").arg(pos.x()).arg(pos.y()).arg(pos.z()) 
        << QString("%1 %2 %3 %4").arg(orient.x()).arg(orient.y()).arg(orient.z()).arg(orient.w()));
  }

  // Set the visualization for camera
  bool Process::setCameraVis(const Point3d &pos, const Quaternion &orient, const Point3d &center, double zoom)
  {
    return systemCommand(SET_CAMERA_VIS, QStringList() << QString("%1 %2 %3").arg(pos.x()).arg(pos.y()).arg(pos.z())
        << QString("%1 %2 %3 %4").arg(orient.x()).arg(orient.y()).arg(orient.z()).arg(orient.w())
        << QString("%1 %2 %3").arg(center.x()).arg(center.y()).arg(center.z()) << QString::number(zoom));
  }

  // Send a signal to Gui thread to run a system command
  bool Process::systemCommand(int command, const QStringList &parms) 
  {
    QMetaObject::invokeMethod(p->parent, "systemCommand", 
        isGuiThread() ? Qt::DirectConnection : Qt::BlockingQueuedConnection,
        Q_ARG(mdx::Process *, this), Q_ARG(int, command), Q_ARG(QStringList, parms));
    return true;
  }

  MDXCamera *Process::camera() { return p->camera; }

  Clip *Process::clip1() { return &p->clip1; }
  Clip *Process::clip2() { return &p->clip2; }
  Clip *Process::clip3() { return &p->clip3; }
  const Clip *Process::clip1() const { return &p->clip1; } 
  const Clip *Process::clip2() const { return &p->clip2; } 
  const Clip *Process::clip3() const { return &p->clip3; }
  
  CuttingSurface* Process::cuttingSurface() { return &p->cuttingSurface; }
  const CuttingSurface* Process::cuttingSurface() const { return &p->cuttingSurface; }
  
  const QString& Process::file() const { return p->filename; }
  
  QMap<QString, ProcessDefinition> processes;
  
//  static const ProcessDefinition* getProcessDefinition(
//      const QMap<QString, ProcessDefinition> &processes, const QString& processName)
//  {
//    if(!processes.contains(processName))
//      return 0;
//    const ProcessDefinition& def = processes[processName];
//    return &def;
//  }
  
  static ProcessDefinition* getProcessDefinition(
       QMap<QString, ProcessDefinition>& processes, const QString& processName)
  {
    if(!processes.contains(processName))
      return 0;
    ProcessDefinition& def = processes[processName];
    return &def;
  }
  
  ProcessDefinition* getProcessDefinition(const QString& processName)
  {
    return getProcessDefinition(processes, processName);
  }
  
  bool getProcessParms(const Process& proc, QStringList& parms) 
  {
    return getProcessParms(proc.name(), parms);
  }
  
  bool getProcessParms(const QString& processName, QStringList& parms)
  {
    ProcessDefinition* def = getProcessDefinition(processName);
    if(!def)
      return false;
    parms = def->parms;
    return true;
  }
  
  bool getDefaultParms(const Process& proc, QStringList& parms)
  {
    return getDefaultParms(proc.name(), parms);
  }
  
  bool getDefaultParms(const QString& processName, QStringList& parms)
  {
    SetupProcess setup;
    Process* p = setup.getProcess(processName);
    if(!p)
      return false;
    parms = p->parmDefaults();
    return true;
  }
  
  bool saveProcessParms(const Process& proc, const QStringList &parms)
  {
    return saveProcessParms(proc.name(), parms);
  }
  
  bool saveProcessParms(const QString& processName, const QStringList &parms)
  {
    ProcessDefinition* def = getProcessDefinition(processName);
    if(!def)
      return false;
    if(parms.size() != def->parms.size())
      return false;
    def->parms = parms;
    return true;
  }
  
  bool checkProcessParms(const Process& proc, const QStringList &parms, size_t *nbParms)
  {
    return checkProcessParms(proc.name(), parms, nbParms);
  }
  
  bool checkProcessParms(const QString& processName, const QStringList &parms, size_t *nbParms)
  {
    ProcessDefinition* def = getProcessDefinition(processName);
    if(!def)
      return false;
    if(nbParms)
      *nbParms = def->parmNames.size();
    if(parms.size() != def->parms.size())
      return false;
    return true;
  }
  
  Process* Process::getProcess(const QString& processName)
  {
    if(!processes.contains(processName))
      return 0;
    Process* proc = (*processes[processName].factory)(*this);
    ProcessDefinition* def = getProcessDefinition(processName);
    // Grab the parms from the GUI
    // RSS this assumes this is the only place the factory is called
    proc->setParms(def->parms);
    return proc;
  }
  
  QStringList listProcesses()
  {
    QStringList res;
  
    for(const ProcessDefinition &def : processes)
      res << def.name;
    return res;
  }

  bool getProcessText(const QString &text, QString &tab, QString &folder, QString &name)
  {
    // The tab is the first part upto "/" and the name is the last part after the last "/"
    // The rest is the folder and currently it should not be empty
    QStringList list = text.split("/", QString::SkipEmptyParts);
    if(list.size() < 3) {
      mdxInfo << "Invalid process tab/folder/name: " << text << endl;
      return false;
    }
    tab = list[0];
    name = list[list.size() - 1];
    list.removeFirst();
    list.removeLast();
    folder = list.join("/");

    return true;
  } 

  bool validProcessName(const QString& processName)
  {
    return processes.contains(processName);
  }

  bool Process::runProcess(const QString& processName, QStringList &parms) throw()
  {
    try {
      Process *proc = getProcess(processName);
      if(!proc)
        return setErrorMessage(QString("Error, cannot create process %1").arg(processName));
      if(!checkProcessParms(processName, parms))
        return setErrorMessage(
          QString("Error, incorrect parameters for process %1.%2").arg(processName));
 
      return runProcess(*proc, parms);
    }
    catch(std::exception& ex) {
      return setErrorMessage(
        QString("Exception caught while creating process %1: %2").arg(processName)
                                                     .arg(processName).arg(ex.what()));
    }
    catch(...) {
      return setErrorMessage(
        QString("Unknown exception caught while creating process %1.").arg(processName));
    }
  }
  
  bool Process::runProcess(Process& proc, QStringList &parms) throw()
  {
    try {
      // RSS this is a bit of a hack, should be called in GUI thread and we should pass QWidget if available
      proc.setParms(parms);
      if(!proc.initialize(0))
        return setErrorMessage(proc.errorMessage());
      if(!proc.run())
        return setErrorMessage(proc.errorMessage());
    }
    catch(QString& err) {
      return setErrorMessage(QString("Error in %1:\n%2").arg(proc.name()).arg(err));
    }
    catch(std::string& err) {
      return setErrorMessage(QString("Error in %1:\n%2").arg(proc.name()).arg(err.c_str()));
    }
    catch(std::exception& ex) {
      return setErrorMessage(QString("Error in %1:\n%2").arg(proc.name()).arg(ex.what()));
    }
    catch(...) {
      return setErrorMessage(QString("Error in %1:\nUnknown C++ exception").arg(proc.name()));
    }
    return true;
  }
  
//  #ifdef WIN32
//    struct mdx_EXPORT Registration;
//  #else
//    Registration::factoryList Registration::factories;
//  #endif

  Registration::Registration(ProcessFactoryPtr f, const char* class_name, 
                      unsigned int compiledVersion) : factory(), classname(class_name)
  {
    if(DEBUG) {
      mdxInfo << "Registration of process " << qdemangle(classname) 
          << " at address 0x"
                       //<< QString::number(f.id(), 16) << endl
                       << "    Type of data: " << qdemangle(typeid(*f).name()) << endl;
    }
    if(Process::processVersion != compiledVersion) {
      QTextStream err(stderr);
      err << "Error registering process " << qdemangle(class_name) << endl
          << " It has been compiled against a different revision of MorphoDynamX:" 
          << QString::number(compiledVersion)
          << endl << " current version:" << QString::number(Process::processVersion) << endl;
    } else {
      factory = f;
      if(!factory and DEBUG)
        mdxInfo << "Storing null factory!" << endl;
      processFactories() << factory;
    }
  }
  
  Registration::~Registration()
  {
    if(factory) {
      if(DEBUG) {
        mdxInfo << "Unregistration of process " << qdemangle(classname) 
           << " at address 0x"
           //<< QString::number(factory.id(), 16) << endl
           << "    Type of data: " << qdemangle(typeid(*factory).name()) << endl;
      }
      QList<ProcessFactoryPtr>& list = processFactories();
      int size_before = list.size();
      for(int i = 0; i < list.size(); ++i) {
        if(list[i] == factory) {
          list.removeAt(i);
          break;
        }
      }
      if(DEBUG and list.size() == size_before) {
        mdxInfo << "Error, could not find ProcessFactoryPtr to remove" << endl;
      }
    }
  } 

  #if defined(WIN32)
    Registration::factoryList Registration::factories
      = Registration::factoryList();
  #else
    Registration::factoryList Registration::factories;
  #endif
}
