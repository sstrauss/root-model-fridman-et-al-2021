//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef CC_DRAW_PARMS_HPP
#define CC_DRAW_PARMS_HPP

#include <Config.hpp>

#include <CCF.hpp>
#include <Attributes.hpp>
//#include <VizAttribute.hpp>
#include <ColorMap.hpp>

#include <GL.hpp>
#include <QString>
#include <Mangling.hpp>
#include <map>
#include <vector>
#include <Misc.hpp>

namespace mdx 
{
  // FIXME Make this text appear in Doxygen
  //
  // Since rendering for cell complexes is user-definable,
  // we can offer a lot of freedom in exactly what is displayed and how.
  // The UI for rendering a selected cell complex thus has two levels of selection:
  // /what/ is to be rendered, and /how/ it is to be rendered.
  //
  // A RenderChoice is the more fundamental of these two; it describes
  // a single family of rendered things.
  // This is essentially encapsulated by everything that is needed
  // to render something in OpenGL: a primitive type, colors, positions, normals,
  // texture coordinates, and so on.
  // Most are stored as QString identifiers that are mapped in CCDrawParms to arrays.
  struct RenderChoice
  {
    uint type;             // The OpenGL primitive: GL_POINTS, GL_LINES, or GL_TRIANGLES

    QString elements;      // The identifier of the OpenGL element array
    QString colors;        // The identifier of the OpenGL color array
    QString vertices;      // The identifier of the OpenGL vertex array
    QString normals;       // The identifier of the OpenGL normal array
    QString texCoord2D;    // The identifier of the OpenGL texture coordinate array
    QString clip;          // The identifier of the OpenGL array for clipping positions

    QString colorMap;      // The identifier of the associated MDX colormap (for colormap editor)

    float opacity;         // Opacity of GL_TRIANGLES
    float plSize;          // Size of GL_POINTS / width of GL_LINES
    Point2f polygonOffset; // The amount of glPolygonOffset that will be applied (if type is GL_TRIANGLES)

    RenderChoice() : opacity(0.0), plSize(1.0), polygonOffset(0.0, 0.0) {}
  };

  // Each RenderChoice is driven by a DrawChoice.
  // The DrawChoice class is how the modeler interacts with the renderer,
  // specifying things that are to be rendered.
  //
  // When the render process runs, each DrawChoice is turned into
  // a RenderChoice which is used by the UI for interaction and rendering.
  struct DrawChoice 
  {
    enum { DCRenderChoice,
           DCCellAttribute, DCLabelAttribute,
           DCLabel, DCSignal, DCVertexSignal, DCHeat, DCCellAxis, DCAxis,
           DCCustomVertex, DCCustomEdge, DCCustomFace, DCCustomVolume } dcType;

    // renderChoice is a RenderChoice corresponding to this DrawChoice.
    RenderChoice renderChoice;

    // If we are visualizing an attribute, we need its identifier
    // and its type (to feed to VizAttribute).
    QString attributeName;
    QString attributeType;
    
    DrawChoice() : dcType(DCRenderChoice), renderChoice() {}
    DrawChoice(const RenderChoice &rc) : dcType(DCRenderChoice), renderChoice(rc) {}
  };

  // A RenderGroup, on the other hand, describes /what/ to render.
  // It contains a map of DrawChoice / RenderChoices of /how/ to render that thing,
  // and its visibility as a group can be toggled off or on.
  struct RenderGroup
  {
    bool visible;                               // Is this group rendered at all?
    std::map<QString, DrawChoice> drawChoices;     // All draw choices for this group
    std::map<QString, RenderChoice> renderChoices; // The corresponding render choices
    QStringList choiceList;                     // The names of the possible render choices
    QString currentChoice;                      // The name of the current render choice
  };


  // CCDrawParms handles the parameters for drawing a single cell complex.
  // It contains not only drawing parameters (render groups, render choices, draw choices)
  // but also the associated colormaps, as well as the geometry and element arrays
  // which interface with OpenGL.
  class mdx_EXPORT CCDrawParms
  {
  public:
    // *
    // * First up are parameters which determine what aspects of the cell complex
    // * are to be rendered.
    // *

    // visible denotes the visibility of the entire cell complex;
    // if visible is false, the cell complex will not be drawn.
    bool visible = true;
    inline bool isVisible() const { return visible; }
    inline void setVisible(bool _visible = true) { visible = _visible; }

    // Render groups.  These are referenced by a render group name.
    typedef std::map<QString, RenderGroup> RenderGroups;
    RenderGroups renderGroups;
    RenderGroup &renderGroup(const QString &groupName);

    // groupList gets the list of render groups.
    QStringList groupList(void) const;

    // eraseGroup deletes the given render group.
    void eraseGroup(const QString &groupName);

    // clearGroups deletes all render groups.
    void clearGroups(void);

    // The render groups' visibility flags can be accessed through CCDrawParms.
    bool isGroupVisible(const QString &groupName) const;
    bool setGroupVisible(const QString &groupName, bool visible = true);
    bool setAllGroupsVisible(bool visible = true);


    // Draw choices. A choice is referenced by its name and the name of its parent group.
    // Draw choices can be created by the user and modified in any thread.
    DrawChoice &drawChoice(const QString &groupName, const QString &choiceName);

    // drawChoiceList gets the list of draw choices for a group.
    QStringList drawChoiceList(const QString &groupName) const;

    // eraseDrawChoice deletes the given draw choice.
    // It will be removed from the GUI menu on the next run of the renderer.
    void eraseDrawChoice(const QString &groupName, const QString &choiceName);

    // Some special legacy methods create simple draw choices, directly encapsulating
    // render choices, and insert them into given groups.
    // These are used by the renderer to create the default draw choices,
    // such as drawing vertices, faces by labels, and so on.
    DrawChoice &createPointChoice(const QString &groupName, const QString &choiceName,
                                  const QString &colorMapName,
                                  const QString &elements, const QString &colors,
                                  const QString &vertices, const QString &normals = QString())
    {
      return createDrawChoice(GL_POINTS, groupName, choiceName, colorMapName,
                              elements, colors, vertices, normals);
    }

    DrawChoice &createLineChoice(const QString &groupName, const QString &choiceName,
                                 const QString &colorMapName,
                                 const QString &elements, const QString &colors,
                                 const QString &vertices, const QString &normals = QString())
    {
      return createDrawChoice(GL_LINES, groupName, choiceName, colorMapName,
                              elements, colors, vertices, normals);
    }

    DrawChoice &createTriangleChoice(const QString &groupName, const QString &choiceName,
                                     const QString &colorMapName,
                                     const QString &elements, const QString &colors,
                                     const QString &vertices, const QString &normals = QString(), const QString &clip = QString())
    {
      return createDrawChoice(GL_TRIANGLES, groupName, choiceName, colorMapName,
                              elements, colors, vertices, normals, clip);
    }

    // The special methods are all instantiated by a single method which also takes
    // the type of the drawn element.
    DrawChoice &createDrawChoice(uint elementType,
                                 const QString &groupName, const QString &choiceName,
                                 const QString &colorMapName,
                                 const QString &elements, const QString &colors,
                                 const QString &vertices, const QString &normals = QString(), const QString &clip = "");

    // In the spirit of the legacy methods, we add other methods to create new DrawChoices.
    // A VertexInterpChoice is drawn on faces, interpolated between
    // the attribute's value on vertices.
    DrawChoice &createVertexInterpChoice(const QString &groupName, const QString &choiceName,
                                         const QString &colorMapName,
                                         const QString &attrName, const QString &attrType);

    // A face choice draws the attribute's value on faces.
    DrawChoice &createFaceChoice(const QString &groupName, const QString &choiceName,
                                 const QString &colorMapName,
                                 const QString &attrName, const QString &attrType);

    // A volume choice draws the attribute's value on volumes.
    DrawChoice &createVolumeChoice(const QString &groupName, const QString &choiceName,
                                   const QString &colorMapName,
                                   const QString &attrName, const QString &attrType);

    // A face label choice draws the label attribute's value on faces.
    DrawChoice &createFaceLabelChoice(const QString &groupName, const QString &choiceName,
                                      const QString &colorMapName,
                                      const QString &attrName, const QString &attrType);

    // A volume label choice draws the label attribute's value on volumes.
    DrawChoice &createVolumeLabelChoice(const QString &groupName, const QString &choiceName,
                                        const QString &colorMapName,
                                        const QString &attrName, const QString &attrType);


    // Render choices. A choice is referenced by its name and the name of its parent group.
    // Render choices are generated from draw choices by the renderer, and used only by the GUI.
    RenderChoice &renderChoice(const QString &groupName, const QString &choiceName);

    // renderChoiceList gets the list of render choices for a group.
    const QStringList &renderChoiceList(const QString &groupName) const;

    // currentRenderChoice gets the name of the current render choice for a group.
    const QString &currentRenderChoice(const QString &groupName) const;

    // setRenderChoice sets the name of the current render choice for a group.
    bool setRenderChoice(const QString &groupName, const QString &choiceName);


    // *
    // * Next are parameters which are used to relay what features have changed
    // * when the renderer is rerun.
    // *

    // changed is a bitset describing what has changed since the renderer was last run.
    // It determines which buffer objects must be recomputed at render time;
    // for instance, if only properties have changed, we usually only have to change
    // the color object, while if topology has changed we must regenerate everything.
    enum { PropertiesChanged      = 1,    // The cells' properties (signal, colors, etc.)
           PositionsChanged       = 1<<1, // The cells' geometry (positions, normals, etc.)
           TopologyChanged        = 1<<2, // The cells' topology (cells added, cells deleted, etc.)
           VertexSelectChanged    = 1<<3, // |
           EdgeSelectChanged      = 1<<4, // | Cells have been selected or unselected
           FaceSelectChanged      = 1<<5, // |
           VolumeSelectChanged    = 1<<6, // |
           SelectPositionsChanged = 1<<7, // Selected vertices have been moved
           FaceLabelChanged       = 1<<8, // |
           VolumeLabelChanged     = 1<<9, // | Labels have changed
           CellEdgesChanged       = 1<<10,// Cell edges
           DrawChoicesChanged     = 1<<11 // Draw choices
    };
    uint changed = TopologyChanged;

    // Vertices/edges/faces/volumes that need updating for selection.
    CCIndexTbbVec vertexChanged;
    CCIndexTbbVec edgeChanged;
    CCIndexTbbVec faceChanged;
    CCIndexTbbVec volumeChanged;

    // Selected vertices/edges/faces/volumes that need updating for selection 
    CCIndexTbbVec vertexSelected;
    CCIndexTbbVec edgeSelected;
    CCIndexTbbVec faceSelected;
    CCIndexTbbVec volumeSelected;


    // *
    // * Finally, we have parameters created during rendering. These include
    // * OpenGL buffers and buffer IDs, as well as some of the data used to create them.
    // *

    // The element, color, vertex, and normal arrays all have names.
    // Each name corresponds to a std::vector of data, as well as
    // (for objects pushed to the graphics card) an array ID.

    // Elements.
    std::map<QString, std::vector<uint> > elementEAs;
    std::vector<uint> &elementEA(const QString &name);

    // Colors.
    std::map<QString, std::vector<Colorb> > colorVAs;
    std::vector<Colorb> &colorVA(const QString &name);

    std::map<QString, uint> colorVAids;
    uint &colorVAid(const QString &name);

    // Vertex positions.
    std::map<QString, std::vector<Point3f> > vertexVAs;
    std::vector<Point3f> &vertexVA(const QString &name);

    std::map<QString, uint> vertexVAids;
    uint &vertexVAid(const QString &name);

    // Normals.
    std::map<QString, std::vector<Point3f> > normalVAs;
    std::vector<Point3f> &normalVA(const QString &name);

    std::map<QString, uint> normalVAids;
    uint &normalVAid(const QString &name);

    // Clipping positions
    std::map<QString, std::vector<Point3f> > clipVAs;
    std::vector<Point3f> &clipVA(const QString &name);

    std::map<QString, uint> clipVAids;
    uint &clipVAid(const QString &name);

    // In order to select faces and volumes, we have to draw in select mode
    // and use unique colours to identify objects. We need some extra buffers
    // for OpenGL to use in this case.
    std::vector<Point3GLub> uniqueColorVA;
    GLuint uniqueColorVAid = 0;
    Point3f *uniqueVertexVA = 0;
    GLvoid *uniqueVertexVAptr = 0;
    GLuint uniqueVertexVAid = 0;


    // clearBuffers deletes all OpenGL buffers and clears the buffer maps.
    void clearBuffers(void);


    // Number of vertices, edges, faces, and volumes.
    uint vertexCount = 0, edgeCount = 0, faceCount = 0, volumeCount = 0;

    // vaInverseMap contains pairs <n-cell, vertex> corresponding to vertex array elements;
    // contiguous pairs with the same n-cell contain the vertices used in drawing that cell.
    // Contains vertices, edges, faces, and volumes;
    // note however that vertex arrays don't contain edge info.
    std::vector< std::pair<CCIndex,CCIndex> > vaInverseMap;

    // faceStart and volumeStart are the indices into vaInverseMap where
    // face and volume entries begin, respectively.
    uint faceStart = 0, volumeStart = 0;

    // There are also maps from cell index to the start of its description in vaInverseMap.
    AttrMap<CCIndex, int> vertexMap;
    AttrMap<CCIndex, int> edgeMap;      // not used
    AttrMap<CCIndex, int> faceMap;

    // volumeFaces is a vector with one entry for each <volume,vertex> pair
    // in the vaInverseMap; it gives the particular face that that element
    // is drawn from, and is used to compute normals to the volume.
    std::vector<CCIndex> volumeFaces;

    // Constructor
    CCDrawParms() {}

    // We access color maps by name from the Mesh attributes.
    ColorMap &colorMap(const QString &name);

    Attributes *attributes = 0;
    void setAttributes(Attributes *attr) { attributes = attr; }

    // Required for attributes
    bool operator==(const CCDrawParms &) const
    {
      return false;
    }
  };

  // Attribute serialization for CCParms
  typedef AttrMap<QString, CCDrawParms> ParmsAttr;  

  // Read/write Cell data
  bool inline readAttr(CCDrawParms &c, const QByteArray &ba, size_t &pos) 
  {
    bool visible;
    readAttr(visible, ba, pos);
    c.setVisible(visible);

    size_t numGroups = 0;
    readAttr(numGroups, ba, pos);
    for(unsigned int i = 0 ; i < numGroups ; i++) {
      QString name, choice;
      bool draw = false;
      readAttr(name, ba, pos);
      readAttr(draw, ba, pos);
      readAttr(choice, ba, pos);

      c.setGroupVisible(name,draw);
      c.setRenderChoice(name,choice);
    }

    return true;
  }

  bool inline writeAttr(const CCDrawParms &c, QByteArray &ba)
  { 
    writeAttr(c.isVisible(), ba);

    QStringList groupNames = c.groupList();
    writeAttr(size_t(groupNames.size()), ba);
    for(QString &group : groupNames) {
      writeAttr(group, ba);
      writeAttr(c.isGroupVisible(group), ba);
      writeAttr(c.currentRenderChoice(group), ba);
    }

    return true;
  }

}
#endif
