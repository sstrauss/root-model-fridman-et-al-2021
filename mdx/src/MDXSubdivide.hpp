//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef MDX_SUBDIVIDE_HPP
#define MDX_SUBDIVIDE_HPP

#include <Config.hpp>
#include <Geometry.hpp>
#include <Attributes.hpp>
#include <CCF.hpp>
#include <Subdivide.hpp>
#include <Mesh.hpp>

namespace mdx 
{
  /// MDXSubdivide assigns properties after division for the base properties
  struct MDXSubdivide : public Subdivide
  {
    MDXSubdivide() {}
    MDXSubdivide(Mesh &mesh);
  
    void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct &ss, 
        CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(), double interpPos = 0.5);

    Mesh *mesh = 0;
    CCIndexDataAttr *indexAttr = 0;
    Subdivide *nextSubdivide = 0;

    std::vector<CCIndexDoubleAttr*> attrVec;
  };
}
#endif
