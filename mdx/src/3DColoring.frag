uniform int colorCount;
uniform sampler1D labelColormap;

uniform sampler3D tex;
uniform sampler1D colormap;
uniform float brightness;
uniform float opacity;

uniform sampler3D secondTex;
uniform sampler1D secondColormap;
uniform float secondBrightness;
uniform float secondOpacity;

uniform sampler1D labelCenters1;
uniform int labelCount1;
uniform sampler1D labelCenters2;
uniform int labelCount2;
uniform vec3 origin;
uniform vec3 imageSize;
uniform ivec3 texSize;
uniform ivec3 hasClipped;

int floatToLabel(float value)
{  
  value = value * 256.0;
  float low = floor(value + 0.5);
  float high = floor((value - low) * 256 + 1./512) * 256;
  return int(low + high);
}

vec4 baseIndexColor(float value)
{
  if(value == 0) 
    return vec4(0,0,0,0);
  float colIdx = (floatToLabel(value)+0.5)/float(colorCount);
  vec4 col = texture1D(labelColormap, colIdx);
  return col;
}

vec4 indexColor1(vec3 texCoord)
{
  float value = texture3D(tex, texCoord).a;
  vec4 col = baseIndexColor(value);
  col.rgb *= brightness;
  col.a *= opacity;
  return col;
}

vec4 indexColor2(vec3 texCoord)
{
  float value = texture3D(secondTex, texCoord).a;
  vec4 col = baseIndexColor(value);
  col.rgb *= secondBrightness;
  col.a *= secondOpacity;
  return col;
}

vec4 colormapColor1(vec3 texCoord)
{
  float value = texture3D(tex, texCoord).a;
  vec4 col = texture1D(colormap, value);
  col.rgb *= brightness;
  col.a *= opacity;
  return col;
}

vec4 colormapColor2(vec3 texCoord)
{
  float value = texture3D(secondTex, texCoord).a;
  vec4 col = texture1D(secondColormap, value);
  col.rgb *= secondBrightness;
  col.a *= secondOpacity;
  return col;
}

int getLabel1(vec3 texCoord)
{
  // Prefer work tex labels
  float value;
  value = texture3D(tex, texCoord).a;
  if(value == 0) 
    return 0;

  return floatToLabel(value);
}

int getLabel2(vec3 texCoord)
{
  // Prefer work tex labels
  float value;
  value = texture3D(secondTex, texCoord).a;
  if(value == 0) 
    return 0;

  return floatToLabel(value);
}

bool texInRange(vec3 texCoord)
{
  for(int i = 0; i < 3; i++)
    if(texCoord[i] < 0.0 || texCoord[i] > 1.0)
      return false;
  return true;
}

// Convert texture coordinates to real positions
vec4 texToRealPos(vec3 texCoord)
{
  vec3 pos = texCoord * imageSize + origin;
  vec4 realPos = gl_ModelViewMatrix * vec4(pos, 1);
  realPos /= realPos.w;
  return realPos;
}

// Simple shading
float doShading(vec4 vPos, vec4 cPos)
{
  vec3 n = normalize(vec3(vPos - cPos));
  float f = abs(dot(n, vec3(0.577, 0.577, 0.577)));
  f = f + abs(dot(n, vec3(-0.577, 0.577, 0.577)));

  return .7 + .15 * f;
}

// Compute intersection with clipping planes
bool isClipped(vec4 pos)
{
  for(int i = 0 ; i < 3 ; ++i) {
    if(hasClipped[i] != 0) {
      vec4 c0 = gl_ClipPlane[2*i];
      vec4 c1 = gl_ClipPlane[2*i+1];

      // If clipped
      if(dot(c0, pos) < 0 || dot(c1, pos) < 0)
        return true;
    }
  }
  return false;
}

void doRayClip(inout vec4 rayStart, inout vec4 rayEnd)
{
  // Compute intersection with clipping planes
  for(int i = 0 ; i < 3 ; ++i) {
    if(hasClipped[i] != 0) {
      vec4 c0 = gl_ClipPlane[2*i];
      vec4 c1 = gl_ClipPlane[2*i+1];

      float testS0 = dot(c0, rayStart);
      float testS1 = dot(c1, rayStart);
      float testE0 = dot(c0, rayEnd);
      float testE1 = dot(c1, rayEnd);

      if((testS0 < 0 && testE0 < 0) || (testS1 < 0 && testE1 < 0)) {
        discard;
        return;
      }
      else if((testS0 * testE0 < 0) ||
              (testS1 * testE1 < 0))
      {
        float lambda0 = testS0 / (testS0 - testE0);
        float lambda1 = testS1 / (testS1 - testE1);
        if(lambda0 > lambda1)
        {
          float a = lambda0;
          lambda0 = lambda1;
          lambda1 = a;
        }
        vec4 newStart = (lambda0 > 0) ? (1-lambda0)*rayStart + lambda0 * rayEnd : rayStart;
        vec4 newEnd = (lambda1 < 1) ? (1-lambda1)*rayStart + lambda1 * rayEnd : rayEnd;
        rayStart = newStart;
        rayEnd = newEnd;
      }
    }
  }
  return;
}

