//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "MorphoDynamX.hpp"
#include <QApplication>
#include <QString>
#include <QGLFormat>
#include <QDir>
#include <QFileInfo>
#include <QMetaType>
#include <QTimer>
#include "Information.hpp"
#include <QMessageBox>
#include <QStringBuilder>
#include "Dir.hpp"
#include "Thrust.hpp"
#include "Version.hpp"
#include "Random.hpp"

int main(int argc, char** argv)
{
  QCoreApplication::setOrganizationDomain("morphodynamx.org");
  QCoreApplication::setOrganizationName("MorphoDynamX");
  QCoreApplication::setApplicationName("MorphoDynamX");
  QCoreApplication::setApplicationVersion(VERSION);

  bool noGui = false;
  bool isDebug = false;
  bool showHelp = false;
  bool showAppdir = false;
  bool showProcessdir = false;
  bool showUserprocessdir = false;
  bool showAllprocessdir = false;
  bool showIncludedir = false;
  bool showLibsdir = false;
  bool showVersion = false;
  bool showResource = false;
  bool setModel = false;
  bool autoRun = false;
  QString filename, modelPath;
  QStringList addLibraries;

  for(int i = 1; i < argc; ++i) {
    QString arg = QString::fromLocal8Bit(argv[i]);
    if(arg == "--debug")
      isDebug = true;
    else if((arg == "--help")or (arg == "-h")) {
      showHelp = true;
      noGui = true;
      break;
    } else if(arg == "--dir") {
      showAppdir = true;
      noGui = true;
      break;
    } else if(arg == "--process") {
      showProcessdir = true;
      noGui = true;
      break;
    } else if(arg == "--user-process") {
      showUserprocessdir = true;
      noGui = true;
      break;
    } else if(arg == "--all-process") {
      showAllprocessdir = true;
      noGui = true;
      break;
    } else if(arg == "--include") {
      showIncludedir = true;
      noGui = true;
      break;
    } else if(arg == "--lib") {
      showLibsdir = true;
      noGui = true;
      break;
    } else if(arg == "--version") {
      showVersion = true;
      noGui = true;
      break;
    } else if(arg == "--resource") {
      showResource = true;
      noGui = true;
      break;
    } else if(arg == "--model") {
      setModel = true;
      if(++i < argc)
        modelPath = QString::fromLocal8Bit(argv[i]);
      else {
        showHelp = true;
        break;
      }
    } else if(arg == "--addlibrary") {
      if(++i < argc) {
	      QString libToAdd = QString::fromLocal8Bit(argv[i]);
	      addLibraries << libToAdd;
      }
      else {
        showHelp = true;
        break;
      }
    } else if(arg == "--run") {
      autoRun = true;
    }
    else if(filename.isEmpty()) {
      filename = arg;
      QString currdir = QFileInfo(filename).dir().path();
      if(currdir.length() > 0)
        mdx::setCurrentPath(currdir);
    }
  }

  if(showHelp) { std::cout 
    << "Usage: MorphoDynamX [<Command>] | [--debug] [--addlibrary LibraryPath] [--model ModelPath [--run]] [FILE.mdxv]\n"
       "  Flags:\n"
       "    --debug        - Launch MorphoDynamX in debug mode\n"
       "    --model        - set the model/process\n"
       "    --run          - runs the model/process automatically\n"
       "    --addlibrary   - adds a process library to MorphoDynamX\n"
       "  Commands:\n"
       "    --dir          - Print the application directory and exit\n"
       "    --process      - Print the process directory and exit\n"
       "    --user-process - Print the user process directory and exit\n"
       "    --all-process  - Print all the directories searched for processes and exit\n"
       "    --resource     - Print the resource directory and exit\n"
       "    --include      - Print the include directory and exit\n"
       "    --lib          - Print the library directory and exit\n"
       "    --version      - Display the version and revision and exit\n"
       "    --help |-h     - Print this help" << std::endl;
    return 0;
  } 

  QCoreApplication* app = 0;
  if(noGui)
    app = new QCoreApplication(argc, argv);
  else
    app = new QApplication(argc, argv);

  qInstallMessageHandler(mdx::Information::msgHandler);

  QTextStream out(stdout);

  if(showAppdir) {
    out << QCoreApplication::applicationDirPath() << endl;
    return 0;
  } else if(showProcessdir) {
    // FIXME: this only shows one arbitrary path
    out << mdx::processDirs()[0].absolutePath() << endl;
    return 0;
  } else if(showUserprocessdir) {
    // FIXME: this only shows one arbitrary path
    out << mdx::userProcessDirs()[0].absolutePath() << endl;
    return 0;
  } else if(showAllprocessdir) {
    QList<QDir> dirs = mdx::processDirs();
    forall(const QDir& dir, dirs)
      out << dir.absolutePath() << endl;
    return 0;
  } else if(showIncludedir) {
    out << mdx::includeDir().path() << endl;
    return 0;
  } else if(showLibsdir) {
    out << mdx::libDir().path() << endl;
    return 0;
  } else if(showResource) {
    out << mdx::resourceDir().path() << endl;
    return 0;
  } else if(showVersion) {
    out << "MorphoDynamX version " VERSION " rev. " REVISION << endl;
    return 0;
  }

  // Unfortunately, once you turn multisampling on, it seems to be impossible to turn it off.
  // This screws up selection by color.
  QGLFormat format = QGLFormat::defaultFormat();
  format.setSampleBuffers(false);
  //format.setVersion(3,0);
  QGLFormat::setDefaultFormat(format);

  // Register extra meta types
  qRegisterMetaType<floatList>("floatList");
  qRegisterMetaTypeStreamOperators<floatList>("floatList");
  qRegisterMetaType<QStringList>("QStringList");

  mdx::DEBUG = isDebug;
  mdxInfo << "Welcome to MorphoDynamX!" << endl << endl;
  mdxInfo << "Thrust host evaluation: " << THRUST_TAG << endl << endl;

  // Initialize random numbers generator
  mdx::sran_time();

  MorphoDynamX* gui = new MorphoDynamX(QCoreApplication::applicationDirPath(), addLibraries);

  gui->setDebug(isDebug);
  gui->show();
  if(filename.toLower().endsWith(".mdxv") or filename.toLower().endsWith(".mgxv"))
    gui->fileOpen(filename);
  else {
    gui->setLoadFile(filename);
    QObject::connect(gui, SIGNAL(processFinished()), gui, SLOT(autoOpen()));
    gui->fileOpen("");
  }
  if(setModel) {
    gui->setModelPath(modelPath);
    if(autoRun) {
      QObject::connect(gui, SIGNAL(processFinished()), gui, SLOT(autoRun()));
    }
  } else if(autoRun)
    mdxInfo << "You cannot use --run without specifying the starting --model" << endl;

  int result = 1;
  try {
    result = app->exec();
  } catch(const QString& str) {
    mdxInfo << "Unhandled exception: " << str << endl;
  }
  if(!result)
    mdxInfo << "Bye bye!" << endl;
  else
    mdxInfo << "Error on exit" << endl;
 
  QApplication::exit(result);

#ifndef _WIN32
  sleep(2);
#endif

  return result;
}
