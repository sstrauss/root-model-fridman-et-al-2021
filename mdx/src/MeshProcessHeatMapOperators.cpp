//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessHeatMapOperators.hpp>

#include <Triangulate.hpp>
#include <QFileDialog>
#include <QMessageBox>

#include "ui_HeatMapCombine.h"

#include <cmath>

namespace mdx 
{
 
//  bool DeleteHeatRangeLabels::run(Mesh* mesh, bool rescale, bool deleteCells, float min, float max)
//  {
//    Point2f& heatMapBounds = mesh->heatMapBounds();
//    if(isNan(min))
//      min = heatMapBounds[0];
//    if(isNan(max))
//      max = heatMapBounds[1];
//    if(min >= max) {
//      setErrorMessage("Error, the minimum must be strictly less than the maximum");
//      return false;
//    }
//    IntFloatAttr& heatAttr = mesh->heatAttr();
//    if(heatAttr.empty())
//      return true;
//    vvGraph& S = mesh->graph();
//    std::vector<vertex> T;
//
//    float vmin = 1, vmax = 0;
//
//    IntSet deleteLabel;
//    for(IntFloatAttr::iterator it = heatAttr.begin(); it != heatAttr.end(); ++it) {
//      float v = it->second;
//      if(v >= min and v <= max)
//        deleteLabel.insert(it->first);
//      else if(rescale) {
//        if(v < vmin)
//          vmin = v;
//        if(v > vmax)
//          vmax = v;
//      }
//    }
//    if(deleteLabel.empty())
//      return true;
//
//    if(rescale and vmin < vmax)
//      heatMapBounds = Point2f(vmin, vmax);
//
//    forall(const vertex& v, S) {
//      if(deleteLabel.count(v->label)) {
//        if(deleteCells)
//          T.push_back(v);
//      }
//    }
//    forall(const vertex& v, T)
//        S.erase(v);
//    forall(int label, deleteLabel)
//        heatAttr.erase(label);
//
//    if(deleteCells) {
//      // At last, erase all vertices not part of a triangle
//      do {
//        T.clear();
//        forall(const vertex& v, S) {
//          if(S.valence(v) < 2)
//            T.push_back(v);
//          else if(S.valence(v) == 2) {
//            const vertex& n = S.anyIn(v);
//            const vertex& m = S.nextTo(v, n);
//            if(!S.edge(n, m))
//              T.push_back(v);
//          }
//        }
//        forall(const vertex& v, T)
//            S.erase(v);
//      } while(!T.empty());
//      mesh->updateLines();
//    }
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(DeleteHeatRangeLabels);


  // Get measures from processes and pre-calculated ones from attributes
  QStringList getAllMeasures(Mesh* mesh)
  {
    // find measure processes
    QStringList procs = mdx::listProcesses();

    QStringList addProcs;
    std::set<QString> procsSet;
    QStringList addAttr;
    forall(const QString& name, procs) {
      mdx::ProcessDefinition* def = mdx::getProcessDefinition(name);
      QStringList list = def->name.split("/");
      QString subfolder = "Measures";

      if(list.size() >= 5 and list[0] == "Mesh" and list[1] == "Heat Map" and list[2].startsWith("Measure")){
        QString newProc = list[2] + "/" + list[3] + "/" + list[4]; // RSS FIXME only one level of subfolders? 
        addProcs << newProc;
        procsSet.insert(newProc);
      }
    }

    // Now the attr maps
    for(auto &name : mesh->heatAttrList("Labels", "Double")) {
      if(procsSet.find(name) == procsSet.end()){
        addProcs << name;
      }
    }

    return addProcs;
  }

  void fillTreeWidgetWithMeasures(Mesh* mesh, QTreeWidget* tree)
  {
    QStringList allProcs = getAllMeasures(mesh);

    forall(QString s, allProcs) {
      QTreeWidgetItem *itemNew = new QTreeWidgetItem(QStringList() << s);
      itemNew->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
      tree->addTopLevelItem(itemNew);
    }
  }

  // FIXME what about labelings?
  void fillTreeWidgetWithHeatMaps(Mesh &mesh, QTreeWidget* tree)
  {
    QStringList allHeatMaps = mesh.heatAttrList();

    forall(QString s, allHeatMaps) {
      QTreeWidgetItem *itemNew = new QTreeWidgetItem(QStringList() << s);
      itemNew->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
      tree->addTopLevelItem(itemNew);
    }
  }

  bool HeatMapCombine::initialize(QWidget *parent)
  {
    Ui_HeatMapCombineDialog ui;
    QDialog dlg(parent);
    ui.setupUi(&dlg);
    Mesh* m = currentMesh();

    ui.combinationGroup->setChecked(stringToBool(parm("Combination")));
    ui.transformationGroup->setChecked(stringToBool(parm("Transformation")));
    ui.combinedName->setText(parm("Heat Map Combine"));
    int index = ui.combinationType->findText(parm("Combination Type"));
    if(index < 0)
      throw QString("%1 Invalid Combine Type Index").arg(name());
    ui.combinationType->setCurrentIndex(index);

    fillTreeWidgetWithHeatMaps(*m, ui.mapTree);
    fillTreeWidgetWithHeatMaps(*m, ui.mapTree2);

    // fill gui with current parameter
    QTreeWidgetItemIterator it(ui.mapTree);
    while (*it) {
      if ((*it)->text(0) == parm("Heat Map 1"))
        (*it)->setSelected(true);
      ++it;
    }

    QTreeWidgetItemIterator it2(ui.mapTree2);
    while (*it2) {
      if ((*it2)->text(0) == parm("Heat Map 2"))
        (*it2)->setSelected(true);
      ++it2;
    }
    if(dlg.exec() == QDialog::Accepted) {
      if(ui.mapTree->currentItem()) 
        setParm("Heat Map 1", ui.mapTree->currentItem()->text(0));
      if(ui.mapTree2->currentItem()) 
        setParm("Heat Map 2", ui.mapTree2->currentItem()->text(0));
      setParm("Combination", ui.combinationGroup->isChecked() ? "Yes" : "No");
      setParm("Combination Type", ui.combinationType->currentText());
      setParm("Transformation", ui.transformationGroup->isChecked() ? "Yes" : "No");
      setParm("Transformation Type", ui.transformationType->currentText());
      setParm("Transformation Type 2", ui.transformationType2->currentText());
      setParm("Ignore Parent", ui.transformationIgnoreParent->text());
      setParm("Lower Threshold", QString::number(ui.transformationLower->value()));
      setParm("Upper Threshold", QString::number(ui.transformationUpper->value()));
      setParm("Heat Map Combine", ui.combinedName->text());
      return true;

    } else 
      return false;
  }

  double minVectorValue(std::vector<double> &values)
  {
    if(values.size() == 0) 
      return 0;

    double res = HUGE_VAL;

    for(double v : values)
      if(v < res) 
        res = v;

    return res;
  }

  double maxVectorValue(std::vector<double> &values)
  {
    if(values.size() == 0) 
      return 0;

    double res = -HUGE_VAL;

    for(double v : values)
      if(v > res) 
        res = v;

    return res;
  }

  double medianVectorValue(std::vector<double> values)
  {
    if(values.size() == 0) 
      return 0;

    double res = 0.;
    sort(values.begin(), values.end());

    // Average if even number
    if (values.size() % 2 == 0)
      res = (values[values.size() / 2 - 1] + values[values.size() / 2]) / 2;
    else
      res = values[values.size() / 2];

    return res;
  }

  double meanVectorValue(std::vector<double> &values)
  {
    if(values.size() == 0) 
      return 0;

    double res = 0.;

    for(double v : values)
      res += v;

    return res/values.size();
  }

  double varianceVectorValue(std::vector<double> &values)
  {
    if(values.size() == 0) 
      return 0;

    double mean = 0.;

    for(double v : values)
      mean += v;
    mean /= values.size();

    double res = 0;

    forall(double v, values)
      res += (mean - v) * (mean - v);

    return res;
  }

  double extractValue(QString type, std::vector<double> &values) {

    if(type == "Mean") {
      return meanVectorValue(values);
    } else if(type == "Median"){
      return medianVectorValue(values);
    } else if(type == "Variance"){
      return varianceVectorValue(values);
    } else if(type == "Minimum"){
      return minVectorValue(values);
    } else if(type == "Maximum"){
      return maxVectorValue(values);
    } else if(type == "Max-Min"){
      return maxVectorValue(values) - minVectorValue(values);
    } else if(type == "Max/Min"){
      return maxVectorValue(values) / minVectorValue(values);
    }
    return 0.0;
  }

  bool HeatMapCombine::run(Mesh &m, const CCStructure &cs, const CCIndexDataAttr &indexAttr, QString heatName1, QString heatName2, 
      bool combination, QString combinationType, bool transformation, QString transformationType, QString transformationType2, int ignoreParent, 
      double lowerT, double upperT, QString heatNameComb)
  {
    auto &heatAttr1 = m.heatAttr<double>(heatName1);

    if(combination) {
      auto &heatAttr1 = m.heatAttr<double>(heatName1);
      auto &heatAttr2 = m.heatAttr<double>(heatName2);

      if(heatNameComb.isEmpty())
        heatNameComb = heatName1 + " " + combinationType + " " + heatName2;
      auto &heatAttrComb = m.heatAttr<double>(heatNameComb);
      heatAttrComb.clear();

      forall(IntDoublePair p, heatAttr1){
        if(combinationType == "Add"){ // FIXME this will create emtpy entries in the maps.
          heatAttrComb[p.first] = p.second + heatAttr2[p.first];
        } else if(combinationType == "Subtract"){
          heatAttrComb[p.first] = p.second - heatAttr2[p.first];
        } else if(combinationType == "Multiply"){
          heatAttrComb[p.first] = p.second * heatAttr2[p.first];
        } else if(combinationType == "Divide"){
          heatAttrComb[p.first] = p.second / heatAttr2[p.first];
        } else if(combinationType == "Power"){
          heatAttrComb[p.first] = pow(p.second, heatAttr2[p.first]);
        }
      }
      m.setHeatBounds(calcBounds(heatAttrComb), heatNameComb);
      m.setHeat(heatNameComb);

    } else { // transformation
      if(heatNameComb.isEmpty())
        heatNameComb = heatName1 + " Trans " + transformationType;
      auto &heatAttrComb = m.heatAttr<double>(heatNameComb);
      heatAttrComb.clear();

      if(transformationType == "Parent Label"){
        std::map<int, std::set<int> > parentLabelMap;
        typedef std::pair<int, std::set<int> > IntSetIntP;
        std::map<int, double> parentValues;

        for(CCIndex f : cs.faces()){
          int label = indexAttr[f].label;
          if(m.labelMap("Parents")[label] < 1)
            continue;
          parentLabelMap[m.labelMap("Parents")[label]].insert(label);
        }

        forall(IntSetIntP p, parentLabelMap){
          std::vector<double> values;
          forall(int l, p.second){
            if(heatAttr1[l] >= lowerT and heatAttr1[l] <= upperT and m.labelMap("Parents")[p.first] != ignoreParent and m.labelMap("Parents")[l] != ignoreParent){
              values.push_back(heatAttr1[l]);
            }
          }
          parentValues[p.first] = extractValue(transformationType2, values);

          forall(int l, p.second){
            heatAttrComb[l] = parentValues[p.first];
          }
        }

      } else {
        // create neighbor graph
        typedef std::pair<int, int> IntInt;
        std::map<int, std::set<int> > cellNeighborMap;
        std::map<IntInt, double> neighMap;
        typedef std::pair<IntInt, double> IntIntDoubleP;
        typedef std::pair<int, std::set<int> > IntSetIntP;
        neighborhood2D(m, cs, indexAttr, neighMap);

        for(IntIntDoubleP p : neighMap){
          cellNeighborMap[p.first.first].insert(p.first.second);
        }

        forall(IntSetIntP p, cellNeighborMap){
          std::vector<double> values;
          forall(int l, p.second){
            if(heatAttr1[l] >= lowerT and heatAttr1[l] <= upperT and m.labelMap("Parents")[p.first] != ignoreParent and m.labelMap("Parents")[l] != ignoreParent){
              values.push_back(heatAttr1[l]);
            }
          }
          heatAttrComb[p.first] = extractValue(transformationType2, values);
        }

      }
      m.setHeatBounds(calcBounds(heatAttrComb), heatNameComb);
      m.setHeat(heatNameComb);
    }
    m.updateProperties();

    return true;
  }
  REGISTER_PROCESS(HeatMapCombine);

  bool HeatMapAverage::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh) 
      throw QString("%1::run No current mesh").arg(name());

    QString ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw QString("%1::run No cell complex").arg(name());

    QString heatName = parm("Heat Name");
    if(heatName.isEmpty())
      heatName = mesh->heat();
    if(heatName.isEmpty())
      throw QString("%1:run Heat Name is empty").arg(name());

    QString labeling = parm("Labeling");
    if(labeling.isEmpty())
      labeling = mesh->labeling();
    if(labeling.isEmpty())
      throw QString("%1:run Labeling is empty").arg(name());
    if(labeling == "Labels")
      throw QString("%1:run Labeling for grouping cannot be 'Labels'").arg(name()).arg(labeling);
    if(!mesh->labelingExists(labeling))
      throw QString("%1:run Labeling (%2) does not exist").arg(name()).arg(labeling);

    if(!mesh->heatExists(heatName, "Labels"))
      throw QString("%1:run Heat (%2) does not exist for labeling 'Labels'").arg(name()).arg(heatName);
    if(mesh->heatType(heatName, "Labels") != "Double")
      throw QString("%1:run Heat (%2) for labeling 'Labels' has type (%3), must be Double")
                                   .arg(name()).arg(heatName).arg(mesh->heatType(heatName, "Labels"));

    const auto &labelingMap = mesh->labelMap(labeling);
    if(labelingMap.size() == 0)
      throw QString("%1:run Labeling (%2) is empty").arg(name()).arg(labeling);

    auto &heatAttr = mesh->heatAttr<double>(heatName, "Labels");
    if(heatAttr.size() == 0)
      throw QString("%1:run Heat (%2) for labeling 'Labels' is empty").arg(name()).arg(heatName);

    QString labelingHeatName = heatName + " Average";
    auto &labelingAttr = mesh->heatAttr<double>(labelingHeatName, labeling);
    labelingAttr.clear();
    IntIntAttr countAttr;
    for(const auto &pr : heatAttr) {
      int label = pr.first;
      if(label == 0)
        continue;
      int labelG = labelingMap[label];
      if(labelG == 0)
        continue;
      labelingAttr[labelG] += heatAttr[label];
      countAttr[labelG]++;
    }
    for(auto &pr : labelingAttr)
      pr.second /= double(countAttr[pr.first]);
      
    mesh->setHeatBounds(calcBounds(labelingAttr), labelingHeatName, labeling);
    mesh->updateProperties(ccName);
    mesh->setLabeling(labeling);
    mesh->setHeat(labelingHeatName);
    mesh->drawParms(ccName).setRenderChoice("Faces", labelingHeatName);

    return true;
  }
  REGISTER_PROCESS(HeatMapAverage);

  bool HeatMapPercentile::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh) 
      throw QString("%1::run No current mesh").arg(name());

    QString ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw QString("%1::run No cell complex").arg(name());

    QString heatName = parm("Heat Name");
    if(heatName.isEmpty())
      heatName = mesh->heat();
    if(heatName.isEmpty())
      throw QString("%1:run Heat Name is empty").arg(name());

    QString labeling = parm("Labeling");
    if(labeling.isEmpty())
      labeling = mesh->labeling();
    if(labeling.isEmpty())
      throw QString("%1:run Labeling is empty").arg(name());
    if(labeling == "Labels")
      throw QString("%1:run Labeling for grouping cannot be 'Labels'").arg(name()).arg(labeling);
    if(!mesh->labelingExists(labeling))
      throw QString("%1:run Labeling (%2) does not exist").arg(name()).arg(labeling);

    if(!mesh->heatExists(heatName, "Labels"))
      throw QString("%1:run Heat (%2) does not exist for labeling 'Labels'").arg(name()).arg(heatName);
    if(mesh->heatType(heatName, "Labels") != "Double")
      throw QString("%1:run Heat (%2) for labeling 'Labels' has type (%3), must be Double")
                                   .arg(name()).arg(heatName).arg(mesh->heatType(heatName, "Labels"));

    const auto &labelingMap = mesh->labelMap(labeling);
    if(labelingMap.size() == 0)
      throw QString("%1:run Labeling (%2) is empty").arg(name()).arg(labeling);

    auto &heatAttr = mesh->heatAttr<double>(heatName, "Labels");
    if(heatAttr.size() == 0)
      throw QString("%1:run Heat (%2) for labeling 'Labels' is empty").arg(name()).arg(heatName);

    double percentile = parm("Percentile").toDouble();
    if(percentile < 0.0 or percentile > 100.0)
      throw QString("%1:run Percentile must be between 0 and 100").arg(name());
    percentile /= 100.0;

    QString labelingHeatName = QString("%1 %2 Percentile").arg(heatName).arg(int(100.0 * percentile));
    auto &labelingAttr = mesh->heatAttr<double>(labelingHeatName, labeling);
    labelingAttr.clear();

    // Sort the heat values
    // Note this goes by labels, not faces
    AttrMap<int, std::set<double> > measureMap;
    for(const auto &pr : heatAttr) {
      int label = pr.first;
      if(label == 0)
        continue;
      int labelG = labelingMap[label];
      if(labelG == 0)
        continue;
      measureMap[labelG].insert(pr.second); 
    }

    // Get the percentile
    for(auto &pr : measureMap) {
      int pos = double(pr.second.size() - 1) * percentile + 0.5;
      mdxInfo << QString("Using cell %1 of %2 - %4").arg(pos).arg(pr.second.size()).arg(pr.first) << endl;
      auto p = pr.second.begin();
      while(pos-- > 0)
        p++;
      labelingAttr[pr.first] = *p;
    }
    mesh->setHeatBounds(calcBounds(labelingAttr), labelingHeatName, labeling);
    mesh->updateProperties(ccName);
    mesh->setLabeling(labeling);
    mesh->setHeat(labelingHeatName);
    mesh->drawParms(ccName).setRenderChoice("Faces", labelingHeatName);

    return true;
  }
  REGISTER_PROCESS(HeatMapPercentile);
}
