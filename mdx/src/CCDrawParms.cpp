//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CCDrawParms.hpp>
#include <Mesh.hpp>
#include <Shader.hpp>

namespace mdx
{
  // CCDrawParms methods
  //
  void CCDrawParms::clearBuffers()
  {
    // Check if in GUI thread
    if(!isGuiThread())
      throw(QString("CCDrawParms::clearBuffers called outside Gui thread"));

    // Clear openGL buffers
    for(auto iter = colorVAids.begin(); iter != colorVAids.end(); iter++) {
      uint &id = iter->second;
      if(id > 0 and glfuncs->glIsBuffer(id))
        glfuncs->glDeleteBuffers(1, &id);
      id = 0;
    }
    for(auto iter = vertexVAids.begin(); iter != vertexVAids.end(); iter++) {
      uint &id = iter->second;
      if(id > 0 and glfuncs->glIsBuffer(id))
        glfuncs->glDeleteBuffers(1, &id);
      id = 0;
    }
    for(auto iter = normalVAids.begin(); iter != normalVAids.end(); iter++) {
      uint &id = iter->second;
      if(id > 0 and glfuncs->glIsBuffer(id))
        glfuncs->glDeleteBuffers(1, &id);
      id = 0;
    }
    for(auto iter = clipVAids.begin(); iter != clipVAids.end(); iter++) {
      uint &id = iter->second;
      if(id > 0 and glfuncs->glIsBuffer(id))
        glfuncs->glDeleteBuffers(1, &id);
      id = 0;
    }
    // Clear unique color buffer pointers
    if(uniqueColorVAid > 0 and glfuncs->glIsBuffer(uniqueColorVAid))
      glfuncs->glDeleteBuffers(1, &uniqueColorVAid);
    uniqueColorVAid = 0;

    uniqueColorVA.clear();
    uniqueVertexVAid = 0; // These must be in one of the render choices
    uniqueVertexVA = 0;

    // Clear array maps
    elementEAs.clear();

    colorVAs.clear();
    colorVAids.clear();

    vertexVAs.clear();
    vertexVAids.clear();

    normalVAs.clear();
    normalVAids.clear();

    clipVAs.clear();
    clipVAids.clear();
  }

  RenderGroup &CCDrawParms::renderGroup(const QString &groupName)
  {
    return renderGroups[groupName];
  }

  void CCDrawParms::clearGroups()
  {
    renderGroups.clear();
  }

  // Get the list of render groups
  QStringList CCDrawParms::groupList(void) const
  {
    QStringList answer;
    for(RenderGroups::const_iterator iter = renderGroups.begin() ;
        iter != renderGroups.end() ; iter++)
      answer.push_back(iter->first);
    return answer;
  }
  // Get the list of render choices for a group
  const QStringList &CCDrawParms::renderChoiceList(const QString &groupName) const
  {
    static QStringList emptyList;
    RenderGroups::const_iterator iter = renderGroups.find(groupName);
    if(iter == renderGroups.end())
      return emptyList;
    else
      return iter->second.choiceList;
  };
  // Get the current render choice
  const QString &CCDrawParms::currentRenderChoice(const QString &groupName) const
  {
    static QString emptyString;
    RenderGroups::const_iterator iter = renderGroups.find(groupName);
    if(iter == renderGroups.end())
      return emptyString;
    else
      return iter->second.currentChoice;
  }
  // Set the current choice
  bool CCDrawParms::setRenderChoice(const QString &groupName, const QString &choiceName)
  {
    renderGroups[groupName].currentChoice = choiceName;
    // Return false if it doesn't exist, but set it anyway
    return renderGroups[groupName].renderChoices.find(choiceName) !=
           renderGroups[groupName].renderChoices.end();
  }
  // Render choice visibility
  bool CCDrawParms::isGroupVisible(const QString &groupName) const
  {
    RenderGroups::const_iterator iter = renderGroups.find(groupName);
    if(iter != renderGroups.end())
      return iter->second.visible;

    return false;
  }
  bool CCDrawParms::setGroupVisible(const QString &groupName, bool visible)
  {
    // We'll do this because flags may be loaded before choices are created
    renderGroups[groupName].visible = visible;

    return visible;
  }
  bool CCDrawParms::setAllGroupsVisible(bool visible)
  {
    forall(RenderGroups::value_type &pr, renderGroups)
      pr.second.visible = visible;

    return visible;
  }

  DrawChoice &CCDrawParms::createDrawChoice(uint type,
                                            const QString &groupName, const QString &choiceName,
                                            const QString &colorMapName,
                                            const QString &elements, const QString &colors,
                                            const QString &vertices, const QString &normals, const QString &clip)
  {
    RenderGroup &rg = renderGroups[groupName];

    DrawChoice &dc = rg.drawChoices[choiceName];
    dc.dcType = DrawChoice::DCRenderChoice;

    RenderChoice &rc = dc.renderChoice;
    rc.type = type;
    rc.elements = elements;
    rc.colors = colors;
    rc.vertices = vertices;
    rc.normals = normals;
    rc.colorMap = colorMapName;
    rc.clip = clip;

    changed |= DrawChoicesChanged;

    return drawChoice(groupName, choiceName);
  }

  DrawChoice &CCDrawParms::createVertexInterpChoice(const QString &groupName, const QString &choiceName,
                                                    const QString &colorMapName,
                                                    const QString &attrName, const QString &attrType)
  {
    QString colorBufferName(groupName+"#"+choiceName);
    DrawChoice &dc = createDrawChoice(GL_TRIANGLES, groupName, choiceName, colorMapName,
                                      "FaceVertex",colorBufferName,"Vertex","Normal");
    dc.dcType = DrawChoice::DCCellAttribute;
    dc.attributeName = attrName;
    dc.attributeType = attrType;
    return dc;
  }

  DrawChoice &CCDrawParms::createFaceChoice(const QString &groupName, const QString &choiceName,
                                            const QString &colorMapName,
                                            const QString &attrName, const QString &attrType)
  {
    QString colorBufferName(groupName+"#"+choiceName);
    DrawChoice &dc = createDrawChoice(GL_TRIANGLES, groupName, choiceName, colorMapName,
                                      "Face",colorBufferName,"Vertex","Normal");
    dc.dcType = DrawChoice::DCCellAttribute;
    dc.attributeName = attrName;
    dc.attributeType = attrType;
    return dc;
  }

  DrawChoice &CCDrawParms::createVolumeChoice(const QString &groupName, const QString &choiceName,
                                              const QString &colorMapName,
                                              const QString &attrName, const QString &attrType)
  {
    QString colorBufferName(groupName+"#"+choiceName);
    DrawChoice &dc = createDrawChoice(GL_TRIANGLES, groupName, choiceName, colorMapName,
                                      "Volume",colorBufferName,"Vertex","Normal");
    dc.dcType = DrawChoice::DCCellAttribute;
    dc.attributeName = attrName;
    dc.attributeType = attrType;
    return dc;
  }

  DrawChoice &CCDrawParms::createFaceLabelChoice(const QString &groupName, const QString &choiceName,
                                                 const QString &colorMapName,
                                                 const QString &attrName, const QString &attrType)
  {
    QString colorBufferName(groupName+"#"+choiceName);
    DrawChoice &dc = createDrawChoice(GL_TRIANGLES, groupName, choiceName, colorMapName,
                                      "Face",colorBufferName,"Vertex","Normal");
    dc.dcType = DrawChoice::DCLabelAttribute;
    dc.attributeName = attrName;
    dc.attributeType = attrType;
    return dc;
  }

  DrawChoice &CCDrawParms::createVolumeLabelChoice(const QString &groupName, const QString &choiceName,
                                                   const QString &colorMapName,
                                                   const QString &attrName, const QString &attrType)
  {
    QString colorBufferName(groupName+"#"+choiceName);
    DrawChoice &dc = createDrawChoice(GL_TRIANGLES, groupName, choiceName, colorMapName,
                                      "Volume",colorBufferName,"Vertex","Normal");
    dc.dcType = DrawChoice::DCLabelAttribute;
    dc.attributeName = attrName;
    dc.attributeType = attrType;
    return dc;
  }


  RenderChoice &CCDrawParms::renderChoice(const QString &groupName, const QString &choiceName)
  {
    return renderGroups[groupName].renderChoices[choiceName];
  }

  DrawChoice &CCDrawParms::drawChoice(const QString &groupName, const QString &choiceName)
  {
    return renderGroups[groupName].drawChoices[choiceName];
  }

  QStringList CCDrawParms::drawChoiceList(const QString &groupName) const
  {
    QStringList choiceList;
    RenderGroups::const_iterator iter = renderGroups.find(groupName);
    if(iter != renderGroups.end())
    {
      for(std::map<QString,DrawChoice>::const_iterator iter2 = iter->second.drawChoices.begin() ;
          iter2 != iter->second.drawChoices.end() ; iter2++)
        choiceList.push_back(iter2->first);
    }
    return choiceList;
  }

  // Get the vertex buffers and ids
  std::vector<uint> &CCDrawParms::elementEA(const QString &name)
  {
    return elementEAs[name];
  }

  std::vector<Colorb> &CCDrawParms::colorVA(const QString &name)
  {
    return colorVAs[name];
  }
  uint &CCDrawParms::colorVAid(const QString &name)
  {
    return colorVAids[name];
  }

  std::vector<Point3f> &CCDrawParms::vertexVA(const QString &name)
  {
    return vertexVAs[name];
  }
  uint &CCDrawParms::vertexVAid(const QString &name)
  {
    return vertexVAids[name];
  }

  std::vector<Point3f> &CCDrawParms::normalVA(const QString &name)
  {
    return normalVAs[name];
  }
  uint &CCDrawParms::normalVAid(const QString &name)
  {
    return normalVAids[name];
  }

  std::vector<Point3f> &CCDrawParms::clipVA(const QString &name)
  {
    return clipVAs[name];
  }
  uint &CCDrawParms::clipVAid(const QString &name)
  {
    return clipVAids[name];
  }

  ColorMap &CCDrawParms::colorMap(const QString &name)
  {
    if(!attributes)
      throw(QString("CCDrawParms::colorMap Attributes pointer not set"));

    return attributes->attrMap<QString, ColorMap>("#ColorMaps#")[name];
  }

  void CCDrawParms::eraseDrawChoice(const QString &groupName, const QString &choiceName)
  {
    auto groupIter = renderGroups.find(groupName);
    if(groupIter != renderGroups.end()) {
      auto choiceIter = groupIter->second.drawChoices.find(choiceName);
      if(choiceIter != groupIter->second.drawChoices.end()) {
        groupIter->second.drawChoices.erase(choiceIter);
        changed |= DrawChoicesChanged;
      }
    }
  }

  void CCDrawParms::eraseGroup(const QString &groupName)
  {
    auto iter = renderGroups.find(groupName);
    if(iter != renderGroups.end())
      renderGroups.erase(iter);
  }
}
