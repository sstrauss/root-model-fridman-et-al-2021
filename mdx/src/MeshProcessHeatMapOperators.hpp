//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_HEAT_MAP_HPP
#define MESH_PROCESS_HEAT_MAP_HPP

#include <limits>

#include <QTreeWidgetItem>

#include <Process.hpp>
#include <Progress.hpp>
#include <Information.hpp>
#include <MeshUtils.hpp>

class Ui_HeatMapLoad;

namespace mdx
{
  mdxBase_EXPORT void getBBox(const HVec3F& pts, Point3f* bBox);

  void fillTreeWidgetWithMeasures(Mesh* mesh, QTreeWidget* tree);

  ///\addtogroup MeshProcess
  ///@{
  
//  /**
//   * \class DeleteHeatRangeLabels ProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
//   *
//   * Delete labeling, and possibly cells, if their heat falls within a defined range.
//   */
//  class mdxBase_EXPORT DeleteHeatRangeLabels : public Process
//  {
//  public:
//    DeleteHeatRangeLabels(const Process& process) : Process(process) {}
//
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      float min, max;
//      bool ok = true;
//      if(parms[2].isEmpty())
//        min = std::numeric_limits<float>::quiet_NaN();
//      else
//        min = parms[2].toFloat(&ok);
//      if(not ok)
//        return setErrorMessage("Error, parameter 'Min Heat' must be either empty, or a number");
//      if(parms[3].isEmpty())
//        max = std::numeric_limits<float>::quiet_NaN();
//      else
//        max = parms[3].toFloat(&ok);
//      if(not ok)
//        return setErrorMessage("Error, parameter 'Min Heat' must be either empty, or a number");
//      return run(mesh, stringToBool(parms[0]), stringToBool(parms[1]), min, max);
//    }
//
//    bool run(Mesh* mesh, bool rescale, bool deleteCells, float min, float max);
//
//      setName("Mesh/Heat Map/Delete Heat Range Labels");
//    QString description() const { return
//          "Delete labels with heat within a given range. The heat is given "
//          "relative to the total range.");
//    QStringList parmNames() const { return QStringList()
//          << "Rescale" << "Delete cells" << "Min Heat" << "Max Heat");
//    QStringList parmDescs() const { return QStringList()
//          << "Redefine the lower/upper bounds of colormap to fit the range of values."
//          << "Delete vertices within a given range of the color map (Min Heat-Max Heat)."
//          << "Lower bound of color value for which the cells will be deleted (empty for current min)."
//          << "Upper bound of color value for which the cells will be deleted "
//             "(empty for current max).");
//    QStringList parmDefaults() const { return QStringList() << "Yes" << "No" << "" << "");
//    ParmChoiceMap parmChoice() const
//    {
//      ParmChoiceMap map;
//      map[0] = booleanChoice();
//      map[1] = booleanChoice();
//      return map;
//    }
//      setIcon(QIcon(":/images/ClearStack.png"));
//  };
  
  // FIXME Make a separate class for signal (that does not use labeling)
  /**
   * \class HeatMapCombine
   *
   * Combine heat maps to create a new one
   */ 
  class HeatMapCombine : public Process
  {
  public:
    HeatMapCombine(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Operators/Combine");
      setDesc("Creates an heat map of the chosen name (preceding Measure Label Double) using the current heat map values.");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Heat Map 1", "Heat Map 1", "");
      addParm("Heat Map 2", "Heat Map 2", "");
      addParm("Combination", "Is it a combination", "Yes", booleanChoice()); 
      addParm("Combination Type", "Type of combination\n", "Add", QStringList() << "Add" << "Subtract" << "Multiply" << "Divide" << "Power");
      addParm("Transformation", "Transformation", "No", booleanChoice());
      addParm("Transformation Type", "Transformation Type", "Neighborhood");
      addParm("Transformation Type 2", "Transformation Type 2", "Average");
      addParm("Ignore Parent", "Ignore Parent", "Yes", booleanChoice());
      addParm("Lower Threshold", "Lower Threshold", "0" );
      addParm("Upper Threshold", "Upper Threshold", "100000");
      addParm("Heat Map Combine", "Name of the new combined heat map", "");
    }
    bool initialize(QWidget *parent);

    bool run()
    {
      mesh = currentMesh();
      ccName = mesh->ccName();
      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      if(!mesh) 
        throw QString("No current mesh");

      run(*mesh, cs, indexAttr, parm("Heat Map 1"), parm("Heat Map 2"), 
        stringToBool(parm("Combination")), parm("Combination Type"), 
        stringToBool(parm("Transformation")), parm("Transformation Type"), parm("Transformation Type 2"), 
        parm("Ignore Parent").toInt(), parm("Lower Threshold").toDouble(), parm("Upper Threshold").toDouble(),
        parm("Heat Map Combine"));

      return true;
    }

    bool finalize(QWidget *parent)
    {
      if(parent and mesh and !heatName.isEmpty()) {
        mesh->drawParms(ccName).setGroupVisible("Faces", true);
        mesh->drawParms(ccName).setRenderChoice("Faces", heatName);
        mesh->setHeat(heatName);
        mesh->updateProperties(ccName);
      }
      return true;
    }

    bool run(Mesh &m, const CCStructure &cs, const CCIndexDataAttr &indexAttr, QString name1, QString name2, 
      bool combination, QString combinationType, bool transformation, QString transformationType, QString transformationType2, int ignoreParent, 
      double lowerT, double upperT, QString combinedAttrMap);

  private:
    Mesh *mesh = 0;
    QString ccName, heatName;
  };

  /**
   * \class HeatMapAverage
   *
   * Average heat map for a labeling
   */ 
  class HeatMapAverage : public Process
  {
  public:
    HeatMapAverage(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Operators/Average");
      setDesc("Average the 'Labels' heat values for a group labeling");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Heat Name", "Heat to use, empty for current", "");
      addParm("Labeling", "Labeling of groups to average, empty for current", "");
    }
    bool run();
  };

  /**
   * \class HeatMapPercentile
   *
   * Get the percentile heat for a labeling
   */ 
  class HeatMapPercentile : public Process
  {
  public:
    HeatMapPercentile(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Operators/Percentile");
      setDesc("Calculate a percentile of the 'Labels' heat values for a group labeling");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Heat Name", "Heat to use, empty for current", "");
      addParm("Labeling", "Labeling of groups to calculate percentile of, empty for current", "");
      addParm("Percentile", "Percentile to take heat value from, 0-100, (50 is median)", "50.0");
    }
    bool run();
  };
}

#endif
