//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef LIBRARY_HPP
#define LIBRARY_HPP

#include <Config.hpp>

#include <QString>

class Library 
{
public:
#if defined(WIN32) || defined(WIN64)
  typedef HINSTANCE handle_t;
#else
  typedef void* handle_t;
#endif

  Library(QString path);

  QString fileName() const {
    return filename;
  }
  void setFileName(const QString& fn) {
    filename = fn;
  }

  bool load();
  bool unload();

  bool isLoaded() const {
    return (bool)handle;
  }

  QString errorString() const {
    return error_string;
  }

  static bool isLibrary(QString path);

protected:
  QString filename, error_string;
  handle_t handle;
};

#endif
