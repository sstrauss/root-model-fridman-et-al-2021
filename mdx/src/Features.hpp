//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MDX_FEATURES_H
#define MDX_FEATURES_H

#include <Config.hpp>

#ifdef __GNUC__
#  ifdef WIN32
#    include <_mingw.h>
#    define __GNUC_PREREQ(x, y) __MINGW_GNUC_PREREQ(x, y)
#  else
#    include <features.h>
#  endif
#  define __GNUC_LESS(x, y) (!__GNUC_PREREQ(x, y))
#endif

#ifndef __GNUC_LESS
#define __GNUC_LESS(x, y) 0
#endif

#endif
