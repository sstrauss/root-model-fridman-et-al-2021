//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef SYMMETRIC_TENSOR_HPP
#define SYMMETRIC_TENSOR_HPP

#include <Vector.hpp>
#include <Matrix.hpp>
#include <Geometry.hpp>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>
namespace mdx 
{
  typedef Vector<3, double> Point3d;
  typedef Matrix<3, 3, double> Matrix3d;
  



  inline void eigenDecompSym3x3Copy(const Matrix3d &A, Matrix3d &V, Point3d &d)
  {
    gsl_matrix* mat = gsl_matrix_alloc(3, 3);
    gsl_vector* eval = gsl_vector_alloc(3);
    gsl_matrix* evec = gsl_matrix_alloc(3, 3);
    gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(3);
    for(int i = 0; i < 3; ++i)
      for(int j = 0; j < 3; ++j)
        gsl_matrix_set(mat, i, j, A[i][j]);
    gsl_eigen_symmv(mat, eval, evec, w);
    gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_DESC);
      
    V[0] = Point3d(gsl_matrix_get(evec, 0, 0), gsl_matrix_get(evec, 1, 0), gsl_matrix_get(evec, 2, 0));
    V[1] = Point3d(gsl_matrix_get(evec, 0, 1), gsl_matrix_get(evec, 1, 1), gsl_matrix_get(evec, 2, 1));
    V[2] = Point3d(gsl_matrix_get(evec, 0, 2), gsl_matrix_get(evec, 1, 2), gsl_matrix_get(evec, 2, 2));
    d = Point3d(gsl_vector_get(eval, 0), gsl_vector_get(eval, 1), gsl_vector_get(eval, 2));
    gsl_matrix_free(mat);
    gsl_matrix_free(evec);
    gsl_vector_free(eval);
    gsl_eigen_symmv_free(w);
  }


  inline void eigenDecompSym3x3Magn(const Matrix3d &A, Matrix3d &V, Point3d &d)
  {
    gsl_matrix* mat = gsl_matrix_alloc(3, 3);
    gsl_vector* eval = gsl_vector_alloc(3);
    gsl_matrix* evec = gsl_matrix_alloc(3, 3);
    gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(3);
    for(int i = 0; i < 3; ++i)
      for(int j = 0; j < 3; ++j)
        gsl_matrix_set(mat, i, j, A[i][j]);
    gsl_eigen_symmv(mat, eval, evec, w);
    gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_DESC);
      
    V[0] = Point3d(gsl_matrix_get(evec, 0, 0), gsl_matrix_get(evec, 1, 0), gsl_matrix_get(evec, 2, 0));
    V[1] = Point3d(gsl_matrix_get(evec, 0, 1), gsl_matrix_get(evec, 1, 1), gsl_matrix_get(evec, 2, 1));
    V[2] = Point3d(gsl_matrix_get(evec, 0, 2), gsl_matrix_get(evec, 1, 2), gsl_matrix_get(evec, 2, 2));
    d = Point3d(gsl_vector_get(eval, 0), gsl_vector_get(eval, 1), gsl_vector_get(eval, 2));
    gsl_matrix_free(mat);
    gsl_matrix_free(evec);
    gsl_vector_free(eval);
    gsl_eigen_symmv_free(w);
  }


  class SymmetricTensor 
  {
  public:
    SymmetricTensor() : _ev1(0, 0, 0), _ev2(0, 0, 0), _evals(0, 0, 0) {}
  
    SymmetricTensor(const Point3d &ev1, const Point3d &ev2, const Point3d &evals) : _ev1(ev1), _ev2(ev2), _evals(evals) {}

    SymmetricTensor(const Matrix3d &m) : _ev1(m[0]), _ev2(m[1]), _evals(m[2]) {}
  
    SymmetricTensor(const SymmetricTensor &copy) : _ev1(copy._ev1), _ev2(copy._ev2), _evals(copy._evals) {}
  
    SymmetricTensor &operator=(const SymmetricTensor &other)
    {
      _ev1 = other._ev1;
      _ev2 = other._ev2;
      _evals = other._evals;
      return *this;
    }

    bool operator==(const SymmetricTensor &other) const
    {
      if(_ev1 == other._ev1 and _ev2 == other._ev2 and _evals == other._evals)
        return true;
      return false;
    }

    bool operator!=(const SymmetricTensor &other) const 
    {
      return (!((*this) == other));
    }

  
    Point3d &ev1() {
      return _ev1;
    }

    Point3d &ev2() {
      return _ev2;
    }

    const Point3d &ev1() const {
      return _ev1;
    }

    const Point3d &ev2() const {
      return _ev2;
    }

    Point3d ev3() const {
      return _ev1 ^ _ev2;
    }

    const Point3d &evals() const {
      return _evals;
    }

    Point3d &evals() {
      return _evals;
    }

    Matrix3d toMatrix() {
        Matrix3d mat;
        Matrix3d e1 = _evals[0]*OuterProduct(Point3d(_ev1),Point3d(_ev1));
        Matrix3d e2 = _evals[1]*OuterProduct(Point3d(_ev2),Point3d(_ev2));
        Matrix3d e3 = _evals[2]*OuterProduct(Point3d(_ev1 ^ _ev2),Point3d(_ev1 ^ _ev2));
        mat = e1+e2+e3;

        return mat;
    }   
  
    bool fromMatrix(const Matrix3d &mat) {
        Point3d p1,p2,p3,ev;
        Matrix3d V;
        eigenDecompSym3x3Copy(mat, V, _evals);
        
        _ev1 = V[0];
        _ev2 = V[1];
        //Should be some error checking here
        return true;
    }

  protected:
    Point3d _ev1, _ev2;
    Point3d _evals;
  };
}
#endif
