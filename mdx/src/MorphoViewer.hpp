// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef VIEWER_H
#define VIEWER_H

#include <Config.hpp>
#include <GL.hpp>

#include <ClipRegion.hpp>
#include <CutSurf.hpp>
#include <ImageData.hpp>
#include <MDXViewer/qglviewer.h>
#include <MDXViewer/manipulatedFrame.h>
#include <Shader.hpp>
#include <Geometry.hpp>
#include <MDXCamera.hpp>

#include "LassoSelect.hpp"

class QDomElement;
class QShowEvent;
class MorphoDynamX;
class MDXCamera;


class MorphoViewer : public QGLViewer
{
  Q_OBJECT
public:
  enum FrameIdentity {FI_BACKGROUND, FI_FULL_IMG1, FI_FULL_IMG2, FI_CUR_PEEL,
    FI_VOLUME1, FI_VOLUME2, FI_OCCLUSION, NB_FRAMES };

  bool quitting = false;
  bool DrawClipBox = false;
  mdx::Point3d oldVPos;
  QPoint oldPos;
  QRect selectRect;
  LassoSelect selectLasso;
  bool voxelEditCursor = false;
  double pixelRadius = 1.0;
  QPoint mousePos;
  mdx::Shader raycastingShader1, raycastingShader2;
  mdx::Shader colormap2Shader, index2Shader;
  mdx::Shader finalCombineShader, combineShader, renderDepthShader;
  mdx::Shader textureSurfShader, colorSurfShader, flatSurfShader;
  mdx::Shader volumeSurfShader1, volumeSurfShader2;
  mdx::Shader postProcessShader;
  mdx::Shader occlusionShader;

  GLuint baseFboId, fboId, fboCopyId;
  GLuint depthTexId[NB_FRAMES];
  GLuint colorTexId[NB_FRAMES];
  GLuint depthBuffer;
  std::vector<GLfloat> depthTexture;
  int drawWidth, drawHeight;
  int prevWidth, prevHeight;

  // GUI Interaction
  bool shiftPressed = false;
  bool altPressed = false;
  bool controlPressed = false;
  bool leftButton = false;
  bool rightButton = false;

  enum GuiAction {STACK_PICK_LABEL, STACK_FILL_LABEL, STACK_PICK_FILL_LABEL, STACK_VOXEL_EDIT, STACK_DEL_LABEL, 
          MESH_RECT_SEL_VERTICES, MESH_RECT_SEL_FACES, MESH_RECT_SEL_VOLUMES, 
          MESH_LASSO_SEL_VERTICES, MESH_LASSO_SEL_FACES, MESH_LASSO_SEL_VOLUMES, 
          MESH_ADD_SEED, MESH_CURR_SEED, MESH_DRAW_SIGNAL, MESH_PICK_LABEL, MESH_SEL_LABEL, MESH_SEL_CONN, 
          MESH_SEL_FACE, MESH_SEL_VOLUME, MESH_GRAB_SEED, MESH_FILL_LABEL, MESH_PICK_FILL_LABEL };
  GuiAction guiAction = MESH_PICK_LABEL;

  bool guiActionOn = false;
  bool meshPickFill = true, stackPickFill = true;
  bool needCellEdgeUpdate = false;

  // Clipping planes
  mdx::ClipRegion clip1, clip2, clip3;
  mdx::Clip* c1, *c2, *c3;

  // Confocal stack objects
  QPointer<mdx::ImgData> stack1 = 0, stack2 = 0;

  // Cutting surface
  mdx::CutSurf* cutSurf = 0;

  double FlySpeed = 0.005;
  int selectedLabel = 0;
  double sampling = 2;
  bool fast_draw = false;
  int texWidth, texHeight;
  int show_slice = -1;
  int slice_type;

  int MaxNbPeels = 100;
  double Specular;
  double Shininess;
  double GlobalBrightness = 0.0;
  double GlobalContrast = 1.0;
  double GlobalShininess = 32.0;
  double GlobalSpecular = 0.2;
  double UnsharpStrength = 0.5;
  double Spinning = 10000;
  double ZoomFactor() const;
  void setZoomFactor(double f);
  mdx::Matrix4d CameraFrame;
  MDXCamera* _camera;

  MorphoDynamX* mainWindow();

  void setSceneBoundingBox(const mdx::Point3d &bbox);

  double getSceneRadius() const { return sceneRadius; }
  void processRunning(bool state) { _processRunning = state; }
  bool processRunning() { return _processRunning; }

private:
  double SceneRadius; // default radius
  double sceneRadius = 500.0;
  bool sceneIsEmpty;

  QString fullSnapshotFileName();
  
  bool initialized = false;
  qglviewer::Camera* lastInitCamera = 0;
  bool _processRunning = false;

  void initCamera();

public:
  MorphoViewer(QWidget* parent);
  MorphoViewer(QGLContext* context, QWidget* parent);
  MorphoViewer(const QGLFormat& format, QWidget* parent);
  virtual ~MorphoViewer();

  static void initFormat();

  virtual QDomElement domElement(const QString& name, QDomDocument& document) const;

  void drawSelectRect();
  void drawSelectLasso();
  void drawVoxelCursor();
  void checkVoxelCursor(bool altPressed);
  void drawColorBar();
  void drawScaleBar();
  void setLighting(mdx::ImgData *stack);
  void readParms(mdx::Parms& parms, QString section);
  void writeParms(QTextStream& pout, QString section);
  void updateAll();
  mdx::Point3d pointUnderPixel(const QPoint& pos, bool& found);
  void startScreenCoordinatesSystem(bool upward = false) const;
  void updateSceneRadius();

protected:
  void preDraw();
  void draw();
  void draw(QPaintDevice* device);
  void fastDraw();
  virtual void postDraw();
  virtual void init();
  void showEvent(QShowEvent* event);
  void resizeGL(int width, int height);
  void updateFBOTex(int width, int height, bool fast_draw = false);
  void drawColorTexture(int i, bool draw_depth = false);
  void alternatePeels(int& curPeelId, int& prevPeelId, int fullImgId);
  void combinePeels(int& fullImgId, int curPeelId, int volume1, int volume2);
  void setupCopyFB(GLuint depth, GLint color);
  void setupFramebuffer(GLuint depth, GLuint color, GLbitfield clear = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  void resetupFramebuffer(GLuint depth, GLuint color, GLbitfield clear = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  void resetFramebuffer();
  void leaveEvent(QEvent* e);
  void enterEvent(QEvent* e);
  void keyReleaseEvent(QKeyEvent* e);
  void keyPressEvent(QKeyEvent* e);
  void mouseMoveEvent(QMouseEvent* e);
  void mousePressEvent(QMouseEvent* e);
  void mouseReleaseEvent(QMouseEvent* e);
  void mouseDoubleClickEvent(QMouseEvent* e);

private:
  mdx::ImgData* findSelectStack();
  mdx::ImgData* findSelectSurf();
  mdx::ImgData* findSelectMesh();
  mdx::CCIndex findSelectFace(mdx::ImgData* stk, int x, int y);

  void getGUIFlags(QMouseEvent* e);
  void getGUIFlags(QKeyEvent* e);

  Qt::MouseButtons lastButtons;

  void clipEnable();
  void clipDisable();

  // Gui actions
  bool callGuiAction(GuiAction guiAction, QMouseEvent *e);

  // Stack gui actions
  bool stackLabel(QMouseEvent *e, GuiAction action);
  bool stackVoxelEdit(QMouseEvent *e);

  // Mesh gui actions
  bool meshMovePoints(QMouseEvent *e);
  bool meshSelect(QMouseEvent *e, GuiAction action);
  bool meshPickLabel(QMouseEvent *e);
  bool meshAddSeed(QMouseEvent *e);
  bool meshCurrentSeed(QMouseEvent *e);
  bool meshDrawSignal(QMouseEvent *e);
  bool meshFillLabel(QMouseEvent *e);
  bool meshPickFillLabel(QMouseEvent *e);
  bool meshSelectLabel(QMouseEvent *e);
  bool meshSelectConnected(QMouseEvent *e);
  bool meshSelectFace(QMouseEvent *e, GuiAction action);
  bool meshGrabSeed(QMouseEvent *e);

  int findLabel(const mdx::Store* store, mdx::Point3d start, mdx::Point3d dir) const;

  QPaintDevice* current_device;

  void clipEnable(mdx::Clip* c, bool _val);
  void clipGrid(mdx::Clip* c, bool _val);
  void clipWidth(mdx::Clip* c, int _val);


public slots:
  void setLabel(int label);
  void selectMeshLabel(int label, int repeat, bool replace = true);
  void reloadShaders();
  void updateViewer() { updateAll(); }
  void loadFile(const QString& pth, bool stack2, bool load_work);
  void updateLabels();

  //  Takes care of GUI clips
  void clip1Enable(bool _val) { clipEnable(c1, _val); }
  void clip1Grid(bool _val) { clipGrid(c1, _val); }
  void clip1Width(int _val) { clipWidth(c1, _val); }
  void clip2Enable(bool _val) { clipEnable(c2, _val); }
  void clip2Grid(bool _val) { clipGrid(c2, _val); }
  void clip2Width(int _val) { clipWidth(c2, _val); }
  void clip3Enable(bool _val) { clipEnable(c3, _val); }
  void clip3Grid(bool _val) { clipGrid(c3, _val); }
  void clip3Width(int _val) { clipWidth(c3, _val); }

  void flySpeed(int val) { FlySpeed = double(val) / 10000.0; }
  void slices(int val)
  {
    mdx::ImgData::Slices = val;
    updateAll();
  }
  void screenSampling(int val);
  void labelColor();
  void resetView();

  void recordMovie(bool on);

  void saveScreenshot(bool automatic = false, bool overwrite = false)
  {
    return QGLViewer::saveSnapshot(automatic, overwrite);
  }
  void saveScreenshot(const QString& fileName, bool overwrite = false)
  {
    return QGLViewer::saveSnapshot(fileName, overwrite);
  }
  void setScreenshotFileName(const QString& name) 
  {
    return QGLViewer::setSnapshotFileName(name);
  }
  void setScreenshotFormat(const QString& format) 
  {
    return QGLViewer::setSnapshotFormat(format);
  }
  void setScreenshotCounter(int counter) 
  {
    return QGLViewer::setSnapshotCounter(counter);
  }
  void setScreenshotQuality(int quality) 
  {
    return QGLViewer::setSnapshotQuality(quality);
  }
  bool openScreenshotFormatDialog() 
  {
    return QGLViewer::openSnapshotFormatDialog();
  }

  virtual void initFromDOMElement(const QDomElement& element);

  void viewerUpdate() { updateAll(); }

  virtual bool saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling = 1.0,
                                 bool expand = false, bool has_gui = false);

  bool saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling, bool expand, bool has_gui,
                         QString* error);

  void showEntireScene() { camera()->showEntireScene(); }

signals:
  void deleteSelection();
  void modified();
  void stepDrawFinished(bool);
  void setLabelColor(QIcon&);
  void recordingMovie(bool);
  void changeSceneRadius(double);
  void selectLabelChanged(int);
};

#endif
