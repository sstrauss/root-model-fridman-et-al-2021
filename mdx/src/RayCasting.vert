varying vec3 objectPos;

void main()
{
  //texCoord = vec3(gl_TextureMatrix[0] * gl_MultiTexCoord0);
  gl_Position = ftransform();
  objectPos = gl_Vertex.xyz / gl_Vertex[3];
  gl_FrontColor = gl_Color;
  //gl_ClipVertex = gl_ModelViewMatrix * gl_Vertex;
}
