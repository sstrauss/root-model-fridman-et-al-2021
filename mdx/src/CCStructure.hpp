//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2021 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_STRUCTURE_HPP
#define CC_STRUCTURE_HPP

#include <Config.hpp>
#include <Types.hpp>
#include <CCFCellStructure.hpp>

#include <boost/variant.hpp>
#include <boost/variant/polymorphic_get.hpp>

namespace mdx {

  struct mdx_EXPORT CCStructure : public CCStructureInterface<CCIndex>
  {
  public:
    typedef unsigned short Dimension;
    typedef ccf::Flip<CCIndex> FlipI;
    typedef ccf::FlipTable<CCIndex> FlipTableI;
    typedef typename FlipTableI::const_iterator FlipTableIterator;
    typedef std::vector<FlipI> FlipVectorI;
    typedef typename FlipVectorI::const_iterator FlipVectorIterator;
    typedef ccf::OrientedObject<CCIndex> OrientedCell;
    typedef ccf::OrientedSet<CCIndex> BoundaryChain;
    typedef typename BoundaryChain::const_iterator BoundaryIterator;

    typedef ccf::DimensionMapG<CCIndex> DimensionMap;
    typedef ccf::FlipTowerG<CCIndex> FlipTower;
    typedef ccf::CellTupleG<CCIndex> CellTuple;
    typedef ccf::SplitStructG<CCIndex> SplitStruct;

    // The core data structure is a boost::variant of
    // our acceptable cell complex representations.
    typedef boost::variant<ccf::CCStructure> VariantCC;

    VariantCC varCC;

    // Constructors.
    // Default constructor builds a CCF_CellStructure.
    CCStructure(Dimension dim = 2) : varCC(ccf::CCStructure(dim)) {}

    // You can also copy from a given variant.
    CCStructure(const ccf::CCStructure &cc) : varCC(cc) {}

    // ** Implementations all involve delegating to the
    //    base class of the varCC.

    inline const CCStructureInterface<CCIndex> *intrfc(void) const
    { return boost::polymorphic_get<const CCStructureInterface<CCIndex>>(&varCC); }

    inline CCStructureInterface<CCIndex> *intrfc(void)
    { return boost::polymorphic_get<CCStructureInterface<CCIndex>>(&varCC); }

    // Report the name of the variant in use.
    QString csType(void) const
    { return intrfc()->csType(); }

    /// Return the maximum dimension of cells in the CCStructure.
    Dimension maxDimension(void) const
    { return intrfc()->maxDimension(); }

    /// Queries if the cell with the given index is in the cell complex.
    bool hasCell(CCIndex cell) const
    { return intrfc()->hasCell(cell); }

    /// Returns the dimension of the given cell in this cell complex.
    Dimension dimensionOf(CCIndex cell) const
    { return intrfc()->dimensionOf(cell); }

    /// Returns a vector of all cells of the given dimension.
    const std::vector<CCIndex>& cellsOfDimension(Dimension d) const
    { return intrfc()->cellsOfDimension(d); }

    /// Returns the total number of cells in the cell complex.
    unsigned int cellCount(void) const
    { return intrfc()->cellCount(); }

    /// Returns the number of cells of the given dimension in the cell complex.
    unsigned int cellCount(Dimension dim) const
    { return intrfc()->cellCount(dim); }

    // ** Queries to the (perhaps synthetic) flip table.

    using CCStructureInterface<CCIndex>::matchV;
    using CCStructureInterface<CCIndex>::matchFirst;

    /// Match the given flip query, returning the matching flips in a std::vector.
    std::vector<FlipI> matchV(const FlipI &query) const
    { return intrfc()->matchV(query); }

    /// Return the first match for the given flip query.
    FlipI matchFirst(const FlipI &query) const
    { return intrfc()->matchFirst(query); }

    // ** Topological queries.

    /// Reports whether the given cells are incident.
    bool incident(CCIndex c1, CCIndex c2) const
    { return intrfc()->incident(c1,c2); }

    /// Reports whether the given cells are adjacent, i.e. flip to one another.
    bool adjacent(CCIndex c1, CCIndex c2) const
    { return intrfc()->adjacent(c1,c2); }

    /// Reports whether the given cell is on the border of the cell complex.
    /// For maximal-dimensional cells, reports instead if the cell is
    /// adjacent to the outside of the cell complex.
    bool onBorder(CCIndex cell) const
    { return intrfc()->onBorder(cell); }

    /// Returns the relative orientation of the two cells.
    ccf::RO ro(CCIndex c1, CCIndex c2) const
    { return intrfc()->ro(c1,c2); }

    /// Returns a set of the cells in the boundary of the given cell.
    std::set<CCIndex> bounds(CCIndex cell) const
    { return intrfc()->bounds(cell); }

    /// Returns a set of the cells in whose boundary the given cell lies.
    std::set<CCIndex> cobounds(CCIndex cell) const
    { return intrfc()->cobounds(cell); }

    /// Returns a set of the cells adjacent to the given cell.
    std::set<CCIndex> neighbors(CCIndex cell) const
    { return intrfc()->neighbors(cell); }

    /// Returns all cells of the given dimension incident on the given cell.
    std::set<CCIndex> incidentCells(CCIndex cell, Dimension dim) const
    { return intrfc()->incidentCells(cell,dim); }

    /// Returns the endpoints of the given edge.
    /// The first element of the pair has positive relative orientation
    /// with respect to the edge.
    std::pair<CCIndex,CCIndex> edgeBounds(CCIndex edge) const
    { return intrfc()->edgeBounds(edge); }

    /// Returns the oriented boundary chain of the given oriented cell.
    BoundaryChain boundary(OrientedCell ocell) const
    { return intrfc()->boundary(ocell); }

    /// Returns the oriented coboundary chain of the given oriented cell.
    BoundaryChain coboundary(OrientedCell ocell) const
    { return intrfc()->coboundary(ocell); }

    using CCStructureInterface<CCIndex>::join;
    using CCStructureInterface<CCIndex>::meet;

    /// Returns the highest-dimension common bounding cell of the given cells.
    CCIndex meet(CCIndex c1, CCIndex c2) const
    { return intrfc()->meet(c1,c2); }

    /// Returns the lowest-dimension common cobounding cell of the given cells.
    CCIndex join(CCIndex c1, CCIndex c2) const
    { return intrfc()->join(c1,c2); }

    /// Returns the oriented edge from the first vertex to the second.
    OrientedCell orientedEdge(CCIndex c1, CCIndex c2) const
    { return intrfc()->orientedEdge(c1,c2); }

    /// Returns the facet between the two maximum-dimension cells,
    /// oriented towards the first.
    OrientedCell orientedFacet(CCIndex c1, CCIndex c2) const
    { return intrfc()->orientedFacet(c1,c2); }

    /// Returns all cells of the given dimension which are
    /// incident to both input cells.
    std::set<CCIndex> coincidentCells(CCIndex c1, CCIndex c2, Dimension dim) const
    { return intrfc()->coincidentCells(c1,c2,dim); }


    // ** Alteration operations

    /// Empty a CCStructure, removing all of its cells.
    void clear(void)
    { return intrfc()->clear(); }

    /// Reverses all relative orientations with repsect to the given cell.
    bool reverseOrientation(CCIndex cell)
    { return intrfc()->reverseOrientation(cell); }

    /// Adds a new cell to the cell complex, given its boundary.
    bool addCell(CCIndex newCell,
		 BoundaryChain boundary = BoundaryChain(),
		 ccf::RO topRO = ccf::POS)
    { return intrfc()->addCell(newCell,boundary,topRO); }

    /// Deletes the given cell from the cell complex.
    bool deleteCell(CCIndex oldCell)
    { return intrfc()->deleteCell(oldCell); }

    /// Splits a cell in the cell complex, given the boundary of the membrane cell.
    bool splitCell(const SplitStruct& ss, BoundaryChain membraneBoundary = BoundaryChain())
    { return intrfc()->splitCell(ss, membraneBoundary); }

    /// Merges two adjacent cells in the cell complex.
    bool mergeCells(SplitStruct& ss)
    { return intrfc()->mergeCells(ss); }
  };

  /// Query whether the CCStructure is in fact a ccf::CCStructure.
  mdx_EXPORT bool confirmCCF(CCStructure &cc);

  /// Get a CCF_CellStructure from the CCStructure,
  /// or throw an exception.
  mdx_EXPORT ccf::CCStructure &ensureCCF(CCStructure &cc);

  /// Get a CCF_CellStructure from the CCStructure,
  /// or throw an exception.
  mdx_EXPORT const ccf::CCStructure &ensureCCF(const CCStructure &cc);

} // end namespace mdx

#endif // CC_STRUCTURE_HPP
