//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PROGRESS_H
#define PROGRESS_H

#include <Config.hpp>

#include <QEvent>
#include <QList>
#include <QMutex>
#include <QObject>
#include <QPointer>
#include <QString>
#include <QProgressBar>
#include <QLabel>
#include <QToolBar>
#include <QPushButton>
#include <QtCore>
#include <string>

namespace mdx 
{
  // Progress class
  class mdx_EXPORT Progress : public QObject 
  {
    Q_OBJECT
  
    QWidget* _parent;
    QProgressBar* _progressBar;
    QLabel* _progressText;
    QPushButton* _progressStop;
    static Progress* _instance;
  
    bool _canceled;
    int _step;
    int _steps;
    QString _msg;
  
  public:
    Progress();
    ~Progress();

    void setupProgress(QWidget* parent, QToolBar* progressToolBar);

    void start(const QString& msg, int steps, bool allowCancel = true);
    void start(const char* msg, int steps, bool allowCancel = true)
    {
      start(QString::fromLocal8Bit(msg), steps, allowCancel);
    }
    void start(std::string msg, int steps, bool allowCancel = true)
    {
      start(QString::fromStdString(msg), steps, allowCancel);
    }
    bool advance(int step = 0, int steps = 1);
    bool canceled();
    void cancel();
    void stop();
    void setMax(int steps);
    void setMsg(const QString& msg);
    void setMsg(const char* msg) { setMsg(QString::fromLocal8Bit(msg)); }
    void setMsg(std::string msg) { setMsg(QString::fromStdString(msg)); }
  
    static Progress& instance();

  signals:
    void progStart(QString msg, int steps, bool allowCancel);
    void progAdvance(int step, int steps = 1);
    void progSetMsg(QString msg);
    void progSetMax(int step);
    void progStop();

  private:
    QString makeMsg() const;
  };

  mdx_EXPORT void progressStart(const QString& msg, int steps = 0, bool allowCancel = true);
  mdx_EXPORT void progressStart(const char* msg, int steps = 0, bool allowCancel = true);
  mdx_EXPORT void progressStart(const std::string &msg, int steps = 0, bool allowCancel = true);
  mdx_EXPORT void progressStop();
  mdx_EXPORT bool progressAdvance(int step = 0, int steps = 1000);
  mdx_EXPORT bool progressCanceled();
  mdx_EXPORT void progressCancel();
  mdx_EXPORT void progressSetMsg(const QString &msg);
  mdx_EXPORT void progressSetMsg(const char *msg);
  mdx_EXPORT void progressSetMsg(const std::string &msg);
  mdx_EXPORT void progressSetMax(int steps = 0);
}
#endif
