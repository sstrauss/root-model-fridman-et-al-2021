//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessFibril.hpp>
#include <Information.hpp>
#include <Progress.hpp>
#include <PCA.hpp>
#include <CCUtils.hpp>

namespace mdx 
{
  //  Adapted from an ImageJ plugin published in Boudaoud et al., Nature Protocols 2014
  // 'FibrilTool, an ImageJ plug-in to quantify fibrillar structures in raw microscopy images'.
  bool FibrilOrientations::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr,
                                        double border, double minAreaRatio, double blurRadius, IntSymTensorAttr &fibrilAttr)
  {
    // List all the internal triangles (non border) first. 
    IntDoubleMap labelArea;
    IntDoubleMap insideArea;
    std::unordered_map<int, CCIndexVec> labelFaces;
    for(CCIndex f : cs.faces()) {
      auto &fIdx = indexAttr[f];
      if(fIdx.label <= 0)
        continue;
      labelArea[fIdx.label] += fIdx.measure;
      // FIXME check border distance
      insideArea[fIdx.label] += fIdx.measure;
      labelFaces[fIdx.label].push_back(f);
    }
  
    // Calculate orientation of the perpendicular to the average gradient of the signal
    std::unordered_map<int, Matrix3d> correlations;
    for(auto &pr : labelFaces) {
      int label = pr.first; 
      // if the total area of the triangles is too small, don't compute the PO
      if(insideArea[label] < labelArea[label] * minAreaRatio)
        continue; 

      for(CCIndex f : pr.second) {
        auto &fIdx = indexAttr[f];
        CCIndexVec fVertices = faceVertices(cs, f);
        if(fVertices.size() != 3)
          throw QString("%1 Face found with %2 edges, only triangles allowed").arg(name()).arg(cs.bounds(f).size());

        // Set signal to known gradient for debugging
        //v->signal = v->pos.y(); n->signal = n->pos.y(); m->signal = m->pos.y();

        // Calculate gradient
        Point3d grad = triangleGradient(indexAttr[fVertices[0]].pos, indexAttr[fVertices[1]].pos, indexAttr[fVertices[2]].pos,
                                        signalAttr[fVertices[0]], signalAttr[fVertices[1]], signalAttr[fVertices[2]]);

        // subtract any component in normal direction
        grad -= (grad * fIdx.nrml) * fIdx.nrml;

        // Take the perpendicular
        grad = fIdx.nrml ^ grad;
  
        Matrix3d corr;
        for(int i = 0; i < 3; i++)
          for(int j = 0; j < 3; j++)
            corr(i,j) = grad[i] * grad[j];
  
        correlations[label] += corr * fIdx.measure; // Weight by triangle area
      }
    }
  
    // Clear the axis data
    fibrilAttr.clear();
  
    // Do the PCA
    for(auto &pr : correlations) {
      int label = pr.first;
      if(labelArea[label] == 0)
        continue;

      // Divide by area weights
      Matrix3d corr = pr.second / labelArea[pr.first];
  
      PCA pca;
      if(!pca(corr))
        return setErrorMessage("Error performing PCA");
      
      auto &fibril = fibrilAttr[label];
      fibril.ev1() = pca.p1;
      fibril.ev2() = pca.p2;
      fibril.evals() = pca.ev;
    }
  
    return true;
  }
  REGISTER_PROCESS(FibrilOrientations);

//  // Display principal orientations (separatly from each other) and heatmaps of anisotropy etc.
//  bool DisplayFibrilOrientations::run(Mesh *mesh,const QString displayHeatMap,const QColor& qaxisColor,
//                                             float axisLineWidth, float axisLineScale, float axisOffset,
//                                             float orientationThreshold)																	 
//  {
//    // if there is no cell axis stored, display error message
//    const IntSymTensorAttr& cellAxis = mesh->cellAxis();
//    if(cellAxis.size() == 0 or mesh->cellAxisType() != "fibril"){
//      setErrorMessage(QString("No fibril axis stored in active mesh!"));
//      return false;
//    }
//  
//    // Check cell axis and prepare them for display. Populate cellAxisScaled in Mesh.  
//    IntMatrix3fAttr &cellAxisVis = mesh->cellAxisVis();
//    cellAxisVis.clear();
//    IntVec3ColorbAttr &cellAxisColor = mesh->cellAxisColor();
//    cellAxisColor.clear();
//    mesh->setAxisWidth(axisLineWidth);
//    mesh->setAxisOffset(axisOffset);
//    Colorb axisColor(qaxisColor);
//  
//    // Update the normals and center of the cell for visualisation.
//    if(mesh->labelCenterVis().empty() or mesh->labelNormalVis().empty())
//      mesh->updateCentersNormals();
//  
//    // Check if there is a cell center for each cell axis
//    IntPoint3fAttr labelCenterVis = mesh->labelCenterVis();
//    int nProblems = 0;
//    forall(const IntSymTensorPair &p, cellAxis) {
//      int label = p.first;
//      if(labelCenterVis.count(label) == 0)
//        nProblems++;
//    }
//    if(nProblems != 0)
//      setStatus("Warning: non-existing cell center found for " << nProblems << " cell axis.");
//  
//    // Scale the cell axis for display
//    forall(const IntSymTensorPair &p, cellAxis) {
//      int cell = p.first;
//      const SymmetricTensor& tensor = p.second;
//      // Degree of orientation = axisMax/axisMin -1. Values always >= 0 
//      //float orientation =  tensor.evals()[0]/tensor.evals()[1]-1;
//      float orientation = (tensor.evals()[0] - tensor.evals()[1])/(tensor.evals()[0] + tensor.evals()[1]);
//      cellAxisVis[cell][0] = (orientation > orientationThreshold ? orientation : 0.f) * tensor.ev1() * axisLineScale;
//      cellAxisVis[cell][1] = Point3f(0,0,0);
//      cellAxisVis[cell][2] = Point3f(0,0,0);
//  
//      cellAxisColor[cell][0] = axisColor;
//    }
//  
//    // Heat map of orientation = axisMax/axisMin -1.
//    if(displayHeatMap == "Orientation") {
//      mesh->labelHeat().clear();
//      IntFloatAttr labelHeatMap;
//      float maxValue = 0;
//      float minValue = 0;
//      bool firstRun = true;
//      forall(const IntSymTensorPair &p, cellAxis)
//      {
//        //float value = p.second.evals()[0] / p.second.evals()[1] -1;
//        float value = (p.second.evals()[0] - p.second.evals()[1]) 
//                                 / (p.second.evals()[0] + p.second.evals()[1]);
//        labelHeatMap[p.first] = value;
//  
//        if(firstRun) {
//          maxValue = value;
//          minValue = value;
//          firstRun = false;
//        }
//  
//        if(value > maxValue)
//          maxValue = value;
//        if(value < minValue)
//          minValue = value;
//      }
//  
//      // Adjust heat map if run on parents
//      if(mesh->labelMap("Parents").size() != 0){
//        mdxInfo << "parent label loaded" << endl;
//        mesh->labelHeat().clear();
//        IntIntAttr & parentMap = mesh->labelMap("Parents");
//        forall(const IntIntPair &p, parentMap)
//          if(p.second > 0 and labelHeatMap.count(p.second) != 0)
//            mesh->labelHeat()[p.first] = labelHeatMap[p.second];
//      }
//      else
//        mesh->labelHeat() = labelHeatMap;
//  
//      mesh->setHeatMapBounds(Point2f(minValue, maxValue));
//      mesh->setHeatMapUnit("Orientation");
//      mesh->setShowLabel("Label Heat");
//      mesh->updateTriangles();
//    }
//  
//    return true;
//  }
//  REGISTER_PROCESS(DisplayFibrilOrientations);
//
//
//  
//  //  Vertex based version
//  bool FibrilOrientationsVertex::run(Mesh* mesh, Mesh* mesh2, float border, float minAreaRatio, float radius)
//  {
//
//    typedef std::unordered_map<vertex, float > VtxFloatMap;
//    IntPoint3fAttr Normals;
//    IntPoint3fMap Orientations;
//    std::map<vertex, Matrix3f> Correlations;
//    VtxFloatMap Areas;
//    VtxFloatMap WeightedAreas;
//    VtxFloatMap InsideAreas;
//    VtxFloatMap Signals;
//
//    std::unordered_map<vertex, std::set<triangle> > TriangleMap;
//  
//    const vvGraph& S = mesh->graph();
//
//    mesh->updateCentersNormals();
//    //Normals = mesh->labelNormal();
//  
//    // Mark the vertices within this distance from the border.
//    mesh->markBorder(border);
//  
//    forall(const vertex &vv, S) {
//    
//      if(!vv->selected) continue;
//      //vertex nearest;
//      //double minDis = 1E20;
//      // List all the internal triangles (non border) first. 
//      forall(const vertex &v, S) {
//        //double dis = norm(v->pos - vv->pos);
//        //if(dis < minDis){
//        //  minDis = dis;
//        //  nearest = v;
//        //}
//        forall(const vertex &n, S.neighbors(v)) {
//          const vertex& m = S.nextTo(v, n); 
//          if(!S.uniqueTri(v, n, m))
//            continue; 
//          if(norm(vv->pos - v->pos) > radius or norm(vv->pos - n->pos) > radius or norm(vv->pos - m->pos) > radius)
//            continue;
//
//          float area = triangleArea(v->pos, n->pos, m->pos);
//          Areas[vv] += area;
//          if(n->minb == 0 and v->minb == 0 and m->minb == 0) {
//            InsideAreas[vv] += area;  
//            triangle t(v,n,m);
//            TriangleMap[vv].insert(t);
//          }
//        }
//      }
//
//      Normals[vv] = Point3f(vv->nrml);
//    }
//
//
//  
//    // Clear data in MorphoDynamX
//    mesh->clearCellAxis();
//  
//    // Calculate orientation of the perpendicular to the average gradient of the signal
//    std::unordered_map<vertex, std::set<triangle> >::iterator it;
//    for(it = TriangleMap.begin() ; it != TriangleMap.end() ; ++it) {
//      vertex vv = it->first; 
//      // if the total area of the triangles is too small, don't compute the PO
//      if(InsideAreas[vv] < Areas[vv]*minAreaRatio)
//        continue; 
//      std::set<triangle> Triangles = it->second; 
//      std::set<triangle>::iterator t;
//      for(t = Triangles.begin() ; t != Triangles.end() ; ++t) {
//        triangle tri = *t;
//        vertex v = tri[0];
//        vertex n = tri[1];
//        vertex m = tri[2];
//  
//        // Get center
//        //Point3f nrml = normalized((v->pos - n->pos) ^ (v->pos - m->pos));
//        Point3d nrml = vv->nrml;
//        double area = triangleArea(v->pos, n->pos, m->pos);
//  
//        // Set signal to known gradient for debugging
//        //v->signal = v->pos.y(); n->signal = n->pos.y(); m->signal = m->pos.y();
//
//        // Calculate gradient
//        Point3d grad = triangleGradient(v->pos, n->pos, m->pos, v->signal, n->signal, m->signal);
//        // subtract any component in normal direction
//        grad -= (grad * nrml) * nrml;
//
//      // Direction along the fibres
//      grad = nrml ^ grad;
//  
//      Matrix3f corr;
//      for(int i = 0; i < 3; i++)
//        for(int j = 0; j < 3; j++)
//          corr(i,j) = grad[i] * grad[j];
//  
//      Correlations[vv] += corr * area;// big triangles should count more than small ones
//      WeightedAreas[vv] += area;
//        
//  //      // Arezki version: we normalize the sum of correlations by the norm of gradient square
//  //      WeightedAreas[label] += area * normsq(grad); 
//      }
//    }
//  
//    // Do the PCA
//    typedef std::pair<vertex,float> VtxFloatPair;
//    forall(const VtxFloatPair &p, WeightedAreas) {
//      if(p.second == 0 )
//        continue;
//      // corr is an average of the correlations, weighted by triangles size
//      Matrix3f corr = Correlations[p.first] / p.second;
//  
//      PCA pca;
//      if(!pca(corr))
//        return setErrorMessage("Error performing PCA");
//      
//      SymmetricTensor& tensor = mesh->vertexAxis()[p.first];
//      tensor.ev1() = pca.p1;
//      tensor.ev2() = pca.p2;
//      tensor.evals() = pca.ev;
//    }
//  
//    mesh->setCellAxisType("fibril");
//    mesh->setCellAxisUnit("a.u.");
//    mesh->updateLines();
//    mesh->updateTriangles();
//  
//    // Run fibril display, with the parameters from the GUI 
//    QStringList parms;
//    DisplayFibrilOrientationsVertex *proc = getProcessParms<DisplayFibrilOrientationsVertex>(this, parms);
//    if(!proc)
//      throw(QString("Unable to create display fibril orientation process"));
//    proc->run(parms);
//  
//    return true;
//  }
//  REGISTER_PROCESS(FibrilOrientationsVertex);
//
//
//  // Display principal orientations (separatly from each other) and heatmaps of anisotropy etc.
//  bool DisplayFibrilOrientationsVertex::run(Mesh *mesh,const QString displayHeatMap,const QColor& qaxisColor,
//                                             float axisLineWidth, float axisLineScale, float axisOffset,
//                                             float orientationThreshold)                                   
//  {
//    // if there is no cell axis stored, display error message
//    const VtxSymTensorAttr& cellAxis = mesh->vertexAxis();
//    if(cellAxis.size() == 0 or mesh->cellAxisType() != "fibril"){
//      setErrorMessage(QString("No fibril axis stored in active mesh!"));
//      return false;
//    }
//  
//    // Check cell axis and prepare them for display. Populate cellAxisScaled in Mesh.  
//    VtxMatrix3fAttr &vtxAxisVis = mesh->vertexAxisVis();
//    vtxAxisVis.clear();
//    VtxVec3ColorbAttr &vtxAxisColor = mesh->vertexAxisColor();
//    vtxAxisColor.clear();
//    mesh->setAxisWidth(axisLineWidth);
//    mesh->setAxisOffset(axisOffset);
//    Colorb axisColor(qaxisColor);
//  
//    // Update the normals and center of the cell for visualisation.
//    if(mesh->labelCenterVis().empty() or mesh->labelNormalVis().empty())
//      mesh->updateCentersNormals();
//  
//    // Scale the cell axis for display
//    typedef std::pair<vertex, SymmetricTensor> VtxSymTensorPair;
//    forall(const VtxSymTensorPair &p, cellAxis) {
//      vertex vv = p.first;
//      const SymmetricTensor& tensor = p.second;
//      // Degree of orientation = axisMax/axisMin -1. Values always >= 0 
//      //float orientation =  tensor.evals()[0]/tensor.evals()[1]-1;
//      float orientation = (tensor.evals()[0] - tensor.evals()[1])/(tensor.evals()[0] + tensor.evals()[1]);
//      vtxAxisVis[vv][0] = (orientation > orientationThreshold ? orientation : 0.f) * tensor.ev1() * axisLineScale;
//      vtxAxisVis[vv][1] = Point3f(0,0,0);
//      vtxAxisVis[vv][2] = Point3f(0,0,0);
//  
//      vtxAxisColor[vv][0] = axisColor;
//    }
//    mesh->updateTriangles();
//  
//    return true;
//  }
//  REGISTER_PROCESS(DisplayFibrilOrientationsVertex);
}

