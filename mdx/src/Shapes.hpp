#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <CCF.hpp>
#include <Attributes.hpp>

namespace mdx
{
  // Approximates a unit sphere as a midpoint subdivision of an octahedron.
  void sphereOct(CCStructure &cs, CCIndexDataAttr &idxAttr, uint subdivs = 0);
};
#endif
