//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PROCESS_DOCS_DIALOG_HPP
#define PROCESS_DOCS_DIALOG_HPP

#include <Config.hpp>
#include <Process.hpp>

#include <QDialog>
#include <QFileDialog>

#include <memory>
#include <QMessageBox>
#include <QCheckBox>

#include <MeshProcessSystem.hpp>
#include <ui_ManageAttrDlg.h>

#include <ui_PlyCellGraphDlg.h>

class QTreeWidget;
class QTreeWidgetItem;

namespace mdx 
{
  class ManageAttributes : public Process
  {
    Q_OBJECT
    Ui_ManageAttrDlg ui;
  public:
    ManageAttributes(const Process &process) : Process(process) 
    {
      setName("Mesh/Attributes/Manage Attributes");
      setDesc("Manage Mesh Attributes");
      setIcon(QIcon(":/images/ManageAttributes.png"));

      addParm("Mesh Id", "Mesh number to manage attributes", "");
    }

    bool initialize(QWidget *parent);
    bool run() { return true; }

    bool setUpTree(const QStringList &attr);

    protected slots:
      void on_attrTreeWidget_itemClicked(QTreeWidgetItem *, int);
      void on_clearPushButton_clicked();
      void on_clearAllPushButton_clicked();
      void on_savePushButton_clicked();

    protected:
      Attributes *attributes;
  };

  class SaveAttributesCSV : public Process
  {
    Q_OBJECT
    Ui_PlyCellGraphDlg ui;

    QStringList attrMapsToBeSaved;
  public:
    SaveAttributesCSV(const Process &process) : Process(process) 
    {
      setName("Mesh/Attributes/Save Attributes to CSV");
      setDesc("Save Attributes to CSV (currently only for Labels#Double Attributes!)");
      setIcon(QIcon(":/images/ManageAttributes.png"));

      addParm("Filename", "Filename", "");
    }

    bool initialize(QWidget *parent);
    bool run()
    { 
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      return run(*mesh);
    }

    QString filename;

    bool run(Mesh &mesh);

   public slots:

     void selectAll();
     void unselectAll();

  };

//  class SaveAttributesCSV : public Process 
//  {
//  Q_OBJECT
//  public:
//
//    Ui_PlyCellGraphDlg ui;
//
//    QStringList attrMapsToBeSaved;
//
//    SaveAttributesCSV(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool initialize(QStringList& parms, QWidget* parent);
//
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      return run(mesh, parms[0]);
//    }
//
//    bool run(Mesh* mesh, QString filename);
//
//      setName("Mesh/Attributes/Save to CSV");
//      setDesc("Save Measure Attribute Maps to a csv file.");
//    QStringList parmNames() const { return QStringList() << "Filename");
//    QStringList parmDescs() const { return QStringList() << "Filename (if empty Dialog will be opened)");
//    QStringList parmDefaults() const { return QStringList() << "");
//      setIcon(QIcon(":/images/MakeHeatMap.png"));
//
//    public slots:
//
//      void selectAll();
//      void unselectAll();
//  };
}
#endif 

