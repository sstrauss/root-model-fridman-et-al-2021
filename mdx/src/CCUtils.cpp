//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CCUtils.hpp>
#include <Information.hpp>
#include <tbb/concurrent_unordered_set.h>
#include <tbb/concurrent_unordered_map.h>

namespace mdx
{
  // Select cells, normally when something goes wrong. Edges cause vertices to be selected. 
  void selectCell(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex c)
  {
    if(!c.isPseudocell()) {
      // For edges select the vertices
      if(cs.dimensionOf(c) == 1) {
        auto eBounds = cs.edgeBounds(c);
        indexAttr[eBounds.first].selected = true;
        indexAttr[eBounds.second].selected = true;
      } else
        indexAttr[c].selected = true;
    }
  }
  void selectCells(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex a, CCIndex b)
  {
    selectCell(cs, indexAttr, a);
    selectCell(cs, indexAttr, b);
  }
  void selectFlip(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCStructure::FlipI &flip)
  {
    selectCell(cs, indexAttr, flip.joint);
    selectCell(cs, indexAttr, flip.facet[0]);
    selectCell(cs, indexAttr, flip.facet[1]);
    selectCell(cs, indexAttr, flip.interior);
  }

  bool getConnectedFaces(const CCStructure &cs, CCIndexSet &faces)
  {
    CCIndexSet fNew(faces);

    // Start with a set of faces and grow by connectivity
    size_t origSize = faces.size();
    do {
      CCIndexSet nNew;
      for(CCIndex f : fNew)
        for(CCIndex n : cs.neighbors(f))
          if(faces.find(n) == faces.end()) {
            faces.insert(n);
            nNew.insert(n); 
          }
      fNew = nNew;
    } while(fNew.size() > 0);

    // Return true if we added something
    if(origSize == faces.size()) 
      return false;
    else
      return true;
  } 

  bool getConnectedSurface(const CCStructure &cs, CCIndexSet &faces)
  {
    CCIndexSet fNew(faces);

    // Start with a set of faces and grow by connectivity
    size_t origSize = faces.size();
    auto dim = cs.maxDimension();
    do {
      CCIndexSet nNew;
      for(CCIndex f : fNew)
        for(CCIndex n : cs.neighbors(f)) {
          if(dim == 3 and cs.incidentCells(f, 3).size() > 1)
            continue;
          if(faces.find(n) == faces.end()) {
            faces.insert(n);
            nNew.insert(n); 
          }
        }
      fNew = nNew;
    } while(fNew.size() > 0);

    // Return true if we added something
    if(origSize == faces.size()) 
      return false;
    else
      return true;
  } 

  // Add a face to a cell complex
  bool addFace(CCStructure &cs, CCIndex face, const std::vector<CCIndex> &vertices)
  {
    // Add vertices to complex
    for(uint i = 0 ; i < vertices.size() ; i++)
      if(!cs.hasCell(vertices[i]))
        if(!cs.addCell(vertices[i])) {
          mdxInfo << "addFace Error, Unable to add vertex" << endl;
          return false;
        }
      
    // Add edges to complex and accumulate face boundary chain
    CCStructure::BoundaryChain faceBoundary;
    for(uint i = 0 ; i < vertices.size() ; i++) {
      uint i1 = (i+1) % vertices.size();
      CCIndex v1 = vertices[i];
      CCIndex v2 = vertices[(i+1) % vertices.size()];
      CCSignedIndex edge = cs.orientedEdge(v1,v2);
      if(edge == CCIndex::UNDEF) {
        CCIndex e = CCIndexFactory.getIndex();
        if(!cs.addCell(e, +vertices[i] -vertices[i1])) {
          mdxInfo << "addFace Error, Unable to add edge" << endl;
          return false;
        }
        faceBoundary += e;
      } else
        faceBoundary += edge;
    }
    
    // Add face to complex
    if(!cs.addCell(face, faceBoundary)) {
      mdxInfo << "addFace Error, Unable to add face" << endl;
      return false;
    }

    return true;
  }


  // Get edge between v1 and v2, special case of join when you don't want faces
  CCIndex edgeBetween(const CCStructure &cs, CCIndex v1, CCIndex v2)
  {
    return cs.matchFirst(CCIndex::BOTTOM, v1, v2, CCIndex::Q).interior;
  }

  // Get the neighbors of a vertex along with the edges in a VV style but in no order
  std::vector<Flip> neighborVertexFlips(const CCStructure &cs, CCIndex v)
  {
    return cs.matchV(CCIndex::BOTTOM, v, CCIndex::Q, CCIndex::Q);
  }

  // Get the neighbors of a cell of the same dimension with the higher dimensional cell between them
  CCIndexPairVec neighborInteriors(const CCStructure &cs, CCIndex v)
  {
    CCIndexPairVec result;

    for(auto flip : cs.matchV(CCIndex::BOTTOM, v, CCIndex::Q, CCIndex::Q))
      result.push_back(std::make_pair(flip.otherFacet(v), flip.interior));

    return result;
  }

  // Get the(an) oriented wall membrane between two cells. 
  CCSignedIndex orientedMembrane(const CCStructure &cs, CCIndex c, CCIndex n)
  {
    FlipVec flipV = cs.matchV(CCIndex::Q, c, n, CCIndex::TOP);
    if(flipV.size() > 0)
      return CCSignedIndex(flipV[0].joint, cs.ro(c, flipV[0].joint));
    else
      return CCSignedIndex();
  }

  // Get the face neighbors of a vertex in no particular order
  CCIndexVec vertexFaces(const CCStructure &cs, CCIndex v, bool infty)
  {
    CCIndexVec result;
    FlipVec flipV = cs.matchV(v, CCIndex::Q, CCIndex::Q, CCIndex::Q);
    for(auto &flip : flipV) 
      if(infty or flip.interior != CCIndex::INFTY)
        result.push_back(flip.interior);
    return result; 
  }

  // Extract a number of cells (with their boundary elements) into a new cell complex.
  CCStructure extractCells(const CCStructure &oldCS, const CCIndexVec &cells)
  {
    CCStructure newCS(oldCS.maxDimension());
    ccf::CCStructure &newCSX = ensureCCF(newCS);

    // Add the cells to the cell complex
    for(CCIndex cell : cells)
    {
      if(!cell.isPseudocell() && oldCS.hasCell(cell))
        newCSX.dimensionMap.insert(cell , oldCS.dimensionOf(cell));
    }

    // Go from highest dimension down, adding flips and bounding cells.
    for(uint dim = newCSX.maxDimension() ; dim > 0 ; dim--)
    {
      std::set<CCIndex> bdCells;  // All flips bounding cells of dimension dim.
      forall(CCIndex cell, newCSX.cellsOfDimension(dim))
      {
        // Add flips with interior cell.
        FlipVec flips = oldCS.matchV(CCIndex::Q,CCIndex::Q,CCIndex::Q,cell);
        newCSX.flips.insert(flips.begin() , flips.end());

        // Record cells in the boundary
        forall(Flip flip , flips)
        {
          bdCells.insert(flip.facet[0]);
          bdCells.insert(flip.facet[1]);
        }
      }

      // Add the cells from bdCells to the dimension map, if they're not already there.
      for(CCIndex b : bdCells)
      {
        if(!newCSX.dimensionMap.hasCell(b))
          newCSX.dimensionMap.insert(b , dim - 1);
      }
    }

    // We must also add maxdim-flips if we have any maxdim-cells.
    if(!newCSX.cellsOfDimension(newCSX.maxDimension()).empty())
    {
      // record external facets for each joint
      std::map<CCIndex , BoundaryChain> externalFacets;

      // First add the maxdim-flips corresponding to each (maxdim - 1)-cell.
      for(CCIndex facet : newCSX.cellsOfDimension(newCSX.maxDimension() - 1))
      {
        Flip facetFlip = oldCS.matchFirst(facet,CCIndex::Q,CCIndex::Q,CCIndex::TOP);

        // If we can't find a flip, then facet is a fin in oldCS and can be skipped.
        if(!facetFlip.isValid())
          continue;

        // We have three cases, depending on which of the maxdim-cells is in the new cell complex.
        bool inComplex0 = (!facetFlip.facet[0].isPseudocell()) && newCSX.hasCell(facetFlip.facet[0]),
             inComplex1 = (!facetFlip.facet[1].isPseudocell()) && newCSX.hasCell(facetFlip.facet[1]);
        // 1. If both maxdim-cells are in the new complex, we insert the flip as-is.
        if(inComplex0 && inComplex1)
        {
          newCSX.flips.insert(facetFlip);
        }
        // 2. If neither of the maxdim-cells is in the new complex,
        //    then the facet is a fin in newCS and we can ignore the flip.
        else if(!inComplex0 && !inComplex1)
        {}
        // 3. If exactly one of the maxdim-cells is in the new complex,
        //    we have to add an INFTY flip and do more bookkeeping.
        else
        {
          if(!inComplex0)
          {
            facetFlip.facet[0] = CCIndex::INFTY;
            for(CCSignedIndex ojoint : newCSX.boundary(+facet))
              externalFacets[~ojoint].insert(ojoint.orientation() * facet);
          }
          else
          {
            facetFlip.facet[1] = CCIndex::INFTY;
            for(CCSignedIndex ojoint : newCSX.boundary(-facet))
              externalFacets[~ojoint].insert(ojoint.orientation() * facet);
          }
          newCSX.flips.insert(facetFlip);
        }
      }

      // Now we have to add in (maxdim - 1)-flips through INFTY.
      for(std::map<CCIndex,BoundaryChain>::iterator efIter = externalFacets.begin() ;
          efIter != externalFacets.end() ; efIter++)
      {
        const CCIndex &joint = efIter->first;
        BoundaryChain &facets = efIter->second;
        
        // Most joints should have two adjoining facets of opposite orientation;
        // we can then slot them directly into a flip.
        if(facets.size() == 2)
        {
          CCSignedIndex sf1 = *(facets.begin()),
                        sf2 = *(++facets.begin());

          if(sf1.orientation() == sf2.orientation())
          {
            mdxInfo << "Inconsistent orientation between facets "
                             << ccf::to_string(sf1).c_str() << " and "
                             << ccf::to_string(sf2).c_str() << " about joint "
                             << ccf::to_string(joint).c_str() << endl;
            throw(QString("Inconsistent orientation detected in extractCells"));
          }

          // Create and add the flip.
          Flip flip;
          flip.joint = joint;
          flip.interior = CCIndex::INFTY;
          flip.facet[sf1.orientation()] = ~sf1;
          flip.facet[sf2.orientation()] = ~sf2;
          newCSX.flips.insert(flip);
        }
        // Otherwise, we're on an osculation point and there should be an even number of facets;
        // we can use an iteration about the joint in oldCS to find where to go through INFTY.
        else
        {
          if(facets.empty() || facets.size() % 2 == 1)
          {
            mdxInfo << "Found " << facets.size() << " facets for external joint "
                             << ccf::to_string(joint).c_str() << endl;
            throw(QString("Bad facet number detected in extractCells"));
          }

          // If the joint is an osculation point in oldCS,
          // there will be more than one rotation around it.
          // We will have to perform each of these rotations
          // until we've accounted for every new external facet.
          while(!facets.empty())
          {
            // startFacet records the facet we flip across to enter newCS;
            // it is combined with the facet we flip across to leave newCS
            // to create the flip across INFTY.
            // It is set to UNDEF initially and when we're moving outside newCS.
            CCSignedIndex startFacet = +CCIndex::UNDEF;
            // Create a tuple from startFacet, starting inside newCS.
            CellTuple tuple(oldCS,joint,~startFacet);
            if(!newCSX.hasCell(tuple[newCSX.maxDimension()]))
              tuple.flip(newCSX.maxDimension() - 1);
            // Iterate about the joint.
            CCIndex firstFacet = ~(*facets.begin());
            do
            {
              // We want to do something if we've moved across the border of newCS.
              if(~startFacet == CCIndex::UNDEF && newCSX.hasCell(tuple[newCSX.maxDimension()]))
              {
                // Moved inside: assign new start facet.
                startFacet = *facets.find(tuple[newCSX.maxDimension() - 1]);
              }
              else if(~startFacet != CCIndex::UNDEF && !newCSX.hasCell(tuple[newCSX.maxDimension()]))
              {
                // Moved outside: create flip and erase start facet.
                // What facet did we flip across to leave newCS?
                CCSignedIndex endFacet = *facets.find(tuple[newCSX.maxDimension() - 1]);

                // Make sure of the orientations.
                if(startFacet.orientation() == endFacet.orientation())
                {
                  mdxInfo << "Inconsistent orientation between facets "
                                   << ccf::to_string(startFacet).c_str() << " and "
                                   << ccf::to_string(endFacet).c_str() << " about joint "
                                   << ccf::to_string(joint).c_str() << endl;
                  throw(QString("Inconsistent orientation detected in extractCells"));
                }

                // Create and add the flip.
                Flip flip;
                flip.joint = joint;
                flip.interior = CCIndex::INFTY;
                flip.facet[startFacet.orientation()] = ~startFacet;
                flip.facet[endFacet.orientation()] = ~endFacet;
                newCSX.flips.insert(flip);

                // Eliminate both facets from facets and clear startFacet.
                facets.erase(startFacet);
                facets.erase(endFacet);
                startFacet = CCIndex::UNDEF;
              }              

              // Advance the rotation.
              tuple.flip(newCSX.maxDimension() - 1, newCSX.maxDimension());
            } while(tuple[newCSX.maxDimension() - 1] != firstFacet);
          }
        }
      }
    }

    return newCS;
  }

  CCStructure extractCell(const CCStructure &oldCS, CCIndex cell)
  {
    CCIndexVec cells(1,cell);
    return extractCells(oldCS,cells);
  }

  /// Delete a number of cells from a cell complex
  // This function effectively reconstructs a new complex without the given cells.
  bool deleteCells(CCStructure &cs, const CCIndexSet &delCells, CCIndexDataAttr &indexAttr)
  {
    ccf::CCStructure &csX = ensureCCF(cs);

    // We can't add elements (i.e. INFTY) to delCells, so we add a helper lambda
    // to tell us if a cell is to be deleted.
    auto toDelete = [&delCells](CCIndex c) -> bool
    {
      return (c == CCIndex::INFTY) || (delCells.count(c) > 0);
    };

    // Find flips for new flip table, i.e. any flip that does not mention any deleted cell.
    // We also check here that we aren't trying to delete a cell in the boundary of
    // a non-deleted cell.
    FlipTbbVec keepFlips;
    bool valid = true;
    #pragma omp parallel for
    for(uint i = 0; i < csX.flips.flipVec.size(); i++) {
      auto flip = csX.flips.flipVec[i];
      // Set up a flip telling us whether the given element is present
      // in the new complex.
      ccf::Flip<bool> present(!toDelete(flip.joint),
                              !toDelete(flip.facet[0]),
                              !toDelete(flip.facet[1]),
                              !toDelete(flip.interior));

      // Case 1: all cells are present, keep the flip.
      if(present.joint && present.facet[0] && present.facet[1] && present.interior)
          keepFlips.push_back(flip);
      // Case 2: interior is either absent or TOP: check if we're
      //         deleting the joint but keeping a facet.
      else if(!present.interior || flip.interior == CCIndex::TOP) {
        if (!present.joint && (present.facet[0] || present.facet[1])) {
          CCIndex kept = present.facet[0] ? flip.facet[0] : flip.facet[1];
          mdxCritical << "deleteCells: Cannot delete cell " << flip.joint
                      << " in boundary of non-deleted cell " << kept << "." << endl;
          valid = false;
        }
      // Fallthrough: interior must be present but others are absent: fail.
      } else {
        CCIndex deleted = (!present.joint)    ? flip.joint :
                          (!present.facet[0]) ? flip.facet[0] : flip.facet[1];
        mdxCritical << "deleteCells: Cannot delete cell " << deleted
                    << " in boundary of non-deleted cell " << flip.interior << "." << endl;
        valid = false;
      }
    }
    if(!valid) return false;

    // Build vectors for dimension map load
    CCIndexVecVec keepCells(csX.maxDimension() + 1);
    #pragma omp parallel for
    for(uint dim = 0 ; dim <= csX.maxDimension() ; dim++) {
      for(CCIndex c : csX.cellsOfDimension(dim))
        if(!toDelete(c))
          keepCells[dim].push_back(c);
    }

    // Create new flip table
    csX.flips.flipVec.resize(keepFlips.size());
    #pragma omp parallel for
    for(uint i = 0; i < keepFlips.size(); i++)
      csX.flips.flipVec[i] = keepFlips[i];

    csX.flips.reindex();
    csX.dimensionMap.load(keepCells);

    // Recompute the INFTY flips
    createBorderFlips(csX, indexAttr);

    mdxInfo << delCells.size() << " n-cells deleted." << endl;

    return true;
  }

  /// Get face vertices in counter-clockwise order
  CCIndexVec faceVertices(const CCStructure &cs, CCIndex face)
  {
    CCIndexVec vertices;

    if(cs.dimensionOf(face) != 2) {
      mdxInfo << "faceVertices Bad face, dimension:" << cs.dimensionOf(face) << endl;
      return vertices;
    }

    typedef CCStructure::FlipI Flip;
    // get the edge flips
    std::vector<Flip> eFlips = cs.matchV(CCIndex::Q,CCIndex::Q,CCIndex::Q,face);
    uint numVertices = eFlips.size();
  
    if(numVertices < 3) {
      mdxInfo << "faceVertices Bad face, vertices:" << numVertices << endl;
      return vertices;
    }

    // Resize vector
    vertices.resize(numVertices);

    if(numVertices == 3) { // special case for triangles
      // we take the first vertex to be eFlips[0].joint
      vertices[0] = eFlips[0].joint;
  
      // then the second vertex is whichever shares the edge eFlips[0].facet[0]
      if(eFlips[1].facet[1] == eFlips[0].facet[0])
      {
        vertices[1] = eFlips[1].joint;
        vertices[2] = eFlips[2].joint;
      }
      else  // eFlips[2].facet[1] == eFlips[0].facet[0]
      {
        vertices[1] = eFlips[2].joint;
        vertices[2] = eFlips[1].joint;
      }
    } else { // general case
      // create lookup so we can find the flip by looking up its second facet
      std::unordered_map<CCIndex,uint> flipMap;
      for(uint i = 0 ; i < numVertices ; i++)
        flipMap[eFlips[i].facet[1]] = i;
  
      // starting on this edge
      CCIndex edge = eFlips[0].facet[0];
      // first vertex is eFlips[0].joint
      vertices[0] = eFlips[0].joint;
      // grab the rest by following the chain of edges
      for(uint i = 1 ; i < numVertices ; i++)
      {
        const Flip &flip = eFlips[flipMap[edge]];
        vertices[i] = flip.joint;
        edge = flip.facet[0];
      }
    }
    return vertices;
  }

  /// Get vertices and edges of a triangle in counter-clockwise order (FIXME not tested)
  bool triangleVerticesEdges(const CCStructure &cs, CCIndex face, CCIndexVec &vertices, CCIndexVec &edges)
  {
    typedef CCStructure::FlipI Flip;
    // get the edge flips
    std::vector<Flip> eFlips = cs.matchV(CCIndex::Q,CCIndex::Q,CCIndex::Q,face);
    if(eFlips.size() != 3)
      return false;
  
    vertices.resize(3);
    edges.resize(3);
  
    // we take the first vertex to be eFlips[0].joint
    vertices[0] = eFlips[0].joint;
    edges[0] = eFlips[0].facet[0];
  
    // then the second vertex is whichever shares the edge eFlips[0].facet[0]
    if(eFlips[0].facet[0] == eFlips[1].facet[1]) {
      vertices[1] = eFlips[1].joint;
      vertices[2] = eFlips[2].joint;
      edges[1] = eFlips[1].facet[0];
      edges[2] = eFlips[2].facet[0];
    } else { // eFlips[2].facet[1] == eFlips[0].facet[0]
      vertices[1] = eFlips[2].joint;
      vertices[2] = eFlips[1].joint;
      edges[1] = eFlips[2].facet[0];
      edges[2] = eFlips[1].facet[0];
    }

    return true;
  }

  CCIndexVec vertexNeighbors(const CCStructure &cs, CCIndex v)
  {
    CCIndexVec answer;
    try {
      CCStructure::CellTuple tuple(cs, v);
      if(tuple.totalOrientation() == ccf::NEG)
        tuple.flip(1);
      CCIndex firstEdge = tuple[1];
      do {
        answer.push_back(tuple.other(0));
        tuple.flip(1,2);
      } while(firstEdge != tuple[1]);
    } catch(...) {
      printf("vertexNeighbors: Exception working with tuple\n");
    }
    return answer;
  }

  inline std::pair<uint,uint> mkEdge(uint v1, uint v2)
  { 
    return (v1 < v2) ? std::make_pair(v1,v2) : std::make_pair(v2,v1); 
  }

  template<typename Index>
  inline Index& facetO(ccf::Flip<Index> &flip, ccf::RO ro)
  { 
    return flip.facet[ro == ccf::POS ? 0 : 1]; 
  }

  // Create a cell complex from a list of triangles
  // This will create the edge and face indices
  bool ccFromTriangles(CCStructure &cs, const CCIndexVec &vertices, const Point3uVec &triangles)
  {
    uint nFaces = triangles.size();
    std::vector<CCIndex> faces(nFaces);
    std::vector< std::vector<uint> > faceVertices(nFaces);

    #pragma omp parallel for
    for(uint f = 0; f < nFaces ; f++) {
      faceVertices[f].resize(3);
      faces[f] = CCIndexFactory.getIndex();
      for(uint v = 0 ; v < 3 ; v++)
        faceVertices[f][v] = triangles[f][v];
    }

    return ccFromFaces(cs, vertices, faces, faceVertices);
  }

  // Create a cell complex from a list of vertices and faces
  // This will create the edge indices.
  bool ccFromFaces(CCStructure &cs, const CCIndexVec &vertices, const CCIndexVec &faces, const UIntVecVec &faceVertices)
  {
    uint nFaces = faces.size();
    if(faceVertices.size() != nFaces)
      throw(QString("Face count %1 does not match faceVertices count %2").arg(nFaces).arg(faceVertices.size()));
      
    CCIndexVec edges;
    UIntPairVec edgeVertices;
    UIntVecVec faceEdges(nFaces);
    UIntPairUIntMap edgeMap;
    uint eCount = 0;
    // Create edges, edgeVertices, and faceEdges
    for(uint f = 0; f < nFaces; f++) {
      uint vSize = faceVertices[f].size();
      faceEdges[f].resize(vSize);
      for(uint v = 0; v < vSize; v++) {
        uint w = (v + 1) % vSize;
        auto e = std::make_pair(faceVertices[f][v], faceVertices[f][w]);
        // Orient edges so that lower index is first
        if(e.first > e.second)
          std::swap(e.first, e.second);
        auto it = edgeMap.find(e);
        uint eNum;
        if(it == edgeMap.end()) {
          eNum = eCount++;
          edgeMap[e] = eNum;
          edgeVertices.push_back(e);
          edges.push_back(CCIndexFactory.getIndex());
        } else
          eNum = it->second;
        faceEdges[f][v] = eNum;
      }
    }
    // Create cell complex
    return ccFromFaces(cs, vertices, edges, edgeVertices, faces, faceVertices, faceEdges);
  }
 
  // Create a cell complex from a list of vertices, edges and faces
  bool ccFromFaces(CCStructure &cs, const CCIndexVec &vertices, const CCIndexVec &edges, const UIntPairVec &edgeVertices, 
    const CCIndexVec &faces, const UIntVecVec &faceVertices, const UIntVecVec &faceEdges)
  {
    ccf::CCStructure &csX = ensureCCF(cs);

    typedef CCStructure::FlipI Flip;
    // Get vector sizes
    uint nEdges = edges.size();
    uint nFaces = faces.size();
    uint enfErrors = 0, efaErrors = 0, bveErrors = 0; 

    // Check the sizes
    if(cs.maxDimension() < 2 && nFaces > 0)
      throw(QString("Cannot create faces in a cell complex of dimension %1").arg(cs.maxDimension()));
    if(edgeVertices.size() != nEdges)
      throw(QString("Edge count %1 does not match edgeVertices count %2").arg(nEdges).arg(edgeVertices.size()));
    if(faceEdges.size() != nFaces)
      throw(QString("Face count %1 does not match faceEdges count %2").arg(nFaces).arg(faceEdges.size()));
    if(faceVertices.size() != nFaces)
      throw(QString("Face count %1 does not match faceVertices count %2").arg(nFaces).arg(faceVertices.size()));

    // Create index into faceMap, should this be input?
    UIntVec facePos(nFaces);
    uint nFaceVertices = 0;
    for(uint f = 0; f < nFaces; f++) {
      facePos[f] = nFaceVertices;
      nFaceVertices += faceVertices[f].size();
    }

    // Create edge to face map
    UIntUIntVecMap edgeFaces;
    for(uint f = 0; f < nFaces; f++)
      for(uint e : faceEdges[f])
        edgeFaces[e].emplace_back(f);

    // Create the edge flips
    std::vector<Flip> flips1(nFaceVertices);
    #pragma omp parallel for
    for(uint f = 0 ; f < nFaces; f++) {
      uint fSize = faceVertices[f].size();
      for(uint i = 0 ; i < fSize ; i++) {
        uint j = (i + 1) % fSize;
        flips1[facePos[f] + i] = Flip(vertices[faceVertices[f][j]],  // RSS Orientation correct?
                                edges[faceEdges[f][j]], edges[faceEdges[f][i]], faces[f]);
      }
    }

    // Create the vertex and face flips
    std::vector<Flip> flips0(nEdges);
    std::vector<Flip> flips2; 
    if(csX.maxDimension() == 2)
      flips2.resize(nEdges); // Top flips, we'll add boundary after
    #pragma omp parallel for
    for(uint e = 0 ; e < nEdges; e++) {
      // Vertex flip, edges are all first -> second
      flips0[e] = Flip(CCIndex::BOTTOM, vertices[edgeVertices[e].first], vertices[edgeVertices[e].second], edges[e]);

      // Face flip
      if(csX.maxDimension() == 2 && !edgeFaces[e].empty()) {
        uint f0 = edgeFaces[e][0];

	// Face opposite f0 across e
	CCIndex otherF = (edgeFaces[e].size() > 1) ? faces[edgeFaces[e][1]] : CCIndex::INFTY;

	flips2[e] = Flip(edges[e], faces[f0], otherF, CCIndex::TOP);

	// Find e's index in f0's faceEdges
        uint i = 0;
        while(i < faceEdges[f0].size()) {
          if(e == faceEdges[f0][i])
            break;
          i++;
        }
        if(i == faceEdges[f0].size()) {
          enfErrors++;
          continue;
        }

        // Reverse flip orientation if edge is oriented against face
	if(edgeVertices[e].first != faceVertices[f0][i])
	  flips2[e] = -flips2[e];
      }
    }

    // If we're in two dimensions, handle border flips
    if(csX.maxDimension() == 2) {
      // Find border vertices and edges
      UIntUIntVecMap borderVertexEdges;
      for(uint e = 0; e < edgeFaces.size(); e++)
        if(edgeFaces[e].size() == 1) {
          // Border edges have only one face
          borderVertexEdges[edgeVertices[e].first].emplace_back(e);
          borderVertexEdges[edgeVertices[e].second].emplace_back(e);
        } else if(edgeFaces[e].size() != 2)
          efaErrors++;

      for(auto &pr : borderVertexEdges) {
        uint v = pr.first;
        if(pr.second.size() != 2)
          bveErrors++;
        uint e0 = pr.second[0];
        uint e1 = pr.second[1];

	flips1.push_back(Flip(vertices[v], edges[e0], edges[e1], CCIndex::INFTY));

	// Check ro in adjacent face to determine orientation wrt INFTY
        uint f = edgeFaces[e0][0];
        const UIntVec &fVertices = faceVertices[f];
        for(uint i = 0; i < fVertices.size(); i++) {
	  uint i1 = (i+1) % fVertices.size();
          // Find vertex in face list
          if(fVertices[i1] == v) {
	    // The flip in f is (fv[i1], fe[i1] | fe[i], f);
	    // we want the opposite RO of e wrt INFTY, so we want
	    // (v, e1 | e0, INF) when e1 == fe[i] or e0 == fe[i1], i.e.
	    if(e1 == faceEdges[f][i] || e0 == faceEdges[f][i1])
	      flips1.back() = -flips1.back();
            break;
          }
        }
       } 
       if(enfErrors > 0)
         mdxInfo <<  QString("%1 edges not found for faces").arg(enfErrors) << endl;
       if(efaErrors > 0)
         mdxInfo << QString("%1 edges found attached to > 2 faces").arg(efaErrors) << endl;
       if(bveErrors > 0)
         mdxInfo << QString("%1 border vertices found with edges != 2").arg(bveErrors) << endl;
    }

    // Clear the cell structure
    csX.clear();

    // Add the flips corresponding to the ones we've built
    const size_t f0Size = flips0.size();
    const size_t f1Size = flips1.size();
    const size_t f2Size = flips2.size();
    csX.flips.flipVec.resize(f0Size + f1Size + f2Size);
    #pragma omp parallel for
    for(uint i = 0; i < f0Size + f1Size + f2Size; ++i)
      if(i < f0Size)
        csX.flips.flipVec[i] = flips0[i];
      else if(i < f0Size + f1Size)
        csX.flips.flipVec[i] = flips1[i-f0Size];
      else if(i < f0Size + f1Size + f2Size)
        csX.flips.flipVec[i] = flips2[i-f0Size-f1Size];

    // Build the indices
    #pragma omp parallel for
    for(uint i = 0; i < 2; i++)
      if(i == 0)
        csX.flips.reindex();
      else if(i == 1)
        csX.dimensionMap.load(CCIndexVecVec({vertices, edges, faces}));

    return true;
  }

  // Make a cell complex from a list of vertices, faces and volumes
  bool ccFromFacesAndVolumes(CCStructure &cs, const CCIndexVec &vertices, const CCIndexVec &faces, 
      const UIntVecVec &faceVertices, const CCIndexVec &volumes, const IntVecVec &volumeFaces)
  {
    bool resultFaces = ccFromFaces(cs, vertices, faces, faceVertices);
    if(!resultFaces)
      mdxInfo << "ccFromFacesAndVolumes ccFromFaces returned and error" << endl;

    bool resultVolumes = ccAddVolumes(cs, faces, volumes, volumeFaces);
    if(!resultFaces)
      mdxInfo << "ccFromFacesAndVolumes ccAddVolumes returned and error" << endl;

    return resultFaces and resultVolumes;
  }

  // Create a cell complex from a list of vertices, edges and faces
  bool ccFromFacesAndVolumes(CCStructure &cs, const CCIndexVec &vertices, const CCIndexVec &edges, 
      const UIntPairVec &edgeVertices, const CCIndexVec &faces, const UIntVecVec &faceVertices, 
      const UIntVecVec &faceEdges, const CCIndexVec &volumes, const IntVecVec &volumeFaces)
  {
    bool resultFaces = ccFromFaces(cs, vertices, edges, edgeVertices, faces, faceVertices, faceEdges);
    if(!resultFaces)
      mdxInfo << "ccFromFacesAndVolumes ccFromFacesAndFaces returned and error" << endl;

    bool resultVolumes = ccAddVolumes(cs, faces, volumes, volumeFaces);
    if(!resultFaces)
      mdxInfo << "ccFromFacesAndVolumes ccAddVolumes returned and error" << endl;

    return resultFaces and resultVolumes;
  }

  // Add volumes to a cell complex
  bool ccAddVolumes(CCStructure &cs, const CCIndexVec &faces, const CCIndexVec &volumes, const IntVecVec &volumeFaces)
  {
    bool result = true;
    // Add the volumes
    for(uint i = 0; i < volumes.size(); i++) {
      BoundaryChain bc;
      for(int j : volumeFaces[i]) {
        if(j > 0)
          bc += +faces[j - 1]; // Index for faces is not 0 based, needs to be + or -
        else if(j < 0)
          bc += -faces[-j - 1];
        else {
          mdxInfo << "ccFromFacesAndVolumes Bad face index (0) in volume: " << i << endl;
          result = false;
          continue;
        }
      }
      if(!cs.addCell(volumes[i], bc)) {
        mdxInfo << "ccFromFacesAndVolumes Unable to add cell: " << i << endl;
        result = false;
      } 
    }
    return result;
  }

  // add known volumes from a map of faces to a CC
  bool addVolumesCC(CCStructure &cs, CCIndexDataAttr &indexAttr, std::map<int, std::set<CCIndex> > &volumeLabelToFacesMap)
  {
    bool allOk = true;

    std::set<CCIndex> addedFaces;
    typedef std::pair<int, std::set<CCIndex> > IntCCIdxP;

    forall(IntCCIdxP p, volumeLabelToFacesMap){
      mdxInfo << "add Volume " << p.first << " with " << p.second.size() << " faces. " << endl; 
      CCIndex newVolume = CCIndexFactory.getIndex();
      indexAttr[newVolume].label = p.first;
      BoundaryChain bcCell;
      for(CCIndex f : p.second){
        if(addedFaces.find(f) != addedFaces.end()){
          bcCell += -f;
        } else {
          bcCell += +f;
          addedFaces.insert(f);
        }
      }
      bool res = cs.addCell(newVolume, bcCell);
      if(!res){
	mdxInfo << "addVolumesCC Error ";
	if(confirmCCF(cs)) {
	  ccf::CCStructure &csX = ensureCCF(cs);
	  mdxInfo << QString::fromStdString(csX.error);
	}
	mdxInfo << endl;
        allOk = false;
      }
    }
    return allOk;
  }

  // Get the neighborhood (same dimension) of a cell within a radius, must be connected
  CCIndexSet neighbors(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndex c, float radius)
  {
    CCIndexVec lastC, newC;
    CCIndexSet nhbd;
    nhbd.insert(c);
    lastC.push_back(c);
    
    Point3d pos = indexAttr[c].pos;
    do {
      for(auto d : lastC)
        for(auto n : cs.neighbors(d)) {
          auto &nIdx = indexAttr[n];
          if(nhbd.count(n) == 0 and norm(nIdx.pos - pos) < radius) {
            newC.push_back(n);
            nhbd.insert(n);
          }
        }
      std::swap(lastC, newC);
      newC.clear();
    } while(!lastC.empty());
    return nhbd;
  }


  // Class to cache neighbors
  bool CellNeighbors::init(const CCStructure &cs, const CCIndexVec &cells, bool clear)
  {
    if(clear)
      neighbors.clear();

    // Lookup all the neighbors
    #pragma omp parallel for
    for(uint i = 0; i < cells.size(); ++i)
      neighbors[cells[i]] = cs.neighbors(cells[i]);

    return true;
  }

  // Class to find border distance
  bool BorderDistance::init(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &cells, 
                                                                                   bool clear, bool doJunctions)
  {
    if(clear) {
      _distanceB.clear();
      _minB.clear();
      _wall.clear();
      _distanceJ.clear();
      _minJ.clear();
    }

    // Calculate face positions
    CCIndexVec faces = cs.faces();
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i];
      auto &fIdx = indexAttr[f];
      auto faceVertices = cs.incidentCells(f, 0);
      fIdx.pos = Point3d(0,0,0);
      for(auto v : faceVertices)
        fIdx.pos += indexAttr[v].pos;
      fIdx.pos /= faceVertices.size();
    }

    // Make a map of all border vertices by label
    AttrMap<int, tbb::concurrent_unordered_set<CCIndex, std::hash<CCIndex> > > labelB;
    CCIndexVec edges = cs.edges();
    #pragma omp parallel for
    for(uint i = 0; i < edges.size(); i++) {
      CCIndex e = edges[i];
      for(auto flip : cs.matchV(e,CCIndex::Q,CCIndex::Q,CCIndex::Q)) {
        auto &f0Idx = indexAttr[flip.facet[0]];
        auto &f1Idx = indexAttr[flip.facet[1]];
        if(f0Idx.label != f1Idx.label) {
          auto pr = cs.edgeBounds(e);
          labelB[f0Idx.label].insert(pr.first);
          labelB[f0Idx.label].insert(pr.second);
          labelB[f1Idx.label].insert(pr.first);
          labelB[f1Idx.label].insert(pr.second);
          break;
        }
      }
    }

    // Find junctions by label
    AttrMap<int, tbb::concurrent_unordered_set<CCIndex, std::hash<CCIndex> > > labelJ;
    if(doJunctions) {
      for(auto &pr : labelB)
        for(CCIndex b : pr.second) {
          IntSet labels;
          for(auto flip : cs.matchV(b,CCIndex::Q,CCIndex::Q,CCIndex::Q)) {
            if(flip.interior == CCIndex::INFTY) // Infty counts as a cell
              labels.insert(-1);
            else {
              int label = indexAttr[flip.interior].label;
              if(label > 0)
                labels.insert(label);
            }
          }
          if(labels.size() > 2)
            labelJ[pr.first].insert(b);
        }
    }

    // Now find the distance, only search vertices with the same label
    #pragma omp parallel for
    for(uint i = 0; i < cells.size(); ++i) {
      CCIndex c = cells[i];
      auto &cIdx = indexAttr[c];
      double minBDist = DBL_MAX;
      CCIndex minB = CCIndex::UNDEF;
      double minJDist = DBL_MAX;
      CCIndex minJ = CCIndex::UNDEF;
      for(CCIndex v : labelB[cIdx.label]) {
        auto &vIdx = indexAttr[v];
        double s = norm(cIdx.pos - vIdx.pos);
        if(minBDist > s) {
          minBDist = s;
          minB = v;
        }
        if(doJunctions and labelJ[cIdx.label].count(v) == 1) {
          if(minJDist > s) {
            minJDist = s;
            minJ = v;
          }
        }
      }
      _distanceB[c] = minBDist;
      _minB[c] = minB;
      if(doJunctions) {
        _distanceJ[c] = minJDist;
        _minJ[c] = minJ;
      }

      // Fill in which wall it is on
      if(minBDist != DBL_MAX)
        for(CCIndex d : cs.incidentCells(minB, 2))
          if(d != c) {
            _wall[c] = d;
            break;
          }
    }
    return true;
  }

  // re-create the border flips, assumes top flips exist for internal cells of highest dimension
  bool createBorderFlips(ccf::CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    typedef CCStructure::FlipI Flip;
    typedef std::unordered_map<CCIndex, CCIndexSet> CCIndexCCIndexSetMap;

    // First delete all the Infty flips
    FlipVec flipV = cs.matchV(CCIndex::Q, CCIndex::Q, CCIndex::INFTY, CCIndex::Q);
    for(auto flip : flipV)
      cs.flips.erase(flip);
    flipV = cs.matchV(CCIndex::Q, CCIndex::Q, CCIndex::Q, CCIndex::INFTY);
    for(auto flip : flipV)
      cs.flips.erase(flip);

    // If above loops are slow, add a method to do this
    // Most procedures call this have probably deleted them already
    //cs.flips.deleteFlips(ccf::INFTY);

    CCIndexVec cells = cs.cellsOfDimension(cs.maxDimension() - 1);

    // Find border edges and faces do top flips
    CCIndexCCIndexSetMap borderJointCells;
    for(uint i = 0; i < cells.size(); i++) {
      CCIndex c = cells[i];
      auto cBounds = cs.bounds(c);

      // Find border edges, their top (infty) flips should have been deleted earlier
      FlipVec flipV = cs.matchV(c, CCIndex::Q, CCIndex::Q, CCIndex::TOP);
      if(flipV.size() == 0) {
        FlipVec flipV = cs.matchV(*cBounds.begin(), c, CCIndex::Q, CCIndex::Q);
        if(flipV.size() == 0) continue; // hanging edge / face
        else if(flipV.size() > 1) {
          mdxInfo << QString("createBorderFlips: Non-interior cell is indicent to %1 n-cells").
                     arg(flipV.size()) << endl;
          selectCell(cs, indexAttr, c);
          continue;
        }
        CCIndex i = flipV[0].interior;

        // This will be edges by vertex for 2D, or faces by edge for 3D 
        for(CCIndex b : cBounds)
          borderJointCells[b].insert(c);

        CCIndex facet0 = CCIndex::INFTY, facet1 = i;
        if(cs.maxDimension() == 3)
          std::swap(facet0, facet1);

        if(c == flipV[0].facet[0]) {
          if(cs.ro(*cBounds.begin(), c) == ccf::POS)
            cs.flips.insert(Flip(c, facet0, facet1, CCIndex::TOP));
          else
            cs.flips.insert(Flip(c, facet1, facet0, CCIndex::TOP));
        } else {
          if(cs.ro(*cBounds.begin(), c) == ccf::NEG)
            cs.flips.insert(Flip(c, facet0, facet1, CCIndex::TOP));
          else
            cs.flips.insert(Flip(c, facet1, facet0, CCIndex::TOP));
        }

      } else if(flipV.size() != 1) {
        mdxInfo << QString("createBorderFlips Cell found with %1 top flips").arg(flipV.size()) << endl;
        selectCell(cs, indexAttr, c);
      }
    }  
  
    for(auto &pr : borderJointCells) {
      CCIndex j = pr.first;
      while(pr.second.size() > 0) {
        CCIndex c = *pr.second.begin();
        auto flip = cs.matchFirst(j, c, CCIndex::Q, CCIndex::Q);
        if(pr.second.size() == 2) { // Special case if only 2 faces, we can assume they match
          if(flip.isValid()) {
            CCIndex c1 = *pr.second.rbegin();

            CCIndex facet0 = c, facet1 = c1;
            if(cs.maxDimension() == 3)
              std::swap(facet0, facet1);

            if(c == flip.facet[0])
              cs.flips.insert(Flip(j, facet0, facet1, CCIndex::INFTY));
            else
              cs.flips.insert(Flip(j, facet1, facet0, CCIndex::INFTY));

            pr.second.erase(c);
            pr.second.erase(c1);
          } else { // we need to find a pair of connected outside edges
            mdxInfo << "createBorderFlips Unable to find matching facet for infty flip" << endl;
            selectCell(cs, indexAttr, j);
          }
        } else { // Find matching pairs of edges to create infty flips
          CCIndex c1 = c;
          CCIndex prev = c;
          CCIndexSet seen;
          while(seen.insert(prev).second) {
            auto flips = cs.matchV(j, prev, CCIndex::Q, CCIndex::Q);
            for(auto flip : flips) {
              if(flip.facet[0] == prev) {
                if(seen.count(flip.facet[1]) == 0) {
                  if(pr.second.count(flip.facet[1]) > 0) {
                    c1 = flip.facet[1];
                    break;
                  } else {
                    prev = flip.facet[1];
                    break;
                  }
                }
              } else if(flip.facet[1] == prev) {
                if(seen.count(flip.facet[0]) == 0) {
                  if(pr.second.count(flip.facet[0]) > 0) {
                    c1 = flip.facet[0];
                    break;
                  } else {
                    prev = flip.facet[0];
                    break;
                  }
                }
              }
            }
            if(c != c1)
              break;
          }
          if(c == c1) {
            mdxInfo << "createBorderFlips Unable to find matching facet for infty flip" << endl;
            pr.second.erase(c);
            selectCell(cs, indexAttr, j);
          } else {
            CCIndex facet0 = c, facet1 = c1;
            if(cs.maxDimension() == 3)
              std::swap(facet0, facet1);

            if(c == flip.facet[0])
              cs.flips.insert(Flip(j, facet0, facet1, CCIndex::INFTY));
            else
              cs.flips.insert(Flip(j, facet1, facet0, CCIndex::INFTY));

            pr.second.erase(c);
            pr.second.erase(c1);
          }
        }
      }
    }
    return true;
  }

  // this will move to GraphAlgorithms
  std::map<int, double> dijkstra(std::set<int>& allLabels, std::map<IntIntPair, double>& wallAreas, 
                               std::set<int>& selectedCells, bool equalWeights, double cutOffDistance, double wallThreshold)
  {
  
      // the map (label->distance) for final results
      std::map<int, double> bestPaths;
  
  
      // the map (label->distance) for intermediate results
      std::map<int,double> bestSoFar;
  
      // multimap (distance->label) that acts as ordered map of current distances
      std::multimap<double, int> distanceLabelMap;
  
      // for initialization of above maps
      std::map<int, bool> labelInit;
  
      // init
      //forall(const vertex &v, S){
      for(int l : allLabels){
        if(!labelInit[l]){
          if(selectedCells.find(l) != selectedCells.end()){
            bestSoFar[l] = 0;
            distanceLabelMap.insert(std::make_pair(0,l));
          } else {
            bestSoFar[l] = cutOffDistance;
            distanceLabelMap.insert(std::make_pair(cutOffDistance,l));
          }
        } 
        labelInit[l] = true;
      }
  
      // map of label->vector of all neighbor labels
      std::map<int, std::vector<int> > labelNeighborMap;
  
      typedef std::pair<IntIntPair, double> IntIntDoublePair;
      forall(const IntIntDoublePair& p, wallAreas){
        if(p.second > wallThreshold){
          labelNeighborMap[p.first.first].push_back(p.first.second);
        }
      }
  
      // now dijkstra
      while(!distanceLabelMap.empty()){
  
        std::multimap<double, int>::iterator it = distanceLabelMap.begin();
        int currentLabel = it->second;
        double curentDis = it->first;
        distanceLabelMap.erase(it);
  
        if(bestPaths.find(currentLabel) != bestPaths.end()) continue; // already searched
        bestPaths[currentLabel] = curentDis; // this is the shortest way to currentlabel
  
        // go through all (unvisited) neighbors
        for(uint i=0; i<labelNeighborMap[currentLabel].size(); i++){
          int neighborLabel = labelNeighborMap[currentLabel][i];
          if(bestPaths.find(neighborLabel) == bestPaths.end()){ // not already searched neighbors
            // calc current distance
            double dis = 1;
            if(!equalWeights){
              IntIntPair p = std::make_pair(currentLabel,neighborLabel);
              dis = max(0.,1./wallAreas[p]);
            }
            double newDis = curentDis+dis;
            if(newDis<bestSoFar[neighborLabel]){ // if nearer than previously, update distance
              distanceLabelMap.insert(std::make_pair(newDis,neighborLabel)); // create a new entry
              bestSoFar[neighborLabel] = newDis;
            }
          }
        }
  
      }
  
    return bestPaths;
  }

  std::vector<CCIndex> cellBoundary(const CCStructure &cs, const CCIndexDataAttr &indexAttr, int label)
  {
    CCIndex v0, f0;
    std::vector<CCIndex> cellOutline;

    // find a face of the cell containing a border vertex
    for(CCIndex f : cs.faces()){
      if(indexAttr[f].label != label) continue;

      std::set<CCIndex> edges = cs.incidentCells(f,1);

      for(CCIndex e : edges){
        std::set<CCIndex> neighborFaces = cs.incidentCells(e,2);
        bool found = false;
        if(neighborFaces.size() == 0) found = true;
        for(CCIndex nf : neighborFaces){
          if(indexAttr[nf].label == label) continue;
          found = true;
        }
        if(found){
          // if we get here then the edge e is at the border of the cell
          std::set<CCIndex> borderVertices = cs.incidentCells(e,0); // now get a vertex of this edge
          f0 = f;
          v0 = (*borderVertices.begin());
          //mdxInfo << "v0 " << indexAttr[v0].pos << endl;
          break;
        }
      }
    }

    //mdxInfo << "v0 done" << indexAttr[v0].pos << "/" << label << endl;

    // v0 is on the border of a cell with label l containing f0:
    CellTuple tuple(cs,v0,f0);
    do
    {
      cellOutline.push_back(tuple[0]);
      //mdxInfo << "res " << indexAttr[tuple[0]].pos << endl;
      //indexAttr[tuple[0]].selected = true;
      while(indexAttr[tuple.other(2)].label == label)
      {
        tuple.flip(2,1);  // rotate around vertex to next edge
      }
      // reaching here means the other face is in a different cell
      tuple.flip(0,1); // edge is on boundary of cell; advance v

    } while(tuple[0] != v0);

    return cellOutline;
  }

  // get centroid of 3D polytope
  CentroidData<Point3d> polytopeCentroidData(CCIndex cell, const CCStructure &cs,
                                             const CCIndexDataAttr &attr)
  {
    if(!cs.hasCell(cell))
    {
      throw(QString("Tried to get polytopeCentroidData for nonexistent cell %1").
            arg(ccf::to_string(cell).c_str()));
    }
    if(cs.dimensionOf(cell) != 3)
    {
      throw(QString("Tried to get polytopeCentroidData for cell of dimension %1").
            arg(cs.dimensionOf(cell)));
    }

    CentroidData<Point3d> answer;

    // Iterate over the faces of the cell
    forall(CCIndex face, cs.incidentCells(cell,2))
    {
      // Get the vertices for the face (oriented outward)
      CCStructure::CellTuple tuple(cs,cell,face);
      if(tuple.totalOrientation() == ccf::POS) tuple.flip(0);
      std::vector<CCIndex> vs;
      do
      {
        vs.push_back(tuple[0]);
        tuple.flip(0,1);
      } while(vs[0] != tuple[0]);

      // One or two vertices => degenerate face
      // Zero area, no contribution to centroid
      if(vs.size() < 3) continue;

      // For now, we only *know* how to do this
      // for polytopes with triangular faces (Nuernberg 2013).
      uint NumPoints = vs.size();
      std::vector<Point3d> pos(NumPoints);
      for(uint i = 0 ; i < NumPoints ; i++) { pos[i] = attr[vs[i]].pos; }

      if(NumPoints == 3)
      {
        Point3d normal = (pos[1] - pos[0]) ^ (pos[2] - pos[0]);
        answer.measure += pos[0] * normal;

        Point3d ab = pos[0] + pos[1], ac = pos[0] + pos[2], bc = pos[1] + pos[2];
        answer.centroid.x() += normal.x() * (ab.x()*ab.x() + ac.x()*ac.x() + bc.x()*bc.x());
        answer.centroid.y() += normal.y() * (ab.y()*ab.y() + ac.y()*ac.y() + bc.y()*bc.y());
        answer.centroid.z() += normal.z() * (ab.z()*ab.z() + ac.z()*ac.z() + bc.z()*bc.z());
      }
      else // NumPoints > 3
      {
        // If the face has more than three sides,
        // we use the algorithm of (Klincsek 1980) to subdivide into triangles
        // and handle each triangle individually.
        std::vector< std::vector<double> > dists(NumPoints,std::vector<double>(NumPoints));
        std::vector< std::vector<uint> > ell(NumPoints,std::vector<uint>(NumPoints));

        // 1. compute edge lengths
        uint k = 1;
        for(uint i = 0 ; i < NumPoints ; i++)
        {
          uint j = (i+k) % NumPoints;
          dists[i][j] = norm(pos[i] - pos[j]);
        }

        // 2. iterate
        for(k = 2 ; k < NumPoints ; k++)
          for(uint i = 0 ; i < NumPoints ; i++)
          {
            uint j = (i+k) % NumPoints;
            dists[i][j] = norm(pos[i] - pos[j]);
            uint minEll = (i+1) % NumPoints;
            double minDist = dists[i][minEll] + dists[minEll][j];
            for(uint l = i+2 ; l < i+k ; l++)
            {
              int l2 = l % NumPoints;
              double distSum = dists[i][l2] + dists[l2][j];
              if(distSum < minDist)
              {
                minDist = distSum;
                minEll = l2;
              }
            }
            dists[i][j] += minDist;
            ell[i][j] = minEll;
          }

        // 3. make triangles
        std::vector< std::pair<uint,uint> > eis;
        eis.push_back(std::make_pair(0,NumPoints-1));
        for(uint c = 0 ; c < eis.size() ; c++)
        {
          uint i = eis[c].first, j = eis[c].second;
          if(j > i+1)
          {
            uint l = ell[i][j];
            eis.push_back(std::make_pair(i,l));
            eis.push_back(std::make_pair(l,j));

            // add this triangle to the accumulation
            Point3d normal = (pos[j] - pos[i]) ^ (pos[l] - pos[i]);
            answer.measure += pos[i] * normal;

            Point3d ab = pos[i] + pos[j], ac = pos[i] + pos[l], bc = pos[j] + pos[l];
            answer.centroid.x() += normal.x() * (ab.x()*ab.x() + ac.x()*ac.x() +
                                                 bc.x()*bc.x());
            answer.centroid.y() += normal.y() * (ab.y()*ab.y() + ac.y()*ac.y() +
                                                 bc.y()*bc.y());
            answer.centroid.z() += normal.z() * (ab.z()*ab.z() + ac.z()*ac.z() +
                                                 bc.z()*bc.z());
          }
        }
      }
    }

    // Divide out the global multiplicative factors
    answer.measure /= 6.;
    answer.centroid /= 48. * answer.measure;
    answer.measure = fabs(answer.measure);

    return answer;
  }

  CentroidData<Point3d> cellCentroidData(CCIndex cell, const CCStructure &cs, const CCIndexDataAttr &attr)
  {
    if(!cs.hasCell(cell))
      throw(QString("cellCentroidData: Cell %1 not in cell complex"));
    else
    {
      CentroidData<Point3d> answer;
      switch(cs.dimensionOf(cell))
      {
      case 0:
        answer.measure = 0;
        answer.centroid = attr[cell].pos;
        break;
      case 1:
      {
        std::pair<CCIndex,CCIndex> ep = cs.edgeBounds(cell);
        Point3d pos0 = attr[ep.first].pos, pos1 = attr[ep.second].pos;
        answer.measure = norm(pos0 - pos1);
        answer.centroid = 0.5 * (pos0 + pos1);
      }
      break;
      case 2:
      {
        // iterate to get points on walls
        CCStructure::CellTuple ct(cs,cell);
        std::vector<Point3d> pos;
        CCIndex firstV = ct[0];
        do
        {
          pos.push_back(attr[ct[0]].pos);
          ct.flip(0,1);
        } while(ct[0] != firstV);
      
        answer = polygonCentroidData(pos);
      }
      break;
      case 3:
        answer = polytopeCentroidData(cell, cs, attr);
      }
      return answer;
    }
  }

  // Intersection of two CCIndex sets
  CCIndexSet intersect(const CCIndexSet &set1, const CCIndexSet &set2)
  {
    CCIndexSet result;
    std::set_intersection(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(result, result.begin())); 
    return result;
  }

  // Find the maximum label value
  int calcMaxLabel(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    // Find the max for each dimension
    IntVec maxL(cs.maxDimension() + 1);
    #pragma omp parallel for
    for(int i = 0; i <= cs.maxDimension(); i++) {
      maxL[i] = INT_MIN;
      for(CCIndex c : cs.cellsOfDimension(i)) {
        auto it = indexAttr.find(c);
        if(it != indexAttr.end() and maxL[i] < it->second.label)
          maxL[i] = it->second.label;
      }
    }

    // Find the max of maxes
    int maxLabel= INT_MIN;
    for(int i = 0; i <= cs.maxDimension(); i++)
      if(maxLabel < maxL[i])
        maxLabel = maxL[i];

    return maxLabel;
  }
}
