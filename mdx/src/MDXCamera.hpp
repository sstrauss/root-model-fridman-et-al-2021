// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MDX_CAMERA_HPP
#define MDX_CAMERA_HPP

#include <MDXViewer/manipulatedCameraFrame.h>

// This class is overloaded so that the zoom behavior can be modified.
// In libqglviewer you cannot zoom very close, you jsut move through the
// surface.
// 
class MDXCameraFrame : public qglviewer::ManipulatedCameraFrame {
public:
  MDXCameraFrame();

  void wheelEvent(QWheelEvent* const event, qglviewer::Camera* const camera);

  double zoom() const { return _zoom; }
  void setZoom(double z) { _zoom = z; }

protected:
  double _zoom;
};

class MDXCamera : public qglviewer::Camera 
{
public:
  MDXCamera();

  void getOrthoWidthHeight(GLdouble &halfWidth, GLdouble &halfHeight) const;
  void resetZoom();
  void fitSphere(const qglviewer::Vec &center, double radius);

  double zoom() const { return _frame->zoom(); }
  void setZoom(double z) { _frame->setZoom(z); }
  MDXCameraFrame* frame() { return _frame; }
  const MDXCameraFrame* frame() const { return _frame; }

protected:
  MDXCameraFrame* _frame;
};

#endif
