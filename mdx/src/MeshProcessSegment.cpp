//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessSegment.hpp>
#include <MeshProcessSignal.hpp>

#include <tbb/concurrent_unordered_map.h>

namespace mdx 
{
  using std::make_pair;

  bool MeshRelabelFaces::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, int start, int step)
  {
    if(step < 1)
      step = 1;
    int label = start;
    if(label < 1)
      label = 1;

    forall(CCIndex f, cs.faces()) {
      CCIndexData &fIdx = indexAttr[f];
      fIdx.label = label;
      label += step;
    }
    return true;
  }
  REGISTER_PROCESS(MeshRelabelFaces);

  bool MeshRelabelCells::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, int start, int step)
  {
    if(step < 1)
      step = 1;
    int label = start;
    if(label < 1)
      label = 1;

    IntIntMap relabelMap;
    for(CCIndex f : cs.faces()) {
      CCIndexData &fIdx = indexAttr[f];
      if(fIdx.label > 0) {
        auto found = relabelMap.find(fIdx.label);
        if(found != relabelMap.end())
          fIdx.label = found->second;
        else {
          relabelMap[fIdx.label] = start;
          fIdx.label = start;
          start += step;
        }
      }
    }
    return true;
  }
  REGISTER_PROCESS(MeshRelabelCells);

  bool MeshLabel3DCellFaces::run(Mesh &mesh, const CCStructure &cs, CCIndexDataAttr &indexAttr, bool labelOutside)
  {
    std::map<Point2i, int> wfMap; // Map walls to faces
    for(CCIndex f : cs.faces()) {
      auto cobounds = cs.cobounds(f);
      if(cobounds.size() == 2) {
        CCIndex c1 = *cobounds.begin();
        CCIndex c2 = *cobounds.rbegin();
        Point2i wall(indexAttr[c1].label, indexAttr[c2].label);
        if(wall.x() > wall.y())
          std::swap(wall.x(), wall.y());
        auto iter = wfMap.find(wall);
        int label;
        if(iter == wfMap.end()) {
          label = mesh.nextLabel();
          wfMap[wall] = label;
        } else
          label = wfMap[wall];
        indexAttr[f].label = label;
      } else if(labelOutside) // Use 3D cell label
        indexAttr[f].label = indexAttr[*cobounds.begin()].label;
      else
        indexAttr[f].label = 0;
    }
    return true;
  }
  REGISTER_PROCESS(MeshLabel3DCellFaces);

  bool MeshRelabelVolumes::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, int start, int step)
  {
    if(step < 1)
      step = 1;
    int label = start;
    if(label < 1)
      label = 1;

    forall(CCIndex l, cs.volumes()) {
      CCIndexData &lIdx = indexAttr[l];
      lIdx.label = label;
      label += step;
    }
    return true;
  }
  REGISTER_PROCESS(MeshRelabelVolumes);

  bool MeshSegmentClear::run(const CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    CCIndexVec faces = activeFaces(cs, indexAttr);
    #pragma omp parallel for
    for(size_t i = 0; i < faces.size(); i++)
      indexAttr[faces[i]].label = 0;

    CCIndexVec vertices = activeVertices(cs, indexAttr);
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); i++)
      indexAttr[vertices[i]].label = 0;
    return true;
  }
  REGISTER_PROCESS(MeshSegmentClear);

  bool MeshSetLabel::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, IntIntAttr *labelMap, int label, const QString dimension)
  {
    CCIndexVec cells;
    if(dimension == "Vertices")
      cells = selectedVertices(cs, indexAttr);
    else if(dimension == "Faces")
      cells = selectedFaces(cs, indexAttr);
    else if(dimension == "Volumes")
      cells = selectedFaces(cs, indexAttr);
    else if(dimension == "All")
      cells = selectedCells(cs, indexAttr);
    else
      throw QString("%1:run() Unknown dimension: %2").arg(name()).arg(dimension);

    if(labelMap) {
      #pragma omp parallel for
      for(size_t i = 0; i < cells.size(); i++)
        (*labelMap)[indexAttr[cells[i]].label] = label;
    } else {
      #pragma omp parallel for
      for(size_t i = 0; i < cells.size(); i++)
        indexAttr[cells[i]].label = label;
    }

    return true;
  }
  REGISTER_PROCESS(MeshSetLabel);

  REGISTER_PROCESS(MeshVertexLabelsFromFaces);
  REGISTER_PROCESS(MeshVertexLabelsFromVolumes);

  // Vertex based segmentation
  bool SegmentMesh::initialize(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr)
  {
    // Create the neighbor list
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
    neighbors.init(cs, vertices);

    // Push the face label to the vertices
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); i++)
      indexAttr[vertices[i]].label = 0;
    vertexLabelsFromFaces(cs, indexAttr, vertices);

    // Put all neighbors of labelled vertices into queue
    for(uint i = 0; i < vertices.size(); ++i) {
      CCIndex v = vertices[i];
      CCIndexData &vIdx = indexAttr[v];
      if(vIdx.label > 0) {
        for(CCIndex n : neighbors(v)) {
          CCIndexData &nIdx = indexAttr[n];
          if(nIdx.label == 0 and !inQueue[n]) {
            inQueue[n] = true;
            Q.insert(make_pair(signalAttr[n], n));
          }
        }
      }
    }
    return true;
  }

  bool SegmentMesh::step(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, uint maxsteps)
  {
    // Process queue
    long steps = (maxsteps > 0 ? maxsteps : LONG_MAX);
    int count = 0;
    while(Q.size() > 0 and steps-- > 0) {
      if(!progressAdvance(0))
        break;

      auto qMax = Q.begin();
      CCIndex vMax = qMax->second;

      int label = 0;
      bool foundlabel = false, difflabels = false;
      for(CCIndex n : neighbors(vMax)) {
        CCIndexData &nIdx = indexAttr[n];
        if(nIdx.label > 0) {
          if(!foundlabel) {
            label = nIdx.label;
            foundlabel = true;
          } else if(label != nIdx.label)
            difflabels = true;
        }
      }
      if(foundlabel) {
        CCIndexData &vMaxIdx = indexAttr[vMax];
        if(difflabels)
          vMaxIdx.label = -1;
        else
          vMaxIdx.label = label;
        count++;

        for(CCIndex n : neighbors(vMax)) {
          CCIndexData &nIdx = indexAttr[n];
          if(nIdx.label == 0 and !inQueue[n]) {
            inQueue[n] = true;
            Q.insert(make_pair(signalAttr[n], n));
          }
        }
      }

      inQueue[vMax] = false;
      Q.erase(qMax);
    }

    if(count > 0)
      faceLabelsFromVertices(cs, indexAttr, activeFaces(cs, indexAttr));

    setStatus(QString("Segmenting mesh, %1 vertices left in queue").arg(Q.size()));

    if(Q.size() > 0)
      return true;
  
    return false;
  }
  REGISTER_PROCESS(SegmentMesh);

  bool SegmentMesh::finalize(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr)
  {
    if(!stringToBool(parm("Fill Corners")))
      return true;

    // Run one step of face segmentation to fill in the corners
    SegmentMeshFaces seg(*this);
    seg.initialize(cs, indexAttr, signalAttr);
    seg.step(cs, indexAttr, signalAttr, 0);
    return true;
  }

  // Face based segmentation
  bool SegmentMeshFaces::initialize(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr)
  {
    // Create the neighbor list
    CCIndexVec faces = activeFaces(cs, indexAttr);
    neighbors.init(cs, faces);

    // Put all neighbors of labelled faces into queue
    for(uint i = 0; i < faces.size(); ++i) {
      CCIndex f = faces[i];
      CCIndexData &fIdx = indexAttr[f];
      if(fIdx.label > 0) {
        for(CCIndex n : neighbors(f)) {
          CCIndexData &nIdx = indexAttr[n];
          if(nIdx.label == 0 and !inQueue[n]) {
            inQueue[n] = true;
            Q.insert(make_pair(signalAttr[n], n));
          }
        }
      }
    }
    return true;
  }

  bool SegmentMeshFaces::step(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, uint maxsteps)
  {
    // Process queue
    long steps = (maxsteps > 0 ? maxsteps : LONG_MAX);
    int count = 0;
    while(Q.size() > 0 and steps-- > 0) {
      if(!progressAdvance(0))
        break;

      auto qMax = Q.begin();
      CCIndex fMin = qMax->second;

      int label = 0;
      double sMin = DBL_MAX;
      for(CCIndex n : neighbors(fMin)) {
        CCIndexData &nIdx = indexAttr[n];
        if(nIdx.label > 0 and signalAttr[n] < sMin) {
          label = nIdx.label;
          sMin = signalAttr[n];
        }
      }
      if(label == 0)
        mdxInfo << "Error, zero label" << endl;

      indexAttr[fMin].label = label;
      count++;

      for(CCIndex n : neighbors(fMin)) {
        CCIndexData &nIdx = indexAttr[n];
        if(nIdx.label == 0 and !inQueue[n]) {
          inQueue[n] = true;
          Q.insert(make_pair(signalAttr[n], n));
        }
      }

      inQueue[fMin] = false;
      Q.erase(qMax);
    }
    setStatus(QString("Segmenting mesh, %1 vertices left in queue").arg(Q.size()));

    if(Q.size() > 0)
      return true;
  
    return false;
  }
  REGISTER_PROCESS(SegmentMeshFaces);

//  
//  bool SegmentClear::run(Mesh* mesh)
//  {
//    const std::vector<vertex>& vertices = mesh->activeVertices();
//    forall(const vertex& v, vertices)
//      v->label = 0;
//    mesh->updateTriangles();
//    mesh->updateLines();
//    return true;
//  }
//  REGISTER_PROCESS(SegmentClear);
//  
//  bool MeshRelabel::run(Mesh* m, int start, int step)
//  {
//    if(step < 1)
//      step = 1;
//    if(start < 1)
//      start = 1;
//    const std::vector<vertex>& vertices = m->activeVertices();
//    progressStart(QString("Relabeling current mesh.").arg(start).arg(step), vertices.size());
//    std::unordered_map<int, int> relabelMap;
//    int prog = vertices.size() / 100;
//    int i = 0;
//    forall(const vertex& v, vertices) {
//      if(v->label > 0) {
//        std::unordered_map<int, int>::iterator found = relabelMap.find(v->label);
//        if(found != relabelMap.end())
//          v->label = found->second;
//        else {
//          relabelMap[v->label] = start;
//          v->label = start;
//          start += step;
//        }
//      }
//      if((i % prog) == 0 and !progressAdvance(i))
//        userCancel();
//      ++i;
//    }
//    m->setLabel(start);
//    if(!progressAdvance(vertices.size()))
//      userCancel();
//    m->updateTriangles();
//    setStatus(QString("Total number of labels created = %1. Last label = %2").arg(relabelMap.size()).arg(start));
//    return true;
//  }
//  REGISTER_PROCESS(MeshRelabel);
//  
//  bool GrabLabelsSegment::run(Mesh* mesh1, Mesh* mesh2, float tolerence)
//  {
//    const vvGraph& S1 = mesh1->graph();
//    const vvGraph& S2 = mesh2->graph();
//  
//    // Find center of all cells (should be closed surfaces)
//    typedef std::pair<int, Point4f> IntPoint4fPair;
//    std::map<int, Point4f> posMap1;
//    forall(const vertex& v, S1)
//      if(v->label > 0)
//        posMap1[v->label] += Point4f(v->pos.x(), v->pos.y(), v->pos.z(), 1.0);
//    forall(const IntPoint4fPair& p, posMap1)
//      posMap1[p.first] /= posMap1[p.first].t();
//  
//    std::map<int, Point4f> posMap2;
//    forall(const vertex& v, S2)
//      if(v->label > 0)
//        posMap2[v->label] += Point4f(v->pos.x(), v->pos.y(), v->pos.z(), 1.0);
//    forall(const IntPoint4fPair& p, posMap2)
//      posMap2[p.first] /= posMap2[p.first].t();
//  
//    // For each label in mesh 1, find closest in mesh 2.
//    std::map<int, int> labelMap;
//    forall(const IntPoint4fPair& p1, posMap1) {
//      float minDist = 1e37;
//      int minLabel = 0;
//      forall(const IntPoint4fPair& p2, posMap2) {
//        float dist = (p1.second - p2.second).norm();
//        if(dist < minDist and dist < tolerence) {
//          minDist = dist;
//          minLabel = p2.first;
//        }
//      }
//      if(minLabel > 0)
//        labelMap[p1.first] = minLabel;
//    }
//    setStatus(QString("Updating %1 matching cells.").arg(labelMap.size()));
//  
//    // Update mesh labels
//    forall(const vertex& v, S1)
//      if(labelMap[v->label] > 0)
//        v->label = labelMap[v->label];
//      else
//        v->label = 0;
//  
//    mesh1->updateTriangles();
//    return true;
//  }
//  REGISTER_PROCESS(GrabLabelsSegment);
//  
//  bool LabelSelected::run(Mesh* mesh, int label)
//  {
//    const std::vector<vertex>& vertices = mesh->activeVertices();
//    if(label <= 0) {
//      forall(const vertex& v, vertices)
//        if(v->label > 0) {
//          if(label == 0)
//            label = v->label;
//          else if(label != v->label)
//            return setErrorMessage("At most one label can be contained in selected vertices");
//        }
//      if(label <= 0)
//        label = mesh->nextLabel();
//    }
//    forall(const vertex& v, vertices)
//      v->label = label;
//    mesh->updateTriangles();
//    mesh->updateLines();
//    return true;
//  }
//  REGISTER_PROCESS(LabelSelected);
//  
//  // Relabel 3D cells
//  bool RelabelCells3D::run(Mesh* mesh, int label_start, int label_step)
//  {
//    // Get the vertices
//    const vvGraph& S = mesh->graph();
//  
//    // Get cells by connected regions
//    VVGraphVec cellVertex;
//    VtxIntMap vertexCell;
//    mesh->getConnectedRegions(S, cellVertex, vertexCell);
//  
//    if(label_start == -1)
//      label_start = mesh->nextLabel();
//  
//    // Relabel vertices
//    forall(const vvGraph& S, cellVertex) {
//      forall(const vertex& v, S)
//        v->label = label_start;
//      label_start += label_step;
//    }
//  
//    if(label_start >= mesh->viewLabel())
//      mesh->setLabel(label_start + 1);
//  
//    // Tell the system the mesh color has changed
//    mesh->updateTriangles();
//    mesh->updateSelection();
//  
//    return true;
//  }
//  REGISTER_PROCESS(RelabelCells3D);
  
  bool MeshCombineRegions::step(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, double borderDist, double threshold)
  {
    progressStart("Combine Regions");

    // Make sure face position and size is correct
    const CCIndexVec &faces = cs.faces();
    // Find the distances of faces to the cell boundaries
    BorderDistance bd(cs, indexAttr, faces, true, true);

    // Average signal for each cell without border
    AttrMap<int, double> labelAvg, labelWeight;
    #pragma omp parallel for
    for(size_t i = 0; i < faces.size(); i++) {
      if(!progressAdvance())
        userCancel();
      CCIndex f = faces[i]; 
      auto &fIdx = indexAttr[f];
      if(fIdx.label <= 0 or bd.distanceB(f) < borderDist)
        continue;
      labelAvg[fIdx.label] += signalAttr[f] * fIdx.measure;
      labelWeight[fIdx.label] += fIdx.measure;
    }
    for(auto &pr : labelWeight)
      if(pr.second != 0)
        labelAvg[pr.first] /= pr.second;
  
    // Calculation of the average signal at the border
    AttrMap<IntIntPair, double> wallSignal, wallWeight;
    #pragma omp parallel for
    for(size_t i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i]; 
      if(!progressAdvance())
        userCancel();
      auto &fIdx = indexAttr[f];
      if(fIdx.label <= 0 or bd.distanceB(f) > borderDist /*or bd.distanceJ(f) > borderDist*/)
        continue;

      auto pr = make_pair(fIdx.label, indexAttr[bd.wall(f)].label);
      wallSignal[pr] += signalAttr[f] * fIdx.measure;
      wallWeight[pr] += fIdx.measure;
    }
  
    // Set of pairs of labels to be updated if the avg. border signal is not too much higher
    // than the average signals of the two adjacent cells
    std::set<IntIntPair> toUpdate;
    for(auto &ws : wallSignal) {
      if(ws.first.first >= ws.first.second)
        continue;
      double wallSignal = 0;
      if(wallWeight[ws.first] != 0)
        wallSignal = ws.second/wallWeight[ws.first];
      double cellSignal = (labelAvg[ws.first.first] + labelAvg[ws.first.second]) / 2.0;
      if(cellSignal > 0 and wallSignal/cellSignal < threshold)
        toUpdate.insert(make_pair(ws.first.first, ws.first.second));
    }
    if(toUpdate.size() == 0)
      return false;
    else
      setStatus(QString("Found %1 regions to combine").arg(toUpdate.size()));

    // Updating the mesh with the changed labels and updating those changed labels in
    // respective pairs further on in the set
    IntSet allLabels;
    for(auto &pr : toUpdate) {
      allLabels.insert(pr.first);
      allLabels.insert(pr.second);
    }
    AttrMap<int, int> labelMap;
    for(auto label : allLabels)
      labelMap[label] = label;

    size_t count = 0;
    for(auto &pr : toUpdate) {
      if(!progressAdvance(count++))
        userCancel();

      // Grab the first entry and update the label
      for(auto &lpr : labelMap)
        if(lpr.first == pr.first)
          lpr.second = pr.second;
    }

    // Update mesh
    #pragma omp parallel for
    for(size_t i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i]; 
      auto &fIdx = indexAttr[f];
      if(labelMap.count(fIdx.label) == 1)
        fIdx.label = labelMap[fIdx.label];
    }

    return true;
  }
  REGISTER_PROCESS(MeshCombineRegions);
  
  bool MeshAutoSegment::run(Mesh &mesh, const QString &ccName, const QString &signalName, bool updateView, bool normalize, 
               double gaussianRadiusCell, double localMinimaRadius, double gaussianRadiusWall, double normalizeRadius, 
               double borderDist, double threshold)
  {
    CCStructure &cs = mesh.ccStructure(ccName);
    CCIndexDataAttr &indexAttr = mesh.indexAttr();
    auto &signalAttr = mesh.signalAttr<double>(signalName);
    const CCIndexVec &vertices = cs.vertices();
    DoubleVec MeshSignal(vertices.size());
    CCDrawParms &cdp = mesh.drawParms(ccName);

    // Set the view to labels, so we can watch
    if(updateView) {
      cdp.setRenderChoice("Faces", "Labels");
      cdp.setGroupVisible("Faces", true);
      updateState();
      updateViewer();
    }
    
    // Clear the existing segmentation
    MeshSegmentClear clearSegment(*this);
    clearSegment.run(cs, indexAttr);
    if(updateView) {
      mesh.updateProperties(ccName);
      updateState();
      updateViewer();
    }

    // Save original mesh signal
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i)
      MeshSignal[i] = signalAttr[vertices[i]];
  
    // Blur the mesh, normally with same radius as local minima
    MeshGaussianBlur blurCells(*this);
    blurCells.run(cs, indexAttr, signalAttr, gaussianRadiusCell);
    if(updateView) {
      mesh.updateProperties(ccName);
      updateState();
      updateViewer();
    }
  
    // Find local minima
    MeshLocalMinima minima(*this);
    minima.run(mesh, cs, indexAttr, signalAttr, localMinimaRadius);
    if(updateView) {
      mesh.updateProperties(ccName);
      updateState();
      updateViewer();
    }
  
    // Restore mesh signal
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i)
      signalAttr[vertices[i]] = MeshSignal[i];
  
    // Blur the mesh for segmentation and merging over-segmented regions
    MeshGaussianBlur blurWalls(*this);
    blurWalls.run(cs, indexAttr, signalAttr, gaussianRadiusWall);
    if(updateView) {
      mesh.updateProperties(ccName);
      updateState();
      updateViewer();
    }
  
    // Normalize Mesh Signal if required
    if(normalize) {
      MeshNormalize normal(*this);
      normal.run(cs, indexAttr, signalAttr, normalizeRadius);
      if(updateView) {
        mesh.updateProperties(ccName);
        updateState();
        updateViewer();
      }
    }
  
    // Segment Mesh
    SegmentMesh segMesh(*this);
    segMesh.initialize(cs, indexAttr, signalAttr);
    while(segMesh.step(cs, indexAttr, signalAttr, segMesh.parm("Steps").toUInt())) {
      mesh.updateProperties(ccName);
      if(!progressAdvance())
        userCancel();
      if(updateView) {
        updateState();
        updateViewer();
      }
    }
    segMesh.finalize(cs, indexAttr, signalAttr);
    mesh.updateProperties(ccName);
    updateState();
    updateViewer();
  
    // Merge over-segmented regions
    MeshCombineRegions combine(*this);
    while(combine.step(cs, indexAttr, signalAttr, borderDist, threshold)) {
      mesh.updateProperties(ccName);
      if(!progressAdvance())
        userCancel();
      if(updateView) {
        updateState();
        updateViewer();
      }
    }
 
    // Restore mesh signal
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i)
      signalAttr[vertices[i]] = MeshSignal[i];

    mesh.updateProperties(ccName);
   
    return true;
  }
  REGISTER_PROCESS(MeshAutoSegment);
}
