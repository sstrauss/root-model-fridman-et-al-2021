//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MeshProcessMisc.hpp>
#include <CCUtils.hpp>
#include <MeshUtils.hpp>

// Misc processes for cell complexes
namespace mdx
{
  bool DebugDraw::initialize(QWidget *parent)
  {
    // Get the tissue and cell graph names
    SourceCC = parm("Source CC");
    if(SourceCC.isEmpty())
      SourceCC = currentMesh()->ccName();
    if(SourceCC.isEmpty())
      throw(QString("No input cell complex specified"));
    OutputCC = parm("Output CC");
    DrawNormals = stringToBool(parm("Draw Normals"));
    NormalSize = parm("Normal Size").toDouble();
    DrawNhbds = stringToBool(parm("Draw Nhbds"));
    NhbdSize = parm("Nhbd Size").toDouble();
    NhbdOffset = parm("Nhbd Offset").toDouble();

    return true;
  }

  bool DebugDraw::run(Mesh *mesh)
  {
    // Get the cell complexes
    if(!mesh->exists(SourceCC))
      throw(QString("Specified input cell complex does not exist"));
    CCStructure &src = mesh->ccStructure(SourceCC);
    CCStructure &out = mesh->ccStructure(OutputCC);
    out = CCStructure(2);

    // We also need the attribute map
    CCIndexDataAttr &indexAttr = mesh->indexAttr();

    if(DrawNormals or DrawNhbds) { 
      // Put the neighborhoods into the cell complex
      forall(CCIndex v, src.cellsOfDimension(0)) {
        CCIndexData &V = indexAttr[v];
        if(DrawNormals) {
          // Add the vertices
          CCIndex v1 = CCIndexFactory.getIndex();
          out.addCell(v1);
          CCIndex v2 = CCIndexFactory.getIndex();
          out.addCell(v2);

          CCIndexData &V1 = indexAttr[v1];
          CCIndexData &V2 = indexAttr[v2];

          V1.pos = V.pos;
          V2.pos = V.pos + normalized(V.nrml) * NormalSize;

          // Add the edge
          CCIndex e = CCIndexFactory.getIndex();
          out.addCell(e, +v1 -v2);
        }

        if(DrawNhbds) {
          CCIndexData &vIdx = indexAttr[v];
          CCIndexVec nbs = vertexNeighbors(src, v);
          int count = 1;
          for(auto n : nbs) {
            CCIndexData &nIdx = indexAttr[n];

            // Add the vertex
            CCIndex v1 = CCIndexFactory.getIndex();
            out.addCell(v1);
            CCIndex v2 = CCIndexFactory.getIndex();
            out.addCell(v2);
           
            CCIndexData &v1Idx = indexAttr[v1];
            CCIndexData &v2Idx = indexAttr[v2]; 

            v1Idx.pos = vIdx.pos;
            v2Idx.pos = vIdx.pos
              + normalized(nIdx.pos - vIdx.pos) * NhbdSize * count
              + vIdx.nrml * NhbdOffset * count;
            count++;

            CCIndex e = CCIndexFactory.getIndex();
            out.addCell(e, +v1 -v2); 
          }
        }
      }
    }
    mesh->drawParms(OutputCC).setGroupVisible("Vertices", true);
    mesh->drawParms(OutputCC).setGroupVisible("Edges", true);
    mesh->updateAll(OutputCC);

    return true;
  }
  REGISTER_PROCESS(DebugDraw);

  bool UpdateGeometry::run(CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    return updateGeometry(cs, indexAttr);
  }
  REGISTER_PROCESS(UpdateGeometry);

  bool CellComplexVerify::run()
  {
    Mesh *mesh = currentMesh();
    if(mesh->ccName().isEmpty())
      throw(QString("CellComplexVerify::run No current mesh"));

    const QString &ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw(QString("CellComplexVerify::run No current cell complex"));

		CCStructure &cs = mesh->ccStructure(ccName);
    mesh->updateProperties(ccName);

    if(!verifyCCStructure(cs, mesh->indexAttr()))
      throw(QString("CellComplexVerify::run Errors found in cell complex, check log window"));

    return true;
  }
  REGISTER_PROCESS(CellComplexVerify);

  bool CellComplexGlobalOrient::run()
  {
    Mesh *mesh = currentMesh();
    if(mesh->ccName().isEmpty())
      throw(QString("CellComplexGlobalOrient::run No current mesh"));

    const QString &ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw(QString("CellComplexGlobalOrient::run No current cell complex"));

    CCStructure &cs = mesh->ccStructure(ccName);

    // Flip all cells with negative global orientation.
    bool hasFlip;
    CCStructure::BoundaryChain gbnd = cs.boundary(+CCIndex::TOP);
    for(CCSignedIndex sx : gbnd) {
      if(sx.orientation() == ccf::NEG)
      {
	cs.reverseOrientation(~sx);
	hasFlip = true;
      }
    }
    if(hasFlip)
      mesh->updateAll(ccName);

    return true;
  }
  REGISTER_PROCESS(CellComplexGlobalOrient);

  bool CellComplexRepair::run()
  { 
    Mesh *mesh = currentMesh();
    if(mesh->ccName().isEmpty())
      throw(QString("CellComplexRepair::run No current mesh"));

    const QString &ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw(QString("CellComplexRepair::run No current cell complex"));
    
	CCStructure &cs = mesh->ccStructure(ccName);
    mesh->updateAll(ccName);

    ccf::CCStructure &csX = ensureCCF(cs);

    return createBorderFlips(csX, mesh->indexAttr());
  }
  REGISTER_PROCESS(CellComplexRepair);
  
  bool PrintFlipTable::run()
  {
    Mesh *mesh = currentMesh();
    if(mesh->ccName().isEmpty())
      throw QString("PrintFlipTable::run No current mesh");
 
    QString fileName = parm("File Name");
    if(fileName.isEmpty())
      throw QString("PrintFlipTable::run No current mesh");
    QFile outFile(fileName);
    if(!outFile.open(QIODevice::WriteOnly | QIODevice::Text))
      throw QString("PrintFlipTable::run Unable to open output file: %1").arg(fileName);

    QTextStream out(&outFile);

    CCStructure &cs = mesh->ccStructure(mesh->ccName());
    CCStructure::FlipVectorI flips = cs.matchV(CCIndex::Q,CCIndex::Q,CCIndex::Q,CCIndex::Q);

    out << "Cell complex \"" << mesh->ccName() << "\" is of maximum dimension "
	<< cs.maxDimension() << " and" << endl << "contains ";

    for(uint dim = 0 ; dim <= cs.maxDimension() ; dim++) {
      if(cs.cellCount(dim) == 0) 
        continue;
      out << cs.cellCount(dim) << " " << dim << "-cells";
      if(dim < cs.maxDimension()) out << ", ";
      if(dim+1 == cs.maxDimension()) out << "and ";
    }
    out << "." << endl << "There are " << flips.size() << " flips:" << endl;

    for(CCStructure::FlipI flip : flips) {
      out << ccf::to_string(flip).c_str() << " (";
      try {
        if(cs.ro(flip.joint, flip.facet[0]) == ccf::POS)
          out << "+,";
        else
          out << "-,";
      } catch(...) {
          out << "x,";
      }
      try {
        if(cs.ro(flip.facet[0], flip.interior) == ccf::POS)
          out << "+) | (";
        else
          out << "-) | (";
      } catch(...) {
          out << "x) | (";
      }
      try {
        if(cs.ro(flip.joint, flip.facet[1]) == ccf::POS)
          out << "+,";
        else
          out << "-,";
      } catch(...) {
          out << "x,";
      }
      try {
        if(cs.ro(flip.facet[1], flip.interior) == ccf::POS)
          out << "+)";
        else
          out << "-)";
      } catch(...) {
          out << "x)";
      }
      out << endl;
    }

    return true;
  }
  REGISTER_PROCESS(PrintFlipTable);

  bool ReduceDimension::run()
  { 
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw QString("%1::run No current mesh").arg(name());

    QString ccName = mesh->ccName();
    if(mesh->ccName().isEmpty())
      throw QString("%1::run No current cell complex").arg(name());

    auto &cs = mesh->ccStructure(ccName);

    if(cs.maxDimension() == 2)  // Currently only 2 or 3 are possible
      return true;
    
    // Get the cells
    auto vertices = cs.vertices();
    auto edges = cs.edges();
    auto faces = cs.faces();

    // Make maps for lookup
    CCIndexIntAttr vertexMap, edgeMap;
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); i++)
      vertexMap[vertices[i]] = i;
    #pragma omp parallel for
    for(uint i = 0; i < edges.size(); i++)
      edgeMap[edges[i]] = i;

    UIntPairVec eVertices(edges.size());
    #pragma omp parallel for
    for(uint i = 0; i < edges.size(); i++) {
      auto eb = cs.edgeBounds(edges[i]);
      eVertices[i] = std::make_pair(edgeMap[eb.first], edgeMap[eb.second]);
    }

    UIntVecVec fVertices(faces.size()), fEdges(faces.size());
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      auto fV = faceVertices(cs, faces[i]);
      fVertices[i].resize(fV.size());
      fEdges[i].resize(fV.size());
      for(uint j = 0; j < fV.size(); j++) {
        CCIndex v = fV[j];
        CCIndex n = fV[(j+1)%fV.size()];
        fVertices[i][j] = vertexMap[v];
        fEdges[i][j] = edgeMap[~cs.orientedEdge(v, n)];
      }
    }
    // Create the new cell complex
    QString ccNameOut = ccName + " 2D";
    auto &csOut = mesh->ccStructure(ccNameOut);
    csOut = CCStructure(2);
    mesh->updateAll(ccNameOut);
    //return ccFromFaces(csOut, vertices, edges, eVertices, faces, fVertices, fEdges);
    return ccFromFaces(csOut, vertices, faces, fVertices);
  }
  REGISTER_PROCESS(ReduceDimension);
}
