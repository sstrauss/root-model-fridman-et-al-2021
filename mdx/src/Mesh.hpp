//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_HPP
#define MESH_HPP

/**
 * \file Mesh.hpp
 *
 * This files contains the definition of a mesh for the Process API
 */

#include <Config.hpp>

#include <Types.hpp>
#include <Attributes.hpp>

#include <cuda/CudaExport.hpp>
#include <Stack.hpp>
#include <TransferFunction.hpp>
#include <CCDrawParms.hpp>

#include <CCF.hpp>
#include <VizAttribute.hpp>

#include <omp.h>
#include <QImage>
#include <QTimer>
#include <string.h>
#include <typeinfo>
#include <Mangling.hpp>

namespace mdx 
{
  /// Map of an integer to a host vector of 3 unsigned integers
  typedef std::unordered_map<int, HVec3U> IntHVec3uMap;
  /// Element in IntHVec3uMap
  typedef std::pair<int, HVec3U> IntHVec3uPair;

  class SetupProcess;
  
  /**
   * \class Mesh Mesh.hpp <Mesh.hpp>
   *
   * This class holds the actual mesh as a group of cell complexes and
   * properties associated with it
   */
  class mdx_EXPORT Mesh : public QObject
  {
    Q_OBJECT

    friend class SetupProcess;
  
  public:
    typedef std::unordered_map<QString, CCStructure>  CCMap;
    /**
     * Create an empty mesh attached to the stack
     */
    Mesh(int id, Stack* stack);
  
    /**
     * Mesh destructor
     */
    ~Mesh();
  
    //@{
    ///\name Global properties methods, mostly saved in project file
  
    /**
     * Id of the current mesh
     *
     * This is the same id used in the Process:mesh(int) method.
     */
    int id() const { return _id; }
  
    /**
     * Change the id of the mesh.
     *
     * Please do not use this method for meshes attached to a process.
     */
    void setId(int i) { _id = i; }
  
    /**
     * Id as seen by the user
     *
     * This is, typically id()+1
     */
    int userId() const { return _id + 1; }

    /**
     * Returns the stack associated to this mesh
     */
    Stack* stack() const { return _stack; }

    /**
     * Returns the name of the mesh file
     */
    const QString &file() const { return _file; }

    /**
     * Set the name of the mesh file.
     *
     * This is called when the mesh is loaded or saved. Processes that create
     * a new mesh should call this without a parameter to reset the filename to null.
     */
    void setFile(const QString &file = QString());

    /**
     * Returns true if the mesh is scaled
     */
    bool scaled() const { return _scaled; }

    /**
     * Set if the mesh is scaled
     */
    void setScaled(bool on = true) { _scaled = on; }
  
    /**
     * Returns true if the mesh is transformed
     */
    bool transformed() const { return _transformed; }

    /**
     * Set if the mesh is transformed
     */
    void setTransformed(bool on = true) { _transformed = on; }
  
    /**
     * Returns true if the bounding box is shown
     */
    bool showBBox() const { return _showBBox; }
  
    /**
     * Set if the bounding box is shown
     */
    void setShowBBox(bool on = true) { _showBBox = on; }
  
    /**
     * Returns the current label, without modifying it
     */
    int viewLabel() const { return _currLabel; }

    /**
     * Sets the current label
     */
    void setLabel(int l) { _currLabel = l; }

    /**
     * Increment the current label and return
     */
    int nextLabel() { return ++_currLabel; }

     //@}

    //@{
    ///\name Cell complex methods
 
    // updateAll: call when topology has changed
    void updateAll(const QString &ccName);
    // updatePositions: call when positions have changed
    void updatePositions(const QString &ccName);
    // updateProperties: call when properties (colour / signal / label) have changed
    void updateProperties(const QString &ccName);
    // updateVertexSelect: call when vertex selection changes
    void updateVertexSelect(const QString &ccName);
    // updateFaceSelect: call when face selection changes
    void updateFaceSelect(const QString &ccName);
    // updateVolumeSelect: call when volume selection changes
    void updateVolumeSelect(const QString &ccName);
    // updateSelect: call when volume selection changes
    void updateSelect(const QString &ccName);
    // updateSelectPositions: call when face selection changes
    void updateSelectPositions(const QString &ccName);
    // updateFaceLabel: call when face labelling changes
    void updateFaceLabel(const QString &ccName);
    // updateCellEdges: call when cell edges are to be updated
    void updateCellEdges(const QString &ccName);
    // updateDrawChoices: call when draw choices have been changed
    void updateDrawChoices(const QString &ccName);

    // Same updates for all graphs
    void updateAll();
    void updatePositions();
    void updateProperties();
    void updateVertexSelect();
    void updateFaceSelect();
    void updateVolumeSelect();
    void updateSelect();
    void updateSelectPositions();
    void updateFaceLabel();
    void updateDrawChoices();

    // ** Read/write cell complexes
    // Write all cell complexes
    bool writeCCStructures(QIODevice &file);
    // Write one cell complex
    bool writeCCStructure(QIODevice &file, const CCStructure &cs);
    // Write a single indec
    bool writeCCIndex(QIODevice &file, CCIndex index);
    // Read all cell complexes
    bool readCCStructures(QIODevice &file);
    // Read one cell complex
    bool readCCStructure(QIODevice &file, CCStructure &cs);
    // Read in a single index, creating if necessary
    bool readCCIndex(QIODevice &file, CCIndex &index);
  
    // ** Access and modification
    /// Is mesh empty, i.e. no cell complexes
    bool empty(void) const { return _ccs.empty(); }
    /// Return the number of cell complexes
    size_t size(void) const { return _ccs.size(); }
    /// Return the list of names of the cell complexes
    QStringList ccNames(void) const;
  
    /// Access the cell complex map
    std::unordered_map<QString, CCStructure> &ccs() { return _ccs; }
    /// Access (or create) the cell complex with the given name
    CCStructure& ccStructure(const QString &ccName);
    /// See if cell complex exists
    bool exists(const QString &ccName);
    /// Delete the given cell complex
    void erase(const QString &ccName);
    /// Delete all of the cell complex OpenGL buffers
    void clearBuffers(void);

    // Selection methods for vertices
    bool addSelectVertices(const QString &ccName, const std::vector<CCIndex> &vVec);
    bool removeSelectVertices(const QString &ccName, const std::vector<CCIndex> &vVec);
    bool clearSelectVertices(const QString &ccName);

    // Selection methods for faces
    bool addSelectFaces(const QString &ccName, const std::vector<CCIndex> &fVec);
    bool removeSelectFaces(const QString &ccName, const std::vector<CCIndex> &fVec);
    bool clearSelectFaces(const QString &ccName);
    bool addSelectLabel(const QString &ccName, int label);
    bool removeSelectLabel(const QString &ccName, int label);
    bool selectConnectedFaces(const QString &ccName, CCIndex f, bool select = true);

    // Label methods for faces
    bool setFaceLabel(const QString &ccName, int label, const CCIndexVec &fVec);
    bool fillLabel(const QString &ccName, int label, IntSet labels);
    bool fillSelect(const QString &ccName, int label);

    // Signal methods for faces
    bool setFaceSignal(const QString &ccName, CCIndexDoubleAttr &signalAttr, double signal, const CCIndexVec &fVec);

    // Selection methods for vertices
    bool addSelectVolumes(const QString &ccName, const std::vector<CCIndex> &vVec);
    bool removeSelectVolumes(const QString &ccName, const std::vector<CCIndex> &vVec);
    bool clearSelectVolumes(const QString &ccName);

    // Attributes for cells, GUI, and cell complexes
  
    // Data associated with every CCIndex
    CCIndexDataAttr &indexAttr(void);
    CCIndexDataAttr &indexData(void) __attribute__((deprecated)) { return indexAttr(); }
    CCSignedIndexDataAttr &signedIndexAttr(void);
    CCSignedIndexDataAttr &signedIndexData(void) __attribute__((deprecated)) { return signedIndexAttr(); }

    /// Drawing and GUI parameters attribute, saved to mesh
    ParmsAttr &drawParmsAttr();
    /// Return the parameters of the given name
    CCDrawParms& drawParms(const QString &ccName); 

    /// General parameters associated with CCStructures, user extendable
    QString &ccAttr(const QString &ccName, const QString &propName);

    /// Call to refresh the cell complex names to the widget
    void refreshNames(void);

    //@}
   
    //@{
    ///\name Mesh Attributes. These are saved along with the mesh and can be extended by the user.

    /**
     * Get the mesh attributes
     */
    const Attributes &attributes() const
    {
      return _attributes;
    }

    Attributes &attributes()
    {
      return _attributes;
    }

    // A general interface to retrieve VizAttributes, given an attribute name, type, and color map.
    template<typename Key, typename Value>
      VizAttribute<Key> vizAttribute(const QString &attrName, const QString &colorMapName)
    {
      return VizAttribute<Key>(attributes().attrMap<Key,Value>(attrName),
                               attributes().attrMap<QString, ColorMap>("#ColorMaps#")[colorMapName]);
    }

    template<typename Key>
      VizAttribute<Key> vizAttribute(const QString &attrName, const QString &attrType,
                                     const QString &colorMapName)
    {
      if(attrType == "Double")
        return vizAttribute<Key,double>(attrName,colorMapName);
      else if(attrType == "Point2d")
        return vizAttribute<Key,Point2d>(attrName,colorMapName);
      else if(attrType == "Point3d")
        return vizAttribute<Key,Point3d>(attrName,colorMapName);
      else if(attrType == "Colorb")
        return vizAttribute<Key,Colorb>(attrName,colorMapName);
      else if(attrType == "Int")
        return vizAttribute<Key,int>(attrName,colorMapName);
      else
        throw(QString("Mesh::vizAttribute: Could not understand attribute type %1").
              arg(attrType));
    }

    /**
     * Reset the whole mesh and its attributes
     *
     * In the end, the mesh is empty, so doesn't contain cells, texture and is neither 
     * scaled nor transformed. All of the attributes are deleted.
     *
     * The next label stays untouched, as it is important to keep coordination between 
     * different meshes/stacks.
     */
    void reset();
    //@}
  
    //@{
    ///\name Structure
  
    // Get the label from a cell complex cell, with parents
    int getLabel(CCIndex c, const IntIntAttr *parents);
  
    /**
     * Returns the label or parent, given a label
     */
    int getLabel(int label, const IntIntAttr &parents);

    /**
     * Reference to the map from each wall to the length of the wall.
     *
     * The length is calculated as the sum of the edges making the wall. This means the 
     * number returned is will depend on the discretization of the surface.
     *
     * \note You shouldn't need to modify this structure directly. You should
     * instead call Mesh::updateWallGeometry()
     */
    IntIntFloatAttr &wallGeom() 
    {
      return _attributes.attrMap<IntIntPair, float>("Wall Geom");
    }

     /**
     * Reference to the map from labels to the set of neighbor labels.
     *
     * A label is neighbors of another ones if they share at least an edge of the mesh.
     *
     * \note You shouldn't need to modify this structure directly. You should
     * instead call Mesh::updateWallGeometry()
     */
    IntIntSetMap &labelNeighbors() { return _labelNeighbors; }

    /**
     * Returns the map from labels to the set of neighbors labels.
     */
    const IntIntSetMap &labelNeighbors() const { return _labelNeighbors; }
  
    //@}
  
    //@{
    ///\name Surface methods
    /**
     * Return the transfer function used to draw the surface in normal mode
     */
    //TransferFunction surfFct() const { return _surf_fct; }

    /**
     * Change the transfer function used to draw the surface in normal mode
     */
    //void setSurfFct(const TransferFunction &f)
    //{
      //if(_surf_fct != f) {
        //_surf_fct = f;
        //changed_surf_function = true;
      //}
    //}
    /**
     * Returns true if the normal transfer function has been changed during the
     * current process.
     */
    //bool surfFctChanged() const { return changed_surf_function; }
  
    /**
     * Return the transfer function used to draw the surface in heat mode
     */
    //TransferFunction heatFct() const { return _heat_fct; }

    /**
     * Change the transfer function used to draw the surface in heat mode
     */
    //void setHeatFct(const TransferFunction &f)
    //{
      //if(_heat_fct != f) {
        //_heat_fct = f;
        //changed_heat_function = true;
      //}
    //}
    /**
     * Returns true if the heat transfer function has been changed during the
     * current process.
     */
    //bool heatFctChanged() const { return changed_heat_function; }
  
    /**
     * Get the current opacity level of the surface
     */
    float opacity() const { return _opacity; }

    /**
     * Change the current opactity level of the surface
     */
    void setOpacity(float f)
    {
      if(f < 0)
        _opacity = 0;
      else if(f > 1)
        _opacity = 1;
      else
        _opacity = f;
    }
    /**
     * Get the current brightness of the surface
     */
    float brightness() const { return _brightness; }

    /**
     * Change the current brightness of the surface
     */
    void setBrightness(float f)
    {
      if(f < 0.0)
        _brightness = 0.0;
      else if(f > 1.0)
        _brightness = 1.0;
      else
        _brightness = f;
    }
  
    /**
     * Returns if the back surface is culled
     */
    bool culling() const { return _culling; }

    /**
     * Cull the back surface
     */
    void setCulling(bool cul) { _culling = cul; }

     /**
     * Returns if cell clipping is on
     */
    bool cellClipping() const { return _cellClipping; }

    /**
     * Set the cell clipping
     */
    void setCellClipping(bool cellClipping) { _cellClipping = cellClipping; } 

    /**
     * Return if the surface is rendered blended or not
     */
    bool blending() const { return _blending; }

    /**
     * Set the blending attribute
     */
    void setBlending(bool b) { _blending = b; }
  
    /**
     * Report if the surface is rendered with shading or not
     */
    bool shading() const { return _shading; }

    /**
     * Set the shading attribute
     */
    void setShading(bool shade) { _shading = shade; }

    /**
     * Returns if we should use parents or labels
     */
    bool useParents() { return _useParents; }

    /**
     * Show the color of the surface
     */
    void setUseParents(bool val) { _useParents = val; }

    /**
     * True if the surface has a texture attached to it
     */
    bool &hasImgTex() 
    { 
      return _attributes.attrMap<QString, bool>("HasImageTex")["HasImageTex"];
    }

    /**
     * Get the current texture attached to the surface
     */
    const QImage &imgTex() const { return _image; }
    QImage &imgTex() { return _image; }

    /**
     * Set the texture attached to the surface
     */
    void setImgTex(const QImage &img)
    {
      hasImgTex() = true;
      _image = img;
    }

    /**
     * Remove any texture attached to the surface
     */
    void clearImgTex()
    {
      _imgtex = false;
      _attributes.attrMap<QString, QByteArray>("ImageTex").erase("ImageTex");
      _image = QImage();
    }
    //@}
    
    //@{
    /**
     * \name Labeling methods. Labels are used to define cells, zones, or any other grouping
     * of CCIndex values. They are also used for cell segmentation and as indices into the heatmaps.
     * They are  stored in attribute maps as <CCIndex, int>.
     */

    /// The current labeling
    const QString &labeling();

    /// Set the current labeling
    bool setLabeling(const QString &labeling);

    /// See if signal attribute exists
    bool labelingExists(const QString &labeling);

    /// Get a labeling map
    IntIntAttr &labelMap(const QString &labeling);
    IntIntAttr &labelMap() { return labelMap(labeling()); }

    /// Old method for parents
    IntIntAttr &parents() __attribute__((deprecated))
    {
      return labelMap("Parents");
    }

    /// Get a label, taking into account the current labeling map
    int getLabel(int label);

    /// Set values in a labeling map for some labels and update the Gui
    bool setLabelLabeling(const QString &ccName, const QString &labeling, int label, const IntSet &labels);

    /// Get the labeling list
    QStringList labelingAttrList();

    /// Names for the labeling
    IntQStringAttr &labelName(const QString &labeling);
    IntQStringAttr &labelName() { return labelName(labeling()); }

    //@}

    //@{
    /**
     * \name Signal methods. The signal can arise from a fluorescence intensity 
     * projection, a curvature projection, or from another process.  They are stored 
     * in attribute maps that have a ColorMap with range and units associated to them.
     */
    const QString &signal();

    /// Set the current signalmap
    bool setSignal(const QString &signal);

    /// See if signal attribute exists
    bool signalExists(const QString &signal);
    bool signalExists() { return signalExists(signal()); }

    /// Get type of signal attribute
    QString signalType(const QString &signal);
    QString signalType() { return signalType(signal()); }

    /// Get the signal map for various data types
    template<typename T> AttrMap<CCIndex,T> &signalAttr(const QString &signal)
    {
      throw(QString("signalAttr: Unsupported signal type %1 requested.")
            .arg(qdemangle(typeid(T).name())));
    }

    /// Get the bounds of the first dimension of the signal
    const Point2d &signalBounds(const QString &signal);
    const Point2d &signalBounds() { return signalBounds(signal()); }
    
    /// Set the bounds of the first dimension of the signal
    bool setSignalBounds(const Point2d &signalBounds, const QString &signal);
    bool setSignalBounds(const Point2d &signalBounds) { return setSignalBounds(signalBounds, signal()); }

    /// Get the unit of the first dimension of the signal
    const QString &signalUnit(const QString &signal);
    const QString &signalUnit() { return  signalUnit(signal()); }
    
    /// Set the unit of the first dimension of the signal
    bool setSignalUnit(const QString &signalUnit, const QString &signal);    
    bool setSignalUnit(const QString &signalUnit) { return setSignalUnit(signalUnit, signal()); }    

    /// Get the signal colormap name
    QString signalColorMapName(const QString &signal);
    QString signalColorMapName() { return signalColorMapName(signal()); }

    /// Get the signal colormap
    ColorMap &signalColorMap(const QString &signal);
    ColorMap &signalColorMap() { return signalColorMap(signal()); }

    /// Get the signal as a VizAttribute
    VizAttribute<CCIndex> signalVizAttribute(const QString &signal);
    VizAttribute<CCIndex> signalVizAttribute() { return signalVizAttribute(signal()); }
    
    /// Get the signal map choice list
    QStringList signalAttrList();

    /// Erase signal
    bool signalErase(const QString &signal);
    bool signalErase() { return signalErase(signal()); }    
    //@}

    //@{
    /**
     * \name Heatmap methods. Heatmaps are generated by quantification of geometry, the 
     * signal, etc. and are based on a particular labeling. That means there is one heat
     * values per label. They are  stored in attribute maps that have a ColoMap with range 
     * and units associated to them.
     */
    const QString &heat();

    /// Set the current heatmap
    bool setHeat(const QString &heat);

    /// See if heat map attribute exists
    bool heatExists(const QString &heat, const QString &labeling);
    bool heatExists(const QString &heat) { return heatExists(heat, labeling()); }
    bool heatExists() { return heatExists(heat(), labeling()); }

    /// Get type of heat map attribute
    QString heatType(const QString &heat, const QString &labeling);
    QString heatType(const QString &heat) { return heatType(heat, labeling()); }
    QString heatType() { return heatType(heat(), labeling()); }

    /// Get the heat map signal for various data types
    template<typename T> AttrMap<int,T> &heatAttr(const QString &heat, const QString &labeling)
    {
      throw(QString("heatAttr: Unsupported heat type %1 requested.")
            .arg(qdemangle(typeid(T).name())));
    }
    template<typename T> AttrMap<int,T> &heatAttr(const QString &heat)
    { return heatAttr<T>(heat, labeling()); }
    template<typename T> AttrMap<int,T> &heatAttr()
    { return heatAttr<T>(heat(), labeling()); }

    /// Get the heat bounds for the first channel
    const Point2d &heatBounds(const QString &heat, const QString &labeling);
    const Point2d &heatBounds(const QString &heat) { return heatBounds(heat, labeling()); }
    const Point2d &heatBounds() { return heatBounds(heat(), labeling()); }
    
    /// Set the heat bounds for the first channel
    bool setHeatBounds(const Point2d &heatBounds, const QString &heat, const QString &labeling);
    bool setHeatBounds(const Point2d &heatBounds, const QString &heat)
    { return setHeatBounds(heatBounds, heat, labeling()); }
    bool setHeatBounds(const Point2d &heatBounds) { return setHeatBounds(heatBounds, heat(), labeling()); }

    /// Get the heat unit for the first channel
    const QString &heatUnit(const QString &heat, const QString &labeling);
    const QString &heatUnit(const QString &heat) { return heatUnit(heat, labeling()); }
    const QString &heatUnit() { return  heatUnit(heat(), labeling()); }
    
    /// Set the heat unit for the first channel
    bool setHeatUnit(const QString &heatUnit, const QString &heat, const QString &labeling);
    bool setHeatUnit(const QString &heatUnit, const QString &heat)
    { return setHeatUnit(heatUnit, heat, labeling()); }
    bool setHeatUnit(const QString &heatUnit) { return setHeatUnit(heatUnit, heat(), labeling()); }

    /// Get the heat colormap name
    QString heatColorMapName(const QString &heat, const QString &labeling);
    QString heatColorMapName(const QString &heat) { return heatColorMapName(heat, labeling()); }
    QString heatColorMapName() { return heatColorMapName(heat(), labeling()); }

    /// Get the heat colormap
    ColorMap &heatColorMap(const QString &heat, const QString &labeling);
    ColorMap &heatColorMap(const QString &heat) { return heatColorMap(heat, labeling()); }
    ColorMap &heatColorMap() { return heatColorMap(heat(), labeling()); }

    /// Get the heat as a VizAttribute
    VizAttribute<int> heatVizAttribute(const QString &heat, const QString &labeling);
    VizAttribute<int> heatVizAttribute(const QString &heat) { return heatVizAttribute(heat, labeling()); }
    VizAttribute<int> heatVizAttribute() { return heatVizAttribute(heat(), labeling()); }
    
    /// Get the heat map choice list
    QStringList heatAttrList(const QString &labeling, const QString &type);
    QStringList heatAttrList(const QString &labeling);
    QStringList heatAttrList() { return heatAttrList(labeling()); }

    /// Erase heat
    bool heatErase(const QString &heat, const QString &labeling);
    bool heatErase(const QString &heat) { return heatErase(heat, labeling()); }    
    bool heatErase() { return heatErase(heat(), labeling()); }    

    //@}


//    IntFloatAttr &labelHeat();
//    /**
//     * Reference to map of heat data by label for a labeling
//     */
//    IntFloatAttr &getLabelHeatMap(const QString &labeling);
//
//    /**
//     * Get the unit used for the current heat map, also shown on the color bar.
//     */
//    const QString &heatMapUnit() 
//    {
//      return _attributes.attrMap<QString, QString>("Cell Heat Unit")["Cell Heat Unit"];
//    }
//    /**
//     * Set the unit used for the current heat map, also shown on the color bar.
//     */
//    bool setHeatMapUnit(const QString &heatMapUint) 
//    {
//      _attributes.attrMap<QString, QString>("Cell Heat Unit")["Cell Heat Unit"] = heatMapUint;
//      return true;
//    }
//
//    /**
//     * Get the upper and lower bounds for the heat.
//     */
//    const Point2f &heatMapBounds() 
//    {
//      return _attributes.attrMap<QString, Point2f>("Cell Heat Bounds")["Cell Heat Bounds"];
//    }
//    /**
//     * Set the upper and lower bounds for the heat.
//     */
//    bool setHeatMapBounds(const Point2f &heatMapBounds) 
//    {
//      _attributes.attrMap<QString, Point2f>("Cell Heat Bounds")["Cell Heat Bounds"] = heatMapBounds;
//      return true;
//    }
//
//    /**
//     * Scale the heat map bounds to the existing range.
//     */
//    Point2f calcHeatMapBounds();
//
//    /** 
//     * Reference to map of heat data by wall (i.e. pair of vertex id) to intensity
//     */
//    IntIntFloatAttr &wallHeat() 
//    {
//      return _attributes.attrMap<IntIntPair, float>("Wall Heat");
//    }
//
//    /**
//     * Upper and lower bounds for the heat.
//     */
//    Point2f &wallHeatBounds() 
//    {
//      return _attributes.attrMap<QString, Point2f>("Wall Heat Bounds")["Wall Heat Bounds"];
//    }
//
//    /**
//     * Wall heat unit
//     */
//    QString &wallHeatUnit() 
//    {
//      return _attributes.attrMap<QString, QString>("Wall Heat Unit")["Wall Heat Unit"];
//    }
//    //@}
  
  
    //@{ \name Custom attributes for the renderer
 
    /// Custom attributes for a vertex
    bool customVertexExists(const QString &ccName, const QString &attrName);

    /// Get type of a custom vertex attribute
    QString customVertexType(const QString &ccName, const QString &attrName);

    /// Get the signal map for various data types
    template<typename T> AttrMap<CCIndex, T> &customVertexAttr(const QString &ccName, const QString &attrName)
    {
      throw QString("customVertexAttr: Unsupported type %1 requested.").arg(qdemangle(typeid(T).name()));
    }

    /// Get the custom vertex attribute's colormap name
    QString customVertexColorMapName(const QString &ccName, const QString &attrName);

    /// Get the custom vertex attribute's colormap name
    ColorMap &customVertexColorMap(const QString &ccName, const QString &attrName);

    /// Get the custom vertex attribute as a VizAttribute
    VizAttribute<CCIndex> customVertexVizAttribute(const QString &ccName, const QString &attrName);
    
    /// Get a list of the custom vertex attributes
    QStringList customVertexAttrList(const QString &ccName);

    /// Erase a custom custom vertex attribute
    bool customVertexErase(const QString &ccName, const QString &attrName);

    /// Set the custom vertex attribute point size
    bool customVertexSetPointSize(const QString &ccName, const QString &attrName, double pointSize);

    /// Get the custom vertex attribute point size
    double customVertexPointSize(const QString &ccName, const QString &attrName);

    //@}

    //@{ \name Cell axis methods 
    
    /// See if a cell axis exists
    bool cellAxisExists(const QString &ccName, const QString &attrName, const QString &labeling);
    bool cellAxisExists(const QString &ccName, const QString &attrName) { return cellAxisExists(ccName, attrName, labeling()); }

    /// Get a list of the cell axes
    QStringList cellAxisAttrList(const QString &ccName, const QString &labeling);
    QStringList cellAxisAttrList(const QString &ccName) { return cellAxisAttrList(ccName, labeling()); }

    /// Get the signal map for various data types
    /// Matrix is stored as 1st two eigenvectors and then values
    IntSymTensorAttr &cellAxisAttr(const QString &ccName, const QString &attrName, const QString &labeling);
    IntSymTensorAttr &cellAxisAttr(const QString &ccName, const QString &attrName) { return cellAxisAttr(ccName, attrName, labeling()); }

    /// Get the cell axis attribute's colormap name
    QString cellAxisColorMapName(const QString &ccName, const QString &attrName, const QString &labeling);
    QString cellAxisColorMapName(const QString &ccName, const QString &attrName) { return cellAxisColorMapName(ccName, attrName, labeling()); }

    /// Get the cell axis attribute's colormap name
    ColorMap &cellAxisColorMap(const QString &ccName, const QString &attrName, const QString &labeling);
    ColorMap &cellAxisColorMap(const QString &ccName, const QString &attrName) { return cellAxisColorMap(ccName, attrName, labeling()); }

    /// Erase a cell axis
    bool cellAxisErase(const QString &ccName, const QString &attrName, const QString &labeling);
    bool cellAxisErase(const QString &ccName, const QString &attrName) { return cellAxisErase(ccName, attrName, labeling()); }

    /// Get the cell axis line width
    double cellAxisLineWidth(const QString &ccName, const QString &attrName, const QString &labeling);
    double cellAxisLineWidth(const QString &ccName, const QString &attrName) { return cellAxisLineWidth(ccName, attrName, labeling()); }

    /// Set the cell axis line width
    bool cellAxisSetLineWidth(const QString &ccName, const QString &attrName, const QString &labeling, double lineWidth);
    bool cellAxisSetLineWidth(const QString &ccName, const QString &attrName, double lineWidth)
           { return cellAxisSetLineWidth(ccName, attrName, labeling(), lineWidth); }

    /// Get the cell axis bounds
    const Point2d &cellAxisBounds(const QString &ccName, const QString &attrName, const QString &labeling, uint channel);
    const Point2d &cellAxisBounds(const QString &ccName, const QString &attrName, uint channel) 
        { return cellAxisBounds(ccName, attrName, labeling(), channel); }

    /// Set the cell axis bounds
    bool cellAxisSetBounds(const QString &ccName, const QString &attrName, const QString &labeling, uint channel, const Point2d &bounds);
    bool cellAxisSetBounds(const QString &ccName, const QString &attrName, uint channel, const Point2d &bounds)
           { return cellAxisSetBounds(ccName, attrName, labeling(), channel, bounds); }

    /// Get the cell axis unit
    const QString &cellAxisUnit(const QString &ccName, const QString &attrName, const QString &labeling);
    const QString &cellAxisUnit(const QString &ccName, const QString &attrName) { return cellAxisUnit(ccName, attrName, labeling()); }

    /// Set the cell axis unit
    bool cellAxisSetUnit(const QString &ccName, const QString &attrName, const QString &labeling, const QString &unit);
    bool cellAxisSetUnit(const QString &ccName, const QString &attrName, const QString &unit)
           { return cellAxisSetUnit(ccName, attrName, labeling(), unit); }

    /// Attribute for label centers and normals
    IntMatrix2x3dAttr &cellAxisCentersNormals(const QString &ccName, const QString &labeling);
    IntMatrix2x3dAttr &cellAxisCentersNormals(const QString &ccName) { return cellAxisCentersNormals(ccName, labeling()); }

    //@}
 
    //@{ \name Axis methods, indexed by CCIndex
    
    /// See if a axis exists
    bool axisExists(const QString &ccName, const QString &attrName);

    /// Get a list of the axes
    QStringList axisAttrList(const QString &ccName);

    /// Get the signal map for various data types
    /// Matrix is stored as 1st two eigenvectors and then values
    CCIndexSymTensorAttr &axisAttr(const QString &ccName, const QString &attrName);

    /// Get the axis attribute's colormap name
    QString axisColorMapName(const QString &ccName, const QString &attrName);

    /// Get the custom vertex attribute's colormap name
    ColorMap &axisColorMap(const QString &ccName, const QString &attrName);

    /// Erase a axis
    bool axisErase(const QString &ccName, const QString &attrName);

    /// Get the axis line width
    double axisLineWidth(const QString &ccName, const QString &attrName);

    /// Set the axis line width
    bool axisSetLineWidth(const QString &ccName, const QString &attrName, double lineWidth);

    /// Get the axis bounds
    const Point2d &axisBounds(const QString &ccName, const QString &attrName, uint channel);

    /// Set the  axis bounds
    bool axisSetBounds(const QString &ccName, const QString &attrName, uint channel, const Point2d &bounds);

    /// Get the  axis unit
    const QString &axisUnit(const QString &ccName, const QString &attrName);

    /// Set the axis unit
    bool axisSetUnit(const QString &ccName, const QString &attrName, const QString &unit);

    //@}

    /** 
     * Returns a reference to the map of correspondence between vertices in mesh1 
     * and mesh2 (store vertices positions)
     */
    IntMatrix3fMap &vvCorrespondence() 
    {
      return _VVCorrespondence;
    }                                                                    

    /*
		 * Returns a boolean for visualization of lines between corresponding vertices
     */
    bool &showVVCorrespondence() 
    {
      return _showVVCorrespondence;
    }                                                                
    //@}
 
    //@{
    /**
     * \name Signal methods. The signal can arise from a fluorescence intensity 
     * projection, a curvature projection, or from another process.
     *
     * These methods specifies the range and the units.
     */

//    /**
//     * Get the unit used for the signal, also shown on the color bar.
//     */
//    const QString &signalUnit() 
//    {
//      return _attributes.attrMap<QString, QString>("Vertex Signal Unit")["Vertex Signal Unit"];
//    }
//
//    /**
//     * Set the unit used for the signal, also shown on the color bar.
//     */
//    bool setSignalUnit(const QString &unit) 
//    {
//      _attributes.attrMap<QString, QString>("Vertex Signal Unit")["Vertex Signal Unit"] = unit;
//      return true;
//    }
//
//    /**
//     * Get the signal bounds, that define the range for the color bar.
//     */
//    const Point2f &signalBounds() 
//    {      
//      return _attributes.attrMap<QString, Point2f>("Vertex Signal Bounds")["Vertex Signal Bounds"];
//    }
//
//    /**
//     * Set the signal bounds, that define the range for the color bar.
//     */
//    bool setSignalBounds(const Point2f &signalBounds) 
//    {      
//      _attributes.attrMap<QString, Point2f>("Vertex Signal Bounds")["Vertex Signal Bounds"] = signalBounds;
//      return true;
//    }
    //@}
  
    /**
     * Get the mesh bounding box
     */ 
    const BoundingBox3d &boundingBox() const 
    {
      return _bbox;
    }
    /**
     * Set the mesh bounding box
     */ 
    void setBoundingBox(const BoundingBox3d &bbox) 
    {
      _bbox = bbox;
    }
    /**
     * Update the bounding box from the mesh
     */ 
    void updateBBox();
    /**
     * Cell complex tab
     */
    /**
     * Current cell complex
     */
    const QString &ccName()
    {
      return _attributes.attrMap<QString, QString>("#CCName#")["CCName"];
    }
    /**
     * Current cell complex
     */
    bool setCCName(const QString &ccName)
    {
      _attributes.attrMap<QString, QString>("#CCName#")["CCName"] = ccName;
      emit updateActiveText();
      return true;
    }
    /**
     * Process use to to delete cells or vertices from different mesh types
     */
    QString &deleteProcessName(QString &meshType)
    {
      return _attributes.attrMap<QString, QString>("Delete Process Name")[meshType];
    }

    /**
     * Create the mesh from a triangle list
     */

    /**
      * Read in a mesh
      */
    bool read(QIODevice &file, bool transform);

    /**
     * Write out a mesh
     */
    bool write(QIODevice &file, bool transform);

  public slots:
    void updateCCName() { emit updateActiveText(); }
    void updateCC();
    void updateCCRequired();

  signals:
    void updateCCData(int id);
    void refreshWidget(const QString &name);
    void clearBuffers(int id);
    void updateActiveText();

  private:
    void init();
    void resetModified();
  
    Attributes _attributes;
    AttrMap<CCIndex,CCIndexData> *_indexAttr = 0;

    CCMap _ccs;

//    TransferFunction _surf_fct, _heat_fct;
    IntMatrix3fMap _VVCorrespondence;
    bool _showVVCorrespondence;
    IntIntSetMap _labelNeighbors;
    bool changed_surf_function, changed_heat_function;
    int _changed;
    bool _culling, _blending, _cellClipping, _shading;
    float _opacity, _brightness;
		bool _useParents;
    IntIntAttr *_labelMap = 0;
    
    bool _imgtex;
    QString _file;
    bool _scaled;
    bool _transformed;
    bool _showBBox;
    int _id;
    Stack* _stack;
    QImage _image;
    int _currLabel;
  
    BoundingBox3d _bbox;
    QString _deleteProcess;

  public:
    QTimer ccUpdateTimer;
  };

  template<> mdx_EXPORT CCIndexIntAttr &Mesh::signalAttr<int>(const QString &signal);
  template<> mdx_EXPORT CCIndexDoubleAttr &Mesh::signalAttr<double>(const QString &signal);
  template<> mdx_EXPORT CCIndexPoint2dAttr &Mesh::signalAttr<Point2d>(const QString &signal);
  template<> mdx_EXPORT CCIndexPoint3dAttr &Mesh::signalAttr<Point3d>(const QString &signal);
  template<> mdx_EXPORT CCIndexColorbAttr &Mesh::signalAttr<Colorb>(const QString &signal);

  template<> mdx_EXPORT IntIntAttr &Mesh::heatAttr<int>(const QString &heat, const QString &labeling);
  template<> mdx_EXPORT IntDoubleAttr &Mesh::heatAttr<double>(const QString &heat, const QString &labeling);
  template<> mdx_EXPORT IntPoint2dAttr &Mesh::heatAttr<Point2d>(const QString &heat, const QString &labeling);
  template<> mdx_EXPORT IntPoint3dAttr &Mesh::heatAttr<Point3d>(const QString &heat, const QString &labeling);
  template<> mdx_EXPORT IntColorbAttr &Mesh::heatAttr<Colorb>(const QString &heat, const QString &labeling);

  template<> mdx_EXPORT CCIndexIntAttr &Mesh::customVertexAttr<int>(const QString &ccName, const QString &attrName);
  template<> mdx_EXPORT CCIndexDoubleAttr &Mesh::customVertexAttr<double>(const QString &ccName, const QString &attrName);
  template<> mdx_EXPORT CCIndexPoint2dAttr &Mesh::customVertexAttr<Point2d>(const QString &ccName, const QString &attrName);
  template<> mdx_EXPORT CCIndexPoint3dAttr &Mesh::customVertexAttr<Point3d>(const QString &ccName, const QString &attrName);
  template<> mdx_EXPORT CCIndexColorbAttr &Mesh::customVertexAttr<Colorb>(const QString &ccName, const QString &attrName);
}

#endif 
