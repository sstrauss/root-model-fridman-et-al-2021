//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CCViewWidget.hpp>
#include <Mesh.hpp>
#include <ImageData.hpp>
#include <ColorMap.hpp>
#include <LabelEditorDlg.hpp>
#include <QMenu>
#include <QListView>

namespace mdx
{
  // CCComplexListModel methods
  CCComplexListModel::CCComplexListModel(QObject *_parent, Mesh *_mesh)
    : QAbstractListModel(_parent), mesh(_mesh), ccNames() {
  }

  QVariant CCComplexListModel::data(const QModelIndex &_index, int _role) const
  {
    if(!_index.isValid() || _index.row() >= ccNames.size() || _index.row() < 0)
      return QVariant();

    if(_role == Qt::DisplayRole)
      return ccNames[_index.row()];
    else
      return QVariant();
  }

  Qt::ItemFlags CCComplexListModel::flags(const QModelIndex &_index) const
  {
    if(!_index.isValid())
      return Qt::ItemIsEnabled;
    else
      return QAbstractListModel::flags(_index);
  }

  int CCComplexListModel::rowCount(const QModelIndex &_parent) const
  {
    return ccNames.size();
  }

  void CCComplexListModel::refreshNames(void)
  {
    beginResetModel();
    ccNames = mesh->ccNames();
    endResetModel();
  }

  // CCViewWidget methods
  CCViewWidget::CCViewWidget(QWidget *_parent) : QWidget(_parent), parent(_parent), ccModel(NULL)
  {
    setupUi(this);
    enableGroups(false);

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
                    SLOT(onCustomContextMenuRequested(const QPoint&)));
  }

  void CCViewWidget::initialize(ImgData *_imgData, Mesh *_mesh)
  {
    imgData = _imgData;
    mesh = _mesh;
    if(ccModel) delete ccModel;
    ccModel = new CCComplexListModel(0,mesh);
    complexList->setModel(ccModel);

    connect(complexList->selectionModel(),
            SIGNAL(currentChanged(const QModelIndex &,const QModelIndex &)),
            this,SLOT(complexSelectionChanged(const QModelIndex &)));
    connect(mesh,SIGNAL(refreshWidget(const QString &)),
            this,SLOT(refreshNames(const QString &)));
    connect(this, SIGNAL(updateCCName()), mesh, SLOT(updateCCName()));
                           
    enableGroups(false);
  }

  CCViewWidget::~CCViewWidget()
  {
    if(ccModel)
      delete ccModel;
  }

  void CCViewWidget::selectName(const QString &name)
  {
    if(ccModel) {
      int row = ccModel->ccNames.indexOf(name);
      if(row >= 0) {
        complexList->selectionModel()->select(ccModel->index(row,0), QItemSelectionModel::SelectCurrent);
        enableGroups(true);
        emit complexList->selectionModel()->currentChanged(ccModel->index(row,0), ccModel->index(row,0));
      } else
        enableGroups(false);
    }
  }

  CCRenderGroupWidget *CCViewWidget::addRenderGroup(const QString &rgName)
  {
    std::map<QString,CCRenderGroupWidget*>::const_iterator iter = renderGroups.find(rgName);
    CCRenderGroupWidget *widget;
    if(iter == renderGroups.end())
      widget = new CCRenderGroupWidget(groupList,rgName);
    else {
      widget = iter->second;
      groupListLayout->removeWidget(widget);
      widget->disconnect();
    }

    groupListLayout->addWidget(widget);
    groupListLayout->setSpacing(0);
    connect(widget, SIGNAL(toggled(bool)), this, SLOT(renderGroupToggled(bool)));
    connect(widget, SIGNAL(indexChanged(const QString&)),
            this, SLOT(renderChoiceChanged(const QString&)));
    connect(widget, SIGNAL(colorMapClicked()), this, SLOT(colorMapClicked()));
    renderGroups[rgName] = widget;
    return widget;
  }

  void CCViewWidget::resetGroups(CCDrawParms *cdp)
  {
    static CCDrawParms defaultParms;
    if(cdp == NULL)
      cdp = &defaultParms;

    // only reset the groups if something has changed
    QStringList allGroups = cdp->groupList();
    bool groupsChanged = false;
    for(QString group : allGroups)
      if(renderGroups.count(group) == 0)  {
        groupsChanged = true;
        break;
      }
    if(!groupsChanged)
      for(auto iter = renderGroups.begin(); iter != renderGroups.end(); iter++)
        if(!allGroups.contains(iter->first)) {
          groupsChanged = true;
          break;
        }

    if(!groupsChanged) {
      // all the same groups, we only have to update the choices
      for(QString group : allGroups) {
        CCRenderGroupWidget *rgWidget = renderGroups[group];
        if(!rgWidget) {
          mdxWarning << "CCViewWidget::resetGroups Null widget pointer" << endl;
          continue;
        }
        const QString &choice = cdp->currentRenderChoice(group);
        rgWidget->updateCmapEnabled(cdp->colorMap(cdp->renderChoice(group,choice).colorMap));
        cdp->setRenderChoice(group,rgWidget->updateChoices(cdp->renderChoiceList(group),choice));
        rgWidget->setChecked(cdp->isGroupVisible(group));
      }
    } else {
      // something has changed, so delete and re-add the groups
      for(auto iter = renderGroups.begin(); iter != renderGroups.end(); iter++)
        iter->second->deleteLater();
      renderGroups.clear();

      for(QString group : allGroups) {
        const QString &choice = cdp->currentRenderChoice(group);
        CCRenderGroupWidget *rgWidget = addRenderGroup(group);
        rgWidget->updateCmapEnabled(cdp->colorMap(cdp->renderChoice(group,choice).colorMap));
        cdp->setRenderChoice(group,rgWidget->updateChoices(cdp->renderChoiceList(group),choice));
        rgWidget->setChecked(cdp->isGroupVisible(group));
      }
    }
  }

  void CCViewWidget::enableGroups(bool enable)
  {
    if(!enable)
      resetGroups();

    for(std::map<QString,CCRenderGroupWidget*>::const_iterator iter = renderGroups.begin() ;
        iter != renderGroups.end() ; iter++)
      iter->second->setEnabled(enable);
  }

  void CCViewWidget::complexSelectionChanged(const QModelIndex &_index)
  {
    selected = _index;
    if(selected.isValid()) {
      enableGroups(true);
      QString name = ccModel->data(selected).toString();
      resetGroups(&mesh->drawParms(name));
      if(imgData)
        imgData->mesh->setCCName(name);
    } else
      enableGroups(false);
    emit updateCCName();
  }

  void CCViewWidget::renderGroupToggled(bool checked)
  {
    if(selected.isValid()) {
      QString name = ccModel->data(selected).toString();
      CCDrawParms &cdp = mesh->drawParms(name);

      // action depends on event sender
      QObject *obj = sender();
      for(std::map<QString,CCRenderGroupWidget*>::const_iterator iter = renderGroups.begin() ;
          iter != renderGroups.end() ; iter++)
      {
        if(obj == (QObject*)(iter->second))
        {
          cdp.setGroupVisible(iter->first, checked);
          const QStringList &choiceList = cdp.renderChoiceList(iter->first);
          const QString &choice = cdp.currentRenderChoice(iter->first);
          if(choice.isEmpty() and choiceList.size() > 0)
            cdp.setRenderChoice(iter->first, choiceList[0]);
          break;
        }
      }

      if(imgData)
        imgData->ccUpdate();
    }
  }

  void CCViewWidget::renderChoiceChanged(const QString &newText)
  {
    if(newText.isEmpty())
      return;

    if(selected.isValid()) {
      QString name = ccModel->data(selected).toString();
      CCDrawParms &cdp = mesh->drawParms(name);

      // action depends on event sender
      bool update = false;
      QObject *obj = sender();
      for(std::map<QString,CCRenderGroupWidget*>::const_iterator iter = renderGroups.begin() ;
          iter != renderGroups.end() ; iter++)
      {
        if(obj == (QObject*)(iter->second))
        {
          cdp.setRenderChoice(iter->first, newText);
          iter->second->updateCmapEnabled(cdp.colorMap(cdp.renderChoice(iter->first,
                                                            cdp.currentRenderChoice(iter->first)).colorMap));
          update = true;
          break;
        }
      }

      if(update && imgData)
        imgData->ccUpdate();
    }
  }

  void CCViewWidget::colorMapClicked()
  {
    QString ccName = ccModel->data(selected).toString();
    if(ccName.isEmpty())
      return;
    CCDrawParms &cdp = mesh->drawParms(ccName);

    QString colorMapName;
    QObject *obj = sender();
    for(std::map<QString,CCRenderGroupWidget*>::const_iterator iter = renderGroups.begin() ;
        iter != renderGroups.end() ; iter++)
    {
      if(obj == (QObject*)(iter->second))
      {
        colorMapName = cdp.renderChoice(iter->first, iter->second->currentChoice()).colorMap;
        break;
      }
    }

    if(colorMapName.isEmpty())
      return;

    ColorMap &colorMap = cdp.colorMap(colorMapName);
    if(colorMap.isLabelMap()) {
      LabelEditorDlg *labelEditDlg = new LabelEditorDlg(&ImgData::LabelColors, parent);
      if(labelEditDlg) {
        connect(labelEditDlg, SIGNAL(update()), this, SLOT(colorMapUpdate()));
        labelEditDlg->exec();
        delete labelEditDlg;
      } else
        mdxInfo << "CCViewWidget::colorMapClicked Unable to create label edit dialog: " << colorMapName << endl;
    } else {
      ColorEditDlg *colorEditDlg;
      colorEditDlg = new ColorEditDlg(colorMap, parent);
      if(colorEditDlg) { 
        connect(colorEditDlg, SIGNAL(update()), this, SLOT(colorMapUpdate()));
        colorEditDlg->exec();
        delete colorEditDlg;
      } else
        mdxInfo << "CCViewWidget::colorMapClicked Unable to create label edit dialog: " << colorMapName << endl;
    }
  }

  void CCViewWidget::colorMapUpdate()
  {
    mesh->updateProperties();
    if(imgData) {
      emit mesh->updateCC();
      emit imgData->viewerUpdate();
    }
  }

  void CCViewWidget::refreshNames(const QString &current)
  {
    if(ccModel) {
      ccModel->refreshNames();
      selectName(current);
    }
  }

  void CCViewWidget::onCustomContextMenuRequested(const QPoint& pos) 
  {
    QMenu menu;
    menu.addAction("Rename");
    menu.addAction("Delete");

    QModelIndex index = complexList->indexAt(pos);
    if(index.isValid()) {
      QAction *action = menu.exec(mapToGlobal(pos));
      if(action and action->text() == "Delete")
        emit execProcess("Mesh/System/Delete Cell Complex", QStringList() << ccModel->ccNames[index.row()]);
      else if(action and action->text() == "Rename") 
        emit execProcess("Mesh/System/Rename Cell Complex", QStringList() << ccModel->ccNames[index.row()] << "");
    }
  }
}
