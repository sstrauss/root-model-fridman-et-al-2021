/*
 * The Cell Complex Framework
 * Brendan Lane 2016
 */

#include <CCF.hpp>

extern int cmds[];

namespace mdx {
namespace ccf {

// Get flips with a single index
template<typename Index, typename FlipMap>
const tbb::concurrent_vector<uint, TBBALLOC<uint> > &getFlips(const FlipMap &map, Index key)
{
  static tbb::concurrent_vector<uint, TBBALLOC<uint> > empty;
  auto it = map.find(key);
  if(it != map.end())
    return it->second.get();
  return empty;
}

// Get flips from two indices, choose smallest
template<typename Index, typename FlipMap>
const tbb::concurrent_vector<uint, TBBALLOC<uint> > &getFlips(const FlipMap &map, Index key1, Index key2)
{
  static tbb::concurrent_vector<uint, TBBALLOC<uint> > empty;
  auto it1 = map.find(key1);
  auto it2 = map.find(key2);
  if(it1 == map.end()) {
    if(it2 != map.end())
      return it2->second.get();
  } else {
    if(it2 == map.end())
      return it1->second.get();
    if(it1->second.size() < it2->second.size())
      return it1->second.get();
    else
      return it2->second.get();
  }
  return empty;
}

template<typename Index>
uint FlipTable<Index>::findIndex(const FlipI &flip)
{
  // See if flip exists and finds its location
  uint pos = flipVec.size();
  for(uint i : getFlips(flipMap, flip.facet[0], flip.facet[1])) {
    auto f = flipVec[i];
    if(f.facet[0] == flip.facet[0] and f.facet[1] == flip.facet[1] and
        f.joint == flip.joint and f.interior == flip.interior) {
      pos = i;
      break;
    }
  }
  return pos;
}

template<typename Index>
typename FlipTable<Index>::const_iterator FlipTable<Index>::find(const FlipI &flip)
{
  uint pos = findIndex(flip);
  if(pos == flipVec.size())
    return flipVec.end();
  return flipVec.begin() + pos;
}

// FlipTable match method definitions
// This is almost the same as matchFirst, is there a way to combine them?
template<typename Index>
typename std::vector< Flip<Index> > FlipTable<Index>::matchV(Flip<Index> query) const
{
  /* Ensure that facet 0 is not \QUERY, if possible */
  if((query.facet[0] == Index::Q or query.facet[0] == Index::INFTY) and query.facet[1] != Index::Q)
    std::swap(query.facet[0], query.facet[1]);

  /* Create mask based on non-\QUERY{} values */
  unsigned char givenMask = 0;
  if(query.interior != Index::Q) givenMask = givenMask | 0x1;
  if(query.joint != Index::Q) givenMask = givenMask | 0x2;
  if(query.facet[0] != Index::Q) givenMask = givenMask | 0x4;
  if(query.facet[1] != Index::Q) givenMask += 4;

  const_cast<int&>(cmds[givenMask])++;
  std::vector< Flip<Index> > answer;
  switch(givenMask)
  {
    /* Create answer based on mask */
    case 0: { // all query, return everything
      for(auto &flip : flipVec)
        answer.emplace_back(flip);
      break;
    }
    case 1: { // interior only
      for(uint i : getFlips(flipMap, query.interior))
        if(flipVec[i].interior == query.interior)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 2: { // joint only
      for(uint i : getFlips(flipMap, query.joint))
        if(flipVec[i].joint == query.joint)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 3: { // interior + joint
      for(uint i : getFlips(flipMap, query.interior, query.joint))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 4: { // only facet
      for(uint i : getFlips(flipMap, query.facet[0]))
        if(flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0])
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 5: { // facet + interior
      for(uint i : getFlips(flipMap, query.interior, query.facet[0]))
        if(flipVec[i].interior == query.interior and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 6: { // facet + joint
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 7: { // facet + joint + interior
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 8: { // two facets
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
           (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 9: { // two facets + interior
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].interior == query.interior)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 10: { // two facets + joint
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].joint == query.joint)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 11: { // all given
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
           ((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
  }
  return answer;
}

template<typename Index>
const Flip<Index> &FlipTable<Index>::matchFirst(Flip<Index> query) const
{
  static const Flip<Index> InvalidFlip(Index::UNDEF);

  /* Ensure that facet 0 is not \QUERY or \INFTY, if possible */
  if((query.facet[0] == Index::Q or query.facet[0] == Index::INFTY) and query.facet[1] != Index::Q)
    std::swap(query.facet[0], query.facet[1]);

  /* Create mask based on non-\QUERY{} values */
  unsigned char givenMask = 0;
  if(query.interior != Index::Q) givenMask = givenMask | 0x1;
  if(query.joint != Index::Q) givenMask = givenMask | 0x2;
  if(query.facet[0] != Index::Q) givenMask = givenMask | 0x4;
  if(query.facet[1] != Index::Q) givenMask += 4;

  const_cast<int &>(cmds[givenMask])++;
  switch(givenMask)
  {
    /* Create answer based on mask */
    case 0: { // all query, return everything
      for(auto &flip : flipVec)
        return flip;
      break;
    }
    case 1: { // interior only
      for(uint i : getFlips(flipMap, query.interior))
        if(flipVec[i].interior == query.interior)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 2: { // joint only
      for(uint i : getFlips(flipMap, query.joint))
        if(flipVec[i].joint == query.joint)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 3: { // interior + joint
      for(uint i : getFlips(flipMap, query.interior, query.joint))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 4: { // only facet
      for(uint i : getFlips(flipMap, query.facet[0]))
        if(flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0])
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 5: { // facet + interior
      for(uint i : getFlips(flipMap, query.interior, query.facet[0]))
        if(flipVec[i].interior == query.interior and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 6: { // facet + joint
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 7: { // facet + joint + interior
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 8: { // two facets
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
           (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 9: { // two facets + interior
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].interior == query.interior)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 10: { // two facets + joint
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].joint == query.joint)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 11: { // all given
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
           ((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
  }
  return InvalidFlip;
}

/* FlipTower method definitions */
/* FlipTower addCell method */
template<typename Index>
void FlipTowerG<Index>::addCell(Index c)
{
  if(c.isPseudocell() && c != Index::INFTY)
    throw std::domain_error
      (std::string("FlipTower: trying to add pseudocell ")+to_string(c));

  if(!(cs->hasCell(c)))
    throw std::domain_error
      (std::string("FlipTower: trying to add cell ")+to_string(c)+
       " which is not in cell structure");

  Dimension cDim = cs->dimensionOf(c);
  if(cellMap.find(cDim) != cellMap.end())
    throw std::domain_error
      (std::string("FlipTower: trying to add cell ")+to_string(c)+
       " which is of dimension "+to_string(cDim)+", already occupied by "+
       to_string(cellMap[cDim]));

  cellMap[cDim] = c;
}

/* FlipTower constructors */
template<typename Index>
FlipTowerG<Index>::FlipTowerG(void) : invalid(true) , cs(NULL) {}

template<typename Index>
FlipTowerG<Index>::FlipTowerG(const CStructure &_cs)
  : invalid(false), cellMap(), flips(_cs.maxDimension()+1), cs(&_cs) {}

template<typename Index>
FlipTowerG<Index>::FlipTowerG(const CStructure &_cs, Index c1)
  : invalid(false), cellMap(), flips(_cs.maxDimension()+1), cs(&_cs)
{ addCell(c1); }

template<typename Index>
FlipTowerG<Index>::FlipTowerG(const CStructure &_cs, Index c1, Index c2)
  : invalid(false), cellMap(), flips(_cs.maxDimension()+1), cs(&_cs)
{ addCell(c1); addCell(c2); }

template<typename Index>
FlipTowerG<Index>::FlipTowerG(const CStructure &_cs, Index c1, Index c2, Index c3)
  : invalid(false), cellMap(), flips(_cs.maxDimension()+1), cs(&_cs)
{ addCell(c1); addCell(c2); addCell(c3); }

template<typename Index>
FlipTowerG<Index>::FlipTowerG(const CStructure &_cs, Index c1, Index c2, Index c3, Index c4)
  : invalid(false), cellMap(), flips(_cs.maxDimension()+1), cs(&_cs)
{ addCell(c1); addCell(c2); addCell(c3); addCell(c4); }

template<typename Index>
FlipTowerG<Index>::FlipTowerG(const CStructure &_cs, const CellMap &_cellMap)
  : invalid(false), cellMap(_cellMap), flips(_cs.maxDimension()+1), cs(&_cs)
{
  for(CellMapIterator iter = cellMap.begin() ; iter != cellMap.end() ; iter++)
    {
      Dimension dim = iter->first;
      Index c = iter->second;

      if(c.isPseudocell() && c != Index::INFTY)
	throw std::domain_error
	  (std::string("FlipTower: trying to add pseudocell ")+to_string(c));

      if(!(cs->hasCell(c)))
	throw std::domain_error
	  (std::string("FlipTower: trying to add cell ")+to_string(c)+
	   " which is not in cell structure");

      if(cs->dimensionOf(c) != dim)
	throw std::domain_error
	  (std::string("FlipTower: trying to add cell ")+to_string(c)+
	   " at dimension "+to_string(dim)+", but dimension in cell structure is "+
	   to_string(cs->dimensionOf(c)));
    }
}

/* FlipTower fillCell methods */
/* FlipTower fillCellsBetween method */
template<typename Index>
bool FlipTowerG<Index>::fillCellsBetween
  (FlipTowerG<Index>::CellMapIterator itFrom,
   FlipTowerG<Index>::CellMapIterator itTo)
{
  if(invalid) return false;
  if(itFrom == itTo) return true;

  /* Split range between itFrom and itTo into simple ranges */
  CellMapIterator nextFrom = itFrom;
  while(++nextFrom != itTo)
    if(!fillCellsBetween(itFrom++,nextFrom)) return false;

  int itDist = itTo->first - itFrom->first;
  if(itDist == 1) return true;
  else if(itDist == 2)
  {
    /* Fill in gap of one dimension */
    FlipI flipM = cs->matchFirst(itFrom->second,Index::Q,Index::Q,itTo->second);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[itFrom->first+1] = flipM;
    cellMap[itFrom->first+1] = flips[itFrom->first+1].realFacet();
    return true;
  }
  else // more than 1 flip in the range; harder!
  {
    /* Fill in gap of more than one dimension */
    /* Create data structures for gap filling */
    Dimension activeDim = itFrom->first;
    std::set<Index> active;
    active.insert(itFrom->second);
    std::map<Index,FlipI> originFlip;

    /* Find cells from the lower-dimensional cell up */
    while(activeDim < itTo->first)
    {
      /* Find active cells two dimensions above the current */
      activeDim += 2;
      std::set<Index> nextActive;
      for(typename std::set<Index>::iterator iter = active.begin() ;
          iter != active.end() ; iter++)
      {
        /* Find the flips from a single cell in the active set */
        FlipVectorI fvec = cs->matchV(*iter,Index::Q,Index::Q,Index::Q);
        if(fvec.empty())
          throw std::runtime_error(std::string("Could not find any coboundary flip for ")+
                                   to_string(*iter)+" in FlipTower::fillCells");

        for(FlipVectorIterator fiter = fvec.begin(); fiter != fvec.end() ; fiter++)
        {
          /* Process a single flip with joint in the active set */
          if((activeDim-1) == itTo->first)
          {
            nextActive.insert(fiter->facet[0]);
            nextActive.insert(fiter->facet[1]);
            /* Assign flip as origin of facet cells */
            originFlip[fiter->facet[0]] = *fiter;
            originFlip[fiter->facet[1]] = *fiter;
          }
          else
          {
            nextActive.insert(fiter->interior);
            /* Assign flip as origin of interior cell */
            originFlip[fiter->interior] = *fiter;
          }
        }
      }
      std::swap(active,nextActive);
    }

    /* Check that the higher-dimensional cell is in the list of incident cells */
    if(active.count(itTo->second) == 0) { invalid = true; return false; }

    /* Fill cell map and flips working down from higher to lower-dimensional cell */
    /* Work down from upper cell to fill in flips */
    Index nextCell = itTo->second;
    for( ; activeDim > itFrom->first ; activeDim -= 2)
    {
      flips[activeDim-1] = originFlip[nextCell];
      nextCell = flips[activeDim-1].joint;
    }

    /* Fill in cell map from flips */
    for(Dimension d = itFrom->first+1 ; d < itTo->first ; d+=2)
    {
      cellMap[d] = flips[d].realFacet();
      cellMap[d+1] = flips[d].interior;
    }
    return true;
  }
}

/* FlipTower fillCellsBelow method */
template<typename Index>
bool FlipTowerG<Index>::fillCellsBelow
  (FlipTowerG<Index>::CellMapIterator itTo)
{
  if(invalid) return false;

  /* Split range below itTo into simple ranges */
  CellMapIterator nextTo = itTo;
  while(itTo != cellMap.begin())
    if(!fillCellsBetween(--nextTo,itTo--)) return false;

  if(itTo->first == 0) return true;
  else if(itTo->first == 1)
  {
    /* Fill in lower gap of one dimension */
    FlipI flipM = cs->matchFirst(Index::BOTTOM,Index::Q,Index::Q,itTo->second);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[0] = flipM;
    cellMap[0] = flips[0].realFacet();
    return true;
  }
  else
  {
    /* Fill in lower gap of more than one dimension */
    FlipI flipM = cs->matchFirst(Index::Q,Index::Q,Index::Q,itTo->second);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[itTo->first-1] = flipM;
    cellMap[itTo->first-2] = flips[itTo->first-1].joint;
    cellMap[itTo->first-1] = flips[itTo->first-1].realFacet();
    itTo--;
    itTo--;
    return fillCellsBelow(itTo);
  }
}

/* FlipTower fillCellsAbove method */
template<typename Index>
bool FlipTowerG<Index>::fillCellsAbove
  (FlipTowerG<Index>::CellMapIterator itFrom)
{
  if(invalid) return false;

  /* Split range above itFrom into simple ranges */
  CellMapIterator nextFrom = itFrom, lastTo = --(cellMap.end());
  while(itFrom != lastTo)
    if(!fillCellsBetween(itFrom++,++nextFrom)) return false;


  if(itFrom->first == cs->maxDimension()) return true;
  else if(itFrom->first == cs->maxDimension()-1)
  {
    /* Fill in upper gap of one dimension */
    FlipI flipM = cs->matchFirst(itFrom->second,Index::Q,Index::Q,Index::TOP);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[cs->maxDimension()] = flipM;
    cellMap[cs->maxDimension()] = flips[cs->maxDimension()].realFacet();
    return true;
  }
  else
  {
    /* Fill in upper gap of more than one dimension */
    FlipVectorI matches = cs->matchV(itFrom->second,Index::Q,Index::Q,Index::Q);
    if(matches.empty()) { invalid = true; return false; }
    {
      FlipVectorIterator iter = matches.begin();
      do
      {
        flips[itFrom->first+1] = *iter;
      } while((iter++)->interior.isPseudocell() && iter != matches.end());
    }
    cellMap[itFrom->first+2] = flips[itFrom->first+1].interior;
    cellMap[itFrom->first+1] = flips[itFrom->first+1].realFacet();
    itFrom++;
    itFrom++;
    return fillCellsAbove(itFrom);
  }
}

/* FlipTower fillFlip methods */
template<typename Index>
bool FlipTowerG<Index>::fillFlip(Dimension dim)
{
  if(invalid) return false;
  if(flips[dim].isValid()) return true;

  FlipI query(Index::Q);

  if(dim > 0)
    {
      fillAt(dim-1);
      query.joint = cellMap[dim-1];
    }
  else query.joint = Index::BOTTOM;

  fillAt(dim); query.facet[0] = cellMap[dim];

  if(dim < cs->maxDimension())
    {
      fillAt(dim+1);
      query.interior = cellMap[dim+1];
    }
  else query.interior = Index::TOP;

  FlipI flipM = cs->matchFirst(query);
  if(!flipM.isValid()) { invalid = true; return false; }
  flips[dim] = flipM;
  return !invalid;
}

template<typename Index>
bool FlipTowerG<Index>::fillDeterminedFlips(void)
{
  if(invalid) return false;

  CellMapIterator it1 = cellMap.begin(), it2 = cellMap.begin(), it3 = cellMap.begin();
  if(it2 == cellMap.end() || (++it3) == cellMap.end()) return true;
  // 0-flip if possible
  if(it2->first == 0 && it3->first == 1 && !fillFlip(0)) return false;
  it2++;
  while((++it3) != cellMap.end())
    {
      Dimension dim2 = it2->first;
      if(dim2 - it1->first == 1 && it3->first - dim2 == 1 && !fillFlip(dim2))
	return false;
      it1++; it2++;
    }
  // maxdim-flip if possible
  if(it1->first == (cs->maxDimension()-1) && it2->first == cs->maxDimension() &&
     !fillFlip(cs->maxDimension())) return false;
  return true;
}


/* CellTuple method definitions */
/* CellTuple constructors */
template<typename Index>
CellTupleG<Index>::CellTupleG(void) : cs(NULL) {}

template<typename Index>
CellTupleG<Index>::CellTupleG(const CStructure &_cs)
  : cs(&_cs), tower(_cs), tuple(_cs.maxDimension()+1)
{ containing(); }

template<typename Index>
CellTupleG<Index>::CellTupleG(const CStructure &_cs,Index c1)
  : cs(&_cs), tower(_cs), tuple(_cs.maxDimension()+1)
{ containing(c1); }

template<typename Index>
CellTupleG<Index>::CellTupleG(const CStructure &_cs,Index c1,Index c2)
  : cs(&_cs), tower(_cs), tuple(_cs.maxDimension()+1)
{ containing(c1,c2); }

template<typename Index>
CellTupleG<Index>::CellTupleG(const CStructure &_cs,Index c1,Index c2,Index c3)
  : cs(&_cs), tower(_cs), tuple(_cs.maxDimension()+1)
{ containing(c1,c2,c3); }

template<typename Index>
CellTupleG<Index>::CellTupleG(const CStructure &_cs,Index c1,Index c2,Index c3,Index c4)
  : cs(&_cs), tower(_cs), tuple(_cs.maxDimension()+1)
{ containing(c1,c2,c3,c4); }

/* CellTuple containing methods */
template<typename Index>
void CellTupleG<Index>::containing(void)
{
  FlipI flip = cs->matchFirst(CCIndex::Q, CCIndex::Q, CCIndex::Q, CCIndex::Q);
  if(!flip.isValid())
    throw(QString("Attempt to create CellTuple on CellStructure with no flips"));
  else {
    std::vector<Index> cells;
    if(!(flip.interior.isPseudocell())) cells.push_back(flip.interior);
    cells.push_back(flip.realFacet());
    if(!(flip.joint.isPseudocell())) cells.push_back(flip.joint);
    containing(cells);
  }
}

template<typename Index>
void CellTupleG<Index>::containing(const std::vector<Index>& cells)
{
  /* Add entries from cells to flip tower */
  try
  {
    for(unsigned int i = 0 ; i < cells.size() ; i++)
      tower.addCell(cells[i]);
  }
  catch(std::domain_error& e)
  {
    throw std::domain_error
      (std::string("Could not create CellTuple: invalid cell passed: ")+e.what());
  }

  /* Fill in all cells in tuple */
  if(!tower.fillAllCells() || !tower.isValid())
  {
    std::ostringstream oss;
    oss << "[ ";
    for(unsigned int i = 0 ; i < cells.size() ; i++)
    {
      if(i != 0) oss << ", ";
      oss << cells[i];
    }
    oss << " ]";

    throw std::invalid_argument
      (std::string("Could not create CellTuple with non-incident cells ")+oss.str());
  }

  /* Copy cells from flip tower to tuple */

  for(typename FlipTower::CellMapIterator iter = tower.cellMap.begin() ;
      iter != tower.cellMap.end() ; iter++)
    tuple[iter->first] = iter->second;

}

/* CellTuple flip operation */
template<typename Index>
CellTupleG<Index>& CellTupleG<Index>::flip(CellTupleG<Index>::Dimension dim)
{
  /* Fill flip at dimension dim */
  /* Validate cached flip at dimension dim */
  if(tower.flips[dim].isValid() && cs->matchFirst(tower.flips[dim]) != tower.flips[dim])
    tower.flips[dim] = FlipI();

  if(!tower.fillFlip(dim))
    throw std::runtime_error
      (std::string("Cell tuple ")+to_string(*this)+
       " found to be invalid during execution of flip("+
       to_string(dim)+")");

  tuple[dim] = tower.cellMap[dim] = tower.flips[dim].otherFacet(tuple[dim]);
  /* Invalidate cached flips in neighboring dimensions */
  if(dim > 0) tower.flips[dim-1] = FlipI();
  if(dim < cs->maxDimension()) tower.flips[dim+1] = FlipI();

  return *this;
}

template<typename Index>
Index CellTupleG<Index>::other(Dimension dim) const
{
  if(tower.flips[dim].isValid() && cs->matchFirst(tower.flips[dim]) != tower.flips[dim])
    tower.flips[dim] = FlipI();

  if(!tower.fillFlip(dim))
    throw std::runtime_error
      (std::string("Cell tuple ")+to_string(*this)+
       " found to be invalid during execution of flip("+
       to_string(dim)+")");

  return tower.flips[dim].otherFacet(tuple[dim]);
}

template<typename Index>
RO CellTupleG<Index>::totalOrientation(void) const
{
  RO acc = POS;
  for(int dim = cs->maxDimension() ; dim >= 0 ; dim -= 2)
    if(tower.getFlip(dim).facet[1] == tuple[dim]) acc *= NEG;
  return acc;
}

} // end namespace ccf

template class ccf::DimensionMapG<CCIndex>;
template class ccf::FlipTowerG<CCIndex>;
template class ccf::CellTupleG<CCIndex>;
template class ccf::SplitStructG<CCIndex>;
template class ccf::FlipTable<CCIndex>;

} // end namespace mdx
