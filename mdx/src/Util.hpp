//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef UTIL_HPP
#define UTIL_HPP
/**
 * \file Util.hpp
 *
 * Common definitions and utilities
 * This file is shared by cuda, do not include headers that nvcc 
 * can't handle (i.e. Qt)
 */
#include <Config.hpp>
#include <cuda/CudaGlobal.hpp>

namespace mdx 
{
  // Min
  template <typename T> CU_HOST_DEVICE T min(const T a, const T b)
  {
    if(a <= b)
      return a;
    return b;
  }
  
  // Max
  template <typename T> T CU_HOST_DEVICE max(const T a, const T b)
  {
    if(a >= b)
      return a;
    return b;
  }
  
  // Trim to min/max
  template <typename T> CU_HOST_DEVICE T trim(const T x, const T minx, const T maxx) {
    return max(minx, min(maxx, x));
  }
}
#endif
