varying vec2 texCoord;

void main()
{
  //texCoord = (gl_TextureMatrix[0]*gl_MultiTexCoord0).xy;
  texCoord = (gl_ModelViewProjectionMatrix * gl_Vertex).xy / 2.0 + 0.5;
  gl_Position = ftransform();
  gl_FrontColor = gl_Color;
}
