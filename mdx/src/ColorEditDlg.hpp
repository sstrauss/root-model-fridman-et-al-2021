//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef COLOR_EDIT_DLG_HPP
#define COLOR_EDIT_DLG_HPP

#include <Config.hpp>

#include <Color.hpp>
#include <ColorMap.hpp>
#include <Misc.hpp>

#include <QAbstractListModel>
#include <vector>
#include <QDialog>

class QAbstractButton;
class QPoint;

class ColorModel : public QAbstractListModel 
{
  Q_OBJECT
public:
  ColorModel(mdx::ColorMap &colorMap);

  int rowCount(const QModelIndex& parent = QModelIndex()) const;
  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const { return 1; }

  Qt::ItemFlags flags(const QModelIndex& index) const;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);

  void apply();
  void reset();

  void presetColors(const QString &preset);
  void loadPresetColors(const QString &preset);
  void setNbColors(int n);

  void setVisible(int i, bool val) { channelsLocal[i].visible = val; }
  void setMinBound(int i, double val) { channelsLocal[i].bounds[0] = val; }
  void setMaxBound(int i, double val) { channelsLocal[i].bounds[1] = val; }
  void setMinIndex(int i, int idx) { channelsLocal[i].indices[0] = idx; }
  void setMaxIndex(int i, int idx) { channelsLocal[i].indices[1] = idx; }
  void setUsingIndices(int i, bool val) { usingIndices[i] = val; }

  bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex());
  bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex());

signals:
  void nbColorsChanged(int n);

protected:
  mdx::ColorMap &colorMapDest;
  std::vector<mdx::Colorb> colorsLocal;
  std::vector<mdx::ColorMap::ChannelMap> channelsLocal;
  std::vector<bool> usingIndices;
};

namespace Ui 
{
  class ColorEditDlg;
}

class mdx_EXPORT ColorEditDlg : public QDialog 
{
  Q_OBJECT
public:
  ColorEditDlg(mdx::ColorMap &cMap, QWidget* parent);
  ~ColorEditDlg();

public slots:
  //void interpolateColor();

protected slots:
  void on_buttonBox_clicked(QAbstractButton* btn);
  void on_setNbColors_clicked();
  void on_importColors_clicked();
  void on_exportColors_clicked();
  void on_presetColors_currentIndexChanged(QString);
  void on_colorsView_doubleClicked(const QModelIndex &idx);
  //void on_colorsView_customContextMenuRequested(const QPoint &pos);
  void changeNbItems();

signals:
  void update();

protected:
  mdx::ColorMap &colorMap;
  int selectedColor;
  ColorModel* model;
  Ui::ColorEditDlg* ui;
  QAction* interpolateAction;
};

#endif
