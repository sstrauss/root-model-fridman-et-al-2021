/*
 * The Cell Complex Framework: UIntIndex and Manager
 * Brendan Lane 2017
 */
#ifndef __CCINDEX_HPP
#define __CCINDEX_HPP

#include <Config.hpp>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <stdint.h>

#include <Information.hpp>

namespace mdx {
namespace ccf {

/* tostring function */
template<typename T>
std::string to_string(const T& t)
{ std::ostringstream ss; ss << t; return ss.str(); }

/// RO represents a relative orientation, POSitive or NEGative.
enum RO { POS = 0, NEG = 1 };

/* RO operations */
/// Negate the orientation (swap POS and NEG)
inline RO operator-(RO r) { return (r==POS)?NEG:POS; }
/// Combine two orientations (POS*POS = NEG*NEG = POS; POS*NEG = NEG).
inline RO operator*(RO r1, RO r2) { return (r1==r2)?POS:NEG; }
/// Combine two orientations (POS*POS = NEG*NEG = POS; POS*NEG = NEG).
inline RO& operator*=(RO& r1, RO r2) { return (r1 = ((r1==r2)?POS:NEG)); }
/// Output a relative orientation as '+' or '-'.
inline std::ostream& operator<<(std::ostream& out, RO ro)
{ return (out << ((ro==NEG)?"-":"+")); }

/// OrientedObject<T> pairs an object of type T with a relative orientation.
template<typename Index>
struct OrientedObject
{
  typedef Index object_type;
  std::pair<Index,RO> val;

  /* OrientedObject constructors */
  //private: OrientedObject(void) {} public:
  OrientedObject(void) {} 
  OrientedObject(const OrientedObject& oo) : val(oo.val) {}
  OrientedObject(const Index& o, RO ro = POS) : val(o,ro) {}
  OrientedObject(const std::pair<Index,RO>& v) : val(v) {}
  template<typename U> OrientedObject(const OrientedObject<U>& oo)
  : val(oo.val.first,oo.val.second) {}

  /* OrientedObject accessors */
  /// Access the object without the orientation.
  inline const Index& operator~() const { return val.first; }
  /// Access the orientation.
  inline RO orientation(void) const { return val.second; }

  /* OrientedObject comparisons */
  inline bool operator==(const OrientedObject& oo2) const
  { return (val == oo2.val); }

  inline bool operator!=(const OrientedObject& oo2) const
  { return (val != oo2.val); }

  inline bool operator<(const OrientedObject& oo2) const
  { return (val < oo2.val); }

  /* OrientedObject operations */
  inline OrientedObject operator-(void) const
  { return OrientedObject(val.first,-val.second); }
  inline OrientedObject operator+(void) const
  { return *this; }
  inline friend OrientedObject operator*(RO ro, const OrientedObject& oo)
  { return OrientedObject(oo.val.first,ro * oo.val.second); }
  inline OrientedObject operator*(RO ro) const
  { return OrientedObject(val.first,val.second * ro); }
  OrientedObject& operator*=(RO ro)
  { val.second *= ro; return *this; }
  friend std::ostream& operator<<(std::ostream& out, const OrientedObject& oo)
  { return (out << oo.val.second << oo.val.first); }
};
/* Other operators which create an OrientedObject */
template<typename Index>
OrientedObject<Index>operator*(RO ro, const Index& t)
{ return OrientedObject<Index>(t,ro); }

template<typename Index>
OrientedObject<Index> operator*(const Index& t, RO ro)
{ return OrientedObject<Index>(t,ro); }


/* An Index implementation must provide:
 *  - Comparison operators (at least ==, !=, <)
 *  - match (which is the same as ==, but also matches anything
 *    to the pseudocell Q)
 *  - Unary + and - operators which create an OrientedObject<Index>
 *  - Pseudocells UNDEF, TOP, BOTTOM, INFTY, and Q, with
 *        Q < (TOP,BOTTOM) < INFTY < real cells
 *  - Method isPseudocell() to tell whether the Index represents
 *    one of these pseudocells
 */

/* Unsigned integer indices
 * The UIntIndex class is an encapsulated unsigned int,
 * with appropriate operators and methods.
 * It is implemented using a more general, templated UIntTIndex class.
 */
/* UIntTIndex class declaration */
template<typename UI>
struct mdx_EXPORT UIntTIndex
{
  typedef UI valueT;

  UI value;

  /* UIntTIndex pseudocell declarations */
  const static UIntTIndex UNDEF, Q, TOP, BOTTOM, INFTY;

  /* UIntTIndex constructors */
  UIntTIndex(void) : value(UNDEF.value) {}
  explicit UIntTIndex(const UI &_value) : value(_value) {}
  
  /* UIntTIndex comparison operators */
  bool operator==(const UIntTIndex& idx) const { return value == idx.value; }
  bool operator!=(const UIntTIndex& idx) const { return value != idx.value; }
  bool operator<(const UIntTIndex& idx) const { return value < idx.value; }
  bool operator<=(const UIntTIndex& idx) const { return value <= idx.value; }
  bool operator>=(const UIntTIndex& idx) const { return value >= idx.value; }
  bool operator>(const UIntTIndex& idx) const { return value > idx.value; }
  
  /* UIntTIndex match method */
  inline bool match(const UIntTIndex& idx) const
  {
    return (value==Q.value) || (idx.value==Q.value) || (value==idx.value);
  }

  /* UIntTIndex isPseudocell method */
  /// Reports whether the cell is a pseudocell.
  inline bool isPseudocell(void) const { return (value <= UIntTIndex::INFTY.value); }

  /* UIntTIndex unary + and - operators */
  /// Assign a positive orientation to the cell.
  OrientedObject<UIntTIndex> operator+(void) const
  { return OrientedObject<UIntTIndex>(*this,POS); }
  /// Assign a negative orientation to the cell.
  OrientedObject<UIntTIndex> operator-(void) const
  { return OrientedObject<UIntTIndex>(*this,NEG); }
  
  /* UIntTIndex output operator */
  friend std::ostream& operator<<(std::ostream& out, const UIntTIndex& i)
  {
    if(i.value > UIntTIndex::INFTY.value) out << i.value;
    else if(i.value == UIntTIndex::UNDEF.value) out << "UNDEF";
    else if(i.value == UIntTIndex::Q.value) out << "Q";
    else if(i.value == UIntTIndex::INFTY.value) out << "INF";
    else if(i.value == UIntTIndex::TOP.value) out << "TOP";
    else if(i.value == UIntTIndex::BOTTOM.value) out << "BOT";

    return out;
  }
};

/* Now the UIntTIndexManager
 * An index manger must provide the following:
 *
 * IndexManager::Index              -- the type produced by the manager
 * IndexManager::getIndex()         -- produce an index
 * IndexManager::releaseIndex(idx)  -- release the given index
 */
/* CellStructure forward declaration ... needed below */
template<typename> struct CellStructure;

/* UIntTIndexManager class declaration */
template<typename UI>
struct UIntTIndexManager
{
  typedef UIntTIndex<UI> Index;

  UI highestIndex;

  /* UIntTIndexManager constructor */
  UIntTIndexManager(void) { reset(); }

  /* UIntTIndexManager getIndex method */
  /// Gets the next available cell index.
  Index getIndex()
  { 
    UI index;
    #pragma omp atomic capture
    index = ++highestIndex;
    return Index(index);
  }

  /* UIntTIndexManager releaseIndex method */
  void releaseIndex(Index idx) { }
  
  /* UIntTIndexManager reset method */
  void reset(void)
  {
    // Because of compilation units, we cannot refer directly to Index::INFTY.
    // We thus have that value here.
    const static unsigned char InftyVal = 4;
    highestIndex = InftyVal;
  }
  
  /* UIntTIndexManager increaseHighestIndex method */
  void increaseHighestIndex(const UI &newMax)
  { 
    if(highestIndex < newMax)
      highestIndex = newMax;
  }

  /* UIntTIndexManager fillSplitStruct method */
  /// Gets fresh cell indexes for all of the undefined entries
  /// in the given SplitStruct.
  void fillSplitStruct(typename CellStructure<Index>::SplitStruct &ss)
  {
    if(ss.parent   == Index::UNDEF) ss.parent   = getIndex();
    if(ss.membrane == Index::UNDEF) ss.membrane = getIndex();
    if(ss.childP   == Index::UNDEF) ss.childP   = getIndex();
    if(ss.childN   == Index::UNDEF) ss.childN   = getIndex();
  }
};


/// The Flip structure represents a single flip;
/// it records a single adjacency between cells in a cell complex.
template<typename Index>
struct Flip
{
  Index joint;
  Index facet[2];
  Index interior;

  /* Flip constructors */
  Flip(void) : joint(), facet(), interior() {}
  Flip(const Flip& _flip) : joint(_flip.joint), interior(_flip.interior)
  { facet[0] = _flip.facet[0]; facet[1] = _flip.facet[1]; }
  Flip(Index _default) : joint(_default), interior(_default)
  { facet[0] = facet[1] = _default; }
  Flip(Index _joint, Index _facet0, Index _facet1, Index _interior)
    : joint(_joint), interior(_interior) { facet[0] = _facet0; facet[1] = _facet1; }

  /* Flip comparison operators */
  inline bool operator<(const Flip& _flip) const
  {
    if(joint < _flip.joint) return true;
    else if(_flip.joint < joint) return false;
    if(facet[0] < _flip.facet[0]) return true;
    else if(_flip.facet[0] < facet[0]) return false;
    if(facet[1] < _flip.facet[1]) return true;
    else if(_flip.facet[1] < facet[1]) return false;
    if(interior < _flip.interior) return true;
    else if(_flip.interior < interior) return false;
    return false;
  }
  inline bool operator==(const Flip &flip) const
  {
    return ((interior == flip.interior) &&
            (facet[0] == flip.facet[0]) &&
            (facet[1] == flip.facet[1]) &&
            (joint == flip.joint));
  }
  inline bool operator!=(const Flip &flip) const { return !(*this == flip); }

  /* Flip operators */
  Flip operator-(void) const { return Flip(joint,facet[1],facet[0],interior); }
  Flip operator*(RO ro) const { return (ro == POS)?(*this):(-(*this)); }
  Flip& operator*=(RO ro) { return (*this = *this * ro); }
  friend Flip operator*(RO ro, const Flip& _flip) { return (ro==POS)?_flip:(-_flip); }
  friend std::ostream& operator<<(std::ostream& out, const Flip& _flip)
  {
    out << "<" << _flip.joint << " , " << _flip.facet[0]
        << " | " << _flip.facet[1] << " , " << _flip.interior << ">";
    return out;
  }

  /* Flip methods */
  /// Returns the facet which is not the given cell.
  inline Index otherFacet(Index _facet) const
  {
    if(_facet == facet[0]) return facet[1];
    else if(_facet == facet[1]) return facet[0];
    else throw std::domain_error(std::string("otherFacet: ")+to_string(_facet)+
                                 " is not a facet of "+to_string(*this));
  }
  /// Returns a real (i.e. not pseudocell) facet, if one exists.
  inline Index realFacet(void) const
  {
    if(facet[0].isPseudocell())
    {
      if(facet[1].isPseudocell())
        throw std::domain_error
          (std::string("Requested non-pseudocell facet of flip ")+to_string(*this));
      else return facet[1];
    }
    else return facet[0];
  }
  /// Replaces the given index with the new one.
  inline Flip replaceIndex(Index _old, Index _new) const
  {
    Flip ans(*this);
    if(interior == _old) ans.interior = _new;
    else if(joint == _old) ans.joint = _new;
    else if(facet[0] == _old) ans.facet[0] = _new;
    else if(facet[1] == _old) ans.facet[1] = _new;
    return ans;
  }
  /// Reports false if any cell is UNDEF.
  inline bool isValid(void) const
  {
    return (interior != Index::UNDEF) && (joint != Index::UNDEF) &&
      (facet[0] != Index::UNDEF) && (facet[1] != Index::UNDEF);
  }
  inline bool match(const Flip &query) const
  {
    return (interior.match(query.interior) && joint.match(query.joint) &&
            ((facet[0].match(query.facet[0]) && facet[1].match(query.facet[1])) ||
             (facet[0].match(query.facet[1]) && facet[1].match(query.facet[0]))));
  }
  inline bool matchM(const Flip &query) const
  {
    return (interior.match(query.interior) && joint.match(query.joint) &&
            facet[0].match(query.facet[0]) && facet[1].match(query.facet[1]));
  }
};
} // end namespace ccf

typedef uint_least64_t CCIndexUnsignedType;
typedef ccf::UIntTIndex<CCIndexUnsignedType> CCIndex;
typedef ccf::OrientedObject<CCIndex> CCSignedIndex;
typedef ccf::UIntTIndexManager<CCIndexUnsignedType> CCIndexManager;

extern mdx_EXPORT CCIndexManager CCIndexFactory;

// Operators for Information::
inline QString toQString(CCIndex idx)
{
  return ccf::to_string(idx).c_str();
}
inline QDebug &operator<<(QDebug &out, CCIndex idx)
{
  return out << toQString(idx);
}

inline QString toQString(CCSignedIndex idx)
{
  return ccf::to_string(idx).c_str();
}
inline QDebug &operator<<(QDebug &out, CCSignedIndex idx)
{
  return out << toQString(idx);
}

} // end namespace mdx

// Add hash functions, unless compiling for cuda
#ifndef __CUDACC__
#include <Common.hpp>
#include <boost/functional/hash.hpp>
namespace std
{
  HASH_NS_ENTER
  template<>
  struct hash<mdx::CCIndex> {
    size_t operator()(const mdx::CCIndex &idx) const
    {
      return (size_t)(idx.value);
    }
  };
  // CCIndex pair
  template <>
  struct hash<std::pair<mdx::CCIndex, mdx::CCIndex> >
  {
    size_t operator()(const std::pair<mdx::CCIndex, mdx::CCIndex> &pr) const
    {
      std::size_t seed = 0;
      boost::hash_combine(seed, size_t(pr.first.value));
      boost::hash_combine(seed, size_t(pr.second.value));
      return seed;
    }
  };


  template<>
  struct hash<mdx::CCSignedIndex> {
    size_t operator()(const mdx::CCSignedIndex &idx) const
    {
      const static hash<mdx::CCIndex> cellHasher;
      size_t cellHash = cellHasher(~idx);
      if(idx.orientation() == mdx::ccf::POS)
        return cellHash;
      else
        return ~cellHash;
    }
  };
  template<>
  struct hash< mdx::ccf::Flip<mdx::CCIndex> > {
    size_t operator()(const mdx::ccf::Flip<mdx::CCIndex> &flip) const
    {
      // combination after boost::hash_combine
      const static hash<mdx::CCIndex> cellHasher;
      const static size_t magic = 0x9e3779b9;
      size_t currentHash = cellHasher(flip.joint);
      currentHash ^= cellHasher(flip.interior) + magic + (currentHash << 6) + (currentHash >> 2);
      int least = (flip.facet[0] < flip.facet[1]) ? 0 : 1;
      currentHash ^= cellHasher(flip.facet[least]) + magic + (currentHash << 6) + (currentHash >> 2);
      currentHash ^= cellHasher(flip.facet[1 - least]) + magic + (currentHash << 6) + (currentHash >> 2);
      return currentHash;
    }
  };
  HASH_NS_EXIT
}
#endif

#endif // __CCINDEX_HPP
