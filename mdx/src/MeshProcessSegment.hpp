//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_SEGMENT_HPP
#define MESH_PROCESS_SEGMENT_HPP

#include <Process.hpp>
#include <CCUtils.hpp>
#include <MeshUtils.hpp>

class Ui_LoadHeatMap;

namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{

  /**
   * \class MeshRelabelFaces MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Relabel all the faces in a mesh
   */
  class mdxBase_EXPORT MeshRelabelFaces : public Process 
  {
  public:
    MeshRelabelFaces(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Relabel Faces");
      setDesc("Relabel mesh faces");
      setIcon(QIcon(":/images/Relabel.png"));

      addParm("Label Start", "Starting label", "1");
      addParm("Label Step", "Step between labels", "1");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("MeshRelabelFaces No current mesh");

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("MeshRelabelFaces No current cell complex");

      const CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, parm("Label Start").toInt(), parm("Label Step").toInt());
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, int start, int step);
  };

  /**
   * \class MeshRelabelCells MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Relabel all the cells in a segmented surface mesh
   */
  class mdxBase_EXPORT MeshRelabelCells : public Process 
  {
  public:
    MeshRelabelCells(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Relabel Cells");
      setDesc("Relabel cells on a segmented surface mesh");
      setIcon(QIcon(":/images/Relabel.png"));

      addParm("Label Start", "Starting label", "1");
      addParm("Label Step", "Step between labels", "1");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("MeshRelabelFaces No current mesh");

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("MeshRelabelFaces No current cell complex");

      const CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, parm("Label Start").toInt(), parm("Label Step").toInt());
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, int start, int step);
  };

  /**
   * \class MeshRelabelVolumes MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Relabel all volumes in a 3D mesh
   */
  class mdxBase_EXPORT MeshRelabelVolumes : public Process 
  {
  public:
    MeshRelabelVolumes(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Relabel Volumes");
      setDesc("Relabel volumes in a 3D mesh");
      setIcon(QIcon(":/images/Relabel.png"));

      addParm("Label Start", "Starting label", "1");
      addParm("Label Step", "Step between labels", "1");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No current cell complex").arg(name());

      const CCStructure &cs = mesh->ccStructure(ccName);
      if(cs.maxDimension() < 3) 
        throw QString("%1::run This process required a 3D mesh").arg(name());

      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, parm("Label Start").toInt(), parm("Label Step").toInt());
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, int start, int step);
  };

  /**
   * \class MeshLabel3DCellFaces MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Label the interfaces between 3D cells
   */
  class mdxBase_EXPORT MeshLabel3DCellFaces : public Process 
  {
  public:
    MeshLabel3DCellFaces(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Label 3D Cell Faces");
      setDesc("Label the faces between 3D cells");
      setIcon(QIcon(":/images/Relabel.png"));

      addParm("Label Outside", "Label outside faces with cell labels", "Yes", booleanChoice());
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No current cell complex").arg(name());

      const CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      return run(*mesh, cs, indexAttr, stringToBool(parm("Label Outside")));
    }
    bool run(Mesh &mesh, const CCStructure &cs, CCIndexDataAttr &indexAttr, bool labelOutside = true);
  };

  /**
   * \class SegmentClear MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Clear the segmentation. If something is selected, only those faces are cleared.
   *
   */
  class mdxBase_EXPORT MeshSegmentClear : public Process 
  {
  public:
    MeshSegmentClear(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Clear Segmentation");
      setDesc("Clear labels from segmented cells.");
      setIcon(QIcon(":/images/SegmentClear.png"));
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No mesh selected").arg(name());

      progressStart(QString("Projecting signal onto mesh"), 0);
      const CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      return run(cs, indexAttr);
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr);
  };

  /**
   * \class MeshSetLabel MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Set selected cells to a specific label
   */
  class mdxBase_EXPORT MeshSetLabel : public Process 
  {
  public:
    MeshSetLabel(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Set Label");
      setDesc("Set (or clear = 0) the labeling on the selected cells.");
      setIcon(QIcon(":/images/SetLabel.png"));

      addParm("Label", "Label to set", "0");
      addParm("Labeling", "Labeling to use, empty for current", "");
      addParm("Label Name", "Name for this label, empty to keep existing", "");
      addParm("Dimension", "Dimension to set", "All", dimChoice());
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::No mesh selected").arg(name());

      const CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      QString labeling = parm("Labeling");
      if(labeling.isEmpty())
        labeling = mesh->labeling();

      int label = parm("Label").toInt();
      IntIntAttr *labelMap = 0;
      if(labeling != "Labels") {
        labelMap = &mesh->labelMap(labeling);
        QString labelName = parm("Label Name");
        if(!labelName.isEmpty())
          mesh->labelName(labeling)[label] = labelName;
      }
      QString dimension = parm("Dimension");

      return run(cs, indexAttr, labelMap, label, dimension);
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, IntIntAttr *labelMap, int label, const QString dimension);
  };

  /**
   * \class MeshVertexLabelsFromFaces MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Propagate the face labels to the vertices
   */
  class mdxBase_EXPORT MeshVertexLabelsFromFaces : public Process 
  {
  public:
    MeshVertexLabelsFromFaces(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Vertex Labels From Faces");
      setDesc("Propagate the face labels to the vertices");
      setIcon(QIcon(":/images/SetLabel.png"));
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::No mesh selected").arg(name());

      const CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      return vertexLabelsFromFaces(cs, indexAttr, activeVertices(cs, indexAttr));
    }
  };

  /**
   * \class MeshVertexLabelsFromVolumes MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Propagate the volume labels to the vertices
   */
  class mdxBase_EXPORT MeshVertexLabelsFromVolumes : public Process 
  {
  public:
    MeshVertexLabelsFromVolumes(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Vertex Labels From Volumes");
      setDesc("Propagate the face labels to the vertices");
      setIcon(QIcon(":/images/SetLabel.png"));
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::No mesh selected").arg(name());

      const CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);

      return vertexLabelsFromVolumes(cs, indexAttr, activeVertices(cs, indexAttr));
    }
  };


  /**
   * \class SegmentMesh MeshProcessSegment.hpp <MeshProcessSegment.hpp>
   *
   * Segment the mesh using a seeded watershed algorithm.
   *
   * The mesh is assumed to already contain seeds for the watershed. For proper
   * use on this process, you should read the protocols and manuals published.
   */
  class mdxBase_EXPORT SegmentMesh : public Process 
  {
  public:
    SegmentMesh(const Process& process) : Process(process), inQueue(false)
    {
      setName("Mesh/Segmentation/Watershed Segmentation");
      setDesc("Segment the mesh signal into cells (labels) using the watershed algorithm.");
      setIcon(QIcon(":/images/SegmentMesh.png"));

      addParm("Steps", "Steps between visualization, increase to run faster", "50000");
      addParm("Fill Corners", "Fill corners by performing one step of face based segmentation "
              "after running the vertex based segmentation", "Yes", booleanChoice());
      addParm("Signal", "Signal to use, empty for current signal", "");
    }
  
    bool initialize(QWidget *parent) 
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No mesh selected").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::initailize No mesh selected").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::initialize No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName); 

      return initialize(cs, indexAttr, signalAttr);
    }
    bool initialize(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr);
  
    bool step()
    {
      if(!mesh) 
        throw QString("%1:step No mesh selected").arg(name());
      if(ccName.isEmpty())
        throw QString("%1:step No mesh selected").arg(name());
      if(signalName.isEmpty())
        throw QString("%1:step No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      mesh->updateProperties(ccName);
      return step(cs, indexAttr, signalAttr, parm("Steps").toUInt());
    }
    bool step(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, uint maxsteps);

    bool finalize(QWidget *parent) 
    {
      if(!mesh) 
        throw QString("%1:finalize No mesh selected").arg(name());
      if(ccName.isEmpty())
        throw QString("%1:finalize No mesh selected").arg(name());
      if(signalName.isEmpty())
        throw QString("%1:finalize No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      return finalize(cs, indexAttr, signalAttr);
    }
    bool finalize(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr);

  private:
    Mesh *mesh;
    QString ccName, signalName;

    CellNeighbors neighbors;

    // Vertex queue
    std::multimap<float, CCIndex> Q;
    tbb::concurrent_unordered_map<CCIndex, bool, std::hash<CCIndex> > inQueue;
  };

  /**
   * \class SegmentMeshFaces ProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
   *
   * Segment the mesh using a seeded watershed algorithm.
   *
   * The mesh is assumed to already contain seeds for the watershed. For proper
   * use on this process, you should read the protocols and manuals published.
   */
  class mdxBase_EXPORT SegmentMeshFaces : public Process 
  {
  public:
    SegmentMeshFaces(const Process& process) : Process(process), inQueue(false)
    {
      setName("Mesh/Segmentation/Watershed Segmentation Faces");
      setDesc("Segment the mesh signal into cells (labels) using the watershed algorithm.\n"
              "This version operates directly on the faces");
      setIcon(QIcon(":/images/SegmentMesh.png"));

      addParm("Steps", "Steps between visualization, increase to run faster", "50000");
      addParm("Signal", "Signal to use, empty for current signal", "");
    }
  
    bool initialize(QWidget *parent) 
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:initialize No mesh selected").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1:initialize No mesh selected").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1:initialize No signal name").arg(name());

      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      CCStructure &cs = mesh->ccStructure(ccName);
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      
      return initialize(cs, indexAttr, signalAttr);
    }
    bool initialize(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr);

  
    bool step()
    {
      if(!mesh) 
        throw QString("%1:step No mesh selected").arg(name());
      if(ccName.isEmpty())
        throw QString("%1:step No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      mesh->updateProperties(ccName);
      return step(cs, indexAttr, signalAttr, parm("Steps").toUInt());
    }
    bool step(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, uint maxsteps);
  
  private:
    Mesh *mesh = 0;
    QString ccName, signalName;
    CellNeighbors neighbors;

    // Vertex queue
    std::multimap<float, CCIndex> Q;
    tbb::concurrent_unordered_map<CCIndex, bool, std::hash<CCIndex> > inQueue;
  };

//  /**
//   * \class GrabLabelsSegment ProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
//   *
//   * Grab labels from other mesh (3D meshes).
//   */
//  class mdxBase_EXPORT GrabLabelsSegment : public Process 
//  {
//  public:
//    GrabLabelsSegment(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
//        return false;
//  
//      Mesh* mesh1, *mesh2;
//      if(currentMesh() == mesh(0)) {
//        mesh1 = mesh(0);
//        mesh2 = mesh(1);
//      } else if(currentMesh() == mesh(1)) {
//        mesh2 = mesh(0);
//        mesh1 = mesh(1);
//      } else
//        return false;
//  
//      bool res = run(mesh1, mesh2, parms[0].toFloat());
//      return res;
//    }
//  
//    bool run(Mesh* mesh1, Mesh* mesh2, float tolerance);
//  
//      setName("Mesh/Segmentation/3D Grab Labels");
//      setDesc("Grab labels from other mesh (3D meshes).");
//    QStringList parmNames() const { return QStringList() << QString("Tolerance (%1)").arg(UM));
//    QStringList parmDescs() const { return QStringList() 
//      << "Maximal distance between matching cells. ");
//    QStringList parmDefaults() const { return QStringList() << "5.0");
//      setIcon(QIcon(":/images/GrabLabels.png"));
//  };
//  
//  /**
//   * \class LabelSelected ProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
//   *
//   * Change the label of the selected vertices.
//   */
//  class mdxBase_EXPORT LabelSelected : public Process 
//  {
//  public:
//    LabelSelected(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_LABEL))
//        return false;
//      return run(currentMesh(), parms[0].toInt());
//    }
//  
//    bool run(Mesh* mesh, int label);
//  
//      setName("Mesh/Segmentation/Label Selected Vertices");
//    QString description() const { return 
//      "Label selected vertices with the same value, which can be a new label");
//    QStringList parmNames() const { return QStringList() << "Label");
//    QStringList parmDescs() const { return QStringList() 
//      << "Asign this label to all selected vertices.");
//    QStringList parmDefaults() const { return QStringList() << "0");
//      setIcon(QIcon(":/images/LabelSelected.png"));
//  };
//  
//  /**
//   * \class RelabelCells3D ProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
//   *
//   * Relabel 3D cells. Each connected region will be given a unique label.
//   */
//  class mdxBase_EXPORT RelabelCells3D : public Process 
//  {
//  public:
//    RelabelCells3D(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      Mesh* mesh = currentMesh();
//      bool res = run(mesh, parms[0].toInt(), parms[1].toInt());
//      if(res)
//        mesh->setShowLabel("Label");
//      return res;
//    }
//  
//    bool run(Mesh* mesh, int label_start, int label_step);
//  
//      setName("Mesh/Segmentation/Relabel 3D Cells");
//    QString description() const { return 
//      "Relabel 3D cells (connected regions)."
//      " A start label of -1 is used to indicate continuing using current labels.");
//    QStringList parmNames() const { return QStringList() << "Label start" << "Label step");
//    QStringList parmDescs() const { return QStringList() 
//      << "Starting label. Use -1 to continue from last current label." << "Increment step.");
//    QStringList parmDefaults() const { return QStringList() << "-1" << "1");
//      setIcon(QIcon(":/images/CellFiles3D.png"));
//  };
  
  /**
   * \class MeshCombineRegions ProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
   *
   * Combine over-segmented regions, based on mesh signal.
   */
  class MeshCombineRegions : public Process 
  {
  public:
    MeshCombineRegions(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Combine Labels");
      setDesc("Combine over-segmented regions, based on mesh signal.");
      setIcon(QIcon(":/images/Merge.png"));

      addParm("Border Distance", "Half of the cell border width on the mesh.", "1.0");
      addParm("Threshold", "Ratio of border signal over internal signal. If the border between 2 cells "
              "has a ratio below the threshold, the cells are fused.", "1.1");
      addParm("Signal", "Signal to use, empty for current signal", "");
    }
  
    bool step()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1:run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      mesh->updateProperties(ccName);
      return step(cs, indexAttr, signalAttr, parm("Border Distance").toDouble(), parm("Threshold").toDouble());
    }
    bool step(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, double borderDist, double Difference);
  };
  
/**
   * \class MeshAutoSegment ProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
   *
   * Auto-Segmentation of the mesh surface based on signal. Combines blurring, auto-seeding,
   * label propagation by watershed and fusion of over-segmented cells.
   */
  class MeshAutoSegment : public Process 
  {
  public:
    MeshAutoSegment(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Auto Segmentation");
      setDesc("Auto-Segmentation of the mesh surface based on signal. Combines blurring, auto-seeding, "
              "label propagation by watershed and fusion of over-segmented cells.");
      setIcon(QIcon(":/images/SegmentMesh.png"));

      addParm("Update", "Option to update mesh drawing while running processes.", "Yes", booleanChoice());
      addParm("Normalize", "Option to normalize mesh signal before merging the cells. Most useful in case "
              "of low contrast mesh signal.", "No", booleanChoice());
      addParm("Blur Cell Radius", "Radius used for gaussian blur of mesh signal. In normal cases, should be equal "
              "to auto-seed radius.", "2.0");
      addParm("Auto-Seed Radius", "Radius used for auto-seeding the mesh in microns, based on local minima of signal. "
              " Should be equal to radius of smallest cells.", "2.0");
      addParm("Blur Borders Radius", "Radius in microns used for Gaussian blur of signal on cell borders "
              "before watershed segmentation. " "Use small values (roughly, half the border width) to avoid "
              "border signal distortion.", "1.0");
      addParm("Normalization Radius", "Radius in microns used for local normalization of signal. "
              "The approximate radius of largest cells usually works well.", "5.0");
      addParm("Border Distance", "Half of the cell border width in microns after blurring. "
              "Used for fusion of over-segmented cells.", "1.0");
      addParm("Combine Threshold", "Ratio of border signal over internal signal. If the borders between "
              "2 cells have a ratio below the threshold, the cells are fused.", "1.1");
      addParm("Signal", "Signal to use, empty for current signal", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No mesh selected").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1:run No signal name").arg(name());

      return run(*mesh, ccName, signalName, stringToBool(parm("Update")), stringToBool(parm("Normalize")), 
        parm("Blur Cell Radius").toDouble(), parm("Auto-Seed Radius").toDouble(), parm("Blur Borders Radius").toDouble(), 
        parm("Normalization Radius").toDouble(), parm("Border Distance").toDouble(), parm("Combine Threshold").toDouble());
    }
  
    bool run(Mesh &mesh, const QString &ccName, const QString &signalName, bool updateView, bool normalize, double gaussianRadiusCell, 
           double localMinimaRadius, double gaussianRadiusWall, double normalizeRadius, double borderDist, double threshold);
  };
  ///@}
}

#endif
