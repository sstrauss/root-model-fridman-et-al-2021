//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "ColorEditorDlg.hpp"
#include "Colors.hpp"

#include <QColorDialog>

ColorEditorDlg::ColorEditorDlg(mdx::Colors* colors, QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f), model(colors)
{
  ui.setupUi(this);
  model->backupColors();
  ui.colorsView->setModel(model);

  int row = model->rowCount();
  for(int i = 0; i < row; ++i) {
    const QModelIndex& index = model->index(i, 0);
    ui.colorsView->setExpanded(index, true);
    // int srow = model->rowCount(index);
    // for(int j = 0 ; j < srow ; ++j)
    //{
    // const QModelIndex& idx = model->index(i, 0, index);
    // ui.colorsView->setItemsExpandable(idx, false);
    //}
  }
  ui.colorsView->resizeColumnToContents(0);
}

void ColorEditorDlg::on_colorsView_doubleClicked(const QModelIndex& idx)
{
  QColor col = model->color(idx);
  if(col.isValid()) {
    col = QColorDialog::getColor(col, this, "Change color");
    if(col.isValid()) {
      model->setColor(idx, col);
    }
  }
}

void ColorEditorDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui.buttonBox->buttonRole(btn)) {
  case QDialogButtonBox::RejectRole:
    model->restoreColors();
    break;
  case QDialogButtonBox::ApplyRole:
  case QDialogButtonBox::AcceptRole:
    model->backupColors();
    break;
  default:
    break;
  }
}
