//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

// Surface class
#ifndef SURFACE_HPP
#define SURFACE_HPP

#include <cmath>
#include <Parms.hpp>
#include <QString>
#include <CellTissue.hpp>

namespace mdx
{
  class mdxBase_EXPORT Surface 
  {
  public: 
    Surface() {};
    virtual ~Surface() {}

    // Methods to be implemented in derived classes, define some defaults.
    virtual bool processParms(const QStringList &parms) { return true; };
    virtual bool setPoint(vertex p, vertex sp, Point3d cp) { return true; };
    virtual bool updatePos(vertex p) { return true; };
    virtual bool updateNormal(vertex p) { return true; };
    virtual bool initialCell(CellTissue &T, double size, int cellInitWall);
    virtual double distance(const vertex &u, const vertex &v) { return norm(u->pos - v->pos); }; 

    // We will normaly have on or the other of these
    virtual bool growPoint(vertex p, double dt, double time) { return true; };
    virtual bool growSurface(double time) { return true; };
  };  
}  
#endif
