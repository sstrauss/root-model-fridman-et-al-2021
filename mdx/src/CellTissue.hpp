//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_TISSUE_HPP
#define CC_TISSUE_HPP

#include <Geometry.hpp>
#include <Attributes.hpp>
#include <CCF.hpp>
#include <CellTissue.hpp>
#include <Subdivide.hpp>
#include <MDXSubdivide.hpp>
#include <ProcessParms.hpp>
/*
 * Class to implement 2D tissue, can this be generalized to 3D?
 */
namespace mdx 
{
  // Main cell tissue parameters
  class TissueParms : virtual public ProcessParms
  {
  public:
    TissueParms()
    {
      setName("Tissue Parms");
      addParm("Dimension","Dimension of cells in the tissue", "2");
      addParm("Tissue", "Name of cell complex for the tissue", "Tissue");
      addParm("Tissue Dual", "Name of cell complex for the cell graph (dual)", "TissueDual");
    }
    ~TissueParms() {}
  };

  /* A CellTissue is a tissue of two-dimensional cells.  The geometry of
   * these cells is stored in the primary cell complex, but a dual graph
   * is also maintained for operations between cells.
   */
  class CellTissue
  {
  public:
    // Constructor
    CellTissue() : cellDim(0), cs(0), dg(0), _indexAttr(0)
    {
      // Give the tissue some defaults
      processTissueParms(TissueParms());
      subdiv.tissue = this;
    }

    // Initialize the tissue 
    bool initialize(CCStructure *_cs, CCStructure *_dg, CCIndexDataAttr *indexAttr);
    bool processTissueParms(const TissueParms &tissueParms);

    // Add a cell to a cell tissue.
    bool addCell(CCIndex cell);
  
    // Remove a cell from the cell tissue.
    bool removeCell(CCIndex cell);
  
    // Update the geometric parameters: measures and centroids.
    void updateGeometry(void);

    // Create the dual complex.
    bool createDual();

    // Clear the tissue.
    void clear();

    // Access methods
    CCStructure &cellStructure() 
    { 
      if(!cs)
        throw(QString("CellTissue::cellStructure Cell Structure is null"));
      else
        return *cs; 
    }
    CCStructure &dualGraph() 
    { 
      if(!dg)
        throw(QString("CellTissue::dualGraph Dual Graph is null"));
      else
        return *dg; 
    }
    CCIndexDataAttr &indexAttr() 
    { 
      if(!_indexAttr)
        throw(QString("CellTissue::indexAttr Index attribute is null"));
      else
        return *_indexAttr; 
    }

    Dimension cellDimension(void) const
    {
      return cellDim;
    }

    const QString &tissueName() { return TissueName; }
    const QString &tissueDualName() { return TissueDualName; }

    // Subdivider
    class CellTissueSubdivide : public Subdivide
    {
    public:
      CellTissue *tissue;

      // Update the cell data
      void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct &ss, 
                           CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(),
                           double interpPos = 0.5);
    };

    CellTissueSubdivide *subdivider(void) { return &subdiv; }

  private:
    Dimension cellDim;

    TissueParms *tissueParms;
    CellTissueSubdivide subdiv;

    CCStructure *cs;  // complete cell structure
    CCStructure *dg;  // cell structure of dual graph
    CCIndexDataAttr *_indexAttr; // Attribute map

    QString TissueName; // Name of cell complex for tissue
    QString TissueDualName; // Name of cell graph (dual)
    QString DeleteProcessName; // Name of process to delete cells
  };
}

#endif
