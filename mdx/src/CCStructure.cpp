#include <CCStructure.hpp>

#include <boost/variant/static_visitor.hpp>

using namespace mdx;

struct ConfirmCCFVisitor
  : public boost::static_visitor<bool>
{
  bool operator()(ccf::CCStructure &cc) { return true; }

  template <typename T>
  bool operator()(T &t) { return false; }
};

bool mdx::confirmCCF(CCStructure &cc)
{
  static ConfirmCCFVisitor visitor;
  return cc.varCC.apply_visitor(visitor);
}

struct EnsureCCFVisitor
  : public boost::static_visitor<ccf::CCStructure&>
{
  ccf::CCStructure &operator()(ccf::CCStructure &cc)
  { return cc; }

  template <typename T>
  ccf::CCStructure &operator()(T &t)
  {
    throw(QString("Could not perform operation on a CCStructure of type %1;\n"
		  "convert to a CCF_CellStructure first.").arg(t.ccsType()));
  }
};

ccf::CCStructure &mdx::ensureCCF(CCStructure &cc)
{
  static EnsureCCFVisitor visitor;
  return cc.varCC.apply_visitor(visitor);
}

struct EnsureCCFConstVisitor
  : public boost::static_visitor<const ccf::CCStructure&>
{
  const ccf::CCStructure &operator()(const ccf::CCStructure &cc)
  { return cc; }

  template <typename T>
  const ccf::CCStructure &operator()(T &t)
  {
    throw(QString("Could not perform operation on a CCStructure of type %1;\n"
		  "convert to a CCF_CellStructure first.").arg(t.ccsType()));
  }
};

const ccf::CCStructure &mdx::ensureCCF(const CCStructure &cc)
{
  static EnsureCCFConstVisitor visitor;
  return cc.varCC.apply_visitor(visitor);
}
