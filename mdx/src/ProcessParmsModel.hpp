//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PROCESS_PARMS_MODEL_HPP
#define PROCESS_PARMS_MODEL_HPP

#include <Process.hpp>

#include <QAbstractTableModel>
#include <QStyledItemDelegate>

class FreeFloatDelegate : public QStyledItemDelegate 
{
  Q_OBJECT
public:
  FreeFloatDelegate(QObject* parent = 0);
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
  void setEditorData(QWidget* editor, const QModelIndex& index) const;
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
  void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const;
};

class ProcessParmsModel : public QAbstractTableModel 
{
  Q_OBJECT
public:
  ProcessParmsModel(QObject* parent = 0) : QAbstractTableModel(parent) {}
  void setParms(const mdx::ProcessDefinition& def);
  void setParms(const QStringList &parms);
  void clear();

  int rowCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return names.size();
  }
  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return 2;
  }
  QVariant data(const QModelIndex& index, int role) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
  Qt::ItemFlags flags(const QModelIndex& index) const;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
  const QStringList &parms() const { return _parms; }

  QStringList parmChoice(int pos) const;

signals:
  void valuesChanged();

protected:
  QStringList names;
  QStringList descs;
  QStringList _parms;
  mdx::ParmChoiceList _parmChoice;
};
#endif
