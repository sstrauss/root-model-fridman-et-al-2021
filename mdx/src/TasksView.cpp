//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "TasksView.hpp"

#include <QKeyEvent>
#include <QItemSelectionModel>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>

#include <QMimeData>

#include <QTextStream>
#include <stdio.h>

static QTextStream err(stderr);

TasksView::TasksView(QWidget* parent) : QTreeView(parent)
{
  setDragDropMode(QAbstractItemView::InternalMove);
}

void TasksView::keyPressEvent(QKeyEvent* event)
{
  if(event->matches(QKeySequence::Delete)) {
    QItemSelectionModel* select = selectionModel();
    emit deleteItems(select->selectedIndexes());
    event->accept();
    return;
  }
  QTreeView::keyPressEvent(event);
}

const QString itemlist_format = QString("application/x-qabstractitemmodeldatalist");
const QString internal_format = QString("MorphoDynamX/modeldatalist");

void TasksView::dragEnterEvent(QDragEnterEvent* event)
{
  const QStringList& lst = event->mimeData()->formats();
  if(lst.contains(itemlist_format) or lst.contains(internal_format))
    QTreeView::dragEnterEvent(event);
  else
    event->ignore();
}

void TasksView::dragLeaveEvent(QDragLeaveEvent* event) {
  QTreeView::dragLeaveEvent(event);
}

void TasksView::dragMoveEvent(QDragMoveEvent* event)
{
  QRect rect = event->answerRect();
  QModelIndex idx = indexAt(rect.center());
  if(idx.isValid()) {
    QTreeView::dragMoveEvent(event);
    return;
  }
  event->ignore();
}
