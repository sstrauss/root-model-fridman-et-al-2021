//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "MeshProcessMulti.hpp"
#include "Information.hpp"
#include "Progress.hpp"

namespace mdx 
{
  bool CopySwapMeshes::run(const QString& actionStr)
  {
    Mesh* mesh1 = getMesh(0);
    if(!mesh1) {
      setErrorMessage("Error, mesh 1 empty");
      return false;
    }
    Mesh* mesh2 = getMesh(1);
    if(!mesh2) {
      setErrorMessage("Error, mesh 2 empty");
      return false;
    }
  
    if(actionStr == "1 -> 2") {
      QByteArray ba;
      QBuffer bf(&ba);
      bf.open(QIODevice::WriteOnly);
      mesh1->write(bf, false);
      bf.close();

      mesh2->reset();
      bf.open(QIODevice::ReadOnly);
      mesh2->read(bf, false);
      mesh2->updateAll();
    } else if(actionStr == "1 <- 2") {
      QByteArray ba;
      QBuffer bf(&ba);
      bf.open(QIODevice::WriteOnly);
      mesh2->write(bf, false);
      bf.close();

      mesh1->reset();
      bf.open(QIODevice::ReadOnly);
      mesh1->read(bf, false);
      mesh1->updateAll();
    } else if(actionStr == "1 <-> 2") {
      QByteArray ba1, ba2;
      QBuffer bf1(&ba1), bf2(&ba2);

      bf1.open(QIODevice::WriteOnly);
      mesh1->write(bf1, false);
      bf1.close();

      bf2.open(QIODevice::WriteOnly);
      mesh2->write(bf2, false);
      bf2.close();

      mesh1->reset();
      bf2.open(QIODevice::ReadOnly);
      mesh1->read(bf2, false);

      mesh2->reset();
      bf1.open(QIODevice::ReadOnly);
      mesh2->read(bf1, false);

      mesh1->updateAll();
      mesh2->updateAll();
    } else {
      setErrorMessage("Error, bad action:" + actionStr);
      return false;
    }
    return true;
  }
  REGISTER_PROCESS(CopySwapMeshes);
}
