#include <GeomMathUtils.hpp>
#include <Matrix.hpp>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
//#include <gsl/gsl_eigen.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>

namespace mdx
{

//----Matrix operations and decompositions---//

  void PolarDecomp(Matrix3d &M,Matrix3d &S, Matrix3d &U){
    Matrix3d Usvd;
    Matrix3d Ssvd;
    Matrix3d Vsvd;

    SVDDecomp(M,Usvd,Ssvd,Vsvd);

    U = Usvd*transpose(Vsvd);
    S = Vsvd*Ssvd*transpose(Vsvd);
  }



  void SVDDecomp(const Matrix3d &M, Matrix3d &U, Matrix3d &S, Matrix3d &V){
     gsl_matrix* Mgsl = gsl_matrix_alloc(3, 3);
     gsl_vector* Sgsl = gsl_vector_alloc(3);
     gsl_matrix* Vgsl = gsl_matrix_alloc(3, 3);
     gsl_vector* wgsl = gsl_vector_alloc(3);

    
     for(int i = 0; i < 3; ++i)
       for(int j = 0; j < 3; ++j)
         gsl_matrix_set(Mgsl, i, j, M[i][j]);
     gsl_linalg_SV_decomp(Mgsl, Vgsl, Sgsl, wgsl);

     for(int i = 0; i < 3; ++i)
       for(int j = 0; j < 3; ++j){
          U[i][j] = gsl_matrix_get(Mgsl,i,j);
          V[i][j] = gsl_matrix_get(Vgsl,i,j);
       }
     //This could be passed around as just a 3D vector, but we'll use a matrix for now
     S = Matrix<3,3,double>::Diagonal(Point3d(gsl_vector_get(Sgsl, 0), gsl_vector_get(Sgsl, 1), gsl_vector_get(Sgsl, 2)));

   }



//----Triangle based computations----//
/*
 Matrix2d DefGradient(Point2d p[],Point2d q[]){

  Matrix2d dGrad;
  Point2d grad_X = triangleGradient(p[0],p[1],p[2],q[0].x(),q[1].x(),q[2].x());
  Point2d grad_Y = triangleGradient(p[0],p[1],p[2],q[0].y(),q[1].y(),q[2].y());
  dGrad[0] = grad_X;
  dGrad[1] = grad_Y;
  return dGrad;

}
*/
  Matrix3d DefGradient(Point3d p[],Point2d q[]){

  Matrix3d dGrad;
  Point3d grad_X = triangleGradient(p[0],p[1],p[2],q[0].x(),q[1].x(),q[2].x());
  Point3d grad_Y = triangleGradient(p[0],p[1],p[2],q[0].y(),q[1].y(),q[2].y());
  Point3d grad_Z = Point3d(0,0,0);//triangleGradient(p[0],p[1],p[2],0,0,0);
  dGrad[0] = grad_X;
  dGrad[1] = grad_Y;
  dGrad[2] = grad_Z;
  return dGrad;

}

Matrix3d DefGradient(Point3d p[],Point3d q[])
{
  return Matrix3d({triangleGradient(p[0],p[1],p[2],q[0].x(),q[1].x(),q[2].x()), 
                   triangleGradient(p[0],p[1],p[2],q[0].y(),q[1].y(),q[2].y()),
                   triangleGradient(p[0],p[1],p[2],q[0].z(),q[1].z(),q[2].z())});
}


 double triangleQuality(Point3d a,Point3d b,Point3d c){
    Point3d dab = a-b;
    Point3d dbc = b-c;
    Point3d dca = c-a;
    double tri_area = triangleArea(a,b,c);
  
    return tri_area/pow((dab*dab)*(dbc*dbc)*(dca*dca),1.0/3.0);
  }

}
