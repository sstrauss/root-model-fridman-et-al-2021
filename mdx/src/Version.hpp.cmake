/**
 * Version information.
 * Avoid including this file via headers. Placing it in a header will trigger
 * a rebuild of all the files depending on the header. That, in turn,
 * discourages people from always building with valid version information.
 */
#ifndef VERSION_H
#define VERSION_H

#define VERSION "${MDX_VERSION}"
#define REVISION "${MDX_REVISION}"

#endif // VERSION_H


