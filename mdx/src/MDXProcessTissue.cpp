//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MDXProcessTissue.hpp>

// Processes for tissue
namespace mdx
{
  bool CellTissueProcess::initialize(Mesh &mesh)
  {
    // Get the tissue and cell graph names
    int cellDim = parm("Dimension").toInt();
    TissueName = parm("Tissue");
    TissueDualName = parm("Tissue Dual");

    // Initialize the tissue
    cellTissue.processTissueParms(*this);
    if(mesh.ccStructure(TissueName).maxDimension() != cellDim)
      mesh.ccStructure(TissueName) = CCStructure(cellDim);
    if(mesh.ccStructure(TissueDualName).maxDimension() < 2)
      mesh.ccStructure(TissueDualName) = CCStructure(2);
       
    cellTissue.initialize(&mesh.ccStructure(TissueName), &mesh.ccStructure(TissueDualName), &mesh.indexAttr());
    cellTissue.createDual();
    cellTissue.updateGeometry();

    // Setup the complex attributes
    mesh.ccAttr(TissueName, "Type") = "Tissue";
    mesh.ccAttr(TissueName, "Dual") = TissueDualName;
    mesh.ccAttr(TissueDualName, "Type") = "TissueDual";
    mesh.ccAttr(TissueDualName, "Dual") = TissueName;

    mesh.updateAll(TissueName);
    mesh.updateAll(TissueDualName);
    return true;
  }
}
