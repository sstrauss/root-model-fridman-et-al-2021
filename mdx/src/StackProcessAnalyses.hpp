//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_ANALYSES_HPP
#define STACK_PROCESS_ANALYSES_HPP

#include <Process.hpp>

namespace mdx 
{
  ///\addtogroup StackProcess
  ///@{
  /**
   * \class ComputeVolume StackProcessAnalyses.hpp <StackProcessAnalyses.hpp>
   *
   * Compute the volume of each label and write the result in a CSV file.
   *
   * The volume is computed as the product between the number of voxel for each
   * label, and the volume of a single voxel.
   */
  class mdxBase_EXPORT ComputeVolume : public Process 
  {
  public:
    ComputeVolume(const Process& process) : Process(process) 
    {
      setName("Stack/Analyses/Compute Volumes");
      setDesc("Compute the volumes of the labels, number of voxels x voxel size.");
      setIcon(QIcon(":/images/CellFiles3D.png"));

      addParm("File Name", "The name of the file to write the data", "");
    }
    bool run();
    bool run(const Store* input, const QString &fileName);
  };
  ///@}
}
#endif
