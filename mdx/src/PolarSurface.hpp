//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef POLAR_SURFACE_HPP
#define POLAR_SURFACE_HPP

#include <complex>

#include <Common.hpp>
#include <Attributes.hpp>
#include <Function.hpp>
#include <Contour.hpp>
#include <ProcessParms.hpp>

#include <CellTissue.hpp>

namespace mdx
{
  class PolarSurface2D : public virtual ProcessParms
  {
  public:
    virtual ~PolarSurface2D() {}

    // * Parameters
    enum GrowthType { ARCLENGTH = 0 };

    struct PolarData
    {
      double r, theta;
      PolarData() : r(0), theta(0) {}
      PolarData(double _r, double _th) : r(_r), theta(_th) {}

      bool operator==(const PolarData &other) const
      {
        if(r == other.r and theta == other.theta)
          return true;
        return false;
      }
    };
    typedef AttrMap<CCIndex,PolarData> PolarAttr;

    // Subdivision object
    class Subdivide : virtual public mdx::Subdivide
    {
    public:
      // Constructor
      Subdivide() : surf(0) {}
      Subdivide(PolarSurface2D &_surf) : surf(&_surf) {}

      PolarSurface2D *surf;

      void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct& ss, 
          CCIndex otherN = CCIndex(), CCIndex otherP = CCIndex(), double interpPos = 0.5);
    };

    // Constructor, fill in default values
    PolarSurface2D() 
    { 
      addParm("Growth Type", "Arclength surface of revolution", "0");
      addParm("Surface Contour", "Surface contour definition file", "Surface.con");
      addParm("ArcLength Growth", "Arclength growth function", "Growth.func");
      addParm("Surface Scale", "Contour function Multiplier", "100");
      addParm("Growth Scale In Mult", "Growth function Multiplier", "1");
      addParm("Growth Scale Out Mult", "Growth function Multiplier", "150");
      addParm("Growth Scale Func", "Growth scale function", "GrowthScale.func");
      addParm("Root Search Max Steps", "Max number of iterations for point inversion", "100");
      addParm("Tissue Process", "Name of the Tissue process", "10 Cell Tissue");
    } 

    bool processParms();

    // Sampling and integrating functions
    void samplePositions(QString &contourFile, Point3d *pos);
    void sampleIntegral(QString &functionFile, double *v);

    Point3d polarToCartesian(double r, double theta) const;
    Point3d polarToCartesian(const PolarData &pd) const;

    // Move vertex vp to closest point on a polar surface to a given cartesian point (cp),
    // starting from a given vertex (vsp)
    bool setPoint(CCIndex vp, CCIndex vsp, Point3d cp);

    void computePos(CCIndex v);
    void computeNormal(CCIndex v);

    // Initialize
    bool initialize(CellTissue &tissue, PolarAttr *pAttr, CCIndexDataAttr *indexAttr);

    // Create an initial cell
    bool initialCell(CellTissue &tissue, double radius, int sides);

    // Grow a surface point
    void growPoint(CCIndex v, double dt, double time);

  private:
    PolarAttr *pAttr;
    CCIndexDataAttr *indexAttr;
    Function growthScaleFunc;       // Function relating growth to simulation time

    // Handling conversions to / from surface polar coordinates
    static constexpr double DX = 1.5e-4;    // delta for explicitly computing tangents and normals
    static const int SAMPLES = 10000;   // number of samples along radial position
    double dr;                          // distance between radial samples
    Point3d maxTangent;                 // tangent direction of surface at maximum sampled radius
    Point3d sampledPos[SAMPLES + 1];    // positions sampled along theta = 0
    double sampledGrowth[SAMPLES + 1];  // growth values sampled

    // Parameters
    int growthType;                  // 0 for growth as function of arclength
    QString surfaceContourFile;      // Filename for surface contour
    double surfaceScale;             // Global scaling of surface contour
    QString arcLengthGrowthFile;     // Filename for growth as function of arclength
    QString growthScaleFile;         // Filename for function f relating growth to time
    double growthScaleInMult;        // Constant k |--   growth(t) ~ c * f(kt)
    double growthScaleOutMult;       // Constant c /
    int rootSearchMaxSteps;          // Number of steps to search for closest point on surface
  };

  // Read/Write functions for attribute maps
  bool inline readAttr(PolarSurface2D::PolarData &m, const QByteArray &ba, size_t &pos) 
  {
    return readChar((char *)&m, sizeof(PolarSurface2D::PolarData), ba, pos);
  }
  bool inline writeAttr(const PolarSurface2D::PolarData &m, QByteArray &ba) 
  {
    return writeChar((char *)&m, sizeof(PolarSurface2D::PolarData), ba);
  }
} 
#endif 
