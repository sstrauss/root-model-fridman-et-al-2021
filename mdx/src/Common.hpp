#ifndef COMMON_HPP
#define COMMON_HPP

#include <Config.hpp>

// prior to C++11, initializing static members other than int was not standardized
// http://stackoverflow.com/questions/9141950/initializing-const-member-within-class-declaration-in-c#9208326
#if __cplusplus < 201103L
#  define constexpr const
#endif

// Use HASH_NS_ENTER/_EXIT to enter the appropriate namespace inside std:: (either none or tr1)
#define HASH_NS_ENTER
#define HASH_NS_EXIT

#ifdef MDX_USE_TR1
#  ifdef __GNUC__
#    include <Features.hpp>
#    if __GNUC_PREREQ(4, 0)
#      include <tr1/unordered_map>
#      include <tr1/unordered_set>
namespace std
{
  using std::tr1::unordered_map;
  using std::tr1::unordered_set;
  using std::tr1::hash;
}
#      undef HASH_NS_ENTER
#      undef HASH_NS_EXIT
#      define HASH_NS_ENTER namespace tr1 {
#      define HASH_NS_EXIT }
#    else
#      error "GCC does not have hash types before the version 4.0"
#    endif
#  endif
#else // Use C++11 STL types
#  include <unordered_map>
#  include <unordered_set>
#endif

#endif // COMMON_HPP
