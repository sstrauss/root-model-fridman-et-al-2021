//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "TransferFunction.hpp"
#include "Util.hpp"
#include "PresetColors.hpp"

#include <QTextStream>
#include <QStringList>
#include <algorithm>

#include <QTextStream>
#include <stdio.h>

namespace mdx {
  const double TransferFunction::epsilon = 1e-6;
  
  TransferFunction::TransferFunction(Interpolation i)
    : _interpolation(i), clamp(true), exteriorColor(0, 0, 0, 0) {}
  
  TransferFunction::TransferFunction(const TransferFunction& copy)
    : _interpolation(copy._interpolation), values(copy.values), keys(copy.keys), 
  	  clamp(copy.clamp), exteriorColor(copy.exteriorColor) {}
  
  TransferFunction TransferFunction::load(QString spec)
  {
    TransferFunction fct;
    QStringList all = spec.split("\n");
    for(int line_number = 0; line_number < all.size(); ++line_number) {
      QString line = all[line_number].trimmed();
      if(line.isEmpty() or line[0] == '#')
        continue;
      QStringList fields = line.split(":");
      QString header = fields[0].trimmed();
      fields.pop_front();
      QString value = fields.join(":").trimmed().toLower();
      if(header == "Interpolation") {
        if(value == "rgb")
          fct._interpolation = RGB;
        else if(value == "hsv")
          fct._interpolation = HSV;
        else if(value == "cyclic_hsv")
          fct._interpolation = CYCLIC_HSV;
      } else if(header == "Clamp") {
        fct.clamp = value == "true";
      } else if(header == "ExteriorColor") {
        QStringList cs = value.split(",");
        Colorf col;
        for(int i = 0; i < 4; ++i)
          col[i] = cs[i].toFloat();
        fct.exteriorColor = col;
      } else if(header == "ColoredPos") {
        QStringList vs = value.split("-");
        double pos = vs[0].toDouble();
        vs.pop_front();
        QString cols = vs.join("-");
        Colorf col;
        QStringList cs = cols.split(",");
        for(int i = 0; i < 4; ++i)
          col[i] = cs[i].toFloat();
        fct.values.push_back(std::make_pair(pos, col));
      }
    }
    fct.clamp = true;
    if(fct.size() < 2)
      fct.clear();
    else
      fct.update_keys();
    return fct;
  }
  
  QString TransferFunction::dump() const
  {
    QString result;
    QTextStream ts(&result, QIODevice::WriteOnly);
    ts << "Interpolation: ";
    switch(_interpolation) {
    case RGB:
      ts << "rgb";
      break;
    case HSV:
      ts << "hsv";
      break;
    case CYCLIC_HSV:
      ts << "cyclic_hsv";
      break;
    }
    ts << endl;
    ts << "Clamp: " << (clamp ? "true" : "false") << endl;
    ts << "ExteriorColor: "
       << QString("%1,%2,%3,%4").arg(exteriorColor[0]).arg(exteriorColor[1]).arg(exteriorColor[2]).arg(exteriorColor[3])
       << endl;
    for(size_t i = 0; i < values.size(); ++i) {
      Colorf col = values[i].second;
      if(_interpolation == RGB) {
        if(col[0] < 0)
          col[0] = 0;
        else if(col[0] > 1)
          col[0] = 1;
      } else {
        if(col[0] < 0)
          col[0] += 360;
        else if(col[0] >= 360)
          col[0] -= 360;
      }
      for(int j = 1; j < 4; ++j) {
        if(col[j] < 0)
          col[j] = 0;
        else if(col[j] > 1)
          col[j] = 1;
      }
      ts << "ColoredPos: " << values[i].first << "-"
         << QString("%1,%2,%3,%4").arg(values[i].second[0]).arg(values[i].second[1]).arg(values[i].second[2]).arg(
        values[i].second[3]) << endl;
    }
    return result;
  }
  
  template <typename T1, typename T2> struct ComparePairFirst {
    bool operator()(const std::pair<T1, T2>& p1, const std::pair<T1, T2>& p2) {
      return p1.first < p2.first;
    }
  };
  
  void TransferFunction::update_keys()
  {
    std::sort(values.begin(), values.end(), ComparePairFirst<double, Colorf>());
    keys.clear();
    for(size_t i = 0; i < values.size(); ++i) {
      keys[values[i].first] = i;
    }
  }
  
  void TransferFunction::adjust(double minValue, double maxValue)
  {
    if(values.size() < 2)
      return;
    double lowest = values.front().first;
    double highest = values.back().first;
    double ratio = (maxValue - minValue) / (highest - lowest);
    for(size_t i = 0; i < values.size(); ++i) {
      values[i].first = (values[i].first - lowest) * ratio + minValue;
    }
    update_keys();
  }
  
  TransferFunction::Colorf TransferFunction::interpolate(double position, double p1, Colorf col1, double p2,
                                                         Colorf col2) const
  {
    double delta = p2 - p1;
    double dp1 = (p2 - position) / delta;
    double dp2 = 1.0 - dp1;
    if(_interpolation == CYCLIC_HSV) {
      if(col2[0] - col1[0] > 180)
        col1[0] += 360;
      else if(col1[0] - col2[0] > 180)
        col2[0] += 360;
    }
    Colorf col(dp1 * col1 + dp2 * col2);
    if(_interpolation == CYCLIC_HSV) {
      if(col[0] < 0)
        col[0] += 360;
      else if(col[0] > 360)
        col[0] -= 360;
    }
    return col;
  }
  
  void TransferFunction::add_rgba_point(double pos, Colorf col)
  {
    if(_interpolation != RGB)
      col = convertRGBtoHSV(col);
    add_point(pos, col);
  }
  
  void TransferFunction::add_point(double pos, Colorf col)
  {
    key_map::iterator it_found = keys.find(pos);
    if(it_found != keys.end()) {
      values[it_found.value()] = std::make_pair(pos, col);
    } else {
      values.push_back(std::make_pair(pos, col));
      update_keys();
    }
  }
  
  void TransferFunction::add_hsva_point(double pos, Colorf col)
  {
    if(_interpolation == RGB)
      col = convertHSVtoRGB(col);
    add_point(pos, col);
  }
  
  void TransferFunction::remove_point(double pos)
  {
    key_map::iterator it_found = keys.find(pos);
    if(it_found != keys.end()) {
      values.erase(values.begin() + it_found.value());
      keys.erase(it_found);
    }
  }
  
  TransferFunction::Colorf TransferFunction::rgba(double pos) const
  {
    Colorf col = color(pos);
    if(_interpolation != RGB)
      return convertHSVtoRGB(col);
    return col;
  }
  
  TransferFunction::Colorf TransferFunction::color(double pos) const
  {
    Colorf prev_col;
    double prev_pos = -1;
    for(size_t i = 0; i < size(); ++i) {
      const double& p = values[i].first;
      const Colorf& col = values[i].second;
      if((p == 0 and fabs(pos) < epsilon)or (fabs(pos - p) / fabs(pos + p) < epsilon)) {
        return col;
      }
      if(pos < p) {
        if(prev_pos < 0) {
          if(clamp)
            return col;
          else
            return exteriorColor;
        }
        return interpolate(pos, prev_pos, prev_col, p, col);
      }
      prev_col = col;
      prev_pos = p;
    }
    if(clamp)
      return prev_col;
    else
      return exteriorColor;
  }
  
  double TransferFunction::alpha(double pos) const
  {
    Colorf prev_col;
    double prev_pos = -1;
    for(size_t i = 0; i < size(); ++i) {
      const double& p = values[i].first;
      const Colorf& col = values[i].second;
      if((p == 0 and fabs(pos) < epsilon)or (fabs(pos - p) / fabs(pos + p) < epsilon)) {
        return col.a();
      }
      if(pos < p) {
        if(prev_pos < 0) {
          if(clamp)
            return col.a();
          else
            return exteriorColor.a();
        }
        double delta = p - prev_pos;
        double dp1 = fabs((pos - prev_pos) / delta);
        double dp2 = fabs((pos - p) / delta);
        return dp1 * col.a() + dp2 * prev_col.a();
      }
      prev_col = col;
      prev_pos = p;
    }
    if(clamp)
      return prev_col.a();
    else
      return exteriorColor.a();
  }
  
  TransferFunction::Colorf TransferFunction::hsva(double pos) const
  {
    Colorf col = color(pos);
    if(_interpolation != RGB)
      return col;
    return convertRGBtoHSV(col);
  }
  
  void TransferFunction::setInterpolation(Interpolation i)
  {
    if(i != _interpolation) {
      if(i * _interpolation == 0)     // Change from HSV to RGB
      {
        if(i == RGB) {
          for(size_t i = 0; i < size(); ++i)
            values[i].second = convertHSVtoRGB(values[i].second);
        } else {
          for(size_t i = 0; i < size(); ++i)
            values[i].second = convertRGBtoHSV(values[i].second);
        }
      }
      _interpolation = i;
    }
  }
  
  void TransferFunction::reverse()
  {
    for(size_t i = 0; i < values.size(); ++i)
      values[i].first = 1 - values[i].first;
    update_keys();
  }
  
  void TransferFunction::clear()
  {
    values.clear();
    keys.clear();
  }
  
  void TransferFunction::move_point(double old_pos, double new_pos)
  {
    key_map::iterator it_found = keys.find(old_pos);
    if(it_found != keys.end()) {
      values[it_found.value()].first = new_pos;
      update_keys();
    }
  }
  
  double TransferFunction::next_pos(double old_pos) const
  {
    key_map::const_iterator it_found = keys.find(old_pos);
    if(it_found != keys.end()) {
      int p = it_found.value();
      if(p < int(size() - 1))
        return values[p + 1].first;
    }
    return -1;
  }
  
  double TransferFunction::prev_pos(double old_pos) const
  {
    key_map::const_iterator it_found = keys.find(old_pos);
    if(it_found != keys.end()) {
      int p = it_found.value();
      if(p > 0)
        return values[p - 1].first;
    }
    return -1;
  }
  
  TransferFunction TransferFunction::scale(Colorf min, Colorf max, Interpolation interpolation)
  {
    TransferFunction fct(interpolation);
    if(interpolation == RGB) {
      fct.add_rgba_point(0, min);
      fct.add_rgba_point(1, max);
    } else {
      fct.add_hsva_point(0, min);
      fct.add_hsva_point(1, max);
    }
    return fct;
  }
  
  TransferFunction TransferFunction::hueScale()
  {
    TransferFunction fct(CYCLIC_HSV);
    fct.add_hsva_point(0, Colorf(0, 1, 1, 0));
    fct.add_hsva_point(0.3, Colorf(0.3 * 360, 1, 1, 0.3));
    fct.add_hsva_point(0.7, Colorf(0.7 * 360, 1, 1, 0.7));
    fct.add_hsva_point(1, Colorf(0, 1, 1, 1));
    return fct;
  }
  
  TransferFunction TransferFunction::frenchFlag()
  {
    TransferFunction fct(RGB);
    fct.add_rgba_point(0, Colorf(0, 0, 1, 1));
    fct.add_rgba_point(0.5, Colorf(1, 1, 1, 0));
    fct.add_rgba_point(1, Colorf(1, 0, 0, 1));
    return fct;
  }

  TransferFunction fromColorbVec(const ColorbVec &colors)
  {
    TransferFunction fct(TransferFunction::RGB);
    uint divs = colors.size() - 1;
    for(uint i = 0 ; i <= divs ; i++)
    {
      fct.add_rgba_point(float(i) / float(divs),
                         Colorf(trim(float(colors[i][0]) / 255.0f, 0.0f, 1.0f),
                                trim(float(colors[i][1]) / 255.0f, 0.0f, 1.0f),
                                trim(float(colors[i][2]) / 255.0f, 0.0f, 1.0f),
                                trim(float(colors[i][3]) / 255.0f, 0.0f, 1.0f)));
    }
    return fct;
  }

  TransferFunction TransferFunction::jet()
  {
    // Special case: in transfer functions, jet is reversed
    // (goes from red to blue)
    const ColorbVec &colors = PresetColors::jet();
    ColorbVec revColors(colors.rbegin(),colors.rend());
    return fromColorbVec(revColors);
  }

  TransferFunction TransferFunction::blackbody()
  {
    return fromColorbVec(PresetColors::blackbody());
  }
  TransferFunction TransferFunction::uniformJet()
  {
    return fromColorbVec(PresetColors::uniformJet());
  }
  TransferFunction TransferFunction::helixWarm()
  {
    return fromColorbVec(PresetColors::helixWarm());
  }
  TransferFunction TransferFunction::helixCool()
  {
    return fromColorbVec(PresetColors::helixCool());
  }
  TransferFunction TransferFunction::helixFull()
  {
    return fromColorbVec(PresetColors::helixFull());
  }
  
  TransferFunction& TransferFunction::operator=(const TransferFunction& other)
  {
    clamp = other.clamp;
    _interpolation = other._interpolation;
    exteriorColor = other.exteriorColor;
    values = other.values;
    keys = other.keys;
    return *this;
  }
  
  double TransferFunction::operator[](int n) const {
    return values[n].first;
  }
  
  TransferFunction::Colorf TransferFunction::hsva_point(double pos) const
  {
    key_map::const_iterator it_found = keys.find(pos);
    if(it_found != keys.end()) {
      Colorf col = values[it_found.value()].second;
      if(_interpolation == RGB)
        col = convertRGBtoHSV(col);
      return col;
    }
    return Colorf(-1.0);
  }
  
  TransferFunction::Colorf TransferFunction::rgba_point(double pos) const
  {
    key_map::const_iterator it_found = keys.find(pos);
    if(it_found != keys.end()) {
      Colorf col = values[it_found.value()].second;
      if(_interpolation != RGB)
        col = convertHSVtoRGB(col);
      return col;
    }
    return Colorf(-1.0);
  }
  
  void TransferFunction::setPointList(const value_list& lst)
  {
    values = lst;
    update_keys();
  }
  
  bool TransferFunction::operator==(const TransferFunction& other) const {
    return values == other.values;
  }
  
  bool TransferFunction::operator!=(const TransferFunction& other) const {
    return values != other.values;
  }
}
