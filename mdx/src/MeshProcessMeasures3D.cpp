//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "MeshProcessMeasures3D.hpp"

#include <Triangulate.hpp> //calcNearestPointOnBezierGrid
#include <StackProcessMeshInteraction.hpp> //getTriangleListVolume

namespace mdx 
{
  bool MeasureVolume::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();
    updateVolumeGeometry(cs, indexAttr);
    for(CCIndex l : cs.volumes()) {
      auto &lIdx = indexAttr[l];
      heatAttr[lIdx.label] += lIdx.measure;
    }
    return true;
  }
  REGISTER_PROCESS(MeasureVolume);

  bool MeasureCellWallArea::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();
    updateVolumeGeometry(cs, indexAttr);
    for(CCIndex f : cs.faces()) {
      auto &lIdx = indexAttr[f];
      std::set<CCIndex> volumes = cs.incidentCells(f, 3);

      for(CCIndex v : volumes) {
        auto &vIdx = indexAttr[v];
        heatAttr[vIdx.label] += lIdx.measure;
      }

    }
    return true;
  }
  REGISTER_PROCESS(MeasureCellWallArea);

  bool measureSignal3D(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, 
                                     const CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr, QString mode, double modeParm)
  {
    heatAttr.clear();
    IntDoubleAttr labelArea;
    updateFaceGeometry(cs, indexAttr);
    #pragma omp parallel for
    for(uint i = 0; i < cs.volumes().size(); i++) {
      CCIndex l = cs.volumes()[i];  
      auto &lIdx = indexAttr[l];
      int label = mesh.getLabel(lIdx.label);
      if(label < 1) 
        continue;
      std::map<double, double> stressAreaMap;
      for(CCIndex f : cs.bounds(l)) {
        auto &fIdx = indexAttr[f];
        if(mode == "Average")
          labelArea[label] += fIdx.measure;
        heatAttr[label] += fIdx.measure * signalAttr[f];
        double stress = signalAttr[f];
        while(stressAreaMap.find(stress) != stressAreaMap.end()){
          stress += 1E-8;
        }
        stressAreaMap[stress] = fIdx.measure;
      }
      if(mode == "Percentile"){
        double wallArea = 0;
        heatAttr[label] = 0;
        for(auto p : stressAreaMap){
          wallArea += p.second;
        }
        double wallCount = 0;
        for(auto p : stressAreaMap){
          if(modeParm < wallCount / wallArea){
            continue;
          } else {
            wallCount += p.second;
            heatAttr[label] = p.first;
          }
        }
//std::cout << "perc " << label << "/" << wallArea << "/" << heatAttr[label] << "/" << modeParm << "/" << wallCount / wallArea << std::endl;
      }
    }
    if(mode == "Average")
      for(auto &pr : labelArea)
        heatAttr[pr.first] /= pr.second;




    return true;
  }

  bool measureSignalFace3D(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, 
                                     const CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr, bool average)
  {
    heatAttr.clear();
    IntDoubleAttr labelArea;
    updateFaceGeometry(cs, indexAttr);
    #pragma omp parallel for
    for(uint i = 0; i < cs.faces().size(); i++) {
      CCIndex l = cs.volumes()[i];  
      auto &lIdx = indexAttr[l];
      int label = mesh.getLabel(lIdx.label);
      if(label < 1) 
        continue;
      for(CCIndex f : cs.bounds(l)) {
        auto &fIdx = indexAttr[f];
        if(average)
          labelArea[label] += fIdx.measure;
        heatAttr[label] += fIdx.measure * signalAttr[f];
      }
    }
    if(average)
      for(auto &pr : labelArea)
        heatAttr[pr.first] /= pr.second;

    return true;
  }

  bool MeasureSignalAverage3D::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr)
  {
    return measureSignal3D(mesh, cs, indexAttr, signalAttr, heatAttr, "Average", 0);
  }
  REGISTER_PROCESS(MeasureSignalAverage3D);

  bool MeasureSignalTotal3D::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr)
  {
    return measureSignal3D(mesh, cs, indexAttr, signalAttr, heatAttr, "Total", 0);
  }
  REGISTER_PROCESS(MeasureSignalTotal3D);


  bool MeasureSignalPercentile3D::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr, double percentile)
  {
    return measureSignal3D(mesh, cs, indexAttr, signalAttr, heatAttr, "Percentile", percentile);
  }
  REGISTER_PROCESS(MeasureSignalPercentile3D);

  std::pair<int, int> getVolumeLabelsOfFace(CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex& face){
    std::pair<int, int> cellPair;

    std::set<CCIndex> cells = cs.incidentCells(face, 3);

    if(cells.size() == 1){
      CCIndex cell = (*cells.begin());
      int l1 = indexAttr[cell].label;

      cellPair = std::make_pair(0,l1);

    } else if(cells.size() == 2){
      CCIndex cell = *(cells.begin());
      int l1 = indexAttr[cell].label;
      CCIndex cell2 = *(++cells.begin());
      int l2 = indexAttr[cell2].label;

      cellPair = std::make_pair(l2,l1);
      if(l1<l2) cellPair = std::make_pair(l1,l2);

    }

    return cellPair;
  }

  bool MeasureDistanceCentroidBezier::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();
    updateVolumeGeometry(cs, indexAttr);

    auto &nearestP = mesh.heatAttr<Point3d>("Nearest Bezier Points");
    auto &nearestPD = mesh.heatAttr<Point3d>("Nearest Bezier Direction");

    CuttingSurface* cutSurf = cuttingSurface();

    Matrix4d rotMatrixS1, rotMatrixCS;
    const Stack *s1 = currentStack();
    s1->getFrame().getMatrix(rotMatrixS1.data());
    cutSurf->frame().getMatrix(rotMatrixCS.data());

    // fix the rotations
    Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);

    // discretization of Bezier
    int dataPointsBezier = 200;

    Bezier b = cutSurf->bezier();
    std::vector<std::vector<Point3d> > bezGrid;
    std::vector<Point3d> bezLine, dBezLine;
    if(stringToBool(parm("Bezier Line"))){
      b.discretizeLine(dataPointsBezier, 0.0, mGLTot, bezLine, dBezLine);
    } else {
      b.discretizeGrid(dataPointsBezier, mGLTot, bezGrid);
    }
    

    #pragma omp parallel for
    for(uint i = 0; i < cs.volumes().size(); i++) {
      CCIndex v = cs.volumes()[i];  
      int volLabel = indexAttr[v].label;
      CentroidData<Point3d> d = cellCentroidData(v, cs, indexAttr);

      Point3d pGrid(0,0,0);
      if(stringToBool(parm("Bezier Line"))){
        int idx;
        pGrid = calcNearestPointOnBezierLine(d.centroid, bezLine, idx);
        nearestP[volLabel] = bezLine[idx];
        nearestPD[volLabel] = dBezLine[idx];
      } else {
        Point2i idx;
        pGrid = calcNearestPointOnBezierGrid(d.centroid, bezGrid, idx);
        nearestP[volLabel] = bezGrid[idx[0]][idx[1]];
      }
      
      heatAttr[volLabel] = norm(pGrid - d.centroid);
    }

    return true;
  }
  REGISTER_PROCESS(MeasureDistanceCentroidBezier);


  bool MeasureAssociatedBezierLineX::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();
    updateVolumeGeometry(cs, indexAttr);

    auto &nearestP = mesh.heatAttr<Point3d>("Nearest Bezier Points");
    auto &nearestPD = mesh.heatAttr<Point3d>("Nearest Bezier Direction");

    CuttingSurface* cutSurf = cuttingSurface();

    Matrix4d rotMatrixS1, rotMatrixCS;
    const Stack *s1 = currentStack();
    s1->getFrame().getMatrix(rotMatrixS1.data());
    cutSurf->frame().getMatrix(rotMatrixCS.data());

    // fix the rotations
    Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);

    // discretization of Bezier
    int dataPointsBezier = 1000;

    Bezier b = cutSurf->bezier();
    std::vector<std::vector<Point3d> > bezGrid;
    std::vector<Point3d> bezLine, dBezLine;
    double totalLength;
    b.discretizeLineEqualWeight(dataPointsBezier, 0.0, mGLTot, bezLine, dBezLine, totalLength);
    

    #pragma omp parallel for
    for(uint i = 0; i < cs.volumes().size(); i++) {
      CCIndex v = cs.volumes()[i];  
      int volLabel = indexAttr[v].label;
      CentroidData<Point3d> d = cellCentroidData(v, cs, indexAttr);

      Point3d pGrid(0,0,0);
      int idx;
      pGrid = calcNearestPointOnBezierLine(d.centroid, bezLine, idx);
      nearestP[volLabel] = bezLine[idx];
      nearestPD[volLabel] = dBezLine[idx];
      
      heatAttr[volLabel] = (double)idx/(double)dataPointsBezier * totalLength;
    }

    return true;
  }
  REGISTER_PROCESS(MeasureAssociatedBezierLineX);


  bool MeasureDistanceCentroidMesh::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();
    updateVolumeGeometry(cs, indexAttr);

    auto &nearestP = mesh.heatAttr<Point3d>("Nearest Mesh Points");

    // TODO put this in hpp
    Matrix4d rotMatrixS1, rotMatrixCS;
    const Stack *s1 = currentStack();
    const Stack *s2 = otherStack();
    Mesh *m2 = otherMesh();
    s1->getFrame().getMatrix(rotMatrixS1.data());
    s2->getFrame().getMatrix(rotMatrixCS.data());

    QString ccName2 = m2->ccName();
    CCStructure &cs2 = m2->ccStructure(ccName2);

    CCIndexDataAttr &indexAttr2 = m2->indexAttr();

    // fix the rotations
    Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);

    // for each volume centroid: find nearest vertex on other mesh
    #pragma omp parallel for
    for(uint i = 0; i < cs.volumes().size(); i++) {
      CCIndex v = cs.volumes()[i]; 
      int volLabel = indexAttr[v].label;
      CentroidData<Point3d> d = cellCentroidData(v, cs, indexAttr);

      Point3d vCorrected = multMatrix4Point3(mGLTot,d.centroid);

      double minDis = HUGE_VAL;

      for(CCIndex c : cs2.vertices()){
        double dis = norm(indexAttr2[c].pos - vCorrected);
        if(dis < minDis){
          nearestP[volLabel] = indexAttr2[c].pos;
          minDis = dis;
        } 
      }

      heatAttr[volLabel] = minDis;
    }

    return true;
  }
  REGISTER_PROCESS(MeasureDistanceCentroidMesh);


  bool MeasureDistanceAttrMaps::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    // check Attr Map 1
    auto &heat1 = mesh.heatAttr<Point3d>(parm("Attr Map 1"));
    if(heat1.empty()) return setErrorMessage("Attr Map 1 doesn't exist.");

    // check Attr Map 2
    auto &heat2 = mesh.heatAttr<Point3d>(parm("Attr Map 2"));
    if(heat2.empty()) return setErrorMessage("Attr Map 2 doesn't exist.");

    heatAttr.clear();

    // calc distance
    for(auto p : heat1){
      if(heat2.find(p.first) == heat2.end()) continue;

      heatAttr[p.first] = norm(p.second - heat2[p.first]);
    }

    return true;
  }
  REGISTER_PROCESS(MeasureDistanceAttrMaps);


  bool MeasureCellWallSignal::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, QString &inputType, CCIndexDoubleAttr &signalAttrIn, CCIndexDoubleAttr &signalAttrOut)
  {
    signalAttrOut.clear();

    std::map<std::pair<int, int>, double> cellWallHeatMap;
    std::map<std::pair<int, int>, double> cellWallAreaMap;

    // find 3D cells foreach face, sum up
    for(CCIndex f : cs.faces()){
      std::set<CCIndex> cells = cs.incidentCells(f, 3);
      double faceArea = indexAttr[f].measure;

      if(inputType == "Total") faceArea = 1;

      std::pair<int, int> cellPair = getVolumeLabelsOfFace(cs, indexAttr, f);
      cellWallHeatMap[cellPair] += signalAttrIn[f] * faceArea;
      cellWallAreaMap[cellPair] += faceArea;
    }
    // set the signal heat
    for(CCIndex f : cs.faces()){
      std::set<CCIndex> cells = cs.incidentCells(f, 3);

      std::pair<int, int> cellPair = getVolumeLabelsOfFace(cs, indexAttr, f);
      double weight = cellWallAreaMap[cellPair];
      if(inputType == "Total") weight = 1;
      signalAttrOut[f] = cellWallHeatMap[cellPair] / weight;

    }    

    return true;
  }
  REGISTER_PROCESS(MeasureCellWallSignal);


  // Get triangle list for cuda operations
  // static void getTriangleList(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex &vol, HVec3F& pts, HVec3U& tris, HVec3F& nrmls)
  // {
  //   // Get the faces
  //   std::set<CCIndex> facesOfVol = cs.incidentCells(vol, 2);
  //   tris.clear();
  //   nrmls.clear();
  //   uint vCount = 0;
  //   AttrMap<CCIndex, uint> vMap;
  //   for(CCIndex f : facesOfVol) {
  //     auto &fIdx = indexAttr[f];
  //     CCIndexVec nbs = faceVertices(cs, f);
  //     if(nbs.size() != 3)
  //       throw(QString("Mesh has non-triangle face"));
  //     // Put in vertex map
  //     for(uint j = 0; j < 3; j++)
  //       if(vMap.count(nbs[j]) == 0)
  //         vMap[nbs[j]] = vCount++;
  //     // Make triangle
  //     tris.push_back(Point3u(vMap[nbs[0]], vMap[nbs[1]], vMap[nbs[2]]));
  //     // Use face normal
  //     nrmls.push_back(Point3f(fIdx.nrml));
  //   }
  //   if(tris.size() == 0)
  //     return;

  //   // Now fill in vertex list
  //   pts.resize(vCount);
  //   for(auto &pr : vMap)
  //     pts[pr.second] = Point3f(indexAttr[pr.first].pos);
  // }


  bool MeasureDistanceToNucleus::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttrIn, CCIndexDoubleAttr &signalAttrOut, 
    double thresholdDis, double thresholdValue)
  {
    // find voxels inside of cell
    Stack* stack = currentStack();//->currentStore()->stack();
    HVecUS stackData = currentStack()->currentStore()->data();

    auto &signalTotal = mesh.heatAttr<double>("Signal Internal Total");
    auto &distanceToNucleus = mesh.signalAttr<double>("Distance Nucleus");
    auto &distanceToCentroid = mesh.signalAttr<double>("Distance Centroid");

    auto &centroids = mesh.heatAttr<Point3d>("Centroid Position");
    auto &nuclei = mesh.heatAttr<Point3d>("Nucleus Position");

    signalTotal.clear();
    distanceToNucleus.clear();
    distanceToCentroid.clear();
    centroids.clear();
    nuclei.clear();

    bool ignoreZ = stringToBool(parm("Ignore Z"));
  
    // Data for cuda
    HVec3F pts;     // Points for triangles
    HVec3U tris;    // Triangle vertices
    HVec3F nrmls;   // Normals for triangles
                    //    Point3f bBoxW[2];  // Bounding box in world coordinates
  
    std::map<CCIndex, Point3d> volAvgMap, volAvgMapFinal;
    std::map<CCIndex, int> volCountMap, volCountMapFinal;

    // // Loop over all labels
    for(CCIndex vol : cs.volumes()){
      int label = indexAttr[vol].label;
      CentroidData<Point3d> d = cellCentroidData(vol, cs, indexAttr);
      Point3d centroid = d.centroid;
      centroids[label] = centroid;

      // Get triangle list and bounding box
      getTriangleListVolume(cs, indexAttr, label, pts, tris, nrmls);
      BoundingBox3f bBoxW;
      forall(const Point3f& p, pts)
        bBoxW |= p;

      BoundingBox3i bBoxI(stack->worldToImagei(Point3d(bBoxW[0])), stack->worldToImagei(Point3d(bBoxW[1])) + Point3i(1, 1, 1));
      Point3i imgSize(stack->size());
      bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
      Point3i size = bBoxI.size();

      // Buffer for mask
      HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);
    
      // call cuda to get stack mask
      if(insideMeshGPU(bBoxI[0], size, Point3f(stack->step()), Point3f(stack->origin()), pts, tris, nrmls, mask))
        return false;

      double minVal = 1E20, maxVal = -1E20;


      // find min, max and total of signal in cell
      for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
        for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
          size_t xIdx = stack->offset(bBoxI[0].x(), y, z);
          for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
            size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
              - bBoxI[0].x();
            if(mask[mIdx]){ // voxel in cell
              signalTotal[label] += stackData[xIdx];
              if(minVal > stackData[xIdx]) minVal = stackData[xIdx];
              if(maxVal < stackData[xIdx]) maxVal = stackData[xIdx];
            }
          }
        }

      
      

      // Filter out bottom 10% of signal values
      double threshold = (maxVal - minVal)*(thresholdValue) + minVal;
      

      for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
        for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
          size_t xIdx = stack->offset(bBoxI[0].x(), y, z);
          for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
            size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
              - bBoxI[0].x();
            if(mask[mIdx]){ // voxel in cell
              if(stackData[xIdx] < threshold) continue;

              Point3i vx (x, y, z);
              if(ignoreZ) vx.z() = 0;
              Point3d p = Point3d(stack->imageToWorld(vx));
              volAvgMap[vol] += p * stackData[xIdx];
              volCountMap[vol]+= stackData[xIdx];
            }
          }
        }

      volAvgMap[vol]/=volCountMap[vol];

      std::cout << "l " << label << "/" << volAvgMap[vol] << std::endl;
      if(thresholdDis > 0){

        for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z) {
          for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
            size_t xIdx = stack->offset(bBoxI[0].x(), y, z);
            for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
              size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
                - bBoxI[0].x();
              if(mask[mIdx]){ // voxel in cell
                if(stackData[xIdx] < threshold) continue;
                Point3i vx (x, y, z);
                double distanceCenter = norm(Point3d(stack->imageToWorld(vx)) - volAvgMap[vol]);
                if(distanceCenter > thresholdDis) continue;
                
                if(ignoreZ) vx.z() = 0;
                Point3d p = Point3d(stack->imageToWorld(vx));
                volAvgMapFinal[vol] += p * stackData[xIdx];
                volCountMapFinal[vol]+= stackData[xIdx];
              }
            }
          }
	}

	volAvgMapFinal[vol]/=volCountMapFinal[vol];

      } else {
        volAvgMapFinal[vol]=volAvgMap[vol];
      }

      nuclei[label] = volAvgMapFinal[vol];

      

      std::cout << "l " << label << "/" << volAvgMapFinal[vol] << std::endl;

      // now calc the distance of each face to the center
      std::set<CCIndex> facesOfVol = cs.incidentCells(vol, 2);

      for(CCIndex tri : facesOfVol){
        Point3d triCenter = indexAttr[tri].pos;
        if(ignoreZ) {
          triCenter.z() = 0;
          centroid.z() = 0;
        }
        distanceToNucleus[tri] = norm(triCenter - volAvgMapFinal[vol]);
        distanceToCentroid[tri] = norm(triCenter - centroid);
      }


    }
    
    return true;
  }
  REGISTER_PROCESS(MeasureDistanceToNucleus);

//  bool Measure3DVolume::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Volume");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DVolume);
//
//  bool Measure3DWallArea::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Cell Wall Area");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DWallArea);
//
//  bool Measure3DCoordLong::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Longitudinal Coord");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DCoordLong);
//
//  bool Measure3DCoordCirc::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Circumferential Coord");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DCoordCirc);
//
//  bool Measure3DCoordRad::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Radial Coord");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DCoordRad);
//
//  bool Measure3DSizeLong::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Longitudinal Cell Size");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DSizeLong);
//
//  bool Measure3DSizeCirc::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Circumferential Cell Size");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DSizeCirc);
//
//  bool Measure3DSizeRad::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Radial Cell Size");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DSizeRad);
//
//  bool Measure3DVolumeSurfaceRatio::run(Mesh* mesh, IntFloatAttr &data, double powerVol, double powerWall)
//  {
//    AttrMap<int, double>& attrDataVol = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Volume");
//    if(attrDataVol.size() == 0) return setErrorMessage("Error");
//
//    AttrMap<int, double>& attrDataWall = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Cell Wall Area");
//    if(attrDataWall.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrDataVol){
//      data[p.first] = std::pow(p.second,1./powerVol)/std::pow(attrDataWall[p.first], 1./powerWall);
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DVolumeSurfaceRatio);
//
//  bool Measure3DOutsideWallArea::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Outside Wall Area");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DOutsideWallArea);
//
//  bool Measure3DOutsideWallAreaPercent::run(Mesh* mesh, IntFloatAttr &data)
//  {
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Outside Wall Area");
//    if(attrData.size() == 0) return setErrorMessage("Error");
//
//    AttrMap<int, double>& attrDataCW = mesh->attributes().attrMap<int, double>("Measure Label Double /Cell Atlas/Cell Wall Area");
//    if(attrDataCW.size() == 0) return setErrorMessage("Error");
//
//    typedef std::pair<int,double> IntDouble;
//
//    forall(IntDouble p, attrData){
//      data[p.first] = p.second/attrDataCW[p.first];
//    }
//
//    mesh->labelHeat() = data;
//    mesh->setHeatMapBounds(mesh->calcHeatMapBounds());
//    updateState();
//    updateViewer();
//
//
//    return true;
//  }
//  REGISTER_PROCESS(Measure3DOutsideWallAreaPercent);
//

}
