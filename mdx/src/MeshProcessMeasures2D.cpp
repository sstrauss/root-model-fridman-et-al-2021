//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessMeasures2D.hpp>

#include <Progress.hpp>
#include <CCUtils.hpp>
//#include <PCAnalysis.hpp> // aspect ratio
#include <Triangulate.hpp> // calcNearestPointOnBezierGrid
#include <StackProcessShapeAnalysis.hpp>

namespace mdx 
{
  bool measureSignal(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, 
                                     const CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr, bool average)
  {
    updateFaceGeometry(cs, indexAttr); // RSS Should not be needed, done by the system
    heatAttr.clear();
    IntDoubleAttr labelArea;
    #pragma omp parallel for
    for(uint i = 0; i < cs.faces().size(); i++) {
      CCIndex f = cs.faces()[i];  
      auto &fIdx = indexAttr[f];
      if(fIdx.label < 1)
        continue;
      labelArea[fIdx.label] += fIdx.measure;
      heatAttr[fIdx.label] += fIdx.measure * signalAttr[f];
    }
    if(average)
      for(auto &pr : labelArea)
        heatAttr[pr.first] /= pr.second;

    return true;
  }

  bool MeasureSignalAverage::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr)
  {
    return measureSignal(mesh, cs, indexAttr, signalAttr, heatAttr, true);
  }
  REGISTER_PROCESS(MeasureSignalAverage);

  bool MeasureSignalTotal::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr)
  {
    return measureSignal(mesh, cs, indexAttr, signalAttr, heatAttr, false);
  }
  REGISTER_PROCESS(MeasureSignalTotal);

  bool MeasureArea::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();
    for(CCIndex f : cs.faces()) {
      auto &fIdx = indexAttr[f];
      int label = mesh.getLabel(fIdx.label);
      if(label <= 0) 
        continue;
      heatAttr[label] += fIdx.measure;
    }
    return true;
  }
  REGISTER_PROCESS(MeasureArea);


  bool MeasurePerimeter::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();

    std::map<IntIntPair, double> wallAreas;

    if(cs.maxDimension() == 2){
      neighborhood2D(mesh, cs, indexAttr, wallAreas);
    } 

    typedef std::pair<IntIntPair, double> IntIntDoubP;

    for(IntIntDoubP p : wallAreas){
      if(p.first.first > 0) 
        heatAttr[p.first.first] += p.second/2.;
      if(p.first.second > 0)
        heatAttr[p.first.second] += p.second/2.;
    }

    return true;
  }
  REGISTER_PROCESS(MeasurePerimeter);

  bool MeasureNeighbors::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();

    std::map<int, std::set<int> > neighorMap;

    for(CCIndex e : cs.edges()) {
      std::set<CCIndex> edgeFaces = cs.incidentCells(e, 2);
      if(edgeFaces.size() <= 1) continue;

      CCIndex f1 = *(edgeFaces.begin());
      CCIndex f2 = *(++edgeFaces.begin());

      // ignore edges inside of a cell
      if(mesh.getLabel(indexAttr[f1].label) == mesh.getLabel(indexAttr[f2].label)) continue;

      neighorMap[mesh.getLabel(indexAttr[f1].label)].insert(mesh.getLabel(indexAttr[f2].label));
      neighorMap[mesh.getLabel(indexAttr[f2].label)].insert(mesh.getLabel(indexAttr[f1].label));
    }

    typedef std::pair<int, std::set<int> > IntIntSetP;

    for(IntIntSetP p : neighorMap){
      heatAttr[p.first] = p.second.size();
    }

    return true;
  }
  REGISTER_PROCESS(MeasureNeighbors);

  // find consecutive border segments nm, ml; add up norm(nm ^ ml)
  bool MeasureBending::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();

    std::set<int> allLabels = faceLabels(cs, indexAttr);

    for(int label : allLabels){
      std::vector<CCIndex> bound = cellBoundary(cs, indexAttr, label);

      for(uint i = 1; i < bound.size()-1; i++){
        CCIndex l = bound[i];
        CCIndex m = bound[i-1];
        CCIndex n = bound[bound.size()-1];
        if(i > 1) n = bound[i-2];

        Point3d nm =  indexAttr[m].pos - indexAttr[n].pos;
        nm /= norm(nm);
        Point3d ml =  indexAttr[l].pos - indexAttr[m].pos;
        ml /= norm(ml);
        double s = norm(nm ^ ml);
        double c = (nm * ml);
        if(c < 0) s = 1 + (1-s);
        heatAttr[label] += s;
      }
    }
    return true;
  }
  REGISTER_PROCESS(MeasureBending);

//  bool MeasureAspectRatioPCA::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
//  {
//    heatAttr.clear();
//
//    PCAnalysis2D *pca2;
//    if(!getProcess("Mesh/Cell Axis/Shape Analysis/Compute PCA 2D", pca2)) 
//      throw(QString("Unable to get Process"));
//
//    IntDoubleAttr heatAttr2;
//    pca2->run(mesh, cs, indexAttr, pca2->parm("Heat Map"), pca2->parm("Axis Line Scale").toDouble(), 
//                                      pca2->parm("Axis Line Width").toDouble(), "Ratio", heatAttr2);
//
//    forall(IntFloatPair p, heatAttr2)
//      heatAttr[p.first] = p.second;
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAspectRatioPCA);

  bool MeasureMajorAxisPCA::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
//    heatAttr.clear();
//
//    PCAnalysis2D *pca2;
//    if(!getProcess("Mesh/Cell Axis/Shape Analysis/Compute PCA 2D", pca2)) 
//      throw(QString("Unable to get Process"));
//
//    QString outp = "Max";
//    IntFloatAttr heatAttr2;
//    bool runDisplay;
//
//    pca2->run(mesh, cs, indexAttr, pca2->parm("Heat Map"), pca2->parm("Axis Line Scale").toDouble(), 
//                                      pca2->parm("Axis Line Width").toDouble(), outp, heatAttr2, runDisplay);
//
//    forall(IntFloatPair p, heatAttr2)
//      heatAttr[p.first] = p.second;
//
    return true;
  }
//  REGISTER_PROCESS(MeasureMajorAxisPCA);

  bool MeasureMinorAxisPCA::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
//    heatAttr.clear();
//
//    PCAnalysis2D *pca2;
//    if(!getProcess("Mesh/Cell Axis/Shape Analysis/Compute PCA 2D", pca2)) 
//      throw(QString("Unable to get Process"));
//
//    QString outp = "Min";
//    IntFloatAttr heatAttr2;
//    bool runDisplay;
//
//    pca2->run(mesh, cs, indexAttr, pca2->parm("Heat Map"), pca2->parm("Axis Line Scale").toDouble(), 
//                                              pca2->parm("Axis Line Width").toDouble(), outp, heatAttr2, runDisplay);
//
//    forall(IntFloatPair p, heatAttr2)
//      heatAttr[p.first] = p.second;
//
    return true;
  }
//  REGISTER_PROCESS(MeasureMinorAxisPCA);

  bool MeasureCellDistance::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();

    std::map<IntIntPair, double> wallAreas;
    IntSet allLabels;
    IntSet selectedLabels;
    double cutOffDistance = 1000;
    double wallThreshold = 0.01;

    if(cs.maxDimension() == 2) {
      neighborhood2D(mesh, cs, indexAttr, wallAreas);
      allLabels = faceLabels(cs, indexAttr);
      selectedLabels = faceLabels(cs, indexAttr, true);
    } else if(cs.maxDimension() == 3){
      neighborhood3D(mesh, cs, indexAttr, wallAreas);
      allLabels = volumeLabels(cs, indexAttr);
      selectedLabels = volumeLabels(cs, indexAttr, true);
    }

    QString weight = parm("Wall Weights");
    bool equalWeights = weight == "1" ? true : false;

    if(weight == "Euclidean"){
      mdxInfo << "Doesn't work yet" << endl;
      //for(auto it = wallAreas.begin(); it!=wallAreas.end(); it++){
      //  it->second = 1./norm(labelPosMap[it->first.first] - labelPosMap[it->first.second]);
      //}
    }

    std::map<int, double> dijkValues = dijkstra(allLabels, wallAreas, selectedLabels, equalWeights, cutOffDistance, wallThreshold);

    typedef std::pair<int, double> IntDoubP;
    for(IntDoubP p : dijkValues)
      heatAttr[p.first] = p.second;

    return true;
  }
  REGISTER_PROCESS(MeasureCellDistance);

  bool MeasureJunctionDistance::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();

    std::map<IntIntPair, double> neighMap;
    std::set<int> allLabels;

    std::map<int, double> labelHeatValueMap;

    QString mode = parm("Mode");
    bool onlyDirect = stringToBool(parm("Only Direct Junctions"));

    if(cs.maxDimension() == 2) {
      neighborhood2D(mesh, cs, indexAttr, neighMap);
      allLabels = faceLabels(cs, indexAttr);
    } else if(cs.maxDimension() == 3)
      return false;
    

    forall(const int& l, allLabels)
      if(mode == "Min") labelHeatValueMap[l] = 1E20;
        else labelHeatValueMap[l] = -1E20;

    forall(auto p, neighMap)
      if(mode == "Min"){
        labelHeatValueMap[p.first.first] = min(labelHeatValueMap[p.first.first],p.second);
        labelHeatValueMap[p.first.second] = min(labelHeatValueMap[p.first.second],p.second);
      } else if(mode == "Max"){
        labelHeatValueMap[p.first.first] = max(labelHeatValueMap[p.first.first],p.second);
        labelHeatValueMap[p.first.second] = max(labelHeatValueMap[p.first.second],p.second);
      }

    if(!onlyDirect){
      std::map<int, std::set<int> > labelNeighborsMap;
      forall(auto p, neighMap){
        labelNeighborsMap[p.first.first].insert(p.first.second);
        labelNeighborsMap[p.first.second].insert(p.first.first);
      }

      forall(auto p, labelNeighborsMap){
        std::vector<std::pair<int, int> > cellNeighPairs;
        for(auto it1 = p.second.begin(); it1 != p.second.end(); it1++){
          for(auto it2 = it1; it2 != p.second.end(); it2++){
            if(it2 == it1) continue;
            std::pair<int, int> neighP = std::make_pair(*it1,*it2);
            cellNeighPairs.push_back(neighP);
          }
        }

        forall(auto nP, cellNeighPairs){
          if(neighMap.find(nP) == neighMap.end()) continue;
          if(neighMap[nP] < labelHeatValueMap[p.first] and mode == "Min") labelHeatValueMap[p.first] = neighMap[nP];
          if(neighMap[nP] > labelHeatValueMap[p.first] and mode == "Max") labelHeatValueMap[p.first] = neighMap[nP];
        }
      }

    }


    typedef std::pair<int, double> IntDoubP;
    for(IntDoubP p : labelHeatValueMap){
      heatAttr[p.first] = p.second;
    }

    return true;
  }
  REGISTER_PROCESS(MeasureJunctionDistance);


  bool MeasureFaceAssociatedBezierLineX::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
  {
    heatAttr.clear();
    updateVolumeGeometry(cs, indexAttr);

    auto &nearestP = mesh.heatAttr<Point3d>("Nearest Bezier Points");
    auto &nearestPD = mesh.heatAttr<Point3d>("Nearest Bezier Direction");

    CuttingSurface* cutSurf = cuttingSurface();

    Matrix4d rotMatrixS1, rotMatrixCS;
    const Stack *s1 = currentStack();
    s1->getFrame().getMatrix(rotMatrixS1.data());
    cutSurf->frame().getMatrix(rotMatrixCS.data());

    // fix the rotations
    Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);

    // discretization of Bezier
    int dataPointsBezier = 1000;

    Bezier b = cutSurf->bezier();
    std::vector<std::vector<Point3d> > bezGrid;
    std::vector<Point3d> bezLine, dBezLine;
    double totalLength;
    b.discretizeLineEqualWeight(dataPointsBezier, 0.0, mGLTot, bezLine, dBezLine, totalLength);
 

    #pragma omp parallel for
    for(uint i = 0; i < cs.faces().size(); i++) {
      CCIndex f = cs.faces()[i];  
      int faceLabel = indexAttr[f].label;

      Point3d pGrid(0,0,0);
      int idx;
      pGrid = calcNearestPointOnBezierLine(indexAttr[f].pos, bezLine, idx);
      nearestP[faceLabel] = bezLine[idx];
      nearestPD[faceLabel] = dBezLine[idx];
      
      heatAttr[faceLabel] = (double)idx/(double)dataPointsBezier * totalLength;
    }

    return true;
  }
  REGISTER_PROCESS(MeasureFaceAssociatedBezierLineX);




//  // calculate bending of cell together with its closest neighbor
//  void MeasureStomatalBending::calculateStomatalBending(vvGraph &T, IntFloatAttr &stomataBending){ 
//      stomataBending.clear();
//      //vvGraph &T = mesh->graph();
//      //AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Common Bending");
//      std::set<VertexPr> NeighbourMap;
//      std::map<VertexPr, Set> NhbrElem;
//      IntFloatAttr Bending;
//
//      MeasureCommonNhbrs cmnNhbr(*this);
//      cmnNhbr.calculateClosestNhbr(T, NeighbourMap); 
//     
//      MeasureVariabilityRadius vari(*this);
//      vari.findCommonNeighbors(T, NhbrElem);
//
//      MeasureBending bend(*this);
//      bend.calculateBending(T, Bending); 
//      
//      forall(VertexPr Pr, NeighbourMap) {  
//          double stomaBending = 0;
//          if(NhbrElem[Pr].size() <= 1)
//              continue;
//          forall(const vertex &n, T.neighbors(Pr.first)){
//              vertex m = T.nextTo(Pr.first, n);
//              vertex l = T.nextTo(Pr.first, m);
//              if(NhbrElem[Pr].find(m) != NhbrElem[Pr].end() and NhbrElem[Pr].find(l) != NhbrElem[Pr].end()){   
//                  Point3d nm =  m->pos - n->pos;
//                  nm /= norm(nm);
//                  Point3d ml =  l->pos - m->pos;
//                  ml /= norm(ml);
//                  float s = norm(nm ^ ml);
//                  float c = (nm * ml);
//                  if(c < 0)
//                    s = 1 + (1-s);
//                  stomaBending += s;
//              }
//          }  
//          forall(const vertex &n, T.neighbors(Pr.second)){
//              vertex m = T.nextTo(Pr.second, n);
//              vertex l = T.nextTo(Pr.second, m);
//              if(NhbrElem[Pr].find(m) != NhbrElem[Pr].end() and NhbrElem[Pr].find(l) != NhbrElem[Pr].end()){   
//                  Point3d nm =  m->pos - n->pos;
//                  nm /= norm(nm);
//                  Point3d ml =  l->pos - m->pos;
//                  ml /= norm(ml);
//                  float s = norm(nm ^ ml);
//                  float c = (nm * ml);
//                  if(c < 0)
//                    s = 1 + (1-s);
//                  stomaBending += s;
//              }
//          }
//          stomataBending[Pr.first->label] = Bending[Pr.first->label] + Bending[Pr.second->label] - stomaBending;
//          stomataBending[Pr.second->label] = stomataBending[Pr.first->label]; 
//      }
//  }
//
//  bool MeasureStomatalBending::run(Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      calculateStomatalBending(T, heatAttr);
//      mesh->labelHeat() = heatAttr;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//
//      AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Common Bending");
//      attrData.clear();
//
//      forall(IntFloatPair p, heatAttr)
//        attrData[p.first] = p.second;
//
//        return true;
//
//  }
//  REGISTER_PROCESS(MeasureStomatalBending);
//

//  
//  void measureRadius(vvGraph& S, QString option, IntFloatAttr &heatAttr)
//  {
//    heatAttr.clear();
//    forall(const vertex& v, S){
//      if(v->label == -1) continue;
//
//      double maxVal = -1E20;
//      double minVal = 1E20;
//      double avgVal = 0.;
//      double counter = 0.;
//      forall(const vertex &n, S.neighbors(v)){
//        vertex m = S.nextTo(v,n);
//        //std::cout << "vnm " << v->pos << "/" << n->pos << "/" << m->pos << "/" << std::endl;
//         if(m->label > -1) std::cout << "sth wrong " << m->label << std::endl;
//        //double dis = norm(v->pos-n->pos);
//        double dis = distLinePoint(n->pos, m->pos, v->pos, true); // min distance
//        double vn = norm(n->pos-v->pos);
//        double vm = norm(m->pos-v->pos);
//        //std::cout << "dis " << dis << "/" << std::endl;
//        if(dis < minVal) minVal = dis;
//        if(vn > maxVal) maxVal = vn;
//        if(vm > maxVal) maxVal = vm;
//
//        avgVal += (vn+vm)/2. * norm(n->pos-m->pos);
//        counter+= norm(n->pos-m->pos);
//      }
//      if(option == "max"){
//        heatAttr[v->label] = maxVal;
//      } else if(option == "min"){
//        heatAttr[v->label] = minVal;
//      } else {
//        heatAttr[v->label] = avgVal/counter;
//      }
//    }
//
//
//  }
//
//
//  bool MeasureMaxRadius::run(Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      measureRadius(T, "max", heatAttr);
//      //calculateNeighbors(T, heatAttr);
//
//      AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Maximum Radius");
//      attrData.clear();
//
//      forall(IntFloatPair p, heatAttr){
//        attrData[p.first] = p.second;
//      }
//
//      mesh->labelHeat() = heatAttr;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//      return true;
//
//  }
//  REGISTER_PROCESS(MeasureMaxRadius);
//
//
//  bool MeasureMinRadius::run(Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      measureRadius(T, "min", heatAttr);
//      //calculateNeighbors(T, heatAttr);
//
//      AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Minimum Radius");
//      attrData.clear();
//
//      forall(IntFloatPair p, heatAttr){
//        attrData[p.first] = p.second;
//      }
//
//      mesh->labelHeat() = heatAttr;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//      return true;
//
//  }
//  REGISTER_PROCESS(MeasureMinRadius);
//
//  bool MeasureAvgRadius::run(Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      measureRadius(T, "avg", heatAttr);
//      //calculateNeighbors(T, heatAttr);
//
//      AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Average Radius");
//      attrData.clear();
//
//      forall(IntFloatPair p, heatAttr){
//        attrData[p.first] = p.second;
//      }
//
//      mesh->labelHeat() = heatAttr;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//      return true;
//
//  }
//  REGISTER_PROCESS(MeasureAvgRadius);
//
//  void disToBezier(vvGraph& T, CuttingSurface* cutSurf, Matrix4d mGLTot, IntFloatAttr &heatAttr, std::map<int,Point3d> &dirs){
//
//    Bezier b = cutSurf->bezier();
//
//    int heatAttrPointsBezier = 200;
//    std::vector<std::vector<Point3d> > bezGrid;
//    b.discretizeGrid(heatAttrPointsBezier, mGLTot, bezGrid);
//
//    heatAttr.clear();
//    forall(const vertex& v, T){
//      if(v->label == -1) continue;
//      Point2i idx;
//      Point3d pGrid = calcNearestPointOnBezierGrid(v->pos, bezGrid, idx);
//      heatAttr[v->label] = norm(pGrid - v->pos);
//      dirs[v->label] = pGrid - v->pos;
//      dirs[v->label] /= norm(dirs[v->label]);
//    }
//
//  }
//
//
//  bool MeasureInclineAngle::run(const Stack *s1, Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    CuttingSurface* cutSurf = cuttingSurface();
//    Matrix4d rotMatrixS1, rotMatrixCS;
//    s1->getFrame().getMatrix(rotMatrixS1.heatAttr());
//    cutSurf->frame().getMatrix(rotMatrixCS.heatAttr());
//
//    Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);
//
//    std::map<int,Point3d> dirs;
//
//    disToBezier(T, cutSurf, mGLTot, heatAttr, dirs);
//
//    PCAnalysis2D pc(*this);
//    pc.run(mesh, "", 0.0, 0.0, "", heatAttr, false);
//
//    heatAttr.clear();
//    forall(const vertex& v, T){
//      if(v->label == -1) continue;
//      SymmetricTensor& tensor = mesh->cellAxis()[v->label];
//      Point3d tensorMajor = Point3d(tensor.ev1().x(), tensor.ev1().y(), tensor.ev1().z());
//      double cosAngle = (tensorMajor * dirs[v->label]) / norm(tensorMajor) / norm(dirs[v->label]);
//      heatAttr[v->label] = cosAngle < 0 ? -cosAngle : cosAngle;
//      heatAttr[v->label] = std::acos(heatAttr[v->label]);
//    }
//
//
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Incline Angle");
//    attrData.clear();
//
//    forall(IntFloatPair p, heatAttr){
//      attrData[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = heatAttr;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//    return true;
//
//  }
//  REGISTER_PROCESS(MeasureInclineAngle);
//
//  bool MeasureMajorAxisTheta::run(const Stack *s1, Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    Matrix4d rotMatrixS1;
//    s1->getFrame().getMatrix(rotMatrixS1.heatAttr());
//
//    PCAnalysis2D pc(*this);
//    pc.run(mesh, "", 0.0, 0.0, "", heatAttr, false);
//
//    heatAttr.clear();
//    forall(const vertex& v, T){
//      if(v->label == -1) continue;
//      SymmetricTensor& tensor = mesh->cellAxis()[v->label];
//      Point3d tensorMajor = Point3d(tensor.ev1().x(), tensor.ev1().y(), tensor.ev1().z());
//      Point3d axisX(1, 0, 0);
//      axisX = multMatrix4Point3(transpose(inverse(rotMatrixS1)),axisX);
//      double cosAngle = (axisX * tensorMajor) / norm(axisX) / norm(tensorMajor);
//      heatAttr[v->label] = cosAngle < 0 ? -cosAngle : cosAngle;
//      heatAttr[v->label] = std::acos(heatAttr[v->label]);
//    }
//
//
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Major Axis Theta");
//    attrData.clear();
//
//    forall(IntFloatPair p, heatAttr){
//      attrData[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = heatAttr;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//    return true;
//
//  }
//  REGISTER_PROCESS(MeasureMajorAxisTheta);
//
//  bool MeasureDistanceToBezier::run(const Stack *s1, Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    CuttingSurface* cutSurf = cuttingSurface();
//    Matrix4d rotMatrixS1, rotMatrixCS;
//    s1->getFrame().getMatrix(rotMatrixS1.heatAttr());
//    cutSurf->frame().getMatrix(rotMatrixCS.heatAttr());
//
//    Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);
//
//    std::map<int,Point3d> dirs;
//
//    disToBezier(T, cutSurf, mGLTot, heatAttr, dirs);
//
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Distance To Bezier");
//    attrData.clear();
//
//    forall(IntFloatPair p, heatAttr){
//      attrData[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = heatAttr;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//    return true;
//
//  }
//  REGISTER_PROCESS(MeasureDistanceToBezier);
//
//  bool MeasureDistanceToMesh::run(const Stack *s1, const Stack *s2, Mesh* mesh, Mesh* m2, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    //CuttingSurface* cutSurf = cuttingSurface();
//    Matrix4d rotMatrixS1, rotMatrixCS;
//    s1->getFrame().getMatrix(rotMatrixS1.heatAttr());
//    s2->getFrame().getMatrix(rotMatrixCS.heatAttr());
//
//    //Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);
//    Matrix4d mGLTot = transpose(inverse(rotMatrixCS)) * transpose(rotMatrixS1);
//
//    //std::map<int,Point3d> dirs;
//
//    //disToBezier(T, cutSurf, mGLTot, heatAttr, dirs);
//    forall(const vertex& v, T){
//      if(v->label < 1) continue;
//      Point3d vCorrected = multMatrix4Point3(mGLTot,v->pos);
//      Point3d p = nearestPointOnMesh(vCorrected, m2->graph());
//      heatAttr[v->label] = norm(p - vCorrected);
//    }
//
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Distance To Mesh");
//    attrData.clear();
//
//    forall(IntFloatPair p, heatAttr){
//      attrData[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = heatAttr;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//    return true;
//
//  }
//  REGISTER_PROCESS(MeasureDistanceToMesh);
//
//
//
//
//
//  // perimeter squared divided by area
//  void MeasureCurvature::calculateCurvature(vvGraph &T, IntFloatAttr &curvature){
//      IntFloatAttr area, perimeter;
//      //vvGraph &T = mesh->graph();
//      //AttrMap<int, double>& cv = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Curvature");
//      MeasureArea ar(*this);
//      ar.calculateArea(T, area);
//      MeasurePerimeter peri(*this);
//      peri.calculatePerimeter(T, perimeter);
//      forall(const vertex &v, T){
//        if(v->label == -1) continue;
//        curvature[v->label] = pow(perimeter[v->label], 2)/area[v->label];
//        //cv[v->label] = curvature[v->label];
//      }
//  }
//
//  bool MeasureCurvature::run(Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      calculateCurvature(T, heatAttr);
//
//      AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Curvature");
//      attrData.clear();
//
//      forall(IntFloatPair p, heatAttr){
//        attrData[p.first] = p.second;
//      }
//
//
//      mesh->labelHeat() = heatAttr;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//      return true;
//  }
//  REGISTER_PROCESS(MeasureCurvature);
//
//  void MeasureVariabilityRadius::findCommonNeighbors(vvGraph &T, std::map<VertexPr, Set> &NhbrElem){
//      NhbrElem.clear();
//      //vvGraph &T = mesh->graph();
//      std::map<int, std::set<vertex> > Nhbr;
//      MeasureNeighbors neighbor(*this);
//      neighbor.findNeighbourVertices(T, Nhbr);
//      forall(const vertex &v, T) {
//          if(v->label == -1)
//              continue;
//          forall(const vertex &n, Nhbr[v->label]){
//              if(n->label == -1)
//                  continue;
//              if(NhbrElem.find(std::make_pair(v, n)) != NhbrElem.end() or NhbrElem.find(std::make_pair(n, v)) != NhbrElem.end())
//                  continue;   
//              forall(const vertex &x, T.neighbors(v)){    //Iterate through the neighbors of v
//                  forall(const vertex &y, T.neighbors(n)){    //Iterate through the neighbors of n
//                    if(x == y and x != n and y != v){           
//                        NhbrElem[std::make_pair(v, n)].insert(x);
//                        NhbrElem[std::make_pair(v, n)].insert(y);
//                    }
//                  }
//              }
//              if(NhbrElem[std::make_pair(v, n)].size() > 1){
//                  NhbrElem[std::make_pair(v, n)] = NhbrElem[std::make_pair(v, n)];
//              }
//          } 
//      } 
//  }
//
//  void MeasureVariabilityRadius::calculateVariabilityRadius(vvGraph &T, IntFloatAttr &variabilityRadius){  
//      variabilityRadius.clear();
//      //vvGraph &T = mesh->graph();
//      //AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Variability Radius");
//      IntFloatAttr Nhbr;
//      MeasureNeighbors neighbor(*this);
//      neighbor.calculateNeighbors(T, Nhbr);
//      std::map<VertexPr, Set> NhbrElem;
//      std::map<VertexPr, Point3d> meanNhbr;
//      findCommonNeighbors(T, NhbrElem);
//      forall(VertexPrSet CNhbr, NhbrElem){   //Calculate center of the border of neighboring vertices
//          meanNhbr[CNhbr.first] = 0;   //cc.NhbrElem contains common neighbors (border vertices)
//          forall(const vertex &elem , CNhbr.second)  //Iterate through neighbors
//              meanNhbr[CNhbr.first] += elem->pos;  //Add positions
//          meanNhbr[CNhbr.first] /= CNhbr.second.size();  //Take average of positions
//          variabilityRadius[CNhbr.first.first->label] = HUGE_VAL;  
//          variabilityRadius[CNhbr.first.second->label] = HUGE_VAL;
//      }
//      forall(VertexPrSet CNhbr, NhbrElem){  //
//        double stdev = 0, countele = 0;  
//          forall(const vertex &elem, T.neighbors(CNhbr.first.first)){
//              if(CNhbr.second.find(elem) == CNhbr.second.end()){
//                  stdev += pow((norm(meanNhbr[CNhbr.first] - elem->pos)),2);
//                  countele++; 
//              }
//          }
//          forall(const vertex &elem, T.neighbors(CNhbr.first.second)){
//              if(CNhbr.second.find(elem) == CNhbr.second.end()){
//                stdev += pow((norm(meanNhbr[CNhbr.first] - elem->pos)),2);
//                countele++; 
//              }
//          }
//          stdev /= countele;
//          stdev = pow(stdev, 0.5); 
//          if(stdev < variabilityRadius[CNhbr.first.first->label])
//              variabilityRadius[CNhbr.first.first->label] = stdev;
//          if(stdev < variabilityRadius[CNhbr.first.second->label])
//              variabilityRadius[CNhbr.first.second->label] = stdev;
//
//          //  ar[CNhbr.first.second->label] = variabilityRadius[CNhbr.first.second->label];
//         //   ar[CNhbr.first.first->label] = variabilityRadius[CNhbr.first.first->label];
//      }
//  }
//
//  bool MeasureVariabilityRadius::run(Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    calculateVariabilityRadius(T, heatAttr);
//
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Variability Radius");
//    attrData.clear();
//
//    forall(IntFloatPair p, heatAttr){
//      attrData[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = heatAttr;
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//    return true;
//  }
//  REGISTER_PROCESS(MeasureVariabilityRadius);
//
//  void MeasureCommonNhbrs::calculateClosestNhbr(vvGraph &T, std::set<VertexPr> &NeighbourMap){
//      NeighbourMap.clear();
//      //vvGraph &T = mesh->graph();
//      //Compute closest neighbor map
//      std::set<vertex> computed;
//      std::map<vertex, vertex> NhbrPr;
//      
//      std::map<VertexPr, Set> NhbrElem;
//      std::map<int, std::set<vertex> > Nhbr;
//      MeasureNeighbors neighbor(*this);
//      neighbor.findNeighbourVertices(T, Nhbr);
//
//      IntFloatAttr variabilityRadius;
//      MeasureVariabilityRadius vari(*this);
//
//      vari.calculateVariabilityRadius(T, variabilityRadius); 
//      vari.findCommonNeighbors(T, NhbrElem);    
//      forall(const vertex &v, T){
//          forall(const vertex &n, Nhbr[v->label]){  
//              if(computed.find(v) == computed.end() and variabilityRadius[v->label] == variabilityRadius[n->label] and NhbrElem[std::make_pair(v, n)].size() > 1 ){
//                  computed.insert(v);
//                  computed.insert(n);
//                  NhbrPr[v] = n;
//                  NhbrPr[n] = v;
//              } 
//          }
//      }
//      forall(VertexPr Pr, NhbrPr)
//          NeighbourMap.insert(std::make_pair(Pr.first, Pr.second));
//  }
//
//  void MeasureCommonNhbrs::calculateCommonNhbrs(vvGraph &T, IntFloatAttr &commonNeighbors){
//      commonNeighbors.clear();
//      //vvGraph &T = mesh->graph();
//      //AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Common Neighbors");
//      std::map<int, std::set<vertex> > Nhbr;
//      IntFloatAttr Neighbors;
//      MeasureNeighbors neighbor(*this);
//      neighbor.findNeighbourVertices(T, Nhbr);
//      neighbor.calculateNeighbors(T, Neighbors);  
//
//      std::set<VertexPr> NeighbourMap;
//      calculateClosestNhbr(T, NeighbourMap);
//      forall(VertexPr Pr, NeighbourMap){
//          int RepeatedNeighbors = 0;
//          forall(const vertex &elem, Nhbr[Pr.first->label]){
//              if(Nhbr[Pr.second->label].find(elem) != Nhbr[Pr.second->label].end()){
//                  RepeatedNeighbors++; 
//              }
//          }
//          commonNeighbors[Pr.first->label] = Neighbors[Pr.first->label] + Neighbors[Pr.second->label] - RepeatedNeighbors - 2;
//          commonNeighbors[Pr.second->label] = Neighbors[Pr.first->label] + Neighbors[Pr.second->label] - RepeatedNeighbors- 2;
//      }
//  }
//
//  bool MeasureCommonNhbrs::run(Mesh* mesh, IntFloatAttr &heatAttr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      calculateCommonNhbrs(T, heatAttr);
//
//      AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Common Neighbors");
//      attrData.clear();
//
//      forall(IntFloatPair p, heatAttr){
//        attrData[p.first] = p.second;
//      }
//
//      mesh->labelHeat() = heatAttr;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//      return true;
//  }
//  REGISTER_PROCESS(MeasureCommonNhbrs);
//
//  void MeasureStomataArea::calculateStomataArea(vvGraph &T, IntFloatAttr &stomataArea){
//      stomataArea.clear();
//      //vvGraph &T = mesh->graph();
//      
//      IntFloatAttr area, perimeter;
//      MeasureArea ar(*this);
//      ar.calculateArea(T, area);
//      MeasurePerimeter peri(*this);
//      peri.calculatePerimeter(T, perimeter);
//      std::map<int, std::set<vertex> > Nhbr;
//      MeasureNeighbors neighbor(*this);
//      neighbor.findNeighbourVertices(T, Nhbr);
//      forall(const vertex &v, T){
//        int flag = 0, max = 0;
//        vertex x;
//        float totArea = 0, totPeri = 0;
//        if(stomataArea[v->label] != 0)
//            continue;
//        forall(const vertex &k, Nhbr[v->label]){        
//          if(abs(area[v->label] - area[k->label]) < 20){
//            flag = 1;
//            x = k;
//            break;
//          } 
//        }
//        if(flag == 1){
//          totArea = area[v->label] + area[x->label];
//          std::set <vertex> vSet;
//          forall(const vertex &m, T.neighbors(v)){ 
//            forall(const vertex &n, T.neighbors(x)){
//              if(n == m){
//                vSet.insert(n); 
//              }
//            }
//          }
//          forall(const vertex &p, vSet){
//            forall(const vertex &q, vSet){
//              if(norm(p->pos - q->pos) > max)
//                max = norm(p->pos - q->pos);
//            }
//          }
//          totPeri = pow(((perimeter[v->label]-max) + (perimeter[x->label] - max)), 2);
//          if(1 - (((totArea/totPeri) * 4 * 3.14)) < 0.25){
//            stomataArea[v->label] = totArea/totPeri;
//            stomataArea[x->label] = totArea/totPeri;
//            //st[v->label] = stomataArea[v->label];
//            //st[x->label] = stomataArea[x->label];
//          }
//        }
//      }
//  }
//
//  bool MeasureStomataArea::run(Mesh* mesh, IntFloatAttr &stomataArea)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    calculateStomataArea(T, stomataArea);
//
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Shape/Common Bending");
//    attrData.clear();
//
//    forall(IntFloatPair p, stomataArea){
//      attrData[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = stomataArea;
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureStomataArea);
//
//  bool MeasureAvgArea::calcAvgNhbdMeasure(vvGraph &T, IntFloatAttr &map, IntFloatAttr &avgMap){
//      avgMap.clear();
//      //vvGraph &T = mesh->graph();
//      std::map<int, std::set<vertex> > Nhbr;
//      //AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Area");
//      //ar.clear();
//      MeasureNeighbors neighbor(*this);
//      neighbor.findNeighbourVertices(T, Nhbr);
//      
//      forall(const vertex &v, T){
//        if(v->label != -1){
//          avgMap[v->label] = 0;
//          forall(const vertex &n, Nhbr[v->label])
//            avgMap[v->label] += map[n->label];
//          avgMap[v->label] /= Nhbr[v->label].size();
//          //ar[v->label] = avgMap[v->label];
//        }
//      }
//      return true;
//  }
//
//  bool MeasureAvgArea::run(Mesh* mesh, IntFloatAttr &avgArea)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    IntFloatAttr area;
//
//    MeasureArea are(*this);
//    are.calculateArea(T, area);
//    
//    calcAvgNhbdMeasure(T, area, avgArea);
//
//    AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Area");
//    attrData.clear();
//
//    forall(IntFloatPair p, avgArea){
//      attrData[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = avgArea;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAvgArea);
//
//  bool MeasureAvgPerimeter::run(Mesh* mesh, IntFloatAttr &avgPerimeter)
//  {
//
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      //vvGraph &T = mesh->graph();
//      AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Perimeter");
//      std::map<int, std::set<vertex> > Nhbr;
//      MeasureNeighbors neighbor(*this);
//      neighbor.findNeighbourVertices(T, Nhbr);
//      
//      IntFloatAttr perimeter;
//      MeasurePerimeter peri(*this);
//      peri.calculatePerimeter(T, perimeter);
//
//      MeasureAvgArea avg(*this);
//      avg.calcAvgNhbdMeasure(T, perimeter, avgPerimeter);
//      
//      forall(IntFloatPair p, avgPerimeter){
//        ar[p.first] = p.second;
//      }
//
//      mesh->labelHeat() = avgPerimeter;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//
//      return true;
//  }
//  REGISTER_PROCESS(MeasureAvgPerimeter);
//
//  bool MeasureAvgBending::run(Mesh* mesh, IntFloatAttr &avgBending)
//  {
//
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//      //vvGraph &T = mesh->graph();
//      AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Bending");
//      std::map<int, std::set<vertex> > Nhbr;
//      MeasureNeighbors neighbor(*this);
//      neighbor.findNeighbourVertices(T, Nhbr);
//      
//      IntFloatAttr bending;
//      MeasureBending bend(*this);
//      bend.calculateBending(T, bending);
//
//      MeasureAvgArea avg(*this);
//      avg.calcAvgNhbdMeasure(T, bending, avgBending);
//      
//      forall(IntFloatPair p, avgBending){
//        ar[p.first] = p.second;
//      }
//
//      mesh->labelHeat() = avgBending;
//
//      mesh->setShowLabel("Label Heat");
//      mesh->heatMapUnit() = "";
//      mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//      mesh->updateTriangles();
//
//      return true;
//  }
//  REGISTER_PROCESS(MeasureAvgBending);
//
//  bool MeasureAvgStomatalBending::run(Mesh* mesh, IntFloatAttr &avgStomataBending)
//  {
//
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    //vvGraph &T = mesh->graph();
//    AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Common Bending");
//    std::map<int, std::set<vertex> > Nhbr;
//    MeasureNeighbors neighbor(*this);
//    neighbor.findNeighbourVertices(T, Nhbr);
//    
//    IntFloatAttr stomataBending;
//    MeasureStomatalBending st(*this);
//    st.calculateStomatalBending(T, stomataBending);
//    
//    MeasureAvgArea avg(*this);
//    avg.calcAvgNhbdMeasure(T, stomataBending, avgStomataBending);
//    
//    forall(IntFloatPair p, avgStomataBending){
//      ar[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = avgStomataBending;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAvgStomatalBending);
//
//  bool MeasureAvgLenBr::run(Mesh* mesh, IntFloatAttr &avgLenBr)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    //vvGraph &T = mesh->graph();
//    AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Length Breadth Ratio");      
//    std::map<int, std::set<vertex> > Nhbr;
//    MeasureNeighbors neighbor(*this);
//    neighbor.findNeighbourVertices(T, Nhbr);
//    
//    IntFloatAttr lenBr;
//    MeasureAspectRatio lb(*this);
//    lb.calculateAspectRatio(T, lenBr);
//
//    MeasureAvgArea avg(*this);
//    avg.calcAvgNhbdMeasure(T, lenBr, avgLenBr);
//    
//    forall(IntFloatPair p, avgLenBr){
//      ar[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = avgLenBr;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAvgLenBr);
//
//  bool MeasureAvgNeighbors::run(Mesh* mesh, IntFloatAttr &avgNeighbors)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    //vvGraph &T = mesh->graph();
//    AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Neighbors");
//    std::map<int, std::set<vertex> > Nhbr;
//    MeasureNeighbors neighbor(*this);
//    neighbor.findNeighbourVertices(T, Nhbr);
//    
//    IntFloatAttr neighbors;
//    MeasureNeighbors nbr(*this);
//    nbr.calculateNeighbors(T, neighbors);
//    
//    MeasureAvgArea avg(*this);
//    avg.calcAvgNhbdMeasure(T, neighbors, avgNeighbors);
//    
//    forall(IntFloatPair p, avgNeighbors){
//      ar[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = avgNeighbors;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAvgNeighbors);
//
//  bool MeasureAvgCurvature::run(Mesh* mesh, IntFloatAttr &avgCurvature)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    //vvGraph &T = mesh->graph();
//    AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Curvature");
//    std::map<int, std::set<vertex> > Nhbr;
//    MeasureNeighbors neighbor(*this);
//    neighbor.findNeighbourVertices(T, Nhbr);
//    
//    IntFloatAttr curvature;
//    MeasureCurvature cr(*this);
//    cr.calculateCurvature(T, curvature);
//    
//    MeasureAvgArea avg(*this);
//    avg.calcAvgNhbdMeasure(T, curvature, avgCurvature);
//    
//    forall(IntFloatPair p, avgCurvature){
//      ar[p.first] = p.second;
//    }
//
//    mesh->labelHeat() = avgCurvature;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAvgCurvature);
//
//  bool MeasureAvgVariabilityRadius::run(Mesh* mesh, IntFloatAttr &avgVariabilityRadius)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    //vvGraph &T = mesh->graph();
//    AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Variability Radius");
//    std::map<int, std::set<vertex> > Nhbr;
//    MeasureNeighbors neighbor(*this);
//    neighbor.findNeighbourVertices(T, Nhbr);
//    
//    IntFloatAttr variabilityRadius;
//    MeasureVariabilityRadius vr(*this);
//    vr.calculateVariabilityRadius(T, variabilityRadius);
//    
//    MeasureAvgArea avg(*this);
//    avg.calcAvgNhbdMeasure(T, variabilityRadius, avgVariabilityRadius);
//    
//    forall(IntFloatPair p, avgVariabilityRadius){
//      ar[p.first] = p.second;
//    }
//    
//    mesh->labelHeat() = avgVariabilityRadius;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAvgVariabilityRadius);
//
//  bool MeasureAvgCommonNhbrs::run(Mesh* mesh, IntFloatAttr &avgCommonNeighbors)
//  {
//    if(mesh->meshType() != "MDXC" and mesh->meshType() != "MDXM")
//      return setErrorMessage("MDXM or MDXC Mesh required.");
//
//    vvGraph T;
//    vvGraph &S = mesh->graph();
//
//
//    if(mesh->tissue().meshType() == "MDXM")
//      mdxmToMgxc(S, T, 1);
//    else
//      T = S;
//
//    //vvGraph &T = mesh->graph();
//    AttrMap<int, double>& ar = mesh->attributes().attrMap<int, double>("Measure Label Double /Neighbourhood/Common Neighbors");
//    std::map<int, std::set<vertex> > Nhbr;
//    MeasureNeighbors neighbor(*this);
//    neighbor.findNeighbourVertices(T, Nhbr);
//    
//    IntFloatAttr commonNeighbors;
//    MeasureCommonNhbrs cmn(*this);
//    cmn.calculateCommonNhbrs(T, commonNeighbors);
//    
//    MeasureAvgArea avg(*this);
//    avg.calcAvgNhbdMeasure(T, commonNeighbors, avgCommonNeighbors);
//    
//    forall(IntFloatPair p, avgCommonNeighbors){
//      ar[p.first] = p.second;
//    }
//    
//    mesh->labelHeat() = avgCommonNeighbors;
//
//    mesh->setShowLabel("Label Heat");
//    mesh->heatMapUnit() = "";
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeasureAvgCommonNhbrs);
//
//
//  // create a 2D neighborhoodmap for a cell graph
//  void neighborhood2D(vvGraph& S, std::map<IntIntPair, double>& neighMap, QString weightType)
//  {
//
//    std::map<IntIntPair, double> neighMapNew;
//
//    typedef std::pair<vertex, vertex> VtxSeg;
//
//    std::map<int, std::set<VtxSeg> > cellBorderMap;
//
//    std::map<VtxSeg, std::set<int> > segCellMap;
//    typedef std::pair<VtxSeg, std::set<int> > VtxSegIntSetP;
//
//    std::map<int, Point3d> labelPosMap;
//
//    forall(const vertex& v, S){
//      if(v->label < 1) continue;
//      labelPosMap[v->label] = v->pos;
//      forall(const vertex& n, S.neighbors(v)){
//        forall(const vertex& m, S.neighbors(n)){
//          if(m->label > -1) continue;
//          bool stillSameCell = false;
//          forall(const vertex& k, S.neighbors(m)){
//            if(k->label == v->label) stillSameCell = true;
//          }
//          if(!stillSameCell) continue;
//
//          VtxSeg vs = std::make_pair(n,m);
//          if(m < n) vs = std::make_pair(m,n);
//          segCellMap[vs].insert(v->label);
//        }
//      }
//    }
//
//    forall(VtxSegIntSetP p, segCellMap){
//      if(p.second.size() > 2) std::cout << "sth weird" << std::endl;
//      else if(p.second.size() == 2){
//        int c1 = *(p.second.begin());
//        int c2 = *(++p.second.begin());
//        double length = norm(p.first.first->pos - p.first.second->pos);
//        neighMapNew[std::make_pair(c1,c2)] += length;
//        neighMapNew[std::make_pair(c2,c1)] += length;
//      }
//      
//    }
//
//    if(weightType == "Euclidean"){
//      for(std::map<IntIntPair, double >::iterator it = neighMapNew.begin(); it!=neighMapNew.end(); it++){
//        it->second = 1./norm(labelPosMap[it->first.first] - labelPosMap[it->first.second]);
//      }
//    }
//    neighMap = neighMapNew;
//
//  }
//

}
