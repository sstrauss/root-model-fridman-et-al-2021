//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef ASSERT_HPP
#define ASSERT_HPP

/**
 * \file Assert.hpp
 *
 * Graphical (or textual) assertion utility
 *
 * Define the macro \c vvassert(expr)
 */

#include <cassert>

class QString;

namespace mdx 
{
  void __assert_fail(const QString& assertion, const char* file, unsigned int line, const char* function);
  
  #ifdef NDEBUG
  #  define mdx_assert(expr) static_cast<void>(0)
  #  define mdx_assert_msg(expr, msg) static_cast<void>(0)
  #else
  /**
   * \def vvassert(expr)
   *
   * If NDEBUG is not defined and expr is false, warn the user of the program
   * of the failed assertion. If TEXT_VVASSERT is defined, vvassert falls back
   * onto the default assert function, otherwise, it shows a Qt message box
   * with the assert message, then abort the program.
   */
  #  define mdx_assert(expr) ((expr) ? static_cast<void>(0) : \
									 mdx::__assert_fail(#expr, __FILE__, __LINE__, __PRETTY_FUNCTION__))
  #  define mdx_assert_msg(expr, msg) ((expr) ? static_cast<void>(0) : \
									 mdx::__assert_fail(msg, __FILE__, __LINE__, __PRETTY_FUNCTION__))
  #endif
}

#endif
