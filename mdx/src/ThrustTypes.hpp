//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef THRUST_HOST_TYPES_HPP
#define THRUST_HOST_TYPES_HPP

#include <Geometry.hpp>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace mdx
{
  // Host vector types
  typedef thrust::host_vector<bool> HostVbool;

  typedef thrust::host_vector<ushort> HostVus;
  typedef thrust::host_vector<Point2i> HostV2i;

  typedef thrust::host_vector<uint> HostVu;
  typedef thrust::host_vector<Point1u> HostV1u;
  typedef thrust::host_vector<Point2u> HostV2u;
  typedef thrust::host_vector<Point3u> HostV3u;
  typedef thrust::host_vector<Point4u> HostV4u;
  typedef thrust::host_vector<Point6u> HostV6u;
  typedef thrust::host_vector<Point12u> HostV12u;
  typedef thrust::host_vector<Point30u> HostV30u;

  typedef thrust::host_vector<float> HostVf;
  typedef thrust::host_vector<Point3f> HostV3f;
  typedef thrust::host_vector<Point4f> HostV4f;

  typedef thrust::host_vector<double> HostVd;
  typedef thrust::host_vector<Point1d> HostV1d;
  typedef thrust::host_vector<Point2d> HostV2d;
  typedef thrust::host_vector<Point3d> HostV3d;
  typedef thrust::host_vector<Point4d> HostV4d;
  typedef thrust::host_vector<Point6d> HostV6d;
  typedef thrust::host_vector<Point9d> HostV9d;
  typedef thrust::host_vector<Matrix1d> HostVM1d;
  typedef thrust::host_vector<Matrix2d> HostVM2d;
  typedef thrust::host_vector<Matrix3d> HostVM3d;
  typedef thrust::host_vector<Matrix6x3d> HostVM6x3d;

  // device vector types
  typedef thrust::device_vector<bool> DeviceVbool;

  typedef thrust::device_vector<ushort> DeviceVus;
  typedef thrust::device_vector<Point2i> DeviceV2i;

  typedef thrust::device_vector<uint> DeviceVu;
  typedef thrust::device_vector<Point1u> DeviceV1u;
  typedef thrust::device_vector<Point2u> DeviceV2u;
  typedef thrust::device_vector<Point3u> DeviceV3u;
  typedef thrust::device_vector<Point4u> DeviceV4u;
  typedef thrust::device_vector<Point6u> DeviceV6u;
  typedef thrust::device_vector<Point12u> DeviceV12u;
  typedef thrust::device_vector<Point30u> DeviceV30u;

  typedef thrust::device_vector<float> DeviceVf;
  typedef thrust::device_vector<Point3f> DeviceV3f;
  typedef thrust::device_vector<Point4f> DeviceV4f;

  typedef thrust::device_vector<double> DeviceVd;
  typedef thrust::device_vector<Point1d> DeviceV1d;
  typedef thrust::device_vector<Point2d> DeviceV2d;
  typedef thrust::device_vector<Point3d> DeviceV3d;
  typedef thrust::device_vector<Point4d> DeviceV4d;
  typedef thrust::device_vector<Point6d> DeviceV6d;
  typedef thrust::device_vector<Point9d> DeviceV9d;
  typedef thrust::device_vector<Matrix1d> DeviceVM1d;
  typedef thrust::device_vector<Matrix2d> DeviceVM2d;
  typedef thrust::device_vector<Matrix3d> DeviceVM3d;
  typedef thrust::device_vector<Matrix6x3d> DeviceVM6x3d;
}
#endif
