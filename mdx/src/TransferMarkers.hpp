//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TRANSFER_MARKERS_HPP
#define TRANSFER_MARKERS_HPP

#include <Config.hpp>

#include <TransferFunction.hpp>

#include <QAbstractTableModel>
#include <QColor>
#include <QFont>
#include <QItemDelegate>
#include <QModelIndex>

class QStyleOptionViewItem;
class QModelIndex;
class QWidget;
class QAbstractItemModel;

class QItemSelection;
class QPushButton;

namespace mdx 
{
  class mdx_EXPORT MarkerColorDelegate : public QItemDelegate {
    Q_OBJECT
  public:
    MarkerColorDelegate(QObject* parent = 0);
  
    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    void setEditorData(QWidget* editor, const QModelIndex& index) const;
    void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
    void updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const;
  
  protected:
    QFont _font;
  };
  
  class mdx_EXPORT TransferMarkerModel : public QAbstractTableModel {
    Q_OBJECT
  public:
    typedef TransferFunction::Interpolation Interpolation;
    TransferMarkerModel(const std::vector<double>& markers, const std::vector<QColor>& colors, Interpolation m,
                        bool showRgba, QObject* parent = 0);
  
    int rowCount(const QModelIndex& parent = QModelIndex()) const;
    int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
      return 2;
    }
  
    Qt::ItemFlags flags(const QModelIndex& index) const;
  
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
  
    const std::vector<double>& getMarkers() const {
      return markers;
    }
    const std::vector<QColor>& getColors() const {
      return colors;
    }
  
  public slots:
    void addMarker(const QItemSelection& selection);
    void removeMarker(const QItemSelection& selection);
    void spreadMarkers(const QItemSelection& selection);
    void rgbaMode();
    void hsvaMode();
  
  protected:
    QString colorText(int idx) const;
    bool setColorText(int idx, QString txt);
  
    std::vector<double> markers;
    std::vector<QColor> colors;
    Interpolation mode;
    bool showRgba;
    QPushButton* spread_button;
    QFont _font;
  };
}
#endif // TRANSFER_MARKERS_HPP
