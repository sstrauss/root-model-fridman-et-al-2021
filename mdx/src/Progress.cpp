//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "Progress.hpp"
#ifndef WIN32
#  include <unistd.h>
#endif
#include <QMutexLocker>

#include <stdio.h>
#include <omp.h>
#include <iostream>
#include <sys/time.h>

#include <Information.hpp>
#include <QApplication>

#define SHOW_TIMER 2000

namespace mdx 
{
  Progress* Progress::_instance = 0;
   struct StartProgressEvent : public QEvent 

  {
    StartProgressEvent(const QString& _msg, int _steps, bool _allowCancel)
      : QEvent(QEvent::User), msg(_msg), steps(_steps), allowCancel(_allowCancel) {}
  
    QString msg;
    int steps;
    bool allowCancel;
  };
  
//  struct AdvanceProgressEvent : public QEvent 
//  {
//    AdvanceProgressEvent(int i) : QEvent(QEvent::User), step(i) {}
//  
//    int step;
//  };
//  
//  struct SetMaxProgressEvent : public QEvent 
//  {
//    SetMaxProgressEvent(int n) : QEvent(QEvent::User), steps(n) {}
//  
//    int steps;
//  };
//  
//  struct SetMsgProgressEvent : public QEvent 
//  {
//    SetMsgProgressEvent(QString _msg) : QEvent(QEvent::User), msg(_msg) {}
//  
//    QString msg;
//  };
//  
//  struct StopProgressEvent : public QEvent 
//  {
//    StopProgressEvent() : QEvent(QEvent::User) {}
//  };


  // Progress class
  Progress::Progress() : _parent(0), _canceled(false), _step(0), _steps(0), _msg("Ready.") {}
  
  Progress::~Progress() {} 

  Progress& Progress::instance()
  {
    if(_instance == 0)
      _instance = new Progress();
    return *_instance;
  }

  void Progress::setupProgress(QWidget* parent, QToolBar* progressToolBar)
  {
    _parent = parent;
    _progressText = new QLabel;
    _progressText->setLayoutDirection(Qt::RightToLeft);
    _progressText->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    _progressText->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
    _progressText->setText(_msg);
    _progressBar = new QProgressBar;
    _progressBar->setFixedWidth(100);
    _progressStop = new QPushButton;
    _progressStop->setIcon(QIcon(":images/Cancel.png"));
    QList<QAction *> actions = progressToolBar->actions();
    progressToolBar->insertWidget(actions[0], _progressText);
    progressToolBar->insertSeparator(actions[0]);
    progressToolBar->insertWidget(actions[0], _progressBar);
    connect(_progressStop, SIGNAL(clicked()), parent, SLOT(stopProgressBar()));
  }
  
  void Progress::cancel() 
  {
    _canceled = true;
  }

  QString Progress::makeMsg() const
  {
    QString ret;
    if(_steps > 0)
      ret = QString("%1 (%2%)").arg(_msg).arg(int(double(_step)/double(_steps) * 100.0 + .5));
    else
      ret = _msg;

    return ret;
  }
 
  void Progress::start(const QString& msg, int steps, bool allowCancel)
  {
    if(thread() != QThread::currentThread()) {
      emit progStart(msg, steps, allowCancel);
      //QThread::yieldCurrentThread();
      return;
    }
    _msg = msg;
    _canceled = false;
    _step = 0;
    _steps = steps;
    _progressBar->setValue(0); 
    _progressBar->setMinimum(0); 
    _progressBar->setMaximum(_steps); 
    _progressText->setText(makeMsg()); 
    //_progressText->setText(msg); 
    _progressBar->setTextVisible(false);
    if(allowCancel)
      _progressStop->setEnabled(true);
    else
      _progressStop->setEnabled(false);
    // Force an update
    QApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 1);
  }

  bool Progress::advance(int step, int steps)
  {
    if(thread() != QThread::currentThread()) { // Process thread
      // We are canceling
      if(canceled())
        return false;
      // Only report for first core with omp
      if(omp_get_thread_num() != 0)
        return true;
      // If progress bar is just spinning and we are in the process thread, nothing to do
      if(_steps == 0)
        return true;
      // If we are reporting % finished, check if step has advanced
      if(step < 0)
        step = 0;
      int newPos = int(double(step)/double(_steps) * 100 + 0.5);
      int oldPos = int(double(_step)/double(_steps) * 100 + 0.5);
      if(newPos == oldPos)
        return true;

      // Step has changed, post an event to the GUI thread
      emit progAdvance(step);
      //QThread::yieldCurrentThread();

      return true;
    }
		// GUI thread

    // We are canceling
    if(canceled())
      return false;

    if(_steps != 0) { // % completed progress bar
      if(step < 0)
        step = 0;
      int newPos = int(double(step)/double(_steps) * 100 + 0.5);
      int oldPos = int(double(_step)/double(_steps) * 100 + 0.5);
      if(newPos == oldPos) 
        return true;
      _step = step;
      _progressBar->setValue(_step);
      _progressText->setText(makeMsg());
    } else { // If spinning, only test every so many steps
      static int lastSteps = 0;
      if(lastSteps++ < steps) 
        return true;
      lastSteps = 0;

      // Only update at most 10 times per second
      static long lastAdvance = 0;
      struct timeval tv;
      gettimeofday(&tv, NULL);
      if(long(tv.tv_usec) - lastAdvance > 0 and long(tv.tv_usec) - lastAdvance < 100000)
        return true;
      lastAdvance = long(tv.tv_usec);
      // Force an update
    }
    QApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 1);

    return true;
  }
  
  bool Progress::canceled()
  {
    return _canceled;
  }
  
  void Progress::setMsg(const QString& msg)
  {
    if(thread() != QThread::currentThread()) {
      emit progSetMsg(msg);
      //QThread::yieldCurrentThread();
      return;
    }
    _msg = msg;
    _progressText->setText(makeMsg());
    _progressBar->setValue(0);
    QApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 1);
  }

  void Progress::setMax(int steps)
  {
    if(thread() != QThread::currentThread()) {
      emit progSetMax(steps);
      //QThread::yieldCurrentThread();
      return;
    }
    _steps = steps;
    _progressBar->setValue(0);
    QApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 1);
  }
 
  void Progress::stop()
  {
    if(thread() != QThread::currentThread()) {
      emit progStop();
      //QThread::yieldCurrentThread();
      return;
    }
    _progressBar->reset();
    _progressBar->setValue(0);
    _progressBar->setMaximum(1);
    _progressBar->setTextVisible(false);
    _progressStop->setEnabled(false);
    _msg = "Ready.";
    _progressText->setText(_msg);
    QApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 1);
  }

  // Progress functions that are called externally
  void progressStart(const QString &msg, int steps, bool allowCancel)
  {
    Progress::instance().start(msg, steps, allowCancel);
  }
  void progressStart(const char *msg, int steps, bool allowCancel)
  {
    Progress::instance().start(msg, steps, allowCancel);
  }
  void progressStart(const std::string &msg, int steps, bool allowCancel)
  {
    Progress::instance().start(msg, steps, allowCancel);
  } 
  void progressStop() 
  {
    Progress::instance().stop();
  }
  bool progressAdvance(int step, int steps) 
  {
    return Progress::instance().advance(step, steps);
  }
  bool progressAdvance(int step) 
  {
    return Progress::instance().advance(step);
  }
  bool progressAdvance() 
  {
    return Progress::instance().advance();
  }
  bool progressCanceled() 
  {
    return Progress::instance().canceled();
  }
  void progressCancel() 
  {
    Progress::instance().cancel();
  }
  void progressSetMsg(const QString &msg)
  {
    Progress::instance().setMsg(msg);
  }
  void progressSetMsg(const char *msg)
  {
    Progress::instance().setMsg(msg);
  }
  void progressSetMsg(const std::string &msg)
  {
    Progress::instance().setMsg(msg);
  } 
  void progressSetMax(int steps)
  {
    Progress::instance().setMax(steps);
  } 
}
