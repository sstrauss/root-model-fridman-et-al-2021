//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_MEASURES_3D_HPP
#define MESH_PROCESS_MEASURES_3D_HPP

#include <Process.hpp>
#include <Progress.hpp>
#include <Information.hpp>
#include <MeshUtils.hpp>

#include <limits>

namespace mdx
{
  /// Base class for 3D measures by label
  class Measure3D : public Process 
  {
  public:
    Measure3D(const Process& process) : Process(process)
    {
      setName("Label based 3D Measure");
      setDesc("3D heat map base process");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Heat Name", "Name of the output heat map", "");
    }

    bool initialize(QWidget *parent)
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::initialize No cell complex").arg(name());

      if(parm("Heat Name").isEmpty())
        throw QString("%1:initialize Heat map output name is empty").arg(name());

      return true;
    }
  
    bool run()
    {
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      if(cs.maxDimension() < 3)
        throw QString("%1::run This measure is for 3D cell complexes").arg(name());

      CCIndexDataAttr &indexAttr = mesh->indexAttr();
 
      QString heatName = parm("Heat Name");
      if(heatName.isEmpty())
        throw QString("%1::run Heat map output name is empty").arg(name());
      auto &heatAttr = mesh->heatAttr<double>(heatName);

      bool result = run(*mesh, cs, indexAttr, heatAttr);

      mesh->setHeatBounds(calcBounds(heatAttr), heatName);
      mesh->setHeatUnit(heatUnit, heatName);
      mesh->updateProperties();

      return result;
    }
    virtual bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr) = 0;

    bool finalize(QWidget *parent)
    {
      // Set display if called from GUI
      QString heatName = parm("Heat Name");
      if(parent and mesh and !ccName.isEmpty() and !heatName.isEmpty() and errorMessage().isEmpty()) {
        auto &cdp = mesh->drawParms(ccName);
        cdp.setGroupVisible("Faces", false);
        cdp.setGroupVisible("Volumes", true);
        cdp.setRenderChoice("Volumes", heatName);
        mesh->setHeat(heatName);
      }
      return true;
    }

  protected:
    Mesh *mesh = 0;
    QString ccName, heatUnit;
  };

  /// Volume by label
  class MeasureVolume : public Measure3D
  {
  public:
    MeasureVolume(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Geometry/Volume");
      setDesc("Generate heat map based on volume by label");
      setParmDefault("Heat Name", "Volume");
      heatUnit = UM3;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  /// Cell Wall Area by label
  class MeasureCellWallArea : public Measure3D
  {
  public:
    MeasureCellWallArea(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Geometry/Cell Wall Area");
      setDesc("Generate heat map based on cell wall area by label");
      setParmDefault("Heat Name", "Cell Wall Area");
      heatUnit = UM2;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  /// Signal average by label
  class MeasureSignalAverage3D : public Measure3D
  {
  public:
    MeasureSignalAverage3D(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Signal/Average");
      setDesc("Average signal per label per 3D volume");
      setParmDefault("Heat Name", "Average");
      addParm("Signal", "Name of the signal to quantify, empty for current", "");
    }

    bool initialize(QWidget *parent)
    {
      Measure3D::initialize(parent);
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::initialize No signal name").arg(name());
      setParm("Heat Name", signalName + " " + parm("Heat Name"));
      return true;
    }
  
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
    {
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());
      auto &signalAttr = mesh.signalAttr<double>(signalName);
      heatUnit = QString("%1/%2").arg(mesh.signalUnit(signalName)).arg(UM2);
      return run(mesh, cs, indexAttr, signalAttr, heatAttr);
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr);

  private:
    QString signalName;
  };

  /// Signal total by label
  class MeasureSignalTotal3D : public Measure3D
  {
  public:
    MeasureSignalTotal3D(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Signal/Total");
      setDesc("Total signal per label");
      setParmDefault("Heat Name", "Total");
      addParm("Signal", "Name of the signal to quantify, empty for current", "");
    }

    bool initialize(QWidget *parent)
    {
      Measure3D::initialize(parent);
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::initialize No signal name").arg(name());
      setParm("Heat Name", signalName + " " + parm("Heat Name"));
      return true;
    }
  
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
    {
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());
      auto &signalAttr = mesh.signalAttr<double>(signalName);
      heatUnit = mesh.signalUnit(signalName);
      return run(mesh, cs, indexAttr, signalAttr, heatAttr);
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr);

  private:
    QString signalName;
  };


  /// Signal total by label
  class MeasureSignalPercentile3D : public Measure3D
  {
  public:
    MeasureSignalPercentile3D(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Signal/Percentile");
      setDesc("Total signal per label");
      setParmDefault("Heat Name", "Percentile");
      addParm("Signal", "Name of the signal to quantify, empty for current", "");
      addParm("Percentile", "Percentile", "0.95");
    }

    bool initialize(QWidget *parent)
    {
      Measure3D::initialize(parent);
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::initialize No signal name").arg(name());
      setParm("Heat Name", signalName + " " + parm("Heat Name"));

      return true;
    }
  
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
    {
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());
      auto &signalAttr = mesh.signalAttr<double>(signalName);
      heatUnit = mesh.signalUnit(signalName);
      return run(mesh, cs, indexAttr, signalAttr, heatAttr, parm("Percentile").toDouble());
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr, double percentile);

  private:
    QString signalName;
  };


  /// Distance centroid to bezier
  class MeasureDistanceCentroidBezier : public Measure3D
  {
  public:
    MeasureDistanceCentroidBezier(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Location/Distance To Bezier");
      setDesc("Generate heat map based distance cell centroids to bezier");
      addParm("Bezier Line", "Set to Yes if Bezier was collapsed into a line", "No", booleanChoice());
      setParmDefault("Heat Name", "Distance To Bezier");
      heatUnit = UM;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  /// Associated bezier pos X
  class MeasureAssociatedBezierLineX : public Measure3D
  {
  public:
    MeasureAssociatedBezierLineX(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Location/Associated Bezier X");
      setDesc("Generate heat map based distance cell centroids to bezier");
      setParmDefault("Heat Name", "Associated Bezier X");
      heatUnit = UM;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  /// Distance centroid to mesh
  class MeasureDistanceCentroidMesh : public Measure3D
  {
  public:
    MeasureDistanceCentroidMesh(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Location/Distance To Mesh");
      setDesc("Generate heat map based distance cell centroids to mesh");
      setParmDefault("Heat Name", "Distance To Mesh");
      heatUnit = UM;
    }

    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  /// Distance centroid to mesh
  class MeasureDistanceAttrMaps : public Measure3D
  {
  public:
    MeasureDistanceAttrMaps(const Process &process) : Measure3D(process) 
    {
      setName("Mesh/Heat Map/Measures 3D/Test/Distance Attr Maps");
      setDesc("Generate heat map based on distance of the values of two specified Attr Maps");
      setParmDefault("Heat Name", "Distance To Mesh");
      addParm("Attr Map 1", "Name of the Attr Map 1", "");
      addParm("Attr Map 2", "Name of the Attr Map 2", "");

      heatUnit = UM;
    }

    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };


  class MeasureCellWallSignal : public Process 
  {
  public:
    MeasureCellWallSignal(const Process& process) : Process(process)
    {
      setName("Mesh/Heat Map/Measures 3D/Test/Heat Sum Cell Walls");
      setDesc("Computes signal average or total of shared walls between 3D cells and sets the signal heat.");
      addParm("Mode", "Mode", "Average", QStringList() << "Average" << "Total");
      addParm("Heat Name", "Name of the output heat map", "");
      addParm("Signal", "Name of the input signal map", "");
      setParmDefault("Heat Name", "Cell Wall Heat");
    }

    bool initialize(QWidget *parent)
    {
      Mesh* mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::initialize No cell complex").arg(name());

      if(parm("Heat Name").isEmpty())
        throw QString("%1:initialize Heat map output name is empty").arg(name());

      return true;
    }
  
    bool run()
    {
      Mesh* mesh = currentMesh();
      QString ccName = mesh->ccName();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      if(cs.maxDimension() < 3)
        throw QString("%1::run This measure is for 3D cell complexes").arg(name());

      CCIndexDataAttr &indexAttr = mesh->indexAttr();
 
      QString heatName = parm("Heat Name");
      if(heatName.isEmpty())
        throw QString("%1::run Heat map output name is empty").arg(name());
      //auto &heatAttr = mesh->heatAttr<double>(heatName);

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      QString outHeat = heatName;//signalName + "New";
      auto &signalAttrOut = mesh->signalAttr<double>(outHeat);
      QString inputType = parm("Mode");

      mesh->updateProperties();

      bool result = run(*mesh, cs, indexAttr, inputType, signalAttr, signalAttrOut);

      mesh->setSignalBounds(calcBounds(signalAttrOut), outHeat);
      auto &cdp = mesh->drawParms(ccName);
      cdp.setGroupVisible("Faces", true);
      cdp.setRenderChoice("Faces", outHeat);
      cdp.setGroupVisible("Volumes", false);
      mesh->setSignal(outHeat);

      return result;
    }

    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, QString &inputType, CCIndexDoubleAttr &signalAttrIn, CCIndexDoubleAttr &signalAttrOut);
  };


  class MeasureDistanceToNucleus : public Process 
  {
  public:
    MeasureDistanceToNucleus(const Process& process) : Process(process)
    {
      setName("Mesh/Heat Map/Measures 3D/Test/Heat distance Nucleus");
      setDesc("Computes signal average or total of shared walls between 3D cells and sets the signal heat.");
      addParm("Heat Name", "Name of the output heat map", "");
      addParm("Signal", "Name of the input signal map", "");
      addParm("Ignore Z", "Ignore Z", "No", booleanChoice());
      addParm("Nucleus size threshold", "Nucleus size threshold", "-1.0");
      addParm("Filter", "Filter threshold", "0.1");
      setParmDefault("Heat Name", "Cell Wall Heat");
    }

    bool initialize(QWidget *parent)
    {
      Mesh* mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::initialize No cell complex").arg(name());

      if(parm("Heat Name").isEmpty())
        throw QString("%1:initialize Heat map output name is empty").arg(name());

      return true;
    }
  
    bool run()
    {
      Mesh* mesh = currentMesh();
      QString ccName = mesh->ccName();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      if(cs.maxDimension() < 3)
        throw QString("%1::run This measure is for 3D cell complexes").arg(name());

      CCIndexDataAttr &indexAttr = mesh->indexAttr();
 
      QString heatName = parm("Heat Name");
      if(heatName.isEmpty())
        throw QString("%1::run Heat map output name is empty").arg(name());
      //auto &heatAttr = mesh->heatAttr<double>(heatName);

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      QString outHeat = heatName;//signalName + "New";
      auto &signalAttrOut = mesh->signalAttr<double>(outHeat);

      mesh->updateProperties();

      bool result = run(*mesh, cs, indexAttr, signalAttr, signalAttrOut, parm("Nucleus size threshold").toDouble(), parm("Filter").toDouble());

      mesh->setSignalBounds(calcBounds(signalAttrOut), outHeat);
      auto &cdp = mesh->drawParms(ccName);
      cdp.setGroupVisible("Faces", true);
      cdp.setRenderChoice("Faces", outHeat);
      cdp.setGroupVisible("Volumes", false);
      mesh->setSignal(outHeat);

      return result;
    }

    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttrIn, CCIndexDoubleAttr &signalAttrOut, double thresholdDis, double thresholdValue);
  };


//  class Measure3DVolume : public Process 
//  {
//  public:
//    Measure3DVolume(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Volume"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Volume"; }
//    QString description() const { return "Volume of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//  class Measure3DWallArea : public Process 
//  {
//  public:
//    Measure3DWallArea(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Cell Wall Area"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Cell Wall Area"; }
//    QString description() const { return "Cell Wall Area of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//  class Measure3DCoordLong : public Process 
//  {
//  public:
//    Measure3DCoordLong(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Longitudinal Coord"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Longitudinal Coord"; }
//    QString description() const { return "Longitudinal Coord of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//  class Measure3DCoordCirc : public Process 
//  {
//  public:
//    Measure3DCoordCirc(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Circumferential Coord"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Circumferential Coord"; }
//    QString description() const { return "Circumferential Coord of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//  class Measure3DCoordRad : public Process 
//  {
//  public:
//    Measure3DCoordRad(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Radial Coord"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Radial Coord"; }
//    QString description() const { return "Radial Coord of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//    class Measure3DSizeLong : public Process 
//  {
//  public:
//    Measure3DSizeLong(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Longitudinal Cell Size"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Longitudinal Cell Size"; }
//    QString description() const { return "Longitudinal Cell Size of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//    class Measure3DSizeCirc : public Process 
//  {
//  public:
//    Measure3DSizeCirc(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Circumferential Cell Size"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Circumferential Cell Size"; }
//    QString description() const { return "Circumferential Cell Size of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//    class Measure3DSizeRad : public Process 
//  {
//  public:
//    Measure3DSizeRad(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Radial Cell Size"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Radial Cell Size"; }
//    QString description() const { return "Radial Cell Size of a 3D cell. Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//    class Measure3DVolumeSurfaceRatio : public Process 
//  {
//  public:
//    Measure3DVolumeSurfaceRatio(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Radial Cell Size"] = parms;
//      return run(mesh, parms[0].toDouble(), parms[1].toDouble());
//    }
//  
//    bool run(Mesh* mesh, double powerVol, double powerWall){ 
//      return run(mesh, mesh->labelHeat(), powerVol, powerWall);
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap, double powerVol, double powerWall);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Volume Surface Ratio"; }
//    QString description() const { return "Volume Surface Ratio of a 3D cell (root3(volume)/root2(wall area)). Requires the Cell Atlas measure maps."; }
//    QStringList parmNames() const { return QStringList() << "Root Volume" << "Root Wall Area"; }
//    QStringList parmDescs() const { return QStringList() << "Root Volume" << "Root Wall Area"; }
//    QStringList parmDefaults() const { return QStringList() << "3.0" << "2.0"; }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//    class Measure3DOutsideWallArea : public Process 
//  {
//  public:
//    Measure3DOutsideWallArea(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Radial Cell Size"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Outside Wall Area"; }
//    QString description() const { return "Wall Area of a cell not shared with a neighbor cell"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
//
//  class Measure3DOutsideWallAreaPercent : public Process 
//  {
//  public:
//    Measure3DOutsideWallAreaPercent(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Cell Atlas/Radial Cell Size"] = parms;
//      return run(mesh);
//    }
//  
//    bool run(Mesh* mesh){ 
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures 3D/Cell Atlas/Outside Wall Area Percent"; }
//    QString description() const { return "Wall Area of a cell not shared with a neighbor cell"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//  };
}
#endif
