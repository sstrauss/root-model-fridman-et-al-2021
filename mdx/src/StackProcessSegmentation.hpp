//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_SEGMENTATION_HPP
#define STACK_PROCESS_SEGMENTATION_HPP

#include <Process.hpp>

namespace mdx 
{
  ///\addtogroup StackProcess
  ///@{
  /**
   * \class WatershedStack StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Compute the seeded watershed of the current stack using the CImg library.
   */
  class mdxBase_EXPORT WatershedStack : public Process 
  {
  public:
    WatershedStack(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Watershed3D");
      setDesc("3D Watershed on the current labeled stack.");
      setIcon(QIcon(":/images/SegmentMesh.png"));
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      Store* main = stack->main();
      Store* labels = stack->work();
      return run(stack, main, labels);
    }
    bool run(Stack* stack, Store* main, Store* labels);
  };
  
  /**
   * \class ConsolidateRegions  StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Try to merge segmented regions based on their contact area and the total voxel
   * intensity at their boundary.
   */
  class mdxBase_EXPORT ConsolidateRegions : public Process 
  {
  public:
    ConsolidateRegions(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Consolidate Regions");
      setDesc("Consilodate regions after watershed overseeding");
      setIcon(QIcon(":/images/Consolidate.png"));

      addParm("Threshold", "Threshold for consolidation", "5000");
      addParm("Min Voxels", "Minimum voxels to consider for consolidation", "100");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      Store* main = stack->main();
      Store* labels = stack->work();
      return run(main, labels, parm("Threshold").toUInt(), parm("MinVoxels").toUInt());
    }
    bool run(Store* data, Store* labels, uint threshold, uint minvoxels);
  };
  
  /**
   * \class ConsolidateRegionsNormalized  StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Variation on the ConsolidateRegions process.
   */
  class mdxBase_EXPORT ConsolidateRegionsNormalized : public Process 
  {
  public:
    ConsolidateRegionsNormalized(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Consolidate Regions Normalized");
      setDesc("Consolidate regions with normalization (slower)");
      setIcon(QIcon(":/images/Consolidate.png"));

      addParm("Tolerance", "Tolerance for consolidation", "0.3");
    }
    bool run()
    {
      Stack* stack = currentStack();
      Store* main = stack->main();
      Store* labels = stack->work();
      return run(main, labels, parm("Tolerance").toFloat());
    }
    bool run(Store* data, Store* labels, float tolerance);
  };
  
  /**
   * \class ThresholdLabelDelete  StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Delete labels that have too few or too many voxels.
   */
  class mdxBase_EXPORT ThresholdLabelDelete : public Process 
  {
  public:
    ThresholdLabelDelete(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Delete Labels by Threshold");
      setDesc("Delete Labels above/below voxel thresholds");
      setIcon(QIcon(":/images/DeleteLabel.png"));

      addParm("Min Voxels", "Minimum voxels of label", "1000");
      addParm("Max Voxels", "Minimum voxels of label", "0");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      Store* input = stack->currentStore();
      Store* output = stack->work();
      if(!input)
        return false;
      bool res = run(input, output, parm("Min Voxels").toInt(), parm("Max Voxels").toInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* labels, uint minVoxels, uint maxVoxels);
  };
  
  /**
   * \class LocalMaximaStack  StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Find the local maxima of the current image
   *
   * \note If the algorithm finds more than 65535 maxima, it will fail as it
   * cannot number them all.
   */
  class mdxBase_EXPORT LocalMaximaStack : public Process 
  {
  public:
    LocalMaximaStack(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Local Maxima");
      setDesc("Find local maxima and possibly number them");
      setIcon(QIcon(":/images/LocalMaxima.png"));

      addParm("X Radius", "Radius in X axis in microns", "1");
      addParm("Y Radius", "Radius in Y axis in microns", "1");
      addParm("Z Radius", "Radius in Z axis in microns", "1");
      addParm("Start Label", "Starting label number", "2");
      addParm("Min Color", "Minimum color", "10000");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      Store* input = stack->currentStore();
      Store* labels = stack->work();
      if(!input)
        return false;
      Point3f radius(parm("X Radius").toFloat(), parm("Y Radius").toFloat(), parm("Z Radius").toFloat());
      uint lab = parm("Start Label").toUInt();
      uint minColor = parm("Min Color").toUInt();
      bool res = run(input, labels, radius, lab, minColor);
      if(res) {
        input->hide();
        labels->show();
      }
      return res;
    }
    bool run(const Store* input, Store* labels, Point3f radius, uint startLabel, uint minColor);
  };
  
  /**
   * \class FillLabelStack  StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Replace one label by another one on the stack
   */
  class mdxBase_EXPORT FillLabelStack : public Process 
  {
  public:
    FillLabelStack(const Process& process) : Process(process)  
    { 
      setName("Stack/Segmentation/Fill Label");
      setDesc("Replace a label with another one");
      setIcon(QIcon(":/images/FillLabel.png"));

      addParm("Filled Label", "Label to fill", "1");
      addParm("New Label", "Value to fill it with", "0");
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      Store* input = stack->currentStore();
      Store* output = stack->work();
      if(!input)
        return false;
      bool res = run(input, output, parm("Filled Label").toUShort(), parm("New Label").toUShort());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, ushort filledLabel, ushort newLabel);
  };
  
  /**
   * \class EraseAtBorderStack StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Erase any label touching the border of the image.
   */
  class mdxBase_EXPORT EraseAtBorderStack : public Process 
  {
  public:
    EraseAtBorderStack(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Erase at Border");
      setDesc("Erase any labelled region touching the border of the image");
      setIcon(QIcon(":/images/EraseLabel.png"));
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      Store* input = stack->currentStore();
      Store* output = stack->work();
      if(!input)
        return false;
      bool res = run(input, output);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output);
  };

   /**
   * \class BlobDetect  StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Find and label blobs in an image.
   */
  /* *** Not currently implemented?
  class mdxBase_EXPORT BlobDetect : public Process 
  {
  public:
    BlobDetect(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Blob Detect");
      setDesc("Find and label blobs in an image");
      setIcon(QIcon(":/images/BlobDetect.png"));

      addParm("Use Watershed", "Use watershed", "No", booleanChoice());
      addParm("Start Label", "Starting label number", "1");
    }
  
    bool run()
    {
      bool wat = stringToBool(parm("Use Watershed"));
      Store* input = currentStack()->currentStore();
      Store* output = currentStack()->work();
      bool res = run(input, output, wat, parm("Start Label").toUInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, bool watershed, uint startlabel);
  };
  */
  
  /**
   * \class StackRelabel  StackProcessSegmentation.hpp <StackProcessSegmenation.hpp>
   *
   * Relabel a 3D stack to use consecutive labels. The list of cells is
   * randomized before relabeling to ensure a different labeling at each run.
   */
  class mdxBase_EXPORT StackRelabel : public Process 
  {
  public:
    StackRelabel(const Process& process) : Process(process) 
    {
      setName("Stack/Segmentation/Relabel");
      setDesc("Relabel a 3D stack to use consecutive labels.\n"
              "The cells are shuffled so each relabling will be different.");
      setIcon(QIcon(":/images/Relabel.png"));

      addParm("Start Label", "Starting label number", "1");
      addParm("Step", "Steps between labels", "1");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* store = s->currentStore();
      Store* output = s->work();
      int start = parm("Start Label").toInt();
      int step = parm("Step").toInt();
      if(run(s, store, output, start, step)) {
        store->hide();
        output->show();
        return true;
      }
      return false;
    }
    bool run(Stack* stack, const Store* store, Store* output, int start, int step);
  };
  
  /**
   * \class JoinRegionsSegment GlobalProcess.hpp <GlobalProcess.hpp>
   *
   * Join regions, using both the segmented stack and the extracted 3D cell mesh.
   *
   * Starting from a segmented stack, and the 3D cell mesh created from it, the
   * user can select a set of cells that needs to be merged. This process will
   * create a new cell corresponding to the union of all the selected cells and
   * rerun the Marching Cube process with the provided cube size.
   */
//  class mdxBase_EXPORT JoinRegionsSegment : public Process {
//  public:
//    JoinRegionsSegment(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      Store* work = currentStack()->work();
//      Mesh* mesh = currentMesh();
//  
//      bool ok;
//      float cubeSize = parms[0].toFloat(&ok);
//      if(not ok)
//        return setErrorMessage("Error, the parameter 'Cube Size' must be a number.");
//  
//      bool res = run(work, mesh, cubeSize);
//      if(res)
//        work->show();
//      return res;
//    }
//  
//    bool run(Store* work, Mesh* mesh, float cubeSize);
//  
//      setName("Stack/Segmentation/Join Regions");
//    QString description() const
//    {
//      return "Join Regions after 3D segmentation.\n"
//             "Cells selected in the 3D cell mesh extracted from the stack "
//             "will be merged and re-extracted.";
//    }
//    QStringList parmNames() const { return QStringList() << "Cube Size");
//    QStringList parmDescs() const { 
//      return QStringList() << "Cube Size for the Marching Cube process.");
//    QStringList parmDefaults() const { return QStringList() << "0");
//      setIcon(QIcon(":/images/JoinRegions.png"));
//  };

  ///@}
}

#endif
