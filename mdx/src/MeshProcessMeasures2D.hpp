//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_MEASURES_2D_HPP
#define MESH_PROCESS_MEASURES_2D_HPP

#include <Process.hpp>
#include <Progress.hpp>
#include <Information.hpp>
#include <MeshUtils.hpp>

#include <limits>

namespace mdx
{
  /// Base class for 2D measures by label
  class Measure2D : public Process 
  {
  public:
    Measure2D(const Process& process) : Process(process)
    {
      setName("Label based 2D Measure");
      setDesc("2D heat map base process");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Heat Name", "Name of the output heat map", "");
    }
    
    virtual bool initialize(QWidget *parent)
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::initialize No cell complex").arg(name());

      if(parm("Heat Name").isEmpty())
        throw QString("%1:initialize Heat map output name is empty").arg(name());

      return true;
    }
  
    virtual bool run()
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString heatName = parm("Heat Name");
      if(heatName.isEmpty())
        throw QString("%1:run Heat map output name is empty").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      if(cs.maxDimension() < 2)
        throw QString("%1::run This measure is for 2D or 3D cell complexes").arg(name());

      CCIndexDataAttr &indexAttr = mesh->indexAttr();
 
      auto &heatAttr = mesh->heatAttr<double>(heatName);
      bool result = run(*mesh, cs, indexAttr, heatAttr);

      mesh->setHeatBounds(calcBounds(heatAttr), heatName);
      mesh->setHeatUnit(heatUnit, heatName);
      mesh->updateProperties(ccName);

      return result;
    }
    virtual bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr) = 0;

    virtual bool finalize(QWidget *parent)
    {
      // Set display if called from GUI
      QString heatName = parm("Heat Name");
      if(parent and mesh and !heatName.isEmpty() and errorMessage().isEmpty()) {
        auto &cdp = mesh->drawParms(ccName);
        cdp.setGroupVisible("Faces", true);
        cdp.setRenderChoice("Faces", heatName);
        mesh->setHeat(heatName);
      }
      return true;
    }

  protected:
    Mesh *mesh = 0;
    QString ccName, heatUnit;
  };

  /// Area by label
  class MeasureArea : public Measure2D
  {
  public:
    MeasureArea(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Geometry/Area");
      setDesc("Generate heat map based on area by label");
      setParmDefault("Heat Name", "Area");
      heatUnit = UM2;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  /// Perimeter by label
  class MeasurePerimeter : public Measure2D 
  {
  public:
    MeasurePerimeter(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Geometry/Perimeter");
      setDesc("Generate heat map based on perimeter by label");
      setParmDefault("Heat Name", "Perimeter");
      heatUnit = UM;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  /// Signal average by label
  class MeasureSignalAverage : public Measure2D
  {
  public:
    MeasureSignalAverage(const Process &process) : Measure2D(process) 
    {
      setName("Mesh/Heat Map/Measures 2D/Signal/Average");
      setDesc("Average signal per label");
      setParmDefault("Heat Name", "Average");
      addParm("Signal", "Name of the signal to quantify, empty for current", "");
    }

    bool initialize(QWidget *parent)
    {
      Measure2D::initialize(parent);
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::initialize No signal name").arg(name());
      setParm("Heat Name", signalName + " " + parm("Heat Name"));
      return true;
    }
  
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
    {
      if(signalName.isEmpty())
        throw QString("%1:run No signal name").arg(name());
      auto &signalAttr = mesh.signalAttr<double>(signalName);
      heatUnit = QString("%1/%2").arg(mesh.signalUnit(signalName)).arg(UM2);
      return run(mesh, cs, indexAttr, signalAttr, heatAttr);
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr);

  private:
    QString signalName;
  };

  /// Signal total by label
  class MeasureSignalTotal : public Measure2D
  {
  public:
    MeasureSignalTotal(const Process &process) : Measure2D(process) 
    {
      setName("Mesh/Heat Map/Measures 2D/Signal/Total");
      setDesc("Total signal per label");
      setParmDefault("Heat Name", "Total");
      addParm("Signal", "Name of the signal to quantify, empty for current", "");
    }

    bool initialize(QWidget *parent)
    {
      Measure2D::initialize(parent);
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::initialize No signal name").arg(name());
      setParm("Heat Name", signalName + " " + parm("Heat Name"));
      return true;
    }

    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr)
    {
      if(signalName.isEmpty())
        throw QString("%1:run No signal name").arg(name());
      auto &signalAttr = mesh.signalAttr<double>(signalName);
      heatUnit = mesh.signalUnit(signalName);
      return run(mesh, cs, indexAttr, signalAttr, heatAttr);
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, IntDoubleAttr &heatAttr);

  private:
    QString signalName;
  };

  /// Number of neighbors by label
  class MeasureNeighbors : public Measure2D 
  {
  public:
    MeasureNeighbors(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Geometry/Neighbors");
      setDesc("Generate heat map based on the number of neighbors by label");
      setParmDefault("Heat Name", "Neighbors");
      heatUnit = "";
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };
  
  /// Bending energy by label
  class MeasureBending : public Measure2D 
  {
  public:
    MeasureBending(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Shape/Bending");
      setDesc("Generate heat map based on the bending by label (consecutive border segments nm, ml; add up norm(nm ^ ml))");
      setParmDefault("Heat Name", "Bending");
      heatUnit = "";
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  class MeasureAspectRatioPCA : public Measure2D 
  {
  public:
    MeasureAspectRatioPCA(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Geometry/PCA Aspect Ratio");
      setDesc("Generate heat map based on the aspect ratio by label (length of major axis / length of minor axis");
      setParmDefault("Heat Name", "Aspect Ratio");
      heatUnit = "";
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };


  class MeasureMajorAxisPCA : public Measure2D 
  {
  public:
    MeasureMajorAxisPCA(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Geometry/PCA Major Axis");
      setDesc("Generate heat map of the length of major axis");
      setParmDefault("Heat Name", "Length Major Axis");
      heatUnit = UM;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  class MeasureMinorAxisPCA : public Measure2D 
  {
  public:
    MeasureMinorAxisPCA(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Geometry/PCA Minor Axis");
      setDesc("Generate heat map of the length of minor axis");
      setParmDefault("Heat Name", "Length Minor Axis");
      heatUnit = UM;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  class MeasureCellDistance : public Measure2D 
  {
  public:
    MeasureCellDistance(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Location/Cell Distance");
      setDesc("Generate heat map based on the distace to selected cells");
      addParm("Wall Weights", "Wall Weights", "1", QStringList() << "1 / Wall Length" << "1" << "Euclidean");
      setParmDefault("Heat Name", "Cell Distance");
      heatUnit = UM;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };

  class MeasureJunctionDistance : public Measure2D 
  {
  public:
    MeasureJunctionDistance(const Process& process) : Measure2D(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Geometry/Junction Distance");
      setDesc("Calculates the distance between junctions of a cell.");
      addParm("Mode", "Mode", "Min", QStringList() << "Min" << "Max");
      addParm("Only Direct Junctions", "Consider Only Direct Cell Junctions. If No then also neighboring cell junctions will be considered.", "No", booleanChoice());

      // *** The "Ignore Border Cells" parameter isn't actually used in the process.
      addParm("Ignore Border Cells", "Heat values for border cells will not be accurate for this measure", "Yes", booleanChoice());
      
      setParmDefault("Heat Name", "Junction Distance");
      heatUnit = UM;
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };


  class MeasureFaceAssociatedBezierLineX : public Process 
  {
  public:
    MeasureFaceAssociatedBezierLineX(const Process& process) : Process(process)
    {
      setName("Mesh/Heat Map/Measures 2D/Location/Associated Bezier X");
      setDesc("Generate heat map based distance cell centroids to bezier");
      addParm("Heat Name", "Name of the output heat map", "");
      setParmDefault("Heat Name", "Cell Wall Heat");
    }

    bool initialize(QWidget *parent)
    {
      Mesh* mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::initialize No cell complex").arg(name());

      if(parm("Heat Name").isEmpty())
        throw QString("%1:initialize Heat map output name is empty").arg(name());

      return true;
    }
  
    bool run()
    {
      Mesh* mesh = currentMesh();
      QString ccName = mesh->ccName();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      if(cs.maxDimension() < 2)
        throw QString("%1::run This measure is for 2D cell complexes").arg(name());

      CCIndexDataAttr &indexAttr = mesh->indexAttr();
 
      QString heatName = parm("Heat Name");
      if(heatName.isEmpty())
        throw QString("%1::run Heat map output name is empty").arg(name());

      mesh->updateProperties();

      auto &heatAttr = mesh->heatAttr<double>(heatName);

      bool result = run(*mesh, cs, indexAttr, heatAttr);

      return result;
    }

    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, QString &inputType, CCIndexDoubleAttr &signalAttrIn, CCIndexDoubleAttr &signalAttrOut);
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, IntDoubleAttr &heatAttr);
  };



//  
//  class MeasureStomatalBending : public Process 
//  {
//  public:
//    MeasureStomatalBending(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    void calculateStomatalBending(vvGraph& T, IntFloatAttr& heatMap);
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/StomatalBending"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Shape/Common Bending"; }
//    QString description() const { return "Computes bending of a cell together with its closest neighbor on the mesh\n"
//    "requires a MDXC mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//


//
//  class MeasureMinRadius : public Process 
//  {
//  public:
//    MeasureMinRadius(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    //void findNeighbourVertices(vvGraph& S, std::map<int, std::set<vertex> > &Nhbr);
//    //void calculateNeighbors(vvGraph& S, IntFloatAttr& heatMap);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/Neighbors"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Geometry/Minimum Radius"; }
//    QString description() const { return "Computes the minimum distance from the cell center to the border"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureMaxRadius : public Process 
//  {
//  public:
//    MeasureMaxRadius(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/Neighbors"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Geometry/Maximum Radius"; }
//    QString description() const { return "Computes the maximum distance from the cell center to the border"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureAvgRadius : public Process 
//  {
//  public:
//    MeasureAvgRadius(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/Neighbors"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Geometry/Average Radius"; }
//    QString description() const { return "Computes the average distance from the cell center to the border"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureInclineAngle : public Process 
//  {
//  public:
//    MeasureInclineAngle(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      Stack *s1 = currentStack();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/Neighbors"] = parms;
//      return run(s1, mesh, mesh->labelHeat());
//    }
//  
//    bool run(const Stack *s1, Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Geometry/Incline Angle"; }
//    QString description() const { return "Computes the angle between the major axis of a cell and the nearest point on the Bezier grid"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureDistanceToMesh : public Process 
//  {
//  public:
//    MeasureDistanceToMesh(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *m = currentMesh();
//      Mesh *m2 = mesh(1);
//      if(m == m2)
//        m2 = mesh(0);
//      Stack *s1 = currentStack();
//      Stack *s2 = stack(1);
//      if(s1 == s2)
//        s2 = stack(0);
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/Neighbors"] = parms;
//      return run(s1, s2, m, m2, m->labelHeat());
//    }
//  
//    bool run(const Stack *s1, const Stack *s2, Mesh* mesh, Mesh* m2, IntFloatAttr &data);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Location/Distance To Mesh"; }
//    QString description() const { return "Computes the closest distance to the inactive mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureMajorAxisTheta : public Process 
//  {
//  public:
//    MeasureMajorAxisTheta(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      Stack *s1 = currentStack();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/Neighbors"] = parms;
//      return run(s1, mesh, mesh->labelHeat());
//    }
//  
//    bool run(const Stack *s1, Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Geometry/Major Axis Theta"; }
//    QString description() const { return "Computes the angle between the major axis of a cell and the x-axis"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureDistanceToBezier : public Process 
//  {
//  public:
//    MeasureDistanceToBezier(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    //void findNeighbourVertices(vvGraph& S, std::map<int, std::set<vertex> > &Nhbr);
//    //void calculateNeighbors(vvGraph& S, IntFloatAttr& heatMap);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      Stack *s1 = currentStack();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Geometry/Neighbors"] = parms;
//      return run(s1, mesh, mesh->labelHeat());
//    }
//  
//    bool run(const Stack *s1, Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Location/Distance To Bezier"; }
//    QString description() const { return "Computes for each cell the distance to a defined Bezier grid or line"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//
//  class MeasureCurvature : public Process 
//  {
//  public:
//    MeasureCurvature(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    void calculateCurvature(vvGraph& S, IntFloatAttr& heatMap);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Shape/Curvature"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Shape/Curvature"; }
//    QString description() const { return "Computes curvature of cells on the mesh (Perimeter squared divided by Area)\n"
//    "requires a MDXC mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//  
//  class MeasureVariabilityRadius : public Process 
//  {
//  public:
//    MeasureVariabilityRadius(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    void findCommonNeighbors(vvGraph& S, std::map<VertexPr, Set> &NhbrElem);
//    void calculateVariabilityRadius(vvGraph& S, IntFloatAttr& heatMap);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Shape/VariabilityRadius"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Shape/Variability Radius"; }
//    QString description() const { return "Computes variability of radius of 2 similar cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureCommonNhbrs : public Process 
//  {
//  public:
//    MeasureCommonNhbrs(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    void calculateCommonNhbrs(vvGraph& S, IntFloatAttr& heatMap);
//    void calculateClosestNhbr(vvGraph& S, std::set<VertexPr> &NeighbourMap);
//
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Shape/LengthBreadthRatio"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Shape/Common Neighbors"; }
//    QString description() const { return "Computes Common neighbors of 2 similar cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//
//  class MeasureStomataArea : public Process 
//  {
//  public:
//    MeasureStomataArea(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    void calculateStomataArea(vvGraph& S, IntFloatAttr &heatMap);
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Shape/StomatalArea"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr &heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Shape/Common Neighbors"; }
//    QString description() const { return "Computes Common neighbors of 2 similar cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureAvgArea : public Process 
//  {
//  public:
//    MeasureAvgArea(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    bool calcAvgNhbdMeasure(vvGraph& S, IntFloatAttr &map, IntFloatAttr &avgMap);
//
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AverageArea"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr &heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Area"; }
//    QString description() const { return "Computes average area of neighborhood cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureAvgPerimeter : public Process 
//  {
//  public:
//    MeasureAvgPerimeter(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AveragePerimeter"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr &heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Perimeter"; }
//    QString description() const { return "Computes average perimeter of neighborhood cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureAvgBending : public Process 
//  {
//  public:
//    MeasureAvgBending(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AverageBending"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Bending"; }
//    QString description() const { return "Computes average bending of neighborhood cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//  
//  class MeasureAvgStomatalBending : public Process 
//  {
//  public:
//    MeasureAvgStomatalBending(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/StomatalBending"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Common Bending"; }
//    QString description() const { return "Computes bending of a cell together with its closest neighbor on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureAvgLenBr : public Process 
//  {
//  public:
//    MeasureAvgLenBr(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    bool run(const QStringList &parms, IntFloatAttr& heatMap);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AverageLengthBreadthRatio"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Length Breadth Ratio"; }
//    QString description() const { return "Computes LenBr of cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//  
//  class MeasureAvgNeighbors : public Process 
//  {
//  public:
//    MeasureAvgNeighbors(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AverageNeighbors"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Neighbors"; }
//    QString description() const { return "Computes neighbors of cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureAvgCurvature : public Process 
//  {
//  public:
//    MeasureAvgCurvature(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    void calculateCurvature(vvGraph& S, IntFloatAttr& heatMap);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AverageCurvature"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Curvature"; }
//    QString description() const { return "Computes curvature of cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//  
//  class MeasureAvgVariabilityRadius : public Process 
//  {
//  public:
//    MeasureAvgVariabilityRadius(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//    
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AverageVariabilityRadius"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Variability Radius"; }
//    QString description() const { return "Computes variability of radius of 2 similar cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
//  class MeasureAvgCommonNhbrs : public Process 
//  {
//  public:
//    MeasureAvgCommonNhbrs(const Process& process) : Process(process) {}
//    bool processParms(const QStringList &parms);
//
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *mesh = currentMesh();
//      //AttrMap<QString, QStringList> mp = mesh->attributes().attrMap<QString, QStringList>("Measure Parms");
//      //mp["/Neighbourhood/AverageCommonNeighbors"] = parms;
//      return run(mesh, mesh->labelHeat());
//    }
//  
//    bool run(Mesh* mesh, IntFloatAttr& heatMap);
//
//    QString name() const { return "Mesh/Heat Map/Measures/Neighbourhood/Common Neighbors"; }
//    QString description() const { return "Computes Common neighbors of 2 similar cells on the mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QStringList parmDefaults() const { return QStringList(); }
//    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
//    bool isMeasureProcess() const { return true; }
//  };
//
}

#endif
