//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CSV_DATA_WINDOW_HPP
#define CSV_DATA_WINDOW_HPP


#include <QtGui>
#include <QMainWindow>
#include <QAbstractTableModel>
#include <QMenu>

#include <CSVData.hpp>

namespace Ui 
{
  class csvDataWindow;
}

namespace mdx
{
  class CSVDataModel : public QAbstractTableModel
  {
    Q_OBJECT

  public:
    CSVDataModel(QObject *parent = 0);

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    bool setCSVData(CSVData &data);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

    CSVData csvData;
    CSVData *csvDataOrig;
    bool changed = false;
  };

  class CSVDataWindow : public QMainWindow
  {
    Q_OBJECT

  public:
    explicit CSVDataWindow(QWidget *parent);
    ~CSVDataWindow();

    void closeEvent(QCloseEvent *event);

    CSVDataModel csvDataModel;

    Ui::csvDataWindow *ui = 0;

  public slots:
    void menuRequested(QPoint pos);
    void menuTriggered(QAction *action);
    void buttonClicked(QAbstractButton *button);
  signals:
    void selectLabels(IntVec labels);
  };
}
#endif
