#ifndef THRUST_HPP
#define THRUST_HPP

#include "Config.hpp"

#include <thrust/version.h>

#if defined(USE_TBB)
  #define USING_TBB
  #include <thrust/system/tbb/vector.h>
  #include <thrust/iterator/retag.h>
  #define THRUST_RETAG(x) thrust::reinterpret_tag<thrust::tbb::tag>(x)
  #define THRUST_TAG "Threading Building Blocks"
#elif defined(USE_OPENMP)
  #define USING_OPENMP
  #include <thrust/system/omp/vector.h>
  #include <thrust/iterator/retag.h>
  #define THRUST_RETAG(x) thrust::reinterpret_tag<thrust::omp::tag>(x)
  #define THRUST_TAG "OpenMP"
#else
  #include <thrust/system/cpp/vector.h>
  #include <thrust/iterator/retag.h>
  #define THRUST_RETAG(x) thrust::reinterpret_tag<thrust::cpp::tag>(x)
  #define THRUST_TAG "Plain C++"
#endif

inline const char* thrust_device_system_str(unsigned device_system) {
  switch (device_system) {
  case THRUST_DEVICE_SYSTEM_CPP:
    return "CPP";
  case THRUST_DEVICE_SYSTEM_CUDA:
    return "CUDA";
  case THRUST_DEVICE_SYSTEM_OMP:
    return "OMP";
  case THRUST_DEVICE_SYSTEM_TBB:
    return "TBB";
  case 0:
    return "Undefined";
  default:
    return "Unknown";
  }
}

#endif

