#ifndef _MATERIALS_HPP_
#define _MATERIALS_HPP_
/**
 * \file materials.h
 *
 * Defines the util::Materials class
 */

#include <Config.hpp>
#include <string>
#include <qgl.h>
#include <GL/glu.h>

namespace mdx 
{
  /**
   * \class Materials materials.h <util/materials.h>
   * \brief A utility class for materials.
   *
   *  This class provides an interface for VLAB material files and
   *  their use for OpenGL.  The material files can contain up to 255
   *  materials.  The class allows access to these by an index.  If a
   *  material index is called that is not in the file, a default
   *  material is provided that uses the default values for OpenGL
   *  1.2.
   */

  class Materials : public FileObject 
  {
  public:
    /** \brief The material data structure. */
    struct Material {
      bool isDefault;
      GLfloat ambient[4];
      GLfloat diffuse[4];
      GLfloat emission[4];
      GLfloat specular[4];
      GLfloat shiny;
      GLfloat transparency;
    };

    Materials(std::string filename);

    void reread();

    void            useMaterial(unsigned int index);
    const Material& getMaterial(unsigned int index);
    void            blend(unsigned int ind1, unsigned int ind2, float t);

  private:
    Material mats[256];
  };
}

#endif
