//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

// Surface class
#ifndef DYNAMX_PROCESS_COLOR_MAP_HPP
#define DYNAMX_PROCESS_COLOR_MAP_HPP

#include <Process.hpp>
#include <Geometry.hpp>
#include <ColorMap.hpp>
#include <ColorEditDlg.hpp>

namespace mdx
{
  class mdxBase_EXPORT ColorMapProcess : public Process 
  {
    Q_OBJECT
  public:
    ColorMapProcess(const Process &process) : Process(process), colorMap(&colors, &bounds) {}

    // Initialize, call the dialog
    bool initialize(QStringList &parms, QWidget *parent);

    // Do nothing
    bool run(const QStringList &parms) { return true; }

    // Process long description
    QString description() const { return "Color Map"; }

    // Parameters
    QStringList parmNames() const { return QStringList() << "Default"; }
    QStringList parmDescs() const { return QStringList() << "Default color map"; }
    QStringList parmDefaults() const { return QStringList() << "Jet"; }
    ParmChoiceMap parmChoice() const 
    {
      ParmChoiceMap map;
      map[0] = colorMap.getColorMapList();
      return map;
    }

    // Plug-in icon
    QIcon icon() const { return QIcon(":/images/ColorPalette.png"); }

  private:
    std::vector<Colorb> colors;
    Point2f bounds;
    ColorEditDlg *dlg;
  public:
    ColorMap colorMap;

  public slots:
    virtual void update();
  };
}  
#endif
