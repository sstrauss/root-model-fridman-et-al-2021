//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef FILELISTWIDGET_HPP
#define FILELISTWIDGET_HPP

#include <Config.hpp>

#include <QListWidget>

namespace mdx 
{
  class mdx_EXPORT FileListWidget : public QListWidget {
    Q_OBJECT
  public:
    FileListWidget(QWidget* parent = 0);
  
  signals:
    void filesDropped(const QStringList& files);
  
  protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dragMoveEvent(QDragMoveEvent* event);
    void dragLeaveEvent(QDragLeaveEvent* event);
    void dropEvent(QDropEvent* event);
  
    bool handlingDrop;
  };
}
#endif 
