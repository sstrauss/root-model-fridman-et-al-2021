//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef COLORBAR_H
#define COLORBAR_H

#include <Config.hpp>
#include <Color.hpp>
#include <ColorMap.hpp>
#include <GL.hpp>

#include <MDXViewer/qglviewer.h>
#include <Parms.hpp>

#include <iostream>
#include <QDataStream>
#include <QString>
#include <QTextStream>
#include <string>
#include <valarray>

namespace mdx 
{
  class mdx_EXPORT Colorbar 
	{
  public:
    typedef std::valarray<double> array;
    enum Position { TOP, BOTTOM, LEFT, RIGHT, TOP_LEFT, TOP_RIGHT, BOTTOM_RIGHT, BOTTOM_LEFT };
  
    enum Orientation { HORIZONTAL, VERTICAL };
  
    Colorbar(Position pos = RIGHT);
  
    void draw(GLuint colormapTexId, QPaintDevice* device) const;
    void draw(const ColorMap &colorMap, uint channel, QPaintDevice* device) const;

    Position position;
    Orientation orientation;
    QFont font;
  
    double scale_length;
    double width;
    double distance_to_border;
    double text_to_bar;
    double tick_size;
    double exp_size;
    double epsilon;
    double line_width;
  
    double vmin, vmax;
    bool reversed;
  
    double globalScaling;
  
    QString label;
  
    void readParms(Parms& parms, QString section);
    void writeParms(QTextStream& pout, QString section);
  
    void scaleDrawing(double scale);
    void restoreScale();

    void setBounds(double _vmin, double _vmax);
    inline void setBounds(const Point2d &bounds)
    {
      setBounds(bounds[0],bounds[1]);
    }

    inline void setLabel(const QString &_label) {
      label = _label;
    }

  protected:
    void startScreenCoordinatesSystem(QPaintDevice* device) const;
    void stopScreenCoordinatesSystem() const;
  
    mutable double prev_width, prev_height;
    void getValues(double start, double end, double delta, array& result) const;
    array selectValuesDirect(double length, bool is_vertical, const QFontMetricsF& metric, double min_dist) const;
    std::pair<double, double> significantDigits(double start, double end) const;
    bool canRenderTicks(const array& ticks, double length, double min_dist, bool is_vertical,
                         const QFontMetricsF& font_metric) const;
    QStringList _tick2str(const array& ticks, QString* extra = 0) const;
    array selectValues(double length, bool is_vertical, const QFontMetricsF* metric = 0) const;
  };
  
  std::ostream& operator<<(std::ostream& s, const Colorbar::Position& pos);
  std::istream& operator>>(std::istream& s, Colorbar::Position& pos);
  
  QTextStream& operator<<(QTextStream& s, const Colorbar::Position& pos);
  QTextStream& operator>>(QTextStream& s, Colorbar::Position& pos);
  
  QDataStream& operator<<(QDataStream& s, const Colorbar::Position& pos);
  QDataStream& operator>>(QDataStream& s, Colorbar::Position& pos);
}
#endif
