//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "DebugDlg.hpp"

DebugDlg::DebugDlg(MorphoViewer* v, QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f), viewer(v)
{
  ui.setupUi(this);
  if(ui.peeling->isChecked())
    viewer->show_slice = ui.peelingSlice->value();
  else
    viewer->show_slice = -1;
  viewer->slice_type = ui.peelingType->currentIndex();
}

void DebugDlg::on_peeling_toggled(bool on)
{
  if(on)
    viewer->show_slice = ui.peelingSlice->value();
  else
    viewer->show_slice = -1;
  viewer->updateAll();
}

void DebugDlg::on_peelingSlice_valueChanged(int val)
{
  viewer->show_slice = val;
  viewer->updateAll();
}

void DebugDlg::on_peelingType_currentIndexChanged(int val)
{
  viewer->slice_type = val;
  viewer->updateAll();
}
