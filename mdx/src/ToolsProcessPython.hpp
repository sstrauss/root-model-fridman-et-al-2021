//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TOOLS_PROCESS_PYTHON_HPP
#define TOOLS_PROCESS_PYTHON_HPP

#include <Process.hpp>

#if defined(WIN64) && defined(__GNUC__)
#  define Py_InitModule4 Py_InitModule4_64
#endif

// Avoid name clash with Qt "slots"
#pragma push_macro("slots")
#undef slots
#include "Python.h"
#pragma pop_macro("slots")

#include <string>

namespace mdx 
{
  class PythonProcess;
  
  struct mdxPython_EXPORT mdx_Factory 
  {
    PythonProcess* process;
  };
  
  struct mdxPython_EXPORT mdx_Factory_Python 
  {
    PyObject_HEAD;
    mdx_Factory intern;
    PyObject* fact_dict;
  };
  
  struct mdxPython_EXPORT mdx_Process 
  {
    QString name;
    int numParms;
    PythonProcess* process;
  };
  
  struct mdxPython_EXPORT mdx_Process_Python 
  {
    PyObject_HEAD;
    PyObject* descr;
    mdx_Process intern;
  };
  
  extern mdxPython_EXPORT PyTypeObject factory_type;
  extern mdxPython_EXPORT PyTypeObject process_type;
  
  /**
   * \class PythonProcess PythonProcess.hpp <PythonProcess.hpp>
   *
   * This process evaluate a Python script from which other processes can be called.
   *
   * \ingroup ToolsProcess
   */
  class mdxPython_EXPORT PythonProcess : public Process 
  {
  public:
    PythonProcess(const Process& process) : Process(process) 
    {
      setName("Tools/Python/Python Script");
      setDesc("Run python script");
      setIcon(QIcon(":/images/Python.png"));

      addParm("File Name", "Name of Python script file", "");
      addParm("Arguments", "Command line arguments for script", "");
    }
    bool initialize(QWidget* parent);
  
    bool run() 
    { 
      return run(parm("File Name"), parm("Arguments")); 
    }
    bool run(QString fileName, QString arguments); 
  
    mdx_Process_Python* createProcess(const QString &name);
  
  protected:
    bool initPython(const QString& fileName);
    bool finalizePython();
    bool addFactory(char* name);
    bool removeFactory(char* name);
    QString getTraceBack(PyObject* tb, int limit);
  
    PyObject* module, *main, *main_dict;
  };
}
#endif
