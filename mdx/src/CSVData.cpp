//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CSVData.hpp>
namespace mdx
{
  // Number of rows in table
  size_t CSVData::rows() const { return _rows.size(); }
  // Number of columns in table
  size_t CSVData::columns() const { return _columns.size(); }

  // Const access to a row of the table
  const QStringList &CSVData::operator[](uint i) const
  { 
    if(i >= rows())
      throw QString("CSVData::[] invalid row: %1").arg(i);

    return _rows[i]; 
  }

  // Set a value in the table
  bool CSVData::setValue(uint row, uint col, const QString &val)
  {
    if(rows() > row and columns() > col) {
      const QString &currVal = _rows[row][col];
      if(val != currVal) {
        // Update the value
        _rows[row][col] = val;
        // Update the index if it is active and valid
        auto &column = _columns[col];
        if(column.doIndex and column.indexValid) {
          column.index[currVal].erase(row);
          column.index[val].insert(row);
        }
      }
      return true;
    }
    return false;
  }

  // Return the column name
  QString CSVData::columnName(uint i) const
  { 
    if(columns() > i)
      return _columns[i].name; 

    mdxWarning << QString("CSVData::columnName invalid column: %1").arg(i) << endl;
    return QString();
  }

  // Return the column number for a name, or columns() if it doesn't exist
  uint CSVData::column(const QString &name) const
  {
    for(uint i = 0; i < columns(); i++)
      if(_columns[i].name == name)
        return i;
    return columns();
  }

  // Return the list of column names
  QStringList CSVData::columnList() const
  { 
    QStringList colList;
    for(const auto &col : _columns)
      colList << col.name;
    return colList; 
  }
   
  // Add a column to the table, pad if required
  int CSVData::addColumn(const QString &name, bool doIndex)
  {
    Column column;
    column.name = name;
    column.doIndex = doIndex;
    column.indexValid = false;
    
    _columns << column;

    // Pad the rows if required
    for(auto &row : _rows)
      while(size_t(row.size()) < columns())
        row << "";

    return columns() - 1;
  }

  // Add a row, pad if required, longer is allowed
  bool CSVData::addRow(const QStringList &row)
  {
    _rows.push_back(row);
    while(_rows.back().size() < _columns.size())
      _rows.back() << "";

    for(uint i = 0; i < columns(); i++) {
      auto &col = _columns[i];
      if(col.doIndex)
        col.index[_rows.back()[i]].insert(_rows.size() - 1);
    }
    return true;
  }

  // Return if index is active
  bool CSVData::doIndex(uint col) const 
  { 
    if(columns() > col)
      return _columns[col].doIndex; 

    mdxWarning << QString("CSVData::doIndex invalid column: %1").arg(col) << endl;
    return false;
  }

  // Set and index active
  bool CSVData::setDoIndex(uint col, bool value)
  { 
    if(columns() > col) {
      auto &columns = _columns[col];
      if(columns.doIndex != value) {
        columns.doIndex = value; 
        columns.indexValid = false; 
      }
      return columns.doIndex; 
    }

    mdxWarning << QString("CSVData::setDoIndex invalid column: %1").arg(col) << endl;
    return false;
  }

  // Rebuild an index
  bool CSVData::reindex(uint col)
  {
    if(columns() > col) {
      auto &column = _columns[col];
      column.index.clear();
      if(column.doIndex)
        for(uint row = 0; row < _rows.size(); row++)
          column.index[_rows[row][col]].insert(row);

      column.indexValid = true;
      return true;
    }
    return false;
  }

  // Rebuild all the row indices
  bool CSVData::reindex()
  {
    bool result = true;
    #pragma omp parallel for
    for(int col = 0; col < _columns.size(); col++)
      if(!reindex(col))
        result = false;

    return result;
  }

  // Find a value in the table
  IntSet CSVData::find(uint col, const QString &s)
  {
    if(columns() > col) {
      auto &column = _columns[col];
      // Turn on index if not already
      setDoIndex(col, true);
      // Build if required
      if(!column.indexValid) {
        #pragma omp critical
        {
          if(!column.indexValid)
            reindex(col);
        }
      }

      auto pr = column.index.find(s);
      if(pr != column.index.end())
        return pr->second;

      // Not found, return empty set
      return IntSet();
    }
    mdxInfo << QString("CSVData::find Invalid column: %1").arg(col);

    return IntSet();
  }

  bool CSVData::clear()
  {
    _columns.clear();
    _rows.clear();
    return true;
  }

  // Serialization
  bool readAttr(CSVData &csv, const QByteArray &ba, size_t &pos)
  {
    size_t sz;
    bool result = readAttr(sz, ba, pos);
    for(size_t i = 0; i < sz; i++) {
      QString name;
      result &= readAttr(name, ba, pos);
      bool doIndex;
      result &= readAttr(doIndex, ba, pos);
      csv.addColumn(name, doIndex);
    }
    size_t vSz;
    result &= readAttr(vSz, ba, pos);
    for(size_t i = 0; i < vSz; i++) {
      result &= readAttr(sz, ba, pos);
      QStringList row;
      for(size_t j = 0; j < sz; j++) {
        QString s;
        result &= readAttr(s, ba, pos);
        row << s;
      }
      csv.addRow(row);
    }
    csv.reindex();

    return result; 
  }
  bool writeAttr(const CSVData &csv, QByteArray &ba) 
  {
    bool result = writeAttr(size_t(csv.columns()), ba);
    for(uint i = 0; i < csv.columns(); i++) {
      result &= writeAttr(csv.columnName(i), ba);
      result &= writeAttr(csv.doIndex(i), ba);
    }
    result &= writeAttr(size_t(csv.rows()), ba);
    for(uint i = 0; i < csv.rows(); i++) {
      const auto &row = csv[i];
      result &= writeAttr(size_t(row.size()), ba);
      for(const QString &s : row)
        result &= writeAttr(s, ba);
    }

    return result;
  }
}
