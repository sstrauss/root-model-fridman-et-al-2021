//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef GL_HPP
#define GL_HPP

#include <Config.hpp>
#include <GL/gl.h>
#include <GL/glu.h>
#include <Information.hpp>

// For renderbuffers we need OpenGL >= 3.0
#define MDX_REQUIRED_OPENGL_VERSION "3.0"
#include <QOpenGLFunctions_3_0>
#define REPORT_GL_ERROR(str) reportGLError(str, __FILE__, __LINE__)
namespace mdx
{
  typedef QOpenGLFunctions_3_0 MDXOpenGLFunctions;
  extern mdx_EXPORT MDXOpenGLFunctions *glfuncs;

  inline bool reportGLError(const QString& str, const char* file, int line)
  {
    GLenum error = glfuncs->glGetError();
    if(error) {
      mdxInfo << "OpenGL error in file " << file << " on line " << line << " for command : " << str << endl
          << (char*)gluErrorString(error) << endl;
      return true;
    }
    return false;
  }
}

#endif
