//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessPDG.hpp>

#include <Dir.hpp>
#include <Progress.hpp>
#include <SetVector.hpp>
#include <Triangulate.hpp>

namespace mdx
{
  using std::swap;
  
  namespace {

    typedef std::unordered_map<int, std::pair<VtxVec, VtxVec> > IntVtxVecPairMap;
    typedef std::pair<int, std::pair<VtxVec, VtxVec> > IntVtxVecPairPair;
    typedef std::unordered_map<int, set_vector<int> > IntLabelsMap;
    typedef std::unordered_map<vertex, set_vector<int> > VertLabelsMap;
    typedef std::unordered_map<int, vertex> VMap;
    typedef std::pair<int, vertex> VMPair;
    
    struct CorrespondenceMeshes {
      void clear()
      {
        J1J2.clear();
        J1J2forC1.clear();
        J1J2forC2.clear();
        centerNeighborsMapT1.clear();
        centerNeighborsMapT2.clear();
      }

      // To store the correspondence between junctions:
      // for the entire meshes:
      VtxVtxMap J1J2;
      // for each cell in mesh1:
      IntVtxVecPairMap J1J2forC1;
      IntVtxVecPairMap J1J2forC2;
      // Needed to check the connections between neighbors:
      IntLabelsMap centerNeighborsMapT1;
      IntLabelsMap centerNeighborsMapT2;
    };
    
    CorrespondenceMeshes cm;
  }
  
  // take the correspondence between junctions for the whole mesh, compute the one for each cell
  bool J1J2forCell(Mesh *mesh, const vvGraph& T1, const IntIntMap& labelsT1, const IntLabelsMap& parentDaughtersMap,
                   set_vector<vertex>& problemJunctionT1)
  {
    // Get label count for each vertex
    VtxIntMap labCount;
    getLabelCount(T1, labCount);

    forall(const vertex &c1, T1) {
      std::vector<vertex> J1forC1;
      std::vector<vertex> J2forC2;

      // c1 has to be a center, connected to  at least 3 vertices
      {
        IntIntMap::const_iterator it = labelsT1.find(c1->label);
        if(c1->type != 'c' or T1.valence(c1) < 3 or it == labelsT1.end() or it->second == 0)
          continue;
      }
      IntLabelsMap::const_iterator it = parentDaughtersMap.find(c1->label);
      if(it == parentDaughtersMap.end())
        continue;
      const set_vector<int>& lDaughters = it->second;
      if(lDaughters.count(0) > 0)
        continue;

      // pick a vertex in neihborhood of c1 to start searching for correspondence.
      vertex v1 = T1.anyIn(c1);
      vertex start1 = v1;
      do
        v1 = T1.nextTo(c1, v1);
      while(labCount[v1] < 3 and v1 != start1);
      // if couldn't find any vertex connected to 3 neighbors, go to next cell in T1
      if(labCount[v1] < 3) {
        mdxInfo << "could not find a 3 way junction in cell: " << c1->label << " in T1" << endl;
        continue;
        //break;
      }

      vertex j1 = v1;     // starting vertex for the correpondance
      // check if there is a corresponding vertex in S2
      do {
        if(j1->type == 'j') {
          VtxVtxMap::iterator it = cm.J1J2.find(j1);
          if(it != cm.J1J2.end()) {
            vertex j2 = it->second;
            J1forC1.push_back(j1);
            J2forC2.push_back(j2);
          } else {
            problemJunctionT1.insert(j1);
          }
        }
        j1 = T1.nextTo(c1, j1);
      } while(j1 != v1);

      // if the correspondence was OK for the whole cell, store it into  J1J2forC1
      {
        IntLabelsMap::iterator it = cm.centerNeighborsMapT1.find(c1->label);
        if(it != cm.centerNeighborsMapT1.end() and it->second.size() != 0 and it->second.size() == J2forC2.size()) {
          cm.J1J2forC1[c1->label].first = J1forC1;
          cm.J1J2forC1[c1->label].second = J2forC2;
        } else
          mdxInfo << "Label " << c1->label << " has only " << J2forC2.size()
                           << " corresponding vertices in T1 but " << J1forC1.size() << " in T0" << endl;
      }
    }
    return true;
  }
  
  // Check the correspondence between the junctions
  bool CorrespondenceJunctions::run(Mesh* mesh1, Mesh* mesh2, bool ShowVVCorrespondence, bool convert)
  {
    // clean up the previous correspondence
    cm.clear();
    mesh1->setShowLabel("Label");
    mesh2->setShowLabel("Label");

    // Define temp vvgraphs
    vvGraph T1s, T2s;
    // Use the same graph if convert set
    vvGraph &T1 = convert ? mesh1->graph() : T1s;
    vvGraph &T2 = convert ? mesh2->graph() : T2s;

    // Create new graph containing only junctions
    if(convert)
      mesh1->updateAll();
    else
      mesh1->updateSelection(); // In case it fails
    if(!mdxmToMgxc(mesh1->graph(), T1, 0))
      return false;

    if(convert)
      mesh2->updateAll();
    else
      mesh2->updateSelection(); // In case it fails
    if(!mdxmToMgxc(mesh2->graph(), T2, 0))
      return false;

    return(checkNhbd(mesh1, mesh2, T1, T2, ShowVVCorrespondence));
  }
  REGISTER_PROCESS(CorrespondenceJunctions);

  bool CorrespondenceJunctions::checkNhbd(Mesh* mesh1, Mesh* mesh2, vvGraph &T1, vvGraph &T2, bool ShowVVCorrespondence){
    // Create a map of labels in both meshes, to check parent labeling or create it
    // we will later add 0 values in the label map for cells which don't have daughters
    // (in T1) or parent (in T2)
    IntIntMap labelsT1, labelsT2;
    forall(const vertex &c, T1)
        if(c->type == 'c')
        labelsT1[c->label] = c->label;
    forall(const vertex &c, T2)
        if(c->type == 'c')
        labelsT2[c->label] = c->label;

    // daughterParentMap will be either labels or parents depending on what is selected
    // If cell does not have parent in T1, key = label in T2, value = 0
    IntIntAttr &parents = mesh2->labelMap("Parents");
    IntIntAttr daughterParentMap;
    if(mesh2->useParents()) {
      mdxInfo << "Found parent labels, using parent labeling to check correspondence between junctions." << endl;
      // Check the parents for cells which are not in the mesh1 or mesh2
      for(IntIntAttr::const_iterator it = parents.begin(); it != parents.end(); it++) {
        int lParent = it->second;
        int lDaughter = it->first;
        if(labelsT1.count(lParent) == 0 or labelsT2.count(lDaughter) == 0)
          // RSS If we don't find it just skip it.
          continue;
        //return setErrorMessage(QString("Problems with parent label: %1, daughter label: %2."
        //     " First run Correct Parents, then Save and Load parents.").arg(lParent).arg(lDaughter));
        daughterParentMap[lDaughter] = lParent;
      }
    } else {
      // If there is no parent map stored in mesh, the meshes should have the same labels
      // create a parent map based on the same cells in both meshes
      mdxInfo << "No parent labels found, using labels to check correspondence between junctions." << endl;
      for(IntIntMap::const_iterator l = labelsT1.begin(); l != labelsT1.end(); l++) {
        int label = l->first;
        if(labelsT2.count(label) != 0)
          daughterParentMap[label] = label;
      }
    }

    // Look at the daughters (in T2) for each cell in T1
    // parentDaughtersMap: key = parent in T1, value = labels of daughters in T2
    // If there is no parent, key = 0, values = labels of orphans (in T2)
    IntLabelsMap parentDaughtersMap;
    for(IntIntMap::const_iterator l = labelsT2.begin(); l != labelsT2.end(); l++) {
      int labelDaughter = l->first;
      int labelParent;
      set_vector<int> LDaughters;
      // add to daughterParentMap 0 values for the cells that do not have parent in T1.
      // map the label of cells in T2 without parent with 0
      {
        IntIntAttr::iterator it = daughterParentMap.find(labelDaughter);
        if(it == daughterParentMap.end() or it->second == 0 or labelDaughter == 0) {
          labelsT2[labelDaughter] = 0;
          daughterParentMap[labelDaughter] = 0;
          labelParent = 0;
        } else {
          labelParent = it->second;
        }
      }
      IntLabelsMap::iterator it = parentDaughtersMap.find(labelParent);
      if(it != parentDaughtersMap.end())
        LDaughters = it->second;
      LDaughters.insert(labelDaughter);
      parentDaughtersMap[labelParent] = LDaughters;
    }
    // add 0 values in parentDaughtersMap for cells in T1 that do not have daughters
    // map the label of cells in T1 without daughters with 0
    set_vector<int> noDaughter;
    noDaughter.insert(0);
    for(IntIntMap::const_iterator l = labelsT1.begin(); l != labelsT1.end(); l++) {
      int labelParent = l->first;
      if(parentDaughtersMap.count(labelParent) == 0) {
        parentDaughtersMap[labelParent] = noDaughter;
        labelsT1[labelParent] = 0;
      }
    }

    // Fill in a map of the labels associated with each junction in T1 and T2.
    // The list of labels is sorted (it is a set).
    // If there are only 2 neighbors, then the edge is at the mesh border and
    // we say it is in contact with the outside (label 0);
    // Labels of cells of T1 that do not have daughters are stored as 0.
    // This will be used to identify junctions at each time points.
    VertLabelsMap junctionLabelsT1;
    forall(const vertex &j, T1) {
      set_vector<int> labels;
      if(j->type == 'j') {
        // list the labels around junction
        forall(const vertex &n, T1.neighbors(j))
            if(n->label > 0) {
          IntIntMap::iterator it = labelsT1.find(n->label);
          if(it != labelsT1.end())
            labels.insert(it->second);
        }
        if(labels.size() == 2)
          labels.insert(0);
        junctionLabelsT1[j] = labels;
      }
    }
    VertLabelsMap junctionLabelsT2;
    forall(const vertex &j, T2) {
      set_vector<int> labels;
      if(j->type == 'j') {
        // list the labels around junction
        forall(const vertex &n, T2.neighbors(j))
            if(n->label > 0) {
          IntIntMap::iterator it = labelsT2.find(n->label);
          if(it != labelsT2.end())
            labels.insert(it->second);
        }
        if(labels.size() == 2)
          labels.insert(0);
        junctionLabelsT2[j] = labels;
      }
    }

    // Look for the correspondence between junctions in T1 and in T2
    forall(const vertex &j2, T2) {
      if(junctionLabelsT2.count(j2) > 0) {
        set_vector<int> labelsDaughters;
        VertLabelsMap::iterator it = junctionLabelsT2.find(j2);
        if(it != junctionLabelsT2.end())
          labelsDaughters = it->second;
        set_vector<int> labelsParents;
        forall(int lDaughter, labelsDaughters) {
          IntIntAttr::iterator it = daughterParentMap.find(lDaughter);
          int lParent = 0;
          if(it != daughterParentMap.end())
            lParent = it->second;
          labelsParents.insert(lParent);
        }
        // here compare the labels of parents of junction in T2 with junction in T1.
        // we considere only 3 ways junctions.
        if(labelsParents.size() == 3)
          for(VertLabelsMap::const_iterator it = junctionLabelsT1.begin();
              it != junctionLabelsT1.end(); it++) {
            const set_vector<int>& labelsJunction = it->second;
            if(labelsJunction == labelsParents)
              cm.J1J2[it->first] = j2;
          }
      }
    }

    // Show the correspondence between vertices.
    IntMatrix3fMap& J1J2vectors = mesh1->vvCorrespondence();
    J1J2vectors.clear();
    mesh1->showVVCorrespondence() = ShowVVCorrespondence;
    int i = 0;
    for(VtxVtxMap::const_iterator it = cm.J1J2.begin(); it != cm.J1J2.end(); it++, i++) {
      vertex j1 = it->first;
      vertex j2 = it->second;
      J1J2vectors[i][0] = Point3f(j1->pos);
      J1J2vectors[i][1] = Point3f(j2->pos);
    }

    // Create a map of neighbors (labels) for each cell in T1 and T2.
    // in T1, labels of neighbors without daughters are stored as 0
    forall(const vertex &c, T1) {
      set_vector<int> labels;
      if(c->type == 'c' and c->label > 0) {
        // list the labels at junctions around the center
        forall(const vertex &j, T1.neighbors(c)) {
          if(junctionLabelsT1.count(j) != 0) {
            VertLabelsMap::iterator it = junctionLabelsT1.find(j);
            if(it != junctionLabelsT1.end())
              labels.insert(it->second.begin(), it->second.end());
          }
        }
        labels.erase(c->label);
        cm.centerNeighborsMapT1[c->label] = labels;
      }
    }
    // in T2, labels of neighbors without parents are stored as 0
    forall(const vertex &c, T2) {
      set_vector<int> labels;
      if(c->type == 'c' and c->label > 0) {
        // list the labels at junctions around the center
        forall(const vertex &j, T2.neighbors(c)) {
          if(junctionLabelsT2.count(j) != 0) {
            VertLabelsMap::iterator it = junctionLabelsT2.find(j);
            if(it != junctionLabelsT2.end())
              labels.insert(it->second.begin(), it->second.end());
          }
        }
        labels.erase(c->label);
        cm.centerNeighborsMapT2[c->label] = labels;
      }
    }

    // Select the vertices in T1 which cause trouble
    set_vector<vertex> problemJunctionT1;
    mdxInfo << "Check problems with correspondence between junctions:" << endl;
    J1J2forCell(mesh1, T1, labelsT1, parentDaughtersMap, problemJunctionT1);
    mdxInfo << problemJunctionT1.size() << " problem junctions found in "
                     << labelsT1.size() << " cells" << endl;
    forall(const vertex &v, T1) {
      v->selected = false;
      if(problemJunctionT1.count(v) != 0)
        v->selected = true;
    }

    // Check that the neighbors around each cell are consistent in T1 and T2
    IntFloatMap heatMapDiffNeighborsT1;
    IntFloatMap heatMapDiffNeighborsT2;
    for(IntLabelsMap::const_iterator it = parentDaughtersMap.begin();
        it != parentDaughtersMap.end(); ++it) {
      IntIntMap::iterator ip = labelsT1.find(it->first);
      if(ip == labelsT1.end())
        continue;
      int labelParent = ip->second;
      if(labelParent == 0)
        continue;

      set_vector<int> LDaughters = it->second;
      // case of cells without daughters (giant cell + periphery)
      if(LDaughters.size() == 1 and LDaughters.count(0) == 1) {
        IntLabelsMap::iterator ip = parentDaughtersMap.find(0);
        if(ip != parentDaughtersMap.end())
          LDaughters = ip->second;
      }

      set_vector<int> neighborsT1;
      set_vector<int> neighborsT2;
      set_vector<int> parentsNeighborsT2;

      // list the neighbors around parent cell in T1
      {
        IntLabelsMap::iterator ip = cm.centerNeighborsMapT1.find(labelParent);
        if(ip != cm.centerNeighborsMapT1.end())
          neighborsT1 = ip->second;
        else
          neighborsT1.insert(0);
      }

      // list the neighbors around daughter cells in T2
      forall(int labelD, LDaughters) {
        if(cm.centerNeighborsMapT2.count(labelD) != 0) {
          IntLabelsMap::iterator ip = cm.centerNeighborsMapT2.find(labelD);
          if(ip != cm.centerNeighborsMapT2.end())
            neighborsT2.insert(ip->second.begin(), ip->second.end());
        }
      }
      // take out daughter cells from neighborsT2
      set_vector<int> neighborsT2temp;
      forall(int neighbor, neighborsT2)
          if(LDaughters.count(neighbor) == 0)
          neighborsT2temp.insert(neighbor);
      neighborsT2 = neighborsT2temp;

      // look at the parents of neighbors in T2
      forall(int neighbor, neighborsT2) {
        IntIntAttr::iterator ip = daughterParentMap.find(neighbor);
        if(ip != daughterParentMap.end())
          parentsNeighborsT2.insert(ip->second);
        else
          parentsNeighborsT2.insert(0);
      }

      // compare the neighbors around parent and daughter cells, fill in heat map with differences
      heatMapDiffNeighborsT1[labelParent] = heatMapDiffNeighborsT2[labelParent]
          = parentsNeighborsT2 != neighborsT1 ? 1 : 0;

      if(parentsNeighborsT2.size() != neighborsT1.size())
        mdxInfo << "problem with cell: " << labelParent
                         << ", the number of neighbors changes. " << parentsNeighborsT2.size()
                         << " parents of neighbors in T2 vs. " << neighborsT1.size()
                         << " neighbors in T1." << endl;
    }

    // Display the difference in neighbor number in a heatmap
    mesh1->labelHeat().clear();
    mesh2->labelHeat().clear();
    forall(const IntFloatPair& p, heatMapDiffNeighborsT1)
        mesh1->labelHeat()[p.first] = p.second;
    forall(const IntFloatPair& p, heatMapDiffNeighborsT2)
        mesh2->labelHeat()[p.first] = p.second;

    mesh1->setHeatMapBounds(Point2f(0, 1));
    mesh1->setShowLabel("Label Heat");
    mesh1->updateTriangles();
    mesh1->updateSelection();
    mesh2->setHeatMapBounds(Point2f(0, 1));
    mesh2->setShowLabel("Label Heat");
    mesh2->updateTriangles();

    setStatus("Cells with change in number of neighbors appear in red, vertices which "
              "could not be identified are selected on the first mesh.");
    return true;
  }
  
  // PDGs computation
  
  // compute polygon center:
  Point3d cellCenter(const std::vector<Point3d>& polygon)
  {
    Point3d center = Point3d(0, 0, 0);
    int nbSides = polygon.size();
    for(int i = 0; i < nbSides; i++)
      center += polygon[i];

    center /= nbSides;
    return center;
  }
  
  // compute polygon normal:
  Point3d cellNormal(const std::vector<Point3d>& polygon, const Point3d& centerPolygon)
  {
    Point3d normalPolygon = Point3d(0, 0, 0);
    int nbSides = polygon.size();
    int prev = nbSides - 1;
    for(int i = 0; i < nbSides; i++) {
      normalPolygon += (polygon[prev] - centerPolygon) % (polygon[i] - centerPolygon);     // cross product
      prev = i;
    }
    normalPolygon.normalize();
    return normalPolygon;
  }
  
  // multiply vector of point3d by matrix:
  std::vector<Point3d> matrixMultiply(const std::vector<Point3d>& P, const Matrix3d& M)
  {
    int nbRows = P.size();
    std::vector<Point3d> result(nbRows);
    for(int i = 0; i < nbRows; ++i)
      result[i] = P[i] * M;
    return result;
  }
  /* now in triangulate.hpp/cpp
  void inplaceMatrixMultiply(std::vector<Point3d>& P, const Matrix3d& M)
  {
    int nbRows = P.size();
    for(int i = 0; i < nbRows; ++i)
      P[i] = P[i] * M;
  }*/
  
  // for two matrices P and M of size 2xnbRows, multiply: transpose(P) * M == the dot produc
  Matrix2d dotProduct(const std::vector<Point2d>& P, const std::vector<Point2d>& M)
  {
    Matrix2d prod;
    for(size_t i = 0; i < P.size(); ++i) {
      prod(0, 0) += P[i][0] * M[i][0];
      prod(0, 1) += P[i][0] * M[i][1];
      prod(1, 0) += P[i][1] * M[i][0];
      prod(1, 1) += P[i][1] * M[i][1];
    }
    return prod;
  }
  
  inline double square(double x) {
    return x * x;
  }
  
  // SVD decomposition of a 2D matrix M in: rotation(-theta)*[maxS,0; 0, minS]*rotation(psi)
  void SVDMatrix2x2(Matrix2d M, double& theta, double& psi, double& maxS, double& minS)
  {
    double c1, c2, c3, c4;
    c1 = sqrt(square(M(0, 0) + M(1, 1)) + square(M(0, 1) - M(1, 0)));
    c2 = sqrt(square(M(0, 0) - M(1, 1)) + square(M(0, 1) + M(1, 0)));
    c3 = atan2(M(0, 1) + M(1, 0), M(0, 0) - M(1, 1));
    c4 = atan2(M(0, 1) - M(1, 0), M(0, 0) + M(1, 1));

    maxS = (c1 + c2) / 2;
    minS = (c1 - c2) / 2;
    theta = (c3 - c4) / 2;
    psi = (c3 + c4) / 2;
  }
  
  // compute Principal Directions of Growth (PDGs) in 2D
  void computePDGs(const std::vector<vertex>& J1forC1, const std::vector<vertex>& J2forC2, SymmetricTensor& CellPDGs_1,
                   SymmetricTensor& CellPDGs_2)
  {
    int nbSides = J1forC1.size();
    std::vector<Point3d> polygon1(nbSides), polygon2(nbSides);

    for(int i = 0; i < nbSides; i++) {
      polygon1[i] = J1forC1[i]->pos;
      polygon2[i] = J2forC2[i]->pos;
    }

    // Compute transformation matrix for the two polygons
    // Compute polygon center (3D)
    const Point3d& centerPolygon1 = cellCenter(polygon1);
    const Point3d& centerPolygon2 = cellCenter(polygon2);

    // Compute polygon normal (3D)
    const Point3d& normalPolygon1 = cellNormal(polygon1, centerPolygon1);
    const Point3d& normalPolygon2 = cellNormal(polygon2, centerPolygon2);

    // Project polygon 3d points on the cell plane and center the polygon on (0,0,0)
    std::vector<Point3d> polygonProj1(nbSides), polygonProj2(nbSides);

    for(int i = 0; i < nbSides; i++) {
      // pos=Point to project (Point3d), p=Point of the plane(Point3d),n=Normal to the plane(point3d)
      polygonProj1[i] = projectPointOnPlane(polygon1[i] - centerPolygon1, Point3d(), normalPolygon1);
      polygonProj2[i] = projectPointOnPlane(polygon2[i] - centerPolygon2, Point3d(), normalPolygon2);
    }
    // Rotate polygon plane to align it with (x,y) plane (new code from triangulate.hpp/cpp, changed Dec 2015, untested)
    Matrix3d invRotPolygonPlane1;
    rotatePointsIntoXY(normalPolygon1, polygonProj1, invRotPolygonPlane1);

    // same for the second polygon
    Matrix3d invRotPolygonPlane2;
    rotatePointsIntoXY(normalPolygon2, polygonProj2, invRotPolygonPlane2);

    // Take out 3rd dimension from projected polygons:
    std::vector<Point2d> polygon1_2D(nbSides), polygon2_2D(nbSides);
    for(int i = 0; i < nbSides; ++i) {
      polygon1_2D[i] = Point2d(polygonProj1[i].x(), polygonProj1[i].y());
      polygon2_2D[i] = Point2d(polygonProj2[i].x(), polygonProj2[i].y());
    }

    // Compute transformation matrix between c1 and c2(in 2D, on projected polygons):
    Matrix2d XTX = dotProduct(polygon1_2D, polygon1_2D);
    Matrix2d XTY = dotProduct(polygon1_2D, polygon2_2D);
    Matrix2d transfM = inverse(XTX) * XTY;

    // Decompose transformation matrix into rotations and growth:
    double theta, psi, maxStretchRatio, minStretchRatio;
    SVDMatrix2x2(transfM, theta, psi, maxStretchRatio, minStretchRatio);

    // Convert transformation matrix from 2D to 3D:
    Matrix3d transfM_3D;
    for(int i = 0; i < 2; ++i)
      for(int j = 0; j < 2; ++j)
        transfM_3D(i, j) = transfM(i, j);

    Matrix3d rotPolygonPlane2 = inverse(invRotPolygonPlane2);//Matrix3d::rotation(rotAxisPlane2, angleRotPlane2);
    Matrix3d rotPolygonPlane1 = inverse(invRotPolygonPlane1);//Matrix3d::rotation(rotAxisPlane1, angleRotPlane1);
    transfM_3D = invRotPolygonPlane1 * transfM_3D * rotPolygonPlane2;

    // Apply 3D transformation to polygon1 (cell before growth), in order to compute residuals
    // (difference between actual and computed transformation):
    std::vector<Point3d> polygon1Centered, polygon1Transf;
    std::vector<double> residuals;
    polygon1Centered.resize(nbSides);
    residuals.resize(nbSides);

    for(int i = 0; i < nbSides; ++i)
      polygon1Centered[i] = polygon1[i] - centerPolygon1;
    polygon1Transf = matrixMultiply(polygon1Centered, transfM_3D);

    for(int i = 0; i < nbSides; ++i) {
      polygon1Transf[i] = polygon1Transf[i] + centerPolygon1;
      residuals[i] = norm(polygon1Transf[i] - polygon2[i]);
    }

    // Return the Principal Directions for polygon2 (deformed configuration):
    CellPDGs_2.ev1() = Point3f(Point3d(cos(psi), sin(psi), 0) * rotPolygonPlane2);    // unit vector PDG max
    CellPDGs_2.ev2() = Point3f(Point3d(-sin(psi), cos(psi), 0) * rotPolygonPlane2);   // unit vector PDG min
    CellPDGs_2.evals() = Point3f(maxStretchRatio, minStretchRatio, 0);

    // Return the Principal Directions for polygon1 (original configuration):
    CellPDGs_1.ev1() = Point3f(Point3d(cos(theta), sin(theta), 0) * rotPolygonPlane1);
    CellPDGs_1.ev2() = Point3f(Point3d(-sin(theta), cos(theta), 0) * rotPolygonPlane1);
    CellPDGs_1.evals() = Point3f(maxStretchRatio, minStretchRatio, 0);
  }
  
  bool GrowthDirections::run(Mesh* mesh1, Mesh* mesh2)
  {
    mesh1->clearCellAxis();
    mesh2->clearCellAxis();
    // Get the meshes
    mesh1->updateCentersNormals();
    mesh2->updateCentersNormals();

    // use the method CellAxis to modify a map associating labels with a vector of
    // 3 Point3f (the cell axis)
    IntSymTensorAttr &CellPDGs_1 = mesh1->cellAxis();
    IntSymTensorAttr &CellPDGs_2 = mesh2->cellAxis();
    CellPDGs_1.clear();
    CellPDGs_2.clear();

    // Calculate the PDGs
    forall(const IntVtxVecPairPair &pr, cm.J1J2forC1)
        computePDGs(cm.J1J2forC1[pr.first].first, cm.J1J2forC1[pr.first].second,
        CellPDGs_1[pr.first], CellPDGs_2[pr.first]);

    mesh1->setCellAxisType("PDG");
    mesh2->setCellAxisType("PDG");
    mesh1->setCellAxisUnit("1");
    mesh2->setCellAxisUnit("1");

    mdxInfo << "Size of Cell PDG map in mesh1: " << CellPDGs_1.size() << endl;
    mdxInfo << "Size of Cell PDG map in mesh2: " << CellPDGs_2.size() << endl;

    // Run PDG display on both meshes, with the parameters from the GUI
    //    ProcessDefinition *def = getProcessDefinition("Mesh/Cell Axis/PDG/Display Growth Directions");
    //    QStringList parms = def->parms;
    //    DisplayPDGs dpdg(*this);
    //    dpdg.run(mesh1, parms);
    //    dpdg.run(mesh2, parms);

    QStringList parms;
    DisplayPDGs *proc = getProcessParms<DisplayPDGs>(this, parms);
    if(!proc)
      throw(QString("Unable to create display fibril orientation process"));
    proc->run(mesh1,parms);
    proc->run(mesh2,parms);

    return true;
  }
  REGISTER_PROCESS(GrowthDirections);

  void DisplayPDGs::processParms(const QStringList &parms)
  {
    DisplayHeatMap = parms[pDisplayHeatMap];
    ScaleHeatMap = parms[pScaleHeatMap];
    RangeHeat = Point2d(parms[pRangeHeatLow].toDouble(), parms[pRangeHeatHigh].toDouble());
    DisplayAxis = parms[pDisplayAxis];
    ColorPos = parms[pColorPos];
    ColorNeg = parms[pColorNeg];
    AxisWidth = parms[pAxisWidth].toFloat();
    AxisScale = parms[pAxisScale].toFloat();
    AxisOffset = parms[pAxisOffset].toFloat();
    AnisotropyThreshold = parms[pAnisotropyThreshold].toFloat();
  }

  bool DisplayPDGs::initialize(QStringList& parms, QWidget* parent)
  {
    Mesh *mesh = currentMesh();
    if(!mesh) throw(QString("No current mesh"));

    // Check values of parameters and change them in the GUI if needed.
    const IntSymTensorAttr& cellAxisBezier = currentMesh()->attributes().attrMap<int, SymmetricTensor>("Cell Axis Bezier");
    // If PDGs not projected on Bezier yet, make sure we won't try to display the projection:
    if(cellAxisBezier.size() == 0) {
      QString heatMap = parms[pDisplayHeatMap];
      if(heatMap == "ShearBezier" or heatMap == "StretchBezierX" or heatMap == "StretchBezierY")
        parms[pDisplayHeatMap] = "StretchMax";

      QString axis = parms[pDisplayAxis];
      if(axis == "StrainBezierX" or axis == "StrainBezierY")
        parms[pDisplayAxis] = "StrainMax";
    }

    return true;
  }

  // Display PDGs (separatly from each other) and heatmaps of anisotropy etc.
  // The stretch vectors are stored in cellAxis, so that the norm of the vector give us the stretch (always positive)
  // The strain (= stretch -1) vectors are visualized in cellAxisVis. No deformation ==> strain = 0 ==> cell axis null
  bool DisplayPDGs::run(Mesh* mesh, const QString displayHeatMap, const QString scaleHeatMap, const Point2d &rangeHeat,
                        const QString displayAxis, const QColor& colorPos, const QColor& colorNeg, float axisWidth, float axisScale,
                        float axisOffset, float anisotropyThreshold)
  {
    // if there is no PDGs stored, display error message
    const IntSymTensorAttr& cellAxis = mesh->cellAxis();
    const IntSymTensorAttr& cellAxisBezier = mesh->attributes().attrMap<int, SymmetricTensor>("Cell Axis Bezier");

    if(cellAxis.size() == 0)
      throw(QString("No cell axis stored in active mesh"));
    if(mesh->cellAxisType() != QString("PDG"))
      throw(QString("No PDGs stored in active mesh"));

    // Scale PDGs for visualization
    mesh->setAxisWidth(axisWidth);
    mesh->setAxisOffset(axisOffset);
    Colorb colorExpansion(colorPos);
    Colorb colorShrinkage(colorNeg);

    // Check if there is a cell center for each cell axis
    mesh->updateCentersNormals();
    IntPoint3fAttr &centers = (mesh->useParents() ? mesh->parentCenterVis() : mesh->labelCenterVis());
    int nProblems = 0;
    forall(const IntSymTensorPair &p, cellAxis) {
      int label = p.first;
      if(centers.count(label) == 0)
        nProblems++;
    }
    if(nProblems != 0)
      setStatus("Warning, non-existing cell center found for " << nProblems << " cells.");

    // Scale the cell axis for display
    IntMatrix3fAttr &cellAxisVis = mesh->cellAxisVis();
    cellAxisVis.clear();
    IntVec3ColorbAttr &cellAxisColor = mesh->cellAxisColor();
    cellAxisColor.clear();

    forall(const IntSymTensorPair &p, cellAxis) {
      int cell = p.first;
      SymmetricTensor tensor = p.second;
      float strain1st, strain2nd, anisotropy;
      Point3f dir1st, dir2nd;

      if(displayAxis == "StrainBezierX" or displayAxis == "StrainBezierY"){
        if(cellAxisBezier.size() == 0)
          throw(QString("No cell axis bezier. Run 'Cell Axis Custom Directions' first."));
        if(cellAxisBezier.find(cell) == cellAxisBezier.end())
          continue;
        tensor = cellAxisBezier.find(cell)->second;
      }

      strain1st = tensor.evals()[0] - 1;
      strain2nd = tensor.evals()[1] - 1;
      dir1st = tensor.ev1();
      dir2nd = tensor.ev2();
      anisotropy = max(tensor.evals()[0],tensor.evals()[1])/min(tensor.evals()[0], tensor.evals()[1]);

      Point3b showAxis;
      showAxis[0] = (displayAxis == "StrainMax" or displayAxis == "StrainBezierX" or displayAxis == "Both");
      showAxis[1] = (displayAxis == "StrainMin" or displayAxis == "StrainBezierY" or displayAxis == "Both");
      showAxis[2] = false;

      if(anisotropyThreshold != 0 and anisotropy < anisotropyThreshold)
        showAxis = Point3b(false, false, false);

      cellAxisVis[cell][0] = (showAxis[0] ? strain1st : 0.f) * dir1st * axisScale;
      cellAxisVis[cell][1] = (showAxis[1] ? strain2nd : 0.f) * dir2nd * axisScale;
      cellAxisVis[cell][2] = Point3f(0, 0, 0);

      for(size_t i = 0; i < 3; ++i) {
        if(tensor.evals()[i] - 1 < 0)
          cellAxisColor[cell][i] = Colorb(colorShrinkage);
        else
          cellAxisColor[cell][i] = Colorb(colorExpansion);
      }
    }

    // Fill in heat map table
    std::vector<float> vecValues;
    if(displayHeatMap != "None") {
      mesh->labelHeat().clear();
      IntFloatAttr& labelHeatMap = mesh->labelHeat();
      forall(const IntSymTensorPair &p, cellAxis) {
        int cell = p.first;
        SymmetricTensor tensor = p.second;
        float value = 0;

        if(displayHeatMap == "StretchMax")
          value = max(tensor.evals()[0], tensor.evals()[1]);
        if(displayHeatMap == "StretchMin")
          value = min(tensor.evals()[0], tensor.evals()[1]);
        if(displayHeatMap == "Aniso=StretchMax/StretchMin")
          value = max(tensor.evals()[0], tensor.evals()[1])/min(tensor.evals()[0], tensor.evals()[1]);
        if(displayHeatMap == "ProductStretches")
          value = tensor.evals()[0] * tensor.evals()[1];

        if(displayHeatMap == "StretchBezierX" or displayHeatMap == "StretchBezierY" or displayHeatMap == "ShearBezier"){
          if(cellAxisBezier.size() == 0)
            throw(QString("No cell axis bezier. Run 'Cell Axis Custom Directions' first."));
          if(cellAxisBezier.find(cell) == cellAxisBezier.end())
            continue;
          tensor = cellAxisBezier.find(cell)->second;
          if(displayHeatMap == "StretchBezierX")
            value = tensor.evals()[0];
          if(displayHeatMap == "StretchBezierY")
            value = tensor.evals()[1];
          if(displayHeatMap == "ShearBezier")
            value = fabs(tensor.evals()[2]);
        }

        labelHeatMap[cell] = value;
        vecValues.push_back(value);
      }
    }

    // Find bounds for heatmap scale.
    float lowBound = 0, highBound = 1;
    // Manual scaling by user input
    if(scaleHeatMap == "Manual"){
      lowBound  = rangeHeat.x();
      highBound = rangeHeat.y();
    }
    // Automatic scaling
    if(vecValues.size() != 0){
      sort(vecValues.begin(), vecValues.end());
      // Default scaling: take min and max values
      if(scaleHeatMap == "None"){
        lowBound = vecValues.front();
        highBound = vecValues.back();
      }
      // Auto scale: take low 5 and high 5 percentile
      else if(scaleHeatMap == "Auto"){
        lowBound = *(vecValues.begin()+int(vecValues.size()*.05));
        highBound = *(vecValues.begin()+int(vecValues.size()*.95));
      }
    }
    // Round the bounds values of heat map, to make sure we have the same bound in case
    // of loading PDGs on both meshes.
    lowBound = floor(lowBound * 1000) / 1000;
    highBound = ceil(highBound * 1000) / 1000;

    mesh->setHeatMapBounds(Point2f(lowBound, highBound));
    mesh->setHeatMapUnit("ratio");
    mesh->setShowLabel("Label Heat");
    mesh->updateTriangles();

    if(displayAxis != "None")
      mesh->setShowAxis("Cell Axis");

    return true;
  }
  REGISTER_PROCESS(DisplayPDGs);
}
