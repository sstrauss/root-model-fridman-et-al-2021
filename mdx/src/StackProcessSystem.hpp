//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_SYSTEM_HPP
#define STACK_PROCESS_SYSTEM_HPP

#include <Process.hpp>
#include <QFileDialog>

/**
 * \file SystemProcessLoad.hpp
 * This file contains processes defined in MorphoDynamX to load data
 */

class QDialog;
class QFile;
class QIODevice;
class Ui_LoadStackDialog;
class Ui_ExportStackDialog;

namespace mdx 
{
  mdxBase_EXPORT QList<int> extractVersion(QIODevice& file);

  /**
   * \class StackImportSeries SystemProcessLoad.hpp <SystemProcessLoad.hpp>
   *
   * Import images forming a stack.
   *
   * \ingroup StackProcess
   */
  class mdxBase_EXPORT StackImportSeries : public Process {
    Q_OBJECT
  public:
    StackImportSeries(const Process& proc) : Process(proc) 
    {
      setName("Stack/System/Import Series");
      setDesc("Import stack from a series of images");
      setIcon(QIcon(":/images/open.png"));

      addParm("Stack", "Stack to import", "-1");
      addParm("Store", "Store to import", "Current", QStringList() << "Main" << "Work" << "Current");
      addParm("X Step", "Size of voxels in X", "1.0");
      addParm("Y Step", "Size of voxels in Y", "1.0");
      addParm("Z Step", "Size of voxels in Z", "1.0");
      addParm("Brightness", "Amount to scale signal in stack", "1.0");
      addParm("Profile File", "Name of file to save image list", "");
    }
    bool initialize(QWidget* parent);
    bool run();
    /**
     * Import the stack
     * \param stack Stack that will contain the image
     * \param store Store that will contain the image
     * \param step Size of a voxel
     * \param brightness Multiplication coefficient to convert less than 16
     * bits images to 16 bits
     * \param filenames Either a single text file containing all the
     * parameters, or a list of image files, one per z slice
     */
    bool run(Stack* stack, Store* store, Point3d step, double brightness, QString filename);
  
  protected slots:
    void saveProfile();
    void loadProfile(QString filename = QString());
    void addFiles();
    void addFiles(const QStringList& files);
    void removeFiles();
    void filterFiles();
    void sortAscending(bool val = true);
    void sortDescending();
  
  protected:
    bool loadProfile(QString filename, Point3u& size, Point3d& step, double& brightness, QStringList& files);
    void setImageSize(Point3u size);
    void setImageSize(uint x, uint y, uint z) { setImageSize(Point3u(x, y, z)); }
    void setImageResolution(Point3d step);
    void setImageResolution(double x, double y, double z) { setImageResolution(Point3d(x, y, z)); }
    Point3d imageResolution();
  
    QString txtFile;
    QDialog* dlg;
    Ui_LoadStackDialog* ui;
    QString loadedFile;
    Point3u imageSize;
    QStringList imageFiles;
  };
  
  /**
   * \class StackOpen SystemProcessLoad.hpp <SystemProcessLoad.hpp>
   *
   * Open a stack in either the Tiff, MDXS or INRIA formats.
   *
   * \ingroup StackProcess
   */
  class mdxBase_EXPORT StackOpen : public Process 
  {
  public:
    StackOpen(const Process& proc) : Process(proc) 
    {
      setName("Stack/System/Open");
      setDesc("Open a stack from a known 3D image format");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "Filename of stack to load", "");
      addParm("Store", "The store to load into", "Main", storeChoice());
      addParm("Stack Id", "The stack number to load", "0");
    }
    bool run();
    /**
     * Open the image
     * \param stack Stack that will contain the image
     * \param store Store that will contain the image
     * \param filename File containing the image.
     */
    bool run(Stack* stack, Store* store, QString filename);

    bool initialize(QWidget* parent);
  
    bool loadMDXS_1_3(QIODevice& file, Stack* stack, Store* store);
    bool loadMDXS_1_2(QIODevice& file, Stack* stack, Store* store);
    bool loadMDXS_1_1(QIODevice& file, Stack* stack, Store* store);
    bool loadMDXS_1_0(QIODevice& file, Stack* stack, Store* store);
    bool loadMDXS_0(QIODevice& file, Stack* stack, Store* store);
    void centerImage();
  };

   /**
   * \class StackImport SystemProcessLoad.hpp <SystemProcessLoad.hpp>
   *
   * Import a stack using CImage.
   *
   * \ingroup StackProcess
   */
  class mdxBase_EXPORT StackImport : public Process 
  {
  public:
    StackImport(const Process& proc) : Process(proc) 
    {
      setName("Stack/System/Import");
      setDesc("Import a stack");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "Filename of stack to load", "");
      addParm("Store", "The store to load into", "Main", storeChoice());
      addParm("Stack Id", "The stack number to load", "0");
      addParm("Auto Scale", "Auto scale the stack on loading", "Yes", booleanChoice());
    }
    bool run();

    /**
     * Open the image
     * \param stack Stack that will contain the image
     * \param store Store that will contain the image
     * \param filename File containing the image.
     */
    bool run(Stack* stack, Store* store, QString filename);

    bool initialize(QWidget* parent);
  
    void centerImage();
  }; 

  /**
   * \class StackSave SystemProcessSave.hpp <SystemProcessSave.hpp>
   *
   * Save the data into a MDXS or INRIA format.
   *
   * Description of the MDXS file format
   * ===================================
   *
   * MDXS is a binary file format to store 3D images with all the metadata needed for proper rendering in MorphoDynamX.
   *
   * There are many versions, from 1.0 to 1.3. This version of MorphoDynamX will always generate MDXS files version 1.3.
   *
   * Each MDXS file starts with the ASCII string `MDXS` followed by a space, the version number in ASCII and another
   * space. For instance, for the current version, the file starts with the ASCII string `"MDXS 1.3 "`.
   *
   * A binary header follows the version number. New versions simply added fields to the header. The header is made of:
   *
   *   Field | Size (bytes) | type  | Version | Description
   * --------|--------------|-------|---------|-------------------------------------------------------------
   * isLabel |     1        | bool  |  1.1    | True if the stack's values are labels and not intensities
   * sxum    |     4        | float |  1.3    | Position of the stack's origin on the X axis, in micrometers
   * syum    |     4        | float |  1.3    | Position of the stack's origin on the Y axis, in micrometers
   * szum    |     4        | float |  1.3    | Position of the stack's origin on the Z axis, in micrometers
   * xsz     |     4        | uint  |  1.0    | Number of voxels along the X axis
   * ysz     |     4        | uint  |  1.0    | Number of voxels along the Y axis
   * zsz     |     4        | uint  |  1.0    | Number of voxels along the Z axis
   * xum     |     4        | float |  1.0    | Resolution along the X axis, in micrometers
   * yum     |     4        | float |  1.0    | Resolution along the Y axis, in micrometers
   * zum     |     4        | float |  1.0    | Resolution along the Z axis, in micrometers
   * datasz  |     8        | uint  |  1.0    | Size in bytes of the data. It should be `2*xsz*ysz*zsz`
   * cl      |     1        | uint  |  1.0    | Compression level
   *
   * If the compression level is 0, the header is then followed by the raw, uncompressed, data. Voxels are ordered
   * C-style (e.g. vertices consecutives along the X axis are consecutive in the file, vertices consecutives along the
   * Y axis are separated by `xsz` voxels in the file, and vertices consecutive in the Z axis are separated by `xsz*ysz`
   * voxels in the files).
   *
   * If the compression level is greater than 0, the stack is cut into slice. Each slice is compressed using the gzip
   * algorithm (using the `qCompress` Qt function). In the file, the size of the compressed slice is written as
   * a 4 bytes unsigned integer, followed by the compressed data. A pseudo code to read the compressed data is:
   *
   *     current_size = 0
   *     while current_size < datasz
   *         slice_size = read 4 bytes
   *         compressed_data = read slice_size bytes
   *         data = uncompress compressed_data
   *         store data
   *         current_size += size of data
   *
   * **Note** before version 1.3, if the stack was labeled, the label numbers were store as big endian. Starting 1.3,
   * they are stores as little endian. Stack intensities are always stored as little endian.
   *
   * \ingroup StackProcess
   */
  class mdxBase_EXPORT StackSave : public Process {
  public:
    StackSave(const Process& proc) : Process(proc) 
    {
      setName("Stack/System/Save");
      setDesc("Save a stack into a known 3D image format");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "The file name to save", "");
      addParm("Store", "The store to save", "Main", storeChoice());
      addParm("Stack Id", "The stack to save", "0");
      addParm("Compression", "Compression level (0-9)", "5");
    }
    bool initialize(QWidget* parent);

    bool run();
    /**
     * Save the stack
     * \param stack Stack containing the data
     * \param store Store containing the data
     * \param filename File in which to save the data, the extension must be
     * .mdxs or .inr
     */
    bool run(Stack* stack, Store* store, const QString& filename, int compressionLevel = 0);
  };
  
  /**
   * \class StackExport SystemProcessSave.hpp <SystemProcessSave.hpp>
   *
   * Export the stack data into a stack of images.
   *
   * \ingroup StackProcess
   */
  class mdxBase_EXPORT StackExport : public Process {
    Q_OBJECT
  public:
    StackExport(const Process& proc) : Process(proc) 
    {
      setName("Stack/System/Export");
      setDesc("Export a stack into an image sequence.");
      setIcon(QIcon(":/images/save.png"));

      addParm("Filename", "Name of the export file", "");
      addParm("Store", "Name of the store to export", "Main", storeChoice());
      addParm("Type", "Export format, if not CImg Auto it will be deduced from file extension", 
                                "CImg Auto", QStringList() <<  "DML" << "TIF" << "CIMG" << "CImg Auto");
      addParm("Gen Spacing", "If true, the process will also generate a voxelspacing.txt "
              "file for easy import into VolViewer.", "No", booleanChoice());
      addParm("Stack Id", "The id of the stack to export", "0");
			addParm("Num Digits", "Number of digits to use to generate the file numbering. "
              "If 0, it will use an optimal number of digits.", "0");
    }

    bool initialize(QWidget* parent);

    bool run();
    /**
     * Export the stack
     * \param stack Stack containing the image
     * \param store Store containing the image
     * \param filename Prefix of the file to store the data in
     * \param type Type of the file. Type must be one of 'DML', 'TIF', 'CIMG' or 'CImg Auto'. If 'CImg Auto', the
     * actual type will be deduced by the file extension, and the CImg library will try to save it using its own data
     * formats, Image Magick or Graphics Magick.
     * \param nb_digit Number of digits to use to generate the file numbering. If 0, it will use an optimal number of
     * digits.
     * \param generate_voxel_spacing If true, the process also generate a voxelspacing.txt file for easy import into
     * VolViewer.
     */
    bool run(Stack* stack, Store* store, const QString& filename, const QString& type,
             unsigned int nb_digit, bool generate_voxel_spacing);

  protected slots:
    void selectImageFile();
    void setImageType(const QString& type);

  protected:
    Ui_ExportStackDialog* ui;
    QDialog* dlg;
  };

  /**
   * \class ClearMainStack StackProcess.hpp <StackProcess.hpp>
   *
   * Erase the content of the main stack
   */
  class mdxBase_EXPORT ClearMainStack : public Process 
  {
  public:
    ClearMainStack(const Process& process) : Process(process) 
    {
      setName("Stack/System/Clear Main Stack");
      setDesc("Clear the main stack");
      setIcon(QIcon(":/images/ClearStack.png"));

      addParm("Fill Value", "Value to clear voxels to", "0");
    }
  
    bool run()
    {
      return run(currentStack(), parm("Fill Value").toUInt());
    }
    bool run(Stack* stack, uint fillValue);
  };

  /**
   * \class ClearWorkStack StackProcess.hpp <StackProcess.hpp>
   *
   * Erase the content of the work stack
   */
  class mdxBase_EXPORT ClearWorkStack : public Process 
  {
  public:
    ClearWorkStack(const Process& process) : Process(process) 
    {
      setName("Stack/System/Clear Work Stack");
      setDesc("Clear the work stack");
      setIcon(QIcon(":/images/ClearStack.png"));

      addParm("Fill Value", "Value to clear voxels to", "0");
    }
  
    bool run()
    {
      return run(currentStack(), parm("Fill Value").toUInt());
    }
    bool run(Stack* stack, uint fillValue);
  };

  /**
   * \class SetCurrentStack StackProcessSystem.hpp <StackProcessSystem.hpp>
   *
   * Set the current stack and store. This process is meant to be used when scripting.
   */
  class mdxBase_EXPORT SetCurrentStack : public Process 
  {
  public:
    SetCurrentStack(const Process& process) : Process(process) 
    {
      setName("Stack/System/Set Current Stack");
      setDesc("Change the current stack and mesh. Needed for scripts.");
      setIcon(QIcon(":/images/Relabel.png"));

      addParm("Store", "The store to set", "Main", storeChoice());
      addParm("Stack Id", "The stack to set", "0");
    }
  
    bool run()
    {
      bool is_main = stringToMainStore(parm("Store"));
      int id = parm("Stack Id").toInt();
      return run(is_main, id);
    }
    bool run(bool is_main, int id);
  };
}

#endif
