//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessCreation.hpp>

#include <Progress.hpp>
#include <MeshProcessSystem.hpp>
#include <BoundingBox.hpp>
#include <Geometry.hpp>
#include <Information.hpp>
#include <Polygonizer.hpp>
#include <MeshUtils.hpp>

#include <algorithm>
#include <SetVector.hpp>

#include <unistd.h>
#include "Tie.hpp"

// Number of evals per progress check for marching cubes
const int EVAL_PROGRESS = 10000;

namespace mdx 
{
  int getStackData(const Stack* stack, const Store* store, Point3d &p);
  
  bool ExtrudeFaces::run(CCStructure &cs, CCStructure &csOut, CCIndexDataAttr &indexAttr, double Distance)
  {
    // Create 3D output struture
    csOut = CCStructure(3);

    std::map<CCIndex, CCIndex> v1Map;
    std::map<CCIndex, CCIndex> v2Map;
    std::map<CCIndex, CCIndex> veMap;
    std::map<CCIndex, CCIndex> e1Map;
    std::map<CCIndex, CCIndex> e2Map;
    std::map<CCIndex, CCIndex> fMap;
    std::map<CCIndex, CCIndex> lMap;

    // Loop through all the vertices
    forall(CCIndex v, cs.vertices())
      if(v1Map.find(v) == v1Map.end()) {
        CCIndex u = CCIndexFactory.getIndex();
        indexAttr[u] = indexAttr[v];
        csOut.addCell(u);
        v1Map[v] = u;

        CCIndex w = CCIndexFactory.getIndex();
        CCIndexData &V = indexAttr[v];
        CCIndexData &W = indexAttr[w];
        W = V;
        W.pos -= Distance * normalized(V.nrml);
        csOut.addCell(w);
        v2Map[v] = w;

        CCIndex e = CCIndexFactory.getIndex();
        csOut.addCell(e, +u -w);
        veMap[v] = e;
      }
    // Loop through all the edges
    forall(CCIndex e, cs.edges())
      if(e1Map.find(e) == e1Map.end()) {
        CCIndex a = CCIndexFactory.getIndex();
        std::pair<CCIndex,CCIndex> ep = cs.edgeBounds(e);
        csOut.addCell(a, +v1Map[ep.first] -v1Map[ep.second]);
        e1Map[e] = a;

        CCIndex b = CCIndexFactory.getIndex();
        csOut.addCell(b, -v2Map[ep.first] +v2Map[ep.second]);
        e2Map[e] = b;
      }
    // Loop through all the faces
    forall(CCIndex f, cs.faces()) {
      CCIndex l = CCIndexFactory.getIndex();
      BoundaryChain lB;
      CCIndex g = CCIndexFactory.getIndex();
      BoundaryChain gB;
      CCIndex h = CCIndexFactory.getIndex();
      BoundaryChain hB;
      forall(CCIndex e, cs.bounds(f)) {
        // top face
        ccf::RO ro = cs.ro(f, e);
        gB += ro * e1Map[e];
        // botom face
        hB += ro * e2Map[e];
        if(fMap.find(e) == fMap.end()) {
          // side faces
          CCIndex s = CCIndexFactory.getIndex();
          BoundaryChain sB;
          sB += ro * -e1Map[e];
          sB += ro * -e2Map[e];
          std::pair<CCIndex,CCIndex> ep = cs.edgeBounds(e);
          sB += ro * +veMap[ep.first]; 
          sB += ro * -veMap[ep.second]; 
          indexAttr[s] = indexAttr[f];
          csOut.addCell(s, sB);
          fMap[e] = s;
          lB += +s;
        } else
          lB += -fMap[e];
      }
      indexAttr[g] = indexAttr[f];
      indexAttr[h] = indexAttr[f];
      csOut.addCell(g, gB);
      csOut.addCell(h, hB);
      lB += +g;
      lB += +h;
      csOut.addCell(l, lB);
      (indexAttr[l]).label = (indexAttr[f]).label;
    }

    mdxInfo << QString("Extruded mesh has %1 vertices, %2 edges, %3 faces and %4 volumes")
        .arg(csOut.vertices().size()).arg(csOut.edges().size()).arg(csOut.faces().size()).arg(csOut.volumes().size()) << endl;


    return true;
  }
  REGISTER_PROCESS(ExtrudeFaces);

  bool ExtrudeTrianglesParallel::initialize(QWidget *parent)
  {
    Mesh *mesh = currentMesh();

    ccName = parm("Complex Name");
    if(ccName.isEmpty())
      ccName = mesh->ccName();
    if(ccName.isEmpty()) 
      throw(QString("No current cell complex"));

    ccNameOut = parm("Complex Name Out");
    if(ccNameOut.isEmpty())
      ccNameOut = ccName + "Extruded";

    cs = &mesh->ccStructure(ccName);
    if(cs->faces().size() == 0) 
      throw(QString("Cell complex must have faces"));

    Distance = parm("Distance").toDouble();
    Scaling = parm("Scaling").toDouble();

    return true;
  }

  bool ExtrudeTrianglesParallel::run()
  {
    // Create 3D output struture
    CCStructure *co =  &mesh->ccStructure(ccNameOut);
    *co = CCStructure(3);

    CCIndexDataAttr &indexAttr = mesh->indexAttr();

    std::map<CCIndex, CCIndex> v1Map;
    std::map<CCIndex, CCIndex> v2Map;
    std::map<CCIndex, CCIndex> veMap;
    std::map<CCIndex, CCIndex> e1Map;
    std::map<CCIndex, CCIndex> e2Map;
    std::map<CCIndex, CCIndex> fMap;
    std::map<CCIndex, CCIndex> lMap;

    Point3d selVtx(0,0,0);
    Point3d selNrml(0,0,0);

    // Find Pos of Selected vertex
    forall(CCIndex v, cs->vertices()){
      CCIndexData &V = indexAttr[v];
      if(V.selected){
        selVtx = V.pos;
        selNrml = normalized(V.nrml);
      } 
    }

    // Loop through all the vertices
    forall(CCIndex v, cs->vertices())
      if(v1Map.find(v) == v1Map.end()) {
        CCIndex u = CCIndexFactory.getIndex();
        indexAttr[u] = indexAttr[v];
        co->addCell(u);
        v1Map[v] = u;

        CCIndex w = CCIndexFactory.getIndex();
        CCIndexData &V = indexAttr[v];
        CCIndexData &W = indexAttr[w];
        W = V;
        W.pos = W.pos - (Distance * selNrml);
        W.pos = ((W.pos-selVtx) * Scaling) + selVtx;
        co->addCell(w);
        v2Map[v] = w;

        CCIndex e = CCIndexFactory.getIndex();
        co->addCell(e, +u -w);
        veMap[v] = e;
      }
    // Loop through all the edges
    forall(CCIndex e, cs->edges())
      if(e1Map.find(e) == e1Map.end()) {
        CCIndex a = CCIndexFactory.getIndex();
        std::pair<CCIndex,CCIndex> ep = cs->edgeBounds(e);
        co->addCell(a, +v1Map[ep.first] -v1Map[ep.second]);
        e1Map[e] = a;

        CCIndex b = CCIndexFactory.getIndex();
        co->addCell(b, -v2Map[ep.first] +v2Map[ep.second]);
        e2Map[e] = b;
      }
    // Loop through all the faces
    forall(CCIndex f, cs->faces()) {
      CCIndex l = CCIndexFactory.getIndex();
      BoundaryChain lB;
      CCIndex g = CCIndexFactory.getIndex();
      BoundaryChain gB;
      CCIndex h = CCIndexFactory.getIndex();
      BoundaryChain hB;
      forall(CCIndex e, cs->bounds(f)) {
        // top face
        ccf::RO ro = cs->ro(f, e);
        gB += ro * e1Map[e];
        // botom face
        hB += ro * e2Map[e];
        if(fMap.find(e) == fMap.end()) {
          // side faces
          CCIndex s = CCIndexFactory.getIndex();
          BoundaryChain sB;
          sB += ro * -e1Map[e];
          sB += ro * -e2Map[e];
          std::pair<CCIndex,CCIndex> ep = cs->edgeBounds(e);
          sB += ro * +veMap[ep.first]; 
          sB += ro * -veMap[ep.second]; 
          indexAttr[s] = indexAttr[f];
          co->addCell(s, sB);
          fMap[e] = s;
          lB += +s;
        } else
          lB += -fMap[e];
      }
      indexAttr[g] = indexAttr[f];
      indexAttr[h] = indexAttr[f];
      co->addCell(g, gB);
      co->addCell(h, hB);
      lB += +g;
      lB += +h;
      co->addCell(l, lB);
      indexAttr[l].label = indexAttr[f].label;
    }

    mdxInfo << QString("Extruded mesh has %1 vertices, %2 edges, %3 faces and %4 volumes")
        .arg(co->vertices().size()).arg(co->edges().size()).arg(co->faces().size()).arg(co->volumes().size()) << endl;

    CCDrawParms *cdp = &mesh->drawParms(ccName);
    cdp->setAllGroupsVisible(false);

    cdp = &mesh->drawParms(ccNameOut);
    cdp->setAllGroupsVisible(true);

    mesh->updateAll(ccNameOut);
    mesh->setCCName(ccNameOut);

    return true;
  }
  REGISTER_PROCESS(ExtrudeTrianglesParallel);

  bool evalCheckProgress()
  {
    static int count = 0;
    if(count++ >= EVAL_PROGRESS) {
      count = 0;
      if(!progressAdvance(0))
        return false;
    }
    return true;
  }
  
//  template <size_t n> void makeNhbd(vvGraph& S, Vector<n, vertex> V)
//  {
//    S.insertEdge(V[0], V[1]);
//    for(size_t i = 2; i < n; ++i)
//      S.spliceAfter(V[0], V[i - 1], V[i]);
//  }
//  
  // 3D surface evaluation function for marching cubes
  int getStackData(const Stack* stack, const Store* store, Point3d &p)
  {
    Point3d pf = stack->worldToImaged(p);
    // Be careful of double->int conversion when we are below 0
    for(int i = 0; i < 3; i++)
      if(pf[i] < 0)
        pf[i] -= 1;
    int x = int(pf.x());
    int y = int(pf.y());
    int z = int(pf.z());
  
    // Decide which stack to use
    const HVecUS& data = store->data();
    if(!stack->boundsOK(x, y, z))
      return (0);
    else
      return (data[stack->offset(x, y, z)]);
  }
  
  // Implicit surface evaluator functions, return true if inside
  bool MarchingCubesSurface::eval(Point3d p)
  {
    if(!evalCheckProgress())
      userCancel();
    int d = getStackData(_stack, _store, p);
    if(d >= _threshold)
      return true;
    else
      return false;
  }
  
  bool MarchingCubesSurface::run(Mesh &mesh, const Store &store, const QString &ccName, double cubeSize, int threshold)
  {
    // Store threshold for eval
    if(threshold <= 0)
      threshold = 1;
    _threshold = threshold;
  
    // Setup progress bar
    progressStart(QString("Marching Cubes Surface on Stack %1").arg(mesh.userId()), 0);
  
    // Clear mesh
    mesh.reset();
  
    // Find bounding box
    _store = &store;
    _stack = store.stack();
    const HVecUS& data = store.data();
    Point3u size = _stack->size();
  
    // Count pixels and create bounding region(s)
    long pixCount = 0;
    BoundingBox3u bBox;
    progressStart(QString("Finding object bounds-%1").arg(mesh.userId()), size.z());
    const ushort* pdata = data.data();
    int i = 0;
    for(uint z = 0; z < size.z(); z++) {
      if(!progressAdvance(z))
        userCancel();
      for(uint y = 0; y < size.y(); y++) {
        for(uint x = 0; x < size.x(); x++, ++pdata, ++i) {
          // For normal march only count "inside" pixels
          if(*pdata < threshold)
            continue;
          pixCount++;
  
          // Find bounding box
          if(pixCount == 1) {
            bBox = BoundingBox3u(Point3u(x, y, z));
          } else {
            bBox |= Point3u(x, y, z);
          }
        }
      }
    }
  
    // Polygonizer object
    std::vector<Point3d> vertList;
    std::vector<Point3u> triList;
    Polygonizer pol(*this, cubeSize, _stack->step(), vertList, triList);
  
    // Get bounding box in world coordinates
    BoundingBox3d bBoxWorld = _stack->imageToWorld(bBox);
  
    mdxInfo << "Computing segmentation for threshold " << threshold << " with bBox of: " << bBoxWorld[0] 
      << " - " << bBoxWorld[1] << " cube size:" << cubeSize << " pixel count:" << pixCount;
  
    // Call polygonizer, catch any errors
    triList.clear();
    Point3d marchBBox[2] = { bBoxWorld.pmin(), bBoxWorld.pmax() };
    pol.march(marchBBox);
  
    // Create vertices
    auto &indexAttr = mesh.indexAttr();
    CCIndexVec vertices(vertList.size());
    #pragma omp parallel for
    for(uint i = 0; i < vertList.size(); ++i) {
      auto v = CCIndexFactory.getIndex();
      auto &vIdx = indexAttr[v];
      vIdx.pos = Point3d(vertList[i]);
      vertices[i] = v;
    }
  
    // Initialize cell complex
    CCStructure &cs = mesh.ccStructure(ccName);
    cs = CCStructure(2);
    ccFromTriangles(cs, vertices, triList);

    // Give some initial signal
    CCIndexDoubleAttr &signalAttr = mesh.signalAttr<double>("Signal");
    #pragma omp parallel for
    for(uint i = 0; i < cs.vertices().size(); i++)
      signalAttr[cs.vertices()[i]] = 0.5;
    faceAttrFromVertices(cs, signalAttr, cs.faces());
    mesh.setCCName(ccName);
    mesh.setSignalBounds(Point2d(0.0, 1.0));
  
    setStatus(QString("Triangles extracted:%1, vertices:%2").arg(triList.size()).arg(vertices.size()));

    return true;
  }
  REGISTER_PROCESS(MarchingCubesSurface);
  
  bool CuttingSurfMesh::run(Mesh &mesh, CuttingSurface &cutSurf, const QString &ccName)
  {
    Point3dVec points;
    int uSize = 0, vSize = 0;
    cutSurf.getSurfPoints(&mesh.stack()->frame(), points, uSize, vSize);
    if(points.empty() or uSize < 2 or vSize < 2)
      return true;
  
    // Create a new cell complex of dimension 2
    CCStructure &cs = mesh.ccStructure(ccName);
    cs = CCStructure(2);

    auto &indexAttr = mesh.indexAttr();
  
    CCIndexVec vertices(uSize * vSize);
    Point3uVec triangles((uSize-1) * (vSize-1) * 2);

    // Create the vertices and fill in the position
    auto  &signalAttr = mesh.signalAttr<double>("Signal");
    for(int u = 0; u < uSize; u++) {
      for(int v = 0; v < vSize; v++) {
        CCIndex c = CCIndexFactory.getIndex();
        auto &cIdx = indexAttr[c];
        cIdx.pos = Point3d(points[u * vSize + v]);
        signalAttr[c] = 1.0;
        vertices[u*vSize + v] = c;

        // Create the triangles
        if(u >= uSize-1 or v >= vSize-1)
          continue;
        triangles[u*(vSize-1) + v] = Point3u(u*vSize+v, (u+1)*vSize+v+1, u*vSize+v+1); 
        triangles[(vSize-1)*(uSize-1) + u*(vSize-1) + v] = Point3u(u*vSize+v, (u+1)*vSize+v, (u+1)*vSize+v+1); 
      }
    }
    ccFromTriangles(cs, vertices, triangles);
    faceAttrFromVertices(cs, signalAttr, cs.faces());

    mesh.setSignalBounds(Point2d(0.0, 1.0), "Signal");

    return true;
  }
  REGISTER_PROCESS(CuttingSurfMesh);
//
//  bool VoxelFaceMesh::run(Stack *stack, Store *store, Mesh *mesh)
//  {
//    // Create a new graph
//    vvGraph S;
//
//    // Map of triangle lists
//    typedef Vector<3, ulong> Point3ul;
//    typedef std::vector<Point3ul> TriVec;
//    std::map<int, TriVec> cellTris;
//
//    Point3u size = stack->size() + Point3u(2, 2, 2);
//    HVecUS data(size.x() * size.y() * size.z());
//
//    // Make a padded copy of the data
//    for(size_t i = 0; i < data.size(); ++i)
//      data[i] = 0;
//
//    for(uint z = 1; z < size.z() - 1; ++z)
//      for(uint y = 1; y < size.y() - 1; ++y)
//        for(uint x = 1; x < size.x() - 1; ++x)
//          data[getOffset(x, y, z, size)] = store->data()[getOffset(x-1, y-1, z-1, stack->size())];
//
//    // Loop through the voxels
//    for(uint z = 1; z < size.z() - 1; ++z)
//      for(uint y = 1; y < size.y() - 1; ++y)
//        for(uint x = 1; x < size.x() - 1; ++x) {
//          ulong off = getOffset(x, y, z, size);
//          int label = data[off];
//          // Skip background
//          if(label == 0)
//            continue;
//
//          // Offsets for neighbors
//          ulong offx = getOffset(x-1, y, z, size);
//          ulong offX = getOffset(x+1, y, z, size);
//          ulong offy = getOffset(x, y-1, z, size);
//          ulong offY = getOffset(x, y+1, z, size);
//          ulong offz = getOffset(x, y, z-1, size);
//          ulong offZ = getOffset(x, y, z+1, size);
//
//          ulong offXY = getOffset(x+1, y+1, z, size);
//          ulong offXZ = getOffset(x+1, y, z+1, size);
//          ulong offYZ = getOffset(x, y+1, z+1, size);
//          ulong offXYZ = getOffset(x+1, y+1, z+1, size);
//
//          // Check all neighbors
//          if(label != data[offx] and y < size.y() - 1 and z < size.z() - 1) {
//            cellTris[label].push_back(Point3ul(off, offZ, offYZ));
//            cellTris[label].push_back(Point3ul(off, offYZ, offY));
//          }
//          if(label != data[offX]) {
//            cellTris[label].push_back(Point3ul(offX, offXYZ, offXZ));
//            cellTris[label].push_back(Point3ul(offX, offXY, offXYZ));
//          }
//          if(label != data[offy]) {
//            cellTris[label].push_back(Point3ul(off, offXZ, offZ));
//            cellTris[label].push_back(Point3ul(off, offX, offXZ));
//          }
//          if(label != data[offY]) {
//            cellTris[label].push_back(Point3ul(offY, offYZ, offXYZ));
//            cellTris[label].push_back(Point3ul(offY, offXYZ, offXY));
//          }
//          if(label != data[offz]) {
//            cellTris[label].push_back(Point3ul(off, offXY, offX));
//            cellTris[label].push_back(Point3ul(off, offY, offXY));
//          }
//          if(label != data[offZ]) {
//            cellTris[label].push_back(Point3ul(offZ, offXZ, offXYZ));
//            cellTris[label].push_back(Point3ul(offZ, offXYZ, offYZ));
//          }
//        }
//
//    // Loop though triangle lists and create cells
//    typedef std::pair<int, TriVec> IntTriPair;
//    forall(const IntTriPair &pr, cellTris) {
//      // Create vertices
//      std::map<ulong, vertex> vMap;
//      for(uint i = 0; i < pr.second.size(); ++i) {
//        for(int j = 0; j < 3; ++j) {
//          ulong off = pr.second[i][j];
//          if(vMap.count(off) == 0) {
//            vertex v;
//            v->label = pr.first;
//            Point3u img(off % size.x(), off / size.x() % size.y(), off / size.x() / size.y());
//            v->pos = Point3d(stack->imageToWorld(img) - stack->step() * (3.0/2.0));
//            vMap[off] = v;
//          }
//        }
//      }
//      // Create vertex list
//      std::vector<vertex> vVec;
//      typedef std::pair<ulong, vertex> ULongVtxPair;
//      ulong saveId = 0;
//      forall(const ULongVtxPair &pr, vMap) {
//        pr.second->saveId = saveId++;
//        vVec.push_back(pr.second);
//      }
//
//      // Create triangle list
//      std::vector<Point3u> tVec;
//      for(uint i = 0; i < pr.second.size(); ++i)
//        tVec.push_back(Point3u(vMap[pr.second[i].x()]->saveId, vMap[pr.second[i].y()]->saveId, vMap[pr.second[i].z()]->saveId));
//
//      if(!Mesh::meshFromTriangles(S, vVec, tVec))
//        for(uint i = 0; i < vVec.size(); i++)
//          vVec[i]->selected = true;
//
//      Mesh::meshFromTriangles(S, vVec, tVec);
//    }
//  
//    // Update the graph
//    mesh->graph().swap(S);
//
//    SETSTATUS("Mesh " << mesh->userId() << " created with " << mesh->graph().size() << " vertices.");
//  
//    mesh->setMeshType("MDXM");
//    mesh->signalBounds() = Point2f(0.0, 1.0);
//    mesh->updateAll();
//
//    return true;
//  }
//  REGISTER_PROCESS(VoxelFaceMesh); 
}
