//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CellTissue.hpp>
#include <Geometry.hpp>
#include <MeshUtils.hpp>

namespace mdx
{
  bool CellTissue::processTissueParms(const TissueParms &tissueParms)
  {
    cellDim = tissueParms.parm("Dimension").toInt();
    TissueName = tissueParms.parm("Tissue");
    TissueDualName = tissueParms.parm("Tissue Dual");

    return true;
  }

  bool CellTissue::initialize(CCStructure *cellStructure, CCStructure *dualGraph, CCIndexDataAttr *indexAttr)
  {
    if(!cellStructure)
      throw(QString("CellTissue::initialize Cell Structure null"));
    if(!dualGraph)
      throw(QString("CellTissue::initialize Dual Graph null"));
    if(!indexAttr)
      throw(QString("CellTissue::initialize Index Data null"));
    if(cellDim < 2)
      throw(QString("CellTissue::initialize Invalid cell dimension %1").arg(cellDim));

    cs = cellStructure;
    dg = dualGraph;
    _indexAttr = indexAttr;

    return true;
  }

  // Add an existing cell to a cell tissue.
  bool CellTissue::addCell(CCIndex cell)
  {
    if(cs->maxDimension() != cellDim)
      throw(QString("CellTissue::addCell: Tissue dimension (%1) and cell complex dimension (%2) must match").
            arg(cellDim).arg(cs->maxDimension()));

    // The cell is already in cs; we add it to the dual graph
    if(!dg->addCell(cell))
      throw(QString("addCell: Unable to add vertex in dual"));

    // Connect to neighbors
    for(CCStructure::FlipI flip : cs->matchV(CCIndex::Q,cell,CCIndex::Q,CCIndex::TOP)) {
      CCIndex nb = flip.otherFacet(cell);
      if(nb != CCIndex::INFTY && edgeBetween(*dg,cell,nb) == CCIndex::UNDEF) {
        CCIndex ne = CCIndexFactory.getIndex();
        dg->addCell(ne, +cell -nb);
      }
    }

    return true;
  }

  // Remove a cell from the cell graph.
  bool CellTissue::removeCell(CCIndex cell)
  {
    // Get all bounding cells
    std::vector< std::set<CCIndex> > bounds(cellDim);
    for(uint d = 0 ; d < cellDim ; d++)
      bounds[d] = cs->incidentCells(cell,d);

    // Remove the cell itself
    cs->deleteCell(cell);

    // Remove its bounds in order, as long as there are no other existing cobounds
    for(uint d1 = cellDim ; d1 > 0 ; d1--)
    {
      for(CCIndex b : bounds[d1 - 1])
        if(cs->cobounds(b).empty())
          cs->deleteCell(b);
    }

    // Remove from dual graph
    // Incident edges first
    for(CCIndex edge : dg->cobounds(cell))
      dg->deleteCell(edge);
    // The cell itself
    dg->deleteCell(cell);

    return true;
  }

  // Update the geometric parameters: measures and centroids.
  void CellTissue::updateGeometry(void)
  {
    // We have to update the geometrical information of
    // _cells_ and the _boundaries_ of cells.
    // Uses cellCentroidData (from CCUtils) apart from special
    // cases for edges and faces (using updateFaceGeometry from CCUtils)
    for(uint d = cellDim - 1 ; d <= cellDim ; d++) {
      if(d == 1) {
        auto &edges = cs->edges();
        #pragma omp parallel for
        for(uint i = 0 ; i < edges.size() ; i++) {
          auto eb = cs->edgeBounds(edges[i]);
          auto &eIdx = (*_indexAttr)[edges[i]];
          Point3d &p1 = (*_indexAttr)[eb.first].pos;
          Point3d &p2 = (*_indexAttr)[eb.second].pos;
          eIdx.measure = norm(p1 - p2);
          eIdx.pos = (p1 - p2)/2.0;
        }
      } else if(d == 2)
        updateFaceGeometry(*cs, *_indexAttr);
      else {
        const std::vector<CCIndex> &cells = cs->cellsOfDimension(d);
        #pragma omp parallel for
        for(uint i = 0 ; i < cells.size() ; i++) {
          CentroidData<Point3d> ccd = cellCentroidData(cells[i],*cs,*_indexAttr);
          CCIndexData &idata = (*_indexAttr)[cells[i]];
          idata.measure = ccd.measure;
          idata.pos = ccd.centroid;
        }
      }
    }

    // Now we update the cell walls with the total of the measures of the included boundaries.
    const CCIndexVec &walls = dg->edges();
    #pragma omp parallel for
    for(uint i = 0; i < walls.size(); i++) {
      CCIndex w = walls[i];
      CCIndexData &wIdx = (*_indexAttr)[w];
      wIdx.measure = 0;

      std::pair<CCIndex,CCIndex> ep = dg->edgeBounds(w);
      for(CCStructure::FlipI flip : cs->matchV(CCIndex::Q,ep.first,ep.second,CCIndex::TOP))
        wIdx.measure += (*_indexAttr)[flip.joint].measure;
    }
  }

  // Create dual graph.
  bool CellTissue::createDual()
  {
    dg->clear();

    // Add vertices (dual to cells)
    for(CCIndex c : cs->cellsOfDimension(cellDim))
      dg->addCell(c);

    // Add in edges between neighbouring cells
    for(CCIndex c : cs->cellsOfDimension(cellDim)) {
      for(CCStructure::FlipI flip : cs->matchV(CCIndex::Q,c,CCIndex::Q,CCIndex::TOP)) {
        CCIndex n = flip.otherFacet(c);
        if(n != CCIndex::INFTY && edgeBetween(*dg,c,n) == CCIndex::UNDEF) {
          CCIndex ne = CCIndexFactory.getIndex();
          dg->addCell(ne, +c -n);
        }
      }
    }

    return true;
  }

  // Clear the cell tissue
  void CellTissue::clear()
  {
    if(cs)
      cs->clear();
    if(dg)
      dg->clear();
  }

  // Subdivider for cell tissues.
  void CellTissue::CellTissueSubdivide::splitCellUpdate(Dimension dim, const CCStructure &cs,
                                                        const CCStructure::SplitStruct &ss, 
                                                        CCIndex,CCIndex,double)
  {
    if(&cs == &tissue->cellStructure()) {
      CCIndexDataAttr &indexAttr = tissue->indexAttr();
      CCStructure &dg = tissue->dualGraph();

      // Splitting a bounding cell; this requires updating wall measures.
      if(dim == tissue->cellDimension() - 1) {
        // Calculate measures of new boundary cells.
        double oldMeasure = indexAttr[ss.parent].measure, newMeasure = 0;
        if(dim == 2) {
          updateFaceGeometry(cs,indexAttr,ss.childP);
          updateFaceGeometry(cs,indexAttr,ss.childN);
          newMeasure = indexAttr[ss.childP].measure + indexAttr[ss.childN].measure;
        } else {
          for(uint i = 0 ; i < 2 ; i++)
          {
            CCIndex child = i == 0 ? ss.childP : ss.childN;

            CentroidData<Point3d> ccd = cellCentroidData(child,cs,indexAttr);
            CCIndexData &idata = indexAttr[child];
            idata.measure = ccd.measure;
            idata.pos = ccd.centroid;

            newMeasure += ccd.measure;
          }
        }

        // Update wall measure, if there is a wall.
        std::set<CCIndex> cob = cs.cobounds(ss.childP);
        if(cob.size() == 2)
        {
          CCIndex wall = edgeBetween(dg,*(cob.begin()),*(++cob.begin()));
          if(wall != CCIndex::UNDEF)
            indexAttr[wall].measure += newMeasure - oldMeasure;
        }
      }

      // Splitting a cell; this requires splitting the cell in the dual graph.
      else if(dim == tissue->cellDimension()) {
        if(!dg.hasCell(ss.parent))
          return;

        // Add the new cells to the dual graph.
        dg.addCell(ss.childP);
        dg.addCell(ss.childN);

        // Measure and add the wall corresponding to the membrane.
        if(dim-1 == 2) {
          updateFaceGeometry(cs,indexAttr,ss.membrane);
        } else {
          CentroidData<Point3d> ccd = cellCentroidData(ss.membrane,cs,indexAttr);
          CCIndexData &idata = indexAttr[ss.membrane];
          idata.measure = ccd.measure;
          idata.pos = ccd.centroid;
        }
        CCIndex membraneWall = CCIndexFactory.getIndex();
        dg.addCell(membraneWall , +ss.childP -ss.childN);
        indexAttr[membraneWall].measure = indexAttr[ss.membrane].measure;

        // Shift the edges from the parent cell to the child cells.
        std::vector<CCStructure::FlipI> nbFlips = neighborVertexFlips(dg,ss.parent);
        for(const CCStructure::FlipI& flip : nbFlips)
        {
          CCIndex nb = flip.otherFacet(ss.parent);
          if(nb == CCIndex::INFTY) continue;

          CCIndex oldEdge = flip.interior;
          ccf::RO edgeOrnt = flip.facet[0] == nb ? ccf::NEG : ccf::POS;

          dg.deleteCell(oldEdge);

          // What we do depends on which of the child cells are adjacent to nb
          CCIndex bdP = ~orientedMembrane(cs,ss.childP,nb), bdN = ~orientedMembrane(cs,ss.childN,nb);
          if(bdP == CCIndex::UNDEF && bdN == CCIndex::UNDEF)
            mdxWarning << "After subdivide, child cells are not adjacent to former neighbour" << nb << endl;
          else if(bdP != CCIndex::UNDEF && bdN == CCIndex::UNDEF) {
            // Only childP is adjacent to nb, reuse the old edge
            dg.addCell(oldEdge , edgeOrnt * (+ss.childP -nb));
          }
          else if(bdP == CCIndex::UNDEF && bdN != CCIndex::UNDEF) {
            // Only childN is adjacent to nb, as above mm.
            dg.addCell(oldEdge , edgeOrnt * (+ss.childN -nb));
          }
          else if(bdP != CCIndex::UNDEF && bdN != CCIndex::UNDEF) {
            // Both are adjacent; add two new edges and recompute measure.
            for(uint i = 0 ; i < 2 ; i++)
            {
              CCIndex child = i == 0 ? ss.childP : ss.childN;
              CCIndex wall = CCIndexFactory.getIndex();
              dg.addCell(wall, edgeOrnt * (+child -nb));
              CCIndexData &wData = indexAttr[wall];
              wData.measure = 0;

              for(CCStructure::FlipI flip : cs.matchV(CCIndex::Q,child,nb,CCIndex::TOP)) {
                wData.measure += indexAttr[flip.joint].measure;
              }
            }
          }
        }

        // Compute measures + centroids for the child cells.
        if(dim == 2) {
          updateFaceGeometry(cs,indexAttr,ss.childP);
          updateFaceGeometry(cs,indexAttr,ss.childN);
        } else {
          for(uint i = 0 ; i < 2 ; i++)
          {
            CCIndex child = i == 0 ? ss.childP : ss.childN;

            CentroidData<Point3d> ccd = cellCentroidData(child,cs,indexAttr);
            CCIndexData &idata = indexAttr[child];
            idata.measure = ccd.measure;
            idata.pos = ccd.centroid;
          }
        }
      }
    }
  }
}
