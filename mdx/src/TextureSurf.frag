// Fragment shader that uses 2D surface texture (i.e. keyence)
//
uniform sampler2D tex;
uniform float opacity;
uniform float brightness;
varying vec2 texCoord;

void setColor()
{
  vec4 color = texture2D(tex, texCoord);
  color = light(color);
  gl_FragColor = brightness*color;
  gl_FragColor.a *= opacity;
}

