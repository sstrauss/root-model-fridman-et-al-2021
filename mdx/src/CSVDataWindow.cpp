//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <Information.hpp>
#include <CSVDataWindow.hpp>

#include <QtGui>
#include <QMessageBox>
#include "ui_CSVDataWindow.h"

namespace mdx
{
  CSVDataWindow::CSVDataWindow(QWidget *parent) : QMainWindow(parent), csvDataModel(this), ui(new Ui::csvDataWindow)
  {
    ui->setupUi(this);

    // Connect model to table view:
    ui->csvDataView->setModel(&csvDataModel);

    // Make table header visible and display table:
    ui->csvDataView->horizontalHeader()->setVisible(true);
    ui->csvDataView->show();
    connect(ui->csvDataView, &QTableView::customContextMenuRequested, this, &CSVDataWindow::menuRequested);
    connect(ui->csvButtons, &QDialogButtonBox::clicked, this, &CSVDataWindow::buttonClicked);
  }
  CSVDataWindow::~CSVDataWindow()
  {
    if(ui)
      delete ui;
  }

  void CSVDataWindow::closeEvent(QCloseEvent *event)
  {
    if(csvDataModel.changed) {
      QMessageBox::StandardButton reply = 
            QMessageBox::question(this, "Data Changed", "Would you like to save you changes?", QMessageBox::Yes | QMessageBox::No);
        if(reply == QMessageBox::Yes)
          *csvDataModel.csvDataOrig = csvDataModel.csvData;
    }
  }

  void CSVDataWindow::menuRequested(QPoint pos)
  {
    QMenu *menu=new QMenu(this);
    menu->addAction(new QAction("Select Labels", this));
    menu->addAction(new QAction("Clear Labels", this));
    menu->popup(ui->csvDataView->viewport()->mapToGlobal(pos));
    connect(menu, &QMenu::triggered, this, &CSVDataWindow::menuTriggered);
  }

  void CSVDataWindow::menuTriggered(QAction *action)
  {
    IntVec labels;
    if(!action)
      return;
    if(action->text() == "Clear Labels") {
      emit selectLabels(labels);
      return;
    }

    uint labelCol = csvDataModel.csvData.column("Label");
    if(labelCol == csvDataModel.csvData.columns())
      return;

    if(action->text() == "Select Labels") {
      QModelIndexList indexList = ui->csvDataView->selectionModel()->selectedIndexes();
      for(QModelIndex index : indexList) {
        if(int(csvDataModel.csvData.rows()) > index.row()) {
          bool ok = false;
          int label = csvDataModel.csvData[index.row()][labelCol].toInt(&ok);
          if(ok and label > 0)
            labels.push_back(label);
        }
      }
      if(labels.size() > 0)
        emit selectLabels(labels);
    }
  }

  void CSVDataWindow::buttonClicked(QAbstractButton *button)
  {
    if(!button)
      return;
    if(button->text() == "&Save") {
      *csvDataModel.csvDataOrig = csvDataModel.csvData;
      csvDataModel.changed = false;
    } else if(button->text() == "&Cancel") {
      csvDataModel.changed = false;
      close();
    } else if(button->text() == "&OK")
      close();
  }

  CSVDataModel::CSVDataModel(QObject *parent) : QAbstractTableModel(parent)
  {
  }

  Qt::ItemFlags CSVDataModel::flags(const QModelIndex &index) const
  {
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
  }

  bool CSVDataModel::setData(const QModelIndex &index, const QVariant &value, int role)
  {
    if(index.isValid() && role == Qt::EditRole) {
      if(int(csvData.columns()) > index.column() and int(csvData.rows()) > index.row()) {
        if(value.toString() != csvData[index.row()][index.column()]) {
          csvData.setValue(index.row(), index.column(), value.toString());
          changed = true;
          emit dataChanged(index, index);
        }
        return true;
      }
    }
    return false;
  }

  bool CSVDataModel::setCSVData(CSVData &data) 
  { 
    beginResetModel();
    csvData = data; 
    csvDataOrig = &data;
    endResetModel();
    return true;
  }

  int CSVDataModel::rowCount(const QModelIndex &parent) const
  {
    Q_UNUSED(parent);
    return csvData.rows();
  }

  int CSVDataModel::columnCount(const QModelIndex &parent) const
  {
    Q_UNUSED(parent);
    return csvData.columns();
  }

  QVariant CSVDataModel::data(const QModelIndex &index, int role) const
  {
    if(!index.isValid() or role != Qt::DisplayRole)
      return QVariant();
    if(int(csvData.columns()) > index.column() and int(csvData.rows()) > index.row())
      return csvData[index.row()][index.column()];

    return QVariant();
  }

  QVariant CSVDataModel::headerData(int column, Qt::Orientation orientation, int role) const
  {
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal) {
     if(int(csvData.columns()) > column)
       return csvData.columnName(column);
    }
    return QVariant();
  }
}
