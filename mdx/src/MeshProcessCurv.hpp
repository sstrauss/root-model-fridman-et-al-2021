//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TISSUECURVATURE_HPP
#define TISSUECURVATURE_HPP

#include <Process.hpp>

#include <MeshProcessCellMesh.hpp>

namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{
  /**
   * \class TissueCurvature ProcessCurv.hpp <MeshProcessCurv.hpp>
   *
   * Compute the curvature tensor for each cell of a cell mesh. If the tissue
   * is a full mesh, it will first be simplified into a cell mesh.
   * */
  class TissueCurvature : public Process {
  public:
    TissueCurvature(const Process& process) : Process(process) {}
  
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      bool ok;
      float neighborhood = parms[0].toFloat(&ok);
      if(not ok)
        return setErrorMessage("Error, parameter 'Radius' must be a number");
      return run(mesh, neighborhood, stringToBool(parms[1]));
    }
  
    bool run(Mesh* mesh, float neighborhood, bool checkLabel);
  
    QString name() const { return "Mesh/Cell Axis/Curvature/Compute Tissue Curvature"; }
    QString description() const {
      return "Compute curvature based on simplified mesh (from Make Cells) "
             "for a neighborhood of given radius."; }
    QStringList parmNames() const {
      return QStringList() << QString("Radius (%1)").arg(UM) << "Selected cell"; }
    QStringList parmDescs() const {
      return QStringList() << QString("Radius (%1)").arg(UM) << "Selected cell"; }
    QStringList parmDefaults() const { return QStringList() << "10.0" << "No"; }
    ParmChoiceMap parmChoice() const
    {
      ParmChoiceMap map;
      map[1] = booleanChoice();
      return map;
    }
    QIcon icon() const { return QIcon(":/images/Curvature.png"); }
  };
  
  /**
   * \class DisplayTissueCurvature ProcessCurv.hpp <MeshProcessCurv.hpp>
   *
   * Once the curvature tensor has been computed, this process allows to change
   * how it is displayed.
   */
  class DisplayTissueCurvature : public Process {
  public:
    DisplayTissueCurvature(const Process& process) : Process(process) {}
  
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
  
      bool ok;
      float heatmapPercentile = parms[2].toFloat(&ok);
      if(not ok)
        return setErrorMessage("Error, argument 'Heatmap percentile' must be a number");
      float axisLineWidth = parms[6].toFloat(&ok);
      if(not ok)
        return setErrorMessage("Error, argument 'Line Width' must be a number");
      float axisLineScale = parms[7].toFloat(&ok);
      if(not ok)
        return setErrorMessage("Error, argument 'Line Scale' must be a number");
      float axisOffset = parms[8].toFloat(&ok);
      if(not ok)
        return setErrorMessage("Error, argument 'Line Offset' must be a number");
      float threshold = parms[9].toFloat(&ok);
      if(not ok)
        return setErrorMessage("Error, argument 'Threshold' must be a number");
  
      return run(mesh, parms[0], stringToBool(parms[1]), heatmapPercentile, 
                 parms[3], QColor(parms[4]), QColor(parms[5]), axisLineWidth, axisLineScale, 
                                                                      axisOffset, threshold);
    }
  
    bool run(Mesh* mesh, QString DisplayHeatMap, bool compareZero, float heatmapPercentile, 
                 QString DisplayCurv, const QColor& ColorExpansion, const QColor& ColorShrinkage, 
                 float AxisLineWidth, float AxisLineScale, float AxisOffset, float CurvThreshold);
  
    QString name() const { return "Mesh/Cell Axis/Curvature/Display Tissue Curvature"; }
    QString description() const { return "Display curvature based on cellular mesh"; }
    QStringList parmNames() const {
      return QStringList() << "Heatmap" << "Compare to 0" << "Heatmap percentile"
        << "Show Axis" << "Color +" << "Color -" << "Line Width" << "Line Scale"
        << "Line Offset" << "Threshold"; }
  
    QStringList parmDescs() const { return QStringList() 
      << "Display curvature values in max or min direction as a color map.\n"
         "Average: (CurvMax + CurvMin)/2,  SignedAverageAbs: sign(CurvMax or CurvMin) x "
         "(abs(CurvMax) + abs(CurvMin))/2, \n"
         "Gaussian:(CurvMax x CurvMin), RootSumSquare: sqrt(CurvMax^2 + CurvMin^2), Anisotropy: "
         "(CurvMax / CurvMin)."
      << "Center the color map on zero, if negtive values exist."
      << "Percentile of values used for color map upper-lower bounds."
      << "Draw main curvature directions as vectors, scaled by 1/(curvature radius)."
      << "Color used for convex (curvature > 0)"
      << "Color used for concave (curvature < 0)"
      << "Line width"
      << "Length of the vectors = Scale * 1/(curvature radius)."
      << "Draw the vector ends a bit tilted up for proper display on surfaces."
      << "Minimal value of curvature required for drawing the directions."; }
    QStringList parmDefaults() const { return QStringList() 
      << "SignedAverageAbs" << "Yes" << "85.0" << "Both" << "white" << "red" << "2.0"
      << "100.0" << "0.1" << "0.0"; }
    ParmChoiceMap parmChoice() const
    {
      ParmChoiceMap map;
      map[0] = QStringList() 
        << "None" << "CurvMax" << "CurvMin" << "Average" << "SignedAverageAbs"
        << "Gaussian" << "RootSumSquare" << "Anisotropy";
      map[1] = booleanChoice();
      map[3] = QStringList() << "Both" << "CurvMax" << "CurvMin" << "None";
      map[4] = QColor::colorNames();
      map[5] = QColor::colorNames();
      return map;
    }
    QIcon icon() const { return QIcon(":/images/Curvature.png"); }
  };
  ///@}
}

#endif
