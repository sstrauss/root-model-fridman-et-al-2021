//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef CCRENDERGROUPWIDGET_HPP
#define CCRENDERGROUPWIDGET_HPP

#include <ui_CCRenderGroupWidget.h>
#include <CCDrawParms.hpp>

namespace mdx {
  class CCRenderGroupWidget
    : public QWidget, public Ui::CCRenderGroupWidget
  {
    Q_OBJECT

  private:
    QString myName;

    // The colormap button is enabled when there is a color map
    // corresponding to the current render choice.
    bool cmapEnabled;
    // In addition, the entire render group is enabled
    // when there is at least one available render choice.
    bool groupEnabled;

    bool setRCTo(const QString &name);

  public:
    CCRenderGroupWidget(QWidget *_parent);
    CCRenderGroupWidget(QWidget *_parent, const QString &_name);

    void setName(QString newName)
    {
      myName = newName;
      visibleName->setText(myName);
    }

    QString name(void) const
    {
      return myName;
    }

    void setChecked(bool);                  // set visibleName to checked
    void setEnabled(bool);                  // set group enabled
    QString currentChoice(void) const;      // current choice on renderChoose widget

    QString updateChoices(const QStringList &choices, const QString &current);
    void updateCmapEnabled(const ColorMap &colorMap);

  signals:
    void toggled(bool);                     // visibility on/off
    void indexChanged(const QString &);     // selected render choice
    void colorMapClicked(void);             // colormap button pressed
  };
} // end namespace mdx

#endif
