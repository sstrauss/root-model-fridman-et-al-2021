//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TIE_HPP
#define TIE_HPP
/**
 * \file Tie.hpp
 *
 * Defines the tie function
 */

#include <Config.hpp>

#include <utility>

namespace mdx 
{
  // Code taken from the Boost library !
  /**
   * \class refpair Tie.hpp <Tie.hpp>
   *
   * Class used to hold references for the tie() function.
   */
  template <typename T, typename U> struct refpair {
    typedef T first_type;
    typedef U second_type;
  
    /// Construct a pair of references to \c x and \c y.
    refpair(T& x, U& y) : first(x) , second(y) {}
    /// Construct a copy.
    refpair(refpair const& rp) : first(rp.first), second(rp.second) {}
  
    /// Assign the values of \c p to the references in this pair.
    refpair& operator=(std::pair<T, U> const& p)
    {
      first = p.first;
      second = p.second;
      return *this;
    }
  
    /// The first member of the pair.
    T& first;
    /// The second member of the pair.
    U& second;
  };
  
  /**
   * Tie two variables to the values of a pair.
   *
   * Example:
   * \code
   * std::pair<int,double> p(1,2.5);
   * int a;
   * double b;
   * tie(a,b) = p;
   * \endcode
   *
   * At the end, \c a is \c 1 and \c b is \c 2.5
   */
  template <typename T, typename U> inline refpair<T, U> tie(T& x, U& y) 
	{
    return refpair<T, U>(x, y);
  }
}
#endif
