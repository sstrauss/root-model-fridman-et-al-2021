//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef WALL_SIGNAL_ORIENTATION_HPP
#define WALL_SIGNAL_ORIENTATION_HPP

#include <Process.hpp>

namespace mdx
{
  class WallSignalOrientation : public Process
  {
  public:
    WallSignalOrientation(const Process& process) : Process(process) {}

    bool run(const QStringList& parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, parms[0].toFloat(), parms[1].toFloat());
    }

    bool run(Mesh* mesh, float border, float minAreaRatio);

    QString name() const { return "Mesh/Cell Axis/Polarization/Compute PIN Orientation"; }
    QString description() const { return "Compute principal orientation of signal gradient between cell border and center."; }
    QStringList parmNames() const { return QStringList() 
			<< "Border Size" 
			<< "Minimum inner area ratio" ; }
    QStringList parmDescs() const { return QStringList() 
			<< "Width of cell border that is not taken into account for the computation." 
			<< "Minimum ratio of inner area (whole cell - border) vs. total area needed for computation." ; }
    QStringList parmDefaults() const { return QStringList() 
			<< "0.5" 
			<< "0.25"; }
    QIcon icon() const { return QIcon(":/images/PDG.png"); }
  };

  class DisplayWallSignalOrientation: public Process
  {
    public:
      DisplayWallSignalOrientation(const Process& process) : Process(process) {}

      bool run(const QStringList& parms)
      {
        Mesh *mesh = currentMesh();
        if(!mesh) throw(QString("No current mesh"));

        return run(mesh,parms[0], parms[1], QColor(parms[2]), QColor(parms[3]), parms[4].toFloat(),
                                     parms[5].toFloat(), parms[6].toFloat(), parms[7].toFloat());
      }

		  bool run(Mesh *mesh, const QString displayHeatMap, const QString displayAxis, const QColor& qaxisColor1,
               const QColor& qaxisColor2, float axisLineWidth, float axisLineScale, float axisOffset, float orientationThreshold);	

      QString name() const { return "Mesh/Cell Axis/Polarization/Display PIN Orientation"; }
      QString description() const { return "Display the orientation of gradients of signal between cell center vs border."; }
      QStringList parmNames() const { return QStringList()
        << "Heatmap" 
        << "Axis" 
				<< "Line Color Max" 
				<< "Line Color Min" 
				<< "Line Width" 
				<< "Line Scale" 
				<< "Line Offset"
				<< "Threshold"; }
      QStringList parmDescs() const { return QStringList()
        << "Display strength of orientation as a colormap.\n" 
				   "Ratio = Max_gradient/Min_gradient -1"
				   "Difference = Max_gradient - Min_gradient"
        << "Display principal directions of signal gradient.\n"
           "Ratio -> display only Max orientation, scaled by (Max/Min-1).\n"
           "Difference -> display only Max orientation, scaled by (Max-Min).\n"
				<< "Color Max direction." 
				<< "Color Min direction." 
				<< "Line Width" 
				<< "Length of the vectors = Scale * orientation strength." 
				<< "Draw the vector ends a bit tilted up for proper display on surfaces."
				<< "Minimal value of orientation strength required for drawing main direction.\n"
				   "Either use Ratio or Difference, depending on the display Axis option.";	}
      QStringList parmDefaults() const { return QStringList() 
        << "None" 
        << "Difference" 
				<< "red" 
				<< "green" 
				<< "5.0" 
				<< "0.01" 
				<< "0.1"
				<< "0.0" ; }
      ParmChoiceMap parmChoice() const {
        ParmChoiceMap map;
        map[0] = QStringList() << "None" << "Ratio" << "Difference" << "BezierX" << "BezierY";
        map[1] = QStringList() << "None" << "Both" << "Ratio" << "Difference" << "BezierX" << "BezierY";
        map[2] = QColor::colorNames();
        map[3] = QColor::colorNames();
        return map;
      }
      QIcon icon() const { return QIcon(":/images/PDG.png"); }
  };
}
#endif
