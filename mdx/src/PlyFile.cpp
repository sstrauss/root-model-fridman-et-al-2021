//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "PlyFile.hpp"

#include "Information.hpp"

#include <QDataStream>
#include <QFile>
#include <QStringList>
#include <QtEndian>
#include <QTextStream>
#include <stdlib.h>

namespace mdx
{
  const unsigned int PlyFile::typeSizes[NB_TYPES] = { 1, 1, 2, 2, 4, 4, 4, 8 };
  char const* const PlyFile::typeNames[NB_TYPES + 1][3] =
    { {"char", "int8", NULL}, {"uchar", "uint8", NULL}, {"short", "int16", NULL}, {"ushort", "uint16", NULL},
      {"int", "int32", NULL}, {"uint", "uint32", NULL}, {"float", "float32", NULL}, {"double", "float64", NULL},
      {"invalid", NULL, NULL} };

  char const* const PlyFile::formatNames[4] = { "unspecified", "ascii", "binary_little_endian", "binary_big_endian" };

  PlyFile::PlyFile() : line_nb(-1)
  {
    clear();
  }

  void PlyFile::clear()
  {
    current_element = 0;
    _elements.clear();
    _element_map.clear();
    _contentPosition = -1;
    _format = UNSPECIFIED_FORMAT;
    _version = "";
    _version_major = _version_minor = -1;
    is_valid = false;
  }

  bool PlyFile::init(FORMAT_TYPES f, const QString& ver)
  {
    clear();
    if(!setVersion(ver))
      return false;
    if(!setFormat(f))
      return false;
    return true;
  }

  bool PlyFile::validate()
  {
    is_valid = false;
    if(_format == UNSPECIFIED_FORMAT)
      return error("Format is not specified");
    if(_version.isEmpty())
      return error("Version number is nor specified");
    is_valid = true;
    current_element = 0;
    return true;
  }

  bool PlyFile::error(const QString err) const
  {
    if(!filename.isEmpty())
      mdxInfo << "Error reading file '" << filename << "' ";
    if(line_nb > 0)
      mdxInfo << "on line " << line_nb << " ";
    mdxInfo << err << endl;
    return false;
  }

  bool PlyFile::parseHeader(const QString& filename)
  {
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly))
      throw(QString("Cannot open input file: %1").arg(filename));
    this->filename = filename;
    clear();
    QTextStream ts(&file);
    QString magick = ts.readLine().trimmed();
    line_nb = 1;
    if(magick != "ply")
      return error("first line must be 'ply\\n'");
    while(!ts.atEnd()) {
      ++line_nb;
      QString line = ts.readLine().trimmed();
      if(not line.isEmpty()) {
        QStringList fields = line.split(" ");
        QString fieldName = fields.front().toLower();
        fields.pop_front();
        if(fieldName == "comment")
          _comments << fields.join(" ");
        else if(fieldName == "format") {
          if(!readFormat(fields))
            return false;
        } else if(fieldName == "element") {
          if(!readElement(fields))
            return false;
        } else if(fieldName == "property") {
          if(!readProperty(fields))
            return false;
        } else if(fieldName == "end_header") {
          _contentPosition = ts.pos();
          break;
        } else {
          mdxInfo << "*** WARNING *** Unknown field in header: " << line << endl;
        }
      }
    }
    if(not _comments.empty()) {
      for(int i = 0; i < _comments.size(); ++i)
        mdxInfo << "// " << _comments[i] << endl;
    }
    is_valid = true;
    return true;
  }

  bool PlyFile::readFormat(const QStringList& fields)
  {
    if(_format != UNSPECIFIED_FORMAT)
      return error("the format has already been specified before");
    if(fields.size() != 2)
      return error("the format should have two fields: type and number");
    QString form = fields[0].toLower();
    setVersion(fields[1]);
    if(form == "ascii")
      _format = ASCII;
    else if(form == "binary_little_endian")
      _format = BINARY_LITTLE_ENDIAN;
    else if(form == "binary_big_endian")
      _format = BINARY_BIG_ENDIAN;
    else
      return error(QString("Unknown format '%1'").arg(form));
    return validate();
  }

  bool PlyFile::readElement(const QStringList& fields)
  {
    if(_format == UNSPECIFIED_FORMAT)
      return error("the first element must be after the format specification");
    if(fields.size() != 2)
      return error("an element must have a name and the size");
    bool ok;
    size_t size = fields[1].toUInt(&ok);
    if(!ok)
      return error("the element size is not a valid unsigned integer");
    if(!createElement(fields[0], size))
      return false;
    return true;
  }

  PlyFile::TYPE PlyFile::parseType(QString typeName) const
  {
    typeName = typeName.toLower();
    for(int i = 0; i < NB_TYPES; ++i) {
      for(int j = 0 ; typeNames[i][j] != NULL ; j++)
        if(typeName == typeNames[i][j])
          return (TYPE)i;
    }
    return INVALID_TYPE;
  }

  bool PlyFile::readProperty(const QStringList& fields)
  {
    if(currentElement() == 0)
      return error("property defined outside any element");
    if(fields.size() != 2 and fields.size() != 4)
      return error("property must be one of 'type name' or 'list type type name'");
    QString type = fields[0].toLower();
    if(type == "list") {
      if(fields.size() != 4)
        return error("list property must have four fields: 'list' TYPE TYPE NAME");
      TYPE tc = parseType(fields[1]);
      TYPE te = parseType(fields[2]);
      if(tc == INVALID_TYPE or tc == FLOAT or tc == DOUBLE)
        return error("type for element count is invalid");
      if(te == INVALID_TYPE)
        return error("type of element is invalid");
      if(!currentElement()->createList(fields[3], tc, te))
        return false;
    } else {
      if(fields.size() != 2)
        return error("property must have two fields: TYPE NAME");
      TYPE t = parseType(fields[0]);
      if(t == INVALID_TYPE)
        return error("type of element is invalid");
      if(!currentElement()->createValue(fields[1], t))
        return false;
    }
    return true;
  }

  bool PlyFile::parseContent()
  {
    if(not is_valid)
      return error("Error, you first need to parse the header of the file.");
    QFile f(filename);
    if(!f.open(QIODevice::ReadOnly)) {
      mdxInfo << "Error, cannot open file '" << filename << "' for reading" << endl;
      return false;
    }
    f.seek(_contentPosition);
    switch(format()) {
    case ASCII:
      return parseAsciiContent(f);
    case BINARY_LITTLE_ENDIAN:
      return parseBinaryContent(f, true);
    case BINARY_BIG_ENDIAN:
      return parseBinaryContent(f, false);
    case UNSPECIFIED_FORMAT:
      return error("Cannot process unspecified format header");
    }
    return true;
  }

  // These functions are local
  namespace 
  {
    template <typename T> T toValue(QDataStream& ds)
    {
      T val;
      ds >> val;
      return val;
    }
  
    template <> float toValue<float>(QDataStream& ds)
    {
      ds.setFloatingPointPrecision(QDataStream::SinglePrecision);
      float val;
      ds >> val;
      return val;
    }
  
    template <> double toValue<double>(QDataStream& ds)
    {
      ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
      double val;
      ds >> val;
      return val;
    }
  
    template <typename T> T toValue(QTextStream& ts)
    {
      T val;
      ts >> val;
      return val;
    }
  
    template <> int8_t toValue<int8_t>(QTextStream& ts)
    {
      int val;
      ts >> val;
      return (int8_t)val;
    }
  
    template <> uint8_t toValue<uint8_t>(QTextStream& ts)
    {
      uint val;
      ts >> val;
      return (uint8_t)val;
    }
  
    template <typename T, typename Stream> std::vector<T> toList(Stream& ss, PlyFile::TYPE sizeType)
    {
      std::vector<T> lst;
      size_t size = 0;
      switch(sizeType) {
        case PlyFile::CHAR: size = (size_t)toValue<int8_t>(ss); break;
        case PlyFile::UCHAR: size = (size_t)toValue<uint8_t>(ss); break;
        case PlyFile::SHORT: size = (size_t)toValue<int16_t>(ss); break;
        case PlyFile::USHORT: size = (size_t)toValue<uint16_t>(ss); break;
        case PlyFile::INT: size = (size_t)toValue<int32_t>(ss); break;
        case PlyFile::UINT: size = (size_t)toValue<uint32_t>(ss); break;
        case PlyFile::FLOAT: size = (size_t)toValue<float>(ss); break;
        case PlyFile::DOUBLE: size = (size_t)toValue<double>(ss); break;
        case PlyFile::INVALID_TYPE: break;
      }
      if(size > 0) {
        lst.resize(size);
        for(size_t i = 0; i < size; ++i)
          lst[i] = toValue<T>(ss);
      }
      return lst;
    }
  
    template <typename T> void convertList(PlyFile::Property* property, const std::vector<T>& list, int element_pos)
    {
      switch(property->memType()) {
        case PlyFile::CHAR: {
            std::vector<int8_t>& memlist = (*property->list<int8_t>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (int8_t)list[i1];
          } break;
        case PlyFile::UCHAR: {
            std::vector<uint8_t>& memlist = (*property->list<uint8_t>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (uint8_t)list[i1];
          } break;
        case PlyFile::SHORT: {
            std::vector<int16_t>& memlist = (*property->list<int16_t>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (int16_t)list[i1];
          } break;
        case PlyFile::USHORT: {
            std::vector<uint16_t>& memlist = (*property->list<uint16_t>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (uint16_t)list[i1];
          } break;
        case PlyFile::INT: {
            std::vector<int32_t>& memlist = (*property->list<int32_t>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (int32_t)list[i1];
          } break;
        case PlyFile::UINT: {
            std::vector<uint32_t>& memlist = (*property->list<uint32_t>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (uint32_t)list[i1];
          } break;
        case PlyFile::FLOAT: {
            std::vector<float>& memlist = (*property->list<float>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (float)list[i1];
          } break;
        case PlyFile::DOUBLE: {
            std::vector<double>& memlist = (*property->list<double>())[element_pos]; 
            memlist.resize(list.size()); 
            for(size_t i1 = 0; i1 < list.size(); ++i1) 
              memlist[i1] = (double)list[i1];
          } break;
        case PlyFile::INVALID_TYPE:
          break;
      }
    }
  
    template <typename T> void storeValue(PlyFile::Property* property, const T& value, int element_pos)
    {
      switch(property->memType()) {
        case PlyFile::CHAR: (*property->value<int8_t>())[element_pos] = (int8_t)value; break;
        case PlyFile::UCHAR: (*property->value<uint8_t>())[element_pos] = (uint8_t)value; break;
        case PlyFile::SHORT: (*property->value<int16_t>())[element_pos] = (int16_t)value; break;
        case PlyFile::USHORT: (*property->value<uint16_t>())[element_pos] = (uint16_t)value; break;
        case PlyFile::INT: (*property->value<int32_t>())[element_pos] = (int32_t)value; break;
        case PlyFile::UINT: (*property->value<uint32_t>())[element_pos] = (uint32_t)value; break;
        case PlyFile::FLOAT: (*property->value<float>())[element_pos] = (float)value; break;
        case PlyFile::DOUBLE: (*property->value<double>())[element_pos] = (double)value; break;
        case PlyFile::INVALID_TYPE: break;
      }
    }
  
    template <typename Stream>
    bool setValue(PlyFile::Property* property, int element_pos, Stream& ts, const QString& element_name, PlyFile* ply)
    {
      switch(property->fileType()) {
        case PlyFile::CHAR: storeValue(property, toValue<int8_t>(ts), element_pos); break;
        case PlyFile::UCHAR: storeValue(property, toValue<uint8_t>(ts), element_pos); break;
        case PlyFile::SHORT: storeValue(property, toValue<int16_t>(ts), element_pos); break;
        case PlyFile::USHORT: storeValue(property, toValue<uint16_t>(ts), element_pos); break;
        case PlyFile::INT: storeValue(property, toValue<int32_t>(ts), element_pos); break;
        case PlyFile::UINT: storeValue(property, toValue<uint32_t>(ts), element_pos); break;
        case PlyFile::FLOAT: storeValue(property, toValue<float>(ts), element_pos); break;
        case PlyFile::DOUBLE: storeValue(property, toValue<double>(ts), element_pos); break;
        case PlyFile::INVALID_TYPE: break;
      }
      if(ts.status() == Stream::ReadCorruptData) 
        return ply->error(QString("Cannot parse value for property %1 of item %2 in element %3") 
                                              .arg(property->name()).arg(element_pos).arg(element_name)); 
      return true;
    }
  
    template <typename Stream>
    bool setValueList(PlyFile::Property* property, int element_pos, Stream& ts, const QString& element_name, PlyFile* ply)
    {
      switch(property->fileType()) {
        case PlyFile::CHAR: convertList(property, toList<int8_t>(ts, property->sizeType()), element_pos); break;
        case PlyFile::UCHAR: convertList(property, toList<uint8_t>(ts, property->sizeType()), element_pos); break;
        case PlyFile::SHORT: convertList(property, toList<int16_t>(ts, property->sizeType()), element_pos); break;
        case PlyFile::USHORT: convertList(property, toList<uint16_t>(ts, property->sizeType()), element_pos); break;
        case PlyFile::INT: convertList(property, toList<int32_t>(ts, property->sizeType()), element_pos); break;
        case PlyFile::UINT: convertList(property, toList<uint32_t>(ts, property->sizeType()), element_pos); break;
        case PlyFile::FLOAT: convertList(property, toList<float>(ts, property->sizeType()), element_pos); break;
        case PlyFile::DOUBLE: convertList(property, toList<double>(ts, property->sizeType()), element_pos); break;
        case PlyFile::INVALID_TYPE: break;
      }
      if(ts.status() == Stream::ReadCorruptData) 
        return ply->error(QString("Cannot parse list for property %1 of item %2 in element %3") 
                                           .arg(property->name()).arg(element_pos).arg(element_name));
      return true;
    }
  
    template <typename Stream>
    bool readPropertyValue(PlyFile::Property* property, Stream& fields, int element_pos, const QString& element_name,
                           PlyFile* ply)
    {
      if(fields.atEnd())
        return ply->error(QString("Error, there are not enough values for the item %1 of the element %2").arg(element_pos).arg(element_name));
  
      switch(property->kind()) {
        case PlyFile::Property::VALUE:
          return setValue(property, element_pos, fields, element_name, ply);
        case PlyFile::Property::LIST:
          return setValueList(property, element_pos, fields, element_name, ply);
      }
      return true;
    }
  
    template <typename T1> void convertContent(const std::vector<T1>& old_c, void* newContent, PlyFile::TYPE newType)
    {
      switch(newType) {
        case PlyFile::CHAR: { 
          std::vector<int8_t>& new_c = *(std::vector<int8_t>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (int8_t)old_c[i]; 
          } break;
        case PlyFile::UCHAR: { 
          std::vector<uint8_t>& new_c = *(std::vector<uint8_t>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (uint8_t)old_c[i]; 
          } break;
        case PlyFile::SHORT: { 
          std::vector<int16_t>& new_c = *(std::vector<int16_t>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (int16_t)old_c[i]; 
          } break;
        case PlyFile::USHORT: { 
          std::vector<uint16_t>& new_c = *(std::vector<uint16_t>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (uint16_t)old_c[i]; 
          } break;
        case PlyFile::INT: { 
          std::vector<int32_t>& new_c = *(std::vector<int32_t>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (int32_t)old_c[i]; 
          } break;
        case PlyFile::UINT: { 
          std::vector<uint32_t>& new_c = *(std::vector<uint32_t>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (uint32_t)old_c[i]; 
          } break;
        case PlyFile::FLOAT: { 
          std::vector<float>& new_c = *(std::vector<float>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (float)old_c[i]; 
          } break;
        case PlyFile::DOUBLE: { 
          std::vector<double>& new_c = *(std::vector<double>*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) 
            new_c[i] = (double)old_c[i]; 
          } break;
        case PlyFile::INVALID_TYPE:
          break;
      }
    }
  
    void convertContent(void* oldContent, PlyFile::TYPE oldType, void* newContent, PlyFile::TYPE newType)
    {
      mdxInfo << "Convert content from " << PlyFile::typeNames[oldType][0] << " to " << PlyFile::typeNames[newType][0] << endl;
      switch(oldType) {
        case PlyFile::CHAR: { 
          std::vector<int8_t>* old_c = (std::vector<int8_t>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::UCHAR: { 
          std::vector<uint8_t>* old_c = (std::vector<uint8_t>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::SHORT: { 
          std::vector<int16_t>* old_c = (std::vector<int16_t>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::USHORT: { 
          std::vector<uint16_t>* old_c = (std::vector<uint16_t>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::INT: { 
          std::vector<int32_t>* old_c = (std::vector<int32_t>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::UINT: { 
          std::vector<uint32_t>* old_c = (std::vector<uint32_t>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::FLOAT: { 
          std::vector<float>* old_c = (std::vector<float>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::DOUBLE: { 
          std::vector<double>* old_c = (std::vector<double>*)oldContent; 
          convertContent(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::INVALID_TYPE:
          break;
      }
    }
  
    template <typename T1> void convertContentList(const std::vector<T1>& old_c, void* newContent, PlyFile::TYPE newType)
    {
      switch(newType) {
        case PlyFile::CHAR: { 
          std::vector<std::vector<int8_t> >& new_c = *(std::vector<std::vector<int8_t> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (int8_t)old_c[i][j]; 
          } } break;
        case PlyFile::UCHAR: { 
          std::vector<std::vector<uint8_t> >& new_c = *(std::vector<std::vector<uint8_t> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (uint8_t)old_c[i][j]; 
          } } break;
        case PlyFile::SHORT: { 
          std::vector<std::vector<int16_t> >& new_c = *(std::vector<std::vector<int16_t> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (int16_t)old_c[i][j]; 
          } } break;
        case PlyFile::USHORT: { 
          std::vector<std::vector<uint16_t> >& new_c = *(std::vector<std::vector<uint16_t> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (uint16_t)old_c[i][j]; 
          } } break;
        case PlyFile::INT: { 
          std::vector<std::vector<int32_t> >& new_c = *(std::vector<std::vector<int32_t> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (int32_t)old_c[i][j]; 
          } } break;
        case PlyFile::UINT: { 
          std::vector<std::vector<uint32_t> >& new_c = *(std::vector<std::vector<uint32_t> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (uint32_t)old_c[i][j]; 
          } } break;
        case PlyFile::FLOAT: { 
          std::vector<std::vector<float> >& new_c = *(std::vector<std::vector<float> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (float)old_c[i][j]; 
          } } break;
        case PlyFile::DOUBLE: { 
          std::vector<std::vector<double> >& new_c = *(std::vector<std::vector<double> >*)newContent; 
          new_c.resize(old_c.size()); 
          for(size_t i = 0; i < new_c.size(); ++i) { 
            new_c[i].resize(old_c[i].size()); 
            for(size_t j = 0; j < new_c[i].size(); ++j) 
              new_c[i][j] = (double)old_c[i][j]; 
          } } break;
        case PlyFile::INVALID_TYPE:
          break;
      }
    }
  
    void convertContentList(void* oldContent, PlyFile::TYPE oldType, void* newContent, PlyFile::TYPE newType)
    {
      switch(oldType) {
        case PlyFile::CHAR: { 
          std::vector<std::vector<int8_t> >* old_c = (std::vector<std::vector<int8_t> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::UCHAR: { 
          std::vector<std::vector<uint8_t> >* old_c = (std::vector<std::vector<uint8_t> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::SHORT: { 
          std::vector<std::vector<int16_t> >* old_c = (std::vector<std::vector<int16_t> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::USHORT: { 
          std::vector<std::vector<uint16_t> >* old_c = (std::vector<std::vector<uint16_t> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::INT: { 
          std::vector<std::vector<int32_t> >* old_c = (std::vector<std::vector<int32_t> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::UINT: { 
          std::vector<std::vector<uint32_t> >* old_c = (std::vector<std::vector<uint32_t> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::FLOAT: { 
          std::vector<std::vector<float> >* old_c = (std::vector<std::vector<float> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::DOUBLE: { 
          std::vector<std::vector<double> >* old_c = (std::vector<std::vector<double> >*)oldContent; 
          convertContentList(*old_c, newContent, newType); 
          delete old_c; 
          } break;
        case PlyFile::INVALID_TYPE:
          break;
      }
    }
  } // end namespace

  bool PlyFile::parseBinaryContent(QFile& f, bool little_endian)
  {
    line_nb = -1;
    QDataStream ds(&f);
    if(little_endian)
      ds.setByteOrder(QDataStream::LittleEndian);
    else
      ds.setByteOrder(QDataStream::BigEndian);
    if(ds.atEnd())
      return error("PlyFile::parseAsciiContent Premature end of file");
    for(int i = 0; i < _elements.size(); ++i) {
      Element* element = _elements[i];
      element->allocate();
      for(size_t j = 0; j < element->size(); ++j) {
        for(size_t k = 0; k < element->nbProperties(); ++k)
          if(!readPropertyValue(element->property(k), ds, j, element->name(), this))
            return false;
      }
    }
    if(!ds.atEnd())
      mdxInfo << "PlyFile::parseAsciiContent There is more data in the file than used" << endl;

    return true;
  }

  bool PlyFile::parseAsciiContent(QFile& f)
  {
    QTextStream ts(&f);
    for(int i = 0; i < _elements.size(); ++i) {
      Element* element = _elements[i];
      element->allocate();
      for(size_t j = 0; j < element->size(); ++j) {
        QString line = ts.readLine().trimmed();
        QTextStream content(&line);
        for(size_t k = 0; k < element->nbProperties(); ++k)
          if(!readPropertyValue(element->property(k), content, j, element->name(), this))
            mdxInfo << QString("PlyFile::parseAsciiContent Error reading item %1 of element %2 line: %3")
                                                                           .arg(j).arg(element->name()).arg(line) << endl;
        if(!content.atEnd())
          mdxInfo << QString("PlyFile::parseAsciiContent There are more fields defined than used for item %1 of element %2 line: %3")
                                                                           .arg(j).arg(element->name()).arg(line) << endl;
      }
    }
    return true;
  }

  PlyFile::Property::Property(const QString& name, Element* p) : _name(name), _fileType(INVALID_TYPE), _memType(INVALID_TYPE), 
                                 _sizeType(INVALID_TYPE), _kind(VALUE), _parent(0), _content(0), _size(0)
  {
    if(p)
      setParent(p);
  }

  PlyFile::Property::~Property()
  {
    setParent(0);
    deallocate();
  }

  bool PlyFile::Property::setParent(Element* p)
  {
    if(p != _parent) {
      if(p and p->hasProperty(_name))
        return false;
      if(_parent)
        _parent->_detach(this);
      _parent = p;
      if(p)
        _parent->_attach(this);
    }
    return true;
  }

  bool PlyFile::Property::error(const QString& str) const
  {
    mdxInfo << "Error " << str << endl;
    return false;
  }

  bool PlyFile::Property::rename(const QString& n)
  {
    if(_parent) {
      if(_parent->hasProperty(n))
        return error(QString("Cannot rename property, its element already has a property named %1").arg(n));
      _parent->_rename_prop(this, n);
    }
    _name = n;
    return true;
  }

  void PlyFile::Element::_rename_prop(Property* prop, const QString& new_name)
  {
    int idx = _property_map[prop->name()];
    _property_map.remove(prop->name());
    _property_map[new_name] = idx;
  }

  void PlyFile::Property::setKind(KIND k)
  {
    if(k != _kind) {
      bool need_allocation = (bool)_content;
      if(need_allocation)
        deallocate();
      _kind = k;
      if(need_allocation)
        allocate(_size);
    }
  }

  void PlyFile::Property::setMemType(TYPE mt)
  {
    if(mt != _memType) {
      if(_content) {
        TYPE oldType = _memType;
        void* oldContent = _content;
        _size = 0;
        _content = 0;
        _memType = mt;
        allocate(_size);
        if(_kind == VALUE)
          convertContent(oldContent, oldType, _content, _memType);
        else
          convertContentList(oldContent, oldType, _content, _memType);
      } else
        _memType = mt;
    }
  }

  void PlyFile::Property::allocate(size_t size)
  {
    if(_content)
      deallocate();
    switch(kind()) {
    case VALUE:
      switch(memType()) {
        case PlyFile::CHAR: _content = new std::vector<int8_t>(size, int8_t()); break;
        case PlyFile::UCHAR: _content = new std::vector<uint8_t>(size, uint8_t()); break;
        case PlyFile::SHORT: _content = new std::vector<int16_t>(size, int16_t()); break;
        case PlyFile::USHORT: _content = new std::vector<uint16_t>(size, uint16_t()); break;
        case PlyFile::INT: _content = new std::vector<int32_t>(size, int32_t()); break;
        case PlyFile::UINT: _content = new std::vector<uint32_t>(size, uint32_t()); break;
        case PlyFile::FLOAT: _content = new std::vector<float>(size, float()); break;
        case PlyFile::DOUBLE: _content = new std::vector<double>(size, double()); break;
        case INVALID_TYPE: break;
      }
      break;
    case LIST:
      switch(memType()) {
        case PlyFile::CHAR: _content = new std::vector<std::vector<int8_t> >(size, std::vector<int8_t>()); break;
        case PlyFile::UCHAR: _content = new std::vector<std::vector<uint8_t> >(size, std::vector<uint8_t>()); break;
        case PlyFile::SHORT: _content = new std::vector<std::vector<int16_t> >(size, std::vector<int16_t>()); break;
        case PlyFile::USHORT: _content = new std::vector<std::vector<uint16_t> >(size, std::vector<uint16_t>()); break;
        case PlyFile::INT: _content = new std::vector<std::vector<int32_t> >(size, std::vector<int32_t>()); break;
        case PlyFile::UINT: _content = new std::vector<std::vector<uint32_t> >(size, std::vector<uint32_t>()); break;
        case PlyFile::FLOAT: _content = new std::vector<std::vector<float> >(size, std::vector<float>()); break;
        case PlyFile::DOUBLE: _content = new std::vector<std::vector<double> >(size, std::vector<double>()); break;
        case INVALID_TYPE: break;
      }
      break;
    }
    if(_content)
      _size = size;
    else
      _size = 0;
  }

  void PlyFile::Property::resize(size_t s)
  {
    if(_content) {
      switch(kind()) {
      case VALUE:
        switch(memType()) {
          case PlyFile::CHAR: { std::vector<int8_t>* c = (std::vector<int8_t>*)_content; c->resize(s); } break;
          case PlyFile::UCHAR: { std::vector<uint8_t>* c = (std::vector<uint8_t>*)_content; c->resize(s); } break;
          case PlyFile::SHORT: { std::vector<int16_t>* c = (std::vector<int16_t>*)_content; c->resize(s); } break;
          case PlyFile::USHORT: { std::vector<uint16_t>* c = (std::vector<uint16_t>*)_content; c->resize(s); } break;
          case PlyFile::INT: { std::vector<int32_t>* c = (std::vector<int32_t>*)_content; c->resize(s); } break;
          case PlyFile::UINT: { std::vector<uint32_t>* c = (std::vector<uint32_t>*)_content; c->resize(s); } break;
          case PlyFile::FLOAT: { std::vector<float>* c = (std::vector<float>*)_content; c->resize(s); } break;
          case PlyFile::DOUBLE: { std::vector<double>* c = (std::vector<double>*)_content; c->resize(s); } break;
          case INVALID_TYPE:
            break;
        }
        break;
      case LIST:
        switch(memType()) {
          case PlyFile::CHAR: { std::vector<std::vector<int8_t> >* c = (std::vector<std::vector<int8_t> >*)_content; c->resize(s); } break;
          case PlyFile::UCHAR: { std::vector<std::vector<uint8_t> >* c = (std::vector<std::vector<uint8_t> >*)_content; c->resize(s); } break;
          case PlyFile::SHORT: { std::vector<std::vector<int16_t> >* c = (std::vector<std::vector<int16_t> >*)_content; c->resize(s); } break;
          case PlyFile::USHORT: { std::vector<std::vector<uint16_t> >* c = (std::vector<std::vector<uint16_t> >*)_content; c->resize(s); } break;
          case PlyFile::INT: { std::vector<std::vector<int32_t> >* c = (std::vector<std::vector<int32_t> >*)_content; c->resize(s); } break;
          case PlyFile::UINT: { std::vector<std::vector<uint32_t> >* c = (std::vector<std::vector<uint32_t> >*)_content; c->resize(s); } break;
          case PlyFile::FLOAT: { std::vector<std::vector<float> >* c = (std::vector<std::vector<float> >*)_content; c->resize(s); } break;
          case PlyFile::DOUBLE: { std::vector<std::vector<double> >* c = (std::vector<std::vector<double> >*)_content; c->resize(s); } break;
          case INVALID_TYPE:
            break;
        }
        break;
      }
    }
    _size = s;
  }

  void PlyFile::Property::deallocate()
  {
    if(not _content)
      return;
    switch(kind()) {
    case VALUE:
      switch(memType()) {




        case PlyFile::CHAR: delete (std::vector<int8_t>*)_content; break;
      case PlyFile::UCHAR: delete (std::vector<uint8_t>*)_content; break;
      case PlyFile::SHORT: delete (std::vector<int16_t>*)_content; break;
      case PlyFile::USHORT: delete (std::vector<uint16_t>*)_content; break;
      case PlyFile::INT: delete (std::vector<int32_t>*)_content; break;
      case PlyFile::UINT: delete (std::vector<uint32_t>*)_content; break;
      case PlyFile::FLOAT: delete (std::vector<float>*)_content; break;
      case PlyFile::DOUBLE: delete (std::vector<double>*)_content; break;

      case INVALID_TYPE:
        break;
      }
      break;
    case LIST:
      switch(memType()) {




        case PlyFile::CHAR: delete (std::vector<std::vector<int8_t> >*)_content; break;
      case PlyFile::UCHAR: delete (std::vector<std::vector<uint8_t> >*)_content; break;
      case PlyFile::SHORT: delete (std::vector<std::vector<int16_t> >*)_content; break;
      case PlyFile::USHORT: delete (std::vector<std::vector<uint16_t> >*)_content; break;
      case PlyFile::INT: delete (std::vector<std::vector<int32_t> >*)_content; break;
      case PlyFile::UINT: delete (std::vector<std::vector<uint32_t> >*)_content; break;
      case PlyFile::FLOAT: delete (std::vector<std::vector<float> >*)_content; break;
      case PlyFile::DOUBLE: delete (std::vector<std::vector<double> >*)_content; break;

      case INVALID_TYPE:
        break;
      }
      break;
    }
    _content = 0;
    _size = 0;
  }

  PlyFile::Element* PlyFile::element(const QString& name)
  {
    QHash<QString, int>::iterator found = _element_map.find(name);
    if(found != _element_map.end())
      return _elements[found.value()];
    return 0;
  }

  PlyFile::Property* PlyFile::Element::property(const QString& name)
  {
    QHash<QString, int>::iterator found = _property_map.find(name);
    if(found != _property_map.end())
      return _properties[found.value()];
    return 0;
  }

  PlyFile::Property* PlyFile::Element::property(size_t idx) {
    return _properties[idx];
  }

  bool PlyFile::Element::hasProperty(const QString& name) const {
    return _property_map.contains(name);
  }

  const PlyFile::Property* PlyFile::Element::property(size_t idx) const {
    return _properties[idx];
  }

  const PlyFile::Element* PlyFile::element(const QString& name) const
  {
    QHash<QString, int>::const_iterator found = _element_map.find(name);
    if(found != _element_map.end())
      return _elements[found.value()];
    return 0;
  }

  const PlyFile::Property* PlyFile::Element::property(const QString& name) const
  {
    QHash<QString, int>::const_iterator found = _property_map.find(name);
    if(found != _property_map.end())
      return _properties[found.value()];
    return 0;
  }

  bool PlyFile::hasElement(const QString& name) const {
    return _element_map.contains(name);
  }

  PlyFile::Element* PlyFile::createElement(const QString& name, size_t nb_elements)
  {
    if(_element_map.contains(name)) {
      error(QString("there is already an element called '%1'").arg(name));
      return 0;
    }
    Element* el = new Element(name, this);
    el->resize(nb_elements);
    current_element = el;
    return el;
  }

  PlyFile::Element::Element(const QString& n, PlyFile* p)
    : _name(n)
    , _nbElements(0)
    , _parent(0)
  {
    if(p)
      setParent(p);
  }

  PlyFile::Element::~Element()
  {
    if(_parent)
      _parent->detach(this);
    clear();
  }

  bool PlyFile::Element::setParent(PlyFile* p)
  {
    if(p != _parent) {
      if(p and p->hasElement(_name))
        return false;
      if(_parent)
        _parent->_detach(this);
      _parent = p;
      if(p)
        _parent->_attach(this);
    }
    return true;
  }

  void PlyFile::Element::resize(size_t n)
  {
    if(_allocated) {
      for(int i = 0; i < _properties.size(); ++i)
        _properties[i]->resize(n);
    }
    _nbElements = n;
  }

  void PlyFile::Element::allocate()
  {
    for(int i = 0; i < _properties.size(); ++i)
      _properties[i]->allocate(_nbElements);
  }

  bool PlyFile::Element::error(const QString& str) const
  {
    mdxInfo << "Error " << str << endl;
    return false;
  }

  void PlyFile::Element::clear()
  {
    for(int i = 0; i < _properties.size(); ++i)
      delete _properties[i];
    _properties.clear();
    _property_map.clear();
    _nbElements = 0;
  }

  bool PlyFile::Element::attach(Property* prop)
  {
    prop->setParent(this);
    return true;
  }

  void PlyFile::Element::_attach(Property* prop)
  {
    _property_map[prop->name()] = _properties.size();
    _properties << prop;
  }

  bool PlyFile::Element::detach(Property* prop)
  {
    prop->setParent(0);
    return true;
  }

  void PlyFile::Element::_detach(Property* prop)
  {
    int idx = _property_map[prop->name()];
    _property_map.remove(prop->name());
    _properties.removeAt(idx);
  }

  PlyFile::Property* PlyFile::Element::detach(const QString& name)
  {
    if(not _property_map.contains(name)) {
      error(QString("No property named '%1'").arg(name));
      return 0;
    }
    int idx = _property_map[name];
    _property_map.remove(name);
    Property* p = _properties[idx];
    _properties.removeAt(idx);
    return p;
  }

  PlyFile::Property* PlyFile::Element::createValue(const QString& name, TYPE file, TYPE mem)
  {
    if(_property_map.contains(name)) {
      error(QString("there is already a property called '%1' is the element '%2'").arg(name).arg(this->name()));
      return 0;
    }
    Property* prop = new Property(name, this);
    prop->setFileType(file);
    prop->setMemType(mem == INVALID_TYPE ? file : mem);
    prop->setKind(Property::VALUE);
    return prop;
  }

  PlyFile::Property* PlyFile::Element::createList(const QString& name, TYPE size, TYPE file, TYPE mem)
  {
    if(_property_map.contains(name)) {
      error(QString("there is already a property called '%1' is the element '%2'").arg(name).arg(this->name()));
      return 0;
    }
    Property* prop = new Property(name, this);
    prop->setFileType(file);
    prop->setMemType(mem == INVALID_TYPE ? file : mem);
    prop->setSizeType(size);
    prop->setKind(Property::LIST);
    return prop;
  }

  PlyFile::Element* PlyFile::element(size_t idx) {
    return _elements[idx];
  }

  const PlyFile::Element* PlyFile::element(size_t idx) const {
    return _elements[idx];
  }

  bool PlyFile::attach(Element* el)
  {
    el->setParent(this);
    return true;
  }

  bool PlyFile::detach(Element* el)
  {
    el->setParent(0);
    return true;
  }

  void PlyFile::_attach(Element* el)
  {
    _element_map[el->name()] = _elements.size();
    _elements << el;
  }

  void PlyFile::_detach(Element* el)
  {
    int idx = _element_map[el->name()];
    _element_map.remove(el->name());
    _elements.removeAt(idx);
  }

  void PlyFile::allocate()
  {
    for(int i = 0; i < _elements.size(); ++i)
      _elements[i]->allocate();
  }

  bool PlyFile::save(const QString& filename) const
  {
    QFile f(filename);
    if(!f.open(QIODevice::WriteOnly)) {
      mdxInfo << "Error, cannot open file '" << filename << "' for writing" << endl;
      return false;
    }

    writeHeader(f);

    switch(format()) {
    case ASCII:
      return writeAsciiContent(f);
    case BINARY_LITTLE_ENDIAN:
      return writeBinaryContent(f, true);
    case BINARY_BIG_ENDIAN:
      return writeBinaryContent(f, false);
    case UNSPECIFIED_FORMAT:
      return error("Cannot write content of unspecified format");
    }

    return true;
  }

  bool PlyFile::writeHeader(QFile& f) const
  {
    QTextStream ts(&f);
    ts << "ply\n";
    ts << "format " << formatNames[format()] << " " << version() << endl;
    ts << "comment File generated by MorphoDynamX\n";
    for(size_t i = 0; i < (size_t)_comments.size(); ++i)
      ts << "comment " << _comments[i] << "\n";
    for(int i = 0; i < _elements.size(); ++i) {
      const Element& el = *_elements[i];
      ts << "element " << el.name() << " " << el.size() << endl;
      for(size_t j = 0; j < el.nbProperties(); ++j) {
        const Property& prop = *el.property(j);
        ts << "property ";
        switch(prop.kind()) {
        case Property::LIST:
          ts << "list " << typeNames[prop.sizeType()][0] << " ";
        case Property::VALUE:
          ts << typeNames[prop.fileType()][0] << " " << prop.name() << endl;
        }
      }
    }
    ts << "end_header" << endl;
    return true;
  }

  namespace {

  template <typename T> void writeValue(const T& val, QDataStream& ds) {
    ds << val;
  }

  void writeValue(const float& val, QDataStream& ds)
  {
    ds.setFloatingPointPrecision(QDataStream::SinglePrecision);
    ds << val;
  }

  void writeValue(const double& val, QDataStream& ds)
  {
    ds.setFloatingPointPrecision(QDataStream::DoublePrecision);
    ds << val;
  }

  template <typename T> void writeValue(const T& val, QTextStream& ds) {
    ds << val << " ";
  }

  template <typename T> void writeList(const std::vector<T>& lst, PlyFile::TYPE, PlyFile::TYPE, QTextStream& ds)
  {
    writeValue(lst.size(), ds);
    for(size_t i = 0; i < lst.size(); ++i)
      writeValue(lst[i], ds);
  }

  template <typename T>
  void writeList(const std::vector<T>& lst, PlyFile::TYPE sizeType, PlyFile::TYPE fileType, QDataStream& ds)
  {
    switch(sizeType) {




      case PlyFile::CHAR: writeValue((int8_t)lst.size(), ds); break;
      case PlyFile::UCHAR: writeValue((uint8_t)lst.size(), ds); break;
      case PlyFile::SHORT: writeValue((int16_t)lst.size(), ds); break;
      case PlyFile::USHORT: writeValue((uint16_t)lst.size(), ds); break;
      case PlyFile::INT: writeValue((int32_t)lst.size(), ds); break;
      case PlyFile::UINT: writeValue((uint32_t)lst.size(), ds); break;
      case PlyFile::FLOAT: writeValue((float)lst.size(), ds); break;
      case PlyFile::DOUBLE: writeValue((double)lst.size(), ds); break;

    case PlyFile::INVALID_TYPE:
      break;
    }
    for(size_t i = 0; i < lst.size(); ++i) {
      switch(fileType) {




        case PlyFile::CHAR: writeValue((int8_t)lst[i], ds); break;
      case PlyFile::UCHAR: writeValue((uint8_t)lst[i], ds); break;
      case PlyFile::SHORT: writeValue((int16_t)lst[i], ds); break;
      case PlyFile::USHORT: writeValue((uint16_t)lst[i], ds); break;
      case PlyFile::INT: writeValue((int32_t)lst[i], ds); break;
      case PlyFile::UINT: writeValue((uint32_t)lst[i], ds); break;
      case PlyFile::FLOAT: writeValue((float)lst[i], ds); break;
      case PlyFile::DOUBLE: writeValue((double)lst[i], ds); break;

      case PlyFile::INVALID_TYPE:
        break;
      }
    }
  }

  template <typename T, typename Stream> void dumpValue(const T& val, PlyFile::TYPE type, Stream& ss)
  {
    switch(type) {




      case PlyFile::CHAR: writeValue((int8_t)val, ss); break;
      case PlyFile::UCHAR: writeValue((uint8_t)val, ss); break;
      case PlyFile::SHORT: writeValue((int16_t)val, ss); break;
      case PlyFile::USHORT: writeValue((uint16_t)val, ss); break;
      case PlyFile::INT: writeValue((int32_t)val, ss); break;
      case PlyFile::UINT: writeValue((uint32_t)val, ss); break;
      case PlyFile::FLOAT: writeValue((float)val, ss); break;
      case PlyFile::DOUBLE: writeValue((double)val, ss); break;

    case PlyFile::INVALID_TYPE:
      break;
    }
  }

  template <typename Stream>
  bool writeValue(const PlyFile::Property& prop, size_t element_pos, Stream& ds, const QString& element_name,
                  const PlyFile* ply)
  {
    switch(prop.memType()) {
# 1083 "PlyFile.cpp"
      case PlyFile::CHAR: { const int8_t& val = (*prop.value<int8_t>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::UCHAR: { const uint8_t& val = (*prop.value<uint8_t>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::SHORT: { const int16_t& val = (*prop.value<int16_t>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::USHORT: { const uint16_t& val = (*prop.value<uint16_t>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::INT: { const int32_t& val = (*prop.value<int32_t>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::UINT: { const uint32_t& val = (*prop.value<uint32_t>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::FLOAT: { const float& val = (*prop.value<float>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::DOUBLE: { const double& val = (*prop.value<double>())[element_pos]; dumpValue(val, prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;


    case PlyFile::INVALID_TYPE:
      break;
    }
    return true;
  }

  template <typename Stream>
  bool writeValueList(const PlyFile::Property& prop, size_t element_pos, Stream& ds, const QString& element_name,
                      const PlyFile* ply)
  {
    switch(prop.memType()) {
# 1108 "PlyFile.cpp"
      case PlyFile::CHAR: { const std::vector<int8_t>& lst = (*prop.list<int8_t>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::UCHAR: { const std::vector<uint8_t>& lst = (*prop.list<uint8_t>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::SHORT: { const std::vector<int16_t>& lst = (*prop.list<int16_t>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::USHORT: { const std::vector<uint16_t>& lst = (*prop.list<uint16_t>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::INT: { const std::vector<int32_t>& lst = (*prop.list<int32_t>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::UINT: { const std::vector<uint32_t>& lst = (*prop.list<uint32_t>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::FLOAT: { const std::vector<float>& lst = (*prop.list<float>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;
      case PlyFile::DOUBLE: { const std::vector<double>& lst = (*prop.list<double>())[element_pos]; writeList(lst, prop.sizeType(), prop.fileType(), ds); if(ds.status() == Stream::ReadCorruptData) return ply->error(QString("Cannot write list for property %1 of item %2 in element %3") .arg(prop.name()) .arg(element_pos) .arg(element_name)); } break;


    case PlyFile::INVALID_TYPE:
      break;
    }
    return true;
  }

  template <typename Stream>
  bool writeProperty(const PlyFile::Property& prop, Stream& ds, size_t element_pos, const QString& element_name,
                     const PlyFile* ply)
  {
    switch(prop.kind()) {
    case PlyFile::Property::VALUE:
      return writeValue(prop, element_pos, ds, element_name, ply);
    case PlyFile::Property::LIST:
      return writeValueList(prop, element_pos, ds, element_name, ply);
    }
    return true;
  }

  void endOfElement(QTextStream& ts) {
    ts << endl;
  }

  void endOfElement(QDataStream&) {
  }

  template <typename Stream> bool writeContent(Stream& ss, const PlyFile* ply)
  {
    for(size_t i = 0; i < ply->nbElements(); ++i) {
      const PlyFile::Element& el = *ply->element(i);
      for(size_t j = 0; j < el.size(); ++j) {
        for(size_t k = 0; k < el.nbProperties(); ++k) {
          const PlyFile::Property& prop = *el.property(k);
          if(!writeProperty(prop, ss, j, el.name(), ply))
            return false;
        }
        endOfElement(ss);
      }
    }
    return true;
  }
  }

  bool PlyFile::writeAsciiContent(QFile& f) const
  {
    QTextStream ts(&f);
    return writeContent(ts, this);
  }

  bool PlyFile::writeBinaryContent(QFile& f, bool little_endian) const
  {
    QDataStream ds(&f);
    if(little_endian)
      ds.setByteOrder(QDataStream::LittleEndian);
    else
      ds.setByteOrder(QDataStream::BigEndian);
    return writeContent(ds, this);
  }

  bool PlyFile::setVersion(QString v)
  {
    QStringList ver = v.split(".");
    if(ver.size() != 2)
      return false;
    bool ok;
    int vmaj = ver[0].toUInt(&ok);
    if(!ok)
      return error("MAJOR part of the version is not a valid unsigned integer");
    int vmin = ver[1].toUInt(&ok);
    if(!ok)
      return error("MINOR part of the version is not a valid unsigned integer");
    _version_major = vmaj;
    _version_minor = vmin;
    _version = v;
    return true;
  }

  void PlyFile::addComment(QString line)
  {
    QStringList lines = line.split("\n");
    _comments.append(lines);
  }
}
