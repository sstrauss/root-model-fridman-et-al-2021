//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "StackProcessShapeAnalysis.hpp"
#include <CCUtils.hpp>
// #include "Information.hpp"
// #include "Progress.hpp"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>
// #include <algorithm>
// //#include <MeshProcessPDG.hpp>
// #include <MeshProcessMeasures.hpp>

// #include <QFile>
// #include <QTextStream>

namespace mdx 
{
  // Note: empty namespace = content is not exported, and it's better (cleaner and more general) than the static keyword
  //namespace 
	//{
    struct PCAnalysisResult 
  	{
      Point3d p1, p2, p3;
      Point3d ev;
      Point3d mean;
    
      bool valid() const { return normsq(ev) > 0; }
    
      operator bool() const { return valid(); }
    };
    
    struct SelectThreshold 
  	{
      SelectThreshold(ushort th) : threshold(th) {}
    
      int operator()(ushort v) const
      {
        if(v > threshold)
          return 0;
        return -1;
      }
    
      ushort value(int vid) const
      {
        if(vid == 1)
          return threshold;
        return 0;
      }
    
      int nb_values() const {
        return 1;
      }
    
      ushort threshold;
    };
    
    struct SelectLabel
   	{
      SelectLabel(const std::vector<ushort>& ls) : labels(ls)
      {
        for(size_t i = 0; i < labels.size(); ++i)
          inv_labels[labels[i]] = i;
      }
    
      int operator()(ushort v) const
      {
        std::unordered_map<ushort, int>::const_iterator found = inv_labels.find(v);
        if(found != inv_labels.end())
          return found->second;
        return -1;
      }
    
      ushort value(int vid) const
      {
        if(vid >= 0 and vid < (int)labels.size())
          return labels[vid];
        return 0;
      }
    
      int nb_values() const {
        return inv_labels.size();
      }
    
      std::vector<ushort> labels;
      std::unordered_map<ushort, int> inv_labels;
    };
    
    template <typename Fct>
    std::vector<PCAnalysisResult> analyzePC(const Stack* stk, const HVecUS& data, const Fct& selection,
                                             Point3d correcting_factor)
    {
      // First, compute mean
      size_t nb_values = selection.nb_values();
      std::vector<Point3d> mean(nb_values, Point3d(0, 0, 0));
      std::vector<unsigned long long> sum(nb_values, 0);
      std::vector<PCAnalysisResult> result(nb_values);
    
      // Compute the mean for each selected value
      mdxInfo << "Compute mean" << endl;
      Point3u s = stk->size();
      size_t k = 0;
      for(size_t z = 0; z < s.z(); ++z)
        for(size_t y = 0; y < s.y(); ++y)
          for(size_t x = 0; x < s.x(); ++x, ++k) {
            int vid = selection(data[k]);
            if(vid >= 0) {
              ++sum[vid];
              mean[vid] += stk->imageToWorld(Point3d(x, y, z));
            }
          }
      for(size_t vid = 0; vid < nb_values; ++vid) {
        if(sum[vid] > 0)
          mean[vid] /= double(sum[vid]);
      }
      mdxInfo << "Compute CC matrix" << endl;
      // Compute the cross-correlation matrix
      std::vector<Matrix3d> corr(nb_values);
      k = 0;
      for(size_t z = 0; z < s.z(); ++z)
        for(size_t y = 0; y < s.y(); ++y)
          for(size_t x = 0; x < s.x(); ++x, ++k) {
            int vid = selection(data[k]);
            if(vid >= 0) {
              Point3d dp = stk->imageToWorld(Point3d(x, y, z)) - mean[vid];
              corr[vid](0, 0) += dp[0] * dp[0];
              corr[vid](1, 1) += dp[1] * dp[1];
              corr[vid](2, 2) += dp[2] * dp[2];
              corr[vid](1, 0) += dp[0] * dp[1];
              corr[vid](2, 0) += dp[0] * dp[2];
              corr[vid](2, 1) += dp[1] * dp[2];
            }
          }
      gsl_matrix* mat = gsl_matrix_alloc(3, 3);
      gsl_vector* eval = gsl_vector_alloc(3);
      gsl_matrix* evec = gsl_matrix_alloc(3, 3);
      gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(3);
      mdxInfo << "Decompose matrices" << endl;
      // Eigen-decomposition of the matrices
      for(size_t vid = 0; vid < nb_values; ++vid) {
        if(sum[vid] == 0)
          continue;
    
        corr[vid](0, 1) = corr[vid](1, 0);
        corr[vid](0, 2) = corr[vid](2, 0);
        corr[vid](1, 2) = corr[vid](2, 1);
        corr[vid] /= sum[vid];
    
        for(int i = 0; i < 3; ++i)
          for(int j = 0; j < 3; ++j)
            gsl_matrix_set(mat, i, j, corr[vid](i, j));
        gsl_eigen_symmv(mat, eval, evec, w);
        gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_DESC);
    
        Point3d p1(gsl_matrix_get(evec, 0, 0), gsl_matrix_get(evec, 1, 0), gsl_matrix_get(evec, 2, 0));
        Point3d p2(gsl_matrix_get(evec, 0, 1), gsl_matrix_get(evec, 1, 1), gsl_matrix_get(evec, 2, 1));
        Point3d p3(gsl_matrix_get(evec, 0, 2), gsl_matrix_get(evec, 1, 2), gsl_matrix_get(evec, 2, 2));
        Point3d ev(gsl_vector_get(eval, 0), gsl_vector_get(eval, 1), gsl_vector_get(eval, 2));
        if((p1 ^ p2) * p3 < 0)
          p3 = -p3;
    
        // Correct eigen-vector for shape-factor
        if(correcting_factor.x() > 0)
          ev = multiply(Point3d(correcting_factor), map(sqrt, ev));
    
        // mdxInfo << "Size of the various dimension = " << ev << endl;
    
        result[vid].p1 = p1;
        result[vid].p2 = p2;
        result[vid].p3 = p3;
        result[vid].ev = ev;
        result[vid].mean = mean[vid];
        // mdxInfo << "Value: " << selection.value(vid) << " -- Major axis = " << p1 << " eigenvalues = "
        // << ev << endl;
      }
    
      if(correcting_factor.x() == 0) {
        mdxInfo << "Compute regions spans" << endl;
        std::vector<Point3d> min_pos(nb_values, Point3d(DBL_MAX));
        std::vector<Point3d> max_pos(nb_values, Point3d(-DBL_MAX));
        // Find the maximum span of each label along each direction
        k = 0;
        for(size_t z = 0; z < s.z(); ++z)
          for(size_t y = 0; y < s.y(); ++y)
            for(size_t x = 0; x < s.x(); ++x, ++k) {
              int vid = selection(data[k]);
              if(vid >= 0) {
                Point3d dp = stk->imageToWorld(Point3d(x, y, z));
                const PCAnalysisResult& res = result[vid];
                double p1 = dp * res.p1;
                double p2 = dp * res.p2;
                double p3 = dp * res.p3;
                Point3d &pmin = min_pos[vid];
                Point3d &pmax = max_pos[vid];
                if(p1 < pmin.x())
                  pmin.x() = p1;
                if(p1 > pmax.x())
                  pmax.x() = p1;
                if(p2 < pmin.y())
                  pmin.y() = p2;
                if(p2 > pmax.y())
                  pmax.y() = p2;
                if(p3 < pmin.z())
                  pmin.z() = p3;
                if(p3 > pmax.z())
                  pmax.z() = p3;
              }
            }
        for(size_t vid = 0; vid < nb_values; ++vid)
          if(sum[vid] > 0) {
            PCAnalysisResult& res = result[vid];
            Point3d pmin = min_pos[vid];
            Point3d pmax = max_pos[vid];
            res.ev = (pmax - pmin) / 2;
            res.mean = (pmax + pmin) / 2;
            res.mean = res.mean.x() * res.p1 + res.mean.y() * res.p2 + res.mean.z() * res.p3;
          }
      } else if(correcting_factor.x() < 0) {
        double percentile = -correcting_factor.x();
        mdxInfo << QString("Computing the %1% of voxels").arg(percentile * 100) << endl;
        std::vector<std::vector<double> > pos_p1(nb_values);
        std::vector<std::vector<double> > pos_p2(nb_values);
        std::vector<std::vector<double> > pos_p3(nb_values);
        k = 0;
        for(size_t z = 0; z < s.z(); ++z)
          for(size_t y = 0; y < s.y(); ++y)
            for(size_t x = 0; x < s.x(); ++x, ++k) {
              int vid = selection(data[k]);
              if(vid >= 0) {
                Point3d dp = stk->imageToWorld(Point3d(x, y, z));
                const PCAnalysisResult& res = result[vid];
                double p1 = dp * res.p1;
                double p2 = dp * res.p2;
                double p3 = dp * res.p3;
                pos_p1[vid].push_back(p1);
                pos_p2[vid].push_back(p2);
                pos_p3[vid].push_back(p3);
              }
            }
        for(size_t vid = 0; vid < nb_values; ++vid)
          if(sum[vid] > 0) {
            std::vector<double>& ps1 = pos_p1[vid];
            std::vector<double>& ps2 = pos_p2[vid];
            std::vector<double>& ps3 = pos_p3[vid];
    
            std::sort(ps1.begin(), ps1.end());
            std::sort(ps2.begin(), ps2.end());
            std::sort(ps3.begin(), ps3.end());
    
            int lower = (1 - percentile) / 2 * (ps1.size() - 1);
            int upper = ps1.size() - 1 - lower;
            // mdxInfo << "Positions for vid = " << vid << "(" << ps1.size() << ") = [" << lower << ";"
            // << upper << "]\n";
            PCAnalysisResult& res = result[vid];
            Point3d pmin(ps1[lower], ps2[lower], ps3[lower]);
            Point3d pmax(ps1[upper], ps2[upper], ps3[upper]);
            // mdxInfo << " -> " << pmin << " -- " << pmax << endl;
            res.ev = (pmax - pmin) / 2;
            res.mean = (pmax + pmin) / 2;
            res.mean = res.mean.x() * res.p1 + res.mean.y() * res.p2 + res.mean.z() * res.p3;
          }
      }
      gsl_eigen_symmv_free(w);
      gsl_vector_free(eval);
      gsl_matrix_free(evec);
      gsl_matrix_free(mat);
      return result;
    }
    
//     void addPCACuboidShape(int lab, const PCAnalysisResult& res, Mesh* mesh, int slices)
//     {
//       if(slices < 1)
//         return;
//       // mdxInfo << "Draw PCA for label " << lab << endl;
//       vvGraph& S = mesh->graph();
//       std::vector<vertex> vs(8, vertex(0));
//       Point3d center = res.mean;
//       for(int i = 0; i < 8; ++i) {
//         vertex v;
//         v->label = lab;
//         vs[i] = v;
//       }
//       Point3d p1 = res.ev[0] * res.p1;
//       Point3d p2 = res.ev[1] * res.p2;
//       Point3d p3 = res.ev[2] * res.p3;
    
//       vs[0]->pos = Point3d((center - p1 - p2 - p3));
//       vs[1]->pos = Point3d(center + p1 - p2 - p3);
//       vs[2]->pos = Point3d(center - p1 + p2 - p3);
//       vs[3]->pos = Point3d(center + p1 + p2 - p3);
//       vs[4]->pos = Point3d(center - p1 - p2 + p3);
//       vs[5]->pos = Point3d(center + p1 - p2 + p3);
//       vs[6]->pos = Point3d(center - p1 + p2 + p3);
//       vs[7]->pos = Point3d(center + p1 + p2 + p3);
    
//       std::vector<Point3u> triangles(12);
//       triangles[0] = Point3u(0, 1, 4);   // 1
//       triangles[1] = Point3u(1, 5, 4);
    
//       triangles[2] = Point3u(1, 3, 5);   // 2
//       triangles[3] = Point3u(3, 7, 5);
    
//       triangles[4] = Point3u(3, 2, 7);   // 3
//       triangles[5] = Point3u(2, 6, 7);
    
//       triangles[6] = Point3u(2, 0, 6);   // 4
//       triangles[7] = Point3u(0, 4, 6);
    
//       triangles[8] = Point3u(2, 3, 0);   // 5
//       triangles[9] = Point3u(3, 1, 0);
    
//       triangles[10] = Point3u(4, 5, 6);   // 6
//       triangles[11] = Point3u(5, 7, 6);
    
//       // mdxInfo << "Calling meshFromTriangles" << endl;
//       Mesh::meshFromTriangles(S, vs, triangles);
//     }
    
//     void addPCACylinderShape(int lab, const PCAnalysisResult& res, Mesh* mesh, int slices)
//     {
//       if(slices < 1)
//         return;
//       vvGraph& S = mesh->graph();
//       std::vector<vertex> vs(5 * slices + 2, vertex(0));
//       Point3d p1 = res.ev[0] * res.p1;
//       Point3d p2 = res.ev[1] * res.p2;
//       Point3d p3 = res.ev[2] * res.p3;
//       Point3d center = res.mean;
    
//       vertex c1, c2;
//       c1->label = c2->label = lab;
//       c1->pos = Point3d(center - p1);
//       c2->pos = Point3d(center + p1);
//       int nc1 = 5 * slices;
//       int nc2 = 5 * slices + 1;
//       vs[nc1] = c1;
//       vs[nc2] = c2;
    
//       double alpha = 2 * M_PI / slices;
//       for(int i = 0; i < slices; ++i) {
//         vertex v1, v2, v3, v4, v5;
//         v1->label = v2->label = v3->label = v4->label = v5->label = lab;
    
//         Point3d v1p = Point3d(-1, cos(i * alpha), sin(i * alpha));
//         v3->pos = v1->pos = Point3d(center + v1p.x() * p1 + v1p.y() * p2 + v1p.z() * p3);
//         Point3d v2p = Point3d(1, cos(i * alpha), sin(i * alpha));
//         v4->pos = v2->pos = Point3d(center + v2p.x() * p1 + v2p.y() * p2 + v2p.z() * p3);
    
//         v5->pos = Point3d(center + v2p.y() * p2 + v2p.z() * p3);
    
//         vs[i] = v1;
//         vs[slices + i] = v2;
//         vs[2 * slices + i] = v3;
//         vs[3 * slices + i] = v4;
//         vs[4 * slices + i] = v5;
//       }
    
//       std::vector<Point3u> triangles(6 * slices);
//       uint i = slices - 1;
//       for(int i1 = 0; i1 < slices; ++i1) {
//         int j = slices + i;
//         int j1 = slices + i1;
//         int k = 2 * slices + i;
//         int k1 = 2 * slices + i1;
//         triangles[i] = Point3u(nc1, i1, i);
//         triangles[i + slices] = Point3u(nc2, j, j1);
//         triangles[i + 2 * slices] = 2 * uint(slices) + Point3u(i, i1, k);
//         triangles[i + 3 * slices] = 2 * uint(slices) + Point3u(i1, k1, k);
//         triangles[i + 4 * slices] = 2 * uint(slices) + Point3u(k, k1, j);
//         triangles[i + 5 * slices] = 2 * uint(slices) + Point3u(k1, j1, j);
//         i = i1;
//       }
    
//       Mesh::meshFromTriangles(S, vs, triangles);
//     }
    
//     struct pair_hash {
//       int operator()(const std::pair<int, int>& p) const {
//         return p.first ^ p.second;
//       }
//     };
    
//     void addPCAEllipsoidShape(int lab, const PCAnalysisResult& res, Mesh* mesh, int subdiv)
//     {
//       static int generated_subdiv = 0;
//       static std::vector<Point3u> triangles;
//       static std::vector<Point3d> positions;
//       if(generated_subdiv != subdiv) {
//         generated_subdiv = subdiv;
//         if(subdiv < 0) {
//           triangles.clear();
//           positions.clear();
//           return;
//         } else {
//           positions.resize(12);
//           triangles.resize(20);
//           const double t = (1 + sqrt(5.)) / 2;
//           const double s = sqrt(1 + t * t);
//           positions[0] = Point3d(t, 1, 0) / s;
//           positions[1] = Point3d(-t, 1, 0) / s;
//           positions[2] = Point3d(t, -1, 0) / s;
//           positions[3] = Point3d(-t, -1, 0) / s;
//           positions[4] = Point3d(1, 0, t) / s;
//           positions[5] = Point3d(1, 0, -t) / s;
//           positions[6] = Point3d(-1, 0, t) / s;
//           positions[7] = Point3d(-1, 0, -t) / s;
//           positions[8] = Point3d(0, t, 1) / s;
//           positions[9] = Point3d(0, -t, 1) / s;
//           positions[10] = Point3d(0, t, -1) / s;
//           positions[11] = Point3d(0, -t, -1) / s;
    
//           triangles[0] = Point3u(0, 8, 4);
//           triangles[1] = Point3u(1, 10, 7);
//           triangles[2] = Point3u(2, 9, 11);
//           triangles[3] = Point3u(7, 3, 1);
//           triangles[4] = Point3u(0, 5, 10);
//           triangles[5] = Point3u(3, 9, 6);
//           triangles[6] = Point3u(3, 11, 9);
//           triangles[7] = Point3u(8, 6, 4);
//           triangles[8] = Point3u(2, 4, 9);
//           triangles[9] = Point3u(3, 7, 11);
//           triangles[10] = Point3u(4, 2, 0);
//           triangles[11] = Point3u(9, 4, 6);
//           triangles[12] = Point3u(2, 11, 5);
//           triangles[13] = Point3u(0, 10, 8);
//           triangles[14] = Point3u(5, 0, 2);
//           triangles[15] = Point3u(10, 5, 7);
//           triangles[16] = Point3u(1, 6, 8);
//           triangles[17] = Point3u(1, 8, 10);
//           triangles[18] = Point3u(6, 1, 3);
//           triangles[19] = Point3u(11, 7, 5);
    
//     #define MAKE_EDGE(i1, i2) (i1 < i2 ? std::make_pair(i1, i2) : std::make_pair(i2, i1))
    
//           // Subdivide triangles
//           for(size_t i = 0; i < (size_t)subdiv; ++i) {
//             typedef std::unordered_map<std::pair<int, int>, int, pair_hash> edge_map_t;
//             typedef edge_map_t::iterator edge_iterator;
//             edge_map_t edges;
//             std::vector<Point3u> new_triangles(4 * triangles.size());
//             for(size_t j = 0; j < triangles.size(); ++j) {
//               const Point3u tr = triangles[j];
//               int p1 = tr[0], p2 = tr[1], p3 = tr[2];
//               std::pair<int, int> e1 = MAKE_EDGE(p1, p2);
//               std::pair<int, int> e2 = MAKE_EDGE(p2, p3);
//               std::pair<int, int> e3 = MAKE_EDGE(p1, p3);
//               int n1, n2, n3;
//               edge_iterator found = edges.find(e1);
//               if(found == edges.end()) {
//                 n1 = positions.size();
//                 positions.push_back(normalized(positions[p1] + positions[p2]));
//                 edges[e1] = n1;
//               } else
//                 n1 = found->second;
//               found = edges.find(e2);
//               if(found == edges.end()) {
//                 n2 = positions.size();
//                 positions.push_back(normalized(positions[p2] + positions[p3]));
//                 edges[e2] = n2;
//               } else
//                 n2 = found->second;
//               found = edges.find(e3);
//               if(found == edges.end()) {
//                 n3 = positions.size();
//                 positions.push_back(normalized(positions[p3] + positions[p1]));
//                 edges[e3] = n3;
//               } else
//                 n3 = found->second;
//               new_triangles[4 * j] = Point3u(p1, n1, n3);
//               new_triangles[4 * j + 1] = Point3u(p2, n2, n1);
//               new_triangles[4 * j + 2] = Point3u(p3, n3, n2);
//               new_triangles[4 * j + 3] = Point3u(n1, n2, n3);
//             }
//             swap(triangles, new_triangles);
//           }
    
//           mdxInfo << "# triangles = " << triangles.size() << endl;
//           mdxInfo << "# points = " << positions.size() << endl;
    
//     #undef MAKE_EDGE
//         }
//       }
//       if(subdiv < 0)
//         return;
    
//       vvGraph& S = mesh->graph();
//       std::vector<vertex> vs(positions.size(), vertex(0));
//       Point3d p1 = res.ev[0] * res.p1;
//       Point3d p2 = res.ev[1] * res.p2;
//       Point3d p3 = res.ev[2] * res.p3;
//       Point3d center = res.mean;
    
//       for(size_t i = 0; i < vs.size(); ++i) {
//         Point3d p = positions[i];
//         vertex v;
//         v->pos = Point3d(center + p.x() * p1 + p.y() * p2 + p.z() * p3);
//         v->label = lab;
//         vs[i] = v;
//       }
    
//       Mesh::meshFromTriangles(S, vs, triangles);
//     }
//   }
  
//   bool PCAnalysis::run(Stack* stk, Store* store, QString filename, int threshold, QString shape,
//                               Point3d correcting_factor, int slices, bool& draw_result)
//   {
//     mdxInfo << "Start PCAnalysis" << endl;
  
//     const HVecUS& data = store->data();
//     // Move the stack w.r.t. the PCs
  
//     std::vector<PCAnalysisResult> result;
//     std::vector<ushort> labels;
//     if(store->labels()) {
//       std::unordered_set<ushort> labs;
//       forall(const ushort& l, data) {
//         if(l > 0)
//           labs.insert(l);
//       }
//       labels = std::vector<ushort>(labs.begin(), labs.end());
  
//       result = analyzePC(stk, data, SelectLabel(labels), correcting_factor);
//     } else
//       result = analyzePC(stk, data, SelectThreshold(threshold), correcting_factor);
  
//     if(shape == "Cuboids" or shape == "Cylinder" or shape == "Ellipsoids") {
//       mdxInfo << "Drawing " << shape << endl;
//       draw_result = true;
//       Mesh* mesh = this->mesh(stk->id());
//       mesh->reset();
//       void (*shape_fct)(int, const PCAnalysisResult&, Mesh*, int) = 0;
//       if(shape == "Cuboids")
//         shape_fct = addPCACuboidShape;
//       else if(shape == "Cylinder")
//         shape_fct = addPCACylinderShape;
//       else if(shape == "Ellipsoids")
//         shape_fct = addPCAEllipsoidShape;
//       for(size_t vid = 0; vid < labels.size(); ++vid) {
//         if(result[vid]) {
//           shape_fct(labels[vid], result[vid], mesh, slices);
//         }
//       }
//       // Clear memory used by addPCAEllipsoidShape
//       addPCAEllipsoidShape(0, PCAnalysisResult(), 0, -1);
//       setNormals(mesh->graph());
//       mesh->updateAll();
//     } else {
//       if(result.size() == 1) {
//         stk->trans().setPositionAndOrientation(qglviewer::Vec(0, 0, 0), qglviewer::Quaternion(0, 0, 0, 1));
//         stk->setShowTrans(true);
  
//         CuttingSurface* cf = cuttingSurface();
//         qglviewer::Frame& frame = cf->frame();
//         // mdxInfo << "Mean = " << mean << endl;
  
//         PCAnalysisResult res = result[0];
//         if(!res) {
//           setErrorMessage("Error, not result found, do you have any point?");
//           return false;
//         }
//         double mm[16] = { res.p1.x(), res.p2.x(), res.p3.x(), 0, res.p1.y(),   res.p2.y(),   res.p3.y(),   0,
//                           res.p1.z(), res.p2.z(), res.p3.z(), 0, res.mean.x(), res.mean.y(), res.mean.z(), 1 };
//         Matrix4d mm_gl(mm);
//         mdxInfo << "Transform matrix = " << mm_gl << endl;
//         frame.setFromMatrix(mm_gl.c_data());
  
//         // Now, set the planes
//         cf->setMode(CuttingSurface::THREE_AXIS);
//         cf->setSize(Point3d(res.ev));
//         cf->showGrid();
//       }
  
//       draw_result = false;
//     }
  
//     if(!filename.isEmpty()) {
//       QFile f(filename);
//       if(!f.open(QIODevice::WriteOnly)) {
//         setErrorMessage(QString("Error, cannot open file '%1' for writing.").arg(filename));
//         return false;
//       }
//       QTextStream ts(&f);
//       ts << "Image file," << store->file() << endl;
//       if(store->labels()) {
//         ts << QString("Label, pos.X (%1), pos.Y (%1), pos.Z (%1), e1.X, e1.Y, e1.Z, e2.X, e2.Y, e2.Z, "
//                       "r1 (%1), r2 (%1), r3 (%1)").arg(UM) << endl;
//         for(size_t vid = 0; vid < labels.size(); ++vid) {
//           const PCAnalysisResult& res = result[vid];
//           ts << labels[vid] << ", " << res.mean.x() << ", " << res.mean.y() << ", " << res.mean.z() << ", "
//              << res.p1.x() << ", " << res.p1.y() << ", " << res.p1.z() << ", " << res.p2.x() << ", " << res.p2.y()
//              << ", " << res.p2.z() << ", " << res.ev[0] << ", " << res.ev[1] << ", " << res.ev[2] << endl;
//         }
//       } else {
//         const PCAnalysisResult& res = result[0];
//         ts << "Threshold," << threshold << endl;
//         ts << "Radii";
//         for(int i = 0; i < 3; ++i)
//           ts << "," << res.ev[i];
//         ts << endl;
//         ts << "Center";
//         for(int i = 0; i < 3; ++i)
//           ts << "," << res.mean[i];
//         ts << "Eigenvectors" << endl;
//         ts << QString("X (%1), Y (%1), Z (%1)").arg(UM) << endl;
//         ts << res.p1.x() << "," << res.p1.y() << "," << res.p1.z() << endl;
//         ts << res.p2.x() << "," << res.p2.y() << "," << res.p2.z() << endl;
//         ts << res.p3.x() << "," << res.p3.y() << "," << res.p3.z() << endl;
//         ts << endl;
//       }
//       f.close();
//       mdxInfo << "Written result in file " << filename << endl;
//     }
  
//     return true;
//   }
  
//   REGISTER_PROCESS(PCAnalysis);

  bool PCAnalysis2D::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, IntSymTensorAttr &cellAxisAttr)
  {
    std::map<int,double> labelAreaMap;
    for(CCIndex f : cs.faces()) {
      auto &fIdx = indexAttr[f];
      int label = fIdx.label;
      if(label <= 0) 
        continue;
      labelAreaMap[label] += fIdx.measure;
    }

    std::set<int> allLabels = faceLabels(cs, indexAttr);
    std::map<int, int> labelEntryMap;
    std::map<int, std::vector<CCIndex> > labelCellBoundaryMap;
    std::map<int, Point3d> labelCenterMap;

    // get all cell boundaries
    for(int label : allLabels){
      std::vector<CCIndex> bound = cellBoundary(cs, indexAttr, label);
      labelCellBoundaryMap[label] = bound;
    }

    int nb_values = allLabels.size();
    std::vector<Matrix3d> corr(nb_values);
    std::vector<PCAnalysisResult> result(nb_values);

    int counter = 0;

    // main loop to go through all cells
    // get cell center, area and fill matrices for pca
    for(int label : allLabels) {
      std::vector<CCIndex> bound = labelCellBoundaryMap[label];

      Point3d cellCenter(0,0,0);

      for(uint i = 0; i < bound.size(); i++)
        cellCenter += indexAttr[bound[i]].pos;

      cellCenter /= bound.size();
      labelCenterMap[label] = cellCenter;

      double cellArea = labelAreaMap[label];

      int vid = counter;

      for(uint i = 0; i < bound.size()-1; i++) {
        CCIndex e1 = bound[i];
        CCIndex e2 = bound[bound.size()-1];
        //CCIndex e3 = bound[bound.size()-1];
        if(i > 0) 
          e2 = bound[i-1];

	// Point3d edgeCenter = (indexAttr[e1].pos + indexAttr[e2].pos)/2.;
	// double edgeLength = norm(indexAttr[e1].pos - indexAttr[e2].pos);

        Point3d triCenter = (indexAttr[e1].pos + indexAttr[e2].pos + cellCenter)/3.;

        double area = triangleArea(indexAttr[e1].pos, indexAttr[e2].pos, cellCenter);
        
        Point3d dp = triCenter - cellCenter;
        labelEntryMap[label] = vid;
        corr[vid](0, 0) += dp[0] * dp[0] * area;
        corr[vid](1, 1) += dp[1] * dp[1] * area;
        corr[vid](2, 2) += dp[2] * dp[2] * area;
        corr[vid](1, 0) += dp[0] * dp[1] * area;
        corr[vid](2, 0) += dp[0] * dp[2] * area;
        corr[vid](2, 1) += dp[1] * dp[2] * area;
      }
      corr[vid](0, 0) /= cellArea;
      corr[vid](1, 1) /= cellArea;
      corr[vid](2, 2) /= cellArea;
      corr[vid](1, 0) /= cellArea;
      corr[vid](2, 0) /= cellArea;
      corr[vid](2, 1) /= cellArea;

      counter++;
    }

    gsl_matrix* mat = gsl_matrix_alloc(3, 3);
    gsl_vector* eval = gsl_vector_alloc(3);
    gsl_matrix* evec = gsl_matrix_alloc(3, 3);
    gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(3);
    mdxInfo << "Decompose matrices" << endl;

    typedef std::pair<int, int> IntIntP;

    // Eigen-decomposition of the matrices
    forall(IntIntP p, labelEntryMap) {
      //if(sum[vid] == 0)
      //  continue;
      int vid = p.second;
  
      corr[vid](0, 1) = corr[vid](1, 0);
      corr[vid](0, 2) = corr[vid](2, 0);
      corr[vid](1, 2) = corr[vid](2, 1);
      corr[vid] /= labelCellBoundaryMap[p.first].size();
  
      for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
          gsl_matrix_set(mat, i, j, corr[vid](i, j));
      gsl_eigen_symmv(mat, eval, evec, w);
      gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_DESC);
  
      Point3d p1(gsl_matrix_get(evec, 0, 0), gsl_matrix_get(evec, 1, 0), gsl_matrix_get(evec, 2, 0));
      Point3d p2(gsl_matrix_get(evec, 0, 1), gsl_matrix_get(evec, 1, 1), gsl_matrix_get(evec, 2, 1));
      Point3d p3(gsl_matrix_get(evec, 0, 2), gsl_matrix_get(evec, 1, 2), gsl_matrix_get(evec, 2, 2));
      Point3d ev(gsl_vector_get(eval, 0), gsl_vector_get(eval, 1), gsl_vector_get(eval, 2));
      if((p1 ^ p2) * p3 < 0)
        p3 = -p3;
  
      // Correct eigen-vector for shape-factor
      //if(correcting_factor.x() > 0)
      //Point3d correcting_factor = (sqrt(3),sqrt(3),sqrt(3));
      Point3d correcting_factor;
      correcting_factor = sqrt(3);
      double (*sqrtd)(double) = &std::sqrt; // FIXME: this declaration allows the compiler to find right template of map()
      ev = Point3d(multiply(correcting_factor, map(sqrtd, Point3d(ev))));
      // mdxInfo << "Size of the various dimension = " << ev << endl;
  
      result[vid].p1 = p1;
      result[vid].p2 = p2;
      result[vid].p3 = p3;
      result[vid].ev = ev * /*sqrt(labelAreaMap[p.first])*/ sqrt(labelCellBoundaryMap[p.first].size());
      result[vid].mean = Point3d(labelCenterMap[p.first]);
      // mdxInfo << "Value: " << selection.value(vid) << " -- Major axis = " << p1 << " eigenvalues = "
      // << ev << endl;
    }
    
//    //if(correcting_factor.x() == 0) {
//      mdxInfo << "Compute regions spans" << endl;
//      std::vector<Point3d> min_pos(nb_values, Point3d(DBL_MAX));
//      std::vector<Point3d> max_pos(nb_values, Point3d(-DBL_MAX));
//      // Find the maximum span of each label along each direction

    gsl_eigen_symmv_free(w);
    gsl_vector_free(eval);
    gsl_matrix_free(evec);
    gsl_matrix_free(mat);

    for(auto &pr : labelEntryMap) {
      int vid = pr.second;
      const PCAnalysisResult &res = result[vid];
      auto &axis = cellAxisAttr[pr.first];
      axis.ev1() = res.p1;
      axis.ev2() = res.p2;
      axis.evals() = res.ev;
    }
    return true;
  } 
	REGISTER_PROCESS(PCAnalysis2D);

// Code for export, TODO shoudl be in export process
//     if(!filename.isEmpty()) {
//       QFile f(filename);
//       if(!f.open(QIODevice::WriteOnly)) {
//         setErrorMessage(QString("Error, cannot open file '%1' for writing.").arg(filename));
//         return false;
//       }
//       QTextStream ts(&f);
//       ts << "Image file," << endl;

//         ts << QString("Label, pos.X (%1), pos.Y (%1), pos.Z (%1), e1.X, e1.Y, e1.Z, e2.X, e2.Y, e2.Z, "
//                       "r1 (%1), r2 (%1), r3 (%1)").arg(UM) << endl;
//         forall(IntIntP p, labelEntryMap) {
//           int vid = p.second;
//           int label = p.first;
//           const PCAnalysisResult& res = result[vid];
//           ts << label << ", " << res.mean.x() << ", " << res.mean.y() << ", " << res.mean.z() << ", "
//              << res.p1.x() << ", " << res.p1.y() << ", " << res.p1.z() << ", " << res.p2.x() << ", " << res.p2.y()
//              << ", " << res.p2.z() << ", " << res.ev[0] << ", " << res.ev[1] << ", " << res.ev[2] << endl;
//         }

//       f.close();
//       mdxInfo << "Written result in file " << filename << endl;
//     }

// 	  // Fill in heat map table
// 		std::vector<double> vecValues; 
// 		if(displayHeatMap != "None") {
//       mesh->labelHeat().clear();
//       IntFloatAttr& labelHeatMap = mesh->labelHeat();
//       forall(const IntSymTensorPair &p, cellAxis) {
//         int cell = p.first;
//         SymmetricTensor tensor = p.second;
//         double value = 0;

//         if(displayHeatMap == "PCAMax")
//           value = max(tensor.evals()[0], tensor.evals()[1]);
//         if(displayHeatMap == "PCAMin")
//           value = min(tensor.evals()[0], tensor.evals()[1]);
//         if(displayHeatMap == "Ratio")
//           value = max(tensor.evals()[0], tensor.evals()[1])/min(tensor.evals()[0], tensor.evals()[1]);
//         if(displayHeatMap == "Product")
//           value = tensor.evals()[0] * tensor.evals()[1];

// 		  	if(displayHeatMap == "PCABezierX" or displayHeatMap == "PCABezierY" or displayHeatMap == "ShearPCABezier"){
// 		  		if(cellAxisBezier.size() == 0)
// 		  		  throw(QString("No cell axis bezier. Run 'Cell Axis Custom Directions' first."));
// 		  		if(cellAxisBezier.find(cell) == cellAxisBezier.end())
// 		  		  continue;
// 		  		tensor = cellAxisBezier.find(cell)->second;
//           if(displayHeatMap == "PCABezierX")
//             value = tensor.evals()[0];
//           if(displayHeatMap == "PCABezierY")
//             value = tensor.evals()[1];
//           if(displayHeatMap == "ShearPCABezier")
//             value = fabs(tensor.evals()[2]);
// 				}

//         labelHeatMap[cell] = value;
// 		   	vecValues.push_back(value);
// 			}
// 		}

//     // Find bounds for heatmap scale. 
// 		double lowBound = 0, highBound = 1; 
// 		// Manual scaling by user input
// 		if(scaleHeatMap == "Manual"){
// 		  lowBound  = rangeHeat.x();
// 		  highBound = rangeHeat.y();
// 		}
// 		// Automatic scaling
// 	  if(vecValues.size() != 0){
// 		  sort(vecValues.begin(), vecValues.end());
// 	  	// Default scaling: take min and max values
// 	  	if(scaleHeatMap == "None"){
// 	  	  lowBound = vecValues.front();
// 	  	  highBound = vecValues.back();
// 	  	}
// 	  	// Auto scale: take low 5 and high 5 percentile
// 	  	else if(scaleHeatMap == "Auto"){
// 	  		lowBound = *(vecValues.begin()+int(vecValues.size()*.05));
// 	  		highBound = *(vecValues.begin()+int(vecValues.size()*.95));
// 	  	}
// 		}
//     // Round the bounds values of heat map, to make sure we have the same bound in case 
//   	// of loading PCA on both meshes.
//     lowBound = floor(lowBound * 1000) / 1000;
//     highBound = ceil(highBound * 1000) / 1000;

//     mesh->setHeatMapBounds(Point2f(lowBound, highBound));
//     mesh->setHeatMapUnit("a.u.");
//     mesh->setShowLabel("Label Heat");
//     mesh->updateTriangles();

//     if(displayAxis != "None")
//       mesh->setShowAxis("Cell Axis");
//  
//    return true;
//  }
//  REGISTER_PROCESS(DisplayPCA);
}
