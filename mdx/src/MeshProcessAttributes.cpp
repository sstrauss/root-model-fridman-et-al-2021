//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessAttributes.hpp>
#include <Attributes.hpp>

#include <Information.hpp>

#include <QHash>
#include <QRegExp>
#include <QTreeWidget>
#include <QWidget>

namespace mdx
{  
  bool ManageAttributes::setUpTree(const QStringList &attr)
  { 
    ui.attrTreeWidget->clear();
    forall(const QString &text, attr) {
      QTreeWidgetItem *item = new QTreeWidgetItem(QStringList() << text << QString::number(attributes->size(text)));
			item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
      if(attributes->saveRequired(text))
        item->setCheckState(2, Qt::Checked);
      else
        item->setCheckState(2, Qt::Unchecked);
			ui.attrTreeWidget->addTopLevelItem(item);
    }
    ui.attrTreeWidget->sortItems(0, Qt::AscendingOrder);
    for(int i = 0; i < 3; i++)
      ui.attrTreeWidget->resizeColumnToContents(i);
    return true;
  }

  bool ManageAttributes::initialize(QWidget *parent)
  { 
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("Invalid mesh Id: %1").arg(parm("Mesh Id").toInt()));

    attributes = &mesh->attributes();
    QDialog dlg(parent);
    ui.setupUi(&dlg);
    QStringList attr = attributes->attrList();
    setUpTree(attr);
    connect(ui.attrTreeWidget, SIGNAL(itemClicked(QTreeWidgetItem *, int)),
            this, SLOT(on_attrTreeWidget_itemClicked(QTreeWidgetItem *, int )));
    connect(ui.clearPushButton, SIGNAL(clicked()), this, SLOT(on_clearPushButton_clicked()));
    connect(ui.clearAllPushButton, SIGNAL(clicked()), this, SLOT(on_clearAllPushButton_clicked()));
    connect(ui.savePushButton, SIGNAL(clicked()), this, SLOT(on_savePushButton_clicked()));
    dlg.exec();

    return true;
  }  

  void ManageAttributes::on_attrTreeWidget_itemClicked(QTreeWidgetItem *item, int column)
  { 
  }

  void ManageAttributes::on_clearPushButton_clicked()
  {
    for(auto item : ui.attrTreeWidget->selectedItems())
      attributes->erase(item->text(0));
    setUpTree(attributes->attrList());
  }

  void ManageAttributes::on_clearAllPushButton_clicked()
  {
    for(auto attr : attributes->attrListUnparsed())
      attributes->erase(attr);
    setUpTree(attributes->attrList());
  }
  void ManageAttributes::on_savePushButton_clicked(){
    for(int itemCount = 0; itemCount < ui.attrTreeWidget->topLevelItemCount(); ++itemCount){ 
      if(Qt::Checked == ui.attrTreeWidget->topLevelItem(itemCount)->checkState(2))
        attributes->saveRequired(ui.attrTreeWidget->topLevelItem(itemCount)->text(0)); 
    }
  }
  REGISTER_PROCESS(ManageAttributes);


 void SaveAttributesCSV::selectAll()
 {
   for(int itemCount = 0; itemCount < ui.attrMapTree->topLevelItemCount(); ++itemCount)
     ui.attrMapTree->topLevelItem(itemCount)->setCheckState(1, Qt::Checked);
 }

 void SaveAttributesCSV::unselectAll()
 {
   for(int itemCount = 0; itemCount < ui.attrMapTree->topLevelItemCount(); ++itemCount)
     ui.attrMapTree->topLevelItem(itemCount)->setCheckState(1, Qt::Unchecked);
 }


void fillTreeWidget(Mesh *mesh, QTreeWidget* tree)
{


    tree->clear();

   QString header = "Label";

   Attributes& attributes = mesh->attributes();
   QStringList attrList = attributes.attrList(QString("#Heat#Labels#Double#.*"));

   qDebug() << "fff " << "/" << attrList.size() << "\n";

   for(QString heat : attrList){
     qDebug() << "attr " << heat << "\n";

    QStringList s = heat.split("#");
    QString heatType = "Labels Double:" + s[4];

    QTreeWidgetItem *itemNew = new QTreeWidgetItem(QStringList() << heatType << "");
    itemNew->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
    itemNew->setCheckState(1, Qt::Unchecked);
    tree->addTopLevelItem(itemNew);

   }

   QStringList attrListCellAxis = attributes.attrList(QString("#Heat#Labels#Point3d#.*"));

   for(QString heat : attrListCellAxis){
     qDebug() << "attraxis " << heat << "\n";

    QStringList s = heat.split("#");
    QString heatType = "Labels Point3d:" + s[4];

    QTreeWidgetItem *itemNew = new QTreeWidgetItem(QStringList() << heatType << "");
    itemNew->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
    itemNew->setCheckState(1, Qt::Unchecked);
    tree->addTopLevelItem(itemNew);

   }


   // now the labeling
   QStringList labelingList = attributes.attrList(QString("#Labeling#.*"));

   for(QString heat : labelingList){
     qDebug() << "labeling " << heat << "\n";

    QStringList s = heat.split("#");
    QString heatType = "Labeling:" + s[2];

    QTreeWidgetItem *itemNew = new QTreeWidgetItem(QStringList() << heatType << "");
    itemNew->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
    itemNew->setCheckState(1, Qt::Unchecked);
    tree->addTopLevelItem(itemNew);

   }

   tree->sortItems(0, Qt::AscendingOrder);

  
}

// file dialogue for save data
bool SaveAttributesCSV::initialize(QWidget* parent)
 {
   Mesh *m = currentMesh();
   filename = parm("Filename");
   if(filename.isEmpty() and parent)
     filename = QFileDialog::getSaveFileName(0, "Choose spreadsheet file to save", QDir::currentPath(), "CSV files (*.csv)");
   if(filename.isEmpty())
     return false;
   if(!filename.endsWith(".csv", Qt::CaseInsensitive))
     filename += ".csv";

   if(!parent) return true;

   // now the new GUI
   QDialog dlg(parent);
   ui.setupUi(&dlg);

   connect(ui.selectAllButton, SIGNAL(clicked()), this, SLOT(selectAll()));
   connect(ui.unselectAllButton, SIGNAL(clicked()), this, SLOT(unselectAll()));
//
   fillTreeWidget(m,ui.attrMapTree);
//
   if(dlg.exec() == QDialog::Accepted){
     QStringList newList;
     for(int itemCount = 0; itemCount < ui.attrMapTree->topLevelItemCount(); ++itemCount){
       QString currentAttr = ui.attrMapTree->topLevelItem(itemCount)->text(0);
       if(Qt::Checked == ui.attrMapTree->topLevelItem(itemCount)->checkState(1))
         newList << currentAttr;
     }
     attrMapsToBeSaved = newList;

     qDebug() << "test " << attrMapsToBeSaved.size() << "/" << filename << "\n";;
     return true;
   }

   return false;
 }


//
 bool SaveAttributesCSV::run(Mesh &mesh)
 {
   
   QFile file(filename);
   if(!file.open(QIODevice::WriteOnly)) {
     setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
     return false;
   }
   QTextStream out(&file);


    QString header = "Label";

    qDebug() << "run " << attrMapsToBeSaved.size() << "/" << filename << "\n";


   std::set<int> allLabels;

   forall(QString s, attrMapsToBeSaved){
    qDebug() << "Save Attribute Maps to CSV File: " << s << "\n";
    QStringList sSplit = s.split(":");
    

    if(sSplit[0].startsWith("Labels Double")){
      header+= "," + sSplit[1];
      auto &heatMap = mesh.heatAttr<double>(sSplit[1], "Labels");
      forall(auto p, heatMap){
        allLabels.insert(p.first);
      }
    } else if(sSplit[0].startsWith("Labels Point3d")){
      header+= "," + sSplit[1] + ".x," + sSplit[1] + ".y," + sSplit[1] + ".z";
      auto &heatMap = mesh.heatAttr<Point3d>(sSplit[1], "Labels");
      forall(auto p, heatMap){
        allLabels.insert(p.first);
      }
    } else if(sSplit[0].startsWith("Labeling")){
      auto &labelMap = mesh.labelMap(sSplit[1]);
      header+= "," + sSplit[1];
      forall(auto p, labelMap){
        allLabels.insert(p.first);
      }
    }

   }

   out << header << endl;

   forall(int label, allLabels){
     if(label < 1) continue;
     QString data = QString::number(label);

     forall(QString s, attrMapsToBeSaved){
      QStringList sSplit = s.split(":");

      if(sSplit[0].startsWith("Labels Double")){
        auto &heatMap = mesh.heatAttr<double>(sSplit[1], "Labels");
        if(heatMap.find(label) == heatMap.end()){
           data += ",";
         } else {
           data += "," + QString::number(heatMap[label]);
         }
      } else if(sSplit[0].startsWith("Labels Point3d")){
        auto &heatMap = mesh.heatAttr<Point3d>(sSplit[1], "Labels");
        if(heatMap.find(label) == heatMap.end()){
           data += ",";
         } else {
           data += "," + QString::number(heatMap[label].x()) + "," + QString::number(heatMap[label].y()) + "," + QString::number(heatMap[label].z());
         }
      } else if(sSplit[0].startsWith("Labeling")){
        auto &labelMap = mesh.labelMap(sSplit[1]);
        if(labelMap.find(label) == labelMap.end()){
           data += ",";
         } else {
           data += "," + QString::number(labelMap[label]);
         }
      }

     }
     out << data << endl;
   }

   return true;
 }
 REGISTER_PROCESS(SaveAttributesCSV);


}
