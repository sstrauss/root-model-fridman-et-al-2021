#ifndef CONTOUR_HPP
#define CONTOUR_HPP
/**
 * \file Contour.hpp
 *
 * Defines the Contour class
 */

#include <Config.hpp>
#include <Vector.hpp>

#include <vector>

#include <QString>

namespace mdx
{
  /**
   * \class Contour Contour.hpp
   * \brief Contour utility class.
   *
   * The Contour class encapsulates open or closed cubic B-spline curves;
   * it can read curve specifications from
   * VLAB contour files of versions 1.01 to version 1.03,
   * though it cannot represent curves with endpoint interpolation
   * (introduced in file format 1.02).
   */
  class mdx_EXPORT Contour
  {
  public:
    /**
     * \fn Contour()
     * \brief Default constructor
     *
     * Creates a Contour which is not attached to a contour file.
     **/
    Contour() : closed(true) {}

    /**
     * \fn Contour()
     * \brief Filename constructor
     *
     * Creates a Contour which is read from the file at the given location.
     **/
    Contour(const QString &_filename) : fileName(_filename) { reread(); }

    Contour(const Contour&) = default;
    Contour(Contour&&) = default;

    Contour& operator=(const Contour&) = default;
    Contour& operator=(Contour&&) = default;

    /**
     * \fn Vector<3,double> operator()(double t) const
     * \brief Compute the curve point at the given parameter
     * \param t Curve parameter, between zero and one
     *
     * This calculates the 3-dimensional coordinates of the curve at time t,
     * where t is between zero and one.
     * If t is less then zero, it is set to zero.
     * If t is larger than one, it is set to one.
     **/
    inline Vector<3,double> operator()(double t) const { return point(t); }

    /**
     * \fn Vector<3,double> point(double t) const
     * \brief Compute the curve point at the given parameter
     * \param t Curve parameter, between zero and one
     *
     * This calculates the 3-dimensional coordinates of the curve at time t,
     * where t is between zero and one.
     * If t is less then zero, it is set to zero.
     * If t is larger than one, it is set to one.
     **/
    Vector<3,double> point(double t) const;

    /**
     * \fn Vector<3,double> tangent(double t, double dt = 0.01)
     * \brief Compute the tangent to the curve at the given parameter
     * \param t Parameter value for where to take the tangent
     * \param dt The interval size used for numerical evaluation
     *
     * If the derivative of the curve is defined at t,
     * the tangent is evaluated analytically and dt is not used.
     */
    Vector<3,double> tangent(double t, double dt = 0.01) const;

    /**
     * \fn double length(double a, double b, double dt = 0.01)
     * \brief Compute the arclength for a parameter interval
     * \param a Parameter for the start of the interval
     * \param b Parameter for the end of the interval
     * \param dt The step used for forward differencing
     *
     * It is expected that 0 <= a < b <= 1.
     **/
    double length(double a, double b, double dt = 0.01) const;

    /**
     * \fn double travel(double t, double b, double dt = 0.01)
     * \brief Compute the parameter after traveling a certain distance
     * \param t Parameter for the start of the interval
     * \param l The arc length distance to travel
     * \param dt The step used for forward differencing
     *
     * It is expected that t is in the interval [0,1]
     * and that l is positive.
     * If traveling the given distance would move off the curve,
     * parameter 1.0 will be returned.
     **/
    double travel(double t, double l, double dt = 0.01) const;

    /**
     * \fn bool reread()
     * \brief Reread the curve data from the associated file.
     *
     * Rereads the curve from the associated file.
     * If there is no associated file, this operation does nothing.
     * Returns false if an error was encountered while reading.
     */
    bool reread();

  private:
    QString fileName;
    bool closed;
    std::vector<Vector<3,double>> controlPoints;
  };
}

#endif // CONTOUR_NEW_HPP
