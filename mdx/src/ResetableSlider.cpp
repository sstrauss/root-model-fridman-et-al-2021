//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "ResetableSlider.hpp"

ResetableSlider::ResetableSlider(QWidget* parent) : QSlider(parent), default_value(0)
{
  init();
}

ResetableSlider::ResetableSlider(Qt::Orientation orientation, QWidget* parent)
  : QSlider(orientation, parent)
{
  init();
}

void ResetableSlider::mouseDoubleClickEvent(QMouseEvent*) {
  emit reset();
}

void ResetableSlider::setDefaultValue(int val)
{
  if(val <= maximum() and val >= minimum()) {
    default_value = val;
  }
}

void ResetableSlider::setValueAsDefault() {
  setDefaultValue(value());
}

void ResetableSlider::checkDefaultValue(int min, int max)
{
  if(default_value < min)
    default_value = min;
  else if(default_value > max)
    default_value = max;
}

void ResetableSlider::init()
{
  connect(this, SIGNAL(rangeChanged(int, int)), this, SLOT(checkDefaultValue(int, int)));
  connect(this, SIGNAL(reset()), this, SLOT(resetValue()));
}

void ResetableSlider::resetValue() {
  setValue(default_value);
}
