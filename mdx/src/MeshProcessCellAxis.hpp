//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESSS_CELLAXIS_HPP
#define MESH_PROCESSS_CELLAXIS_HPP

#include <Process.hpp>

// #include <MeshProcessCellMesh.hpp>

namespace mdx
{
//   ///\addtogroup MeshProcess
//   ///@{
//   /**
//    * \class SaveCellAxis <ProcessCellAxis.hpp>
//    */

  class SaveCellAxis : public Process 
  {
  public:
    SaveCellAxis(const Process& process) : Process(process)
    {
      setName("Mesh/Cell Axis/Cell Axis Save");
      setDesc("Save PDG, Fibril Orientations or Curvature to a spreadsheet file.");
      setIcon(QIcon(":/images/CellAxisSave.png"));

      addParm("Output File", "Path to output file", "");
    }

    bool initialize(QWidget* parent);
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw(QString("%1:run No mesh selected").arg(name()));

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw(QString("%1:run No cell complex").arg(name()));

      bool result = run(mesh, parm("Output File"));

      return result;
    }
    bool run(Mesh* mesh, const QString& filename);

  };


//   class mdxBase_EXPORT SaveCellAxis : public Process 
//   {
//   public:
//     SaveCellAxis(const Process& process) : Process(process) {}
  
//     bool initialize(QStringList& parms, QWidget* parent);
  
//     bool run(const QStringList &parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh, parms[0]);
//     }
  
//     bool run(Mesh* mesh, const QString& filename);
  
//     QString name() const { return "Mesh/Cell Axis/Cell Axis Save"; }
//     QString description() const {
//       return "Save PDG, Fibril Orientations or Curvature to a spreadsheet file."; }
//     QStringList parmNames() const { return QStringList() << "Output File"; }
//     QStringList parmDescs() const { return QStringList() << "Path to output file"; }
//     QStringList parmDefaults() const { return QStringList() << ""; }
//     QIcon icon() const { return QIcon(":/images/CellAxisSave.png"); }
//   };
  
//   /**
//    * \class LoadCellAxis <ProcessCellAxis.hpp>
//    */
  class LoadCellAxis : public Process 
  {
  public:
    LoadCellAxis(const Process& process) : Process(process)
    {
      setName("Mesh/Cell Axis/Cell Axis Load");
      setDesc("Load cell axis from a spreadsheet file.");
      setIcon(QIcon(":/images/CellAxisOpen.png"));

      addParm("Type", "Load PDG, Fibril Orientations or Curvature from a spreadsheet file.", "PDG", 
        QStringList() << "PDG" << "PCA" << "fibril" << "curvature" << "polarization");
      addParm("Input File", "Path to input file. If empty, a browser will open.", "");
    }

    bool initialize(QWidget* parent);
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw(QString("%1:run No mesh selected").arg(name()));

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw(QString("%1:run No cell complex").arg(name()));

      bool result = run(mesh, parm("Type"), parm("Input File"));

      return result;
    }
    bool run(Mesh* mesh, const QString& type, const QString& filename);

  };


//   class mdxBase_EXPORT LoadCellAxis : public Process {
//   public:
//     LoadCellAxis(const Process& process) : Process(process) {}
  
//     bool initialize(QStringList& parms, QWidget* parent);
  
//     bool run(const QStringList &parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh, parms[0], parms[1]);
//     }
  
//     bool run(Mesh* mesh, const QString& type, const QString& filename);
  
//     QString name() const { return "Mesh/Cell Axis/Cell Axis Load"; }
//     QString description() const { return "Load cell axis from a spreadsheet file."; }
//     QStringList parmNames() const { return QStringList() << "Type" << "Input File"; }
//     QStringList parmDescs() const {
//       return QStringList() << "Load PDG, Fibril Orientations or Curvature from a spreadsheet file."
//                            << "Path to input file. If empty, a browser will open."; }
//     QStringList parmDefaults() const { return QStringList() << "PDG" << ""; }
//     ParmChoiceMap parmChoice() const
//     {
//       ParmChoiceMap map;
//       map[0] = QStringList() << "PDG" << "PCA" << "fibril" << "curvature" << "polarization";
//       return map;
//     }
//     QIcon icon() const { return QIcon(":/images/CellAxisOpen.png"); }
//   };
  
//   /**
//    * \class ClearCellAxis <ProcessCellAxis.hpp>
//    *
//    * Clear the cell axis on the mesh
//    */
//   class mdxBase_EXPORT ClearCellAxis : public Process 
//   {
//   public:
//     ClearCellAxis(const Process& process) : Process(process) {}
  
//     bool run(const QStringList &parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh);
//     }
  
//     bool run(Mesh* mesh);
  
//     QString name() const { return "Mesh/Cell Axis/Cell Axis Clear"; }
//     QString description() const {
//       return "Remove any cell axis information from the current mesh."; }
//     QStringList parmNames() const { return QStringList(); }
//     QStringList parmDescs() const { return QStringList(); }
//     QIcon icon() const { return QIcon(":/images/CellAxisClear.png"); }
//   };


//   typedef std::map<int, Point3d> IntP3dMap;
//   typedef std::map<Point2i, Point3d> P2iP3dMap;

//   /**
//    * \class CustomizeDirections <MeshProcessCellAxis.hpp>
//    *
//    * align the cell axis to customly chosen directions of a bezier grid
//    */
//   class CustomizeDirections : public Process
//   {
//   public:
//     CustomizeDirections(const Process& process) : Process(process) { }

//     bool run(const QStringList& parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh, parms[0]);
//     }
//     bool run(Mesh *m, QString projectOnSurface);

//     QString name() const { return "Mesh/Cell Axis/Cell Axis Custom Directions"; }
//     QString description() const { return 
//       "Project the cell axis (e.g. PDGs, fibrils...) onto a 2D Bezier.\n"
//       "The new directions follow the lines of the specified Bezier.\n"
//       "Note that the Bezier and the mesh should be as parallel as possible."; }
//     QStringList parmNames() const { return QStringList() 
//       << "Project Bezier on Surface" ; }
//     QStringList parmDefaults() const { return QStringList() 
//       << "Yes" ; }
//     QIcon icon() const { return QIcon(":/images/PDG.png"); }

//     ParmChoiceMap parmChoice() const {
//       ParmChoiceMap map;
//       map[0] = booleanChoice();
//       return map;
//     }
//   };

//   /**
//    * \class CellAxisAttrMapExport <MeshProcessCellAxis.hpp>
//    *
//    * align the cell axis to customly chosen directions of a bezier grid
//    */
//   class CellAxisAttrMapExport : public Process
//   {
//   public:
//     CellAxisAttrMapExport(const Process& process) : Process(process) { }

//     bool run(const QStringList& parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh, parms[0], parms[1]);
//     }
//     bool run(Mesh *m, QString prefix, QString name);

//     QString name() const { return "Mesh/Cell Axis/Cell Axis Export To Attr Map"; }
//     QString description() const { return 
//       ""; }
//     QStringList parmNames() const { return QStringList() << "Prefix" << "Name"; }
//     QStringList parmDefaults() const { return QStringList() << "Measure Label Tensor /" << "Cell Axis PDG"; }
//     QIcon icon() const { return QIcon(":/images/PDG.png"); }
//   };

//   /**
//    * \class CellAxisAttrMapImport <MeshProcessCellAxis.hpp>
//    *
//    * align the cell axis to customly chosen directions of a bezier grid
//    */
//   class CellAxisAttrMapImport : public Process
//   {
//   public:
//     CellAxisAttrMapImport(const Process& process) : Process(process) { }

//     bool run(const QStringList& parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh, parms[0], parms[1]);
//     }
//     bool run(Mesh *m, QString type, QString name);

//     QString name() const { return "Mesh/Cell Axis/Cell Axis Import From Attr Map"; }
//     QString description() const { return 
//       ""; }
//     QStringList parmNames() const { return QStringList() << "Type" << "Name"; }
//     QStringList parmDefaults() const { return QStringList() << "PDG" << "Measure Label Tensor /Cell Axis PDG"; }
//     QIcon icon() const { return QIcon(":/images/PDG.png"); }
//     ParmChoiceMap parmChoice() const
//     {
//       ParmChoiceMap map;
//       map[0] = QStringList() << "PDG" << "PCA" << "fibril" << "curvature" << "polarization";
//       return map;
//     }
//   };
//   /**
//    * \class CellAxisAngle <MeshProcessCellAxis.hpp>
//    *
//    * 
//    */
//   class CellAxisAngle : public Process
//   {
//   public:
//     CellAxisAngle(const Process& process) : Process(process) { }

//     bool run(const QStringList& parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh, parms[0], parms[1], parms[2]);
//     }
//     bool run(Mesh *m, QString attrMap1, QString attrMap2, QString attrMapOut);

//     QString name() const { return "Mesh/Cell Axis/Cell Axis Angle"; }
//     QString description() const { return 
//       ""; }
//     QStringList parmNames() const { return QStringList() << "Attr Map 1" << "Attr Map 2" << "Attr Map Output"; }
//     QStringList parmDefaults() const { return QStringList() << "Measure Label Tensor AM1" << "Measure Label Tensor AM2" << "Measure Label Double AxisAngle"; }
//     QIcon icon() const { return QIcon(":/images/PDG.png"); }
//   };



//   ///@}
}
#endif
