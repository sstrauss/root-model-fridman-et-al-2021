//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MORPHOGRAPHX_H
#define MORPHOGRAPHX_H
typedef unsigned short ushort;
typedef unsigned int uint;

#include <Config.hpp>

#include <Misc.hpp>
#include <Geometry.hpp>
#include <CutSurf.hpp>
#include <Process.hpp>
#include <TaskEditDlg.hpp>
#include <Mesh.hpp>
#include <SystemCommand.hpp>

#include <string.h>
#include <QMutex>
#include <QTime>
#include <QPointer>

#include <ui_GUI.h>
#include <ui_EditParms.h>
#include <ui_ProcessDocs.h>

class QDragEnterEvent;
class QDropEvent;
class QCloseEvent;
class ProcessParmsModel;
class Library;
class QFileSystemWatcher;
class QAction;
class LabelEditorDlg;

/**
 * MorphoDynamX Namespace
 */
namespace mdx 
{
  class ProcessThread;

  class SetupProcess;
  class Process;
}

/** 
 * \class MorphoDynamX MorphoDynamX.hpp <MorphoGraphX.hpp>
 *
 * Main application window class
 */
class MorphoDynamX : public QMainWindow 
{
  Q_OBJECT

public:
  MorphoDynamX(QString appdir,QStringList addLibraries = QStringList());
  ~MorphoDynamX();

  static QString AppDir;
  static QLabel* ActiveStack;

  typedef mdx::Point3u Point3u;
  typedef mdx::Point3d Point3d;

  void setDebug(bool debug);

  /// Launch a process
  bool launchProcess(const QString& processName, QStringList& parms, 
               mdx::Process::ProcessAction processAction, bool useGUI = true);

  int activeStack() const;
  int activeMesh() const;
  QString getStackName(bool main, int num);
  QString getMeshName(int num);
  void setLoadFile(const QString& f) { _loadFilename = f; }
  bool runningProcess() const { return currentProcess != 0; }
  const mdx::Process* globalProcess() const;
  mdx::Process* globalProcess();
  static void writeProcessParms(const mdx::ProcessDefinition& def, QTextStream& pout);
  bool setFrameVis(const QString &name, ulong flags, const mdx::Point3d &pos, const mdx::Quaternion &orient); 
  bool setCameraVis(const mdx::Point3d &pos, const mdx::Quaternion &orient, const mdx::Point3d &center, double zoom); 

signals:
  void processFinished();
  void updateStatusBar(const QString&);

public slots:
  // Process communication
  void systemCommand(mdx::Process *proc, int command, QStringList parms);

  void modified(bool state = true);
  void storeParameters();

  // FIXME RSS Maybe these could all be done through execProcess()?
  void importMesh(int stack);
  void exportMesh(int stack);
  void resetMesh(int mesh);
  void loadMesh(int mesh);
  void saveMesh(int mesh);
  void importStack(int stack, bool main);
  void importStackSeries(int stack, bool main);
  void openStack(int stack, bool main);
  void saveStack(int stack, bool main);
  void exportStack(int stack, bool main);

  /// Launch a process
  void execProcess(QString process, QStringList parms);

  void clearBuffers(int stack);
  void updateActiveText();
  void updateCCData(int stack);

  void progStart(QString msg, int steps, bool allowCancel);
  void progAdvance(int step);
  void progSetMsg(QString msg);
  void progSetMax(int step);
  void progStop();

  // Shortcuts
  void toggleControlsTab();
  void toggleStackTabs();
  void toggleControlKeyInteraction();
  void toggleMainCheckBox();
  void toggleWorkCheckBox();

  //Attributes
  void manageAttributesMesh(int mesh);

  // Open session file
  void fileOpen(const QString& filename);
  void setModelPath(const QString& path);
  // Open any file, using extension to figure out which file type
  void autoOpen(const QString& filename = QString(), int stack = 0, bool main = true);
  void autoRun();
  void resetDefaultParameters();
  void copyProcessName();
  void renameStack1SurfSignal();
  void deleteStack1SurfSignal();
  void renameStack2SurfSignal();
  void deleteStack2SurfSignal();
  void editTasks();
  void reloadTasks();
  void debugDialog();
  void resetCutSurf();
  void setCutSurfControl();
  void resetClipControl(double sceneRadius);
  void changeSelectLabel(int lab);
  void updateSliderScale();
  void loadHeatChoices(int id);
  void loadSignalChoices(int id);
  void loadLabelingChoices(int id);

private slots:
  void stack1Unloaded();
  void stack2Unloaded();
  void changeStack1Size(const Point3u &size, const Point3d &step, const Point3d &origin);
  void changeStack2Size(const Point3u &size, const Point3d &step, const Point3d &origin);

  void rotate(bool checked);

  void processCommand(QTreeWidgetItem* current, QTreeWidgetItem* prev);
  void processTasksCommand(QTreeWidgetItem* current, QTreeWidgetItem* prev);
  void seedStack(bool val);
  void voxelEditRadius(int val) {
    mdx::ImgData::VoxelEditRadius = val;
  }
  void fillWorkData(bool val) {
    mdx::ImgData::FillWorkData = val;
  }

  void loadView(const QString& filename);
  void saveView(const QString& filename);

  void fileOpen();
  void fileSave();
  void fileSaveAs();
  void fileQuickSave();
  void exitMDX();

  void globalContrast();
  void globalBrightness();
  void globalShininess();
  void globalSpecular();

  void stack1Import();
  void stack1ImportSeries();
  void stack1Save();
  void stack1Open();
  void stack1Export();
  void stack1Reset();
  void stack2Import();
  void stack2ImportSeries();
  void stack2Save();
  void stack2Open();
  void stack2Export();
  void stack2Reset();

  void stack1WorkImport();
  void stack1WorkImportSeries();
  void stack1WorkSave();
  void stack1WorkOpen();
  void stack1WorkExport();
  void stack2WorkImport();
  void stack2WorkImportSeries();
  void stack2WorkSave();
  void stack2WorkOpen();
  void stack2WorkExport();

  void mesh1Import();
  void mesh1Export();
  void mesh1Load();
  void mesh1Save();
  void mesh1Reset();
  void mesh2Import();
  void mesh2Export();
  void mesh2Load();
  void mesh2Save();
  void mesh2Reset();

  void meshSelectAll();
  void meshClearSelection();
  void meshInvertSelection();
  void meshCopySelection();
  void meshPasteSelection();
  void meshAddCurrentLabel();
  void meshAddUnlabeled();
  void meshRemoveCurrentLabel();
  void changeCurrentSeed();

  void mesh1ManageAttributes();
  void mesh2ManageAttributes();

  void userManual();
  void doxygen();
  void qglViewerHelp();
  void about();

  void labelColor();
  void setLabelColor(QIcon& icon);

  //void on_Stack1DrawTabs_currentChanged(int tab);
  //void on_Stack2DrawTabs_currentChanged(int tab);

  void on_actionPickLabel_triggered(bool on);
  void on_actionAddNewSeed_triggered(bool on);
  void on_actionAddCurrentSeed_triggered(bool on);
  void on_actionDrawSignal_triggered(bool on);
  void on_actionGrabSeed_triggered(bool on);
  void on_actionFillLabel_triggered(bool on);
  void on_actionPickFillLabel_triggered(bool on);
  void on_actionLabelSelect_triggered(bool on);
  void on_actionRectSelectVertices_toggled(bool on);
  void on_actionRectSelectFaces_toggled(bool on);
  void on_actionRectSelectVolumes_toggled(bool on);
  void on_actionLassoSelectVertices_toggled(bool on);
  void on_actionLassoSelectFaces_toggled(bool on);
  void on_actionLassoSelectVolumes_toggled(bool on);
  void on_actionFaceSelect_toggled(bool on);
  void on_actionVolumeSelect_toggled(bool on);
  void on_actionConnectedSelect_triggered(bool on);

  void on_actionPickVolumeLabel_triggered(bool on);
  void on_actionFillVolumeLabel_triggered(bool on);
  void on_actionPickFillVolumeLabel_triggered(bool on);
  void on_actionVoxelEdit_triggered(bool on);
  void on_actionDeleteVolumeLabel_triggered(bool on);

  void on_actionRewind_triggered(bool on);
  void on_actionStop_triggered(bool on);
  void on_actionStep_triggered(bool on);
  void on_actionRun_triggered(bool on);

  void eraseSelect();
  void fillSelect();
  void deleteSelection();
  void editParms();
  void processDocs();
  void colorPalette();
  void labelsEdit(bool on);
  void editParmsSaveAs();
  void swapSurfaces();
  void stopProgressBar();

  void processCommandTab(int tab);

  void saveProcessesParameters();
  void recallProcessesParameters();
  void reloadProcesses();
  void processCommandFinished();

protected:
  void dragEnterEvent(QDragEnterEvent* event);
  void dropEvent(QDropEvent* event);
  void closeEvent(QCloseEvent* e);
  void saveSettings();

  QTime currentProcessTime;
  QTimer ccUpdateTimer;
  QString _loadFilename;

private:
  void loadControls();
  void connectControlsIn();
  void connectControlsOut();
  void readParms();
  void writeParms();
  void clearMeshSelect();
  void updateVBOs();
  void readProcessParms();
  void createSetupProcess();
  void updateCurrentTasks(const TaskEditDlg::tasks_t& saved_tasks);
  QTreeWidgetItem* getFolder(QString name, 
                   QHash<QString, QTreeWidgetItem*>& folders, QTreeWidget* tree);
  void createItem(const mdx::ProcessDefinition& def);
  void updateDefinitions(QMap<QString, mdx::ProcessDefinition>& procDefs,
                         mdx::ProcessFactoryPtr factory, const 
                              QMap<QString, mdx::ProcessDefinition>& oldProcDefs);
  void setProjectFile(const QString &f);

  void updateViewer();
  void loadView(mdx::Process *proc, QString fileName);
  void saveView(mdx::Process *proc, QString fileName);
  void setCurrentStack(mdx::Process *proc, bool isMain, int id);
  void takeSnapshot(mdx::Process *proc, QString fileName, 
        double overSampling, int width, int height, int quality, bool expandFrustum);
  void updateStateFromProcess(mdx::Process* proc, bool finishing = false);
  void setStatus(QString fileName, bool alsoPrint = true);

  QDialog* editParmsDlog;
  Ui_EditParmsDialog* editParmsDialog;
  Ui::MainWindow ui;

  // Confocal stack objects
  mdx::ImgData stack1, stack2;

  // Cutting surface
  mdx::CutSurf cutSurf;

  size_t CudaHoldMem;
  bool initalView;
  bool needSaving;

  ProcessParmsModel* parmsModel;
  QString currentProcessName;
  bool currentProcessInTasks;
  mdx::Process* currentProcess;
  mdx::SetupProcess* currentSetup;
  mdx::ProcessThread* processThread;
  QList<QFileInfo> addedLibFiles;
  QList<Library*> loadedLibs;
#ifdef WATCH_PROCESS_FOLDERS
  QFileSystemWatcher* processWatcher;
  QTimer* processReloadTimer;
#endif
  QAction* resetParametersAct;
  QAction* copyProcessNameAct;
  QAction* editTasksAct;

  QAction* renameStack1SurfSignalAct;
  QAction* deleteStack1SurfSignalAct;

  QAction* renameStack2SurfSignalAct;
  QAction* deleteStack2SurfSignalAct;

  QMap<QString, mdx::ProcessDefinition> savedProc;

  TaskEditDlg::tasks_t tasks;

  QPointer<LabelEditorDlg> labelEditorDlg;

  const QString& projectFile() const;
  QHash<QString, QTreeWidgetItem*> stackFolders, meshFolders, toolsFolders, modelFolders;
};

#endif
