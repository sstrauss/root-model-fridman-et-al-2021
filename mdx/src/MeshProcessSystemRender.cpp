//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MeshProcessSystemRender.hpp>
#include <CCUtils.hpp>
#include <Geometry.hpp>
#include <Triangulate.hpp>

extern "C" {
#include <triangle.h>
}

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
#  define FileDialogOptions 0
#endif

//#define DEBUG_CC_TIMING
// System processes for cell complexes
namespace mdx
{
  // Only called when initiated from GUI
  bool Render::initialize(QWidget *parent)
  {
    // Don't run if no GUI
    if(!parent)
      return true;

    if(parm("Mesh Id").isEmpty())
      mesh = currentMesh();
    else {
      int meshId = parm("Mesh Id").toInt();
      if(meshId >= 0)
        mesh = getMesh(meshId);
      else
        throw QString("Invalid mesh id: %1").arg(meshId);
    }
    if(!mesh)
      throw(QString("No current mesh: %1"));
    setParm("Mesh Id", QString::number(mesh->id()));
    
    ccName = parm("CC Name");
    if(ccName.isEmpty()) {
      if(mesh->ccName().isEmpty())
        throw QString("No current cell complex");
    
      ccName = mesh->ccName();
    }
    setParm("CC Name", ccName);

    DefaultChoices = stringToBool(parm("Default Choices"));
    SelectedEdges = stringToBool(parm("Selected Edges"));
    CellEdges = stringToBool(parm("Cell Edges"));
    FaceNormals = stringToBool(parm("Face Normals"));
    TriangleFan = stringToBool(parm("Triangle Fan"));
    PointSize = parm("Point Size").toDouble();
    LineWidth = parm("Line Width").toDouble();
    DefaultColor = stringToColorb(parm("Default Color"));

    // Set the render process name
    mesh->ccAttr(ccName, "RenderProcess") = name();

    // Flag a full redraw
    mesh->updateAll(ccName);

    return true;
  }

  bool Render::run()
  {
    if(ccName.isEmpty())
      throw(QString("Render::run No current cell complex"));
    if(!mesh)
      throw(QString("Render::run No current mesh"));

    return true;
  }

  bool Render::finalize(QWidget *parent)
  {
    // Don't run if no GUI
    if(!parent)
      return true;

    if(parm("Mesh Id").isEmpty()) {
      mdxInfo << "Render: mesh id empty" << endl;
      return false;
    }

    mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh) {
      mdxInfo << "Render: invalid mesh, id:" << parm("Mesh Id") << endl;
      return false;
    }
    ccName = parm("CC Name");
    if(ccName.isEmpty()) {
      mdxInfo << "Render: no ccName" << endl;
      return false;
    }

#ifdef DEBUG_CC_TIMING
    QTime vboTimer;
    vboTimer.start(); 
    float vboSec = 0, totSec = 0;
#endif

    DefaultChoices = stringToBool(parm("Default Choices"));
    SelectedEdges = stringToBool(parm("Selected Edges"));
    CellEdges = stringToBool(parm("Cell Edges"));
    FaceNormals = stringToBool(parm("Face Normals"));
    TriangleFan = stringToBool(parm("Triangle Fan"));
    PointSize = parm("Point Size").toDouble();
    LineWidth = parm("Line Width").toDouble();
    AxisOffset = parm("Axis Offset").toDouble();
    DefaultColor = stringToColorb(parm("Default Color"));

    // get the draw parms
    CCDrawParms &cdp = mesh->drawParms(ccName);
    // Setup the render choices
    defaultDrawChoices(*mesh, ccName);
    updateRenderChoices(*mesh, ccName);
    oldSignal = mesh->signal();
    oldHeat = mesh->heat();

#ifdef DEBUG_CC_TIMING
    vboSec = vboTimer.elapsed()/1000.0;
    mdxInfo << "Render Choices:" << vboSec << " seconds" << endl;
    vboTimer.restart(); 
    totSec += vboSec;
#endif

    // Recompute the maps if the topology has changed
    if(cdp.changed & CCDrawParms::TopologyChanged) {
      // If topology changes, so do positions and properties
      cdp.changed |= CCDrawParms::PositionsChanged | CCDrawParms::PropertiesChanged;
      topologyChanged(*mesh, ccName);
    }

#ifdef DEBUG_CC_TIMING
    vboSec = vboTimer.elapsed()/1000.0;
    mdxInfo << "Topology processed:" << vboSec << " seconds" << endl;
    vboTimer.restart(); 
    totSec += vboSec;
#endif

    // Store position and normal data if positions have changed
    if(cdp.changed & CCDrawParms::PositionsChanged)
      positionsChanged(*mesh, ccName);

#ifdef DEBUG_CC_TIMING
    vboSec = vboTimer.elapsed()/1000.0;
    mdxInfo << "Positions processed:" << vboSec << " seconds" << endl;
    vboTimer.restart(); 
    totSec += vboSec;
#endif

// RSS FIXME
if(cdp.changed & CCDrawParms::VolumeSelectChanged)
  cdp.changed |= CCDrawParms::PropertiesChanged;

    // Set color data if properties or topology have changed
    if(cdp.changed & CCDrawParms::PropertiesChanged) 
      propertiesChanged(*mesh, ccName);

#ifdef DEBUG_CC_TIMING
    vboSec = vboTimer.elapsed()/1000.0;
    mdxInfo << "Properties processed:" << vboSec << " seconds" << endl;
    vboTimer.restart(); 
    totSec += vboSec;
#endif

    // Call if only vertex selection has changed
    if(cdp.changed & CCDrawParms::VertexSelectChanged and not (cdp.changed & CCDrawParms::PropertiesChanged)) 
      vertexSelectChanged(*mesh, ccName); 

    // Call if only face selection has changed
    if(cdp.changed & CCDrawParms::FaceSelectChanged and not (cdp.changed & CCDrawParms::PropertiesChanged)) 
      faceSelectChanged(*mesh, ccName); 

    // Call if only volume selection has changed
    // RSS FIXME Doesn't work
    //if(cdp.changed & CCDrawParms::VolumeSelectChanged and not (cdp.changed & CCDrawParms::PropertiesChanged)) 
      //volumeSelectChanged(*mesh, ccName); 

    // Call if only face labeling has changed
    if(cdp.changed & CCDrawParms::FaceLabelChanged and not (cdp.changed & CCDrawParms::PropertiesChanged)) 
      faceLabelChanged(*mesh, ccName); 

    // Call when cell edges need to be updated
    if((cdp.changed & CCDrawParms::CellEdgesChanged) or (cdp.changed & CCDrawParms::PropertiesChanged)) 
      cellEdgesChanged(*mesh, ccName); 

    // Call when only selected positions change (usually from Gui)
    if((cdp.changed & CCDrawParms::SelectPositionsChanged) and not (cdp.changed & CCDrawParms::PositionsChanged)) 
      selectPositionsChanged(*mesh, ccName); 

    cdp.changed = 0;

#ifdef DEBUG_CC_TIMING
    vboSec = vboTimer.elapsed()/1000.0;
    totSec += vboSec;
    mdxInfo << "CC VBO create time:" << totSec << " seconds" << endl;
#endif
    return true;
  }

  // Rewind, this will reset the color maps to default
  bool Render::rewind(QWidget* parent)
  {
    if(ccName.isEmpty())
      throw(QString("Render::rewind No current cell complex"));
    if(!mesh)
      throw(QString("Render::rewind Mesh pointer null"));

    CCDrawParms &cdp = mesh->drawParms(ccName);

    defaultDrawChoices(*mesh, ccName);
    cdp.changed |= CCDrawParms::DrawChoicesChanged;
    updateRenderChoices(*mesh, ccName);

    // add something here to refresh colormaps!!!

    cdp.setGroupVisible("Vertices", false);
    cdp.setGroupVisible("Edges", false);
    cdp.setGroupVisible("Faces", true);

    // Set render process name
    mesh->ccAttr(ccName, "RenderProcess") = name();

    return true;
  }

  bool Render::defaultDrawChoices(Mesh &mesh, const QString &ccName)
  {
    CCStructure &cs = mesh.ccStructure(ccName);
    CCDrawParms &cdp = mesh.drawParms(ccName);

    size_t vertexCount = cs.vertices().size();
    size_t edgeCount = cs.edges().size();
    size_t faceCount = cs.faces().size();
    size_t volumeCount = cs.volumes().size();

    if(SelectedEdges)
    {
      if(edgeCount == 0) {
        cdp.eraseDrawChoice("Edges","Selected Edges");
      } else {
        cdp.createLineChoice("Edges", "Selected Edges", "", "SelectEdge", "Color", "Vertex");
      }
    }

    if(CellEdges)
    {
      if(edgeCount == 0 || faceCount == 0) {
        cdp.eraseDrawChoice("Edges","Cell Edges");
      } else {
        cdp.createLineChoice("Edges", "Cell Edges", "", "CellEdge", "Color", "Vertex");
      }
    }

    if(DefaultChoices)
    {
      // Our default vertex draw choices are Vertices and Selected Vertices,
      // both in render group Vertices.

      // First delete custom vertex choices, recreated later
      QStringList toDelete;
      for(const QString &dcName : cdp.drawChoiceList("Vertices")) {
          const DrawChoice &dc = cdp.drawChoice("Vertices",dcName);
        if(dc.dcType == DrawChoice::DCCustomVertex)
          toDelete.push_back(dcName);
      }
      for(const QString &toDel : toDelete)
        cdp.eraseDrawChoice("Vertices",toDel); 

      // Delete cell axis choices
      toDelete.clear();
      for(const QString &dcName : cdp.drawChoiceList("Cell Axis")) {
          const DrawChoice &dc = cdp.drawChoice("Cell Axis", dcName);
        if(dc.dcType == DrawChoice::DCCellAxis)
          toDelete.push_back(dcName);
      }
      for(const QString &toDel : toDelete)
        cdp.eraseDrawChoice("Cell Axis", toDel); 

      // Delete the axis choices
      toDelete.clear();
      for(const QString &dcName : cdp.drawChoiceList("Axis")) {
          const DrawChoice &dc = cdp.drawChoice("Axis", dcName);
        if(dc.dcType == DrawChoice::DCAxis)
          toDelete.push_back(dcName);
      }
      for(const QString &toDel : toDelete)
        cdp.eraseDrawChoice("Axis", toDel); 

      if(vertexCount == 0) {
        cdp.eraseDrawChoice("Vertices","Vertices");
        cdp.eraseDrawChoice("Vertices","Selected Vertices");
      } else {
        QString vertexColorMapName = QString("%1#Vertex").arg(ccName);
        cdp.createPointChoice("Vertices", "Vertices", vertexColorMapName,
                              "Vertex", "Color", "Vertex").renderChoice.plSize = PointSize;;
        cdp.createPointChoice("Vertices", "Selected Vertices", "SelectVertex",
                              "SelectVertex", "Color", "Vertex").renderChoice.plSize = PointSize;

        // We reset the vertex and selection colormaps if necessary.
        {
          ColorMap &colorMap = cdp.colorMap(vertexColorMapName);
          if(colorMap.colors.size() == 0)
          {
            colorMap.setColors("Single Cyan");
            colorMap.makeIndexMap();
          }
        }
        {
          ColorMap &colorMap = cdp.colorMap("SelectVertex");
          if(colorMap.colors.size() == 0)
          {
            colorMap.setColors("Single Red");
            colorMap.makeIndexMap();
          }
        }
        // Now create the custom vertex choices
        for(const QString &attr : mesh.customVertexAttrList(ccName)) {
          QString cvName = QString("CustomVertex#%1").arg(attr);
          DrawChoice &customVertexChoice = cdp.createPointChoice("Vertices", attr, 
                 mesh.customVertexColorMapName(ccName, attr), cvName, cvName, "Vertex", "Normal");
          
          customVertexChoice.renderChoice.plSize = mesh.customVertexPointSize(ccName, attr);
          customVertexChoice.dcType = DrawChoice::DCCustomVertex;
        }
        // Create the cell axis choices
        for(const QString &attr : mesh.cellAxisAttrList(ccName)) {
          QString cellAxisName = QString("CellAxis#%1#%2").arg(ccName).arg(attr);
          DrawChoice &cellAxisChoice = cdp.createLineChoice("Cell Axis", attr, 
                 mesh.cellAxisColorMapName(ccName, attr), cellAxisName, cellAxisName, cellAxisName);
          
          cellAxisChoice.renderChoice.plSize = mesh.cellAxisLineWidth(ccName, attr);
          cellAxisChoice.dcType = DrawChoice::DCCellAxis;
        }
        // Create the axis choices
        for(const QString &attr : mesh.axisAttrList(ccName)) {
          QString axisName = QString("Axis#%1#%2").arg(ccName).arg(attr);
          DrawChoice &axisChoice = cdp.createLineChoice("Axis", attr, 
                 mesh.axisColorMapName(ccName, attr), axisName, axisName, axisName);
          
          axisChoice.renderChoice.plSize = mesh.axisLineWidth(ccName, attr);
          axisChoice.dcType = DrawChoice::DCAxis;
        }
      }

      // Our only default edge draw choice is Edges.
      if(edgeCount == 0) {
        cdp.eraseDrawChoice("Edges","Edges");
      } else {
        // Edges use vertex color anyway
        //QString vertexColorMapName = QString("%1#Vertex").arg(ccName);
        QString vertexColorMapName = QString("#Vertex#%1#").arg(ccName);
        cdp.createLineChoice("Edges", "Edges", vertexColorMapName,
                             "EdgeVertex", "Color", "Vertex").renderChoice.plSize = LineWidth;
        // If we have edges, we have vertices so the vertex color map has been initialized above.
      }

      // Faces and Volumes can be drawn with Signal and Heat.
      // We will remove and recreate these DrawChoices.
      {
        // From faces: all signal, vertex signal, and heat choices
        QStringList toDelete;
        for(const QString &dcName : cdp.drawChoiceList("Faces")) {
          const DrawChoice &dc = cdp.drawChoice("Faces",dcName);
          if(dc.dcType == DrawChoice::DCLabel ||
             dc.dcType == DrawChoice::DCSignal ||
             dc.dcType == DrawChoice::DCVertexSignal ||
             dc.dcType == DrawChoice::DCHeat)
            toDelete.push_back(dcName);
        }
        for(const QString &toDel : toDelete)
          cdp.eraseDrawChoice("Faces", toDel);

        // From volumes: only signal and heat choices.
        toDelete.clear();
        for(const QString &dcName : cdp.drawChoiceList("Volumes"))
        {
          const DrawChoice &dc = cdp.drawChoice("Volumes", dcName);
          if(dc.dcType == DrawChoice::DCSignal ||
             dc.dcType == DrawChoice::DCHeat)
            toDelete.push_back(dcName);
        }
        for(const QString &toDel : toDelete)
           cdp.eraseDrawChoice("Volumes",toDel);
      }

      // Our default face draw choices are Labels, Signal and Heat on the face,
      // and Signal interpolated from the vertices.
      QString labeling = mesh.labeling();
      if(faceCount > 0) {
        // Labels
        DrawChoice &faceLabelChoice = cdp.createTriangleChoice("Faces", labeling, "Label",
                                                               "Face", "LabelColor", "Vertex", "Normal", "Clip");
        faceLabelChoice.dcType = DrawChoice::DCLabel;
        faceLabelChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);
        cdp.colorMap("Label").makeLabelMap();   // The label colormap is always the same.

        // Now we recreate the Signal and Heat choices.
        if(mesh.signalExists())
        {
          // Face signal
          DrawChoice &faceSignalChoice = cdp.createTriangleChoice
            ("Faces",mesh.signal(),mesh.signalColorMapName(),
             "Face","Signal","Vertex","Normal");
          faceSignalChoice.dcType = DrawChoice::DCSignal;
          faceSignalChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);

          // Vertex signal
          QString vertexSignalName = QString("Vertex %1").arg(mesh.signal());
          DrawChoice &faceVertexSignalChoice = cdp.createTriangleChoice
            ("Faces",vertexSignalName,mesh.signalColorMapName(),
             "FaceVertex","Signal","Vertex","Normal");
          faceVertexSignalChoice.dcType = DrawChoice::DCVertexSignal;
          faceVertexSignalChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);
        }

        if(mesh.heatExists())
        {
          DrawChoice &faceHeatChoice = cdp.createTriangleChoice
            ("Faces",mesh.heat(),mesh.heatColorMapName(),
             "Face","Heat","Vertex","Normal");
          faceHeatChoice.dcType = DrawChoice::DCHeat;
          faceHeatChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);
        }
      }

      // Our default volume draw choices are Labels, and Signal and Heat on the volume only.
      if(volumeCount == 0) {
        cdp.eraseDrawChoice("Volumes","Labels");
      } else {
        DrawChoice &volLabelChoice = cdp.createTriangleChoice
          ("Volumes", "Labels", "Label", "Volume", "LabelColor", "Vertex", "Normal", "Clip");
        volLabelChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);
        cdp.colorMap("Label").makeLabelMap();           // The label colormap is always the same.

        // Recreate the Signal and Heat choices.
        if(mesh.signalExists())
        {
          DrawChoice &volumeSignalChoice = cdp.createTriangleChoice
            ("Volumes",mesh.signal(),mesh.signalColorMapName(), "Volume","Signal","Vertex","Normal", "Clip");
          volumeSignalChoice.dcType = DrawChoice::DCSignal;
          volumeSignalChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);
        }

        if(mesh.heatExists())
        {
          DrawChoice &volumeHeatChoice = cdp.createTriangleChoice
            ("Volumes",mesh.heat(),mesh.heatColorMapName(), "Volume","Heat","Vertex","Normal", "Clip");
          volumeHeatChoice.dcType = DrawChoice::DCHeat;
          volumeHeatChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);
        }
      }
    }

    return true;
  }

  bool Render::updateRenderChoices(Mesh &mesh, const QString &ccName)
  {
    CCDrawParms &cdp = mesh.drawParms(ccName);
    QString labeling = mesh.labeling();

    bool newSignal = !oldSignal.isEmpty() && mesh.signalExists() && (mesh.signal() != oldSignal);
    bool newHeat = !oldHeat.isEmpty() && mesh.heatExists() && (mesh.heat() != oldHeat);

    QStringList oldGroups = cdp.groupList();
    for(const QString &groupName : oldGroups) {
      RenderGroup &rg = cdp.renderGroup(groupName);

      // If the render group has no DrawChoices,
      // it will have no RenderChoices so we make it disappear.
      if(rg.drawChoices.empty()) {
        cdp.eraseGroup(groupName);
        continue;
      }

      // There are two options here:
      // if DrawChoicesChanged, then we will completely regenerate the RenderChoices;
      // otherwise, we will only generate the new ones.
      if(cdp.changed & CCDrawParms::DrawChoicesChanged) {
        rg.renderChoices.clear();
        rg.choiceList.clear();
      } else {
        QStringList oldList = rg.choiceList;
        for(const QString &choiceName : oldList)
          if(rg.drawChoices.count(choiceName) == 0) {
            rg.renderChoices.erase(choiceName);
            rg.choiceList.removeAll(choiceName);
          }
      }

      // Now we add a RenderChoice for every DrawChoice without one.
      for(auto iter = rg.drawChoices.begin(); iter != rg.drawChoices.end(); iter++)
        if(!rg.choiceList.contains(iter->first))
          rg.renderChoices[iter->first] = iter->second.renderChoice;

      // Regenerate choiceList.
      rg.choiceList.clear();
      for(const std::pair<QString,RenderChoice> &pr : rg.renderChoices)
        rg.choiceList.push_back(pr.first);

      // Sanity check.
      if(rg.choiceList.empty())
        throw(QString("Unexpected empty RenderChoice list for group %1 in Render").arg(groupName));

      // Set a new currentRenderChoice if we've deleted the old one.
      if(!rg.choiceList.contains(rg.currentChoice)) {
        // Maintain a signal or heat choice.
        if(newSignal && rg.currentChoice == oldSignal)
            cdp.setRenderChoice(groupName,mesh.signal());
        else if(newSignal && rg.currentChoice == QString("Vertex %1").arg(oldSignal))
            cdp.setRenderChoice(groupName,QString("Vertex %1").arg(mesh.signal()));
        else if(newHeat && rg.currentChoice == oldHeat)
            cdp.setRenderChoice(groupName,mesh.heat());

        // There are defaults for Vertices, Edges, Faces, and Volumes.
        else if(groupName == "Vertices" && cdp.setRenderChoice("Vertices","Vertices"));
        else if(groupName == "Edges" && cdp.setRenderChoice("Edges","Edges"));
        else if(groupName == "Faces" && cdp.setRenderChoice("Faces",labeling));
        else if(groupName == "Volumes" && cdp.setRenderChoice("Volumes",labeling));

        // Otherwise we just use the first entry.
        else cdp.setRenderChoice(groupName,rg.choiceList.first());
      }
    }

    return true;
  }


// *** DRAW
//
//  // These are used to set up the default color maps
//  bool Render::setVertexColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset)
//  {
//    CCDrawParms &cdp = mesh.drawParms(ccName);
//    ColorMap &colorMap = cdp.colorMap(colorMapName);
//    return true;
//  }
//
//  bool Render::setFaceVertexColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset)
//  {
//    ColorMap &colorMap = mesh.drawParms(ccName).colorMap(colorMapName);
//    if(reset or colorMap.size() == 0) {
//      colorMap.setColors("Greyscale");
//      colorMap.makeRangeMap(mesh.signalBounds(), mesh.signalUnit());
//    }
//    return true;
//  }
//
//  bool Render::setFaceSignalColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset)
//  {
//    ColorMap &colorMap = mesh.drawParms(ccName).colorMap(colorMapName);
//    if(reset or colorMap.size() == 0) {
//      colorMap.setColors("Greyscale");
//      colorMap.makeRangeMap(mesh.signalBounds(), mesh.signalUnit());
//    }
//    return true;
//  }
//
//  bool Render::setFaceHeatColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset)
//  {
//    ColorMap &colorMap = mesh.drawParms(ccName).colorMap(colorMapName);
//    if(reset or colorMap.size() == 0) {
//      colorMap.setColors("Uniform Jet");
//      colorMap.makeRangeMap();
//    }
//    return true;
//  }
//
//  bool Render::setVolumeSignalColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset)
//  {
//    ColorMap &colorMap = mesh.drawParms(ccName).colorMap(colorMapName);
//    if(reset or colorMap.size() == 0) {
//      colorMap.setColors("Uniform Jet");
//      colorMap.makeRangeMap(mesh.signalBounds(), mesh.signalUnit());
//    }
//    return true;
//  }
//
//  bool Render::setVolumeHeatColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset)
//  {
//    ColorMap &colorMap = mesh.drawParms(ccName).colorMap(colorMapName);
//    if(reset or colorMap.size() == 0) {
//      colorMap.setColors("Uniform Jet");
//      colorMap.makeRangeMap();
//    }
//    return true;
//  }
//

//  Colorb Render::setVertexColor(CCIndex v, const ColorMap &colorMap)
//  {
//    return colorMap.getRawColor(0);
//  }

  Colorb Render::setFaceSignalColor(CCIndex f, VizAttribute<CCIndex> &signalAttr)
  {
    // Get signal for faces
    return signalAttr[f];
  }

//  Colorb Render::setFaceVertexColor(CCIndex v, const ColorMap &)
//  {
//    // Get signal from custom attribute
//    if(!signalAttr)
//      std::cout << "Render::setFaceVertexColor signalAttr is null" << std::endl;
//    else
//      return (*signalAttr)[v];
//
//    return Colorb();
//  }
//
//  Colorb Render::setFaceHeatColor(CCIndex f, const ColorMap &)
//  {
//    if(!indexAttr)
//      std::cout << "Render::setFaceHeatColor indexAttr is null" << std::endl;
//    else if(!mesh)
//      std::cout << "Render::setFaceHeatColor Mesh is null" << std::endl;
//    else if(!heatAttr)
//      std::cout << "Render::setFaceHeatColor heatAttr is null" << std::endl;
//    else
//      return (*heatAttr)[mesh->getLabel((*indexAttr)[f].label)];
//
//    return Colorb();
//  }
//
//  Colorb Render::setVolumeSignalColor(CCIndex l, const ColorMap &)
//  {
//    if(!signalAttr)
//      std::cout << "Render::setVolumeSignalColor signalAttr is null" << std::endl;
//    else
//      return (*signalAttr)[l];
//
//    return Colorb();
//  }
//
//  Colorb Render::setVolumeHeatColor(CCIndex f, const ColorMap &)
//  {
//    if(!indexAttr)
//      std::cout << "Render::setFaceHeatColor indexAttr is null" << std::endl;
//    else if(!mesh)
//      std::cout << "Render::setFaceHeatColor Mesh is null" << std::endl;
//    else if(!heatAttr)
//      std::cout << "Render::setFaceHeatColor heatAttr is null" << std::endl;
//    else
//      return (*heatAttr)[mesh->getLabel((*indexAttr)[f].label)];
//
//    return Colorb();
//  }

  bool Render::topologyChanged(Mesh &mesh, const QString &ccName)
  {
#ifdef DEBUG_CC_TIMING
    QTime ccTimer;
    float ccSec = 0;
    ccTimer.start(); 
#endif

    indexAttr = &mesh.indexAttr();
#ifdef DEBUG_CC_TIMING
    ccSec = ccTimer.elapsed()/1000.0;
    mdxInfo << "Topology Index Data:" << ccSec << " seconds" << endl;
    ccTimer.restart(); 
#endif

    CCDrawParms &cdp = mesh.drawParms(ccName);
    CCStructure &cs = mesh.ccStructure(ccName);

    // The automatically created element arrays are
    // 1. Vertices, color from the vertex
    auto &vertexElements = cdp.elementEA("Vertex");
    // 2. Edges, color interpolated from vertices
    auto &edgeInterpElements = cdp.elementEA("EdgeVertex");
    // 3. Faces, color from the face
    auto &faceElements = cdp.elementEA("Face");
    // 4. Faces, color interpolated from vertices
    auto &faceInterpElements = cdp.elementEA("FaceVertex");
    // 5. Volumes, color from the volume
    auto &volumeElements = cdp.elementEA("Volume");

    // We create the elements and fill in our vaInverseMap.
    const std::vector<CCIndex> &vertices = cs.vertices();
    cdp.vertexCount = vertices.size();
    const std::vector<CCIndex> &edges = cs.edges();
    cdp.edgeCount = edges.size();
    const std::vector<CCIndex> &faces = cs.faces();
    cdp.faceCount = faces.size();
    const std::vector<CCIndex> &volumes = cs.volumes();
    cdp.volumeCount = volumes.size();

#ifdef DEBUG_CC_TIMING
    ccSec = ccTimer.elapsed()/1000.0;
    mdxInfo << "Topology Setup:" << ccSec << " seconds" << endl;
    ccTimer.restart(); 
#endif

    // Add in vertex instances for vertices
    vertexElements.resize(cdp.vertexCount);
    cdp.vaInverseMap.resize(cdp.vertexCount);
    cdp.vertexMap.clear();
    #pragma omp parallel for
    for(uint i = 0; i < cdp.vertexCount; i++) {
      vertexElements[i] = i;
      cdp.vertexMap[vertices[i]] = i;

      // Map back to vertices
      cdp.vaInverseMap[i] = std::make_pair(vertices[i], vertices[i]);
    }
#ifdef DEBUG_CC_TIMING
    ccSec = ccTimer.elapsed()/1000.0;
    mdxInfo << "Topology vertices:" << ccSec << " seconds" << endl;
    ccTimer.restart(); 
#endif

    // Add in vertex instances for edges
    edgeInterpElements.resize(2 * cdp.edgeCount);
    cdp.vaInverseMap.resize(cdp.vertexCount + 2 * cdp.edgeCount);
    #pragma omp parallel for
    for(uint i = 0; i < cdp.edgeCount; i++) {
      try {  // Needed because the edgeBounds operation might fail (RSS why?)
        // Interp index for edges
        std::pair<CCIndex,CCIndex> ep = cs.edgeBounds(edges[i]);
        edgeInterpElements[i*2] = cdp.vertexMap[ep.first];
        edgeInterpElements[i*2+1] = cdp.vertexMap[ep.second];

        cdp.vaInverseMap[cdp.vertexCount + i*2] = std::make_pair(edges[i], ep.first);
        cdp.vaInverseMap[cdp.vertexCount + i*2 + 1] = std::make_pair(edges[i], ep.second);
      } catch(...) {
        printf("Error finding edge bounds\n");
      }
    } 
#ifdef DEBUG_CC_TIMING
    ccSec = ccTimer.elapsed()/1000.0;
    mdxInfo << "Topology edges:" << ccSec << " seconds" << endl;
    ccTimer.restart(); 
#endif

    // Add in vertex instances for faces
    uint ec2 = cdp.edgeCount * 2;
    cdp.faceStart = cdp.vaInverseMap.size();
    faceElements.clear();
    faceInterpElements.clear();
    cdp.uniqueColorVA.clear();

    // Grab the lists of vertices for each face
    std::vector<std::vector<CCIndex> > faceVtxs(cdp.faceCount); 
    #pragma omp parallel for
    for(uint i = 0; i < cdp.faceCount; i++)
      faceVtxs[i] = faceVertices(cs, faces[i]);

    // Find the starting points of the faces
    uint faceSize = 0, maxFaceSize = 0;
    for(uint i = 0; i < cdp.faceCount; i++) {
      CCIndex f = faces[i];
      cdp.faceMap[f] = cdp.faceStart + faceSize;
      cdp.vertexMap[f] = cdp.faceStart - ec2 + faceSize;
      uint facets = faceVtxs[i].size();
      if(facets < 3) {
        mdxInfo << "Bad face " << faces[i] << " with only " << facets << " edges" << endl;
        continue;
      } else if(facets == 3)
        faceSize += 3;                // triangle
      else if(TriangleFan)
        faceSize += facets * 3;       // triangle fan
      else
        faceSize += (facets - 2) * 3; // triangulation of polygon
      maxFaceSize = std::max(maxFaceSize, facets);
    }

    // Resize arrays
    faceElements.resize(faceSize);
    faceInterpElements.resize(faceSize);
    cdp.uniqueColorVA.resize(faceSize);
    cdp.vaInverseMap.resize(faceSize + cdp.vaInverseMap.size());

#ifdef DEBUG_CC_TIMING
    ccSec = ccTimer.elapsed()/1000.0;
    mdxInfo << "Topology face map:" << ccSec << " seconds" << endl;
    ccTimer.restart(); 
#endif

    // Do the faces
    #pragma omp parallel for
    for(uint i = 0; i < cdp.faceCount; i++) {
      CCIndex f = faces[i];

      // Get the vertices of the face
      const std::vector<CCIndex> &fVtxs = faceVtxs[i];
      uint facets = fVtxs.size();
      if(facets < 3)
        continue;

      uint fPos = cdp.faceMap[f] - cdp.faceStart;
      uint vtxPos = cdp.faceStart - ec2 + fPos;

      auto addFace = [&](CCIndex tv1, CCIndex tv2, CCIndex tv3) {
        // Face colors
        faceElements[fPos  ] = vtxPos++;
        faceElements[fPos+1] = vtxPos++;
        faceElements[fPos+2] = vtxPos++;

        // Interp vertex colors
        faceInterpElements[fPos  ] = cdp.vertexMap[tv1];
        faceInterpElements[fPos+1] = cdp.vertexMap[tv2];
        faceInterpElements[fPos+2] = cdp.vertexMap[tv3];

        // Index map back to faces
        cdp.vaInverseMap[cdp.faceStart+fPos  ] = std::make_pair(f,tv1);
        cdp.vaInverseMap[cdp.faceStart+fPos+1] = std::make_pair(f,tv2);
        cdp.vaInverseMap[cdp.faceStart+fPos+2] = std::make_pair(f,tv3);

        // Write unique colors for selection buffer
        cdp.uniqueColorVA[fPos++] = vMapColor(i);
        cdp.uniqueColorVA[fPos++] = vMapColor(i);
        cdp.uniqueColorVA[fPos++] = vMapColor(i);
      };

      if(facets == 3) {
        addFace(fVtxs[0],fVtxs[1],fVtxs[2]);
      } else if(TriangleFan) { // Render using triangle fan to centroid.
        for(uint fi = 0 ; fi < facets ; fi++) {
          uint fi1 = (fi+1) % facets;
          addFace(f,fVtxs[fi],fVtxs[fi1]);
        }
      } else { // Use Triangle to make a constrained Delaunay triangulation
        // Get vertex positions and rotate to XY-plane.
        std::vector<Point3d> pos(facets);
        #pragma omp parallel for
        for(uint fi = 0 ; fi < facets ; fi++)
          pos[fi] = (*indexAttr)[fVtxs[fi]].pos;
        Matrix3d invRot;
        rotatePointsIntoXY((*indexAttr)[f].nrml, pos, invRot);

        // Initialize Triangle data structures.
        triangulateio triIn, triOut;
        triIn.numberofpointattributes = 0;
        triIn.pointmarkerlist = NULL;
        triIn.segmentmarkerlist = NULL;
        triIn.numberofholes = triIn.numberofregions = 0;
        triIn.holelist = triIn.regionlist = NULL;

        triIn.numberofpoints = triIn.numberofsegments = facets;
        triIn.pointlist = new double[2 * facets];
        triIn.segmentlist = new int[2 * facets];
        triOut.trianglelist = new int[3 * facets];

        // Copy vertex positions into triangulateio structure.
        #pragma omp parallel for
        for(uint fi = 0 ; fi < facets ; fi++) {
          triIn.pointlist[2*fi  ] = pos[fi][0];
          triIn.pointlist[2*fi+1] = pos[fi][1];

          triIn.segmentlist[2*fi  ] = fi;
          triIn.segmentlist[2*fi+1] = fi+1;
        }
        triIn.segmentlist[2*facets-1] = 0;

        // Run triangle.
        triangulate((char*)"QzpNP",&triIn,&triOut,NULL);

        // Add each found triangle in turn.
        for(int t = 0, tidx = 0 ; t < triOut.numberoftriangles ; t++, tidx += 3) {
          addFace(fVtxs[triOut.trianglelist[tidx+2]],
                  fVtxs[triOut.trianglelist[tidx+1]],
                  fVtxs[triOut.trianglelist[tidx]]);
        }
        
        // Delete allocated triangulateio lists.
        delete[] triIn.pointlist;
        delete[] triIn.segmentlist;
        delete[] triOut.trianglelist;
      }
    }

#ifdef DEBUG_CC_TIMING
    ccSec = ccTimer.elapsed()/1000.0;
    mdxInfo << "Topology faces:" << ccSec << " seconds" << endl;
    ccTimer.restart(); 
#endif

    // Finally, add vertex instances for volumes
    volumeElements.clear();
    cdp.volumeFaces.clear();
    cdp.volumeStart = cdp.vaInverseMap.size();
    uint vtxPos = cdp.volumeStart - ec2;

    for(uint i = 0 ; i < cdp.volumeCount ; i++) {
      try {
        CCIndex vol = volumes[i];
        // get vertex indices from bounding face entries
        for(CCIndex face : cs.bounds(vol)) {
          uint numVertices = cs.incidentCells(face,0).size();
          if(numVertices < 3) 
            continue;
          uint numTriangles = numVertices - 2;
          if(TriangleFan && numVertices > 3) 
            numTriangles = numVertices;
          uint vaPos = cdp.faceMap[face];
          int numTriVs = 3 * numTriangles;

          // Triangle orientation depends on the orientation of the face to the volume.
          if(cs.ro(face,vol) == ccf::POS) {
            for(int fi = 0 ; fi < numTriVs ; fi++) {
              cdp.volumeFaces.emplace_back(face);
              volumeElements.emplace_back(vtxPos + fi);
            }
          } else  {
            for(int fi = numTriVs - 1 ; fi >= 0 ; --fi) {
              cdp.volumeFaces.emplace_back(face);
              volumeElements.emplace_back(vtxPos + fi);
            }
          }
          for(int fi = 0 ; fi < numTriVs ; fi++)
            cdp.vaInverseMap.emplace_back(vol,cdp.vaInverseMap[vaPos+fi].second);
          vtxPos += numTriVs;
        }
      } catch(...) { 
        printf("Error making volume arrays\n");
      }

#ifdef DEBUG_CC_TIMING
      ccSec = ccTimer.elapsed()/1000.0;
      mdxInfo << "Topology volumes:" << ccSec << " seconds" << endl;
      ccTimer.restart(); 
#endif

    }
    return true;
  }

  // Calculate the colors for the label and heat arrays
  void Render::faceLabelSignalHeatColor(Mesh &mesh, CCIndexData &fIdx, Colorb &signalColor, Colorb &labelColor, Colorb &heatColor)
  {
    if(mesh.getLabel(fIdx.label) <= 0) {
      labelColor = signalColor;
      heatColor = signalColor;
    } else if(mesh.blending()) {
      float s = trim(parm("Blending").toFloat(), 0.0f, 1.0f);
      Colorf signalColorf(signalColor, 1.0/255.0);
      Colorf labelColorf(labelColor, 1.0/255);
      labelColorf = labelColorf * s + signalColorf * (1.0 - s);
      labelColor = Colorb(labelColorf, 255.0);
      if(heatColor.a() != 0) {
        Colorf heatColorf(heatColor, 1.0/255.0);
        heatColorf = heatColorf * s + signalColorf * (1.0 - s);
        heatColor = Colorb(heatColorf, 255.0);
      }
    }
    if(fIdx.selected) {
      signalColor = selectionColor(signalColor);
      labelColor = selectionColor(labelColor);
      if(heatColor.a() != 0)
        heatColor = selectionColor(heatColor);
    }
  }

  // Called when the signal or color changes
  bool Render::propertiesChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    CCDrawParms &cdp = mesh.drawParms(ccName);
    CCStructure &cs = mesh.ccStructure(ccName);

    bool doSignal = mesh.signalExists();
    bool doHeat = mesh.heatExists();

    // Our only specified color maps are for vertices (selected and otherwise) (and labels)
    ColorMap &vertexColorMap = cdp.colorMap(QString("%1#Vertex").arg(ccName));
    ColorMap &vertexSelectColorMap = cdp.colorMap("SelectVertex");

    // The color arrays are shared for all element types.
    ColorbVec &colorVA = cdp.colorVA("Color");
    uint &colorVAid = cdp.colorVAid("Color");
    ColorbVec &labelColorVA = cdp.colorVA("LabelColor");
    uint &labelColorVAid = cdp.colorVAid("LabelColor");
    ColorbVec &signalColorVA = cdp.colorVA("Signal");
    uint &signalColorVAid = cdp.colorVAid("Signal");
    ColorbVec &heatColorVA = cdp.colorVA("Heat");
    uint &heatColorVAid = cdp.colorVAid("Heat");

    uint ec2 = cdp.edgeCount * 2;     // vtx arrays shorter -- edges don't have their own colors
    colorVA.resize(cdp.vaInverseMap.size() - ec2); // FIXME This one is only used for vertices, it should not need to be full sized
    labelColorVA.resize(cdp.vaInverseMap.size() - ec2);
    signalColorVA.resize(cdp.vaInverseMap.size() - ec2);
    heatColorVA.resize(cdp.vaInverseMap.size() - ec2);
    bool hasSelectVertex = false;
    bool hasSelectEdge = false;
    bool hasLabels = false;
    Colorb vertexSelectColor(vertexSelectColorMap.getRawColor(0));

    // Generate VizAttributes for signal and heat.
    std::unique_ptr<VizAttribute<CCIndex> > signalAttr;
    if(doSignal)
      signalAttr.reset(new VizAttribute<CCIndex>(mesh.signalVizAttribute()));
    std::unique_ptr<VizAttribute<int> > heatAttr;
    if(doHeat)
      heatAttr.reset(new VizAttribute<int>(mesh.heatVizAttribute()));

    // Default vertex colors
    #pragma omp parallel for
    for(uint i = 0 ; i < cdp.vertexCount; i++) {
      CCIndex v = cdp.vaInverseMap[i].first;
      CCIndexData &vIdx = (*indexAttr)[v];

      // Write the color for the vertices
      if(vIdx.selected) {
        if(!hasSelectVertex) {
          #pragma omp atomic write
          hasSelectVertex = true;
        }
        colorVA[i] = vertexSelectColor;
      } else
        colorVA[i] = vertexColorMap.getRawColor(0);

      if(doSignal)
        signalColorVA[i] = (*signalAttr)[v];
      else
        signalColorVA[i] = DefaultColor;
    }

    // Record colors for face labels, signal, heat
    #pragma omp parallel for
    for(uint i = cdp.faceStart; i < cdp.volumeStart; i++) {
      CCIndex f = cdp.vaInverseMap[i].first;
      CCIndexData &fIdx = (*indexAttr)[f];
      int label = mesh.getLabel(fIdx.label);
      if(!hasLabels and label > 0) {
        #pragma omp atomic write
        hasLabels = true;
      }

      // Write the color, selected faces are blended with grey
      uint pos = i - ec2;

      // RSS This could be optimized, same colors for the entire face, could go in 3s
      Colorb labelColor(LabelColors[label % LabelColors.size()], 255.0);
      Colorb signalColor{DefaultColor}, heatColor;
      if(doSignal)
        signalColor = setFaceSignalColor(f, *signalAttr);
      else
        signalColor = DefaultColor;
      if(doHeat)
        heatColor = (*heatAttr)[label];

      faceLabelSignalHeatColor(mesh, fIdx, signalColor, labelColor, heatColor);
      labelColorVA[pos] = labelColor;
      signalColorVA[pos] = signalColor;
      if(doHeat)
        heatColorVA[pos] = heatColor;

//      if(fIdx.selected)
//        labelColorVA[pos] = selectionColor(labelColor);
//      else
//        labelColorVA[pos] = labelColor;
//
//      if(doSignal) {
//        if(fIdx.selected)
//          signalColorVA[pos] = selectionColor(setFaceSignalColor(f, *signalAttr));
//        else
//          signalColorVA[pos] = setFaceSignalColor(f, *signalAttr);
//      }
//
//      if(doHeat) {
//        auto heatColor = (label > 0) ? (*heatAttr)[label] : setFaceSignalColor(f, *signalAttr);
//        if(fIdx.selected)
//          heatColorVA[pos] = selectionColor(heatColor);
//        else
//          heatColorVA[pos] = heatColor;
//      }
    }

    // Record colors for volume labels, signal, heat
    #pragma omp parallel for
    for(uint i = cdp.volumeStart ; i < cdp.vaInverseMap.size() ; i++) {
      CCIndex vol = cdp.vaInverseMap[i].first;
      CCIndexData &volIdx = (*indexAttr)[vol];
      int label = mesh.getLabel(volIdx.label);
      if(!hasLabels && label > 0) {
        #pragma omp atomic write
        hasLabels = true;
      }

      Colorb labelColor(DefaultColor);
      if(label > 0)
        labelColor = Colorb(LabelColors[label % LabelColors.size()], 255.0);

      uint pos = i - ec2;
      if(volIdx.selected)
        labelColorVA[pos] = selectionColor(labelColor);
      else
        labelColorVA[pos] = labelColor;

      Colorb signalColor{DefaultColor};
      if(doSignal)
        signalColor = (*signalAttr)[vol];
      if(volIdx.selected)
        signalColor = selectionColor(signalColor);
      signalColorVA[pos] = signalColor;

      if(doHeat) {
        int lbl = mesh.getLabel(volIdx.label);
        auto heatColor = (lbl > 0) ? (*heatAttr)[lbl] : (*signalAttr)[vol];
        if(volIdx.selected)
          heatColorVA[pos] = selectionColor(heatColor);
        else
          heatColorVA[pos] = heatColor;
      }
    }

    // Element array for selected vertices
    auto &vertexSelectElements = cdp.elementEA("SelectVertex");
    vertexSelectElements.clear();
    if(hasSelectVertex) {
      const std::vector<CCIndex> &vertices = cs.vertices();
      tbb::concurrent_vector<uint> vertexSelectE;
      vertexSelectE.reserve(vertices.size());
      #pragma omp parallel for
      for(uint i = 0; i < vertices.size(); i++) {
        CCIndexData &vIdx = (*indexAttr)[vertices[i]];
        if(vIdx.selected)
          vertexSelectE.push_back(i);
      }
      vertexSelectElements.resize(vertexSelectE.size());
      #pragma omp parallel for
      for(uint i = 0; i < vertexSelectE.size(); ++i)
        vertexSelectElements[i] = vertexSelectE[i];
    }

    // Element array for selected edges, if required
    auto &edgeSelectElements = cdp.elementEA("SelectEdge");
    edgeSelectElements.clear();
    if(SelectedEdges) {
      if(hasSelectEdge) {
        const std::vector<CCIndex> &edges = cs.edges();
        // Reserve more than we need, the memory must be contiguous
        tbb::concurrent_vector<uint> edgeSelectE;
        edgeSelectE.reserve(edges.size() * 2);
        #pragma omp parallel for
        for(uint i = 0; i < edges.size(); i++) {
          CCIndex e1 = cdp.vaInverseMap[cdp.vertexCount + i*2].second;
          CCIndexData &e1Idx = (*indexAttr)[e1];
          if(!e1Idx.selected)
            continue;
          CCIndex e2 = cdp.vaInverseMap[cdp.vertexCount + i*2 + 1].second;
          CCIndexData &e2Idx = (*indexAttr)[e2];
          if(!e2Idx.selected)
            continue;
  
          auto it = edgeSelectE.grow_by(2);
          *it++ = cdp.vertexMap[e1];
          *it = cdp.vertexMap[e2];
        }
        edgeSelectElements.resize(edgeSelectE.size());
        #pragma omp parallel for
        for(uint i = 0; i < edgeSelectE.size(); ++i)
          edgeSelectElements[i] = edgeSelectE[i];
      }
    }

    // Custom vertex choices
    std::vector<UIntVec *> customVertexElementEA;
    std::vector<ColorbVec *> customVertexColorVA;
    std::vector<uint *> customVertexColorVAid;
    std::vector<VizAttribute<CCIndex> > customVertexVizAttr;
    for(const QString &dcName : cdp.drawChoiceList("Vertices")) {
      const DrawChoice &dc = cdp.drawChoice("Vertices", dcName);
      if(dc.dcType != DrawChoice::DCCustomVertex)
        continue;
      QString cvName = QString("CustomVertex#%1").arg(dcName);
      customVertexElementEA.push_back(&cdp.elementEA(cvName));
      customVertexColorVA.push_back(&cdp.colorVA(cvName));
      customVertexColorVAid.push_back(&cdp.colorVAid(cvName));
      customVertexVizAttr.emplace_back(VizAttribute<CCIndex>(mesh.customVertexVizAttribute(ccName, dcName)));
    }
    bool doCustomVertex = customVertexElementEA.size() > 0;
    if(doCustomVertex) {
      for(uint j = 0; j < customVertexElementEA.size(); j++) {
        customVertexElementEA[j]->clear();
        // We only need the vertex part of this, but it needs to be the same size as the vertex array.
        customVertexColorVA[j]->resize(cdp.vaInverseMap.size() - ec2);
      }
      #pragma omp parallel for
      for(uint i = 0; i < customVertexElementEA.size(); i++)
        for(uint j = 0 ; j < cdp.vertexCount; j++) {
          CCIndex v = cdp.vaInverseMap[j].first;
          Colorb color = customVertexVizAttr[i][v];
          if(color.a() > 0) {
            customVertexElementEA[i]->push_back(j);
            (*customVertexColorVA[i])[j] = color;
          } 
        }
    }

    // Cell Axis
    QStringList cellAxisAttrList = mesh.cellAxisAttrList(ccName);
    bool doCellAxis = cellAxisAttrList.size() > 0;
    std::vector<UIntVec *> cellAxisElementEA;
    std::vector<ColorbVec *> cellAxisColorVA;
    std::vector<uint *> cellAxisColorVAid;
    std::vector<Point3fVec *> cellAxisVertexVA;
    std::vector<uint *> cellAxisVertexVAid;
    std::vector<IntSymTensorAttr *> cellAxisAttr;
    std::vector<ColorMap *> cellAxisColorMap;
    if(doCellAxis) {
      auto &centerNormalAttr = mesh.cellAxisCentersNormals(ccName);

      // Cell axis choices
      for(const QString &dcName : cdp.drawChoiceList("Cell Axis")) {
        const DrawChoice &dc = cdp.drawChoice("Cell Axis", dcName);
        if(dc.dcType != DrawChoice::DCCellAxis)
          continue;
        QString cellAxisName = QString("CellAxis#%1#%2").arg(ccName).arg(dcName);
        cellAxisElementEA.push_back(&cdp.elementEA(cellAxisName));
        cellAxisVertexVA.push_back(&cdp.vertexVA(cellAxisName));
        cellAxisVertexVAid.push_back(&cdp.vertexVAid(cellAxisName));
        cellAxisColorVA.push_back(&cdp.colorVA(cellAxisName));
        cellAxisColorVAid.push_back(&cdp.colorVAid(cellAxisName));
        cellAxisAttr.push_back(&mesh.cellAxisAttr(ccName, dcName));
        cellAxisColorMap.push_back(&mesh.cellAxisColorMap(ccName, dcName));
      }
      #pragma omp parallel for
      for(uint i = 0; i < cellAxisElementEA.size(); i++) {
        auto &elementEA = *cellAxisElementEA[i];
        auto &vertexVA = *cellAxisVertexVA[i];
        auto &colorVA = *cellAxisColorVA[i];
        auto &attr = *cellAxisAttr[i];
        auto &colorMap = *cellAxisColorMap[i];
        Point2d bounds[3];
        for(int j = 0; j < 3; j++)
          bounds[j] = colorMap.channelMap(j).bounds;
        elementEA.clear();
        vertexVA.clear();
        colorVA.clear();

        for(auto &pr : attr) {
          int label = pr.first;
          SymmetricTensor &cellAxis = pr.second;
          auto it = centerNormalAttr.find(label);
          if(it == centerNormalAttr.end())
            continue;
          auto &centerNormal = it->second;
          Point3f center(centerNormal[0]);
          Point3f normal(centerNormal[1]);
          for(uint j = 0; j < 3; j++) {
            if(!colorMap.isVisible(j))
              continue;
            if(bounds[j][0] == 0 or bounds[j][1] == 0)
              continue;
            if(cellAxis.evals()[j] == 0)
              continue;
            double scale = bounds[j][0]/bounds[j][1] * cellAxis.evals()[j];
            Point3f offset = AxisOffset * normal * scale;
            Point3f dir = j == 0 ? Point3f(cellAxis.ev1()) : j == 1 ? Point3f(cellAxis.ev2()) : Point3f(cellAxis.ev1() ^ cellAxis.ev2());
            dir *= scale;
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center);
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center + dir + offset);
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center);
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center - dir + offset);
            Colorb color = cellAxis.evals()[j] > 0 ? colorMap.getColor(j, 0) : colorMap.getColor(j, 1);
            colorVA.push_back(color); colorVA.push_back(color);
            colorVA.push_back(color); colorVA.push_back(color);
          }
        }
      }
    }

    // Axis
    QStringList axisAttrList = mesh.axisAttrList(ccName);
    bool doAxis = axisAttrList.size() > 0;
    std::vector<UIntVec *> axisElementEA;
    std::vector<ColorbVec *> axisColorVA;
    std::vector<uint *> axisColorVAid;
    std::vector<Point3fVec *> axisVertexVA;
    std::vector<uint *> axisVertexVAid;
    std::vector<CCIndexSymTensorAttr *> axisAttr;
    std::vector<ColorMap *> axisColorMap;
    if(doAxis) {
      // Axis choices
      for(const QString &dcName : cdp.drawChoiceList("Axis")) {
        const DrawChoice &dc = cdp.drawChoice("Axis", dcName);
        if(dc.dcType != DrawChoice::DCAxis)
          continue;
        QString axisName = QString("Axis#%1#%2").arg(ccName).arg(dcName);
        axisElementEA.push_back(&cdp.elementEA(axisName));
        axisVertexVA.push_back(&cdp.vertexVA(axisName));
        axisVertexVAid.push_back(&cdp.vertexVAid(axisName));
        axisColorVA.push_back(&cdp.colorVA(axisName));
        axisColorVAid.push_back(&cdp.colorVAid(axisName));
        axisAttr.push_back(&mesh.axisAttr(ccName, dcName));
        axisColorMap.push_back(&mesh.axisColorMap(ccName, dcName));
      }
      #pragma omp parallel for
      for(uint i = 0; i < axisElementEA.size(); i++) {
        auto &elementEA = *axisElementEA[i];
        auto &vertexVA = *axisVertexVA[i];
        auto &colorVA = *axisColorVA[i];
        auto &attr = *axisAttr[i];
        auto &colorMap = *axisColorMap[i];
        Point2d bounds[3];
        for(int j = 0; j < 3; j++)
          bounds[j] = colorMap.channelMap(j).bounds;
        elementEA.clear();
        vertexVA.clear();
        colorVA.clear();

        for(auto &pr : attr) {
          CCIndex f = pr.first;
          auto &fIdx = (*indexAttr)[f];
          SymmetricTensor &axis = pr.second;
          Point3f center(fIdx.pos);
          Point3f normal(fIdx.nrml);
          for(uint j = 0; j < 3; j++) {
            if(axis.evals()[j] == 0)
              continue;
            if(!colorMap.isVisible(j))
              continue;
            double scale = bounds[j][0]/bounds[j][1];
            Point3f dir = j == 0 ? Point3f(axis.ev1()) : j == 1 ? Point3f(axis.ev2()) : Point3f(axis.ev1() ^ axis.ev2());
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center);
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center + dir * axis.evals()[j] * scale + AxisOffset * normal);
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center);
            elementEA.push_back(vertexVA.size());
            vertexVA.push_back(center - dir * axis.evals()[j] * scale + AxisOffset * normal);
            Colorb color = axis.evals()[j] > 0 ? colorMap.getColor(j, 0) : colorMap.getColor(j, 1);
            colorVA.push_back(color); colorVA.push_back(color);
            colorVA.push_back(color); colorVA.push_back(color);
          }
        }
      }
    }

    // We also have to create / update color arrays for the attribute-based DrawChoices.
    QStringList attrColorBuffers;
    for(const auto &rgs : cdp.renderGroups)
    {
      const RenderGroup &rg = rgs.second;
      for(const auto &dcs : rg.drawChoices)
      {
        const DrawChoice &dc = dcs.second;
        if(dc.dcType == DrawChoice::DCCellAttribute)
        {
          // Get the color buffer we're supposed to use
          const QString &colors = dc.renderChoice.colors;
          if(!attrColorBuffers.contains(colors))
            attrColorBuffers.push_back(colors);
          ColorbVec &colorBuffer = cdp.colorVA(colors);
          // Get a VizAttribute for this DrawChoice
          VizAttribute<CCIndex> viz
            = mesh.vizAttribute<CCIndex>(dc.attributeName,dc.attributeType,dc.renderChoice.colorMap);
          // Evaluate viz at every cell.
          colorBuffer.resize(cdp.vaInverseMap.size() - ec2);
          #pragma omp parallel for
          for(uint i = 0 ; i < cdp.vertexCount ; i++)
            colorBuffer[i] = viz[cdp.vaInverseMap[i].first];
          #pragma omp parallel for
          for(uint i = cdp.faceStart ; i < cdp.vaInverseMap.size() ; i++)
            colorBuffer[i - ec2] = viz[cdp.vaInverseMap[i].first];
        }
        else if(dc.dcType == DrawChoice::DCLabelAttribute && hasLabels)
        {
          // Get the color buffer and VizAttribute
          const QString &colors = dc.renderChoice.colors;
          if(!attrColorBuffers.contains(colors))
            attrColorBuffers.push_back(colors);
          ColorbVec &colorBuffer = cdp.colorVA(colors);
          VizAttribute<int> viz
            = mesh.vizAttribute<int>(dc.attributeName,dc.attributeType,dc.renderChoice.colorMap);
          // Evaluate viz for the label of every face and volume
          colorBuffer.resize(cdp.vaInverseMap.size());
          #pragma omp parallel for
          for(uint i = cdp.faceStart ; i < cdp.vaInverseMap.size() ; i++) {
            CCIndexData &idxData = (*indexAttr)[cdp.vaInverseMap[i].first];
            colorBuffer[i - ec2] = viz[mesh.getLabel(idxData.label)];
          }
        }
      }
    }

    REPORT_GL_ERROR("propertiesChanged: start");

    cdp.vertexChanged.clear();
    cdp.edgeChanged.clear();
    cdp.faceChanged.clear();
    cdp.volumeChanged.clear();

    // Write to the card
    if(!colorVAid) 
      glfuncs->glGenBuffers(1, &colorVAid);
    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, colorVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      colorVA.size() * sizeof(Colorb), &colorVA[0], GL_STATIC_DRAW_ARB); 

    if(!labelColorVAid) 
      glfuncs->glGenBuffers(1, &labelColorVAid);
    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, labelColorVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      labelColorVA.size() * sizeof(Colorb), &labelColorVA[0], GL_STATIC_DRAW_ARB); 

    if(!cdp.uniqueColorVAid) 
      glfuncs->glGenBuffers(1, &cdp.uniqueColorVAid);
    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, cdp.uniqueColorVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      cdp.uniqueColorVA.size() * sizeof(Point3GLub), &cdp.uniqueColorVA[0], GL_STATIC_DRAW_ARB);

    if(!signalColorVAid) 
      glfuncs->glGenBuffers(1, &signalColorVAid);
    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, signalColorVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      signalColorVA.size() * sizeof(Colorb), &signalColorVA[0], GL_STATIC_DRAW_ARB); 

    if(doHeat) {
      if(!heatColorVAid) 
        glfuncs->glGenBuffers(1, &heatColorVAid);
      glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, heatColorVAid);
      glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
        heatColorVA.size() * sizeof(Colorb), &heatColorVA[0], GL_STATIC_DRAW_ARB); 
    }

    for(const QString &cbuf : attrColorBuffers) {
      ColorbVec &va = cdp.colorVA(cbuf);
      uint &vaid = cdp.colorVAid(cbuf);

      if(!vaid)
        glfuncs->glGenBuffers(1, &vaid);

      glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, vaid);
      glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
        va.size() * sizeof(Colorb), &va[0], GL_STATIC_DRAW_ARB); 
    }

    // Custom vertex attributes
    if(doCustomVertex) {
      for(uint i = 0; i < customVertexColorVA.size(); i++) {
        ColorbVec &va = *customVertexColorVA[i];
        uint *vaid = customVertexColorVAid[i];

        if(!*vaid)
          glfuncs->glGenBuffers(1, vaid);

        glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, *vaid);
        glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB, va.size() * sizeof(Colorb), &va[0], GL_STATIC_DRAW_ARB); 
      }
    }

    // Cell axis attributes
    if(doCellAxis) {
      for(uint i = 0; i < cellAxisVertexVA.size(); i++) {
        uint *vertexVAid = cellAxisVertexVAid[i];
        if(!*vertexVAid)
          glfuncs->glGenBuffers(1, vertexVAid);
        Point3fVec &vertexVA = *cellAxisVertexVA[i];
        glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, *vertexVAid);
        glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB, vertexVA.size() * sizeof(Point3f), &vertexVA[0], GL_STATIC_DRAW_ARB); 

        uint *colorVAid = cellAxisColorVAid[i];
        if(!*colorVAid)
          glfuncs->glGenBuffers(1, colorVAid);
        ColorbVec &colorVA = *cellAxisColorVA[i];
        glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, *colorVAid);
        glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB, colorVA.size() * sizeof(Colorb), &colorVA[0], GL_STATIC_DRAW_ARB); 
      }
    }

    // Axis attributes
    if(doAxis) {
      for(uint i = 0; i < axisVertexVA.size(); i++) {
        uint *vertexVAid = axisVertexVAid[i];
        if(!*vertexVAid)
          glfuncs->glGenBuffers(1, vertexVAid);
        Point3fVec &vertexVA = *axisVertexVA[i];
        glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, *vertexVAid);
        glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB, vertexVA.size() * sizeof(Point3f), &vertexVA[0], GL_STATIC_DRAW_ARB); 

        uint *colorVAid = axisColorVAid[i];
        if(!*colorVAid)
          glfuncs->glGenBuffers(1, colorVAid);
        ColorbVec &colorVA = *axisColorVA[i];
        glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, *colorVAid);
        glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB, colorVA.size() * sizeof(Colorb), &colorVA[0], GL_STATIC_DRAW_ARB); 
      }
    }
    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("propertiesChanged: end");

    return true;
  }

  // Called when position or normal changes
  bool Render::positionsChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    CCDrawParms &cdp = mesh.drawParms(ccName);

    // The standard geometry buffers are Vertex and Normal.
    uint &vertexVAid = cdp.vertexVAid("Vertex");
    std::vector<Point3f> &vertexVA = cdp.vertexVA("Vertex");
    uint &normalVAid = cdp.normalVAid("Normal");
    std::vector<Point3f> &normalVA = cdp.normalVA("Normal");
    uint &clipVAid = cdp.clipVAid("Clip");
    std::vector<Point3f> &clipVA = cdp.clipVA("Clip");

    uint ec2 = cdp.edgeCount * 2;   // vertex array vs vaInverseMap offset
    vertexVA.resize(cdp.vaInverseMap.size() - ec2);
    normalVA.resize(cdp.vaInverseMap.size() - ec2);
    clipVA.resize(cdp.vaInverseMap.size() - ec2);
    // Do vertices
    #pragma omp parallel for
    for(uint i = 0; i < cdp.vertexCount; i++) {
      const CCIndexData &vIdx = (*indexAttr)[cdp.vaInverseMap[i].second];
      vertexVA[i] = Point3f(vIdx.pos);
      normalVA[i] = Point3f(vIdx.nrml);
      clipVA[i] = Point3f(vIdx.pos);
    }

    // Now do faces, we skip the edges
    #pragma omp parallel for
    for(uint i = cdp.faceStart; i < cdp.volumeStart ; i++) {
      const CCIndexData &vIdx = (*indexAttr)[cdp.vaInverseMap[i].second];
      vertexVA[i - ec2] = Point3f(vIdx.pos);
// FIXME
const CCIndexData &fIdx = (*indexAttr)[cdp.vaInverseMap[i].first];
      if(FaceNormals) {
        //const CCIndexData &fIdx = (*indexAttr)[cdp.vaInverseMap[i].first];
        normalVA[i - ec2] = Point3f(fIdx.nrml);
      } else
        normalVA[i - ec2] = Point3f(vIdx.nrml);
      clipVA[i - ec2] = Point3f(fIdx.pos);
    }

    // Now volumes
    #pragma omp parallel for
    for(uint i = cdp.volumeStart; i < cdp.vaInverseMap.size() ; i++) {
      const CCIndexData &vIdx = (*indexAttr)[cdp.vaInverseMap[i].second];
      vertexVA[i - ec2] = Point3f(vIdx.pos);
      if(FaceNormals) {
        const CCIndexData &fIdx = (*indexAttr)[cdp.volumeFaces[i - cdp.volumeStart]];
        normalVA[i - ec2] = Point3f(fIdx.nrml);
      } else
        normalVA[i - ec2] = Point3f(vIdx.nrml);

      // For cell clipping
      const CCIndexData &cIdx = (*indexAttr)[cdp.vaInverseMap[i].first];
      clipVA[i - ec2] = Point3f(cIdx.pos);
    }

    // Write vertex data to the card
    if(!vertexVAid) 
      glfuncs->glGenBuffers(1, &vertexVAid);
    if(!normalVAid) 
      glfuncs->glGenBuffers(1, &normalVAid);
    if(!clipVAid) 
      glfuncs->glGenBuffers(1, &clipVAid);

    REPORT_GL_ERROR("positionsChanged: start");

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, vertexVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      vertexVA.size() * sizeof(Point3f), &vertexVA[0], GL_STATIC_DRAW_ARB); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, normalVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      normalVA.size() * sizeof(Point3f), &normalVA[0], GL_STATIC_DRAW_ARB); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, clipVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      clipVA.size() * sizeof(Point3f), &clipVA[0], GL_STATIC_DRAW_ARB); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("positionsChanged: end");

    // Record location of faces for uniqueColor
    cdp.uniqueVertexVAid = vertexVAid;
    cdp.uniqueVertexVA = &vertexVA[0];
    cdp.uniqueVertexVAptr = (char *)NULL + cdp.vertexCount * 12;

    return true;
  }

  // Called when set of selected vertices has changed
  // (note that propertiesChanged also does this,
  //  but the separate implementation is for efficiency on selection).
  bool Render::vertexSelectChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    CCDrawParms &cdp = mesh.drawParms(ccName);
    if(cdp.vertexChanged.size() == 0)
      return true;

    CCStructure &cs = mesh.ccStructure(ccName);

    // Get color and element arrays
    std::vector<Colorb> &colorVA = cdp.colorVA("Color");
    uint &colorVAid = cdp.colorVAid("Color");
    std::vector<Colorb> &labelColorVA = cdp.colorVA("LabelColor");
    uint &labelColorVAid = cdp.colorVAid("LabelColor");
    auto &vertexSelectElements = cdp.elementEA("SelectVertex");
    auto &edgeSelectElements = cdp.elementEA("SelectEdge");

    // If there is no data return
    if(!glfuncs->glIsBuffer(colorVAid))
      return false;

    // Get the colormaps
    QString vertexColorMapName = QString("%1#Vertex").arg(ccName);
    ColorMap &vertexColorMap = cdp.colorMap(vertexColorMapName);
    ColorMap &vertexSelectColorMap = cdp.colorMap("SelectVertex");

    bool hasSelectVertex = false;
    Colorb vertexSelectColor(vertexSelectColorMap.getRawColor(0));
    CCIndexVec vertices = cs.vertices();
    uint min = cdp.vertexCount, max = 0;

    #pragma omp parallel for reduction(min:min) reduction(max:max)
    for(uint i = 0; i < cdp.vertexChanged.size(); i++) {
      CCIndex v = cdp.vertexChanged[i];
      CCIndexData &vIdx = (*indexAttr)[v];

      uint vPos = cdp.vertexMap[v];
      if(min > vPos)
        min = vPos;
      if(max < vPos)
        max = vPos;
      if(vIdx.selected) {
        if(!hasSelectVertex) {
          #pragma omp atomic write
          hasSelectVertex = true;
        }
        colorVA[vPos] = vertexSelectColor;
      } else
        colorVA[vPos] = vertexColorMap.getRawColor(0);
    }
    cdp.vertexChanged.clear();

    REPORT_GL_ERROR("vertexSelectChanged: start");

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, colorVAid);
    glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &colorVA[min]); 
    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, labelColorVAid);
    glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &labelColorVA[min]); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("vertexSelectChanged: end");

    // Create element array for selected vertices
    vertexSelectElements.clear();
    cdp.vertexSelected.clear();
    if(hasSelectVertex) {
      // Reserve more than we need, the memory must be contiguous
      vertexSelectElements.reserve(vertices.size());
      for(uint i = 0; i < vertices.size(); i++) {
        CCIndex v = vertices[i];
        if((*indexAttr)[v].selected) {
          vertexSelectElements.emplace_back(i);
          cdp.vertexSelected.push_back(v);
        }
      }
    }

    // Create element array for selected edges if required
    edgeSelectElements.clear();
    if(SelectedEdges) {
      if(hasSelectVertex) {
        const std::vector<CCIndex> &edges = cs.edges();
        // Reserve more than we need, the memory must be contiguous
        edgeSelectElements.reserve(edges.size() * 2);
        for(uint i = 0; i < edges.size(); i++) {
          CCIndex e1 = cdp.vaInverseMap[cdp.vertexCount + i*2].second;
          CCIndexData &e1Idx = (*indexAttr)[e1];
          if(!e1Idx.selected)
            continue;
          CCIndex e2 = cdp.vaInverseMap[cdp.vertexCount + i*2 + 1].second;
          CCIndexData &e2Idx = (*indexAttr)[e2];
          if(!e2Idx.selected)
            continue;

          // Make sure they are contiguous
          edgeSelectElements.emplace_back(cdp.vertexMap[e1]);
          edgeSelectElements.emplace_back(cdp.vertexMap[e2]);
        }
      }
    }
 
    return true;
  }

  // Called when the face selection changes.
  bool Render::faceSelectChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    CCDrawParms &cdp = mesh.drawParms(ccName);
    if(cdp.faceChanged.size() == 0)
      return true;

    bool doSignal = mesh.signalExists();
    bool doHeat = mesh.heatExists();

    // Generate VizAttributes for signal and heat.
    std::unique_ptr<VizAttribute<CCIndex> > signalAttr;
    if(doSignal)
      signalAttr.reset(new VizAttribute<CCIndex>(mesh.signalVizAttribute()));
    std::unique_ptr<VizAttribute<int> > heatAttr;
    if(doHeat)
      heatAttr.reset(new VizAttribute<int>(mesh.heatVizAttribute()));

    // Get color arrays
    std::vector<Colorb> &labelColorVA = cdp.colorVA("LabelColor");
    uint &labelColorVAid = cdp.colorVAid("LabelColor");
    ColorbVec &signalColorVA = cdp.colorVA("Signal");
    uint &signalColorVAid = cdp.colorVAid("Signal");
    ColorbVec &heatColorVA = cdp.colorVA("Heat");
    uint &heatColorVAid = cdp.colorVAid("Heat");

    // If there is no data return
    if(!glfuncs->glIsBuffer(labelColorVAid))
      return false;
    uint min = cdp.vaInverseMap.size(), max = 0;
    for(uint i = 0; i < cdp.faceChanged.size(); i++) {
      CCIndex f = cdp.faceChanged[i];
      CCIndexData &fIdx = (*indexAttr)[f];

      int label = mesh.getLabel(fIdx.label);

      Colorb labelColor(LabelColors[label % LabelColors.size()], 255.0);
      Colorb signalColor, heatColor;
      if(doSignal)
        signalColor = setFaceSignalColor(f, *signalAttr);
      else
        signalColor = DefaultColor;
      if(doHeat)
        heatColor = (*heatAttr)[label];
      faceLabelSignalHeatColor(mesh, fIdx, signalColor, labelColor, heatColor);

      uint fPos = cdp.faceMap[f];
      uint pos = fPos - 2 * cdp.edgeCount;

      // Update min range of indices for update
      if(min > pos)
        min = pos;
      while(fPos < cdp.vaInverseMap.size() and cdp.vaInverseMap[fPos].first == f) {
        // Write the color, selected faces are blended with grey
        labelColorVA[pos] = labelColor;

        if(doSignal)
          signalColorVA[pos] = signalColor;
        if(doHeat)
          heatColorVA[pos] = heatColor;

        pos++; fPos++;
      }
      // Update max range of indices for update
      if(max < pos - 1)
        max = pos - 1;
    }
    cdp.faceChanged.clear();

    REPORT_GL_ERROR("faceSelectChanged: start");

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, labelColorVAid);
    glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &labelColorVA[min]); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, signalColorVAid);
    glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &signalColorVA[min]); 

    if(doHeat) {
      glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, heatColorVAid);
      glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &heatColorVA[min]); 
    }

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("faceSelectChanged: end");

    return true;
  }

  bool Render::volumeSelectChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    CCDrawParms &cdp = mesh.drawParms(ccName);
    if(cdp.volumeChanged.size() == 0)
      return true;

    // Get color arrays
    std::vector<Colorb> &colorVA = cdp.colorVA("Color");
    uint &colorVAid = cdp.colorVAid("Color");
    std::vector<Colorb> &labelColorVA = cdp.colorVA("LabelColor");
    uint &labelColorVAid = cdp.colorVAid("LabelColor");

    // If there is no data return
    if(!glfuncs->glIsBuffer(colorVAid) or !glfuncs->glIsBuffer(labelColorVAid))
      return false;

    uint min = cdp.vaInverseMap.size(), max = 0;
    for(uint i = 0; i < cdp.volumeChanged.size(); i++) {
      CCIndex l = cdp.volumeChanged[i];
      CCIndexData &lIdx = (*indexAttr)[l];

      Colorf c = LabelColors[mesh.getLabel(lIdx.label) % LabelColors.size()];
      Colorb labelColor = Colorb(c.r() * 255.0, c.g() * 255.0, c.b() * 255.0, c.a() * 255.0);
      Colorb color = Colorb(0,0,0,255);

      if(lIdx.selected)
      {
        labelColor = selectionColor(labelColor);
        color = selectionColor(color);
      }

      uint fPos = cdp.faceMap[l];
      uint pos = fPos - 2 * cdp.edgeCount;

      // Update min range of indices for update
      if(min > pos)
        min = pos;
      while(fPos < cdp.vaInverseMap.size() and cdp.vaInverseMap[fPos].first == l) {
        // Write the color, selected faces are blended with grey
        colorVA[pos] = color;
        labelColorVA[pos] = labelColor;
        pos++; fPos++;
      }
      // Update max range of indices for update
      if(max < pos - 1)
        max = pos - 1;
    }
    cdp.volumeChanged.clear();

    REPORT_GL_ERROR("volumeSelectChanged: start");

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, colorVAid);
    glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &colorVA[min]); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, labelColorVAid);
    glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &labelColorVA[min]); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("volumeSelectChanged: end");

    return true;
  }

  // Called when face labels change.
  bool Render::faceLabelChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    CCDrawParms &cdp = mesh.drawParms(ccName);
    if(cdp.faceChanged.size() == 0)
      return true;

    bool doSignal = mesh.signalExists();
    bool doHeat = mesh.heatExists();

    // Generate VizAttributes for signal and heat.
    std::unique_ptr<VizAttribute<CCIndex> > signalAttr;
    if(doSignal)
      signalAttr.reset(new VizAttribute<CCIndex>(mesh.signalVizAttribute()));
    std::unique_ptr<VizAttribute<int> > heatAttr;
    if(doHeat)
      heatAttr.reset(new VizAttribute<int>(mesh.heatVizAttribute()));

    // Get color and element arrays
    std::vector<Colorb> &labelColorVA = cdp.colorVA("LabelColor");
    uint &labelColorVAid = cdp.colorVAid("LabelColor");
    ColorbVec &signalColorVA = cdp.colorVA("Signal");
    uint &signalColorVAid = cdp.colorVAid("Signal");
    ColorbVec &heatColorVA = cdp.colorVA("Heat");
    uint &heatColorVAid = cdp.colorVAid("Heat");

    // If there is no data return
    if(cdp.faceChanged.size() <= 0 or !glfuncs->glIsBuffer(labelColorVAid))
      return false;

    uint min = cdp.vaInverseMap.size(), max = 0;

    #pragma omp parallel for reduction(min:min) reduction(max:max)
    for(uint i = 0; i < cdp.faceChanged.size(); i++) {
      CCIndex f = cdp.faceChanged[i];
      CCIndexData &fIdx = (*indexAttr)[f];

//      Colorf c = LabelColors[mesh.getLabel(fIdx.label) % LabelColors.size()];
//      Colorb labelColor = Colorb(c.r() * 255.0, c.g() * 255.0, c.b() * 255.0, c.a() * 255.0);
//
//      if(fIdx.selected)
//        labelColor = selectionColor(labelColor);
//
      int label = mesh.getLabel(fIdx.label);

      Colorb labelColor(LabelColors[label % LabelColors.size()], 255.0);
      Colorb signalColor, heatColor;
      if(doSignal)
        signalColor = setFaceSignalColor(f, *signalAttr);
      else
        signalColor = DefaultColor;

      if(doHeat)
        heatColor = (*heatAttr)[label];
      faceLabelSignalHeatColor(mesh, fIdx, signalColor, labelColor, heatColor);

      uint fPos = cdp.faceMap[f];
      uint pos = fPos - 2 * cdp.edgeCount;
 
      // Update min range of indices for update
      if(min > pos)
        min = pos;
      while(fPos < cdp.vaInverseMap.size() and cdp.vaInverseMap[fPos].first == f) {
        // Write the colors to arrays
        labelColorVA[pos] = labelColor;
        if(doSignal)
          signalColorVA[pos] = signalColor;
        if(doHeat)
          heatColorVA[pos] = heatColor;
        pos++; fPos++;
      }
      // Update max range of indices for update
      if(max < pos - 1)
        max = pos - 1;
    }
    cdp.faceChanged.clear();

    REPORT_GL_ERROR("faceLabelChanged: start");

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, labelColorVAid);
    glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &labelColorVA[min]); 
    if(doSignal) {
      glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, signalColorVAid);
      glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &signalColorVA[min]); 
    }
    if(doHeat) {
      glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, heatColorVAid);
      glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(min * sizeof(Colorb)), 
                                                 (max - min + 1) * sizeof(Colorb), &heatColorVA[min]); 
    }

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("faceLabelChanged: end");

    return true;
  }

  // Called when the positions of selected vertices have changed.
  // This should be faster because we're only handling a small subset of the geometry
  // (and we're using it interactively, ideally).
  bool Render::selectPositionsChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    CCDrawParms &cdp = mesh.drawParms(ccName);

    // Vertex array
    uint &vertexVAid = cdp.vertexVAid("Vertex");
    std::vector<Point3f> &vertexVA = cdp.vertexVA("Vertex");

    uint ec2 = cdp.edgeCount * 2;   // vertex array vs vaInverseMap offset
    // Check size
    if(vertexVA.size() != cdp.vaInverseMap.size() - ec2) {
      mdxInfo << "Render::selectPositionsChanged Invalid vertexVA size" << endl;
      return false;
    }

    // We only need to update the selected positions, but it is probably
    // just as fast to update them all
    uint minVPos = vertexVA.size(), maxVPos = 0;
    //uint minFPos = vertexVA.size(), maxFPos = 0;
    //uint minLPos = vertexVA.size(), maxLPos = 0;

    // Do vertices
    #pragma omp parallel for reduction(min:minVPos) reduction(max:maxVPos)
    for(uint i = 0; i < cdp.vertexSelected.size(); i++) {
      CCIndex v = cdp.vertexSelected[i];
      uint j = cdp.vertexMap[v];
      if(j < minVPos)
        minVPos = j;
      if(j > maxVPos)
        maxVPos = j;
      vertexVA[j] = Point3f((*indexAttr)[v].pos);
    }
    // Now do faces, we skip the edges
//    #pragma omp parallel for reduction(min:minFPos) reduction(max:maxFPos)
//    for(uint i = cdp.faceStart; i < cdp.volumeStart ; i++) {
//      const CCIndexData &vIdx = (*indexAttr)[cdp.vaInverseMap[i].second];
//      if(!vIdx.selected)
//        continue;
//      uint j = i - ec2;
//      if(j < minFPos)
//        minFPos = j;
//      if(j > maxFPos)
//        maxFPos = j;
//      vertexVA[j] = Point3f(vIdx.pos);
//    }
//    // Now volumes
//    #pragma omp parallel for reduction(min:minLPos) reduction(max:maxLPos)
//    for(uint i = cdp.volumeStart; i < cdp.vaInverseMap.size() ; i++) {
//      const CCIndexData &vIdx = (*indexAttr)[cdp.vaInverseMap[i].second];
//      if(!vIdx.selected)
//        continue;
//      uint j = i - ec2;
//      if(j < minLPos)
//        minLPos = j;
//      if(j > maxLPos)
//        maxLPos = j;
//      vertexVA[j] = Point3f(vIdx.pos);
//    }

    // Write vertex data to the card
    if(!vertexVAid) 
      glfuncs->glGenBuffers(1, &vertexVAid);

    REPORT_GL_ERROR("selectPositionsChanged: start");

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, vertexVAid);

    if(maxVPos >= minVPos)
      glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(minVPos * sizeof(Point3f)), 
                                   (maxVPos - minVPos + 1) * sizeof(Point3f), &vertexVA[minVPos]); 
//    if(maxFPos >= minFPos)
//      glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(minFPos * sizeof(Point3f)), 
//                                   (maxFPos - minFPos + 1) * sizeof(Point3f), &vertexVA[minFPos]); 
//    if(maxLPos >= minLPos)
//      glfuncs->glBufferSubData(GL_ARRAY_BUFFER_ARB, (GLintptr)(minLPos * sizeof(Point3f)), 
//                                   (maxLPos - minLPos + 1) * sizeof(Point3f), &vertexVA[minLPos]); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("selectPositionsChanged: end");

    return true;
  }

  // Called when cell edges have changed, as long as parameter "Cell Edges" is true.
  bool Render::cellEdgesChanged(Mesh &mesh, const QString &ccName)
  {
    indexAttr = &mesh.indexAttr();
    auto &cdp = mesh.drawParms(ccName);
    auto &cs = mesh.ccStructure(ccName);
    auto &edgeCellElements = cdp.elementEA("CellEdge");

    // Element array for cell edges, if required
    edgeCellElements.clear();
    if(!stringToBool(parm("Cell Edges")) or cs.faces().size() == 0)
      return true;

    const auto &edges = cs.edges();
    tbb::concurrent_vector<uint> edgeCellE;
    edgeCellE.reserve(edges.size() * 2);
    #pragma omp parallel for
    for(uint i = 0; i < edges.size(); i++) {
      CCIndex e = edges[i];
      bool keep = false;
      std::vector<CCStructure::FlipI> eflips = cs.matchV(e,CCIndex::Q,CCIndex::Q,CCIndex::Q);
      if(eflips.empty()) {
        if(cs.cobounds(e).size() == 1)
          keep = true;
      }
      else
      {
        for(auto flip : eflips) {
          if(flip.facet[0] == CCIndex::INFTY || flip.facet[1] == CCIndex::INFTY) {
            keep = true;
            break;
          }
          auto &f0Idx = (*indexAttr)[flip.facet[0]];
          auto &f1Idx = (*indexAttr)[flip.facet[1]];
          if(f0Idx.label != f1Idx.label) {
            keep = true;
            break;
          }
        }
      }
      if(!keep)
        continue;

      CCIndex e1 = cdp.vaInverseMap[cdp.vertexCount + i*2].second;
      CCIndex e2 = cdp.vaInverseMap[cdp.vertexCount + i*2 + 1].second;

      auto it = edgeCellE.grow_by(2);
      *it++ = cdp.vertexMap[e1];
      *it = cdp.vertexMap[e2];
    }
    edgeCellElements.resize(edgeCellE.size());
    #pragma omp parallel for
    for(uint i = 0; i < edgeCellE.size(); ++i)
      edgeCellElements[i] = edgeCellE[i];
    
    return true;
  }

  Colorb Render::selectionColor(const Colorb &color)
  {
    return Colorb(color.r()/3.0 + 128, color.g()/3.0 + 128, color.b()/3.0 + 128, color.a());
  }


  /*
   * Renderer for polarity
   */
  bool RenderPolarity::defaultDrawChoices(Mesh &mesh, const QString &ccName)
  {
    // Call base method
    if(!Render::defaultDrawChoices(mesh, ccName))
      return false;

    // Return if no faces
    CCStructure &cs = mesh.ccStructure(ccName);
    if(cs.faces().size() == 0)
      return true;

    // Get the polarity attribute map
    signedSignalAttrDouble = &mesh.attributes().attrMap<CCSignedIndex, double>(parm("Polarity Attribute"));

    // Add the new face choice
    CCDrawParms &cdp = mesh.drawParms(ccName);
    DrawChoice &polChoice = cdp.createTriangleChoice
      ("Faces", "Polarity", "PolColor", "PolElements", "PolColor", "PolVertex", "PolNormal");
    polChoice.dcType = DrawChoice::DCRenderChoice;
    polChoice.renderChoice.polygonOffset = Point2f(1.0,1.0);

    // Reset the color map if necessary
    polColors = &cdp.colorMap("PolColor");
    if(polColors->colors.size() == 0) {
      polColors->setColors("Green Red");
      polColors->makeRangeMap();
      polColors->setBounds(0, mesh.signalBounds());
      polColors->setUnit(0, mesh.signalUnit());
      polColors->setIndices(0, Point2i(0, 1));
      polColors->setBounds(1, Point2d(0, 1));
      polColors->setUnit(1, "");
      polColors->setIndices(1, Point2i(2, 3));
    }

    return true; 
  }

//    // Get face color from the signal Attribute
//    if(!signalAttr)
//      std::cout << "Render::setFacePolarityColor signalAttr is null" << std::endl;
//    else  {
//     // If we have a double signal, use the polarity map, otherwise use the signal map
//      const CCIndexDoubleAttr *spAttr = boost::get<const CCIndexDoubleAttr>(&signalAttr->attr);
//      if(spAttr)
//        return colorMap.getColor(0, (*spAttr)[f]);
//      else
//        return (*signalAttr)[f];
//    }
//
//    return Colorb();
//  }

/*  Colorb RenderPolarity::setBorderPolarityColor(CCSignedIndex e, const ColorMap &colorMap)
  {
    // Get border color from the signed polarity Attribute
    if(!signedSignalAttrDouble)
      std::cout << "RenderPolarity::setBorderPolarityColor signedIndexAttr is null" << std::endl;
    else
      return colorMap.getColor(1, (*signedSignalAttrDouble)[e]);

    return Colorb();
  }
*/

  // This information is used to round corners.
  struct CornerInfo
  {
    Point3f pos;
    CCIndex prev, next;
    bool convex;
  };
  typedef std::map<CCIndex, CornerInfo> CornerInfoMap;
  typedef std::pair<CCIndex, CornerInfo> CornerInfoPair;
  typedef std::vector<Point3f> Point3fVec;

  void findCornerPoints(CCStructure &cs, CCIndexDataAttr *indexAttr, CCIndex f, CCIndex e, 
                                                                double width, CornerInfoMap &cornerInfo)
  {
    // Find vertices attached to the edge
    CCIndexPair ep = cs.edgeBounds(e);
    CCIndex v = ep.first, w = ep.second;

    // Swap v and w if required
    if(cs.ro(f, e) * cs.ro(e, v) == ccf::NEG)
      std::swap(v, w);

    // Get the data
    CCIndexData &fIdx = (*indexAttr)[f];
    CCIndexData &vIdx = (*indexAttr)[v];
    CCIndexData &wIdx = (*indexAttr)[w];

    // Convert everything to float
    Point3f fPos = Point3f(fIdx.pos);
    Point3f fNrml = normalized(Point3f(fIdx.nrml));
    Point3f vPos = Point3f(vIdx.pos);
    Point3f wPos = Point3f(wIdx.pos);

    // Dir from v to w in cell plane
    Point3f vwDir = normalized(wPos - vPos);
    vwDir = normalized(vwDir - fNrml * (vwDir * fNrml));

    // Do v end
    // Get the point before v
    CCStructure::CellTuple tuple(cs, f, e, v);
    tuple.flip(1, 0);
    Point3f vPrev = Point3f((*indexAttr)[tuple[0]].pos);

    // Dir toward prev vertex before v in cell plane
    Point3f vPrevDir = normalized(vPrev - vPos);
    vPrevDir = normalized(vPrevDir - fNrml * (vPrevDir * fNrml));

    // Take average of edges from vertex as direction
    Point3f vDir = (vwDir + vPrevDir)/2.0;
    double vNorm = norm(vDir);

    // Check for straight junction
    if(vNorm < width * 1e-6)
      vDir = normalized(vwDir ^ fNrml);
    else
      vDir /= vNorm;

    // Check if vector points in or out of cell
    bool convex = true;
    if((fPos - vPos) * vDir < 0) {
      vDir *= -1;
      convex = false;
    }

    // Scale based on sin of angle
    double vFactor = fabs(1.0 / norm(vDir ^ vwDir));

    CornerInfo &cV = cornerInfo[v];
    cV.pos = vPos + width * vFactor * vDir;
    cV.convex = convex;
    cV.prev = tuple[0];
    cV.next = w;
  }

  void pushClosePoints(CCStructure &cs, CCIndexDataAttr *indexAttr, CCIndex f, CCIndex e, double width, CornerInfoMap &cornerInfo)
  {
    // Find vertices attached to the edge
    CCIndexPair ep = cs.edgeBounds(e);
    CCIndex v = ep.first, w = ep.second;

    // Swap v and w if required
    if(cs.ro(f, e) * cs.ro(e, v) == ccf::NEG)
      std::swap(v, w);

    // Get the data
    CCIndexData &fIdx = (*indexAttr)[f];
    CCIndexData &vIdx = (*indexAttr)[v];
    CCIndexData &wIdx = (*indexAttr)[w];
    Point3f fPos(fIdx.pos);
    Point3f fNrml(fIdx.nrml);
    Point3f vPos(vIdx.pos);
    Point3f wPos(wIdx.pos);

    forall(CornerInfoMap::value_type &pr, cornerInfo) {
      double dis = distLinePoint(vPos, wPos, pr.second.pos, false);
      if(dis < width) {
        Point3f pDir = normalized(fNrml ^ (wPos - vPos));
        pr.second.pos += pDir * (width - dis);
      }
    }
  }

  // Generate the face and border triangles for polarity visualization
  void findFaceBorderTris(CCStructure &cs, CCIndexDataAttr *indexAttr, CCIndex f, CCIndex e, double width, bool doCorners,
     CornerInfoMap &cornerInfo, Point3fVec &faceTris, Point3fVec &faceNrmls, Point3fVec &borderTris, Point3fVec &borderNrmls)
  {
    // Find vertices attached to the edge
    CCIndexPair ep = cs.edgeBounds(e);
    CCIndex v = ep.first, w = ep.second;

    // Swap v and w if required
    if(cs.ro(f, e) * cs.ro(e, v) == ccf::NEG)
      std::swap(v, w);

    // Get the data
    CCIndexData &fIdx = (*indexAttr)[f];
    CCIndexData &vIdx = (*indexAttr)[v];
    CCIndexData &wIdx = (*indexAttr)[w];

    // Convert everything to float
    Point3f fPos = Point3f(fIdx.pos);
    Point3f fNrml = normalized(Point3f(fIdx.nrml));

    Point3f vPos = Point3f(vIdx.pos);
    Point3f vNrml = normalized(Point3f(vIdx.nrml));
    Point3f wPos = Point3f(wIdx.pos);
    Point3f wNrml = normalized(Point3f(wIdx.nrml));

    // Center point in edge
    Point3f vwNrml = normalized((vNrml + wNrml)/2.0);
    Point3f vwPos = (vPos + wPos)/2.0;

    CornerInfo &vC = cornerInfo[v];
    CornerInfo &wC = cornerInfo[w];

    Point3f &viPos = vC.pos;
    Point3f &wiPos = wC.pos;
    Point3f &vPrev = cornerInfo[vC.prev].pos;
    Point3f &wNext = cornerInfo[wC.next].pos;
    bool vConvex = vC.convex;
    bool wConvex = wC.convex;

    // Make points to round corners
    Point3f vjPos, vkPos, wjPos, wkPos;
    if(doCorners and (vConvex or wConvex)) {
      Point3f vwDir = wiPos - viPos;
      double vwLen = norm(vwDir);
      vwDir /= vwLen;
      Point3f vPrevDir = vPrev - viPos;
      double vPrevLen = norm(vPrevDir);
      vPrevDir /= vPrevLen;
      Point3f wNextDir = wNext - wiPos;
      double wNextLen = norm(wNextDir);
      wNextDir /= wNextLen;
      if(vConvex) {
        vjPos = viPos + vwDir * min(width, vwLen/2.0);
        vkPos = (viPos + vjPos + (viPos + vPrevDir * min(width, vPrevLen/2.0)))/3.0;
      }
      if(wConvex) {
        wjPos = wiPos - vwDir * min(width, vwLen/2.0);
        wkPos = (wiPos + wjPos + (wiPos + wNextDir * min(width, wNextLen/2.0)))/3.0;
      }
    }

    // Create face triangles
    Point3f vpos = viPos;
    Point3f wpos = wiPos;
    if(doCorners and vConvex) {
      faceTris.push_back(fPos);
      faceTris.push_back(vkPos);
      faceTris.push_back(vjPos);
      vpos = vjPos;
      faceNrmls.push_back(fNrml);
      faceNrmls.push_back(vNrml);
      faceNrmls.push_back(vNrml);
    }
    if(doCorners and wConvex) {
      faceTris.push_back(fPos);
      faceTris.push_back(wjPos);
      faceTris.push_back(wkPos);
      wpos = wjPos;
      faceNrmls.push_back(fNrml);
      faceNrmls.push_back(wNrml);
      faceNrmls.push_back(wNrml);
    }
    faceTris.push_back(fPos);
    faceTris.push_back(vpos);
    faceTris.push_back(wpos);
    faceNrmls.push_back(fNrml);
    faceNrmls.push_back(vNrml);
    faceNrmls.push_back(wNrml);

    // Create border triangles
    borderTris.push_back(viPos);
    borderTris.push_back(vPos);
    borderTris.push_back(vwPos);
    borderTris.push_back(wiPos);
    borderTris.push_back(viPos);
    borderTris.push_back(vwPos);
    borderTris.push_back(wiPos);
    borderTris.push_back(vwPos);
    borderTris.push_back(wPos);

    borderNrmls.push_back(vNrml);
    borderNrmls.push_back(vNrml);
    borderNrmls.push_back(vwNrml);
    borderNrmls.push_back(wNrml);
    borderNrmls.push_back(vNrml);
    borderNrmls.push_back(vwNrml);
    borderNrmls.push_back(wNrml);
    borderNrmls.push_back(vwNrml);
    borderNrmls.push_back(wNrml);

    if(doCorners and vConvex) {
      borderTris.push_back(viPos);
      borderTris.push_back(vjPos);
      borderTris.push_back(vkPos);

      borderNrmls.push_back(vNrml);
      borderNrmls.push_back(vNrml);
      borderNrmls.push_back(vNrml);
    }
    if(doCorners and wConvex) {
      borderTris.push_back(wiPos);
      borderTris.push_back(wkPos);
      borderTris.push_back(wjPos);

      borderNrmls.push_back(wNrml);
      borderNrmls.push_back(wNrml);
      borderNrmls.push_back(wNrml);
    }
  }

  // Generates positions and triangle list for rendering polarity
  // Here we'll do everything in properties, since the signal value changes both the 
  // color and the geometry of the visualization
  bool RenderPolarity::positionsChanged(Mesh &mesh, const QString &ccName)
  {
    // First call base method
    Render::positionsChanged(mesh, ccName);

    if(!(mesh.drawParms(ccName).changed & CCDrawParms::PropertiesChanged))
    {
      mesh.drawParms(ccName).changed |= CCDrawParms::PropertiesChanged;
      return propertiesChanged(mesh, ccName);
    }
    return true;
  }

  Colorb RenderPolarity::setFacePolarityColor(CCIndex f, VizAttribute<CCIndex> &signalAttr)
  {
    // If we have a double signal, use the polarity map, otherwise use the signal map
    const CCIndexDoubleAttr *spAttr = boost::get<const CCIndexDoubleAttr>(&signalAttr.attr);
    spAttr = boost::get<const CCIndexDoubleAttr>(&signalAttr.attr);
    if(spAttr) {
      if(!polColors) {
        mdxWarning << "RenderPolarity::setFacePolarityColor Polarity colorMap not set" << endl;
        return Colorb(128,128,128,255);
      }
      return polColors->getColor(0, (*spAttr)[f]);
    }
    return signalAttr[f];
  }

  bool RenderPolarity::propertiesChanged(Mesh &mesh, const QString &ccName)
  {
    // First call base method
    Render::propertiesChanged(mesh, ccName);

    CCDrawParms &cdp = mesh.drawParms(ccName);
    CCStructure &cs = mesh.ccStructure(ccName);

    indexAttr = &mesh.indexAttr();

    // Get the element array
    auto &elementEA = cdp.elementEA("PolElements");
    elementEA.clear();

    // Get vertex and normal arrays
    uint &vertexVAid = cdp.vertexVAid("PolVertex");
    std::vector<Point3f> &vertexVA = cdp.vertexVA("PolVertex");
    uint &normalVAid = cdp.normalVAid("PolNormal");
    std::vector<Point3f> &normalVA = cdp.normalVA("PolNormal");
    vertexVA.clear();
    normalVA.clear();
 
    // Get the color array
    std::vector<Colorb> &colorVA = cdp.colorVA("PolColor");
    uint &colorVAid = cdp.colorVAid("PolColor");
    colorVA.clear();

    // Get the signal attribute, if there is one.
    bool doSignal = mesh.signalExists();
    std::unique_ptr<VizAttribute<CCIndex> > signalAttr;
    if(doSignal)
      signalAttr.reset(new VizAttribute<CCIndex>(mesh.signalVizAttribute()));

    // Get the parameters
    double Width = parm("Width").toDouble();
    bool DoCorners = stringToBool(parm("Round Corners"));

    // Create geometry for polarity rendering
    uint count = 0;
    for(CCIndex f : cs.faces()) {
      CCIndexData &fIdx = (*indexAttr)[f];

      // Check face normal, it should be set already
      if(isNan(norm(fIdx.nrml))) {
        std::cout << "RenderPolarity::propertiesChanged face normal not set" << std::endl;
        continue;
      }

      CornerInfoMap cornerInfo;
      for(CCIndex e : cs.bounds(f))
        findCornerPoints(cs, indexAttr, f, e, Width, cornerInfo);
      for(CCIndex e : cs.bounds(f))
        pushClosePoints(cs, indexAttr, f, e, Width, cornerInfo);

//      for(CCIndex e : cs.bounds(f)) {
//        CCSignedIndex fe(e, cs.ro(f, e));
      for(CCSignedIndex fe : cs.boundary(f)) {
        CCIndex e = ~fe;
        Point3fVec faceTris, faceNrmls, borderTris, borderNrmls;
        findFaceBorderTris(cs, indexAttr, f, e, Width, DoCorners, cornerInfo, faceTris, faceNrmls, borderTris, borderNrmls);

        Colorb faceColor(128,128,128,255);
        if(doSignal)
          faceColor = setFacePolarityColor(f, *signalAttr);

        // Change color if selected
        if(fIdx.selected)
          faceColor = selectionColor(faceColor);

        Colorb borderColor = polColors->getColor(1, (*signedSignalAttrDouble)[fe]);

        // Change color if selected
        if(fIdx.selected)
          borderColor = selectionColor(borderColor);

        for(uint i = 0; i < faceTris.size(); i += 3) {
          vertexVA.push_back(faceTris[i]);
          vertexVA.push_back(faceTris[i+1]);
          vertexVA.push_back(faceTris[i+2]);
          normalVA.push_back(faceNrmls[i]);
          normalVA.push_back(faceNrmls[i+1]);
          normalVA.push_back(faceNrmls[i+2]);

          colorVA.push_back(faceColor);
          colorVA.push_back(faceColor);
          colorVA.push_back(faceColor);

          elementEA.push_back(count++);
          elementEA.push_back(count++);
          elementEA.push_back(count++);
        }

        for(uint i = 0; i < borderTris.size(); i += 3) {
          vertexVA.push_back(borderTris[i]);
          vertexVA.push_back(borderTris[i+1]);
          vertexVA.push_back(borderTris[i+2]);
          normalVA.push_back(borderTris[i]);
          normalVA.push_back(borderTris[i+1]);
          normalVA.push_back(borderTris[i+2]);
        
          colorVA.push_back(borderColor);
          colorVA.push_back(borderColor);
          colorVA.push_back(borderColor);

          elementEA.push_back(count++);
          elementEA.push_back(count++);
          elementEA.push_back(count++);
        }
      }
    }

    // Write vertex data to the card
    if(!vertexVAid) 
      glfuncs->glGenBuffers(1, &vertexVAid);
    if(!normalVAid) 
      glfuncs->glGenBuffers(1, &normalVAid);
    if(!colorVAid) 
      glfuncs->glGenBuffers(1, &colorVAid);

    REPORT_GL_ERROR("RenderPolarity::positionsChanged: start");

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, vertexVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      vertexVA.size() * sizeof(Point3f), &vertexVA[0], GL_STATIC_DRAW_ARB); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, normalVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      normalVA.size() * sizeof(Point3f), &normalVA[0], GL_STATIC_DRAW_ARB); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, colorVAid);
    glfuncs->glBufferData(GL_ARRAY_BUFFER_ARB,
      colorVA.size() * sizeof(Colorb), &colorVA[0], GL_STATIC_DRAW_ARB); 

    glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

    REPORT_GL_ERROR("RenderPolarity::positionsChanged: end");

    return true;
  }

  bool RenderPolarity::faceSelectChanged(Mesh &mesh, const QString &ccName)
  {
    // First call base method
    Render::faceSelectChanged(mesh, ccName);

    // Just call properties changed if not being called already
    if(!(mesh.drawParms(ccName).changed & CCDrawParms::PropertiesChanged))
    {
      mesh.drawParms(ccName).changed |= CCDrawParms::PropertiesChanged;
      return propertiesChanged(mesh, ccName);
    }
    return true;
  }

  bool RenderPolarity::volumeSelectChanged(Mesh &mesh, const QString &ccName)
  {
    // First call base method
    Render::volumeSelectChanged(mesh, ccName);

    // Just call properties changed if not being called already
    if(!(mesh.drawParms(ccName).changed & CCDrawParms::PropertiesChanged))
    {
      mesh.drawParms(ccName).changed |= CCDrawParms::PropertiesChanged;
      return propertiesChanged(mesh, ccName);
    }
    return true;
  }


  REGISTER_PROCESS(Render);
  REGISTER_PROCESS(RenderPolarity);
  REGISTER_PROCESS(RenderSetEmptyColor);

}
