// Fragment shader that uses color arrays.
//
uniform float brightness;
uniform float opacity;
varying vec4 vColor; // Set in vertex shader

int fmod(int value, int div)
{
  int rat = value / div;
  int mul = rat * div;
  return value - mul;
}

void setColor()
{
  gl_FragColor = light(vColor);
  gl_FragColor.a *= opacity;
}

