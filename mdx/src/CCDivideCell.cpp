//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2021 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#include <CCDivideCell.hpp>
#include <MeshUtils.hpp>

using namespace mdx;

// Helper function: Ensure that v is not within distance mw of v1 or v2
double pushToWallMin(Point3d &v, Point3d v1, Point3d v2, double mw)
{
  Point3d v1v2 = v2 - v1;
  double v1v2l = norm(v1v2);
  v1v2 /= v1v2l;

  // len is the distance along the edge from v to v1
  double len = (v - v1) * v1v2, len1 = len;
  if(v1v2l <= mw * 2.0)
    // If the edge is too short, we put v at its midpoint
    len1 = (v1v2l/2.0);
  else if(len < mw)
    // If v is too close to v1, we put it at the minimum distance
    len1 = mw;
  else if(len > v1v2l - mw)
    // If v is too close to v2, we put it at the minimum distance
    len1 = v1v2l - mw;

  v = v1 + v1v2 * len1;
  return(fabs(len - len1));
}

// Helper function: add noise after having found the shortest wall
void addWallNoise(Point3d &v, Point3d v1, Point3d v2, double noiseMaxDis)
{
  Point3d v1v2 = v2 - v1;
  double v1v2l = norm(v1v2);
  v1v2 /= v1v2l;

  double noise = ran(noiseMaxDis) - .5 * noiseMaxDis;
  if(noiseMaxDis < 0)
    noise = gaussRan(0.0,-noiseMaxDis);


  // len is the distance along the edge from v to v1
  double len = norm(v - v1) + noise; // If the noise is inside of the segment, we just apply it
  if(len < 0.05 * v1v2l)
    // If the noise goes beyond v1, we set the point close to v1
    len = 0.05 * v1v2l;
  else if(len > 0.95 * v1v2l)
    // If the noise goes beyond v2, we set the point close to v2
    len = 0.95 * v1v2l;

  v = v1 + v1v2 * len;
  return;
}

// Structure representing a division wall in 2D
struct DivWall2d
{
  struct {
    Point3d pos;    // position of endpoint
    CCIndex vA, vB; // vertices of wall to be split
    double sfrac;   // fraction of position along wall from A to B
  } endpoints[2];
};

/* Find cell division wall */
bool findCellDiv2d(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndex cell,
                   const Cell2dDivideParms &divParms, Cell2dDivideParms::DivAlg divAlg,
                   const Point3d &divVector, DivWall2d &divWall)
{
  // Read parameters from Parms
  double CellWallMin = divParms.parm("Cell Wall Min").toDouble();
  double CellWallSample = divParms.parm("Cell Wall Sample").toDouble();
  double CellPinch = divParms.parm("Cell Pinch").toDouble();
  double CellMaxPinch = divParms.parm("Cell Max Pinch").toDouble();
  double CenterNoise = divParms.parm("Center Noise").toDouble();
  double WallNoise = divParms.parm("Wall Noise").toDouble();
  double MinAngle = divParms.parm("MinAngle").toDouble();

  for(uint i = 0 ; i < 2 ; i++)
    divWall.endpoints[i].vA = divWall.endpoints[i].vB = CCIndex::UNDEF;

  // Check dimension
  Dimension dimension = cs.dimensionOf(cell);
  if(dimension != 2)
    throw(QString("findCellDiv2d: Can only split a two-dimensional cell!"));

  // iterate to get points on walls
  CCStructure::CellTuple ct(cs,cell);
  std::vector<Point3d> pos;
  CCIndex firstV = ct[0];
  do {
    pos.push_back(indexAttr[ct[0]].pos);
    ct.flip(0,1);
  } while(ct[0] != firstV);
  uint numVertices = pos.size();

  // get centroid and normal
  auto cdata = polygonCentroidData(pos);
  // If there is noise, add it to the centroid (would be nice to check if still inside the cell. 
  if(CenterNoise > 0) {
    Point3d noiseVec(ran(CenterNoise) - .5 * CenterNoise, ran(CenterNoise) - .5 * CenterNoise, ran(CenterNoise) - .5 * CenterNoise);
    // Remove normal component
    cdata.centroid += noiseVec - (noiseVec * cdata.normal) * cdata.normal;
  }
  if (divAlg == Cell2dDivideParms::ASSIGNED_VECTOR_TRHOUGH_CENTROID) {
    /*
      //translate the division vector into the centroid
      // (the division segment will be composed by these two points:
      //  cdata.centroid and divVector + cdata.centroid)
      Point3d divVecTrans = divVector + cdata.centroid;
      // counter for the number of intersecting walls found (need to find 2)
      int numberOfInter = 0;
      Matrix3x2d interPoints(0.0);
      for(uint i = 0 ; i < numVertices ; i++) {
        bool foundIntersect =false;
        uint i1 = (i+1) % numVertices;
        Point3d u1 = pos[i], u2 = pos[i1], u1u2 = u2 - u1;
        double u1u2l = norm(u1u2);
        Point3d u1u2d = u1u2 / u1u2l;
        double s = 0;
        double t = 0;
        foundIntersect = lineLineIntersect(u1, u2, cdata.centroid, divVecTrans, s, t, interPoints[numberOfInter])
        if (foundIntersect and 0.<= s and 1.>= s ){
          double leftDistance =
          numberOfInter++;
        }
      }
    */
  }
  else if (divAlg == Cell2dDivideParms::SHORTEST_WALL_THROUGH_CENTROID) {
    double shortestDist = HUGE_VAL;
    // try walls from each position in turn
    for(uint i = 0 ; i < numVertices ; i++) {
      uint i1 = (i+1) % numVertices;
      Point3d u1 = pos[i], u2 = pos[i1], u1u2 = u2 - u1;
      double u1u2l = norm(u1u2);
      Point3d u1u2d = u1u2 / u1u2l;

      // Try a wall from points along the wall separated by CellWallSample
      double ulEnd = u1u2l - CellWallMin;
      for(double ul = CellWallMin ; ul <= ulEnd ; ul += CellWallSample) {
        Point3d u = u1 + ul * u1u2d, uc = cdata.centroid - u;
        Point3d nu = cdata.normal ^ uc;
        double centroidDist = norm(uc);

        // Now we check the upcoming walls for an intersection
        CCStructure::CellTuple ct1(ct);
        ct1.flip(0,1);
        for(uint j = i+1; j < numVertices; j++) {
          uint j1 = (j+1) % numVertices;
          Point3d v;
          double s;
          if(planeLineIntersect(u, nu, pos[j], pos[j1], s, v) and (s >= 0. && s <= 1.)) {
            double dist = norm(u - v);

            // Check that the line actually goes through the centroid
            // (needed if the cell is not convex)
            if(dist < centroidDist) continue;

            dist += pushToWallMin(v, pos[j], pos[j1], CellWallMin);
            if(norm(WallNoise) > 0.01) addWallNoise(v, pos[j], pos[j1], WallNoise);
            if(norm(WallNoise) > 0.01) addWallNoise(u, pos[i], pos[i1], WallNoise);

            // compute angle at walls
            Point3d vecUV = u-v;
            Point3d vecVU = v-u;
            Point3d vecJV = pos[j]-v;
            Point3d vecIU = pos[i]-u;

            double angle1 = std::acos(vecJV * vecUV / norm(vecUV) / norm(vecJV)) * 180. / 3.14159;
            double angle2 = std::acos(vecIU * vecVU / norm(vecVU) / norm(vecIU)) * 180. / 3.14159;

            if(angle1 > 90) angle1 = 180 - angle1;
            if(angle2 > 90) angle2 = 180 - angle2;

            if(angle1 < MinAngle or angle2 < MinAngle) continue;

            //double noiseFac = Noise * (std::rand() % 100) / 100.0 + 1;
            //dist *= noiseFac;
            std::cout << "angles " << angle1 << "/" << angle2 << std::endl;
            if(dist < shortestDist) {
              shortestDist = dist;
              divWall.endpoints[0] = { u,  ct[0],  ct.other(0), norm(u - pos[i]) / norm(pos[i1] - pos[i]) };
              divWall.endpoints[1] = { v, ct1[0], ct1.other(0), norm(v - pos[j]) / norm(pos[j1] - pos[j]) };
            }
          }
          ct1.flip(0,1);
        }
      }
      ct.flip(0,1);
    }
  }
  else
    throw(QString("findCellDiv2d: Unknown division algorithm %1").arg(divAlg));

  // If the CellWallMin is too high, maybe we can't find a wall
  if(divWall.endpoints[0].vA.isPseudocell() or divWall.endpoints[0].vB.isPseudocell() or
     divWall.endpoints[1].vA.isPseudocell() or divWall.endpoints[1].vB.isPseudocell()) {
    mdxInfo << "CellTissue::findCellDiv Unable to find divide wall for cell:"
            << to_string(cell).c_str() << ", label:" << indexAttr[cell].label
            << ", CellWallMin or CellWallSample too high?" <<  endl;
    return false;
  }

  // Now we pinch the walls.
  for(uint i = 0 ; i < 2 ; i++) {
    auto &ep = divWall.endpoints[i];
    CCIndex edge = cs.join(ep.vA,ep.vB);
    // Don't pinch a given cell wall if it is external
    if(!cs.onBorder(edge)) {
      Point3d centroidDir = cdata.centroid - ep.pos;
      double cdl = norm(centroidDir);
      centroidDir /= cdl;

      Point3d posA = indexAttr[ep.vA].pos, posB = indexAttr[ep.vB].pos;
      double distA = norm(ep.pos - posA), distB = norm(ep.pos - posB);
      double minD = std::min(distA , distB);
      double pinchAmt = std::min(minD , cdl) * CellPinch;
      pinchAmt = std::min(pinchAmt , CellMaxPinch);

      ep.pos += centroidDir * pinchAmt;
    }
  }

  return true;
}

// Divide a two-dimensional cell using to the given division parameters and algorithm.
bool mdx::divideCell2d(CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex cell,
                       const Cell2dDivideParms &divParms, Subdivide *sDiv,
                       Cell2dDivideParms::DivAlg divAlg, const Point3d &divVector)
{
  // Find out where the division will take place
  DivWall2d divWall;
  if(!findCellDiv2d(cs, indexAttr, cell, divParms, divAlg, divVector, divWall))
    return false;

  // Divide the cell walls
  CCIndex ep[2];
  for(int i = 0 ; i < 2 ; i++) {
    CCIndex edge = edgeBetween(cs, divWall.endpoints[i].vA, divWall.endpoints[i].vB);

    CCStructure::SplitStruct ss(edge);
    CCIndexFactory.fillSplitStruct(ss);
    ep[i] = ss.membrane;

    bool neg = cs.ro(cell, edge) == ccf::NEG;
    cs.splitCell(ss);
    // Update the position ourselves since the subdivider may need it
    indexAttr[ep[i]].pos = divWall.endpoints[i].pos;
    if(sDiv) {
      sDiv->splitCellUpdate(1, cs, ss, divWall.endpoints[i].vA, divWall.endpoints[i].vB,
                            neg ? divWall.endpoints[i].sfrac : (1.0 - divWall.endpoints[i].sfrac));
    }
  }

  // Divide the cell itself
  CCStructure::SplitStruct ss(cell);
  CCIndexFactory.fillSplitStruct(ss);
  cs.splitCell(ss, +ep[0] -ep[1]);

  if(sDiv) {
    updateFaceGeometry(cs, indexAttr, ss.childP);
    updateFaceGeometry(cs, indexAttr, ss.childN);
    double s = indexAttr[ss.childP].measure / (indexAttr[ss.childP].measure + indexAttr[ss.childN].measure);
    sDiv->splitCellUpdate(2, cs, ss, CCIndex(), CCIndex(), s);
  }

  return true;
}

// Rotate a splitting plane by the given rotation about the given axis.
void mdx::SplittingPlane::rotate(Point3d axis, double degrees)
{
  axis.normalize();

  Point3d wpar = (normal * axis) * axis;
  Point3d wperp = normal - wpar;

  double radians = degrees * M_PI / 180.;
  normal = wpar + cos(radians)*wperp + sin(radians)*(wperp ^ axis);
}

// Helper function: divide a quad into two triangles.
void quadSplit(CCStructure &cc, CCIndexDataAttr &indexAttr, CCIndex face,
	       Subdivide *subdiv = NULL)
{
  if(face.isPseudocell() || !cc.hasCell(face) || cc.dimensionOf(face) != 2)
    return;

  // Get the contour points
  std::vector<CCIndex> vertices;
  {
    CCStructure::CellTuple tuple(cc,face);
    if(tuple.totalOrientation() == ccf::NEG) tuple.flip(0);
    CCIndex firstV = tuple[0];
    do {
      vertices.push_back(tuple[0]);
      tuple.flip(0,1);
    } while(tuple[0] != firstV);
  }

  // We can only do something if the face is a quad
  if(vertices.size() == 4) {
    CCStructure::SplitStruct ss(face);
    CCIndexFactory.fillSplitStruct(ss);
    if(norm(indexAttr[vertices[0]].pos - indexAttr[vertices[2]].pos) <
       norm(indexAttr[vertices[1]].pos - indexAttr[vertices[3]].pos))
      cc.splitCell(ss, +vertices[0] -vertices[2]);
    else
      cc.splitCell(ss, +vertices[1] -vertices[3]);

    if(subdiv) subdiv->splitCellUpdate(2,cc,ss);
  } else if(vertices.size() > 4) {
    mdxInfo << "quadSplit: expected to triangulate a quad "<<face<<", got "
	    << vertices.size() << " vertices instead." << endl;
  }

  return;
}


// Split a volume with the given splitting plane
bool mdx::splitVolume(CCStructure &cc, CCIndexDataAttr &indexAttr,
		      CCIndex vol, const SplittingPlane &plane,
		      CCStructure::SplitStruct &ss,
		      bool triangulated, Subdivide *subdiv)
{
  static const double EPS = 1e-6;

  // neighbouring vertices by vertex
  std::map<CCIndex, std::set<CCIndex>> bdAdj;

  // point on edge
  typedef std::pair<CCIndex,double> PointOnEdge;
  std::map<CCIndex, std::vector<PointOnEdge>> faceEdges;
  std::map<CCIndex, std::vector<CCIndex>> facePoints;

  // figure out where to split edges
  for(CCIndex face : cc.incidentCells(vol,2)) {
    CCStructure::CellTuple tuple(cc,face);
    CCIndex firstV = tuple[0];
    std::vector<PointOnEdge> facePt;
    do {
      Point3d p0 = indexAttr[tuple[0]].pos;
      tuple.flip(0);
      Point3d p1 = indexAttr[tuple[0]].pos;

      if(plane.inside(p0) != plane.inside(p1)) {
        double t = plane.intersectEdge(p0,p1);

        if(t < EPS || t > (1.+EPS)) continue;
        else if(t > (1.-EPS)) {  // line passes through vertex tuple[0]
          facePt.push_back(std::make_pair(tuple[1], (tuple.ro(0) == ccf::POS) ? 0 : 1));
        } else {
          facePt.push_back(std::make_pair(tuple[1], (tuple.ro(0) == ccf::POS) ? (1-t) : t));
        }
      }
      tuple.flip(1);
    } while(tuple[0] != firstV);

    // There should be zero, one, or two intersection points in this face.
    switch(facePt.size()) {
      // Zero points means the split doesn't hit the face.
    case 0:

      // One means a vertex of the original face,
      // but the split doesn't travel through the face.
    case 1:
      break;

      // Two vertices means we have an edge, and may have to split the face.
    case 2:
      // We can check if the new points share an edge;
      // for this to happen, they must both be endpoints!
      {
        CCIndex p0;
        if(facePt[0].second == 0) p0 = cc.edgeBounds(facePt[0].first).first;
        else if(facePt[0].second == 1) p0 = cc.edgeBounds(facePt[0].first).second;

        CCIndex p1;
        if(facePt[1].second == 0) p1 = cc.edgeBounds(facePt[1].first).first;
        else if(facePt[1].second == 1) p1 = cc.edgeBounds(facePt[1].first).second;

        // If they share an edge, they are adjacent in the membrane boundary.
        if(!(p0.isPseudocell() || p1.isPseudocell()) &&
           cc.dimensionOf(cc.join(p0,p1)) == 1) {
          bdAdj[p0].insert(p1);
          bdAdj[p1].insert(p0);
        }

        // Otherwise, they don't make an edge and we mark the face for splitting.
        else {
          if(p0.isPseudocell())
            faceEdges[face].push_back(facePt[0]);
          else
            facePoints[face].push_back(p0);

          if(p1.isPseudocell())
            faceEdges[face].push_back(facePt[1]);
          else
            facePoints[face].push_back(p1);

          if(faceEdges[face].empty())
            throw QString("splitVolume: two points not an edge??");
        }
      }
      break;

      // Otherwise, something weird has happened.
    default:
      throw QString("splitVolume: facePt has %1 vertices").arg(facePt.size());
    }
  }

  // Split the faces.
  std::map<CCIndex,CCIndex> splitVs;
  for(std::pair<CCIndex, std::vector<PointOnEdge>> pair : faceEdges) {
    CCIndex face = pair.first;
    std::vector<CCIndex> &pts = facePoints[face];

    // split any unsplit edges
    for(PointOnEdge ept : pair.second) {
      CCIndex edge = ept.first;
      if(splitVs.find(edge) == splitVs.end()) {
        // we have to split this edge
        std::pair<CCIndex,CCIndex> eps = cc.edgeBounds(edge);
        double t = ept.second;
	Point3d p0 = indexAttr[eps.first].pos, p1 = indexAttr[eps.second].pos;
	Point3d p2 = (1-t)*p0 + t*p1;

	CCStructure::SplitStruct ss(edge);
	CCIndexFactory.fillSplitStruct(ss);
	cc.splitCell(ss);
	indexAttr[ss.membrane].pos = p2;
	if(subdiv) subdiv->splitCellUpdate(1,cc,ss,eps.first,eps.second,t);

	splitVs[edge] = ss.membrane;
      }
      pts.push_back(splitVs[edge]);
    }

    if(pts.size() != 2)  // this shouldn't be possible
      throw QString("splitVolume: facePoints has %1 vertices").arg(pts.size());

    // these two vertices are adjacent
    bdAdj[pts[0]].insert(pts[1]);
    bdAdj[pts[1]].insert(pts[0]);

    // split the face
    CCStructure::SplitStruct ss(face);
    CCIndexFactory.fillSplitStruct(ss);
    cc.splitCell(ss, +pts[0] -pts[1]);
    if(subdiv) subdiv->splitCellUpdate(2,cc,ss);

    // Keep it triangulated if it already is
    if(triangulated) {
      quadSplit(cc, indexAttr, ss.childP, subdiv);
      quadSplit(cc, indexAttr, ss.childN, subdiv);
    }
  }

  // If we have no adjacent vertices, there was no intersection
  if(bdAdj.empty()) return false;

  // create the boundary chain for the volume split
  CCStructure::BoundaryChain chain;
  CCIndex vL = bdAdj.begin()->first, v = *bdAdj.begin()->second.begin();
  while(!bdAdj.empty()) {
    CCIndexSet &nbs = bdAdj[v];
    nbs.erase(vL); // we came from here
    if(nbs.empty())
      throw QString("splitVolume: could not extend boundary chain");
    else if(nbs.size() > 1)
      throw QString("splitVolume: boundary chain bifurcates");
    CCIndex vN = *nbs.begin();
    chain += cc.orientedEdge(v,vN);

    bdAdj.erase(v);
    vL = v;
    v = vN;
  }

  // and split it
  ss = CCStructure::SplitStruct(vol);
  CCIndexFactory.fillSplitStruct(ss);
  cc.splitCell(ss, chain);

  // we want the child on the positive "outside" of the plane to be childP
  Point3d centroidP = cellCentroidData(ss.childP,cc,indexAttr).centroid;
  if(plane.inside(centroidP)) {
    std::swap(ss.childN,ss.childP);
    cc.reverseOrientation(ss.membrane);
  }

  if(subdiv) subdiv->splitCellUpdate(3,cc,ss);

  return true;
}
