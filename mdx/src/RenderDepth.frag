uniform sampler2D tex2D;
uniform sampler2D depthValue;
varying vec2 texCoord;

void main()
{
  gl_FragColor = texture2D(tex2D, texCoord);
  gl_FragDepth = texture2D(depthValue, texCoord).a;
}

