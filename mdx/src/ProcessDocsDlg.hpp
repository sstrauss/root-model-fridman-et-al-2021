//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PROCESS_DOCS_DIALOG_HPP
#define PROCESS_DOCS_DIALOG_HPP

#include <Config.hpp>
#include <Process.hpp>

#include <QDialog>

#include <memory>

class QTreeWidget;
class QTreeWidgetItem;

namespace Ui {
  class ProcessDocsDialog;
}

namespace mdx 
{
  class mdx_EXPORT ProcessDocsDialog : public QDialog
  {
    Q_OBJECT
  public:
    ProcessDocsDialog(QWidget *parent);
    virtual ~ProcessDocsDialog();
  
  protected slots:
    // Automatically connected slots
    void on_StackTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);
    void on_MeshTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);
    void on_ToolsTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);
    void on_ModelTreeWidget_currentItemChanged(QTreeWidgetItem* item, QTreeWidgetItem* previous);
  
    void on_StackFilter_textChanged(const QString& text);
    void on_MeshFilter_textChanged(const QString& text);
    void on_ToolsFilter_textChanged(const QString& text);
    void on_ModelFilter_textChanged(const QString& text);
  
  private:
    void updateDocView(ProcessDefinition* def);
    void findProcesses();
  
    //std::unique_ptr<Ui::ProcessDocsDialog> ui;
    Ui::ProcessDocsDialog *ui;
  };
}

#endif 

