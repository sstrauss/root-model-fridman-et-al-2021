//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef COLOR_MAP_HPP
#define COLOR_MAP_HPP

#include <Config.hpp>
#include <Geometry.hpp>
#include <Attributes.hpp>
#include <Color.hpp>

namespace mdx
{
  /* A ColorMap describes how a value is converted into a color for rendering.
   * There are several ways this can be done:
   *   1. An integer value can be used as an index into a local palette
   *      of colors to choose the particular color to use;
   *   2. An integer value can be used as an index into the global
   *      "Label" palette;
   *   3. The value could itself be a color, which may be modified or
   *      used unchanged;
   *   4. A floating-point value can be used to interpolate between colors
   *      in a palette;
   *   5. A number of values can be mapped to individual colors, which may
   *      then be blended into a final result.
   *
   * The ColorMap class allows for all of these possibilities.
   */

  class mdx_EXPORT ColorMap
  {
  public:
    // The type of ColorMap is one of those given above.
    enum CMType {
      Index  ,  // An integer value used to index into a local palette
      Label  ,  // An integer value used to index into the global "Label" palette
      Color  ,  // A literal colour value
      Range     // One or many floating-point values, interpolated between colours
    };

    // The ChannelMap structure describes a scalar input value and how it is
    // transformed into a colour.  For Index, Label, and Color there is only one
    // ChannelMap used, while for Range there are as many as floating-point values
    // (channels) in the input.
    struct ChannelMap
    {
      CMType cmType;   // The type of this channel -- currently the same as the ColorMap's cmType

      QString name;    // The name of the value (used for all color map types)

      QString unit;    // The unit of the value (used for Range color maps)

      Point2i indices; // The starting and ending palette indices for mapping
                       // this color to.  For Index and Label types, the output
                       // colour will cycle between the values in this range,
                       // while for Range types the colours in the range are
                       // interpolated to get the final colour.
                       // A negative value indicates that the entire range is to be used.

      Point2d bounds;  // The bounds of the value corresponding to the
                       // low and high palette indices (used for Range color maps).

      bool visible;    // A flag indicating whether the signal component is being visualized at all.

      ChannelMap(bool _visible = true)
        : cmType(Range), indices(-1,-1), bounds(0,1), visible(_visible) {}

      bool operator==(const ChannelMap &cm) const
      {
        return (cmType == cm.cmType)
          && (name == cm.name) && (unit == cm.unit)
          && (indices == cm.indices) && (bounds == cm.bounds)
          && (visible == cm.visible);
      }
    };

    CMType cmType;
    ColorbVec colors;
    Colorb defaultColor;
    std::vector<ChannelMap> channelMaps;
    mutable bool chErr;

    ColorMap(void) : cmType(Label), colors(), defaultColor(32,32,32,255), channelMaps(1), chErr(false) {}

    inline size_t numChannels(void) const { return channelMaps.size(); }
    const ChannelMap &channelMap(uint n) const;
    ChannelMap &channelMap(uint n);
    inline bool channelError(void) const
    {
      bool tch = chErr;
      chErr = false;
      return tch;
    }

    size_t size() const { return colors.size(); }

    // Set colors
    inline void setDefaultColor(const Colorb &_color)
    {
      defaultColor = _color;
    }
    inline void setColors(const ColorbVec &_colors)
    {
      colors = _colors;
    }
    bool setColors(const QString &mapName, int sz = 0);
    QStringList getColorMapNames() const;

    // Query type
    bool isLabelMap(void) const
    { return cmType == Label; }
    bool isPassthroughMap(void) const
    { return cmType == Color; }
    bool isIndexMap(void) const
    { return cmType == Index; }
    bool isRangeMap(void) const
    { return cmType == Range; }

    // Create color map
    bool makeLabelMap(void);
    bool makePassthroughMap(void);
    bool makeIndexMap(void);
    bool makeRangeMap(uint numCh = 1);
    bool makeRangeMap(const Point2d &bounds);
    bool makeRangeMap(const Point2d &bounds, const QString &unit);

    bool reserveChannels(uint numCh);
    bool isVisible(uint channel) const;

    inline bool setName(const QString &name) { return setName(0,name); }
    bool setName(uint channel, const QString &name);
    inline bool setBounds(const Point2d &bounds) { return setBounds(0,bounds); }
    bool setBounds(uint channel, const Point2d &bounds);
    inline bool setUnit(const QString &unit) { return setUnit(0,unit); }
    bool setUnit(uint channel, const QString &unit);
    inline bool setIndices(const Point2i &indices) { return setIndices(0,indices); }
    bool setIndices(uint channel, const Point2i &indices);

    // The getRawColor methods return a color directly from the color map,
    // ignoring the ChannelMaps.
    Colorb getRawColor(int index) const;
    Colorb getRawColor(double s, const Point2d &bounds) const;
    Colorb getRawColor(double s, const Point2d &bounds, const Point2i &indices) const;

    // The getColor methods return the color corresponding to the input value,
    // given the parameters specified in the ChannelMaps.
    // They take an optional unsigned integer parameter which gives
    // an offset of the ChannelMaps to use.
    Colorb getColor(uint channelOffset, int index) const;
    Colorb getColor(uint channelOffset, const Colorb &inColor) const;

    inline Colorb getColor(uint channelOffset, double val) const
    { return getColorImpl(channelOffset, 1, &val); }
    template<size_t dim> inline Colorb getColor(uint channelOffset, const Vector<dim,double> &vec) const
    { return getColorImpl(channelOffset, dim, vec.c_data()); }

    inline Colorb getColor(uint channelOffset, float val) const
    { return getColor(channelOffset, double(val)); }
    template<size_t dim> inline Colorb getColor(uint channelOffset, const Vector<dim,float> &vec) const
    { return getColor(channelOffset, Vector<dim,double>(vec)); }

    // Without the offset parameter, the channel maps start at 0.
    inline Colorb getColor(int index) const { return getColor(0,index); }
    inline Colorb getColor(const Colorb &inColor) const { return getColor(0,inColor); }
    inline Colorb getColor(double val) const { return getColor(0,val); }
    template<size_t dim> inline Colorb getColor(const Vector<dim,double> &vec) const
    { return getColorImpl(0, dim, vec.c_data()); }

    inline Colorb getColor(float val) const { return getColor(0,double(val)); }
    template<size_t dim> inline Colorb getColor(const Vector<dim,float> &vec) const
    { return getColor(0,Vector<dim,double>(vec)); }

    // operator== for Attributes
    bool operator==(const ColorMap &cmap) const
    {
      return (cmType == cmap.cmType)
        && (colors == cmap.colors)
        && (channelMaps == cmap.channelMaps);
    }

  private:
    Colorb getColorImpl(uint channelOffset, size_t sz, const double *vec) const;
  };

  bool inline writeAttr(const ColorMap &c, QByteArray &ba)
  { 
    writeAttr(c.cmType, ba);
    writeAttr(c.colors, ba);
    writeAttr(c.channelMaps, ba);

    return true;
  }

  bool inline readAttr(ColorMap &c, const QByteArray &ba, size_t &pos) 
  { 
    readAttr(c.cmType, ba, pos);
    readAttr(c.colors, ba, pos);
    readAttr(c.channelMaps, ba, pos);

    return true;
  }

  bool inline writeAttr(const ColorMap::ChannelMap &cm, QByteArray &ba)
  {
    writeAttr(cm.cmType, ba);
    writeAttr(cm.name, ba);
    writeAttr(cm.unit, ba);
    writeAttr(cm.indices, ba);
    writeAttr(cm.bounds, ba);
    writeAttr(cm.visible, ba);

    return true;
  }
  
  bool inline readAttr(ColorMap::ChannelMap &cm, const QByteArray &ba, size_t &pos)
  {
    readAttr(cm.cmType, ba, pos);
    readAttr(cm.name, ba, pos);
    readAttr(cm.unit, ba, pos);
    readAttr(cm.indices, ba, pos);
    readAttr(cm.bounds, ba, pos);
    readAttr(cm.visible, ba, pos);

    return true;
  }
}

    
#endif
