//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef CC_DIST_OBJECT_HPP
#define CC_DIST_OBJECT_HPP

#include <Config.hpp>
#include <thrust/device_vector.h>

#include "CCIndex.hpp"
#include "CCF.hpp"
#include "Attributes.hpp"

namespace mdx
{
  /**
   * Distributed object specialization for cell complexes.
   */

  /**
   * Allocate and copy data to the GPU.
   */
  template<typename T1, typename T2> 
  int copyGPU(T1 *src, T2 *dst);

  template<typename T> 
  int allocGPU(T **vec, size_t n);

  /**
   * Distributed neighborhood object, specialized for a cell complex.
   */
  template<typename CCStructure>
  class DistNhbd
  {
  public:
    // Type of the graph
    typedef CCStructure Graph;
    // Type of a vertex
    typedef CCIndex Vertex;
    // Type of an edge
    typedef CCSignedIndex Edge;
    typedef CCIndex UEdge;

  private:
    uint _n, _nnzs, _dataSize;
    const CCStructure &_cs;
    uint _dimension;   // Dimension of cells which are "vertices"
    thrust::device_vector<uint> *_data;  // Row offsets, then column offset-edge index pairs (CSR)
    std::unordered_map<Vertex, uint> _vNum;
    std::unordered_map<UEdge, uint> _eNum;

  public:
    DistNhbd(const CCStructure &cs)
      : _n(0), _cs(cs), _dimension(0), _data(0) {}
    DistNhbd(const CCStructure &cs, uint dim)
      : _n(0), _cs(cs), _dimension(dim), _data(0) {}

    // Clean up
    virtual ~DistNhbd() { allocGPU(&_data, 0); }

    // Return # of vertices
    uint n() const { return _n; };

    // Return # (off-diagonal) non-zero entries
    uint nnzs() const { return _nnzs; }

    // Return CSR data size (usually (n+1) + 2*nnzs)
    uint dataSize() const { return _dataSize; }

    // Return dimension of cells we care about
    uint dimension() const { return _dimension; };
  
    // Return pointer to device buffer
    thrust::device_vector<uint> *data() { return _data; };
  
    // Return graph
    const CCStructure& graph() { return _cs; };
  
    // Return vertex number from our ordering map
    uint vNum(const Vertex &v) { return _vNum[v]; };

    // Return edge number in DistEdge
    uint eNum(const UEdge &e) { return _eNum[e]; }
    uint eNum(const Edge &e)
    {
      uint eVal = _eNum[~e];
      if(e.orientation() == ccf::POS)
        return eVal;
      else
        return eVal + 1;
    }

    // Return oriented edge in upper triangle
    Edge upperEdge(const UEdge &e)
    {
      if(dimension() != 0) return CCIndex::UNDEF;

      std::pair<Vertex,Vertex> eps = _cs.edgeBounds(e);
      uint v1 = vNum(eps.first), v2 = vNum(eps.second);
      if(v1 < v2) return +e;
      else return -e;
    }

    // Return oriented edge in upper triangle
    inline Edge upperEdge(const Edge &e)
    { return upperEdge(~e); }
   
    // Allocate and send to GPU
    void write()
    {
      // Record cell numbers
      const std::vector<CCIndex> &cells = _cs.cellsOfDimension(_dimension);
      _n = cells.size();
      _vNum.clear();
      for(uint i = 0 ; i < _n ; i++)
        _vNum[cells[i]] = i;

      // Only 0-cells, right now, actually _have_ a specified neighborhood
      // (which is defined by the 1-flips); the rest have no neighborhoods.
      std::vector<Point3u> adjacencies;
      if(_dimension == 0)
      {
        // Record edge numbers (positive / negative orientations are staggered)
        const std::vector<CCIndex> &edges = _cs.cellsOfDimension(1);
        _eNum.clear();
        for(uint i = 0 ; i < edges.size() ; i++)
          _eNum[edges[i]] = 2*i;

        // Extract vector of 1-flips
        std::vector<typename CCStructure::FlipI>
          ft1Flips(_cs.matchV(CCIndex::BOTTOM,CCIndex::Q,CCIndex::Q,CCIndex::Q));
        _nnzs = 2 * ft1Flips.size();

        // Convert to vector of triples (v,v,e)
        adjacencies.resize(_nnzs);
        #pragma omp parallel for
        for(uint i = 0 ; i < ft1Flips.size() ; i++)
        {
          const typename CCStructure::FlipI &flip = ft1Flips[i];
          uint v0 = _vNum[flip.facet[0]], v1 = _vNum[flip.facet[1]];
          uint e = _eNum[flip.interior];
          adjacencies[2*i  ] = Point3u(v0,v1,e);
          adjacencies[2*i+1] = Point3u(v1,v0,e+1);
        }
        std::sort(adjacencies.begin(),adjacencies.end());
      } else {
        adjacencies.clear();
       _nnzs = 0;
      }

      _dataSize = (_n + 1) + (2 * _nnzs); // row offsets + (column offset, edge index)

      // Copy temporary adjacency table into host vector
      thrust::host_vector<uint> hData(_dataSize);
      uint ridx = 0, cidx = _n + 1;
      for(uint i = 0 ; i < _nnzs ; i++)
      {
        const Point3u &triple = adjacencies[i];
        while(ridx <= triple[0]) hData[ridx++] = cidx;
        hData[cidx++] = triple[1];
        hData[cidx++] = triple[2];
      }
      while(ridx <= _n) hData[ridx++] = cidx;

      // Copy data to device
      try {
        allocGPU(&_data, _dataSize);
      } catch (const std::exception &e) {
        throw(QString("DistNhbd::write Unable to allocate object of sizes %1 %2, err:%3")
              .arg(_dataSize).arg(sizeof(Point3u)).arg(e.what()));
      } catch(...) {
        throw(QString("DistNhbd::write Unable to allocate object of sizes %1 %2")
              .arg(_dataSize).arg(sizeof(Point3u)));
      }
      try {
        copyGPU(&hData, _data);
      } catch (const std::exception &e) {
        throw(QString("DistNhbd::write Unable to copy object of sizes %1 %2, err:%3")
              .arg(_dataSize).arg(sizeof(Point3u)).arg(e.what()));
      } catch(...) {
        throw(QString("DistNhbd::write Unable to copy object of sizes %1 %2")
              .arg(_dataSize).arg(sizeof(Point3u)));
      }
    }
  };

  /**
   * Distributed vertex object, specialized for cell complexes.
   */
  template<typename  DistNhbdT, typename DataT>
  class DistVertex
  {
    DistNhbdT &_nhbd; // Distributed neighborhood object
    thrust::device_vector<DataT> *_data;     // Distributed data
    bool _hasHostData;// Is there host-side storage? or is it GPU only?
  
  protected:
    // Constructor for object with host data, only from derived classes
    DistVertex(DistNhbdT &nhbd, bool hasData) : _nhbd(nhbd), _data(0), _hasHostData(hasData) {}
  
  public:
    // Constructor for object with no host data (GPU only)
    DistVertex(DistNhbdT &nhbd) : _nhbd(nhbd), _data(0), _hasHostData(false) {}
  
    // Clean up
    virtual ~DistVertex() { allocGPU(&_data, 0); }
  
    // Get a data entry
    virtual DataT *vData(const typename DistNhbdT::Vertex &) { return 0; }
  
    // Return neighborhood object
    DistNhbdT &nhbd() { return _nhbd; };
  
    // Return pointer to device buffer
    thrust::device_vector<DataT> *data() 
    { 
      alloc();
      return _data;
    };
  
    // Allocate storage for a distributed vertex on GPU
    void alloc()
    {
      try {
        allocGPU(&_data, _nhbd.n());
      } catch (const std::exception &e) {
        throw(QString("Unable to allocate distributed vertex object of sizes %1 %2, err:%3")
              .arg(_nhbd.n()).arg(sizeof(DataT)).arg(e.what()));
      } catch(...) {
        throw(QString("Unable to allocate distributed vertex object of sizes %1 %2")
              .arg(_nhbd.n()).arg(sizeof(DataT)));
      }
    }
  
    // Copy vertex data to GPU
    void write()
    {
      if(!_hasHostData)
        throw(QString("Error: Distributed vector has no host storage"));
      if(_nhbd.n() <= 0)
        return;

      // Copy to host vector
      thrust::host_vector<DataT> hData(_nhbd.n());
      for(CCIndex v : _nhbd.graph().cellsOfDimension(_nhbd.dimension()))
        hData[_nhbd.vNum(v)] = *vData(v);

      // Write to GPU
      alloc();
      try {
        copyGPU(&hData, _data);
      } catch (const std::exception &e) {
        throw(QString("DistVertex::write Unable to write object of sizes %1 %2, err:%3")
              .arg(_nhbd.n()).arg(sizeof(DataT)).arg(e.what()));
      } catch(...) {
        throw(QString("DistVertex::write Unable to write object of sizes %1 %2")
              .arg(_nhbd.n()).arg(sizeof(DataT)));
      }
    }
  
    // Copy vertex data to Host
    void read()
    {
      if(!_hasHostData)
        throw(QString("Error: Distributed vector has no host storage"));
      if(_nhbd.n() <= 0)
        return;
      if(_data->size() != _nhbd.n())
        throw(QString("Error: Distributed vector size mismatch on device buffer, actual %1, expected %2")
              .arg(_data->size()).arg(_nhbd.n()));
  
      // Read from GPU
      thrust::host_vector<DataT> hData(_nhbd.n());
      try {
        copyGPU(_data, &hData);
      } catch (const std::exception &e) {
        throw(QString("DistVertex::read Unable to read object of sizes %1 %2, err:%3")
              .arg(_nhbd.n()).arg(sizeof(DataT)).arg(e.what()));
      } catch(...) {
        throw(QString("DistVertex::read Unable to read object of sizes %1 %2")
              .arg(_nhbd.n()).arg(sizeof(DataT)));
      }
      // Write to cell complex
      for(CCIndex v : _nhbd.graph().cellsOfDimension(_nhbd.dimension()))
        *vData(v) = hData[_nhbd.vNum(v)];
    }
  };

   /**
   * \class DistVertexAttr DistObject.hpp <DistObject.hpp>
   *
   * Derived class from distributed vertex with storage in an attribute map keyed by vertex.
   */
  template <typename DistNhbdT, typename ContentT, typename DataT>
  class DistVertexAttr : public DistVertex<DistNhbdT, DataT>
  {
  private:
    AttrMap<typename DistNhbdT::Vertex, ContentT> *_map;
    DataT ContentT::*_off;
  
  public:
    // Constructor for object with memory in vertices of the graph
    DistVertexAttr(DistNhbdT &nhbd, AttrMap<typename DistNhbdT::Vertex, ContentT> *map, 
                                                                         DataT ContentT::*off) 
      : DistVertex<DistNhbdT, DataT>(nhbd, true), _map(map), _off(off) {}
  
    // Get data 
    DataT *vData(const typename DistNhbdT::Vertex &v)
    {
      return &((*_map)[v].*_off);
    }

    // Set (or reset) attribute map. I may change when mesh is reset.
    void setAttrMap(AttrMap<typename DistNhbdT::Vertex, ContentT> *map)
    {
      _map = map;
    }
  };
  
  /**
   * \class DistEdge DistObject.hpp <DistObject.hpp>
   *
   * Distributed edge object, specialized to cell complexes.
   * Changes both in iteration and lookup (i.e. from pairs of vertices to edge indexes)
   */
  template<typename DistNhbdT, typename DataT>
  class DistEdge
  {
  public:
    DistNhbdT &_nhbd; // Distributed neighborhood object
    bool _hasHostData;// Is there host-side storage? (or is it GPU only)
    thrust::device_vector<DataT> *_data;     // Distributed data

  public:
    // Constructor for object with no host data (only GPU)
    DistEdge(DistNhbdT &nhbd)
      : _nhbd(nhbd), _hasHostData(false), _data(0) {}

  protected:
    // Constructor for object with host data, only from derived classes
    DistEdge(DistNhbdT &nhbd, bool hasData)
      : _nhbd(nhbd), _hasHostData(hasData), _data(0) {}
  
  public:
    // Clean up
    virtual ~DistEdge()  { allocGPU(&_data, 0); }

    // Return neighborhood object
    DistNhbdT &nhbd() { return _nhbd; }; 
    
    // Get edge data
    virtual DataT *eData(const typename DistNhbdT::Edge &e1) { return 0; }

    // Return pointer to device buffer, auto alloc
    thrust::device_vector<DataT> *data() 
    { 
      alloc();
      return _data;
    };

    // Allocate storage for a distributed edge on GPU
    void alloc()
    {
      try {
        allocGPU(&_data, _nhbd.nnzs());
      } catch (const std::exception &e) {
        throw(QString("DistEdge::alloc Unable to allocate object of sizes %1 %2, err:%3")
              .arg(_nhbd.nnzs()).arg(sizeof(DataT)).arg(e.what()));
      } catch(...) {
        throw(QString("DistEdge::alloc Unable to allocate object of sizes %1 %2")
              .arg(_nhbd.nnzs()).arg(sizeof(DataT)));
      }
    } 
  
    virtual void write()
    {
      if(!_hasHostData)
        throw(QString("Error: Distributed edge has no host storage"));
      if(_nhbd.nnzs() <= 0)
        return;

      if(_nhbd.dimension() == 0)
      {
        // Data laid out in order of eNum
        thrust::host_vector<DataT> hData(_nhbd.nnzs());

        const std::vector<CCIndex> &edges = _nhbd.graph().edges();
        for(CCIndex edge : edges)   // could this be even simpler, since eNum[edge] = index in edges()?
        {
          uint eix = _nhbd.eNum(edge);
          DataT *s1 = eData(+edge), *s2 = eData(-edge);
          hData[eix  ] = *s1;
          hData[eix+1] = *s2;
        }

        // Write to GPU
        alloc();
        try {
          copyGPU(&hData, _data);
        } catch (const std::exception &e) {
          throw(QString("DistEdge::write Unable to write object of sizes %1 %2, err:%3")
                .arg(_nhbd.nnzs()).arg(sizeof(DataT)).arg(e.what()));
        } catch(...) {
          throw(QString("DistEdge::write Unable to write object of sizes %1 %2")
                .arg(_nhbd.nnzs()).arg(sizeof(DataT)));
        }
      }
    }
    
    // Copy edge data to host
    virtual void read()
    {
      if(!_hasHostData)
        throw(QString("Error: Distributed edge has no host storage"));
      if(_nhbd.nnzs() <= 0)
        return;
      if(_data->size() != _nhbd.nnzs())
        throw(QString("Error: Distributed edge size mismatch on device buffer, actual %1, expected %2")
              .arg(_data->size()).arg(_nhbd.nnzs()));

      if(_nhbd.dimension() == 0) { // only vertex-graphs have DistEdges
        // Copy data from GPU
        thrust::host_vector<DataT> hData(_nhbd.nnzs());
        try {
          copyGPU(_data, &hData);
         } catch (const std::exception &e) {
          throw(QString("DistEdge::read Unable to read object of sizes %1 %2, err:%3")
                .arg(_nhbd.nnzs()).arg(sizeof(DataT)).arg(e.what()));
        } catch(...) {
          throw(QString("DistEdge::read Unable to read object of sizes %1 %2")
                .arg(_nhbd.nnzs()).arg(sizeof(DataT)));
        } 

        const std::vector<CCIndex> &edges = _nhbd.graph().edges();
        for(CCIndex edge : edges)   // could this be even simpler, since eNum[edge] = index in edges()?
        {
          uint eix = _nhbd.eNum(edge);
          DataT *s1 = eData(+edge), *s2 = eData(-edge);
          *s1 = hData[eix];
          *s2 = hData[eix+1];
        }
      }
    }
  };
  
  /**
   * \class DistEdgeAttr DistObject.hpp <DistObject.hpp>
   *
   * Derived class from distributed edge with storage in an attribute map keyed by an edge index
   */
  template <typename DistNhbdT, typename ContentT, typename DataT>
  class DistEdgeAttr : public DistEdge<DistNhbd<CCStructure>, DataT>
  {
  public:
    AttrMap<typename DistNhbdT::Edge, ContentT> *_map;
    DataT ContentT::*_off;
  
  public:
    // Constructor for object with memory in the attributes system
    DistEdgeAttr(DistNhbdT &nhbd, AttrMap<typename DistNhbdT::Edge, ContentT> *map, DataT ContentT::*off) 
      : DistEdge<DistNhbdT, DataT>(nhbd, true), _map(map), _off(off) {}

    // Get data 
    DataT *eData(const typename DistNhbdT::Edge &e)
    {
      return &((*_map)[e].*_off);
    }

    // Set (or reset) attribute map. I may change when mesh is reset.
    void setAttrMap(AttrMap<typename DistNhbdT::Edge, ContentT> *map)
    {
      _map = map;
    }
  };

  /**
   * \class DistEdgeSymmetricAttr DistObject.hpp <DistObject.hpp>
   *
   * Distributed edge with data from an attribute map over unsigned edges;
   * this maps to a DistEdgeAttr with the same value on both orientations of an edge.
   */
  template <typename DistNhbdT, typename ContentT, typename DataT>
  class DistEdgeSymmetricAttr : public DistEdge<DistNhbdT, DataT> {};

  template <typename ContentT, typename DataT>
  class DistEdgeSymmetricAttr<DistNhbd<CCStructure>,ContentT,DataT>
    : public DistEdge<DistNhbd<CCStructure>, DataT>
  {
  public:
    typedef DistNhbd<CCStructure> DistNhbdT;
    typedef typename DistNhbdT::Edge::object_type SymmEdge;
    typedef DistEdge<DistNhbdT,DataT> ParentT;    // why is ParentT:: scoping necessary here?
    AttrMap<SymmEdge, ContentT> *_map;
    DataT ContentT::*_off;
  
  public:
    // Constructor for object with memory in the attributes system
    DistEdgeSymmetricAttr(DistNhbdT &nhbd, AttrMap<SymmEdge, ContentT> *map, DataT ContentT::*off) 
      : DistEdge<DistNhbdT, DataT>(nhbd, true), _map(map), _off(off) {}

    // Send edge data to GPU
    void write()
    {
      DistNhbdT &nhbd = ParentT::nhbd();
      if(nhbd.nnzs() <= 0)
        return;

      if(nhbd.dimension() == 0)
      {
        // Data laid out in order of eNum
        thrust::host_vector<DataT> hData(nhbd.nnzs());

        const std::vector<CCIndex> &edges = nhbd.graph().edges();
        for(CCIndex edge : edges)
        {
          uint eix = nhbd.eNum(edge);
          DataT *s = eData(edge);
          hData[eix  ] = *s;
          hData[eix+1] = *s;
        }

        // Write to GPU
        ParentT::alloc();
        try {
          copyGPU(&hData, ParentT::data());
        } catch (const std::exception &e) {
          throw(QString("DistEdgeSymmetricAttr::write Unable to write object of sizes %1 %2, err:%3")
                .arg(nhbd.nnzs()).arg(sizeof(DataT)).arg(e.what()));
        } catch(...) {
          throw(QString("DistEdgeSymmetricAttr::write Unable to write object of sizes %1 %2")
                .arg(nhbd.nnzs()).arg(sizeof(DataT)));
        }
      }
    }

    // Copy edge data to host
    void read()
    {
      DistNhbdT &nhbd = ParentT::nhbd();
      if(nhbd.nnzs() <= 0)
        return;
      if(ParentT::data()->size() != nhbd.nnzs())
        throw(QString("Error: Distributed edge size mismatch on device buffer, actual %1, expected %2")
              .arg(ParentT::data()->size()).arg(nhbd.nnzs()));

      if(nhbd.dimension() == 0) {
        // Copy data from GPU
        thrust::host_vector<DataT> hData(nhbd.nnzs());
        try {
          copyGPU(ParentT::data(), &hData);
        } catch (const std::exception &e) {
          throw(QString("DistEdgeSymmetricAttr::read Unable to read object of sizes %1 %2, err:%3")
                .arg(nhbd.nnzs()).arg(sizeof(DataT)).arg(e.what()));
        } catch(...) {
          throw(QString("DistEdgeSymmetricAttr::read Unable to read object of sizes %1 %2")
                .arg(nhbd.nnzs()).arg(sizeof(DataT)));
        } 

        const std::vector<CCIndex> &edges = nhbd.graph().edges();
        for(CCIndex edge : edges)   // could this be even simpler, since eNum[edge] = index in edges()?
        {
          uint eix = nhbd.eNum(edge);
          DataT *s = eData(edge);
          *s = hData[eix];
        }
      }
    }

    // Get data 
    DataT *eData(const SymmEdge &e)
    {
      return &((*_map)[e].*_off);
    }

    // Set (or reset) attribute map. I may change when mesh is reset.
    void setAttrMap(AttrMap<SymmEdge, ContentT> *map)
    {
      _map = map;
    }
  };
}
#endif
