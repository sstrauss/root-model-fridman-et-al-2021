//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_UTILS_HPP
#define CC_UTILS_HPP

#include <Config.hpp>
#include <CCF.hpp>
#include <Types.hpp>
#include <CellTissue.hpp>
#include <Attributes.hpp>
#include <Subdivide.hpp>

namespace mdx
{
  // Some useful typedefs
  typedef CCStructure::FlipI Flip;
  typedef std::vector<Flip> FlipVec;
  typedef tbb::concurrent_vector<Flip> FlipTbbVec;
  typedef CCStructure::SplitStruct SplitStruct;
  typedef CCStructure::CellTuple CellTuple;
  typedef CCStructure::BoundaryChain BoundaryChain;

  /// Select a cell, normally when somehting goes wrong. Edges cause vertices to be selected. 
  mdx_EXPORT void selectCell(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex c);
  /// Select two cells. 
  mdx_EXPORT void selectCells(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex a, CCIndex b);
  /// Select all the cells in a flip 
  mdx_EXPORT void selectFlip(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCStructure::FlipI &flip);

  /// Get connected faces
  mdx_EXPORT bool getConnectedFaces(const CCStructure &cs, CCIndexSet &faces);
  /// Get connected faces that are only on one volume, ie are on the surface
  mdx_EXPORT bool getConnectedSurface(const CCStructure &cs, CCIndexSet &faces);

  /// Add a face to a cell complex
  mdx_EXPORT bool addFace(CCStructure &cs, CCIndex face, const std::vector<CCIndex> &vertices);

  /// Get edge between v1 and v2, special case of join when you don't want faces
  mdx_EXPORT CCIndex edgeBetween(const CCStructure &cs, CCIndex v1, CCIndex v2);

  /// Get neighbors (of same dimension) with in a radius of a cell (normally vertex)
  mdx_EXPORT CCIndexSet neighbors(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndex v, float radius);

  /// Get the neighbors of a cell of the same dimension with the higher dimensional cell between them
  mdx_EXPORT CCIndexPairVec neighborInteriors(const CCStructure &cs, CCIndex v);

  /// Get the neighbors of a vertex along with the edges in no order
  mdx_EXPORT std::vector<Flip> neighborVertexFlips(const CCStructure &cs, CCIndex v);

  /// Get the(an) oriented wall membrane between two cells. 
  mdx_EXPORT CCSignedIndex orientedMembrane(const CCStructure &cs, CCIndex c, CCIndex n);

  /// Get faces for a vertex in no particular order
  mdx_EXPORT CCIndexVec vertexFaces(const CCStructure &cs, CCIndex v, bool infty = false);

  /// Get vertices for a face in counter-clockwise order
  mdx_EXPORT CCIndexVec faceVertices(const CCStructure &cs, CCIndex f);

  /// Get vertices and edges of a triangle in counter-clockwise order
  mdx_EXPORT bool triangleVerticesEdges(const CCStructure &cs, CCIndex face, CCIndexVec &vertices, CCIndexVec &edges);

  /// Get the vertex neighbors of a vertex in counter-clockwise order
  mdx_EXPORT CCIndexVec vertexNeighbors(const CCStructure &cs, CCIndex v);

  /// Make a cell complex from a list of triangles
  mdx_EXPORT bool ccFromTriangles(CCStructure &cs, const CCIndexVec &vertices, const Point3uVec &triangles);

  /// Make a cell complex from a list of vertices and faces
  mdx_EXPORT bool ccFromFaces(CCStructure &cs, const CCIndexVec &vertices, const CCIndexVec &faces, const UIntVecVec &faceVertices);

  /// Create a cell complex from a list of vertices, edges and faces
  mdx_EXPORT bool ccFromFaces(CCStructure &cs, const CCIndexVec &vertices, const CCIndexVec &edges, 
         const UIntPairVec &edgeVertices, const CCIndexVec &faces, const UIntVecVec &faceVertices, const UIntVecVec &faceEdges);

  /// Add volumes to a cell complex, note that faces are 1-indexed with +/- for orientation
  mdx_EXPORT bool ccAddVolumes(CCStructure &cs, const CCIndexVec &faces, const CCIndexVec &volumes, const IntVecVec &volumeFaces);

  /// Make a cell complex from a list of vertices, faces and volumes
  mdx_EXPORT bool ccFromFacesAndVolumes(CCStructure &cs, const CCIndexVec &vertices, 
             const CCIndexVec &faces, const UIntVecVec &faceVertices, const CCIndexVec &volumes, const IntVecVec &volumeFaces);

  /// Make a cell complex from a list of vertices, edges, faces and volumes
  mdx_EXPORT bool ccFromFacesAndVolumes(CCStructure &cs, const CCIndexVec &vertices, const CCIndexVec &edges, const UIntPairVec &edgeVertices, 
      const CCIndexVec &faces, const UIntVecVec &faceVertices, const UIntVecVec &faceEdges, const CCIndexVec &volumes, const IntVecVec &volumeFaces);

  /// Adds volumes to a CC from a map of known faces
  mdx_EXPORT bool addVolumesCC(CCStructure &cs, CCIndexDataAttr &indexAttr, std::map<int, std::set<CCIndex> > &volumeLabelToFacesMap);

  /// Extract a number of cells (with their boundary elements) into a new cell complex
  mdx_EXPORT CCStructure extractCells(const CCStructure &oldCS, const CCIndexVec &cells);
  mdx_EXPORT CCStructure extractCell(const CCStructure &oldCS, CCIndex cell);

  /// Delete a number of cells from a cell complex
  mdx_EXPORT bool deleteCells(CCStructure &cs, const CCIndexSet &toDelete, CCIndexDataAttr &indexAttr);

  /// Class to cache neighbors
  class mdx_EXPORT CellNeighbors
  {
  public:
    CellNeighbors() {}
    CellNeighbors(const CellNeighbors &other) { neighbors = other.neighbors; }
    CellNeighbors(const CCStructure &cs, const CCIndexVec &cells, bool clear = true) { init(cs, cells, clear); }
    bool init(const CCStructure &cs, const CCIndexVec &cells, bool clear = true);
    CCIndexSet operator()(CCIndex c) { return neighbors[c]; }

  private:
    AttrMap<CCIndex, CCIndexSet> neighbors;
  };

  /// Class to find distance to 
  class mdx_EXPORT BorderDistance
  {
  public:
    BorderDistance() {}
    BorderDistance(const BorderDistance &other) 
    { 
      _distanceB = other._distanceB; 
      _minB = other._minB; 
      _distanceJ = other._distanceJ; 
      _minJ = other._minJ; 
      _wall = other._wall;
    }
    BorderDistance(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &cells,
          bool clear = true, bool doJunctions = false) { init(cs, indexAttr, cells, clear, doJunctions); }
    bool init(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &cells, 
                                                             bool clear = true, bool doJunctions = false);
    double distanceB(CCIndex c) { return _distanceB[c]; }
    CCIndex minB(CCIndex c) { return _minB[c]; }
    double distanceJ(CCIndex c) { return _distanceJ[c]; }
    CCIndex minV(CCIndex c) { return _minJ[c]; }
    CCIndex wall(CCIndex c) { return _wall[c]; }

  private:
    AttrMap<CCIndex, double> _distanceB;
    AttrMap<CCIndex, CCIndex> _minB;
    AttrMap<CCIndex, double> _distanceJ;
    AttrMap<CCIndex, CCIndex> _minJ;
    AttrMap<CCIndex, CCIndex> _wall;
  };

  /// Create border flips
  mdx_EXPORT bool createBorderFlips(ccf::CCStructure &cs, CCIndexDataAttr &indexAttr);

  /// Dijkstra algorithm for finding the shortest path between cells using shared wallAreas map as input
  mdx_EXPORT std::map<int, double> dijkstra(std::set<int>& allLabels, std::map<IntIntPair, double>& wallAreas, std::set<int>& selectedCells, 
    bool equalWeights, double cutOffDistance, double wallThreshold);

  /// Returns the border vertices of a cell in order (TODO probably random clockwise or counterclockwise)
  mdx_EXPORT std::vector<CCIndex> cellBoundary(const CCStructure &cs, const CCIndexDataAttr &indexAttr, int label);

  /// Returns centroid data for a given cell in the given cell complex with the given attributes
  mdx_EXPORT CentroidData<Point3d> cellCentroidData(CCIndex cell, const CCStructure &cs, const CCIndexDataAttr &attr);

  /// Intersection of two CCIndex sets
  CCIndexSet intersect(const CCIndexSet &set1, const CCIndexSet &set2);

  /// Find the maximum label value
  int calcMaxLabel(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
}

#endif

