//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_TRANSFORM_HPP
#define STACK_PROCESS_TRANSFORM_HPP

/**
 * \file StackProcessTransform.hpp
 * Contains processes to save/load the transform
 * Note this includes the mesh
 */
#include <Process.hpp>

namespace mdx 
{
  ///\addtogroup StackProcess
  ///@{

  /**
   * \class SaveTransform StackProcessTransform.hpp <StackProcessTransform.hpp>
   *
   * Save the frame (or transform) matrix to a file, as a list of values in column-major.
   */
  class mdxBase_EXPORT SaveTransform : public Process 
  {
    Q_OBJECT
  
  public:
    SaveTransform(const Process& process) : Process(process) 
    {
      setName("Stack/Transform/Save Transform");
      setDesc("Save the frame matrix (or transform if trans checked) to a file");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "File name to save transform", "");
    }
    bool initialize(QWidget* parent);
  
    bool run() 
    {
      return run(currentStack(), parm("File Name"));
    }
    bool run(Stack* stack, const QString& filename);
  };
  
  /**
   * \class LoadTransform StackProcessTransform.hpp <StackProcessTransform.hpp>
   *
   * Load the frame (or transform) matrix from a file containing a list of values in column-major.
   */
  class mdxBase_EXPORT LoadTransform : public Process 
  {
    Q_OBJECT
  
  public:
    LoadTransform(const Process& process) : Process(process) 
    {
      setName("Stack/Transform/Load Transform");
      setDesc("Save the frame matrix (or transform if trans checked) from a file");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "File name to save transform", "");
    }
    bool initialize(QWidget* parent);
  
    bool run() 
    {
      return run(currentStack(), parm("File Name"));
    }
    bool run(Stack* stack, const QString& filename);
  };
  ///@}
}

#endif
