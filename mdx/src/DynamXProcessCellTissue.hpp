//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef DYNAMX_PROCESS_CELL_TISSUE_HPP
#define DYNAMX_PROCESS_CELL_TISSUE_HPP

/**
 * \file CellTissueParms.hpp
 *
 * This file contains the class holding the parameters for Cell Tissue
 */

#include <Process.hpp>

namespace mdx 
{

	// Class to hold Cell Tissue parameters
  class mdxBase_EXPORT CellTissueParms : public Process 
  {
  public:
    CellTissueParms(const Process &process) : Process(process) {}

    // Don't do anything, process is only for parms
    bool run(const QStringList &) { return true; }

    // Functions for Gui
    QString description() const { return "Parameters for FEM simulation for MDXM meshes"; }

    QStringList parmNames() const 
    {
      QVector <QString> vec(CellTissueMgx2d::pNumParms);

      vec[CellTissueMgx2d::pCellUpdGeomMode] = "CellUpdGeomMode";
      vec[CellTissueMgx2d::pCellWallWidth] = "CellWallWidth";
      vec[CellTissueMgx2d::pCellWallCorner] = "CellWallCorner";
      vec[CellTissueMgx2d::pCellColorBegin] = "CellColorBegin";
      vec[CellTissueMgx2d::pCellColorEnd] = "CellColorEnd";
      vec[CellTissueMgx2d::pCellColorCenter] = "CellColorCenter";
      vec[CellTissueMgx2d::pCellBorderColor] = "CellBorderColor";
      vec[CellTissueMgx2d::pCellPolWidth] = "CellPolWidth";
      vec[CellTissueMgx2d::pCellPolCorner] = "CellPolCorner";
      vec[CellTissueMgx2d::pCellPolColorBegin] = "CellPolColorBegin";
      vec[CellTissueMgx2d::pCellPolColorEnd] = "CellPolColorEnd";
      vec[CellTissueMgx2d::pCellNormalColor] = "CellNormalColor";
      vec[CellTissueMgx2d::pCellGraphColor] = "CellGraphColor";
      vec[CellTissueMgx2d::pCellLineColor] = "CellLineColor";
      vec[CellTissueMgx2d::pCellLineWidth] = "CellLineWidth";

      return vec.toList();
    }
    QStringList parmDescs() const 
    {
      QVector <QString> vec(CellTissueMgx2d::pNumParms);

      vec[CellTissueMgx2d::pCellUpdGeomMode] = "1-Area, 2-Length, 4-Center, 8-Normals";
      vec[CellTissueMgx2d::pCellWallWidth] = "Width to draw cell walls";
      vec[CellTissueMgx2d::pCellWallCorner] = "Width of cell wall corners";
      vec[CellTissueMgx2d::pCellColorBegin] = "Start of cell color ramp";
      vec[CellTissueMgx2d::pCellColorEnd] = "End of cell color ramp";
      vec[CellTissueMgx2d::pCellColorCenter] = "Difference in color between center and edges";
      vec[CellTissueMgx2d::pCellBorderColor] = "Color of cell borders";
      vec[CellTissueMgx2d::pCellPolWidth] = "Width to draw cell polarity";
      vec[CellTissueMgx2d::pCellPolCorner] = "Size of cell polarity corner triangles";
      vec[CellTissueMgx2d::pCellPolColorBegin] = "Start of polarity color ramp";
      vec[CellTissueMgx2d::pCellPolColorEnd] = "End of polarity color ramp";
      vec[CellTissueMgx2d::pCellNormalColor] = "Color to draw normals";
      vec[CellTissueMgx2d::pCellGraphColor] = "Color to draw cell graph";
      vec[CellTissueMgx2d::pCellLineColor] = "Color to draw lines of graph";
      vec[CellTissueMgx2d::pCellLineWidth] = "Cell line width";

      return vec.toList();
    }
    QStringList parmDefaults() const 
    {
      QVector <QString> vec(CellTissueMgx2d::pNumParms);

      vec[CellTissueMgx2d::pCellUpdGeomMode] = "15";
      vec[CellTissueMgx2d::pCellWallWidth] = "0.025";
      vec[CellTissueMgx2d::pCellWallCorner] = "0.15";
      vec[CellTissueMgx2d::pCellColorBegin] = "1";
      vec[CellTissueMgx2d::pCellColorEnd] = "2";
      vec[CellTissueMgx2d::pCellColorCenter] = "0.3";
      vec[CellTissueMgx2d::pCellBorderColor] = "0";
      vec[CellTissueMgx2d::pCellPolWidth] = "0.0";
      vec[CellTissueMgx2d::pCellPolCorner] = "0.3";
      vec[CellTissueMgx2d::pCellPolColorBegin] = "3";
      vec[CellTissueMgx2d::pCellPolColorEnd] = "4";
      vec[CellTissueMgx2d::pCellNormalColor] = "5";
      vec[CellTissueMgx2d::pCellGraphColor] = " 5";
      vec[CellTissueMgx2d::pCellLineColor] = "5";
      vec[CellTissueMgx2d::pCellLineWidth] = "1.0";

      return vec.toList();
    }
    // Icon file
    QIcon icon() const { return QIcon(":/images/Parameters.png"); }
  };
}

#endif
