# MakeFile for MorphoDynamX

# Driectories
MDX_BIN = $(shell MorphoDynamX --dir)
MDX_LIB = $(shell MorphoDynamX --libs)
MDX_INCLUDE = $(shell MorphoDynamX --include)
MDX_P_INCLUDE = $(shell MorphoDynamX --process)/include
MDX_PROCESS = $(shell MorphoDynamX --process)
# Flags for compilation
QT_DIR=$(MDX_BIN)/../Qt
CUDA_DIR=$(MDX_BIN)/../cuda
INCLUDES = -I"$(QT_DIR)/mkspecs/linux-g++-64" -I"$(QT_DIR)/include/QtCore" \
  -I"$(QT_DIR)/include/QtGui" -I"$(QT_DIR)/include/QtOpenGL" -I"$(QT_DIR)/include/QtXml" \
  -I"$(QT_DIR)/include" -I"$(MDX_INCLUDE)" -I"$(MDX_P_INCLUDE)" -isystem "$(CUDA_DIR)/include" -I. 
#  -I/usr/X11R6/include 
DEFINES = -Dcimg_display=0 -DQT_NO_DEBUG -DQT_PLUGIN -DQT_XML_LIB -DQT_OPENGL_LIB \
  -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED -DQT_MESSAGELOGCONTEXT
CFLAGS = -m64 -pipe -O3 -D_REENTRANT -Wall -W -fPIC $(DEFINES)
CXXFLAGS = -m64 -pipe -Wno-unused-local-typedefs -Wno-system-headers -fopenmp -O3 \
  -D_REENTRANT -Wall -W -std=c++14 $(DEFINES)
LD_FLAGS = -m64 -Wl,-O1 -shared
#LIBS = -L/usr/lib/x86_64-linux-gnu -L/usr/X11R6/lib64 -L/usr/local/lib 
LIBS = -L"$(QT_DIR)/lib" -L"$(MDX_LIB)" -lmdx -L"$(MDX_PROCESS)" -lmdxBase -lQtXml4 -lQtOpenGL4 -lQtGui4 -lQtCore4 -fopenmp -lpthread 

#LD_SO_FLAGS?=  -pipe -mfpmath=sse -msse3 -O3 -rdynamic 
#LD_EXE_FLAGS?=  -pipe -mfpmath=sse -msse3 -O3 -rdynamic 

# Command definitions
CXX = g++
CC = gcc
MOC = moc

RCC = rcc
UIC = uic
RM = rm -f
CP = cp -f

# Compile rules
%.dll:
	$(CXX) -o $@ $^ -shared -Wl,-soname=$@ $(LD_FLAGS) $(LIBS)
#-lGL -lpthread 

%.o: %.cpp
	$(CXX) -c -o $@ $< -fpic $(CXXFLAGS) $(INCLUDES)

%.moc: %.hpp
	$(MOC) -o $@ $<

%.moc: %.cpp $(wildcard %.hpp)
	$(MOC) -o $@ $<

qrc_%.cpp: %.qrc
	$(RCC) -name $(basename $<) -o $@ $<

ui_%.h: %.ui
	$(UIC) -o $@ $<

clean:
	$(RM) *.o *.moc qrc_*.cpp ui_*.h

install:
	$(CP) *.dll $(MDX_PROCESS)

