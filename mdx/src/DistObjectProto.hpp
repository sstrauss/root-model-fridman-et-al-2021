//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef DIST_OBJECT_PROTO_HPP
#define DIST_OBJECT_PROTO_HPP

#include <ThrustTypes.hpp>

namespace mdx
{
  /**
   * Function prototypes for distributed object library.
   */
  template<typename T1, typename T2> 
  int copyGPU(T1 *src, T2 *dst);

  template<typename T> 
  int allocGPU(T **vec, size_t n);
}
#endif
