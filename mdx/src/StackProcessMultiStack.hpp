//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_MULTI_STACK_HPP
#define STACK_PROCESS_MULTI_STACK_HPP

/**
 * \file StackProcessMultiStack.hpp
 * Contains processes that operate on multiple stacks
 */

#include <Process.hpp>

namespace mdx 
{
  
  ///\addtogroup StackProcess
  ///@{

  /**
   * \class AlignCanvas StackProcessMultiStack.hpp <StackProcessMultiStack.hpp>
   *
   * Align two stacks' canvas. Expand the canvas of the first stack so the
   * second one can be drawn onto it. If the 'change both stack' option is
   * selected, then all the stacks are extended and made aligned with the axis
   * of the reference system. Otherwise, the work store of the current stack is
   * replaced with the projection of the other stack onto the extended canvas,
   * keeping the resolution of the current stack.
   */
  class AlignCanvas : public Process 
  {
  public:
    AlignCanvas(const Process& process) : Process(process) 
    {
      setName("Stack/MultiStack/Align Canvas");
      setDesc("Align two stacks' canvas. Expand the canvas of the first stack so the\n"
              "second one can be drawn onto it. If the 'change both stack' option is \n"
              "selected, then all the stacks are extended and made aligned with the \n"
              "axis of the reference system. Otherwise, the work store of the current\n"
              "stack is replaced with the projection of the other stack onto the\n"
              "extended canvas, keeping the resolution of the current stack.\n");
      setIcon(QIcon(":/images/AlignCanvas.png"));

      addParm("Change Both", "Extend the non-active stacks canvas as well", "Yes", booleanChoice());
    }
  
    bool run()
    {
      Stack* current = currentStack();
      Stack* other = otherStack();
      if(current->currentStore()->labels() != other->currentStore()->labels())
        return setErrorMessage("Error, one stack is labeled and the other is not.");
      bool change_both = stringToBool(parm("Change Both"));
      bool result = run(current, other, change_both);
      if(not change_both and result)
        current->work()->show();
      return result;
    }
    bool run(Stack* target, Stack* other, bool change_both);
  
    bool projectGlobal(Stack* s1, Stack* s2);
    bool projectOnStack(Stack* target, const Stack* to_project);
  };
  
  /**
   * \class CombineStacks StackProcessMultiStack.hpp <StackProcessMultiStack.hpp>
   *
   * Combine the values of the main and work store onto the work store.
   */
  class CombineStacks : public Process 
  {
  public:
    CombineStacks(const Process& process) : Process(process) 
    {
      setName("Stack/MultiStack/Combine Stacks");
      setDesc("Combine the values of the main and work store onto the work store.");
      setIcon(QIcon(":/images/CombineStacks.png"));

      addParm("Method", "The method to combine", "Average", 
         QStringList() << "Max" << "Min" << "Average" << "Product" << "Add" << "Subtract");
    }
  
    bool run()
    {
      Stack* current = currentStack();
      if(current->main()->labels() != current->work()->labels())
        return setErrorMessage("Error, one store is labeled and the other is not.");
      return run(current, parm("Method"));
    }
  
    bool run(Stack* target, QString method);
  };
  
  /**
   * \class MergeStacks StackProcessMultiStack.hpp <StackProcessMultiStack.hpp>
   *
   * Merge the main store of the current stack with the current store of the other one.
   * The current stack will be aligned with the other before the stores being combines.
   * The method argument is simply passed to the Combine_Stacks process.
   */
  class MergeStacks : public Process 
  {
  public:
    MergeStacks(const Process& process) : Process(process) 
    {
      setName("Stack/MultiStack/Merge Stacks");
      setDesc("Merge the main store of the current stack with the current store of the other one.\n"
              "The current stack will be resampled if required.");
      setIcon(QIcon(":/images/MergeStacks.png"));

      addParm("Method", "The method used to merge", "Max", 
                                 QStringList() << "Max" << "Min" << "Average" << "Product");
    }
  
    bool run()
    {
      Stack* current = currentStack();
      Stack* other = otherStack();
      if(current->currentStore() != current->main())
        return setErrorMessage("Error, the current stack must have its main store active.");
      if(current->currentStore()->labels() != other->currentStore()->labels())
        return setErrorMessage("Error, one stack is labeled and the other is not.");
      bool result = run(current, other, parm("Method"));
      if(result)
        current->work()->show();
      return result;
    }
    bool run(Stack* target, const Stack* other, QString method);
  };

  /**
   * \class CopyMainToWork StackProcess.hpp <StackProcess.hpp>
   *
   * Copy the content of the main stack into the work stack
   */
  class mdxBase_EXPORT CopyMainToWork : public Process 
  {
  public:
    CopyMainToWork(const Process& process) : Process(process) 
    {
      setName("Stack/MultiStack/Copy Main to Work Stack");
      setDesc("Copy Main to Work Stack");
      setIcon(QIcon(":/images/CopyMainToWork.png"));
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      bool res = run(stack);
      if(res) {
        stack->main()->hide();
        stack->work()->show();
      }
      return res;
    }
    bool run(Stack* stack);
  };
  
  /**
   * \class CopyWorkToMain StackProcess.hpp <StackProcess.hpp>
   *
   * Copy the content of the work stack into the main stack
   */
  class mdxBase_EXPORT CopyWorkToMain : public Process 
  {
  public:
    CopyWorkToMain(const Process& process) : Process(process) 
    {
      setName("Stack/MultiStack/Copy Work to Main Stack");
      setDesc("Copy Work to Main Stack");
      setIcon(QIcon(":/images/CopyWorkToMain.png"));
    }
  
    bool run()
    {
      Stack* stack = currentStack();
      bool res = run(stack);
      if(res) {
        stack->work()->hide();
        stack->main()->show();
      }
      return res;
    }
    bool run(Stack* stack);
  };

  /**
   * \class SwapStacks StackProcess.hpp <StackProcess.hpp>
   *
   * Swap the main and work stores of a stack
   */
  class mdxBase_EXPORT SwapStacks : public Process 
  {
  public:
    SwapStacks(const Process& process) : Process(process) 
    {
      setName("Stack/MultiStack/Swap Main and Work Stacks");
      setDesc("Swap the main and work data of the current stack.");
      setIcon(QIcon(":/images/SwapStacks.png"));
    }
  
    bool run()
    {
      Stack* s = currentStack();
      if(!s) {
        setErrorMessage("You need to select a stack to launch this process.");
        return false;
      }
      return run(s);
    }
    bool run(Stack* stack);
  };
  
  /**
   * \class CopySwapStacks <StackProcess.hpp>
   *
   * Copy or swap stacks between stack 1 and 2.
   */
  class mdxBase_EXPORT CopySwapStacks : public Process 
  {
  public:
    CopySwapStacks(const Process& process) : Process(process) 
    {
      setName("Stack/MultiStack/Swap or Copy Stack 1 and 2");
      setDesc("Copy or Swap Stack 1 and 2");
      setIcon(QIcon(":/images/CopySwapStacks.png"));

      addParm("Store", "The store to use", "Main", storeChoice());
      addParm("Action", "The direction of the copy or swap", "1 -> 2", 
                      QStringList() << "1 -> 2" << "1 <- 2" << "1 <-> 2");
    }
  
    bool run()
    {
      // Stack *stack = currentStack();
      return run(parm("Store"), parm("Action"));
    }
  
    bool run(const QString &storeStr, const QString &actionStr);
  };
  
  ///@}
}

#endif
