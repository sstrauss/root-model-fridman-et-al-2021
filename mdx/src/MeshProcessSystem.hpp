//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef MESH_PROCESS_SYSTEM_HPP
#define MESH_PROCESS_SYSTEM_HPP

#include <Config.hpp>
#include <Process.hpp>
#include <QFileDialog>
#include <QTreeWidget>

/**
 * \file MeshProcessSystem.hpp
 * This file contains system processes for meshes
 */

class QDialog;
class Ui_LoadStackDialog;
class Ui_LoadMeshDialog;
class Ui_ImportMeshDialog;
class Ui_SaveMeshDialog;
class Ui_ExportMeshDialog;
class Ui_PlyCellGraphDlg;
class Ui_RenameDlg;
class QFile;
class QIODevice;

namespace mdx 
{
  mdxBase_EXPORT QList<int> extractVersion(QIODevice& file);
  bool loadPly(Mesh &mesh, CCStructure &cs, const QString& fileName, bool transform = false, bool add = false);
  
  /**
   * \class MeshLoad SystemProcessLoad.hpp <SystemProcessLoad.hpp>
   *
   * Load a mesh file.
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT MeshLoad : public Process 
  {
    Q_OBJECT
  public:
    MeshLoad(const Process& proc) : Process(proc) 
    {
      setName("Mesh/System/Load");
      setDesc("Load a mesh from one of the known formats.");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "File name of mesh to load", "");
      addParm("Transform", "Transform the mesh on load", "No", booleanChoice());
      addParm("Add", "Reset mesh before load (add to existing)", "No", booleanChoice());
      addParm("Mesh Id", "The id of the mesh to load", "0");
    }
  
    bool run();
    /**
     * Load a MDXM file
     * \param mesh Mesh that will contain the data
     * \param filename File containing the mesh.
     * \param scale If true, the mesh will be scaled according to the current
     * scale factor
     * \param transform If true, the mesh will be transformed according to
     * the current transformation frame
     * \param add If true, the mesh will be added to the current one instead
     * of replacing it.
     */
    bool run(Mesh &mesh, QString filename, bool transform, bool add);

    bool initialize(QWidget* parent);
  
    bool loadMGXM_0(QIODevice& file, Mesh &mesh, bool scale, bool transform, bool has_color = true);
    bool loadMGXM_1_0(QIODevice& file, Mesh &mesh, bool& scale, bool& transform, bool has_color = true);
    bool loadMGXM_1_1(QIODevice& file, Mesh &mesh, bool& scale, bool& transform, bool has_color = true);
    bool loadMGXM_1_2(QIODevice& file, Mesh &mesh, bool& scale, bool& transform, bool has_color = true);
    bool loadMGXM_1_3(QIODevice& file, Mesh &mesh, bool& scale, bool& transform);

    bool loadMDXM_2_x(QIODevice& file, Mesh &mesh, bool& transform);

    /**
      * Read in vertex list
      */
    bool readVertexList(QIODevice &file);

    /**
      * Read in neighborhoods
      */
    bool readNhbds(QIODevice &file);

    /**
      * Read in vertices
      */
    bool readVertices(QIODevice &file, Mesh &mesh, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, CCIndexVec &vertices, bool transform);

    /**
      * Read in neighborhoods
      */
    bool readNhbds(QIODevice &file, std::vector<std::vector<uint> > &neighbors);

    /**
      * Read in a cellular mesh
      */
    bool readCellMesh(QIODevice &file, Mesh &mesh, CCIndexVec &vertices, CCIndexVec &faces, UIntVecVec &faceVertices, 
            CCIndexVec &volumes, IntVecVec &volumeFaces, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, bool transform);
    /**
      * Make triangles from a vertex and neighbor list
      */
    bool makeTriangles(const CCIndexVec &vertices, const UIntVecVec &neighbors, 
                             CCIndexVec &faces, UIntVecVec &faceVertices, CCIndexDataAttr &indexAttr);
    //void findSignalBounds(Mesh &mesh);
  
  protected slots:
    void selectMeshFile();
  
  protected:
    void setMeshFile(const QString& filename);
  
    QDialog* dlg;
    Ui_LoadMeshDialog* ui;
  };
  
  /**
   * \class MeshImport MeshProcessSystem.hpp <MeshProcessSystem.hpp>
   *
   * Import a mesh from a file
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT MeshImport : public Process 
  {
    Q_OBJECT
  
  public:
    MeshImport(const Process& proc) : Process(proc) 
    {
      setName("Mesh/System/Import");
      setDesc("Import a cell complex from a file.");
      setIcon(QIcon(":/images/open.png"));

      addParm("Mesh Id", "Id of mesh to import, empty for current", "");
      addParm("File Name", "File name to import from", "");
      addParm("Cell Complex", "Cell complex name to import into", "Imported");
      addParm("Transform", "Transform the cell complex with the current transformation", "No", booleanChoice());
      addParm("Add", "Do not reset the mesh first", "No", booleanChoice());
    }

    bool run();
    /**
     * Import a cell complex
     * \param name Name of the cell complex
     * \param filename File name containing the cell complex
     */
    bool run(Mesh &mesh, const QString &ccName, const QString &filename, bool transform, bool add);

    bool initialize(QWidget* parent);
  
  protected slots:
    void selectMeshFile();
    void selectMeshType(const QString& type);
  
  protected:
    void setMeshFile(const QString& filename, const QString& type = QString());
    QString properFile(QString filename, const QString& type) const;
  
    bool loadMeshPly(Mesh &mesh, const QString &ccName, const QString &filename, bool transform, bool add);
    bool loadMeshText(Mesh &mesh, const QString& filename, bool scale, bool transform, bool add);
    bool loadMeshCells(Mesh &mesh, const QString& filename, bool scale, bool transform, bool add);
    bool loadMeshKeyence(Mesh &mesh, const QString& filename, bool scale, bool transform, bool add);
    bool loadMeshEdit(Mesh &mesh, const QString& filename, bool scale, bool transform, bool add);
    bool loadMeshVTK(Mesh &mesh, const QString& filename, bool& scale, bool transform, bool add);
    bool loadMeshOBJ(Mesh &mesh, const QString& filename, bool transform);
  
    QString fileType;
    QDialog* dlg;
    Ui_ImportMeshDialog* ui;
  };
  
  /**
   * \class ResetMesh SystemProcessLoad.hpp <SystemProcessLoad.hpp>
   *
   * Reset a mesh
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT ResetMesh : public Process 
  {
  public:
    ResetMesh(const Process& proc) : Process(proc) 
    {
      setName("Mesh/System/Reset");
      setDesc("Reset a mesh.");
      setIcon(QIcon(":/images/ClearStack.png"));

      addParm("Mesh Id", "Mesh to reset, empty for the current mesh", "");
    }
    bool run();
  };

  // Delete selected cells
  class DeleteSelection : public Process
  {
  public:
    DeleteSelection(const Process &process) : Process(process) 
    {
      setName("Mesh/System/Delete Selection");
      setDesc("Delete selection from the mesh");
      setIcon(QIcon(":/images/DeleteLabel.png"));

      addParm("Delete Dangling Cells",
              "Should low-dimensional cells not in the boundary "
              "of higher-dimensional cells be deleted?",
              "Yes", booleanChoice());
      addParm("Dimension", "Dimension of cells to be deleted", "All", allDimChoice());
    }

    // Rewind will set the delete process for the current tissue
    bool rewind(QWidget *parent);
    bool run();
    bool run(CCStructure &cs, CCIndexDataAttr &indexAttr, QString dimension = "All");
  };

  // Delete a cell complex
  class CCDelete : public Process
  {
  public:
    CCDelete(const Process &process) : Process(process) 
    {
      setName("Mesh/System/Delete Cell Complex");
      setDesc("Delete a cell complex");
      setIcon(QIcon(":/images/DeleteLabel.png"));

      addParm("Cell Complex Name", "Name of cell complex to delete", "");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw("No mesh selected");

      QString ccName = parm("Cell Complex Name");
      if(ccName.isEmpty()) {
        ccName = mesh->ccName();
        if(ccName.isEmpty()) 
          throw("No cell complex selected");
      }

      return run(*mesh, ccName);
    }
    bool run(Mesh &mesh, const QString &ccName);
  };

  // Rename a cell complex
  class CCRename : public Process
  {
  public:
    CCRename(const Process &process) : Process(process) 
    {
      setName("Mesh/System/Rename Cell Complex");
      setDesc("Rename a cell complex");
      setIcon(QIcon(":/images/open.png"));

      addParm("Old Name", "Name for new cell complex", "");
      addParm("New Name", "Name for new cell complex", "");
    }

    bool initialize(QWidget *parent);

    bool run()
    {
      return run(*mesh, oldName, newName);
    }
    bool run(Mesh &mesh, const QString &oldName, const QString &newName);

  private:
    Mesh *mesh;
    QString oldName;
    QString newName;

    QDialog* dlg;
    Ui_RenameDlg* ui;
  };


  /**
   * \class MeshSave SystemProcessSave.hpp <SystemProcessSave.hpp>
   *
   * Save a mesh into a INRIA or MDXM file.
   *
   * Description of the MGXM file format
   * ===================================
   * MGXM is a binary file format to store triangular meshes with all the data and meta-data used by MorphoDynamX.
   *
   * There are a few versions, from 1.0 to 1.2. This version of MorphoDynamX will always generate MGXM files version
   * 1.2.
   *
   * Each MGXM file starts with the ASCII string `MGXM` followed by a space, the version number in ASCII and another
   * space. For instance, for the current version, the file starts with the ASCII string `"MGXM 1.2 "`.
   *
   * A binary header follows the version number. The header is made of:
   *
   *   Field       | Size (bytes) | type  | Version | Description
   * --------------|--------------|-------|---------|-------------------------------------------------------------
   *  is_cells     |      1       | bool  |  1.2    | If true, the mesh represent 2D cells (see notes)
   *  scale        |      1       | bool  |  1.1    | If true, the mesh has been scaled when saved
   *  transform    |      1       | bool  |  1.1    | If true, the mesh has been transformed rigidly
   *  signalLow    |      4       | float |  1.1    | Lower value of the signal
   *  signalHigh   |      4       | float |  1.1    | Upper value of the signal
   *  signalUnit   |      *       | utf8  |  1.1    | Text of the signal unit (see notes)
   *  header_size  |      4       | uint  |  1.0    | Specified the size in bytes of extra data placed after the header
   *  vertex_cnt   |      4       | uint  |  1.0    | Number of vertices in the mesh
   *  vertex_size  |      4       | uint  |  1.0    | Size in bytes of the structure describing a vertex, minus 12.
   *  edge_size    |      4       | uint  |  1.0    | Size in bytes of the structure describing an edge.
   *
   * The description of the vertices follows the header. For each vertex, a structure of size `vertex_size+12` is
   * written. Only fields fitting in the given size are actually written, and any other data is ignored. Note that the
   * first four field are always there!
   *
   *   Field       | Size (bytes) | type  |  Description
   * --------------|--------------|-------|----------------------------------------------------------------------
   *   Id          |      4       | uint  | Id of the vertex, used to reference it in other places in the file
   *   X           |      4       | float | X position of the vertex
   *   Y           |      4       | float | Y position of the vertex
   *   Z           |      4       | float | Z position of the vertex
   *   label       |      4       | uint  | Label of the vertex
   *   color       |      4       | float | "Color" of the vertex (e.g. like signal, but scaled from 0 to 1)
   *   signal      |      4       | float | Signal of the vertex
   *   type        |      1       | char  | type of vertex `'c'` for center and `'j'` for junction.
   *   selected    |      1       | bool  | If true, the vertex is selected
   *
   * Currently, the edges have no defined fields. For each vertex, the `Id` of the vertex is written, followed by a 32
   * bits unsigned integer for the number of neighbors, and the list of `Id` for each neighbor. Neighbors of a vertex
   * are always written counter-clockwise.
   *
   * Notes on UTF-8 strings
   * ----------------------
   *
   * UTF-8 strings are written as: their size in bytes as a 32 bits unsigned integer, followed by the bytes themselves.
   *
   * Notes on vertex labels
   * ----------------------
   *
   * Vertex are labels to mark which cells they are part of. Boundary vertices are labeled with -1 or -2. -1 mark
   * vertices between cells, and -2 vertices at the border of the whole mesh.
   *
   * Notes on cell mesh
   * ------------------
   *
   * If the mesh is a cell mesh, then each cell has a unique labeled vertex on its center, surrounded by cell-boundary
   * vertices, labels -1 or -2. In this case, labeled vertices have type `'c'` and boundary vertices `'j'`.
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT MeshSave : public Process {
    Q_OBJECT
  public:
    MeshSave(const Process& proc) : Process(proc) 
    {
      setName("Mesh/System/Save");
      setDesc("Save a mesh into a known mesh format");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "File name to save the mesh", "");
      addParm("Transform", "Transform the mesh points", "No", booleanChoice());
      addParm("Mesh Id", "Id of mesh to save, empty for current", "");
    }

    bool run();
    /**
     * Save a mesh
     * \param mesh Mesh to be saved
     * \param fileName File to save the mesh in. Its extension must be either .inr or .mgmx
     * \param transform Save the transformed positions
     */
    bool run(Mesh* mesh, const QString& fileName, bool transform);
    bool initialize(QWidget* parent);

  protected slots:
    void selectMeshFile();

  protected:
    QString properFile(QString filename) const;
    void setMeshFile(const QString& filename);
    Point3d savedPos(Point3d pos, bool transform, const Stack* stack);

    bool saveMesh(Mesh* mesh, const QString& filename, bool transform);

    QDialog* dlg;
    Ui_SaveMeshDialog* ui;
  };
  
  class mdxBase_EXPORT MeshSaveExtendedPly : public Process 
  {
    Q_OBJECT
  public:
    MeshSaveExtendedPly(const Process& proc) : Process(proc) 
    {
      setName("Mesh/System/Save Extended PLY");
      setDesc("Save a mesh into a known mesh format");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "File name to save.", ""); 
    }

    bool run();
    /**
     * Save a mesh
     * \param mesh Mesh to be saved
     * \param filename File to save the mesh in. Its extension must be either .inr or .mgmx
     * \[aram transform Save the transformed positions
     */
    bool run(Mesh* mesh, const QString& filename);
  };

  /**
   * \class MeshExport MeshProcessSystem.hpp <MeshProcessSystem.hpp>
   *
   * Export a mesh on various formats: \ref PLY, \ref VTK, \ref text, \ref cells, \ref MeshEdit, \ref STL or \ref OBJ.
   *
   * We will here describe the file formats, or at least how MorphoDynamX uses them.
   *
   * \section PLY PLY format
   *
   * The PLY format is fully described on the website of the Paul Bourke: http://paulbourke.net/dataformats/ply/
   *
   * It is also available on Wikipedia: https://en.wikipedia.org/wiki/PLY_(file_format)
   *
   * MorphoDynamX exports only the two kind of elements described as mandatory: vertex and face.
   *
   * In the case of a cell mesh, each vertex contains only its position (x,y,z). Otherwise, vertices also have their
   * label.
   *
   * In any case, each face contains the list of 0-based index into the vertex elements that form the face, and the
   * label marking which cell the face is a part of. Vertices are oriented counter-clockwise.
   *
   * \section VTK VTK format
   *
   * MorphoDynamX save VTU format, which are XML files for unstructure grids.
   *
   * The file format is quite complex, and fully described in the VTK book and website.
   *
   * MorphoDynamX stores the following properties in addition to the mandatory ones:
   *
   * For each vertex: 'Signal' (float32), 'Normals' (3 floats32), 'Color' (float32) and 'Label' (int32)
   *
   * For each "cell" (e.g. triangles or 2D cells): 'Label' (int32)
   *
   * MorphoDynamX also adds a DataArray to store the heat per VTU "cell". The name is either "Cell Heat" or "Wall Heat".
   *
   * At last, an extra DataArray is created, store the heat per label instead of per cell.
   *
   * \section text "text" format
   *
   * The text format is a trivial ASCII file format. The file contains:
   *
   *     n
   *     vertex 0
   *     vertex 1
   *     ...
   *     vertex n-1
   *     neighbors 0
   *     neighbors 1
   *     ...
   *     neighbors n-1
   *
   * Each vertex is described as:
   *
   *     id x y z label
   *
   * Each neighborhood is described as:
   *
   *     id m id_1 id_2 ... id_m
   *
   * Where `id` is the id of the current vertex, `m` the number of neighbors, and `id_1`, `id_2` are vertex ids as
   * described before. The vertex must be ordered counter-clockwise.
   *
   * \section cells "cells" format
   *
   * This format is very similar to the "text" format. Only each vertex is described as:
   *
   *     id x z y label type
   *
   * Where `type` is `j` for junctions and `c` for cell centers.
   *
   * \section MeshEdit MeshEdit format
   *
   * This file format is the one used by the MeshEdit software. In MorphoDynamX, the file has the following format
   *
   *     MeshVersionFormatted 1
   *     Dimension 3
   *     Vertices
   *     vertex 1
   *     vertex 2
   *     ...
   *     vertex n
   *     Triangles
   *     triangle 1
   *     triangle 2
   *     ...
   *     triangle m
   *     End
   *
   * Each vertex is described as:
   *
   *     x y z label
   *
   * And each triangle is described as:
   *
   *     v0 v1 v2 label
   *
   * Again, the vertices must be ordered counter-clockwise.
   *
   * \section STL STL format
   *
   * The STL file format is the one used in many CAD software, including Abaqus.
   *
   * In MorphoDynamX, the mesh is stored as a single solid called "mdx". The file structure is:
   *
   *     solid mdx
   *     facet 1
   *     facet 2
   *     ...
   *     facet n
   *     endsolid mdx
   *
   * Where each facet is described as:
   *
   *     facet normal nx ny nz
   *       outer loop
   *         vertex vx1 vy1 vz1
   *         vertex vx2 vy2 vz2
   *         vertex vx3 vy3 vz3
   *       endloop
   *     endfacet
   *
   * \section OBJ OBJ format
   *
   * MorphoDynamX can also export files in the WaveFront's OBJ file format.
   *
   * The file has the following structure:
   *
   *     # Triangular mesh created by MorphoDynamX
   *     # Length unit: µm
   *     # Vertices
   *     vertex 1
   *     vertex 2
   *     ...
   *     vertex n
   *     # Triangles
   *     triangle 1
   *     triangle 2
   *     ...
   *     triangle m
   *
   * Each vertex is stored as:
   *
   *     v x y z
   *     vn nx ny nz
   *
   * And each triangle as:
   *
   *     f v1 v2 v3
   *
   * where `v1`, `v2` and `v3` are 1-based index in the vertex array. Also, the vertices are oriented counter-clockwise.
   *
   * \ingroup MeshProcess
   */

  bool savePLYCellGraph(Mesh* mesh, const QString& filename, bool binary);

  class mdxBase_EXPORT MeshExport : public Process 
  {
    Q_OBJECT
  public:
    MeshExport(const Process& proc) : Process(proc) 
    {
      setName("Mesh/System/Export");
      setDesc("Export a mesh into a known mesh format");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "File name to save", "");
      addParm("File Type", "The format of file to save", "Text", QStringList() << "PLY Binary" << "PLY Ascii" 
        << "VTK Mesh Binary" << "VTK Mesh Ascii" << "Text" << "Cells" << "MeshEdit" << "STL" << "OBJ");
      addParm("Transform", "Transform the mesh coordinates", "No", booleanChoice());
      addParm("Mesh Id", "The mesh id, empty for current", "");
    }

    bool run();
    /**
     * Export a mesh
     * \param mesh Mesh to be exported
     * \param filename File to save the mesh into
     * \param type Type (format) of the export.
     * \param transform Save the transformed positions
     */
    bool run(Mesh* mesh, const QString& filename, const QString& type, bool transform);
    bool initialize(QWidget* parent);

    bool savePLY(Mesh &mesh, const CCStructure &cs, const QString& filename, bool transform, bool binary = false); 

  protected slots:
    void selectMeshFile();
    void selectMeshType(const QString& type);

  protected:
    QString properFile(QString filename, const QString& type) const;
    void setMeshFile(const QString& filename);
    Point3d savedPos(Point3d pos, bool transform, const Stack* stack);

    bool saveText(Mesh* mesh, const QString& filename, bool transform);
    bool saveCells(Mesh* mesh, const QString& filename, bool transform);
    bool saveMeshEdit(Mesh* mesh, const QString& filename, bool transform);
    bool saveMeshSTL(Mesh* mesh, const QString& filename, bool transform);
    bool saveVTU(Mesh* mesh, const QString& filename, bool transform, bool binary = false);
    bool saveOBJ(Mesh* mesh, const QString& filename, bool transform);

    QDialog* dlg;
    Ui_ExportMeshDialog* ui;
  };
  
  struct PlyCellData{

    QString name, type;

    std::map<int, int> intData;
    std::map<int, float> floatData;
    std::map<int, Point3f> pointData;
    std::map<int, SymmetricTensor> tensorData;

  };

  struct PlyWallData
  {
    QString name, type;
    std::map<std::pair<int, int>, int> intData;
    std::map<std::pair<int, int>, float> floatData;
    std::map<std::pair<int, int>, Point3f> pointData;
    std::map<std::pair<int, int>, SymmetricTensor> tensorData;
  };

  void createPlyDataFromAttr(Mesh* m, QStringList attrMapsToBeSaved, 
                 std::vector<PlyCellData>& cellVec, std::vector<PlyWallData>& wallVec);
  void fillTreeWidgetWithAttrMaps(Mesh* m, QTreeWidget* tree);

  class SavePlyFile : public Process
  {
    Q_OBJECT
  public:
    QStringList attrMapsToBeSaved;

    SavePlyFile(const Process& process) : Process(process) 
    {
      setName("Mesh/System/Save Cell Graph Ply File");
      // RSS There should be a separate addon or plugin for Marion
      setDesc("Save a standard PLY file and a cell graph PLY file with specified attributes.\n"
        "If Marion's file is selected than it will create a file with a fixed set of attributes.");
      setIcon(QIcon(":/images/MakeHeatMap.png"));
      
      addParm("File Name", "Name of file to save", "");
      addParm("Marion's File", "Write file in format defined with Marion", "No", booleanChoice());
    }
    bool initialize(QWidget* parent);
    bool run();
    bool run(Mesh* mesh, QString filename, bool marionsFile);

  public slots:
    void selectAll(Ui_PlyCellGraphDlg &ui);
    void unselectAll(Ui_PlyCellGraphDlg &ui);
  };

  class LoadPlyFileCellGraph : public Process
  {
  public:
    LoadPlyFileCellGraph(const Process& process) : Process(process) 
    {
      setName("Mesh/System/ToDo Soeren/Load Cell Graph Ply File");
      setDesc("Loads Cell Graph data from a file and creates Attribute Maps from the data.");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("File Name", "File name to load", "");
    }
    bool initialize(QWidget* parent);
    bool run();
    bool run(Mesh* mesh, const QString& filename);
  };
}
#endif

