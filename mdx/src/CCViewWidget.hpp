//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef CCVIEWWIDGET_HPP
#define CCVIEWWIDGET_HPP

#include <ui_CCViewTemplate.h>
#include <CCRenderGroupWidget.hpp>
#include <Mesh.hpp>

#include <QStringList>
#include <QAbstractListModel>

namespace mdx 
{
  class ImgData;

  class CCComplexListModel : public QAbstractListModel
  {
  private:
    Mesh *mesh;
  
  public:
    QStringList ccNames;

    CCComplexListModel(QObject *_parent, Mesh *_mesh);
    QVariant data(const QModelIndex &_index, int _role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &_index) const;
  
    int rowCount(const QModelIndex &_parent) const;
  
    void refreshNames(void);
  };

  class CCViewWidget : public QWidget, protected Ui::CCViewWidget
  {
    Q_OBJECT
  
  public:
    QWidget *parent;
    ImgData *imgData;
    CCComplexListModel *ccModel;
    Mesh *mesh;
    QModelIndex selected;

    std::map<QString,CCRenderGroupWidget*> renderGroups;
  
    CCViewWidget(QWidget *_parent);
    ~CCViewWidget();
  
    void initialize(ImgData *_imgData,Mesh *_mesh);

    void selectName(const QString &name);


  private:
    CCRenderGroupWidget *addRenderGroup(const QString &rgName);

  private slots:
    void resetGroups(CCDrawParms *cdp = NULL);
    void enableGroups(bool);

    void renderChoiceChanged(const QString &);
    void renderGroupToggled(bool);
    void colorMapClicked();
    void colorMapUpdate();

    void complexSelectionChanged(const QModelIndex &_index);
    void refreshNames(const QString &name);

    void onCustomContextMenuRequested(const QPoint& pos);

  signals:
    void execProcess(QString, QStringList);
    void updateCCName();
  };
}
#endif
