//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef DIST_OBJECT_CUDA_HPP
#define DIST_OBJECT_CUDA_HPP

#include <Cuda.hpp>
#include <ThrustTypes.hpp>

// Public Cuda interface methods
namespace mdx
{
  template<typename T1, typename T2> 
  int copyGPU(T1 *src, T2 *dst)
  {
    try {
      if(!src or !dst)
        return 1;
#ifdef DEBUG_THRUST
      printf("copyGPU sizes src dst %ld %ld %ld %ld\n", src->size(), dst->size(), src, dst);
#endif
      *dst = *src;
    } catch (const std::exception &exc) {
      std::cout << "copyGPU Thrust error:" << exc.what() << std::endl;
      throw;
    } catch (...) {
      printf("copyGPU failed src dst %ld %ld\n", src->size(), dst->size());
      std::cout << "copyGPU Thrust unknown exception" << std::endl;
      throw;
    }
    return 0;
  }

  template<typename T> 
  int allocGPU(T **vec, size_t n)
  {
    try {
      if(n == 0) {
        if(*vec) {
#ifdef DEBUG_THRUST
          printf("Delete vec %ld\n", *vec);
#endif
          delete *vec;
          *vec = 0;
        }
        return 1;
      } else {
#ifdef DEBUG_THRUST
        printf("allocGPU size %ld memavail: %ld\n", n, memAvail());
#endif
        if(*vec == 0) {
          *vec = new T;
#ifdef DEBUG_THRUST
          printf("New vec: %ld\n", *vec);
#endif
        }
        (*vec)->resize(n);
      }
    } catch (const std::exception &exc) {
      std::cout << "allocGPU Thrust error:" << exc.what() << std::endl;
      throw;
    } catch (...) {
      printf("allocGPU failed size %ld\n", n);
      std::cout << "allocGPU Thrust unknown exception" << std::endl;
      throw;
    }
    return 0;
  }
} 
#endif
