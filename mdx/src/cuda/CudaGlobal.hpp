//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef CUDA_GLOBAL_H
#define CUDA_GLOBAL_H

/*
 * \file CudaGlobal.hpp
 *
 * Defines global items for cuda
 */
#ifdef __CUDACC__
  #define CU_HOST_DEVICE __host__ __device__ inline
#else
  #define CU_HOST_DEVICE inline
#endif

#ifdef _WIN32
  #ifdef THRUST_BACKEND_CUDA
    #ifdef cuda_EXPORTS
      #define cuda_EXPORT extern "C" __declspec(dllexport)
    #else
      #define cuda_EXPORT extern "C" __declspec(dllimport)
    #endif
  #else
    #ifdef mdx_EXPORTS
      #define cuda_EXPORT extern "C" __declspec(dllexport)
    #else
      #define cuda_EXPORT extern "C" __declspec(dllimport)
    #endif
  #endif
#else
  #define cuda_EXPORT
#endif

#define CU_EPSILON 1e-10f

#endif
