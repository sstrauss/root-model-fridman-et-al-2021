//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#ifndef TOOLS_PROCESS_BEZIER_HPP
#define TOOLS_PROCESS_BEZIER_HPP

#include <Process.hpp>
#include <Parms.hpp>
#include <Bezier.hpp> 

namespace mdx 
{
  /**
   * \class LoadBezier ToolsProcessBezier.hpp <ToolsProcessBezier.hpp>
   */
  class mdxBase_EXPORT LoadBezier : public Process {
  public:
    LoadBezier(const Process& process) : Process(process) 
    {
      setName("Tools/Bezier/Load Bezier");
      setDesc("Load Bezier from a parameter file.");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "Name of file to load", "");
    }
    bool initialize(QWidget* parent);
  
    bool run()
    {
      return run(parm("File Name"));
    }
    bool run(const QString &fileName);
  };

  /**
   * \class SaveBezier ToolsProcessBezier.hpp <ToolsProcessBezier.hpp>
   */
  class mdxBase_EXPORT SaveBezier : public Process {
  public:
    SaveBezier(const Process& process) : Process(process) 
    {
      setName("Tools/Bezier/Save Bezier");
      setDesc("Save Bezier to a parameter file.");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "Name of save file", "");
    }
    bool initialize(QWidget* parent);
  
    bool run()
    {
      return run(parm("File Name"));
    }
    bool run(const QString& filename);
  };


	 /**
   * \class NewBezier ToolsProcessBezier.hpp <ToolsProcessBezier.hpp>
   */
  class mdxBase_EXPORT NewBezier : public Process 
  {
  public:
    NewBezier(const Process& process) : Process(process) 
    {
      setName("Tools/Bezier/New Bezier");
      setDesc("New Bezier from parameters.");
      setIcon(QIcon(":/images/Bezier.png"));

			addParm("Controls X", "Nb of control points in X", "5");
			addParm("Controls Y", "Nb of control points in Y", "5");
			addParm("Size X", "Physical size along X (um)", "200");
			addParm("Size Y", "Physical size along Y (um)", "200");
			addParm("Lines Y", "Nb of lines to draw in Y", "15");
			addParm("Lines X", "Nb of lines to draw in X", "15");
    }
    bool run(const QStringList &parms) 
    {
      return run(parm("Controls X").toInt(), parm("Controls Y").toInt(), parm("Size X").toFloat(), 
                              parm("Size Y").toFloat(), parm("Lines Y").toInt(), parm("Lines X").toInt());
    }
    bool run(uint bezPointsX, uint bezPointsY, float sizeX, float sizeY, uint linesX, uint linesY);
  };
}

#endif

