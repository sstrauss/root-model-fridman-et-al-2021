//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#include <ToolsProcessBezier.hpp>
#include <Dir.hpp>
#include <QFileDialog>

namespace mdx
{
  bool LoadBezier::initialize(QWidget* parent)
  {
    QString fileName = parm("File Name");
    if(fileName.isEmpty())
      fileName = QFileDialog::getOpenFileName(parent, "Choose Bezier to load", QDir::currentPath(),
                                              "Bezier file (*.bez);;All files (*.*)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".bez", Qt::CaseInsensitive))
      fileName += ".bez";
    setParm("File Name", stripCurrentDir(fileName));
    return true;
  }
  
  bool LoadBezier::run(const QString &fileName)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
      setErrorMessage(QString("File '%1' cannot be opened for reading").arg(fileName));
      return false;
    }

    CuttingSurface* cs = cuttingSurface();
    cs->setMode(CuttingSurface::BEZIER);
    cs->bezier().loadBezier(fileName);
    cs->showGrid();
    
    setStatus(QString("Loaded Bezier file: %1").arg(fileName));
    return true;
  }
  REGISTER_PROCESS(LoadBezier);

  bool SaveBezier::initialize(QWidget* parent)
  {
    QString fileName = parm("File Name");
    if(fileName.isEmpty())
      fileName = QFileDialog::getSaveFileName(parent, "Choose Bezier file to save", fileName, "Bezier files (*.bez)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".bez", Qt::CaseInsensitive))
      fileName += ".bez";
    setParm("File Name", stripCurrentDir(fileName));
    return true;
  }
  
  bool SaveBezier::run(const QString& fileName)
  {
    QFile file(fileName);
    if(fileName.isEmpty()) {
      setErrorMessage(QString("Unable to write to output file"));
      return false;
    }
    file.setFileName(fileName);
    if(!file.open(QIODevice::WriteOnly)) {
      setErrorMessage(QString("Unable to write to output file"));
      return false;
    }

    CuttingSurface* cs = cuttingSurface();
    cs->bezier().saveBezier(fileName);
    
    setStatus(QString("Saved Bezier file: %1").arg(fileName));
    return true;
  }
  REGISTER_PROCESS(SaveBezier);

  bool NewBezier::run(uint bezPointsX, uint bezPointsY, float sizeX, float sizeY, uint linesX, uint linesY)
  {
    CuttingSurface* cs = cuttingSurface();// NB: this does not call the default constructor, just returns the pointer to existing cutting surface.
    cs->setMode(CuttingSurface::BEZIER);
    cs->bezier().setBezPoints(Point2u(bezPointsX, bezPointsY));
    cs->bezier().setBezSize(Point2d(sizeX, sizeY));
    cs->bezier().setBezLines(Point2u(linesX, linesY));
    cs->bezier().initBez();
    cs->showGrid();

    return true;
  }
  REGISTER_PROCESS(NewBezier);
}
