// Fragment shader that uses stack texture for color.
//
//uniform float opacity;
varying vec3 texCoord;
/*
 *varying vec3 normal;
 *varying vec3 lightDir[4], halfVector[4];
 */

void setColor()
{
  if((texCoord.x < 0.0) || (texCoord.x > 1.0) ||
     (texCoord.y < 0.0) || (texCoord.y > 1.0) ||
     (texCoord.z < 0.0) || (texCoord.z > 1.0))
  {
    discard;
    return;
  }
  vec4 col = color(texCoord);
  col.a = 1-col.a;
  col = light(col);
  gl_FragColor.rgb = col.rgb;
  gl_FragColor.a = opacity*col.a;
}
