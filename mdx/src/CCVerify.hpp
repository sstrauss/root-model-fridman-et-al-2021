//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_VERIFY_HPP
#define CC_VERIFY_HPP

#include <Config.hpp>
#include <CCF.hpp>
#include <Types.hpp>
#include <Attributes.hpp>

namespace mdx
{
  bool mdx_EXPORT verifyCCStructure(const CCStructure &cs, CCIndexDataAttr &indexAttr);
}
#endif

