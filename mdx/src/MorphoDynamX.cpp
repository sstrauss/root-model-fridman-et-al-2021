//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "CudaExport.hpp"
#include "Version.hpp"

#ifndef WIN32
#  include <dlfcn.h>
#endif

#include <QPointer>
#include <QMessageBox>
#include <QToolButton>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QUrl>
#include <QList>
#include <QFileInfo>
#include <QSettings>
#include <QPushButton>
#include <QMutexLocker>
#include <QFileSystemWatcher>
#include <QTimer>
#include <QDesktopServices>
#include <QFileDialog>
#include <QInputDialog>
#include <Progress.hpp>
#include <QShortcut>

#include <Process.hpp>
#include <PrivateProcess.hpp>

#include <Colors.hpp>
#include <ColorEditorDlg.hpp>
#include <LabelEditorDlg.hpp>
#include <MorphoDynamX.hpp>
#include <Forall.hpp>
#include <ProcessParmsModel.hpp>
#include <ProcessThread.hpp>
#include <ProcessDocsDlg.hpp>
#include <Dir.hpp>
#include <DebugDlg.hpp>
#include <Library.hpp>
#include <Geometry.hpp>
#include <MeshUtils.hpp>
#include <About.hpp>
#include <Information.hpp>
#include <CCViewWidget.hpp>
#include <CCUtils.hpp>
#include <VisFlags.hpp>

#include <memory>
#include <typeinfo>

#include <omp.h>
#include <sched.h>

#ifdef WIN32
// ensure nvidia recognises this is a 3D program
__declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
#endif

using namespace mdx;

enum TasksTreeRole { TaskNameRole = Qt::UserRole, ProcessPositionRole = Qt::UserRole + 1 };

QString MorphoDynamX::AppDir = QString();
QLabel* MorphoDynamX::ActiveStack;

MorphoDynamX::~MorphoDynamX() {}

MorphoDynamX::MorphoDynamX(QString appdir, QStringList addLibraries)
  : QMainWindow(), stack1(0, this), stack2(1, this), needSaving(false),
    currentProcess(0), currentSetup(0), processThread(0)
{
  setAcceptDrops(true);
  mdxInfo << introText() << endl;
  AppDir = appdir;
  ui.setupUi(this);

  createSetupProcess();

  stack1.init(currentSetup->getStack(0), currentSetup->getMesh(0));
  stack2.init(currentSetup->getStack(1), currentSetup->getMesh(1));

  // Setup widgets for cell complexes 
  ui.Stack1CCViewWidget->initialize(&stack1,stack1.mesh);
  ui.Stack2CCViewWidget->initialize(&stack2,stack2.mesh);

  std::function<void(QString,bool)> _setStatus =
    [this](QString text, bool alsoPrint) { this->setStatus(text,alsoPrint); };
  Information::init(_setStatus);
  connect(this,SIGNAL(updateStatusBar(const QString&)),
          statusBar(),SLOT(showMessage(const QString&)),
          Qt::QueuedConnection);

  // EditParms dialog
  editParmsDlog = new QDialog(this);
  editParmsDialog = new Ui_EditParmsDialog;
  editParmsDialog->setupUi(editParmsDlog);
  QPushButton* saveAs = editParmsDialog->OKCancel->addButton("Save As", 
                                                  QDialogButtonBox::ActionRole);
  saveAs->setText("Save As");
  connect(saveAs, SIGNAL(clicked()), this, SLOT(editParmsSaveAs()));

  // Connect control In slots
  connectControlsIn();

  parmsModel = new ProcessParmsModel(this);
  ui.ProcessParameters->setModel(parmsModel);
  ui.ProcessParameters->setItemDelegateForColumn(1, new FreeFloatDelegate(this));
  ui.ProcessParameters->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
  ui.ProcessParameters->resizeColumnToContents(0);
  connect(parmsModel, SIGNAL(valuesChanged()), this, SLOT(storeParameters()));

  // Add popups
  resetParametersAct = new QAction("Reset to default", this);
  connect(resetParametersAct, SIGNAL(triggered()), this, SLOT(resetDefaultParameters()));
  ui.ProcessParameters->addAction(resetParametersAct);

  editTasksAct = new QAction("Edit tasks", this);
  connect(editTasksAct, SIGNAL(triggered()), this, SLOT(editTasks()));
  ui.ProcessTasksCommand->addAction(editTasksAct);

  copyProcessNameAct = new QAction("Copy process name", this);
  connect(copyProcessNameAct, SIGNAL(triggered()), this, SLOT(copyProcessName()));
  ui.ProcessStackCommand->setContextMenuPolicy(Qt::ActionsContextMenu);
  ui.ProcessStackCommand->addAction(copyProcessNameAct);
  ui.ProcessMeshCommand->setContextMenuPolicy(Qt::ActionsContextMenu);
  ui.ProcessMeshCommand->addAction(copyProcessNameAct); 
  ui.ProcessToolsCommand->setContextMenuPolicy(Qt::ActionsContextMenu);
  ui.ProcessToolsCommand->addAction(copyProcessNameAct);
  ui.ProcessModelCommand->setContextMenuPolicy(Qt::ActionsContextMenu);
  ui.ProcessModelCommand->addAction(copyProcessNameAct);

  ui.ProcessTasksCommand->addAction(copyProcessNameAct);

  renameStack1SurfSignalAct = new QAction("Rename", this);
  deleteStack1SurfSignalAct = new QAction("Delete", this);
  connect(renameStack1SurfSignalAct, SIGNAL(triggered()), this, SLOT(renameStack1SurfSignal()));
  connect(deleteStack1SurfSignalAct, SIGNAL(triggered()), this, SLOT(deleteStack1SurfSignal()));
  ui.Stack1SurfSignal->setContextMenuPolicy(Qt::ActionsContextMenu);
  ui.Stack1SurfSignal->addAction(renameStack1SurfSignalAct);
  ui.Stack1SurfSignal->addAction(deleteStack1SurfSignalAct);

  renameStack2SurfSignalAct = new QAction("Rename", this);
  deleteStack2SurfSignalAct = new QAction("Delete", this);
  connect(renameStack2SurfSignalAct, SIGNAL(triggered()), this, SLOT(renameStack2SurfSignal()));
  connect(deleteStack2SurfSignalAct, SIGNAL(triggered()), this, SLOT(deleteStack2SurfSignal()));
  ui.Stack2SurfSignal->setContextMenuPolicy(Qt::ActionsContextMenu);
  ui.Stack2SurfSignal->addAction(renameStack2SurfSignalAct);
  ui.Stack2SurfSignal->addAction(deleteStack2SurfSignalAct);

  // Create system processes
  // loadSystemProcesses();

  // First, make sure the factory lists is created here
  processFactories();

#ifdef WATCH_PROCESS_FOLDERS
  // Keep an eye on the process directory
  QString process = processesDir().absolutePath();
  processWatcher = new QFileSystemWatcher(this);
  processWatcher->addPath(process);
  processReloadTimer = new QTimer(this);
  processReloadTimer->setSingleShot(true);
  processReloadTimer->setInterval(100);
  connect(processWatcher, SIGNAL(directoryChanged(const QString &)), processReloadTimer, SLOT(start()));
  connect(processWatcher, SIGNAL(fileChanged(const QString &)), processReloadTimer, SLOT(start()));
  connect(processReloadTimer, SIGNAL(timeout()), this, SLOT(reloadProcesses()));
#endif

  // Initialize cuda
  initGPU();

  // Set affinity
  #ifdef WIN32
    omp_set_dynamic(1);
  #else
//    cpu_set_t my_set;
//    CPU_ZERO(&my_set);
//    int numCPUs = sysconf(_SC_NPROCESSORS_ONLN);   // Linux only?
//    for(int i = 0; i < numCPUs; i++)
//      CPU_SET(i, &my_set);     /* set the bit that represents core i. */
//    sched_setaffinity(0, sizeof(cpu_set_t), &my_set);
//
//    // Ask OpenMP to use all processors
//    omp_set_dynamic(1);
//    omp_set_num_threads(numCPUs);
    omp_set_num_threads(1);
  #endif

  mdxInfo << "OpenMP Processors: " << omp_get_num_procs() << endl;
  mdxInfo << "OpenMP Max threads: " << omp_get_max_threads() << endl << endl;

  // Added process libraries
  {
    QList<QDir> dirs = mdx::processDirs();
    for(int i = 0 ; i < addLibraries.size() ; i++) {
      QFileInfo qf(addLibraries.at(i));
      if(addedLibFiles.contains(qf)) 
        continue;
      else if(!(qf.exists())) {
        mdxInfo << "Library file " << addLibraries.at(i) << " does not exist." << endl;
      } else {
        for(int d = 0 ; d < dirs.size() ; d++) {
        if(qf.canonicalPath() == dirs.at(d).canonicalPath()) {
          mdxInfo << "Library file " << addLibraries.at(i)
                 << " is already included in process directory "
                 << dirs.at(d).path() << endl;
          goto libFileAlreadyIncluded;
        }
      }
      addedLibFiles << qf;
libFileAlreadyIncluded:
      continue;
      }
    }

    if(addedLibFiles.size() > 0) {
      mdxInfo << "Libraries added from command line:";
      for(int i = 0 ; i < addedLibFiles.size() ; i++)
        mdxInfo << " " << addedLibFiles.at(i).filePath();
      mdxInfo << endl << endl;
    }
  }

  reloadProcesses();
  saveProcessesParameters();

  // Restore geometry and settings
  {
    QSettings settings;
    settings.beginGroup("MainWindow");
    restoreGeometry(settings.value("Geometry").toByteArray());
    restoreState(settings.value("WindowState").toByteArray(), 1);
    settings.endGroup();
  }

  // Set up ActiveStack widget;
  ActiveStack = new QLabel;
  ui.statusBar->addPermanentWidget(ActiveStack);
  ActiveStack->setText("Main Stack 1 Active");

  // Load data into controls (before connecting)
  loadControls();

  // Connect control In slots
  connectControlsOut();

  // connect the different controls
  connect(&stack1, SIGNAL(changeSize(const Point3u &, const Point3d &, const Point3d &)), this,
          SLOT(changeStack1Size(const Point3u &, const Point3d &, const Point3d &)));
  connect(&stack2, SIGNAL(changeSize(const Point3u &, const Point3d &, const Point3d &)), this,
          SLOT(changeStack2Size(const Point3u &, const Point3d &, const Point3d &)));
  connect(&stack1, SIGNAL(stackUnloaded()), this, SLOT(stack1Unloaded()));
  connect(&stack2, SIGNAL(stackUnloaded()), this, SLOT(stack2Unloaded()));

  connect(ui.Viewer, SIGNAL(modified()), this, SLOT(modified()));

  stack1.initControls(ui.Viewer);
  stack2.initControls(ui.Viewer);

  connect(&stack1, SIGNAL(changedInterface()), this, SLOT(modified()));
  connect(&stack2, SIGNAL(changedInterface()), this, SLOT(modified()));

  // send stack and plane pointers to Viewer
  ui.Viewer->stack1 = &stack1;
  ui.Viewer->stack2 = &stack2;
  ui.Viewer->cutSurf = &cutSurf;

  // Start with clipping plane as manipulated frame
  // ui.Clip1Rotate->setChecked(true);
  rotate(true);

  // Connect debug dialog
  connect(ui.actionDebug_Dialog, SIGNAL(triggered()), this, SLOT(debugDialog()));

  // Setup the shortcuts
  QAction *altQ = new QAction(this);
  altQ->setShortcut(Qt::Key_Q | Qt::ALT);

  QAction *altW = new QAction(this);
  altW->setShortcut(Qt::Key_W | Qt::ALT);

  QAction *altE = new QAction(this);
  altE->setShortcut(Qt::Key_E | Qt::ALT);

  QAction *altD = new QAction(this);
  altD->setShortcut(Qt::Key_D | Qt::ALT);

  QAction *altS = new QAction(this);
  altS->setShortcut(Qt::Key_S | Qt::ALT);


  QAction *ctrlAltZ = new QAction(this);
  ctrlAltZ->setShortcut(Qt::Key_Z | Qt::ALT | Qt::CTRL);

  QAction *ctrlAltX = new QAction(this);
  ctrlAltX->setShortcut(Qt::Key_X | Qt::ALT | Qt::CTRL);

  connect(altQ, SIGNAL(triggered()), this, SLOT(toggleControlsTab()));
  this->addAction(altQ);
  connect(altW, SIGNAL(triggered()), this, SLOT(toggleStackTabs()));
  this->addAction(altW);
  connect(altE, SIGNAL(triggered()), this, SLOT(toggleControlKeyInteraction()));
  this->addAction(altE);
  connect(altD, SIGNAL(triggered()), this, SLOT(toggleWorkCheckBox()));
  this->addAction(altD);
  connect(altS, SIGNAL(triggered()), this, SLOT(toggleMainCheckBox()));
  this->addAction(altS);

  connect(stack1.mesh, SIGNAL(clearBuffers(int)), this, SLOT(clearBuffers(int)));
  connect(stack2.mesh, SIGNAL(clearBuffers(int)), this, SLOT(clearBuffers(int)));

  connect(&stack1.mesh->ccUpdateTimer, SIGNAL(timeout()), stack1.mesh, SLOT(updateCC()));
  connect(stack1.mesh, SIGNAL(updateCCData(int)), this, SLOT(updateCCData(int)));

  connect(&stack2.mesh->ccUpdateTimer, SIGNAL(timeout()), stack2.mesh, SLOT(updateCC()));
  connect(stack2.mesh, SIGNAL(updateCCData(int)), this, SLOT(updateCCData(int)));

  // Create progress widget and setup progress class
  Progress::instance().setupProgress(this, ui.progressToolBar);
  progressStart("", 0, false);

  connect(&(Progress::instance()), SIGNAL(progStart(QString, int, bool)), this, SLOT(progStart(QString, int, bool)));
  connect(&(Progress::instance()), SIGNAL(progAdvance(int)), this, SLOT(progAdvance(int)));
  connect(&(Progress::instance()), SIGNAL(progSetMsg(QString)), this, SLOT(progSetMsg(QString)));
  connect(&(Progress::instance()), SIGNAL(progSetMax(int)), this, SLOT(progSetMax(int)));
  connect(&(Progress::instance()), SIGNAL(progStop()), this, SLOT(progStop()));

  connect(ui.Stack1CCViewWidget, SIGNAL(execProcess(QString, QStringList)), this, SLOT(execProcess(QString, QStringList)));
  connect(ui.Stack2CCViewWidget, SIGNAL(execProcess(QString, QStringList)), this, SLOT(execProcess(QString, QStringList)));
}

void MorphoDynamX::createSetupProcess()
{
  currentSetup = new SetupProcess(this);
  Stack* s1 = currentSetup->addStack();
  s1->main()->setTransferFct(TransferFunction::scaleGreen());
  s1->work()->setTransferFct(TransferFunction::scaleCyan());
  Stack* s2 = currentSetup->addStack();
  s2->main()->setTransferFct(TransferFunction::scaleGreen());
  s2->work()->setTransferFct(TransferFunction::scaleCyan());
  currentSetup->addMesh(s1);
  currentSetup->addMesh(s2);
  currentSetup->p->camera = ui.Viewer->_camera;
//  Mesh* m1 = currentSetup->addMesh(s1);
//  m1->setSurfFct(TransferFunction::scaleGray());
//  m1->setHeatFct(TransferFunction::jet());
//  Mesh* m2 = currentSetup->addMesh(s2);
//  m2->setSurfFct(TransferFunction::scaleGray());
//  m2->setHeatFct(TransferFunction::jet());

  ui.Viewer->clip1.setClip(currentSetup->clip1());
  ui.Viewer->clip2.setClip(currentSetup->clip2());
  ui.Viewer->clip3.setClip(currentSetup->clip3());
  ui.Viewer->c1 = currentSetup->clip1();
  ui.Viewer->c2 = currentSetup->clip2();
  ui.Viewer->c3 = currentSetup->clip3();
  cutSurf.cut = currentSetup->cuttingSurface();
}

void MorphoDynamX::setDebug(bool debug)
{
  if(!debug)
    ui.actionDebug_Dialog->setVisible(false);
}

void MorphoDynamX::loadControls()
{
  // RSS A lot of this code is duplicated in updateStateFromProcess
  // Load clipplane info into controls
  ui.Clip1Enable->setChecked(ui.Viewer->c1->enabled());
  ui.Clip1Grid->setChecked(ui.Viewer->c1->grid());
  ui.Clip1Width->setSliderPosition(int(2000 * log(ui.Viewer->c1->width() * 2.0 / ui.Viewer->getSceneRadius())));
  ui.Clip2Enable->setChecked(ui.Viewer->c2->enabled());
  ui.Clip2Grid->setChecked(ui.Viewer->c2->grid());
  ui.Clip2Width->setSliderPosition(int(2000 * log(ui.Viewer->c2->width() * 2.0 / ui.Viewer->getSceneRadius())));
  ui.Clip3Enable->setChecked(ui.Viewer->c3->enabled());
  ui.Clip3Grid->setChecked(ui.Viewer->c3->grid());
  ui.Clip3Width->setSliderPosition(int(2000 * log(ui.Viewer->c3->width() * 2.0 / ui.Viewer->getSceneRadius())));

  ui.Clip1Width->setDefaultValue(0);
  ui.Clip2Width->setDefaultValue(0);
  ui.Clip3Width->setDefaultValue(0);

  ui.GlobalContrast->setValue(int(ui.Viewer->GlobalContrast * 5000.0));
  ui.GlobalBrightness->setValue(int((ui.Viewer->GlobalBrightness + 1.0) * 5000.0));
  ui.GlobalShininess->setValue(int(ui.Viewer->GlobalShininess/128.0 * 5000.0));
  ui.GlobalSpecular->setValue(int(ui.Viewer->GlobalSpecular * 5000.0));

  ui.GlobalBrightness->setDefaultValue(5000);
  ui.GlobalContrast->setDefaultValue(5000);
  ui.GlobalShininess->setDefaultValue(160);
  ui.GlobalSpecular->setDefaultValue(1000);

  // Load controls for Stack 1
  ui.Stack1MainShow->setChecked(stack1.stack->main()->isVisible());
  ui.Stack1MainBright->setValue(int(stack1.stack->main()->brightness() * 10000.0));
  ui.Stack1MainOpacity->setValue(int(stack1.stack->main()->opacity() * 10000.0));
  ui.Stack1MainLabels->setChecked(stack1.stack->main()->labels());
  ui.Stack1Main16Bit->setChecked(stack1.Main16Bit);
  ui.Stack1MainClipVoxels->setChecked(stack1.MainClipVoxels);
  ui.Stack1MainClipLabels->setChecked(stack1.MainClipLabels);
  ui.Stack1MainShading->setChecked(stack1.MainShading);

  ui.Stack1WorkShow->setChecked(stack1.stack->work()->isVisible());
  ui.Stack1WorkBright->setValue(int(stack1.stack->work()->brightness() * 10000.0));
  ui.Stack1WorkOpacity->setValue(int(stack1.stack->work()->opacity() * 10000.0));
  ui.Stack1WorkLabels->setChecked(stack1.stack->work()->labels());
  ui.Stack1Work16Bit->setChecked(stack1.Work16Bit);
  ui.Stack1WorkClipVoxels->setChecked(stack1.WorkClipVoxels);
  ui.Stack1WorkClipLabels->setChecked(stack1.WorkClipLabels);
  ui.Stack1WorkShading->setChecked(stack1.WorkShading);

  ui.Stack1SurfBright->setValue(int(stack1.mesh->brightness() * 10000.0));
  ui.Stack1SurfOpacity->setValue(int(stack1.mesh->opacity() * 10000.0));
  ui.Stack1SurfBlend->setChecked(stack1.mesh->blending());
  ui.Stack1SurfCull->setChecked(stack1.mesh->culling());
  ui.Stack1SurfClip->setChecked(stack1.mesh->cellClipping());
  ui.Stack1SurfShade->setChecked(stack1.mesh->shading());

  ui.Stack1SurfLabeling->setCurrentText(stack1.mesh->labeling());
  ui.Stack1SurfHeat->setCurrentText(stack1.mesh->heat());
  ui.Stack1SurfSignal->setCurrentText(stack1.mesh->signal());

  ui.Stack1ShowTrans->setChecked(stack1.stack->showTrans());
  ui.Stack1ShowBBox->setChecked(stack1.stack->showBBox());
  ui.Stack1ShowScale->setChecked(stack1.stack->showScale());
  ui.Stack1TieScales->setChecked(stack1.stack->tieScales());
  ui.Stack1Scale_X->setValue(stack1.stack->scale().x());
  ui.Stack1Scale_Y->setValue(stack1.stack->scale().y());
  ui.Stack1Scale_Z->setValue(stack1.stack->scale().z());

  ui.Stack1Scale_X->setDefaultValue(0);
  ui.Stack1Scale_Y->setDefaultValue(0);
  ui.Stack1Scale_Z->setDefaultValue(0);

  // Load controls for Stack 2
  ui.Stack2MainShow->setChecked(stack2.stack->main()->isVisible());
  ui.Stack2MainBright->setValue(int(stack2.stack->main()->brightness() * 10000.0f));
  ui.Stack2MainOpacity->setValue(int(stack2.stack->main()->opacity() * 10000.0f));
  ui.Stack2MainLabels->setChecked(stack2.stack->main()->labels());
  ui.Stack2Main16Bit->setChecked(stack2.Main16Bit);
  ui.Stack2MainClipVoxels->setChecked(stack2.MainClipVoxels);
  ui.Stack2MainClipLabels->setChecked(stack2.MainClipLabels);
  ui.Stack2MainShading->setChecked(stack2.MainShading);

  ui.Stack2WorkShow->setChecked(stack2.stack->work()->isVisible());
  ui.Stack2WorkBright->setValue(int(stack2.stack->work()->brightness() * 10000.0f));
  ui.Stack2WorkOpacity->setValue(int(stack2.stack->work()->opacity() * 10000.0f));
  ui.Stack2WorkLabels->setChecked(stack2.stack->work()->labels());
  ui.Stack2Work16Bit->setChecked(stack2.Work16Bit);
  ui.Stack2WorkClipVoxels->setChecked(stack2.WorkClipVoxels);
  ui.Stack2WorkClipLabels->setChecked(stack2.WorkClipLabels);
  ui.Stack2WorkShading->setChecked(stack2.WorkShading);

  ui.Stack2SurfBright->setValue(int(stack2.mesh->brightness() * 10000.0f));
  ui.Stack2SurfOpacity->setValue(int(stack2.mesh->opacity() * 10000.0f));
  ui.Stack2SurfBlend->setChecked(stack2.mesh->blending());
  ui.Stack2SurfCull->setChecked(stack2.mesh->culling());
  ui.Stack2SurfClip->setChecked(stack2.mesh->cellClipping());
  ui.Stack2SurfShade->setChecked(stack2.mesh->shading());

  ui.Stack2SurfLabeling->setCurrentText(stack2.mesh->labeling());
  ui.Stack2SurfHeat->setCurrentText(stack2.mesh->heat());
  ui.Stack2SurfSignal->setCurrentText(stack2.mesh->signal());

  ui.Stack2ShowTrans->setChecked(stack2.stack->showTrans());
  ui.Stack2ShowBBox->setChecked(stack2.stack->showBBox());
  ui.Stack2ShowScale->setChecked(stack2.stack->showScale());
  ui.Stack2TieScales->setChecked(stack2.stack->tieScales());
  ui.Stack2Scale_X->setValue(stack2.stack->scale().x());
  ui.Stack2Scale_Y->setValue(stack2.stack->scale().y());
  ui.Stack2Scale_Z->setValue(stack2.stack->scale().z());

  ui.Stack2Scale_X->setDefaultValue(0);
  ui.Stack2Scale_Y->setDefaultValue(0);
  ui.Stack2Scale_Z->setDefaultValue(0);

  // Load global settings
  ui.VoxelEditRadius->setValue(ImgData::VoxelEditRadius);
  ui.Slices->setValue(ImgData::Slices);
  ui.ScreenSampling->setValue(int(ui.Viewer->sampling * 10) - 10);

  ui.Slices->setDefaultValue(1014);
  ui.ScreenSampling->setDefaultValue(10);
  ui.VoxelEditRadius->setDefaultValue(25);

  ui.FillWorkData->setChecked(ImgData::FillWorkData);
  ui.SeedStack->setChecked(ImgData::SeedStack);
  ui.DrawCutSurf->setChecked(currentSetup->cuttingSurface()->isVisible());
  CuttingSurface* cut = currentSetup->cuttingSurface();
  switch(cut->mode()) {
  case CuttingSurface::THREE_AXIS:
    ui.ThreeAxis->setChecked(true);
    break;
  case CuttingSurface::PLANE:
    ui.CutSurfPlane->setChecked(true);
    break;
  case CuttingSurface::BEZIER:
    ui.CutSurfBezier->setChecked(true);
    break;
  }
  ui.CutSurfGrid->setChecked(cut->drawGrid());
  setCutSurfControl();

  // ui.CutSurfSizeX->setDefaultValue(0);
  // ui.CutSurfSizeY->setDefaultValue(0);
  // ui.CutSurfSizeZ->setDefaultValue(0);
}

void MorphoDynamX::connectControlsIn()
{
  // Create action group for select mode
  QActionGroup* selectActionGroup = new QActionGroup(this);
  selectActionGroup->addAction(ui.actionPickLabel);
  selectActionGroup->addAction(ui.actionAddNewSeed);
  selectActionGroup->addAction(ui.actionAddCurrentSeed);
  selectActionGroup->addAction(ui.actionDrawSignal);
  selectActionGroup->addAction(ui.actionGrabSeed);
  selectActionGroup->addAction(ui.actionFillLabel);
  selectActionGroup->addAction(ui.actionPickFillLabel);
  selectActionGroup->addAction(ui.actionLabelSelect);
  selectActionGroup->addAction(ui.actionRectSelectVertices);
  selectActionGroup->addAction(ui.actionRectSelectFaces);
  selectActionGroup->addAction(ui.actionRectSelectVolumes);
  selectActionGroup->addAction(ui.actionLassoSelectVertices);
  selectActionGroup->addAction(ui.actionLassoSelectFaces);
  selectActionGroup->addAction(ui.actionLassoSelectVolumes);
  selectActionGroup->addAction(ui.actionFaceSelect);
  selectActionGroup->addAction(ui.actionVolumeSelect);
  selectActionGroup->addAction(ui.actionConnectedSelect);

  selectActionGroup->addAction(ui.actionPickVolumeLabel);
  selectActionGroup->addAction(ui.actionFillVolumeLabel);
  selectActionGroup->addAction(ui.actionPickFillVolumeLabel);
  selectActionGroup->addAction(ui.actionVoxelEdit);
  selectActionGroup->addAction(ui.actionDeleteVolumeLabel);

  QMenu *selectRectMenu = new QMenu;
  selectRectMenu->addAction(ui.actionRectSelectVertices);
  selectRectMenu->addAction(ui.actionRectSelectFaces);
  selectRectMenu->addAction(ui.actionRectSelectVolumes);

  QToolButton *selectRectToolButton = new QToolButton;
  connect(selectRectToolButton, SIGNAL(triggered(QAction*)), selectRectToolButton, SLOT(setDefaultAction(QAction*)));
  selectRectToolButton->setPopupMode(QToolButton::DelayedPopup);
  selectRectToolButton->setMenu(selectRectMenu);
  selectRectToolButton->setDefaultAction(ui.actionRectSelectVertices);
  ui.meshToolbar->insertWidget(ui.actionConnectedSelect, selectRectToolButton);

  QMenu *selectLassoMenu = new QMenu;
  selectLassoMenu->addAction(ui.actionLassoSelectVertices);
  selectLassoMenu->addAction(ui.actionLassoSelectFaces);
  selectLassoMenu->addAction(ui.actionLassoSelectVolumes);

  QToolButton *selectLassoToolButton = new QToolButton;
  connect(selectLassoToolButton, SIGNAL(triggered(QAction*)), selectLassoToolButton, SLOT(setDefaultAction(QAction*)));
  selectLassoToolButton->setPopupMode(QToolButton::DelayedPopup);
  selectLassoToolButton->setMenu(selectLassoMenu);
  selectLassoToolButton->setDefaultAction(ui.actionLassoSelectVertices);
  ui.meshToolbar->insertWidget(ui.actionConnectedSelect, selectLassoToolButton);

  QMenu *selectCellMenu = new QMenu;
  selectCellMenu->addAction(ui.actionFaceSelect);
  selectCellMenu->addAction(ui.actionVolumeSelect);

  QToolButton *selectCellToolButton = new QToolButton;
  connect(selectCellToolButton, SIGNAL(triggered(QAction*)), selectCellToolButton, SLOT(setDefaultAction(QAction*)));
  selectCellToolButton->setPopupMode(QToolButton::DelayedPopup);
  selectCellToolButton->setMenu(selectCellMenu);
  selectCellToolButton->setDefaultAction(ui.actionFaceSelect);
  ui.meshToolbar->insertWidget(ui.actionConnectedSelect, selectCellToolButton);
}

void MorphoDynamX::connectControlsOut()
{
  // Connect outgoing signals from widgets
  connect(ui.Stack1Rotate, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));
  connect(ui.Stack2Rotate, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));
  connect(ui.Clip1Rotate, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));
  connect(ui.Clip2Rotate, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));
  connect(ui.Clip3Rotate, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));
  connect(ui.CutSurfRotate, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));

  connect(ui.GlobalBrightness, SIGNAL(valueChanged(int)), this, SLOT(globalBrightness()));
  connect(ui.GlobalContrast, SIGNAL(valueChanged(int)), this, SLOT(globalContrast()));
  connect(ui.GlobalShininess, SIGNAL(valueChanged(int)), this, SLOT(globalShininess()));
  connect(ui.GlobalSpecular, SIGNAL(valueChanged(int)), this, SLOT(globalSpecular()));

  // Connect controls for Stack 1
  connect(ui.Stack1MainShow, SIGNAL(toggled(bool)), &stack1, SLOT(mainShow(bool)));
  connect(ui.Stack1MainShow, SIGNAL(toggled(bool)), this, SLOT(updateActiveText()));
  connect(ui.Stack1WorkShow, SIGNAL(toggled(bool)), &stack1, SLOT(workShow(bool)));
  connect(ui.Stack1WorkShow, SIGNAL(toggled(bool)), this, SLOT(updateActiveText()));

  connect(ui.Stack1MainBright, SIGNAL(valueChanged(int)), &stack1, SLOT(mainBright(int)));
  connect(ui.Stack1MainOpacity, SIGNAL(valueChanged(int)), &stack1, SLOT(mainOpacity(int)));
  connect(ui.Stack1MainLabels, SIGNAL(toggled(bool)), &stack1, SLOT(mainLabels(bool)));
  connect(ui.Stack1Main16Bit, SIGNAL(toggled(bool)), &stack1, SLOT(main16Bit(bool)));
  connect(ui.Stack1MainClipVoxels, SIGNAL(toggled(bool)), &stack1, SLOT(mainClipVoxels(bool)));
  connect(ui.Stack1MainClipLabels, SIGNAL(toggled(bool)), &stack1, SLOT(mainClipLabels(bool)));
  connect(ui.Stack1MainShading, SIGNAL(toggled(bool)), &stack1, SLOT(mainShading(bool)));
  connect(ui.Stack1MainColorMap, SIGNAL(clicked()), &stack1, SLOT(mainColorMap()));

  connect(ui.Stack1WorkBright, SIGNAL(valueChanged(int)), &stack1, SLOT(workBright(int)));
  connect(ui.Stack1WorkOpacity, SIGNAL(valueChanged(int)), &stack1, SLOT(workOpacity(int)));
  connect(ui.Stack1WorkLabels, SIGNAL(toggled(bool)), &stack1, SLOT(workLabels(bool)));
  connect(ui.Stack1Work16Bit, SIGNAL(toggled(bool)), &stack1, SLOT(work16Bit(bool)));
  connect(ui.Stack1WorkClipVoxels, SIGNAL(toggled(bool)), &stack1, SLOT(workClipVoxels(bool)));
  connect(ui.Stack1WorkClipLabels, SIGNAL(toggled(bool)), &stack1, SLOT(workClipLabels(bool)));
  connect(ui.Stack1WorkShading, SIGNAL(toggled(bool)), &stack1, SLOT(workShading(bool)));
  connect(ui.Stack1WorkColorMap, SIGNAL(clicked()), &stack1, SLOT(workColorMap()));

  connect(ui.Stack1SurfBright, SIGNAL(valueChanged(int)), &stack1, SLOT(surfBright(int)));
  connect(ui.Stack1SurfOpacity, SIGNAL(valueChanged(int)), &stack1, SLOT(surfOpacity(int)));
  connect(ui.Stack1SurfBlend, SIGNAL(toggled(bool)), &stack1, SLOT(surfBlend(bool)));
  connect(ui.Stack1SurfCull, SIGNAL(toggled(bool)), &stack1, SLOT(surfCull(bool)));
  connect(ui.Stack1SurfClip, SIGNAL(toggled(bool)), &stack1, SLOT(surfCellClipping(bool)));
  connect(ui.Stack1SurfShade, SIGNAL(toggled(bool)), &stack1, SLOT(surfShade(bool)));
  connect(ui.Stack1SurfLabeling, SIGNAL(currentTextChanged(const QString &)), &stack1, SLOT(surfLabeling(const QString &)));
  connect(&stack1, SIGNAL(loadHeatChoices(int)), this, SLOT(loadHeatChoices(int)));
  connect(ui.Stack1SurfSignal, SIGNAL(currentTextChanged(const QString &)), &stack1, SLOT(surfSignal(const QString &)));
  connect(ui.Stack1SurfHeat, SIGNAL(currentTextChanged(const QString &)), &stack1, SLOT(surfHeat(const QString &)));

  connect(ui.Stack1ShowTrans, SIGNAL(toggled(bool)), &stack1, SLOT(showTrans(bool)));
  connect(ui.Stack1ShowTrans, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));
  connect(ui.Stack1ShowBBox, SIGNAL(toggled(bool)), &stack1, SLOT(showBBox(bool)));
  connect(ui.Stack1ShowScale, SIGNAL(toggled(bool)), &stack1, SLOT(showScale(bool)));
  connect(ui.Stack1TieScales, SIGNAL(toggled(bool)), &stack1, SLOT(tieScales(bool)));
  connect(ui.Stack1Scale_X, SIGNAL(valueChanged(int)), &stack1, SLOT(scaleX(int)));
  connect(ui.Stack1Scale_Y, SIGNAL(valueChanged(int)), &stack1, SLOT(scaleY(int)));
  connect(ui.Stack1Scale_Z, SIGNAL(valueChanged(int)), &stack1, SLOT(scaleZ(int)));

  // Connect controls for Stack 2
  connect(ui.Stack2MainShow, SIGNAL(toggled(bool)), &stack2, SLOT(mainShow(bool)));
  connect(ui.Stack2MainShow, SIGNAL(toggled(bool)), this, SLOT(updateActiveText()));
  connect(ui.Stack2WorkShow, SIGNAL(toggled(bool)), &stack2, SLOT(workShow(bool)));
  connect(ui.Stack2WorkShow, SIGNAL(toggled(bool)), this, SLOT(updateActiveText()));

  connect(ui.Stack2MainBright, SIGNAL(valueChanged(int)), &stack2, SLOT(mainBright(int)));
  connect(ui.Stack2MainOpacity, SIGNAL(valueChanged(int)), &stack2, SLOT(mainOpacity(int)));
  connect(ui.Stack2MainLabels, SIGNAL(toggled(bool)), &stack2, SLOT(mainLabels(bool)));
  connect(ui.Stack2Main16Bit, SIGNAL(toggled(bool)), &stack2, SLOT(main16Bit(bool)));
  connect(ui.Stack2MainClipVoxels, SIGNAL(toggled(bool)), &stack2, SLOT(mainClipVoxels(bool)));
  connect(ui.Stack2MainClipLabels, SIGNAL(toggled(bool)), &stack2, SLOT(mainClipLabels(bool)));
  connect(ui.Stack2MainShading, SIGNAL(toggled(bool)), &stack2, SLOT(mainShading(bool)));
  connect(ui.Stack2MainColorMap, SIGNAL(clicked()), &stack2, SLOT(mainColorMap()));

  connect(ui.Stack2WorkBright, SIGNAL(valueChanged(int)), &stack2, SLOT(workBright(int)));
  connect(ui.Stack2WorkOpacity, SIGNAL(valueChanged(int)), &stack2, SLOT(workOpacity(int)));
  connect(ui.Stack2WorkLabels, SIGNAL(toggled(bool)), &stack2, SLOT(workLabels(bool)));
  connect(ui.Stack2Work16Bit, SIGNAL(toggled(bool)), &stack2, SLOT(work16Bit(bool)));
  connect(ui.Stack2WorkClipVoxels, SIGNAL(toggled(bool)), &stack2, SLOT(workClipVoxels(bool)));
  connect(ui.Stack2WorkClipLabels, SIGNAL(toggled(bool)), &stack2, SLOT(workClipLabels(bool)));
  connect(ui.Stack2WorkShading, SIGNAL(toggled(bool)), &stack2, SLOT(workShading(bool)));
  connect(ui.Stack2WorkColorMap, SIGNAL(clicked()), &stack2, SLOT(workColorMap()));

  connect(ui.Stack2SurfBright, SIGNAL(valueChanged(int)), &stack2, SLOT(surfBright(int)));
  connect(ui.Stack2SurfOpacity, SIGNAL(valueChanged(int)), &stack2, SLOT(surfOpacity(int)));
  connect(ui.Stack2SurfBlend, SIGNAL(toggled(bool)), &stack2, SLOT(surfBlend(bool)));
  connect(ui.Stack2SurfCull, SIGNAL(toggled(bool)), &stack2, SLOT(surfCull(bool)));
  connect(ui.Stack2SurfClip, SIGNAL(toggled(bool)), &stack2, SLOT(surfCellClipping(bool)));
  connect(ui.Stack2SurfShade, SIGNAL(toggled(bool)), &stack2, SLOT(surfShade(bool)));
  connect(ui.Stack2SurfLabeling, SIGNAL(currentTextChanged(const QString &)), &stack2, SLOT(surfLabeling(const QString &)));
  connect(&stack2, SIGNAL(loadHeatChoices(int)), this, SLOT(loadHeatChoices(int)));
  connect(ui.Stack2SurfSignal, SIGNAL(currentTextChanged(const QString &)), &stack2, SLOT(surfSignal(const QString &)));
  connect(ui.Stack2SurfHeat, SIGNAL(currentTextChanged(const QString &)), &stack2, SLOT(surfHeat(const QString &)));

  connect(ui.Stack2ShowTrans, SIGNAL(toggled(bool)), &stack2, SLOT(showTrans(bool)));
  connect(ui.Stack2ShowTrans, SIGNAL(toggled(bool)), this, SLOT(rotate(bool)));
  connect(ui.Stack2ShowScale, SIGNAL(toggled(bool)), &stack2, SLOT(showScale(bool)));
  connect(ui.Stack2TieScales, SIGNAL(toggled(bool)), &stack2, SLOT(tieScales(bool)));
  connect(ui.Stack2ShowBBox, SIGNAL(toggled(bool)), &stack2, SLOT(showBBox(bool)));
  connect(ui.Stack2Scale_X, SIGNAL(valueChanged(int)), &stack2, SLOT(scaleX(int)));
  connect(ui.Stack2Scale_Y, SIGNAL(valueChanged(int)), &stack2, SLOT(scaleY(int)));
  connect(ui.Stack2Scale_Z, SIGNAL(valueChanged(int)), &stack2, SLOT(scaleZ(int)));

  // Clipping controls
  connect(ui.Clip1Enable, SIGNAL(toggled(bool)), ui.Viewer, SLOT(clip1Enable(bool)));
  connect(ui.Clip1Grid, SIGNAL(toggled(bool)), ui.Viewer, SLOT(clip1Grid(bool)));
  connect(ui.Clip1Width, SIGNAL(valueChanged(int)), ui.Viewer, SLOT(clip1Width(int)));
  connect(ui.Clip2Enable, SIGNAL(toggled(bool)), ui.Viewer, SLOT(clip2Enable(bool)));
  connect(ui.Clip2Grid, SIGNAL(toggled(bool)), ui.Viewer, SLOT(clip2Grid(bool)));
  connect(ui.Clip2Width, SIGNAL(valueChanged(int)), ui.Viewer, SLOT(clip2Width(int)));
  connect(ui.Clip3Enable, SIGNAL(toggled(bool)), ui.Viewer, SLOT(clip3Enable(bool)));
  connect(ui.Clip3Grid, SIGNAL(toggled(bool)), ui.Viewer, SLOT(clip3Grid(bool)));
  connect(ui.Clip3Width, SIGNAL(valueChanged(int)), ui.Viewer, SLOT(clip3Width(int)));

  connect(ui.Viewer, SIGNAL(changeSceneRadius(double)), this, SLOT(resetClipControl(double)));

  connect(ui.Slices, SIGNAL(valueChanged(int)), ui.Viewer, SLOT(slices(int)));
  connect(ui.ScreenSampling, SIGNAL(valueChanged(int)), ui.Viewer, SLOT(screenSampling(int)));
  connect(ui.VoxelEditRadius, SIGNAL(valueChanged(int)), this, SLOT(voxelEditRadius(int)));

  connect(ui.ProcessStackCommand, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this,
          SLOT(processCommand(QTreeWidgetItem*, QTreeWidgetItem*)));
  connect(ui.ProcessMeshCommand, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this,
          SLOT(processCommand(QTreeWidgetItem*, QTreeWidgetItem*)));
  connect(ui.ProcessToolsCommand, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this,
          SLOT(processCommand(QTreeWidgetItem*, QTreeWidgetItem*)));
  connect(ui.ProcessModelCommand, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this,
          SLOT(processCommand(QTreeWidgetItem*, QTreeWidgetItem*)));
  connect(ui.ProcessTasksCommand, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this,
          SLOT(processTasksCommand(QTreeWidgetItem*, QTreeWidgetItem*)));

  connect(ui.ProcessTabs, SIGNAL(currentChanged(int)), this, SLOT(processCommandTab(int)));
  connect(ui.StackTabs, SIGNAL(currentChanged(int)), this, SLOT(updateActiveText()));
  connect(ui.FillWorkData, SIGNAL(toggled(bool)), this, SLOT(fillWorkData(bool)));
  connect(ui.SeedStack, SIGNAL(toggled(bool)), this, SLOT(seedStack(bool)));

  connect(stack1.mesh, SIGNAL(updateActiveText()), this, SLOT(updateActiveText()));
  connect(stack2.mesh, SIGNAL(updateActiveText()), this, SLOT(updateActiveText()));

  // File menu
  connect(ui.menuFileOpen, SIGNAL(triggered()), this, SLOT(fileOpen()));
  connect(ui.menuFileSave, SIGNAL(triggered()), this, SLOT(fileSave()));
  connect(ui.menuFileSaveAs, SIGNAL(triggered()), this, SLOT(fileSaveAs()));
  connect(ui.menuFileQuickSave, SIGNAL(triggered()), this, SLOT(fileQuickSave()));
  connect(ui.menuExit, SIGNAL(triggered()), this, SLOT(exitMDX()));

  // Stack menu
  connect(ui.menuStack1Open, SIGNAL(triggered()), this, SLOT(stack1Open()));
  connect(ui.menuStack1Save, SIGNAL(triggered()), this, SLOT(stack1Save()));
  connect(ui.menuStack1Import, SIGNAL(triggered()), this, SLOT(stack1Import()));
  connect(ui.menuStack1Load, SIGNAL(triggered()), this, SLOT(stack1ImportSeries()));
  connect(ui.menuStack1Export, SIGNAL(triggered()), this, SLOT(stack1Export()));
  connect(ui.menuWorkStack1Open, SIGNAL(triggered()), this, SLOT(stack1WorkOpen()));
  connect(ui.menuWorkStack1Save, SIGNAL(triggered()), this, SLOT(stack1WorkSave()));
  connect(ui.menuWorkStack1Import, SIGNAL(triggered()), this, SLOT(stack1WorkImport()));
  connect(ui.menuWorkStack1Load, SIGNAL(triggered()), this, SLOT(stack1WorkImportSeries()));
  connect(ui.menuWorkStack1Export, SIGNAL(triggered()), this, SLOT(stack1WorkExport()));
  connect(ui.menuStack1Reset, SIGNAL(triggered()), this, SLOT(stack1Reset()));

  connect(ui.menuStack2Open, SIGNAL(triggered()), this, SLOT(stack2Open()));
  connect(ui.menuStack2Save, SIGNAL(triggered()), this, SLOT(stack2Save()));
  connect(ui.menuStack2Import, SIGNAL(triggered()), this, SLOT(stack2Import()));
  connect(ui.menuStack2Load, SIGNAL(triggered()), this, SLOT(stack2ImportSeries()));
  connect(ui.menuStack2Export, SIGNAL(triggered()), this, SLOT(stack2Export()));
  connect(ui.menuWorkStack2Open, SIGNAL(triggered()), this, SLOT(stack2WorkOpen()));
  connect(ui.menuWorkStack2Save, SIGNAL(triggered()), this, SLOT(stack2WorkSave()));
  connect(ui.menuWorkStack2Import, SIGNAL(triggered()), this, SLOT(stack2WorkImport()));
  connect(ui.menuWorkStack2Load, SIGNAL(triggered()), this, SLOT(stack2WorkImportSeries()));
  connect(ui.menuWorkStack2Export, SIGNAL(triggered()), this, SLOT(stack2WorkExport()));
  connect(ui.menuStack2Reset, SIGNAL(triggered()), this, SLOT(stack2Reset()));

  // Mesh menu
  connect(ui.menuMesh1Import, SIGNAL(triggered()), this, SLOT(mesh1Import()));
  connect(ui.menuMesh1Export, SIGNAL(triggered()), this, SLOT(mesh1Export()));
  connect(ui.menuMesh1Load, SIGNAL(triggered()), this, SLOT(mesh1Load()));
  connect(ui.menuMesh1Save, SIGNAL(triggered()), this, SLOT(mesh1Save()));
  connect(ui.menuMesh1Reset, SIGNAL(triggered()), this, SLOT(mesh1Reset()));
  connect(ui.menuMesh2Import, SIGNAL(triggered()), this, SLOT(mesh2Import()));
  connect(ui.menuMesh2Export, SIGNAL(triggered()), this, SLOT(mesh2Export()));
  connect(ui.menuMesh2Load, SIGNAL(triggered()), this, SLOT(mesh2Load()));
  connect(ui.menuMesh2Save, SIGNAL(triggered()), this, SLOT(mesh2Save()));
  connect(ui.menuMesh2Reset, SIGNAL(triggered()), this, SLOT(mesh2Reset()));

  //Attributes Menu
  connect(ui.menuMesh1Attributes, SIGNAL(triggered()), this, SLOT(mesh1ManageAttributes()));
  connect(ui.menuMesh2Attributes, SIGNAL(triggered()), this, SLOT(mesh2ManageAttributes()));

  // Selection menu
  connect(ui.actionSelectAll, SIGNAL(triggered()), this, SLOT(meshSelectAll()));
  connect(ui.actionClearSelection, SIGNAL(triggered()), this, SLOT(meshClearSelection()));
  connect(ui.actionInvertSelection, SIGNAL(triggered()), this, SLOT(meshInvertSelection()));
  connect(ui.actionCopySelection, SIGNAL(triggered()), this, SLOT(meshCopySelection()));
  connect(ui.actionPasteSelection, SIGNAL(triggered()), this, SLOT(meshPasteSelection()));
  connect(ui.actionAddCurrentLabel, SIGNAL(triggered()), this, SLOT(meshAddCurrentLabel()));
  connect(ui.actionSelectUnlabled, SIGNAL(triggered()), this, SLOT(meshAddUnlabeled()));
  connect(ui.actionRemoveCurrentLabel, SIGNAL(triggered()), this, SLOT(meshRemoveCurrentLabel()));
  connect(ui.actionChangeSeed, SIGNAL(triggered()), this, SLOT(changeCurrentSeed()));

  // Help menu
  connect(ui.actionUserManual, SIGNAL(triggered()), this, SLOT(userManual()));
  connect(ui.actionDoxygen, SIGNAL(triggered()), this, SLOT(doxygen()));
  connect(ui.actionQGLViewerHelp, SIGNAL(triggered()), this, SLOT(qglViewerHelp()));
  connect(ui.actionAbout, SIGNAL(triggered()), this, SLOT(about()));

  // Edit toolbar
  connect(ui.actionLabelColor, SIGNAL(triggered()), ui.Viewer, SLOT(labelColor()));
  connect(ui.Viewer, SIGNAL(setLabelColor(QIcon&)), this, SLOT(setLabelColor(QIcon&)));
  connect(ui.actionEraseSelection, SIGNAL(triggered()), this, SLOT(eraseSelect()));
  connect(ui.actionFillSelection, SIGNAL(triggered()), this, SLOT(fillSelect()));
  connect(ui.actionDeleteSelection, SIGNAL(triggered()), this, SLOT(deleteSelection()));
  connect(ui.actionDeleteSelection, SIGNAL(triggered()), this, SLOT(deleteSelection()));
  connect(ui.Viewer, SIGNAL(deleteSelection()), this, SLOT(deleteSelection()));

  // View toolbar
  connect(ui.actionResetView, SIGNAL(triggered()), ui.Viewer, SLOT(resetView()));
  connect(ui.actionEditParms, SIGNAL(triggered()), this, SLOT(editParms()));
  connect(ui.actionProcessDocs, SIGNAL(triggered()), this, SLOT(processDocs()));
  connect(ui.actionColorPalette, SIGNAL(triggered()), this, SLOT(colorPalette()));
  connect(ui.actionSwapSurfaces, SIGNAL(triggered()), this, SLOT(swapSurfaces()));
  connect(Colors::instance(), SIGNAL(colorsChanged()), ui.Viewer, SLOT(update()));
  connect(ui.actionEditLabels, SIGNAL(toggled(bool)), this, SLOT(labelsEdit(bool)));

  connect(ui.actionSaveScreenshot, SIGNAL(triggered()), ui.Viewer, SLOT(saveScreenshot()));
  connect(ui.Viewer, SIGNAL(recordingMovie(bool)), ui.actionRecordMovie, SLOT(setChecked(bool)));
  connect(ui.actionRecordMovie, SIGNAL(toggled(bool)), ui.Viewer, SLOT(recordMovie(bool)));
  connect(ui.actionReload_shaders, SIGNAL(triggered()), ui.Viewer, SLOT(reloadShaders()));

  // Object communication
  connect(&stack1, SIGNAL(viewerUpdate()), ui.Viewer, SLOT(viewerUpdate()));
  connect(&stack2, SIGNAL(viewerUpdate()), ui.Viewer, SLOT(viewerUpdate()));
  connect(&stack1, SIGNAL(updateSliderScale()), this, SLOT(updateSliderScale()));
  connect(&stack2, SIGNAL(updateSliderScale()), this, SLOT(updateSliderScale()));
  //connect(&stack1, SIGNAL(forceSurfHeat()), this, SLOT(forceStack1SurfHeat()));
  //connect(&stack2, SIGNAL(forceSurfHeat()), this, SLOT(forceStack2SurfHeat()));
  connect(&stack1, SIGNAL(toggleEditLabels()), ui.actionEditLabels, SLOT(trigger()));
  connect(&stack2, SIGNAL(toggleEditLabels()), ui.actionEditLabels, SLOT(trigger()));

  // Cutting surface controls
  connect(ui.DrawCutSurf, SIGNAL(toggled(bool)), &cutSurf, SLOT(drawCutSurf(bool)));
  connect(ui.ThreeAxis, SIGNAL(toggled(bool)), &cutSurf, SLOT(cutSurfThreeAxis(bool)));
  connect(ui.CutSurfGrid, SIGNAL(toggled(bool)), &cutSurf, SLOT(cutSurfGrid(bool)));
  connect(ui.CutSurfReset, SIGNAL(clicked()), this, SLOT(resetCutSurf()));
  connect(ui.CutSurfPlane, SIGNAL(toggled(bool)), &cutSurf, SLOT(cutSurfPlane(bool)));
  connect(ui.CutSurfBezier, SIGNAL(toggled(bool)), &cutSurf, SLOT(cutSurfBezier(bool)));
  connect(ui.CutSurfSizeX, SIGNAL(valueChanged(int)), &cutSurf, SLOT(sizeX(int)));
  connect(ui.CutSurfSizeY, SIGNAL(valueChanged(int)), &cutSurf, SLOT(sizeY(int)));
  connect(ui.CutSurfSizeZ, SIGNAL(valueChanged(int)), &cutSurf, SLOT(sizeZ(int)));

  // Changing label
  connect(ui.Viewer, SIGNAL(selectLabelChanged(int)), this, SLOT(changeSelectLabel(int)));
}

// Update sliders according to scale values. Useful when scales are tied to each other.
void MorphoDynamX::updateSliderScale()
{
  QWidget *currentTab = ui.StackTabs->currentWidget();
  if(currentTab == ui.Stack1Tab) {
    ui.Stack1Scale_X->setValue(stack1.toSliderScale(stack1.stack->scale().x()));
    ui.Stack1Scale_Y->setValue(stack1.toSliderScale(stack1.stack->scale().y()));
    ui.Stack1Scale_Z->setValue(stack1.toSliderScale(stack1.stack->scale().z()));
  }
  if(currentTab == ui.Stack2Tab) {
    ui.Stack2Scale_X->setValue(stack2.toSliderScale(stack2.stack->scale().x()));
    ui.Stack2Scale_Y->setValue(stack2.toSliderScale(stack2.stack->scale().y()));
    ui.Stack2Scale_Z->setValue(stack2.toSliderScale(stack2.stack->scale().z()));
  }
};

void MorphoDynamX::readParms()
{
  // Get filename
  QString filename = projectFile();
  modified(false);

  if(!filename.isEmpty()) {
    QFileInfo fi(filename);
    if(!fi.exists()) {
      if(!filename.endsWith(".mdxv", Qt::CaseInsensitive) and !filename.endsWith(".mgxv", Qt::CaseInsensitive))
        filename += ".mdxv";
      if(!QFileInfo(filename).exists()) {
        QFile f(filename);
        f.open(QIODevice::WriteOnly);
        f.close();
      }
    }
    filename = QDir(filename).absolutePath();

    filename = stripCurrentDir(filename);
    if(filename.isEmpty())
      setWindowTitle(QString("MorphoDynamX"));
    else
      setWindowTitle(QString("MorphoDynamX - ") + filename);
    filename = QDir::currentPath() + "/" + filename;

    // Check ending, add suffix if required
    QString suffix = ".mdxv";
    if(filename.right(suffix.size()) != suffix)
      filename.append(suffix);

    // Check that file can be read
    QFile file(projectFile());
    if(!file.exists()) {
      if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        mdxWarning << "readParms::Cannot create parms file-" << projectFile() << endl;
        return;
      }
    } else {
      if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        mdxWarning << "readParms::Cannot open parms file for reading-" << projectFile() << endl;
        return;
      }
    }
    file.close();
  }

  // Create parms object
  Parms parms(projectFile());

  // Read model parms
  parms("Main", "SurfOffset", ImgData::SurfOffset, .01);

  parms("Main", "Slices", ImgData::Slices, 1024u);
  parms("Main", "ScreenSampling", ui.Viewer->sampling, 0.0);
  parms("Main", "TileCount", ImgData::TileCount, 20u);
  parms("Main", "MaxTexSize", ImgData::MaxTexSize, Point3u(1024u, 1024u, 1024u));
  parms("Main", "DrawNormals", ImgData::DrawNormals, 0.0);
  parms("Main", "DrawOffset", ImgData::DrawOffset, 1.0);
  if(ImgData::DrawOffset < 1.0f)
    ImgData::DrawOffset = 1.0f;
  parms("Main", "DrawZeroLabels", ImgData::DrawZeroLabels, 0.0);
  parms("Main", "DrawNhbds", ImgData::DrawNhbds, 0.0);
  parms("Main", "DeleteBadVertex", ImgData::DeleteBadVertex, false);
  parms("Main", "CudaHoldMem", CudaHoldMem, size_t(0));

  ImgData::scaleBar.readParms(parms, "ScaleBar");
  ImgData::colorBar.readParms(parms, "ColorBar");

  parms("Main", "VoxelEditRadius", ImgData::VoxelEditRadius, 25);
  parms("Main", "VoxelEditMaxPix", ImgData::VoxelEditMaxPix, 1024000);

  parms("Main", "MeshPointSize", ImgData::MeshPointSize, 3.0);
  parms("Main", "MeshLineWidth", ImgData::MeshLineWidth, 1.0);
  parms("Main", "FillWorkData", ImgData::FillWorkData, false);
  parms("Main", "SeedStack", ImgData::SeedStack, false);

  Colors::instance()->readParms(parms, "Colors");

  // Read the viewer and stack parms
  ui.Viewer->readParms(parms, "Main");
  if(currentSetup) {
    currentSetup->setGlobalContrast(ui.Viewer->GlobalContrast);
    currentSetup->setGlobalBrightness(ui.Viewer->GlobalBrightness);
    currentSetup->setGlobalShininess(ui.Viewer->GlobalShininess);
    currentSetup->setGlobalSpecular(ui.Viewer->GlobalSpecular);
  }
  stack1.readParms(parms, "Stack1");
  stack2.readParms(parms, "Stack2");
  cutSurf.readParms(parms, "Plane");

  readProcessParms();

  parms.all("Labels", "Color", ImgData::LabelColors);
  if(ImgData::LabelColors.empty()) {
    ImgData::LabelColors.resize(16);
    ImgData::LabelColors[0] = Colorf(1, 0, 0, 1);
    ImgData::LabelColors[1] = Colorf(0, 1, 0, 1);
    ImgData::LabelColors[2] = Colorf(0, 0, 1, 1);
    ImgData::LabelColors[3] = Colorf(1, 1, 0, 1);
    ImgData::LabelColors[4] = Colorf(1, 0, 1, 1);
    ImgData::LabelColors[5] = Colorf(0, 1, 1, 1);
    ImgData::LabelColors[6] = Colorf(.5, 0, 0, 1);
    ImgData::LabelColors[7] = Colorf(0, .5, 0, 1);
    ImgData::LabelColors[8] = Colorf(0, 0, .5, 1);
    ImgData::LabelColors[9] = Colorf(.5, 0, .5, 1);
    ImgData::LabelColors[10] = Colorf(.5, .5, 0, 1);
    ImgData::LabelColors[11] = Colorf(1, 0, .5, 1);
    ImgData::LabelColors[12] = Colorf(.5, .5, 1, 1);
    ImgData::LabelColors[13] = Colorf(0, .5, 1, 1);
    ImgData::LabelColors[14] = Colorf(.5, 0, 1, 1);
    ImgData::LabelColors[15] = Colorf(1, .5, 0, 1);
  }
  stack1.initTex();
  stack2.initTex();

  // Set cuda memory
  setHoldMemGPU(CudaHoldMem);
}

void MorphoDynamX::readProcessParms()
{
  Parms parms(projectFile());

  mdxInfo << "Reading Process Parameters" << endl;

  QString mdxVersion;
  parms("Main", "MDXVersion", mdxVersion, QString("0.0"));

  std::vector<QString> procNames;
  std::vector<uint> numParms;
  std::vector<QString> parmNames;
  std::vector<QString> parmStrings;

  QStringList sections;
  if(mdxVersion >= QString("1.1"))
    sections << "Process";
  else
    sections <<"StackProcess" << "MeshProcess" << "GlobalProcess";

  // Read process parms into arrays
  forall(QString &section, sections) {
    if(mdxVersion >= QString("1.0")) {
      parms.all(section, "ProcessName", procNames);
      parms.all(section, "NumParms", numParms);
      parms.all(section, "ParmName", parmNames);
      parms.all(section, "ParmString", parmStrings);
    } else {
      // Older versions had both "strings" and "values", as well as "Named" vs "UnNamed" parameters
      // Now all parameters are stored as strings, and all parameters have names
      std::vector<int> nbStrings;
      std::vector<int> nbValues;
      std::vector<int> nbNamedStrings;
      std::vector<int> nbNamedValues;
      std::vector<QString> strings;
      std::vector<QString> values;
  
      parms.all(section, "ProcessName", procNames);
      parms.all(section, "NbProcessStrings", nbStrings);
      parms.all(section, "NbProcessValues", nbValues);
      parms.all(section, "NbProcessNamedStrings", nbNamedStrings);
      parms.all(section, "NbProcessNamedValues", nbNamedValues);
      parms.all(section, "ProcessParameter", parmNames);
      parms.all(section, "ProcessString", strings);
      parms.all(section, "ProcessValue", values);
  
      int valuesIdx = 0;
      int stringsIdx = 0;
      for(size_t i = 0; i < procNames.size(); ++i) {
        if(i >= nbStrings.size() or i >= nbValues.size() or i >= nbNamedStrings.size()
           or i >= nbNamedValues.size()) {
          mdxInfo << "Error reading Process definition for process " 
              << procNames[i] << endl;
          return;
        }
        numParms.push_back(nbNamedStrings[i] + nbNamedValues[i]);
        // Get named strings
        for(int j = 0; j < nbNamedStrings[i]; ++j) {
          if(stringsIdx >= (int)strings.size()) {
            mdxInfo << "Error reading String parms for process " << procNames[i] << endl;
            return;
          }
          parmStrings.push_back(strings[stringsIdx++]);
        }
        // Toss "unnamed strings"
        stringsIdx += nbStrings[i] - nbNamedStrings[i];
  
        // Get named values
        for(int j = 0; j < nbNamedValues[i]; ++j) {
          if(valuesIdx >= (int)values.size()) {
            mdxInfo << "Error reading Value parms for process " << procNames[i] << endl;
            return;
          }
          parmStrings.push_back(values[valuesIdx++]);
        }
        // Toss "unnamed values"
        valuesIdx += nbValues[i] - nbNamedValues[i];
      }
    }
    // If before version 1.1, add the path to the process names
    if(mdxVersion < QString("1.1")) {
      for(uint i = 0; i < procNames.size(); ++i) {
        QMap<QString, mdx::ProcessDefinition>::const_iterator it = savedProc.constBegin();
        while (it != savedProc.constEnd()) {
          if(it.key().endsWith(procNames[i])) {
            procNames[i] = it.key();
          }
          ++it;
        }      
      }
    }
  
    // Now process parameters
    size_t parmIdx = 0;
    for(size_t i = 0; i < procNames.size(); ++i) {
      // Check process definition
      if(i >= numParms.size()) {
        mdxInfo << "Error reading Process definition for process " << procNames[i] << endl;
        return;
      }
  
      // Get parm names and their string values and put in a map
      std::map<QString, QString> parmMap;
      for(size_t j = 0; j < numParms[i]; ++j) {
        if(parmIdx + j >= parmNames.size() or parmIdx + j >= parmStrings.size()) {
          mdxInfo << "Error reading Parms for process " << procNames[i] << endl;
          return;
        }
        parmMap[parmNames[parmIdx + j]] = parmStrings[parmIdx + j];
      }
  
      // Get process definition and fill in the parms
      if(savedProc.count(procNames[i]) == 1) {
        ProcessDefinition& def = savedProc[procNames[i]];
        for(int j = 0; j < def.parmNames.size(); ++j)
          if(parmMap.count(def.parmNames[j]) == 1)
            def.parms[j] = parmMap[def.parmNames[j]];
          else
            mdxInfo << "Parameter '" << def.parmNames[j] << "' not found for process '" 
                          << procNames[i] << "'" << endl;
      } else
        mdxInfo << "Process '" << procNames[i] << "' not found." << endl;
      parmIdx += numParms[i];
    }
  }

  mdxInfo << "Reading Tasks" << endl;

  TaskEditDlg::tasks_t newTasks;
  TaskEditDlg::readTasks(projectFile(), savedProc, newTasks);
  updateCurrentTasks(newTasks);
}

void MorphoDynamX::updateCurrentTasks(const TaskEditDlg::tasks_t& saved_tasks)
{
  // Now, update the tasks with the saved ones, unless empty
  for(TaskEditDlg::tasks_t::const_iterator it = saved_tasks.begin(); 
                                                                 it != saved_tasks.end(); ++it) {
    QString name = it.key();
    QList<ProcessDefinition> sprocs = it.value();
    bool task_exists = tasks.contains(name);
    std::vector<bool> used;
    forall(ProcessDefinition& sdef, sprocs) {
      // Check the parameters are valid
      ProcessDefinition* real_def = getProcessDefinition(sdef.name);
      if(real_def) {
        sdef.description = real_def->description;
        sdef.parmNames = real_def->parmNames;
        sdef.parmDescs = real_def->parmDescs;
        sdef.icon = real_def->icon;
        sdef.parmChoice = real_def->parmChoice;
      }

      int old_proc_id = -1;
      if(task_exists) {
        const QList<ProcessDefinition>& procs = tasks[name];
        used.resize(procs.size(), false);
        for(int i = 0; i < procs.size(); ++i) {
          const ProcessDefinition& def = procs[i];
          if(not used[i] and def.name == sdef.name) {
            used[i] = true;
            old_proc_id = i;
            break;
          }
        }
      }
      if(not checkProcessParms(sdef.name, sdef.parms)) {
        if(old_proc_id < 0)
          getProcessParms(sdef.name, sdef.parms);
        else {
          const ProcessDefinition& def = tasks[name][old_proc_id];
          sdef.parms = def.parms;
        }
      }
    }
    tasks[name] = sprocs;
  }
}

void MorphoDynamX::writeProcessParms(const ProcessDefinition& def, QTextStream& pout)
{
  pout << "ProcessName: " << def.name << endl;
  pout << "NumParms: " << def.parmNames.size() << endl;
  for(int i = 0; i < def.parmNames.size(); ++i) {
    pout << "ParmName: " << def.parmNames[i] << endl;
    pout << "ParmString: " << def.parms[i] << endl;
  }
}

void MorphoDynamX::writeParms()
{
  // Open the file for write
  QFile file(projectFile());
  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    mdxWarning << "writeParms::Cannot open parms file for writing-" << projectFile() << endl;
    return;
  }
  QTextStream pout(&file);
  pout.setCodec("UTF-8");

  pout << "// Parameters for MorphoDynamX" << endl;
  pout << "[Main]" << endl;

  // Write out model parms
  pout << "MDXVersion: " << VERSION << endl;
  pout << "MDXRevision: " << REVISION << endl;
  pout << "ClearColor: " << ImgData::ClearColor << endl;
  pout << "SurfOffset: " << ImgData::SurfOffset << endl;
  pout << "Slices: " << ImgData::Slices << endl;
  pout << "ScreenSampling: " << ui.Viewer->sampling << endl;
  pout << "TileCount: " << ImgData::TileCount << endl;
  pout << "MaxTexSize: " << ImgData::MaxTexSize << endl;
  pout << endl;

  pout << "DrawNormals: " << ImgData::DrawNormals << endl;
  pout << "DrawOffset: " << ImgData::DrawOffset << endl;

  pout << "DrawZeroLabels: " << ImgData::DrawZeroLabels << endl;
  pout << "DrawNhbds: " << ImgData::DrawNhbds << endl;
  pout << "DeleteBadVertex: " << (ImgData::DeleteBadVertex ? "true" : "false") << endl;
  pout << "CudaHoldMem: " << CudaHoldMem << endl;
  pout << endl;

  pout << "MeshPointSize: " << ImgData::MeshPointSize << endl;
  pout << "MeshLineWidth: " << ImgData::MeshLineWidth << endl;
  pout << "SeedStack: " << (ImgData::SeedStack ? "true" : "false") << endl;

  pout << "VoxelEditRadius: " << ImgData::VoxelEditRadius << endl;
  pout << "VoxelEditMaxPix: " << ImgData::VoxelEditMaxPix << endl;
  pout << endl;

  pout << "[Labels]" << endl;
  for(size_t i = 0; i < ImgData::LabelColors.size(); ++i)
    pout << "Color: " << ImgData::LabelColors[i] << endl;
  pout << endl;

  Colors::instance()->writeParms(pout, "Colors");

  // Write the viewer and stack parms
  ui.Viewer->writeParms(pout, "Main");
  stack1.writeParms(pout, "Stack1");
  stack2.writeParms(pout, "Stack2");
  cutSurf.writeParms(pout, "Plane");

  ImgData::scaleBar.writeParms(pout, "ScaleBar");
  ImgData::colorBar.writeParms(pout, "ColorBar");

  pout << endl << "[Process]" << endl;
  forall(const ProcessDefinition& def, processes)
    writeProcessParms(def, pout);

  // Write out tasks
  TaskEditDlg::writeTasks(pout, tasks);

  needSaving = false;
}

void MorphoDynamX::saveView(const QString& filename)
{
  QStringList parms;
  parms << filename;
  launchProcess("Tools/System/Save View", parms, Process::PROCESS_RUN, false);
}

void MorphoDynamX::loadView(const QString& filename)
{
  QStringList parms;
  parms << filename;
  launchProcess("Tools/System/Load View", parms, Process::PROCESS_RUN, false);
}

void MorphoDynamX::clearMeshSelect()
{
  // RSS Fix
  mdxInfo << "Clear selection not implemented, fix" << endl;
}

// Slots
void MorphoDynamX::rotate(bool)
{
  if(ui.Stack1Rotate->isChecked())
    ui.Viewer->setManipulatedFrame(&stack1.getFrame());
  else if(ui.Stack2Rotate->isChecked())
    ui.Viewer->setManipulatedFrame(&stack2.getFrame());
  else if(ui.Clip1Rotate->isChecked())
    ui.Viewer->setManipulatedFrame(&ui.Viewer->c1->frame());
  else if(ui.Clip2Rotate->isChecked())
    ui.Viewer->setManipulatedFrame(&ui.Viewer->c2->frame());
  else if(ui.Clip3Rotate->isChecked())
    ui.Viewer->setManipulatedFrame(&ui.Viewer->c3->frame());
  else if(ui.CutSurfRotate->isChecked())
    ui.Viewer->setManipulatedFrame(cutSurf.getFrame());
  else
    ui.Viewer->setManipulatedFrame(&ui.Viewer->c1->frame());

  ui.Viewer->manipulatedFrame()->setSpinningSensitivity(ui.Viewer->Spinning);
  ui.Viewer->updateAll();
}

void MorphoDynamX::updateCCData(int stack)
{
  Mesh *mesh;
  if(stack == 0)
    mesh = stack1.mesh;
  else
    mesh = stack2.mesh;

  if(!mesh)
    return;

  // Check which CCs need to be update their VBOs
  QStringList ccNames = mesh->ccNames();

  bool updateView = false;
  for(int i = 0; i < ccNames.size(); i++) {
    QString ccName = ccNames[i];
    CCDrawParms &cdp = mesh->drawParms(ccName);
    if(!cdp.changed)
      continue;

    QString pName = mesh->ccAttr(ccName, "RenderProcess");
    if(pName.isEmpty()) // Use default if there is none
      pName = "Mesh/System/Render";

    Process *p = currentProcess->getProcess(pName);
    if(!p) {
      mdxInfo << "MorphoDynamX: Can't make render process:" << pName << endl;
      continue;
    } 

    QStringList parms;
    getProcessParms(p->name(), parms);
    if(parms.size() < 2) {
      mdxInfo << "Render process must have at least 2 parameters: " << endl;
      continue;
    }
    try{
      p->setParm("Mesh Id", QString("%1").arg(stack));
      p->setParm("CC Name", ccName);
      if(!p->finalize(this))
        mdxInfo << "Error calling render process finalize: " << pName << endl;
      else
        updateView = true;
    } catch(const QString& str) {
      QMessageBox::critical(this, QString("Error finalize of process %1: ").arg(pName), str);
    }
  }
  mesh->refreshNames();

  if(updateView)
    ui.Viewer->updateAll();
}

void MorphoDynamX::clearBuffers(int stack)
{
  Mesh *mesh;
  if(stack == 0)
    mesh = stack1.mesh;
  else
    mesh = stack2.mesh;

  if(!mesh)
    return;

  // Check which CCs need to be update their VBOs
  mesh->clearBuffers();
}

void MorphoDynamX::progStart(QString msg, int steps, bool allowCancel)
{
  progressStart(msg, steps, allowCancel);
}
void MorphoDynamX::progAdvance(int step)
{
  progressAdvance(step);
}
void MorphoDynamX::progSetMsg(QString msg)
{
  progressSetMsg(msg);
}
void MorphoDynamX::progSetMax(int step)
{
  progressSetMax(step);
}
void MorphoDynamX::progStop()
{
  progressStop();
}

void MorphoDynamX::fileOpen()
{
  QStringList parms;
  parms << projectFile();
  launchProcess("Tools/System/Load View", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::fileOpen(const QString& filename) 
{
  loadView(filename);
}

void MorphoDynamX::setModelPath(const QString& modelPath) 
{
  if(modelPath.size() == 0)
    return;

  // Set the main tab
  ui.ControlsTab->setCurrentIndex(2);

  // Look through tree widgets and make the matching one current
  QList<QTreeWidget *> trees;
  trees << ui.ProcessStackCommand << ui.ProcessMeshCommand
        << ui.ProcessToolsCommand << ui.ProcessTasksCommand << ui.ProcessModelCommand;
  for(int i = 0; i < trees.size(); i++) {
    if(i == 3) // Skip tasks
      continue; 
    QTreeWidgetItemIterator it(trees[i]);
    while (*it) {
      if ((*it)->text(1) == modelPath) {
        ui.ProcessTabs->setCurrentIndex(i);
        trees[i]->setCurrentItem(*it);
        return;
      }
      ++it;
    }
  }
}

void MorphoDynamX::fileSave()
{
  if(projectFile().isEmpty())
    fileSaveAs();
  else
    saveView(projectFile());
}

void MorphoDynamX::fileSaveAs()
{
  QStringList parms;
  parms << projectFile();
  launchProcess("Tools/System/Save View", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::fileQuickSave()
{
  QStringList parms;
  //parms << projectFile();
  launchProcess("Tools/System/Quick Save", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::exitMDX()
{
  QApplication::quit();
  //close();
}

QString MorphoDynamX::getMeshName(int num)
{
  ImgData* data = (num == 0) ? &stack1 : &stack2;
  return data->mesh->file();
}

QString MorphoDynamX::getStackName(bool main, int num)
{
  ImgData* data = (num == 0) ? &stack1 : &stack2;
  if(main)
    return data->stack->main()->file();
  else
    return data->stack->work()->file();
}

void MorphoDynamX::importMesh(int stack)
{
  QStringList parms;
  if(!getProcessParms("Mesh/System/Import", parms)) {
    QMessageBox::critical(this, "Error importing mesh", "There is no Mesh process named 'Import'");
    return;
  }
  // RSS how to fix these to use the named parms
  parms[0] = QString::number(stack);
  parms[1] = getMeshName(stack);
  launchProcess("Mesh/System/Import", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::resetMesh(int stack)
{
  QMessageBox::StandardButton a = QMessageBox::question(this, "Reset Mesh", "Are you sure you want to reset the mesh?",
                                                        QMessageBox::No | QMessageBox::Yes);
  if(a == QMessageBox::No)
    return;

  QStringList parms;
  if(!getProcessParms("Mesh/System/Reset", parms)) {
    QMessageBox::critical(this, "Error reseting mesh", "There is no Mesh process named 'Reset'");
    return;
  }
  parms[0] = QString::number(stack);
  launchProcess("Mesh/System/Reset", parms, Process::PROCESS_RUN, false);
}

void MorphoDynamX::exportMesh(int stack)
{
  QStringList parms;
  if(!getProcessParms("Mesh/System/Export", parms)) {
    QMessageBox::critical(this, "Error exporting mesh", "There is no Mesh process named 'Export'");
    return;
  }
  QString fn = getMeshName(stack);
  //if(!fn.isEmpty())
    parms[0] = fn;
  parms[3] = QString::number(stack);
  launchProcess("Mesh/System/Export", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::loadMesh(int stack)
{
  QStringList parms;
  if(!getProcessParms("Mesh/System/Load", parms)) {
    QMessageBox::critical(this, "Error loading mesh", "There is no Mesh process named 'Load'");
    return;
  }
  QString fn = getMeshName(stack);
  //if(!fn.isEmpty())
    parms[0] = fn;
  parms[3] = QString::number(stack);
  launchProcess("Mesh/System/Load", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::saveMesh(int stack)
{
  QStringList parms;
  if(!getProcessParms("Mesh/System/Save", parms)) {
    QMessageBox::critical(this, "Error saving mesh", "There is no Mesh process named 'Save'");
    return;
  }
  QString fn = getMeshName(stack);
  //if(!fn.isEmpty())
    parms[0] = fn;
  parms[2] = QString::number(stack);
  launchProcess("Mesh/System/Save", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::importStackSeries(int stack, bool main)
{
  QStringList parms;
  if(!getProcessParms("Stack/System/Import", parms)) {
    QMessageBox::critical(this, "Error loading stack", "There is no Stack process named 'Import Stack'");
    return;
  }
  parms[0] = QString::number(stack);
  parms[1] = (main ? "Main" : "Work");
  launchProcess("Stack/System/Import", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::importStack(int stack, bool main)
{
  QStringList parms;
  if(!getProcessParms("Stack/System/Import", parms)) {
    QMessageBox::critical(this, "Error loading stack", "Cannot find process: Stack/System/Import");
    return;
  }
  parms[1] = (main ? "Main" : "Work");
  parms[2] = QString::number(stack);
  launchProcess("Stack/System/Import", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::openStack(int stack, bool main)
{
  QStringList parms;
  if(!getProcessParms("Stack/System/Open", parms)) {
    QMessageBox::critical(this, "Error opening stack", "There is no Stack process named 'Open Stack'");
    return;
  }
  QString fn = getStackName(main, stack);
  //if(!fn.isEmpty())
    parms[0] = fn;
  parms[1] = (main ? "Main" : "Work");
  parms[2] = QString::number(stack);
  launchProcess("Stack/System/Open", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::saveStack(int stack, bool main)
{
  // Check if stack is scaled before saving
  Stack* s = currentSetup->getStack(stack);
  if(s->showScale()) {
    QMessageBox::StandardButton a = QMessageBox::question(
        this, "Save Stack", QString("Stack %1 is scaled. Do you want to save it?").arg(stack + 1),
        QMessageBox::No | QMessageBox::Yes);
    if(a == QMessageBox::No)
      return;
  }

  QStringList parms;
  if(!getProcessParms("Stack/System/Save", parms)) {
    QMessageBox::critical(this, "Error saving stack", "There is no Stack process named 'Save'");
    return;
  }
  QString fn = getStackName(main, stack);
  //if(!fn.isEmpty())
    parms[0] = fn;
  parms[1] = (main ? "Main" : "Work");
  parms[2] = QString::number(stack);
  launchProcess("Stack/System/Save", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::exportStack(int stack, bool main)
{
  QStringList parms;
  if(!getProcessParms("Stack/System/Export", parms)) {
    QMessageBox::critical(this, "Error exporting stack", "There is no Stack process named 'Export'");
    return;
  }
  parms[1] = (main ? "Main" : "Work");
  parms[4] = QString::number(stack);
  launchProcess("Stack/System/Export", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::execProcess(QString process, QStringList parms)
{
  launchProcess(process, parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::manageAttributesMesh(int mesh)
{
  QStringList parms;
  if(!getProcessParms("Mesh/Attributes/Manage Attributes", parms)) {
    QMessageBox::critical(this, "Error loading mesh", "There is no Mesh process named 'ManageAttributes'");
    return;
  }
  parms[0] = QString::number(mesh);
  launchProcess("Mesh/Attributes/Manage Attributes", parms, Process::PROCESS_RUN, true);
}

// Slots from the GUI
void MorphoDynamX::stack1Import() { importStack(0, true); }
void MorphoDynamX::stack1ImportSeries() { importStackSeries(0, true); }
void MorphoDynamX::stack1Save() { saveStack(0, true); }
void MorphoDynamX::stack1Export() { exportStack(0, true); }
void MorphoDynamX::stack1Open() { openStack(0, true); }
void MorphoDynamX::stack1Reset() { stack1.resetStack(); ui.Viewer->updateAll(); }
void MorphoDynamX::stack2Import() { importStack(1, true); }
void MorphoDynamX::stack2ImportSeries() { importStackSeries(1, true); }
void MorphoDynamX::stack2Save() { saveStack(1, true); }
void MorphoDynamX::stack2Export() { exportStack(1, true); }
void MorphoDynamX::stack2Open() { openStack(1, true); }
void MorphoDynamX::stack2Reset() { stack2.resetStack(); ui.Viewer->updateAll(); }
void MorphoDynamX::stack1WorkImport() { importStack(0, false); }
void MorphoDynamX::stack1WorkImportSeries() { importStackSeries(0, false); }
void MorphoDynamX::stack1WorkSave() { saveStack(0, false); }
void MorphoDynamX::stack1WorkExport() { exportStack(0, false); }
void MorphoDynamX::stack1WorkOpen() { openStack(0, false); }
void MorphoDynamX::stack2WorkImport() { importStack(1, false); }
void MorphoDynamX::stack2WorkImportSeries() { importStackSeries(1, false); }
void MorphoDynamX::stack2WorkSave() { saveStack(1, false); }
void MorphoDynamX::stack2WorkExport() { exportStack(1, false); }
void MorphoDynamX::stack2WorkOpen() { openStack(1, false); }
void MorphoDynamX::mesh1Load() { loadMesh(0); }
void MorphoDynamX::mesh1Save() { saveMesh(0); }
void MorphoDynamX::mesh2Import() { importMesh(1); }
void MorphoDynamX::mesh2Export() { exportMesh(1); }
void MorphoDynamX::mesh1Import() { importMesh(0); }
void MorphoDynamX::mesh1Export() { exportMesh(0); }
void MorphoDynamX::mesh1Reset() { resetMesh(0); }
void MorphoDynamX::mesh2Load() { loadMesh(1); }
void MorphoDynamX::mesh2Save() { saveMesh(1); }
void MorphoDynamX::mesh2Reset() { resetMesh(1); }
void MorphoDynamX::mesh1ManageAttributes() { manageAttributesMesh(0); }
void MorphoDynamX::mesh2ManageAttributes() { manageAttributesMesh(1); }
void MorphoDynamX::stopProgressBar() { progressCancel(); }

void MorphoDynamX::toggleControlsTab()
{
 int currentTab = ui.ControlsTab->currentIndex();
 currentTab++;
 if(currentTab>=ui.ControlsTab->count()) currentTab = 0;
 ui.ControlsTab->setCurrentIndex(currentTab);
}

void MorphoDynamX::toggleStackTabs()
{
 if(ui.ControlsTab->currentIndex() == 0){
   int currentTab = ui.StackTabs->currentIndex();
   currentTab++;
   if(currentTab>=ui.StackTabs->count()) currentTab = 0;
   ui.StackTabs->setCurrentIndex(currentTab);
 }
}

void MorphoDynamX::toggleMainCheckBox()
{
 if(ui.StackTabs->currentWidget() == ui.Stack1Tab){
   ui.Stack1MainShow->setChecked(!ui.Stack1MainShow->isChecked());
 } else if(ui.StackTabs->currentWidget() == ui.Stack2Tab){
   ui.Stack2MainShow->setChecked(!ui.Stack2MainShow->isChecked());
 }
}

void MorphoDynamX::toggleWorkCheckBox()
{
 if(ui.StackTabs->currentWidget() == ui.Stack1Tab){
   ui.Stack1WorkShow->setChecked(!ui.Stack1WorkShow->isChecked());
 } else if(ui.StackTabs->currentWidget() == ui.Stack2Tab){
   ui.Stack2WorkShow->setChecked(!ui.Stack2WorkShow->isChecked());
 }
}


void MorphoDynamX::toggleControlKeyInteraction()
{
  if(ui.Clip1Rotate->isChecked()) ui.Clip2Rotate->setChecked(true);
  else if(ui.Clip2Rotate->isChecked()) ui.Clip3Rotate->setChecked(true);
  else if(ui.Clip3Rotate->isChecked()) ui.Stack1Rotate->setChecked(true);
  else if(ui.Stack1Rotate->isChecked()) ui.Stack2Rotate->setChecked(true);
  else if(ui.Stack2Rotate->isChecked()) ui.CutSurfRotate->setChecked(true);
  else if(ui.CutSurfRotate->isChecked()) ui.Clip1Rotate->setChecked(true);
}

void MorphoDynamX::meshSelectAll()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Select All", parms)) {
    QMessageBox::critical(this, "Error select all", "There is no Mesh process named 'Select All'");
    return;
  }
  launchProcess("Mesh/Selection/Select All", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::meshClearSelection()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Clear Selection", parms)) {
    QMessageBox::critical(this, "Error unselect", "There is no Mesh process named 'Clear Selection'");
    return;
  }
  launchProcess("Mesh/Selection/Clear Selection", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::meshInvertSelection()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Invert Selection", parms)) {
    QMessageBox::critical(this, "Error invert selection", "There is no Mesh process named 'Invert Selection'");
    return;
  }
  launchProcess("Mesh/Selection/Invert Selection", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::meshCopySelection()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Copy Selection", parms)) {
    QMessageBox::critical(this, "Error copy selection", "There is no Mesh process named 'Copy Selection'");
    return;
  }
  launchProcess("Mesh/Selection/Copy Selection", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::meshPasteSelection()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Paste Selection", parms)) {
    QMessageBox::critical(this, "Error paste selection", "There is no Mesh process named 'Paste Selection'");
    return;
  }
  launchProcess("Mesh/Selection/Paste Selection", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::meshAddCurrentLabel()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Select Label", parms)) {
    QMessageBox::critical(this, "Error add current label", "There is no Mesh process named 'Select Label'");
    return;
  }
  parms[0] = "No";
  parms[1] = QString::number(ui.Viewer->selectedLabel);
  launchProcess("Mesh/Selection/Select Label", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::meshAddUnlabeled()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Select Unlabeled", parms)) {
    QMessageBox::critical(this, "Error add unlabeled", "There is no Mesh process named 'Select Unlabeled'");
    return;
  }
  parms[0] = "No";
  launchProcess("Mesh/Selection/Select Unlabeled", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::meshRemoveCurrentLabel()
{
  QStringList parms;
  if(!getProcessParms("Mesh/Selection/Unselect Label", parms)) {
    QMessageBox::critical(this, "Error remove current label", "There is no Mesh process named 'Unselect Label'");
    return;
  }
  parms[0] = "0";
  launchProcess("Mesh/Selection/Unselect Label", parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::changeCurrentSeed()
{
  bool ok;
  int value = QInputDialog::getInt(this, "Change Current Seed", "New value for the seed:", ui.Viewer->selectedLabel, 0,
                                   65535, 1, &ok);
  if(ok)
    ui.Viewer->setLabel(value);
}

void MorphoDynamX::userManual()
{
  QString link = "file://" + docDir().canonicalPath() + "/MDXUserManual.pdf";
  #ifdef WIN32
    QDesktopServices::openUrl(QUrl::fromLocalFile(link));
  #else
    QDesktopServices::openUrl(QUrl(link));
  #endif
}

void MorphoDynamX::doxygen()
{
  QString link = "file://" + docDir().canonicalPath() + "/html/index.html";
  mdxInfo << "Doxygen link:" << link << endl;
  #ifdef WIN32
    QDesktopServices::openUrl(QUrl::fromLocalFile(link));
  #else
    QDesktopServices::openUrl(QUrl(link));
  #endif
}

void MorphoDynamX::qglViewerHelp() 
{
  ui.Viewer->aboutQGLViewer();
}

void MorphoDynamX::about()
{
  QString about(aboutText());
  QMessageBox dlg(tr("MorphoDynamX - A tool for computational morphodynamics!"), about, 
       QMessageBox::Information, QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton, this);
  // Set icon
  dlg.setIconPixmap(QPixmap(":/images/Icon.png"));
  dlg.setTextFormat(Qt::RichText);

  // Force minimum width
  QSpacerItem* horizontalSpacer = new QSpacerItem(600, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
  QGridLayout* layout = (QGridLayout*)dlg.layout();
  layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());

  dlg.exec();
}

void MorphoDynamX::labelColor()
{
  QIcon icon;
  ui.actionLabelColor->setIcon(icon);
}

void MorphoDynamX::setLabelColor(QIcon& icon)
{
  ui.actionLabelColor->setIcon(icon);
}

void MorphoDynamX::on_actionAddNewSeed_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_ADD_SEED;
}

void MorphoDynamX::on_actionAddCurrentSeed_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_CURR_SEED;
}

void MorphoDynamX::on_actionDrawSignal_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_DRAW_SIGNAL;
}

void MorphoDynamX::on_actionPickLabel_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_PICK_LABEL;
}

void MorphoDynamX::on_actionGrabSeed_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_GRAB_SEED;
}

void MorphoDynamX::on_actionFillLabel_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_FILL_LABEL;
}

void MorphoDynamX::on_actionPickFillLabel_triggered(bool on)
{
  if(on) {
    ui.Viewer->guiAction = MorphoViewer::MESH_PICK_FILL_LABEL;
    ui.Viewer->meshPickFill = true;
  }
}

void MorphoDynamX::on_actionRectSelectVertices_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_RECT_SEL_VERTICES;
  ImgData::MeshSelect = on;
}

void MorphoDynamX::on_actionRectSelectFaces_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_RECT_SEL_FACES;
  ImgData::MeshSelect = on;
}

void MorphoDynamX::on_actionRectSelectVolumes_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_RECT_SEL_VOLUMES;
  ImgData::MeshSelect = on;
}

void MorphoDynamX::on_actionLassoSelectVertices_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_LASSO_SEL_VERTICES;
  ImgData::MeshSelect = on;
}

void MorphoDynamX::on_actionLassoSelectFaces_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_LASSO_SEL_FACES;
  ImgData::MeshSelect = on;
}

void MorphoDynamX::on_actionLassoSelectVolumes_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_LASSO_SEL_VOLUMES;
  ImgData::MeshSelect = on;
}

void MorphoDynamX::on_actionLabelSelect_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_SEL_LABEL;
}

void MorphoDynamX::on_actionConnectedSelect_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_SEL_CONN;
}

void MorphoDynamX::on_actionFaceSelect_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_SEL_FACE;
}

void MorphoDynamX::on_actionVolumeSelect_toggled(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::MESH_SEL_VOLUME;
}

void MorphoDynamX::on_actionVoxelEdit_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::STACK_VOXEL_EDIT;
}

void MorphoDynamX::on_actionPickVolumeLabel_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::STACK_PICK_LABEL;
}

void MorphoDynamX::on_actionFillVolumeLabel_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::STACK_FILL_LABEL;
}

void MorphoDynamX::on_actionPickFillVolumeLabel_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::STACK_PICK_FILL_LABEL;
}

void MorphoDynamX::on_actionDeleteVolumeLabel_triggered(bool on)
{
  if(on)
    ui.Viewer->guiAction = MorphoViewer::STACK_DEL_LABEL;
}

void MorphoDynamX::on_actionRewind_triggered(bool on)
{
  // RSS This reloads the dll, maybe not so useful
  //ProcessDefinition* def = getProcessDefinition(currentProcessName);
  //if(def and !ui.Viewer->processRunning())
  //  def->factory->reload();
  if(ui.Viewer->processRunning()) {
    progressCancel();
    return;
  }

  if(currentProcessName.isEmpty()) {
    QMessageBox::warning(this, 
        "Process Control", "Please select a process before trying to run it");
    return;
  }

  ui.ProcessParameters->setFocus();
  QStringList parms = parmsModel->parms();
  launchProcess(currentProcessName, parms, Process::PROCESS_REWIND);
}

void MorphoDynamX::on_actionStop_triggered(bool on)
{
  progressCancel();
}

void MorphoDynamX::on_actionStep_triggered(bool on)
{
  if(ui.Viewer->processRunning()) {
    progressCancel();
    return;
  }

  if(currentProcessName.isEmpty()) {
//    QMessageBox::warning(this, 
//        "Process Control", "Please select a process before trying to run it");
    return;
  }
  ui.ProcessParameters->setFocus();
  QStringList parms = parmsModel->parms();
  launchProcess(currentProcessName, parms, Process::PROCESS_STEP);
}

void MorphoDynamX::on_actionRun_triggered(bool on)
{
  if(ui.Viewer->processRunning())
    return;

  if(currentProcessName.isEmpty()) {
// RSS Double click triggers run, but this often happens when a group is opened, not an error.
//
//    QMessageBox::warning(this, 
//        "Process Control", "Please select a process before trying to run it");
    return;
  }
  ui.ProcessParameters->setFocus();
  QStringList parms = parmsModel->parms();
  launchProcess(currentProcessName, parms, Process::PROCESS_RUN);
}

void MorphoDynamX::eraseSelect()
{
  QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
  ImgData &stk = (activeMesh() == 0 ? stack1 : stack2);
  Mesh *mesh = stk.mesh;
  QString ccName = mesh->ccName();
  if(ccName.isEmpty())
    return;
  mesh->fillSelect(ccName, 0);
  QApplication::restoreOverrideCursor();
}

void MorphoDynamX::fillSelect()
{
  QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
  ImgData &stk = (activeMesh() == 0 ? stack1 : stack2);
  Mesh *mesh = stk.mesh;
  QString ccName = mesh->ccName();
  if(ccName.isEmpty())
    return;
  mesh->fillSelect(ccName, ui.Viewer->selectedLabel);
  QApplication::restoreOverrideCursor();
}

void MorphoDynamX::deleteSelection()
{
  ImgData &stk = (activeMesh() == 0 ? stack1 : stack2);
  Mesh *mesh = stk.mesh;
  QString ccName = mesh->ccName();
  if(ccName.isEmpty())
    return;
  QString deleteProcess = mesh->ccAttr(ccName, "DeleteProcess");
  if(deleteProcess.isEmpty())
    return;
  QStringList parms;
  if(!getProcessParms(deleteProcess, parms)) {
    QMessageBox::critical(this, "Error deleting selection",
                    QString("Cannot find process %1").arg(deleteProcess));
    return;
  }
  launchProcess(deleteProcess, parms, Process::PROCESS_RUN, true);
}

void MorphoDynamX::colorPalette()
{
  ColorEditorDlg dlg(Colors::instance(), this);
  dlg.exec();
}

void MorphoDynamX::labelsEdit(bool on)
{
  if(labelEditorDlg.isNull()) {
    labelEditorDlg = new LabelEditorDlg(&ImgData::LabelColors, this);
    labelEditorDlg->setCurrentLabel(ui.Viewer->selectedLabel);
    connect(ui.Viewer, SIGNAL(selectLabelChanged(int)), labelEditorDlg, SLOT(setCurrentLabel(int)));
    connect(labelEditorDlg, SIGNAL(update()), ui.Viewer, SLOT(updateLabels()));
    connect(labelEditorDlg, SIGNAL(finished(int)), ui.actionEditLabels, SLOT(toggle()));
    connect(labelEditorDlg, SIGNAL(selectLabel(int, int, bool)), ui.Viewer, SLOT(selectMeshLabel(int, int, bool)));
    connect(labelEditorDlg, SIGNAL(makeLabelCurrent(int)), ui.Viewer, SLOT(setLabel(int)));
  }
  labelEditorDlg->setVisible(on);
}

void MorphoDynamX::swapSurfaces()
{
  // RSS How to do with CCs? 
  ui.Viewer->updateAll();
}

void MorphoDynamX::editParms()
{
  if(projectFile().isEmpty()) {
    QMessageBox::information(
      this, "Cannot edit parameters",
      "To edit the parameter file, you first need to create one. Save the current parameters in File->Save");
    return;
  }
  writeParms();
  QFile file(projectFile());
  if(file.open(QIODevice::ReadOnly)) {
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    editParmsDialog->Parms->setPlainText(stream.readAll());
  }
  file.close();

  if(editParmsDlog->exec() == QDialog::Accepted) {
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
      mdxWarning << "EditParms::Error:Cannot open output file:" << projectFile() << endl;
      return;
    }
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    stream << editParmsDialog->Parms->toPlainText();
    file.close();
    // editParmsDialog->Parms->setModified(false);
    setStatus(QString("Saved parameters to file: %1").arg(projectFile()));
  }
  saveProcessesParameters();
  readParms();
  loadControls();
  recallProcessesParameters();
  reloadTasks();
  ui.Viewer->updateAll();
}

void MorphoDynamX::editParmsSaveAs()
{
  // Be sure to grab current process parameters
  // processCommandGetParmValues(ImgData::processCommandIndex);

  // Write to file
  QString filename
    = QFileDialog::getSaveFileName(this, QString("Select file to save parmaters to "), projectFile(),
                                   QString("Parameter files *.mdxv"), 0, QFileDialog::DontUseNativeDialog);
  if(filename == "")
    return;

  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    mdxWarning << "MorphoDynamX::editParmsSaveAs Error:Cannot open output file:" << filename << endl;
    return;
  }
  QTextStream stream(&file);
  stream << editParmsDialog->Parms->toPlainText();
  file.close();
  setStatus(QString("Saved parameters to file: %1").arg(projectFile()));
}

// Display automatically generated documentation for processes, takes from tooltips
void docEntry(const ProcessDefinition& def, QTextStream& doc)
{
  doc << "<frame><b>" << def.name << "</b>: " << def.description;
  if(def.parmNames.size() > 0) {
    doc << "<br><b>Parameters</b>";
    doc << "<table width='100%' border=1 cell-spacing='1' style='border-width: 1px;border-style: solid'>";
    for(int i = 0; i < def.parmNames.size(); ++i) {
      doc << "<tr><td width='20%'>" << def.parmNames[i] << "</td>";
      if(i < def.parmDescs.size())
        doc << "<td>" << def.parmDescs[i] << "</td>";
      doc << "</tr>";
    }
  }
  doc << "</table></frame><br><br>";
}

void MorphoDynamX::processDocs()
{
  ProcessDocsDialog processDocsDlg(this);
  processDocsDlg.exec();
}

void MorphoDynamX::processCommandTab(int)
{
  QWidget *activeTab = ui.ProcessTabs->currentWidget();
  if(activeTab == ui.ProcessStack) {
    if(ui.ProcessStackCommand->topLevelItemCount() == 0)
      processCommand(0, 0);
    else
      processCommand(ui.ProcessStackCommand->currentItem(), 0);
  } else if(activeTab == ui.ProcessMesh) {
    if(ui.ProcessMeshCommand->topLevelItemCount() == 0)
      processCommand(0, 0);
    else
      processCommand(ui.ProcessMeshCommand->currentItem(), 0);
  } else if(activeTab == ui.ProcessTools) {
    if(ui.ProcessToolsCommand->topLevelItemCount() == 0)
      processCommand(0, 0);
    else
      processCommand(ui.ProcessToolsCommand->currentItem(), 0);
  } else if(activeTab == ui.ProcessTasks) {
    if(ui.ProcessTasksCommand->topLevelItemCount() == 0)
      processTasksCommand(0, 0);
    else
      processTasksCommand(ui.ProcessTasksCommand->currentItem(), 0);
  } else if(activeTab == ui.ProcessModel) {
    if(ui.ProcessModelCommand->topLevelItemCount() == 0)
      processCommand(0, 0);
    else
      processCommand(ui.ProcessModelCommand->currentItem(), 0);
  }
  ui.ProcessParameters->resizeColumnToContents(0);
}

void MorphoDynamX::updateActiveText()
{
  QWidget *activeTab = ui.StackTabs->currentWidget();
  currentSetup->setCurrentMeshId(-1);
  currentSetup->setCurrentStackId(-1);
  Stack* s1 = currentSetup->getStack(0);
  s1->setMainAsCurrent();
  Stack* s2 = currentSetup->getStack(1);
  s2->setMainAsCurrent();
  if(ui.Stack1WorkShow->isChecked())
    s1->setWorkAsCurrent();
  if(ui.Stack2WorkShow->isChecked())
    s2->setWorkAsCurrent();

  QString activeText;
  if(activeTab == ui.Stack1Tab) {
    currentSetup->setCurrentStackId(0);
    currentSetup->setCurrentMeshId(0);
    if(ui.Stack1WorkShow->isChecked())
      activeText = "Work Stack 1 active";
    else if(ui.Stack1MainShow->isChecked())
      activeText = "Main Stack 1 active";
    else 
      activeText = "Stack 1 active";

    Mesh *mesh = currentSetup->getMesh(0);
    if(mesh and not mesh->empty()) {
      QString ccName = mesh->ccName();
      if(!ccName.isEmpty()) {
        const CCStructure &cs = mesh->ccStructure(ccName);
        activeText += QString(" - %1 (%2D, ").arg(ccName).arg(cs.maxDimension());
        if(cs.cellCount() == 0)
          activeText += QString("no cells)");
        else
          activeText += QString("%1 cells)").arg(cs.cellCount());
      }
    }
  } else if(activeTab == ui.Stack2Tab) {
    currentSetup->setCurrentStackId(1);
    currentSetup->setCurrentMeshId(1);
    if(ui.Stack2WorkShow->isChecked())
      activeText = "Work Stack 2 active";
    else if(ui.Stack2MainShow->isChecked())
      activeText = "Main Stack 2 active";
    else
      activeText = "Stack 2 active";

    Mesh *mesh = currentSetup->getMesh(1);
    if(mesh and not mesh->empty()) {
      QString ccName = mesh->ccName();
      if(!ccName.isEmpty()) {
        const CCStructure &cs = mesh->ccStructure(ccName);
        activeText += QString(" - %1 (%2D, ").arg(ccName).arg(cs.maxDimension());
        if(cs.cellCount() == 0)
          activeText += QString("no cells)");
        else
          activeText += QString("%1 cells)").arg(cs.cellCount());
      }
    }
  } else
    activeText = "No Stack";
  ActiveStack->setText(activeText);
}

void MorphoDynamX::processCommand(QTreeWidgetItem* current, QTreeWidgetItem*)
{
  if(current == 0 or current->childCount() > 0) {
    parmsModel->clear();
    currentProcessName.clear();
  } else {
    currentProcessInTasks = false;
    QString name = current->text(1);
    currentProcessName = name;
    parmsModel->setParms(processes[name]);
  }
}

void MorphoDynamX::processTasksCommand(QTreeWidgetItem* current, QTreeWidgetItem*)
{
  if(current == 0) {
    parmsModel->clear();
    currentProcessName.clear();
  } else {
    bool found = false;
    if(current->flags() & Qt::ItemIsEnabled) {
      currentProcessName = current->text(0);
      currentProcessInTasks = true;
      QString task = current->data(0, TaskNameRole).toString();
      int proc_num = current->data(0, ProcessPositionRole).toInt();
      if(tasks.contains(task)) {
        const QList<ProcessDefinition>& defs = tasks[task];
        if(proc_num < defs.size()) {
          const ProcessDefinition& def = defs[proc_num];
          parmsModel->setParms(def);
          found = true;
        }
      }
    }
    if(!found) {
      parmsModel->clear();
      currentProcessName.clear();
    }
  }
}

void MorphoDynamX::seedStack(bool val)
{
  ImgData::SeedStack = val;
  ui.Viewer->updateAll();
}

bool MorphoDynamX::launchProcess(const QString &processName, QStringList &parms, Process::ProcessAction processAction, bool useGUI)
{
  if(processThread) {
    // QMessageBox::critical(this, QString("Error starting process %1").arg(processName),
    //      QString("The process '%1' is already running. Please wait for the process to finish " 
    //      "before starting another one").arg(currentProcess->name()));
    return false;
  }

  size_t expectedParms = size_t(-1);
  if(!checkProcessParms(processName, parms, &expectedParms)) {
    if(expectedParms == size_t(-1))
      QMessageBox::critical(this, "Error starting process",
         QString("There is no process named '%1'").arg(processName));
    else
      QMessageBox::critical(this, "Error starting process",
        QString("Expected %1 arguments for process %2, but %3 were provided")
                                                .arg(expectedParms).arg(processName).arg(parms.size()));
    return false;
  }

  updateActiveText();
  currentSetup->resetModified();

  currentProcess = currentSetup->getProcess(processName);
  if(not currentProcess) {
    QMessageBox::critical(this, "Error finding process",
                          QString("There is no process named '%1'").arg(processName));
    return false;
  }

  // Call the process initialize()
  try {
// FIXME There seems to be an intermitant seg fault calling initialize sometimes
DebugFile;
    currentProcess->setParms(parms);
DebugFile; // RSS Had once crash with this as last line 
    if(not currentProcess->initialize(useGUI ? this : 0))
      return false;
DebugFile;
    bool valid = checkProcessParms(processName, parms);
DebugFile;
    if(not valid) {
      QMessageBox::critical(this, "Error during process initialization", "Parameters invalid");
      return false;
    }
DebugFile;
    // Do rewind in Gui thread
    if(processAction == Process::PROCESS_REWIND) {
      bool status = currentProcess->rewind(useGUI ? this : 0);
      updateStateFromProcess(currentProcess, true);
      ui.Viewer->updateAll();
      currentProcess = 0;
      progressStop();
      return status;
    }
DebugFile;

  } catch(const QString &str) {
    QMessageBox::critical(this, "Error during process initialization: ", str);
    return false;
  } catch(const std::exception &ex) {
    QMessageBox::critical(this, "Error during process initialization: ", QString::fromLocal8Bit(ex.what()));
    return false;
  } catch(...) {
    QMessageBox::critical(this, "Error during process initialization", "Unknown exception");
    return false;
  }
DebugFile;
  setStatus(QString());

  // Save the current command in MorphoDynamX.py, but only if it already exists
  if(QFile::exists("MorphoDynamX.py")) {
    QFile file("MorphoDynamX.py");
    if(file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
      QTextStream ss(&file);
      currentProcess->p->currentPythonCall = currentProcess->pythonCall(parms);
      ss << currentProcess->p->currentPythonCall << endl;
      file.close();
    }
  }
  progressStart(QString("Running process %1").arg(processName), 0, false);

  processThread = new ProcessThread(currentProcess, processAction, this);
  // Connect thread finish slots
  connect(processThread, SIGNAL(finished()), this, SLOT(processCommandFinished()));

  currentProcessTime.start();
  //ui.ControlsTab->setDisabled(true); // RSS Is it dangerous to leave this open?
  ui.menubar->setDisabled(true);
  ui.volumeToolbar->setDisabled(true);
  ui.miscToolbar->setDisabled(true);
  ui.meshToolbar->setDisabled(true);
  ui.actionRewind->setDisabled(true);
  ui.actionRun->setDisabled(true);
  ui.actionStep->setDisabled(true);
  ui.Viewer->processRunning(true);

  ui.Stack1SurfLabeling->setDisabled(true);
  ui.Stack2SurfLabeling->setDisabled(true);
  ui.Stack1SurfHeat->setDisabled(true);
  ui.Stack2SurfHeat->setDisabled(true);
  ui.Stack1SurfSignal->setDisabled(true);
  ui.Stack2SurfSignal->setDisabled(true);

  processThread->start();
DebugFile;
  return true;
}

void MorphoDynamX::processCommandFinished()
{
  QString name = currentProcess->name();
  name.replace(" ", "_");
  int msec = currentProcessTime.elapsed();
  mdxInfo << name << " Process running time: " << (msec / 1000.0) << " s." << endl;

  // Set the error message in case one was thrown so that finalize can use it
  if(!processThread->errorMessage().isEmpty())
    currentProcess->setErrorMessage(processThread->errorMessage());

  // Call the process finalize()
  bool useGUI = true;
  bool success = true;
  try {
    success = currentProcess->finalize(useGUI ? this : 0);
    if(!success)
      QMessageBox::critical(this, "Error during process finalize: ", "Process returned error status");
  } catch(const QString& str) {
    QMessageBox::critical(this, "Error during process finalize: ", str);
    success = false;
  } catch(const std::exception& ex) {
    QMessageBox::critical(this, "Error during process finalize: ", QString::fromLocal8Bit(ex.what()));
    success = false;
  } catch(...) {
    QMessageBox::critical(this, "Error during process finalize: ", "Unknown exception");
    success = false;
  }

  //ui.ControlsTab->setEnabled(true);
  ui.menubar->setEnabled(true);
  ui.volumeToolbar->setEnabled(true);
  ui.miscToolbar->setEnabled(true);
  ui.meshToolbar->setEnabled(true);
  ui.actionRewind->setEnabled(true);
  ui.actionRun->setEnabled(true);
  ui.actionStep->setEnabled(true);

  ui.Stack1SurfLabeling->setEnabled(true);
  ui.Stack2SurfLabeling->setEnabled(true);
  ui.Stack1SurfHeat->setEnabled(true);
  ui.Stack2SurfHeat->setEnabled(true);
  ui.Stack1SurfSignal->setEnabled(true);
  ui.Stack2SurfSignal->setEnabled(true);

  ui.Viewer->processRunning(false);

  // User canceling is not error
  if(!processThread->errorMessage().isEmpty() and processThread->errorMessage() != "UserCancel") { 
    QMessageBox::critical(this, QString("Error running process %1").arg(currentProcess->name()), processThread->errorMessage());
    success = false;
  }

  // If a process partially completes, we still may need to update things.
  updateStateFromProcess(currentProcess, true);
  ui.Viewer->updateAll();
  processThread->deleteLater();
  processThread = 0;
  currentProcess = 0;
  progressStop();
  currentSetup->p->currentPythonCall.clear();

  if(success)
    emit processFinished();
}

bool MorphoDynamX::setFrameVis(const QString &name, ulong iFlags, const Point3d &pos, const Quaternion &orient)
{
  if(name.isEmpty()) {
    mdxInfo << "Empty frame name passed to setFrameVis" << endl;
    return false;
  }
  if(!(pos == pos)) {
    mdxInfo << "Bad position passed to setFrameVis" << endl;
    return false;
  }
  if(!(orient == orient)) {
    mdxInfo << "Bad orientation passed to setFrameVis" << endl;
    return false;
  }

  VisFlags flags;
  flags.iFlags = iFlags;
  qglviewer::ManipulatedFrame *frame = 0;

  if(name == "Clip1") {
    bool enabled = flags.clipFlags.enabled;
    if(enabled != currentSetup->clip1()->enabled()) {
      if(enabled)
        currentSetup->clip1()->enable();
      else
        currentSetup->clip1()->disable();
      ui.Clip1Enable->setChecked(enabled);
    }
    bool grid = flags.clipFlags.grid;
    if(grid != currentSetup->clip1()->grid()) {
      if(grid)
        currentSetup->clip1()->showGrid();
      else
        currentSetup->clip1()->hideGrid();
      ui.Clip1Grid->setChecked(grid);
    }
    frame = &currentSetup->clip1()->frame();
  } else if(name == "Clip2") {
    bool enabled = flags.clipFlags.enabled;
    if(enabled != currentSetup->clip2()->enabled()) {
      if(enabled)
        currentSetup->clip2()->enable();
      else
        currentSetup->clip2()->disable();
      ui.Clip2Enable->setChecked(enabled);
    }
    bool grid = flags.clipFlags.grid;
    if(grid != currentSetup->clip2()->grid()) {
      if(grid)
        currentSetup->clip2()->showGrid();
      else
        currentSetup->clip2()->hideGrid();
      ui.Clip2Grid->setChecked(grid);
    }
    frame = &currentSetup->clip2()->frame();
  } else if(name == "Clip3") {
    bool enabled = flags.clipFlags.enabled;
    if(enabled != currentSetup->clip3()->enabled()) {
      if(enabled)
        currentSetup->clip3()->enable();
      else
        currentSetup->clip3()->disable();
      ui.Clip3Enable->setChecked(enabled);
    }
    bool grid = flags.clipFlags.grid;
    if(grid != currentSetup->clip3()->grid()) {
      if(grid)
        currentSetup->clip3()->showGrid();
      else
        currentSetup->clip3()->hideGrid();
      ui.Clip3Grid->setChecked(grid);
    }
    frame = &currentSetup->clip3()->frame();
  } else if(name == "CutSurf") {
    bool enabled = flags.cutSurfFlags.enabled;
    if(enabled != currentSetup->cuttingSurface()->isVisible()) {
      if(enabled)
        currentSetup->cuttingSurface()->show();
      else
        currentSetup->cuttingSurface()->hide();
      ui.DrawCutSurf->setChecked(enabled);
    }
    bool grid = flags.cutSurfFlags.grid;
    if(grid != currentSetup->cuttingSurface()->drawGrid()) {
      if(grid)
        currentSetup->cuttingSurface()->showGrid();
      else
        currentSetup->cuttingSurface()->hideGrid();
      ui.CutSurfGrid->setChecked(grid);
    }
    frame = &currentSetup->cuttingSurface()->frame();
  } else if(name == "Stack1") {
    bool main = flags.stackFlags.main;
    if(main != currentSetup->getStack(0)->main()->isVisible()) {
      if(main)
        currentSetup->getStack(0)->main()->show();
      else
        currentSetup->getStack(0)->main()->hide();
      ui.Stack1MainShow->setChecked(main);
    }
    bool work = flags.stackFlags.work;
    if(work != currentSetup->getStack(0)->work()->isVisible()) {
      if(work)
        currentSetup->getStack(0)->work()->show();
      else
        currentSetup->getStack(0)->work()->hide();
      ui.Stack1WorkShow->setChecked(work);
    }
    frame = &currentSetup->getStack(0)->frame();
    } else if(name == "Stack2") {
    bool main = flags.stackFlags.main;
    if(main != currentSetup->getStack(1)->main()->isVisible()) {
      if(main)
        currentSetup->getStack(1)->main()->show();
      else
        currentSetup->getStack(1)->main()->hide();
      ui.Stack1MainShow->setChecked(main);
    }
    bool work = flags.stackFlags.work;
    if(work != currentSetup->getStack(1)->work()->isVisible()) {
      if(work)
        currentSetup->getStack(1)->work()->show();
      else
        currentSetup->getStack(1)->work()->hide();
      ui.Stack1WorkShow->setChecked(work);
    }
    frame = &currentSetup->getStack(1)->frame();
  }
  if(!frame)
    return false;

  // Set position and orientation
  if(pos != Point3d(frame->position()) or orient != Point4d(frame->orientation()))
    frame->setPositionAndOrientation(qglviewer::Vec(pos), qglviewer::Quaternion(orient.x(), orient.y(), orient.z(), orient.w()));

  return true;
}

bool MorphoDynamX::setCameraVis(const Point3d &pos, const Quaternion &orient, const Point3d &center, double zoom)
{
  MDXCamera *camera = dynamic_cast<MDXCamera *>(ui.Viewer->camera());
  if(!camera) {
    mdxInfo << "Bad camera passed to setCameraVis" << endl;
    return false;
  }
  if(!(pos == pos)) {
    mdxInfo << "Bad position passed to setCameraVis" << endl;
    return false;
  }
  if(!(orient == orient)) {
    mdxInfo << "Bad orientation passed to setCameraVis" << endl;
    return false;
  }
  if(!(center == center)) {
    mdxInfo << "Bad center passed to setCameraVis" << endl;
    return false;
  }
  if(!(zoom == zoom)) {
    mdxInfo << "Bad zoom passed to setCameraVis" << endl;
    return false;
  }

  if(center != Point3d(camera->sceneCenter()))
    camera->setSceneCenter(qglviewer::Vec(center));
  if(zoom != camera->zoom())
    camera->setZoom(zoom);

  if(pos != Point3d(camera->frame()->position()) or orient != Point4d(camera->frame()->orientation()))
    camera->frame()->setPositionAndOrientation(qglviewer::Vec(pos), qglviewer::Quaternion(orient.x(), orient.y(), orient.z(), orient.w()));

  return true;
} 

void MorphoDynamX::systemCommand(mdx::Process *proc, int command, QStringList parms)
{
  if(command == UPDATE_VIEWER) {
    updateViewer();
  } else if(command == UPDATE_STATE) {
    if(!proc)
      mdxInfo << "Bad parameters passed to Update State" << endl;
    else
      updateStateFromProcess(proc);
  } else if(command == SET_STATUS) {
    if(parms.size() < 2)
      mdxInfo << "Bad parameters passed to Set Status" << endl;
    else
      setStatus(parms[0], stringToBool(parms[1]));
  } else if(command == LOAD_VIEW) {
    if(!proc or parms.size() < 1)
      mdxInfo << "Bad parameters passed to Load View" << endl;
    else
      loadView(proc, parms[0]);
  } else if(command == SAVE_VIEW) {
    if(!proc or parms.size() < 1)
      mdxInfo << "Bad parameters passed to Save View" << endl;
    else
      saveView(proc, parms[0]);
  } else if(command == SET_CURRENT_STACK) {
    if(!proc or parms.size() < 2)
      mdxInfo << "Bad parameters passed to Set Current Stack" << endl;
    else
      setCurrentStack(proc, stringToBool(parms[0]), parms[1].toInt());
  } else if(command == TAKE_SNAPSHOT) {
    if(!proc or parms.size() < 6)
      mdxInfo << "Bad parameters passed to Take Snapshot" << endl;
    else
      takeSnapshot(proc, parms[0], parms[1].toFloat(), parms[2].toInt(), parms[3].toInt(), 
        parms[4].toInt(), stringToBool(parms[5]));
  } else if(command == SET_FRAME_VIS) {
    if(!proc or parms.size() < 4)
      mdxInfo << "Bad parameters passed to Set Frame Vis" << endl;
    else
      setFrameVis(parms[0], parms[1].toULong(), stringToPoint3d(parms[2]), stringToPoint4d(parms[3]));
  } else if(command == SET_CAMERA_VIS) {
    if(!proc or parms.size() < 4)
      mdxInfo << "Bad parameters passed to Set Camera Vis" << endl;
    else
      setCameraVis(stringToPoint3d(parms[0]), stringToPoint4d(parms[1]), stringToPoint3d(parms[2]), parms[3].toDouble());
  }
}

void MorphoDynamX::updateViewer()
{
  ui.Viewer->updateAll();
}

void MorphoDynamX::loadView(Process *proc, QString fileName)
{
  setProjectFile(fileName);

  // Save the process parameters
  saveProcessesParameters();

  // Load the project
  readParms();

  recallProcessesParameters();
  loadControls();
  reloadTasks();

  ui.Viewer->updateAll();
}

void MorphoDynamX::saveView(Process *proc, QString fileName)
{
  setProjectFile(fileName);
  saveSettings();

  updateStateFromProcess(proc, true);

  // Save the process parameters
  saveProcessesParameters();

  // Load the project
  writeParms();

  ui.Viewer->updateAll();
}

void MorphoDynamX::setCurrentStack(mdx::Process *proc, bool isMain, int id)
{
  if(id < ui.StackTabs->count()) {
    proc->p->current_stack = id;
    proc->p->current_mesh = id;
    if(isMain)
      proc->p->_stacks[id]->setMainAsCurrent();
    else
      proc->p->_stacks[id]->setWorkAsCurrent();
    ui.StackTabs->setCurrentIndex(id);
    if(isMain) {
      proc->p->_stacks[id]->main()->show();
      proc->p->_stacks[id]->work()->hide();
    } else
      proc->p->_stacks[id]->work()->show();
    switch(id) {
    case 0:
      if(isMain) {
        ui.Stack1MainShow->setChecked(true);
        ui.Stack1WorkShow->setChecked(false);
      } else {
        ui.Stack1WorkShow->setChecked(true);
      }
      break;
    case 1:
      if(isMain) {
        ui.Stack2MainShow->setChecked(true);
        ui.Stack2WorkShow->setChecked(false);
      } else {
        ui.Stack2WorkShow->setChecked(true);
      }
      break;
    }
  }
}

void MorphoDynamX::takeSnapshot(mdx::Process *proc, QString fileName, 
        double overSampling, int width, int height, int quality, bool expandFrustum)
{
  updateStateFromProcess(proc, true);
  ui.Viewer->updateAll();
  // Force event processing
  QCoreApplication::flush();

  if(width == 0)
    width = ui.Viewer->width();
  if(height == 0)
    height = ui.Viewer->height();

  QString error;
  proc->p->success = ui.Viewer->saveImageSnapshot(fileName, QSize(width, height), overSampling, expandFrustum, false, &error);
  if(!proc->p->success)
    proc->setErrorMessage(error);
}

void MorphoDynamX::setStatus(QString text, bool alsoPrint)
{
  emit updateStatusBar(text);
  if(alsoPrint && text.size() > 0) mdxInfo << text << endl;
}

void MorphoDynamX::updateStateFromProcess(Process* proc, bool finishing)
{
  // Wait if process still running
  QMutexLocker locker(&proc->p->lock);

  // First, check the stores and stacks
  Store* mainStore1 = 0, *mainStore2 = 0, *workStore1 = 0, *workStore2 = 0;
  Mesh* mesh1 = 0, *mesh2 = 0;
  Stack* _stack1 = 0, *_stack2 = 0;
  for(int i = 0; i < proc->stackCount(); ++i) {
    Stack* stack = proc->getStack(i);
    if(stack->id() == 0) {
      _stack1 = stack;
      mainStore1 = stack->main();
      workStore1 = stack->work();
    } else if(stack->id() == 1) {
      _stack2 = stack;
      mainStore2 = stack->main();
      workStore2 = stack->work();
    }
  }
  for(int i = 0; i < proc->meshCount(); ++i) {
    Mesh* mesh = proc->getMesh(i);
    if(mesh->id() == 0)
      mesh1 = mesh;
    else if(mesh->id() == 1)
      mesh2 = mesh;
  }
  if(_stack1) {
    stack1.stack = _stack1;
    stack1.updateStackSize();
    ui.Stack1Scale_X->setValue(stack1.toSliderScale(_stack1->scale().x()));
    ui.Stack1Scale_Y->setValue(stack1.toSliderScale(_stack1->scale().y()));
    ui.Stack1Scale_Z->setValue(stack1.toSliderScale(_stack1->scale().z()));

    if(mainStore1->data().size() != _stack1->storeSize()) {
      mainStore1->reset();
      mainStore1->changed();
    }
    if(mainStore1->wasChanged())
      stack1.loadTex("Main", mainStore1->changedBBox());
    if(mainStore1->transferFunctionChanged())
      stack1.updateMainColorMap();

    if(workStore1->data().size() != _stack1->storeSize()) {
      setStatus("Warning, work store 1 is not of the right size: deleting");
      workStore1->reset();
      workStore1->changed();
    }
    if(workStore1->wasChanged())
      stack1.loadTex("Work", workStore1->changedBBox());
    if(workStore1->transferFunctionChanged())
      stack1.updateWorkColorMap();

    ui.Stack1MainShow->setChecked(mainStore1->isVisible());
    ui.Stack1WorkShow->setChecked(workStore1->isVisible());

    ui.Stack1ShowTrans->setChecked(_stack1->showTrans());
    ui.Stack1ShowBBox->setChecked(_stack1->showBBox());
    ui.Stack1ShowScale->setChecked(_stack1->showScale());
    ui.Stack1TieScales->setChecked(_stack1->tieScales());

    ui.Stack1MainLabels->setChecked(mainStore1->labels());
    ui.Stack1WorkLabels->setChecked(workStore1->labels());

    ui.Stack1MainBright->setValue(mainStore1->brightness() * 10000);
    ui.Stack1MainOpacity->setValue(mainStore1->opacity() * 10000);
    ui.Stack1WorkBright->setValue(workStore1->brightness() * 10000);
    ui.Stack1WorkOpacity->setValue(workStore1->opacity() * 10000);
  }

  if(_stack2) {
    stack2.stack = _stack2;
    stack2.updateStackSize();
    ui.Stack2Scale_X->setValue(stack2.toSliderScale(_stack2->scale().x()));
    ui.Stack2Scale_Y->setValue(stack1.toSliderScale(_stack2->scale().y()));
    ui.Stack2Scale_Z->setValue(stack1.toSliderScale(_stack2->scale().z()));

    if(mainStore2->data().size() != _stack2->storeSize()) {
      setStatus("Warning, main store 2 is not of the right size: deleting");
      mainStore2->reset();
      mainStore2->changed();
    }

    if(workStore2->data().size() != _stack2->storeSize()) {
      setStatus("Warning, work store 2 is not of the right size: deleting");
      workStore2->reset();
      workStore2->changed();
    }

    if(mainStore2->wasChanged())
      stack2.loadTex("Main", mainStore2->changedBBox());
    if(mainStore2->transferFunctionChanged())
      stack2.updateMainColorMap();

    if(workStore2->wasChanged())
      stack2.loadTex("Work", workStore2->changedBBox());
    if(workStore2->transferFunctionChanged())
      stack2.updateWorkColorMap();

    ui.Stack2MainShow->setChecked(mainStore2->isVisible());
    ui.Stack2WorkShow->setChecked(workStore2->isVisible());

    ui.Stack2ShowTrans->setChecked(_stack2->showTrans());
    ui.Stack2ShowBBox->setChecked(_stack2->showBBox());
    ui.Stack2ShowScale->setChecked(_stack2->showScale());
    ui.Stack2TieScales->setChecked(_stack2->tieScales());

    ui.Stack2MainLabels->setChecked(mainStore2->labels());
    ui.Stack2WorkLabels->setChecked(workStore2->labels());

    ui.Stack2MainBright->setValue(mainStore2->brightness() * 10000);
    ui.Stack2MainOpacity->setValue(mainStore2->opacity() * 10000);
    ui.Stack2WorkBright->setValue(workStore2->brightness() * 10000);
    ui.Stack2WorkOpacity->setValue(workStore2->opacity() * 10000);
  }

  // Then the meshes
  if(mesh1) {
		if(finishing)
      progressSetMsg("Updating Geometry");
    stack1.mesh = mesh1;

    if(mesh1->labeling().isEmpty())
      mesh1->setLabeling("Labels");

    // Disable controls that call updateCC
    ui.Stack1SurfBlend->blockSignals(true);
    ui.Stack1SurfLabeling->blockSignals(true);
    ui.Stack1SurfHeat->blockSignals(true);
    ui.Stack1SurfSignal->blockSignals(true);

    ui.Stack1SurfBlend->setChecked(mesh1->blending());
    ui.Stack1SurfLabeling->setCurrentText(mesh1->labeling());
    if(finishing) {
      loadHeatChoices(mesh1->id());
      loadSignalChoices(mesh1->id());
      loadLabelingChoices(mesh1->id());
    }

    ui.Stack1SurfBlend->blockSignals(false);
    ui.Stack1SurfLabeling->blockSignals(false);
    ui.Stack1SurfHeat->blockSignals(false);
    ui.Stack1SurfSignal->blockSignals(false);

    ui.Stack1SurfCull->setChecked(mesh1->culling());
    ui.Stack1SurfClip->setChecked(mesh1->cellClipping());
    ui.Stack1SurfShade->setChecked(mesh1->shading());
    ui.Stack1SurfBright->setValue(mesh1->brightness() * 10000);
    ui.Stack1SurfOpacity->setValue(mesh1->opacity() * 10000);

    // Recalculate normals and face geometry
    for(QString &ccName : mesh1->ccNames()) {
      CCDrawParms &cdp = mesh1->drawParms(ccName);
      if((cdp.changed & CCDrawParms::TopologyChanged) or (cdp.changed & CCDrawParms::PositionsChanged))
        mdx::updateGeometry(mesh1->ccStructure(ccName), mesh1->indexAttr());
    }
    mesh1->updateBBox();
  }

  if(mesh2) {
		if(finishing)
      progressSetMsg("Updating Geometry");
    stack2.mesh = mesh2;

    if(mesh2->labeling().isEmpty())
      mesh2->setLabeling("Labels");

    // Disable controls that call updateCC
    ui.Stack2SurfBlend->blockSignals(true);
    ui.Stack2SurfLabeling->blockSignals(true);
    ui.Stack2SurfHeat->blockSignals(true);
    ui.Stack2SurfSignal->blockSignals(true);

    ui.Stack2SurfBlend->setChecked(mesh2->blending());
    ui.Stack2SurfLabeling->setCurrentText(mesh2->labeling());
    if(finishing) {
      loadHeatChoices(mesh2->id());
      loadSignalChoices(mesh2->id());
      loadLabelingChoices(mesh2->id());
    }

    ui.Stack2SurfBlend->blockSignals(false);
    ui.Stack2SurfLabeling->blockSignals(false);
    ui.Stack2SurfHeat->blockSignals(false);
    ui.Stack2SurfSignal->blockSignals(false);

    ui.Stack2SurfCull->setChecked(mesh2->culling());
    ui.Stack2SurfClip->setChecked(mesh2->cellClipping());
    ui.Stack2SurfShade->setChecked(mesh2->shading());
    ui.Stack2SurfBright->setValue(mesh2->brightness() * 10000);
    ui.Stack2SurfOpacity->setValue(mesh2->opacity() * 10000);

    // Recalculate normals and face geometry
    for(QString &ccName : mesh2->ccNames()) {
      CCDrawParms &cdp = mesh2->drawParms(ccName);
      if((cdp.changed & CCDrawParms::TopologyChanged) or (cdp.changed & CCDrawParms::PositionsChanged))
        mdx::updateGeometry(mesh2->ccStructure(ccName), mesh2->indexAttr());
    }
    mesh2->updateBBox();
  }

  // Update from cutting surface
  CuttingSurface* cut = currentSetup->cuttingSurface();
  ui.DrawCutSurf->setChecked(cut->isVisible());
  switch(cut->mode()) {
  case CuttingSurface::THREE_AXIS:
    ui.ThreeAxis->setChecked(true);
    break;
  case CuttingSurface::PLANE:
    ui.CutSurfPlane->setChecked(true);
    break;
  case CuttingSurface::BEZIER:
    ui.CutSurfBezier->setChecked(true);
    break;
  }
  // Update the viewer scene
  ui.Viewer->updateSceneRadius();

  // Find the bounding box of all images
//  BoundingBox3d bbox(Point3d(FLT_MAX, FLT_MAX, FLT_MAX), Point3d(-FLT_MAX, -FLT_MAX, -FLT_MAX));
//  forall(Stack* stack, proc->stacks()) {
//    Point3d worldSize = multiply(Point3d(stack->size()), stack->step());
//    bbox |= worldSize;
//  }
//  forall(Mesh* mesh, proc->meshes()) {
//    if(!mesh->empty()) {
//      const BoundingBox3d& b = mesh->boundingBox();
//      bbox |= b;
//    }
//  }
//  Point3d rbox;
//  if(bbox[0].x() > bbox[1].x())
//    rbox = Point3d(0.f);
//  else
//    rbox = max(fabs(bbox[0]), fabs(bbox[1]));
//
//  ui.Viewer->setSceneBoundingBox(rbox);
//  cutSurf.setSceneBoundingBox(rbox);
  ui.CutSurfGrid->setChecked(cut->drawGrid());
  setCutSurfControl();

  // Update global brightness and contrast
  int intBright = int((proc->globalBrightness() + 1.0) * 5000.0);
  int intContrast = int((proc->globalContrast() * 5000.0));
  int intShininess = int((proc->globalShininess()/128.0 * 5000.0));
  int intSpecular = int((proc->globalSpecular() * 5000.0));

  ui.GlobalBrightness->setValue(intBright);
  ui.GlobalContrast->setValue(intContrast);
  ui.GlobalShininess->setValue(intShininess);
  ui.GlobalSpecular->setValue(intSpecular);

  resetClipControl(ui.Viewer->getSceneRadius());

  if(currentSetup->actingFile().isEmpty())
    setWindowTitle(QString("MorphoDynamX"));
  else
    setWindowTitle(QString("MorphoDynamX - ") + currentSetup->actingFile());

  currentSetup->resetModified();
  updateActiveText();
  ui.Viewer->setLabel(proc->selectedLabel());

  // Update the CC widgets
  updateCCData(mesh1->id());
  updateCCData(mesh2->id());

  // Re-write the parms
  ProcessDefinition* def = getProcessDefinition(proc->name());
  proc->setParms(def->parms);
}

void MorphoDynamX::setProjectFile(const QString &f)
{
  QString filename = f;
  if(!filename.isEmpty())
    filename = absoluteFilePath(filename);
  currentSetup->setFile(filename);
}

const QString& MorphoDynamX::projectFile() const { return currentSetup->file(); }

void MorphoDynamX::dragEnterEvent(QDragEnterEvent* event)
{
  if(event->keyboardModifiers() == Qt::AltModifier)
    event->setDropAction(Qt::MoveAction);
  else
    event->setDropAction(Qt::CopyAction);

  const QMimeData* mime = event->mimeData();
  if(mime->hasUrls()) {
    QList<QUrl> urls = mime->urls();
    if(urls.size() == 1) {
      QUrl url = urls[0];
      QFileInfo fi(url.toLocalFile());
      if(fi.exists() and fi.isReadable()) {
        event->accept();
        return;
      }
    }
  }
  event->ignore();
}

void MorphoDynamX::autoRun()
{
  disconnect(this, SIGNAL(processFinished()), this, SLOT(autoRun()));
  ui.actionRun->trigger();
}

void MorphoDynamX::autoOpen(const QString& filename, int stack, bool main)
{
  disconnect(this, SIGNAL(processFinished()), this, SLOT(autoOpen()));
  QString pth = (filename.isEmpty() ? _loadFilename : filename);
  QFileInfo fi(pth);
  QString ext = fi.suffix().toLower();
  if(ext == "mdxv" or ext == "mgxv") {
    fileOpen(pth);
  } else {
    QStringList parms;
    if(ext == "vtu") {
      parms << QString::number(stack) << pth << "Imported Vtu" << "No" << "No";
      launchProcess("Mesh/System/Import", parms, Process::PROCESS_RUN, false);
    } else if(ext == "mdxs" or ext == "mgxs" or ext == "inr" or ext == "tif" or ext == "tiff") {
      parms << pth << (main ? "Main" : "Work") << QString::number(stack);
      launchProcess("Stack/System/Open", parms, Process::PROCESS_RUN, false);
    } else if(ext == "jpg" or ext == "jpeg" or ext == "png" or ext == "bmp" or ext == "cimg") {
      parms << pth << (main ? "Main" : "Work") << QString::number(stack) << "Yes";
      launchProcess("Stack/System/Import", parms, Process::PROCESS_RUN, true);
    } else if(ext == "mdxm" or ext == "mgxm") {
      parms << pth << "no" << "no" << QString::number(stack);
      launchProcess("Mesh/System/Load", parms, Process::PROCESS_RUN, false);
    } else if(ext == "mesh") {
      parms << QString::number(stack) << pth << "Imported" <<  "No" << "No" ;
      launchProcess("Mesh/System/Import", parms, Process::PROCESS_RUN, false);
    } else if(ext == "ply") {
      parms << QString::number(stack) << pth << "Imported Ply" << "No" << "No";
      launchProcess("Mesh/System/Import", parms, Process::PROCESS_RUN, false);
    } else if(ext == "fct") {
      ui.Viewer->loadFile(pth, (stack == 1), not main);
    } else if(ext == "py") {
      parms << pth;
      launchProcess("Tools/Python/Python Script", parms, Process::PROCESS_RUN, false);
    } else if(ext == "h5" or ext == "hdf5" or ext == "hdf") {
      parms << pth << (main ? "Main" : "Work") << QString::number(stack) << "";
      launchProcess("Stack/System/Open HDF5", parms, Process::PROCESS_RUN, true);
    } else if(!ext.isEmpty()) { // If all else fails, call LOCI tools
      parms << pth << (main ? "Main" : "Work") << QString::number(stack);
      launchProcess("Stack/ITK/System/ITK Image Reader", parms, Process::PROCESS_RUN, true);
    }
  }
}

void MorphoDynamX::dropEvent(QDropEvent* event)
{
  QString pth = event->mimeData()->urls()[0].toLocalFile();

  int stack = 0;
  bool main = true;
  Qt::KeyboardModifiers mods = QApplication::queryKeyboardModifiers();
  if(mods & Qt::AltModifier)
    stack++;
  if(mods & Qt::ShiftModifier)
    main = false;

  autoOpen(pth, stack, main);
}

void MorphoDynamX::closeEvent(QCloseEvent* e)
{
  if(needSaving) {
    QMessageBox::StandardButton a = QMessageBox::question(
        this, "Closing with unsaved information",
        "You haven't saved the current state of MorphoDynamX. Do you want to quit without saving?",
        QMessageBox::No | QMessageBox::Yes);
    if(a == QMessageBox::No) {
      e->ignore();
      return;
    }
  }
  ui.Viewer->quitting = true;
  saveSettings();

  QApplication::quit();

//  // Unload the processes by hand
//
//  ui.ProcessStackCommand->clear();
//  ui.ProcessMeshCommand->clear();
//  ui.ProcessToolsCommand->clear();
//  ui.ProcessModelCommand->clear();
//
//  processes.clear();
//
//  forall(Library* l, loadedLibs) {
//    if(l->isLoaded()) {
//      if(DEBUG)
//        mdxInfo << "Unloading library '" << l->fileName() << "'" << endl;
//      l->unload();
//      if(l->isLoaded())
//        setStatus("Failed to unload library '" << l->fileName() << "': " << l->errorString());
//    }
//    delete l;
//  }
//  loadedLibs.clear();
//  unregisterSystemProcesses();
//
//  delete currentSetup;
//
//  QMainWindow::closeEvent(e);
}

void MorphoDynamX::saveSettings()
{
  QSettings settings;
  settings.beginGroup("MainWindow");
  settings.setValue("Geometry", saveGeometry());
  settings.setValue("WindowState", saveState(1));
  settings.endGroup();
}

void MorphoDynamX::stack1Unloaded() 
{
  ui.Stack1Size->setText("Not loaded");
}

void MorphoDynamX::stack2Unloaded() 
{
  ui.Stack2Size->setText("Not loaded");
}

void MorphoDynamX::changeStack1Size(const Point3u& size, const Point3d& step, const Point3d& )
{
  QString msg;
  if(size_t(size.x()) * size.y() * size.z() != 0) {
    msg = QString("%1x%2x%3    %4x%5x%6").arg(size.x()).arg(size.y()).arg(size.z()).arg(step.x()).arg(step.y()).arg(
        step.z());
    msg += UM;
    ui.Stack1Size->setText(msg);
  } else
    stack1Unloaded();
}

void MorphoDynamX::changeStack2Size(const Point3u& size, const Point3d& step, const Point3d& )
{
  QString msg;
  if(size_t(size.x()) * size.y() * size.z() != 0) {
    msg = QString("%1x%2x%3    %4x%5x%6").arg(size.x()).arg(size.y()).arg(size.z()).arg(step.x()).arg(step.y()).arg(
        step.z());
    msg += UM;
    ui.Stack2Size->setText(msg);
  } else
    stack2Unloaded();
}

void MorphoDynamX::modified(bool on)
{
  if(on != needSaving)
    needSaving = on;
}

QTreeWidgetItem* MorphoDynamX::getFolder(QString name, QHash<QString, QTreeWidgetItem*>& folders, QTreeWidget* tree)
{
  if(folders.contains(name))
    return folders[name];
  int idx = name.lastIndexOf('/');
  QTreeWidgetItem* item = 0;
  if(idx == -1) {
    item = new QTreeWidgetItem(QStringList() << name);
    item->setExpanded(false);
    item->setFlags(Qt::ItemIsEnabled);
    tree->addTopLevelItem(item);
  } else {
    QString newname = name.left(idx);
    QTreeWidgetItem* parent = getFolder(newname, folders, tree);
    QString fn = name.mid(idx + 1);
    item = new QTreeWidgetItem(QStringList() << fn);
    item->setExpanded(false);
    item->setFlags(Qt::ItemIsEnabled);
    parent->addChild(item);
  }
  folders[name] = item;
  return item;
}

// Load the processes into the tree view
void MorphoDynamX::createItem(const ProcessDefinition& def)
{
  // Extract the tab folder and name from the process name
  QString tab, folder, name;
  if(!getProcessText(def.name, tab, folder, name))
    return;

  QTreeWidget *tree;
  QHash<QString, QTreeWidgetItem*> *folders;
  if(tab == "Stack") {
    tree = ui.ProcessStackCommand;
    folders = &stackFolders;
  } else if(tab == "Mesh") {
    tree = ui.ProcessMeshCommand;
    folders = &meshFolders;
  } else if(tab == "Tools") {
    tree = ui.ProcessToolsCommand;
    folders = &toolsFolders;
  } else if(tab == "Model") {
    tree = ui.ProcessModelCommand;
    folders = &modelFolders;
  } else {
    mdxInfo 
        << QString("Create item cannot find correct tab (%1) for process (%2)").arg(tab).arg(name) << endl;
    return;
  }
    
  QStringList desc;
  // The full name goes in column 2
  desc << name << def.name;

  QTreeWidgetItem* item = new QTreeWidgetItem(desc);
  item->setToolTip(0, def.description);
  item->setIcon(0, def.icon);
  if(folder.isEmpty())
    tree->addTopLevelItem(item);
  else {
    // getFolder, recursively creates the tree
    QTreeWidgetItem* folderWidget = getFolder(folder, *folders, tree);
    folderWidget->addChild(item);
  }
}

void MorphoDynamX::updateDefinitions(QMap<QString, ProcessDefinition>& procDefs,
  ProcessFactoryPtr factory, const QMap<QString, ProcessDefinition>& oldProcDefs)
{
  Process* proc = (*factory)(*currentSetup);
  ProcessDefinition def;
  def.name = proc->name();
  def.name.replace('_', " ");
  def.description = proc->description();
  def.parmNames = proc->parmNames();
  def.parmDescs = proc->parmDescs();
  def.icon = proc->icon();
  ProcessDefinition oldDef = oldProcDefs[def.name];
  if(oldProcDefs.contains(def.name) and oldDef.parmNames.size() == def.parmNames.size())
    def.parms = oldDef.parms;
  else
    def.parms = proc->parmDefaults();
  def.parmChoice = proc->parmChoice();
  def.factory = factory;
  int defined = def.parmNames.size();
  int provided = proc->parmDefaults().size();
  int available = def.parms.size();
  if(defined != provided or defined != available)
    mdxWarning << "Error for process " << def.name
                     << ": parameters doesn't match the number of defaults. This process won't be used." << endl;
  else if(defined > available)
    mdxWarning << "Error for process " << def.name
                     << ": the number of defined parameters is greater than the number of parameters available."
       "This process won't be used." << endl;
  else {
    if(!def.factory)
      mdxWarning << "Error, creating a process with an empty factory" << endl;
    procDefs[def.name] = def;
  }
}

// Perform a deep copy of the strings, and don't copy the pointers
void duplicateDefinitions(const QMap<QString, ProcessDefinition>& procDefs,
                                QMap<QString, ProcessDefinition>& savedProcDefs)
{
  forall(const ProcessDefinition& def, procDefs) {
    ProcessDefinition newDef;
    newDef.name = def.name;
    newDef.parmNames = def.parmNames;
    newDef.parms = def.parms;
    savedProcDefs[newDef.name] = newDef;
  }
}

void MorphoDynamX::saveProcessesParameters()
{
  duplicateDefinitions(processes, savedProc);
}

void MorphoDynamX::recallProcessesParameters()
{
  int cnt = 0;
  for(processesMap_t::iterator it = processes.begin();
                                     it != processes.end(); ++it, ++cnt) {
    if(it->factory)
      updateDefinitions(processes, it->factory, savedProc);
    else if(DEBUG)
      mdxInfo << "Warning, process " << cnt << " (" << it->name << ") with empty factory" << endl;
  }

  ProcessDefinition* def = getProcessDefinition(currentProcessName);
  if(def)
    parmsModel->setParms(*def);
}

void MorphoDynamX::reloadProcesses()
{
#ifdef WATCH_PROCESS_FOLDERS
  if(currentProcess) {
    processReloadTimer->setSingleShot(true);
    processReloadTimer->start();
    return;
  }
#endif
  if(!currentProcessName.isEmpty()) {
    currentProcessName.clear();
    parmsModel->clear();
  }

  ui.ProcessStackCommand->clear();
  ui.ProcessMeshCommand->clear();
  ui.ProcessToolsCommand->clear();
  ui.ProcessModelCommand->clear();

  // Save the current state of things, duplicating everything to never
  // reference a constant of the dll
  saveProcessesParameters();

  processes.clear();

  //unregisterSystemProcesses();

  uint totalProcesses = processFactories().size();
  mdxInfo << "Base processes" << endl;
  mdxInfo << " processes: " << totalProcesses << endl << endl;

  mdxInfo << "Loading system processes" << endl;
  //registerSystemProcesses();
  uint procCount = processFactories().size() - totalProcesses;
  mdxInfo << " added " << procCount << " processes, total: " << processFactories().size() << endl;
  totalProcesses = processFactories().size();

  // setStatus("Unloading libraries");
  // First, unload loaded libraries
  forall(Library* l, loadedLibs) {
    if(l->isLoaded())
      l->unload();
    delete l;
  }
  loadedLibs.clear();

  // Then, find and load libraries
  QList<QDir> dirs = processDirs();
  forall(const QDir& dir, dirs) {
    mdxInfo << "\nLoading libraries from '" << dir.absolutePath() << "'" << endl;
    QStringList files = dir.entryList(QDir::Files);
    files.sort();
#ifdef WATCH_PROCESS_FOLDERS
    QStringList watched = processWatcher->files();
#endif
    forall(const QString& f, files) {
      QString lib_path = dir.absoluteFilePath(f);
      if(Library::isLibrary(lib_path)) {
        Library* lib = new Library(lib_path);
        lib->load();
        if(lib->isLoaded()) {
#ifdef WATCH_PROCESS_FOLDERS
          if(!watched.contains(lib_path))
            processWatcher->addPath(lib_path);
#endif
          loadedLibs << lib;
          mdxInfo << "Loaded library " << f << endl;
          procCount = processFactories().size() - totalProcesses;
          mdxInfo << " added " << procCount << " processes, total: " << processFactories().size() << endl;
          totalProcesses = processFactories().size();
        } else {
          mdxWarning << "Failed to load library file " << f << ": \n" << lib->errorString() << endl;
          delete lib;
        }
      }
    }
  }

  if(addedLibFiles.size() > 0) mdxInfo << endl;
  forall(const QFileInfo& qf, addedLibFiles) {
    QString lib_path = qf.absoluteFilePath();
    if(Library::isLibrary(lib_path)) {
      Library* lib = new Library(lib_path);
      lib->load();
      if(lib->isLoaded()) {
        loadedLibs << lib;
        mdxInfo << "Loaded library " << qf.fileName() << endl;
        procCount = processFactories().size() - totalProcesses;
        mdxInfo << " added " << procCount << " processes, total: " << processFactories().size() << endl;
        totalProcesses = processFactories().size();
      } else {
        mdxWarning << "Failed to load library file " << qf.fileName() << ": \n" << lib->errorString() << endl;
        delete lib;
      }
    }
  }

  /*
     setStatus("Now watching:");
     watched = processWatcher->files();
     forall(const QString& s, watched)
     setStatus("  file: " << s);
     watched = processWatcher->directories();
     forall(const QString& s, watched)
     setStatus("  path: " << s);
   */

  // then create processes
  forall(ProcessFactoryPtr factory, processFactories())
    updateDefinitions(processes, factory, savedProc);

  // list of folders

  //setStatus("Generated " << processes.size() << " processes from "
  //                       << processFactories().size() << " registered");
  QString msg = QString("Created %1 processes").arg(processes.size());
  if(processes.size() < processFactories().size())
    msg += QString(", Warning, %1 registered").arg(processFactories().size());
  setStatus(msg);

  stackFolders.clear();
  meshFolders.clear();
  toolsFolders.clear();
  modelFolders.clear();
  forall(const ProcessDefinition& def, processes)
    createItem(def);

  ui.ProcessStackCommand->sortItems(0, Qt::AscendingOrder);
  ui.ProcessStackCommand->resizeColumnToContents(0);
  QTreeWidgetItem* item = ui.ProcessStackCommand->topLevelItem(0);
  if(item)
    ui.ProcessStackCommand->setCurrentItem(item);
  if(ui.ProcessStackCommand->topLevelItemCount() > 0 and ui.ProcessStackCommand->topLevelItem(0)->childCount() == 0)
    processCommand(ui.ProcessStackCommand->topLevelItem(0), 0);

  ui.ProcessMeshCommand->sortItems(0, Qt::AscendingOrder);
  ui.ProcessMeshCommand->resizeColumnToContents(0);
  item = ui.ProcessMeshCommand->topLevelItem(0);
  if(item)
    ui.ProcessMeshCommand->setCurrentItem(item);
  if(ui.ProcessMeshCommand->topLevelItemCount() > 0 and ui.ProcessMeshCommand->topLevelItem(0)->childCount() == 0)
    processCommand(ui.ProcessMeshCommand->topLevelItem(0), 0);

  ui.ProcessToolsCommand->sortItems(0, Qt::AscendingOrder);
  ui.ProcessToolsCommand->resizeColumnToContents(0);
  item = ui.ProcessToolsCommand->topLevelItem(0);
  if(item)
    ui.ProcessToolsCommand->setCurrentItem(item);
  if(ui.ProcessToolsCommand->topLevelItemCount() > 0 and ui.ProcessToolsCommand->topLevelItem(0)->childCount() == 0)
    processCommand(ui.ProcessToolsCommand->topLevelItem(0), 0);

  ui.ProcessModelCommand->sortItems(0, Qt::AscendingOrder);
  ui.ProcessModelCommand->resizeColumnToContents(0);
  item = ui.ProcessModelCommand->topLevelItem(0);
  if(item)
    ui.ProcessModelCommand->setCurrentItem(item);
  if(ui.ProcessModelCommand->topLevelItemCount() > 0 and ui.ProcessModelCommand->topLevelItem(0)->childCount() == 0)
    processCommand(ui.ProcessModelCommand->topLevelItem(0), 0);
  reloadTasks();
}

void MorphoDynamX::storeParameters()
{
  if(currentProcessName.isEmpty())
    return;
  ProcessDefinition* def = 0;
  if(currentProcessInTasks) {
    QTreeWidgetItem* current = ui.ProcessTasksCommand->currentItem();
    if(current and current->flags() & Qt::ItemIsEnabled) {
      QString task = current->data(0, TaskNameRole).toString();
      int proc_num = current->data(0, ProcessPositionRole).toInt();
      if(tasks.contains(task)) {
        QList<ProcessDefinition>& defs = tasks[task];
        if(proc_num < defs.size())
          def = &defs[proc_num];
      }
    }
  } else {
    if(!currentProcessName.isEmpty()) 
      def = &processes[currentProcessName];
  }
  if(def)
    def->parms = parmsModel->parms();
}

int MorphoDynamX::activeStack() const
{
  int active = ui.StackTabs->currentIndex();
  if((active == 0 and (ui.Stack1MainShow->isChecked() or ui.Stack1WorkShow->isChecked()))
     or (active == 1 and (ui.Stack2MainShow->isChecked() or ui.Stack2WorkShow->isChecked())))
    return active;
  return -1;
}

int MorphoDynamX::activeMesh() const
{
  int active = ui.StackTabs->currentIndex();
  if(active == 0 or active == 1)
    return active;
  return -1;
}

void MorphoDynamX::resetDefaultParameters()
{
  if(!currentProcessName.isEmpty()) {
    QStringList parms;
    if(getDefaultParms(currentProcessName, parms))
      parmsModel->setParms(parms);
  }
}

void MorphoDynamX::copyProcessName()
{
  if(!currentProcessName.isEmpty()) {
    QGuiApplication::clipboard()->setText(currentProcessName);
    mdxInfo << "Copied to clipboard:" << currentProcessName << endl;
  }
}

// Rename a signal attribute
void MorphoDynamX::renameStack1SurfSignal()
{
  QStringList parms = QStringList() << "0" << ui.Stack1SurfSignal->currentText() << "";
  launchProcess("Mesh/Signal/Rename Signal", parms, Process::PROCESS_RUN, false);
}
void MorphoDynamX::renameStack2SurfSignal()
{
  QStringList parms = QStringList() << "1" << ui.Stack2SurfSignal->currentText() << "";
  launchProcess("Mesh/Signal/Rename Signal", parms, Process::PROCESS_RUN, false);
}

// Delete a signal attribute
void MorphoDynamX::deleteStack1SurfSignal()
{
  QStringList parms = QStringList() << "0" << ui.Stack1SurfSignal->currentText();
  launchProcess("Mesh/Signal/Delete Signal", parms, Process::PROCESS_RUN, false);
}
void MorphoDynamX::deleteStack2SurfSignal()
{
  QStringList parms = QStringList() << "1" << ui.Stack2SurfSignal->currentText();
  launchProcess("Mesh/Signal/Delete Signal", parms, Process::PROCESS_RUN, false);
}

void MorphoDynamX::reloadTasks()
{
  ui.ProcessTasksCommand->clear();
  QStringList ts = tasks.keys();
  ts.sort();
  forall(const QString& t, ts) {
    QList<ProcessDefinition>& defs = tasks[t];
    QTreeWidgetItem* task_item = new QTreeWidgetItem(QStringList() << t);
    int proc_num = 0;
    forall(ProcessDefinition& def, defs) {
      QTreeWidgetItem* item = new QTreeWidgetItem(QStringList() << def.name);
      item->setToolTip(0, def.description);
      item->setIcon(0, def.icon);
      item->setData(0, TaskNameRole, t);
      item->setData(0, ProcessPositionRole, proc_num);
      if(validProcessName(def.name)) {
        if(!checkProcessParms(def.name, def.parms))
          getProcessParms(def.name, def.parms);
      } else
        item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
      task_item->addChild(item);
      ++proc_num;
    }
    ui.ProcessTasksCommand->addTopLevelItem(task_item);
    ui.ProcessTasksCommand->expandItem(task_item);
  }
}

void MorphoDynamX::editTasks()
{
  TaskEditDlg* dlg = new TaskEditDlg(tasks, savedProc, this);
  if(dlg->exec() == QDialog::Accepted) {
    // Deep copy to duplicate all strings
    const TaskEditDlg::tasks_t& ts = dlg->tasks();
    tasks.clear();
    updateCurrentTasks(ts);
    reloadTasks();
  }
}

void MorphoDynamX::globalContrast()
{
  ui.Viewer->GlobalContrast = double(ui.GlobalContrast->value()) / 5000.0;
  currentSetup->setGlobalContrast(ui.Viewer->GlobalContrast);
  ui.Viewer->updateAll();
}

void MorphoDynamX::globalBrightness()
{
  ui.Viewer->GlobalBrightness = double(ui.GlobalBrightness->value() - 5000) / 5000.0;
  currentSetup->setGlobalBrightness(ui.Viewer->GlobalBrightness);
  ui.Viewer->updateAll();
}

void MorphoDynamX::globalShininess()
{
  ui.Viewer->GlobalShininess = double(ui.GlobalShininess->value()) / 5000.0 * 128.0;
  currentSetup->setGlobalShininess(ui.Viewer->GlobalShininess);
  ui.Viewer->updateAll();
}

void MorphoDynamX::globalSpecular()
{
  ui.Viewer->GlobalSpecular = double(ui.GlobalSpecular->value()) / 5000.0;
  currentSetup->setGlobalSpecular(ui.Viewer->GlobalSpecular);
  ui.Viewer->updateAll();
}

void MorphoDynamX::debugDialog()
{
  static QPointer<DebugDlg> dlg = 0;
  if(dlg.isNull())
    dlg = new DebugDlg(ui.Viewer, this);
  dlg->show();
}

void MorphoDynamX::setCutSurfControl()
{
  double sceneRadius = cutSurf.getSceneRadius();
  if(sceneRadius == 0.0f)
    sceneRadius = 1.0f;
  ui.CutSurfSizeX->setSliderPosition(int(log(cutSurf.cut->size().x() * 2.0 / sceneRadius) * 2000));
  ui.CutSurfSizeY->setSliderPosition(int(log(cutSurf.cut->size().y() * 2.0 / sceneRadius) * 2000));
  ui.CutSurfSizeZ->setSliderPosition(int(log(cutSurf.cut->size().z() * 2.0 / sceneRadius) * 2000));
}

void MorphoDynamX::resetClipControl(double sceneRadius)
{
  if(sceneRadius == 0.0f)
    sceneRadius = 1.0f;
  ui.Clip1Width->setSliderPosition(int(log(currentSetup->clip1()->width() * 2.0 / sceneRadius) * 2000));
  ui.Clip2Width->setSliderPosition(int(log(currentSetup->clip2()->width() * 2.0 / sceneRadius) * 2000));
  ui.Clip3Width->setSliderPosition(int(log(currentSetup->clip3()->width() * 2.0 / sceneRadius) * 2000));
}

void MorphoDynamX::resetCutSurf()
{
  cutSurf.reset(ui.Viewer->getSceneRadius());
  setCutSurfControl();
  resetClipControl(ui.Viewer->getSceneRadius());
}

const Process* MorphoDynamX::globalProcess() const {
  return currentSetup;
}

Process* MorphoDynamX::globalProcess() {
  return currentSetup;
}

void MorphoDynamX::changeSelectLabel(int lab) {
  currentSetup->setSelectedLabel(lab);
}

// Load the Heat choices
void MorphoDynamX::loadHeatChoices(int id)
{
  Mesh *mesh = currentSetup->getMesh(id);
  if(!mesh)
    return;
  if(id == 0) {
    ui.Stack1SurfHeat->clear();
    const QStringList &choices = mesh->heatAttrList();
    if(choices.size() > 0) {
      ui.Stack1SurfHeat->addItems(choices);
      if(mesh->heat().isEmpty())
        mesh->setHeat(choices[0]);
      ui.Stack1SurfHeat->setCurrentText(mesh->heat());
      ui.Stack1SurfHeat->setEnabled(true);
    } else
      ui.Stack1SurfHeat->setDisabled(true);
  } else if(id == 1) {
    ui.Stack2SurfHeat->clear();
    const QStringList &choices = mesh->heatAttrList();
    if(choices.size() > 0) {
      ui.Stack2SurfHeat->addItems(choices);
      if(mesh->heat().isEmpty())
        mesh->setHeat(choices[0]);
      ui.Stack2SurfHeat->setCurrentText(mesh->heat());
      ui.Stack2SurfHeat->setEnabled(true);
    } else
      ui.Stack2SurfHeat->setDisabled(true);
  }
}

// Load the Signal choices
void MorphoDynamX::loadSignalChoices(int id)
{
  Mesh *mesh = currentSetup->getMesh(id);
  if(!mesh)
    return;
  if(id == 0) {
    ui.Stack1SurfSignal->clear();
    const QStringList &choices = mesh->signalAttrList();
    if(choices.size() > 0) {
      ui.Stack1SurfSignal->addItems(choices);
      if(mesh->signal().isEmpty())
        mesh->setSignal(choices[0]);
      ui.Stack1SurfSignal->setCurrentText(mesh->signal());
      ui.Stack1SurfSignal->setEnabled(true);
    } else
      ui.Stack1SurfSignal->setDisabled(true);
  } else if(id == 1) {
    ui.Stack2SurfSignal->clear();
    const QStringList &choices = mesh->signalAttrList();
    if(choices.size() > 0) {
      ui.Stack2SurfSignal->addItems(choices);
      if(mesh->signal().isEmpty())
        mesh->setSignal(choices[0]);
      ui.Stack2SurfSignal->setCurrentText(mesh->signal());
      ui.Stack2SurfSignal->setEnabled(true);
    } else
      ui.Stack2SurfSignal->setDisabled(true);
  }
}


// Load the Labeling choices
void MorphoDynamX::loadLabelingChoices(int id)
{
  Mesh *mesh = currentSetup->getMesh(id);
  if(!mesh)
    return;
  if(id == 0) {
    ui.Stack1SurfLabeling->clear();
    const QStringList &choices = mesh->labelingAttrList();
    if(choices.size() > 0) {
      ui.Stack1SurfLabeling->addItems(choices);
      if(mesh->labeling().isEmpty())
        mesh->setLabeling(choices[0]);
      ui.Stack1SurfLabeling->setCurrentText(mesh->labeling());
      ui.Stack1SurfLabeling->setEnabled(true);
    } else
      ui.Stack1SurfLabeling->setDisabled(true);
  } else if(id == 1) {
    ui.Stack2SurfLabeling->clear();
    const QStringList &choices = mesh->labelingAttrList();
    if(choices.size() > 0) {
      ui.Stack2SurfLabeling->addItems(choices);
      if(mesh->labeling().isEmpty())
        mesh->setLabeling(choices[0]);
      ui.Stack2SurfLabeling->setCurrentText(mesh->labeling());
      ui.Stack2SurfLabeling->setEnabled(true);
    } else
      ui.Stack2SurfLabeling->setDisabled(true);
  }
}
