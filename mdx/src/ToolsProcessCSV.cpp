//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <ToolsProcessCSV.hpp>

#include "ui_CSVDataWindow.h"

// Processes for CSV data
namespace mdx
{
  bool LoadCSVFile::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw QString("LoadCSVFiles::run No current mesh");

    QString attrName = parm("CSV Attribute");
    if(attrName.isEmpty())
      throw QString("LoadCSVFiles::run Attribute name cannot be empty");
    auto &csvData = mesh->attributes().attrMap<QString, CSVData>("#CSVData#")[attrName];

    QString fileName = parm("File Name");
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
      throw QString("LoadCSVFiles::run Cannot open file: %1").arg(fileName);

    QTextStream in(&file);
    bool firstLine = true;
    csvData.clear();
    while (!in.atEnd()) {
      QString line = in.readLine();
      // First line is header
      if(firstLine) {
        QStringList header = line.split(QRegExp(","));
        for(const QString &columnName : header)
          csvData.addColumn(columnName);

        firstLine = false;
      } else 
        csvData.addRow(line.split(QRegExp(",")));
    }
    mdxInfo << QString("CSV file read rows: %1 cols: %2").arg(csvData.rows()).arg(csvData.columns()) << endl;

    return true;
  }
  REGISTER_PROCESS(LoadCSVFile);

  bool SaveCSVFile::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw QString("SaveCSVFiles::run No current mesh");

    QString attrName = parm("CSV Attribute");
    if(attrName.isEmpty())
      throw QString("SaveCSVFiles::run Attribute name cannot be empty");
    auto &csvData = mesh->attributes().attrMap<QString, CSVData>("#CSVData#")[attrName];

    if(csvData.rows() == 0)
      throw QString("SaveCSVFiles::run No CSV data in attribute: %1").arg(attrName);

    QString fileName = parm("File Name");
    if(fileName.isEmpty())
      throw QString("SaveCSVFiles::run File name cannot be empty");
    if(!fileName.endsWith(".csv"))
      fileName += ".csv";

    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
      throw QString("SaveCSVFiles::run Cannot open file: %1").arg(fileName);

    QTextStream out(&file);
    int count = 0;
    for(const QString &s : csvData.columnList()) {
      if(count++ > 0)
        out << "," << s;
      else
        out << s;
    }
    out << endl;

    for(uint i = 0; i < csvData.rows(); i++) {
      const QStringList &row = csvData[i];
      int count = 0;
      for(const QString &s : row) {
        if(count++ > 0)
          out << "," << s;
        else
          out << s;
      }
      out << endl;
    }
    mdxInfo << QString("CSV attribute written, rows: %1").arg(csvData.rows()) << endl;

    return true;
  }
  REGISTER_PROCESS(SaveCSVFile);

  bool MakeCSVLabeling::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw QString("%1::run No current mesh").arg(name());

    QString attrName = parm("CSV Attribute");
    if(attrName.isEmpty())
      throw QString("%1::run Attribute name cannot be empty").arg(name());
    auto &csvData = mesh->attributes().attrMap<QString, CSVData>("#CSVData#")[attrName];
    if(csvData.rows() == 0)
      throw QString("%1::run No data in CSV attribute: %2").arg(name()).arg(attrName);

    QString labelingColumn = parm("Labeling Column");
    if(labelingColumn.isEmpty())
      throw QString("%1::run No column specified").arg(name());

    uint labelingCol = csvData.column(labelingColumn);
    if(labelingCol == csvData.columns())
      throw QString("%1::run No column %2 found in CSV data %3").arg(name()).arg(labelingColumn).arg(attrName);

    QString labelingName = parm("Labeling Name");
    if(labelingName.isEmpty())
      labelingName = labelingColumn;
    if(labelingName == "Labels")
      throw QString("%1::run Labeling cannot be called 'Labels'").arg(name());

    QString labelColumn = parm("Label Column");
    if(labelColumn.isEmpty())
      throw QString("%1::run Label column name empty").arg(name());
    uint labelCol = csvData.column(labelColumn);
    if(labelCol == csvData.columns())
      throw QString("%1::run No column %2 found in CSV data %3").arg(name()).arg(labelColumn).arg(attrName);
    
    // All parms OK, now create the labeling
    int labelingLabel = 1;
    AttrMap<QString, int> labelingLabels;
    auto &labelingLabelNames = mesh->labelName(labelingName);
    auto &labelingLabelMap = mesh->labelMap(labelingName);

    for(uint i = 0; i < csvData.rows(); i++) {
      auto &row = csvData[i];
      if(row.size() > int(labelCol) and row.size() > int(labelingCol)) {
        int rowLabel = row[labelCol].toInt();
        const QString &rowLabeling = row[labelingCol];
        if(!rowLabeling.isEmpty()) {
          // See if labeling entry exists
          auto pr = labelingLabels.find(rowLabeling);
          if(pr == labelingLabels.end()) {
            labelingLabelNames[labelingLabel] = rowLabeling;
            labelingLabelMap[rowLabel] = labelingLabel;
            labelingLabels[rowLabeling] = labelingLabel++;
          } else
            labelingLabelMap[rowLabel] = pr->second;
        }
      }
    }

    // Print out the labeling
    mdxInfo << endl << "Created labeling: " << labelingName << endl;
    for(const auto &pr : labelingLabels)
      mdxInfo << " " << pr.first << ":" << pr.second << endl;
    mdxInfo << endl;
 
    mesh->setLabeling(labelingName);

    return true;
  }
  REGISTER_PROCESS(MakeCSVLabeling);

  bool ViewCSVData::initialize(QWidget *parent)
  {
    if(!parent)
      return true;

    mesh = currentMesh();
    if(!mesh)
      throw QString("%1::run No current mesh").arg(name());
    ccName = mesh->ccName();

    QString attrName = parm("CSV Attribute");
    if(attrName.isEmpty())
      throw QString("%1::run Attribute name cannot be empty").arg(name());
    auto &csvData = mesh->attributes().attrMap<QString, CSVData>("#CSVData#")[attrName];
    if(csvData.rows() == 0)
      throw QString("%1::run No data in CSV attribute: %2").arg(name()).arg(attrName);

    auto *csvDataWindow = new CSVDataWindow(parent);
    csvDataWindow->csvDataModel.setCSVData(csvData);
    csvDataWindow->ui->csvDataView->update(QModelIndex());
    csvDataWindow->ui->csvDataView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(csvDataWindow, &CSVDataWindow::selectLabels, this, &ViewCSVData::selectLabels);
    csvDataWindow->setWindowTitle(QString("CSV Data - %1").arg(attrName));
    csvDataWindow->show();

    return true;
  }
  void ViewCSVData::selectLabels(IntVec labels)
  {
    if(!mesh or ccName.isEmpty())
      return;

    mesh->clearSelectFaces(ccName);
    for(int label : labels)
      mesh->addSelectLabel(ccName, label);
  }
  REGISTER_PROCESS(ViewCSVData);
}
