//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef DIST_MATRIX_PROTO_HPP
#define DIST_MATRIX_PROTO_HPP

#include <DistObject.hpp>
#include <ThrustTypes.hpp>

namespace mdx
{
  /**
   * Function prototypes for distributed matrix library.
   */
  // Cuda functions (compiled with nvcc)
  template<typename T> 
  int multGPU(T *v, typename T::value_type::value_type a, T *r);

  template<typename T> 
  int multGPU(T *v1, T *v2, typename T::value_type::value_type &r);

  template<typename TM, typename TV> 
  int multGPU(DeviceVu *csr, TM *mv, TM *me, TV *v, TV *r);

  template<typename TM, typename TV>
  int multGPU(DeviceVu *csr, TV *v, TM *mv, TM *me, TV *r);

  template<typename T> 
  int addToDiagGPU(typename T::value_type::value_type a, T *r);

  template<typename T>
  int addGPU(T *v1, T *v2, T *r);
 
  template<typename T>
  int subtractGPU(T *v1, T *v2, T *r);

  template<typename T> 
  int fillGPU(typename T::value_type::value_type a, T *r);

  template<typename T> 
  int minGPU(T *v, typename T::value_type::value_type &r);

  template<typename T> 
  int maxGPU(T *v, typename T::value_type::value_type &r);

  template<typename T> 
  int saxpyGPU(T *v1, T *v2, typename T::value_type::value_type a, T *r);

  template<typename T> 
  int jacobiPreCondGPU(T *mv, T *av);
}
#endif
