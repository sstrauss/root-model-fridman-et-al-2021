#include <Contour.hpp>

#include <Matrix.hpp>
#include <Information.hpp>

#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>

#if defined(__APPLE__) || defined(__linux__)
#  include <sys/file.h>
#endif

#define err mdxWarning
#define REPORT_ERROR(filename, line_nb, error) \
  err << "Error in file " << filename << " on line " << line_nb << ": " << error << endl

namespace mdx
{
  // Reread the contour from the associated file, if there is one.
  bool Contour::reread()
  {
    // If there's no associated file, we just return.
    if(fileName.isEmpty()) return true;

    // Open the file for reading.
    QFile f(fileName);
    if(!f.open(QIODevice::ReadOnly))
    {
      err << "Error opening file " << fileName << ": " << f.errorString() << endl;
      return false;
    }
#if defined(__APPLE__) || defined(__linux__)
    flock(f.handle(), LOCK_SH);
#endif

    // Start reading the file with the file header.
    QTextStream ts(&f);
    int line_nb = 0;    // current line number of file
    bool ok;            // used for QString::toWhatever() functions

    // Helper function to read in the next line from the file.
    auto readNextLine = [&ts,&line_nb](void) {
      line_nb++;
      return ts.readLine().simplified().split(" ");
    };

    // The first line is
    //   cver  major_version  minor_version
    QStringList versionLine = readNextLine();
    if(versionLine.size() != 3 || versionLine[0] != "cver")
    {
      REPORT_ERROR(fileName, line_nb, "Could not parse contour file version");
      return false;
    }

    // Get the version number.
    uint vMajor = versionLine[1].toUInt(&ok);
    if(!ok)
    {
      REPORT_ERROR(fileName, line_nb, "Could not read the major version number");
      return false;
    }
    uint vMinor = versionLine[2].toUInt(&ok);
    if(!ok)
    {
      REPORT_ERROR(fileName, line_nb, "Could not read the minor version number");
      return false;
    }

    // We only handle versions 1.01 to 1.03.
    uint version = 100*vMajor + vMinor;
    if(version < 101 || version > 103)
    {
      REPORT_ERROR(fileName, line_nb, "Cannot handle contour file format version " << vMajor << " " << vMinor);
      return false;
    }

    // Now we read in the rest of the header.

    // The second line is the curve name.
    // MDX doesn't care about this, so we ignore it.
    QStringList nameLine = readNextLine();
    if(nameLine[0] != "name:")
    {
      REPORT_ERROR(fileName, line_nb, "'name' expected, found '" << nameLine[0] << "'");
      return false;
    }

    // The third line is the number of points and their total multiplicity.
    QStringList pointsLine = readNextLine();
    if(pointsLine[0] != "points:")
    {
      REPORT_ERROR(fileName, line_nb, "'points' expected, found '" << pointsLine[0] << "'");
      return false;
    }
    uint numCPs = pointsLine[1].toUInt(&ok);
    if(!ok)
    {
      REPORT_ERROR(fileName, line_nb, "Could not parse the number of points");
      return false;
    }
    uint totalMult = pointsLine[2].toUInt(&ok);
    if(!ok)
    {
      REPORT_ERROR(fileName, line_nb, "Could not parse the number of points");
      return false;
    }
    if(numCPs < 1 || totalMult < numCPs || totalMult < 4)
    {
      REPORT_ERROR(fileName, line_nb, "Illegal number of points for a cubic B-spline");
      return false;
    }

    // The fourth line is the curve type.
    QStringList typeLine = readNextLine();
    if(typeLine[0] != "type:")
    {
      REPORT_ERROR(fileName, line_nb, "'type' expected, found '" << typeLine[0] << "'");
      return false;
    }
    // The possible curve types differ by version.
    if(version <= 101)
    {
      if(typeLine[1] == "closed")
	closed = true;
      else if(typeLine[1] == "open")
	closed = false;
      else
      {
	REPORT_ERROR(fileName, line_nb, "type must be open or closed, found '" << typeLine[1] << "'");
	return false;
      }
    }
    else // Versions 102 and up.
    {
      // The first character determines open vs closed.
      if(typeLine[1][0] == 'c')
	closed = true;
      else if(typeLine[1][0] == 'o')
	closed = false;
      else
      {
	REPORT_ERROR(fileName, line_nb, "first character of type must be o or c, found '" << typeLine[1] << "'");
	return false;
      }

      // The second character determines regular vs endpoint interpolated.
      if(typeLine[1][1] == 'e')
      {
	REPORT_ERROR(fileName, line_nb, "MDX does not currently handle endpoint-interpolated splines");
	return false;
      }
      else if(typeLine[1][1] != 'r')
      {
	REPORT_ERROR(fileName, line_nb, "second character of type must be r or e, found '" << typeLine[1] << "'");
	return false;
      }
    }

    // The fifth line of the header is the number of samples.
    // It's only found in version 103 and is ignored by MDX.
    if(version >= 103)
    {
      QStringList samplesLine = readNextLine();
      if(samplesLine[0] != "samples:")
      {
	REPORT_ERROR(fileName, line_nb, "'samples' expected, found '" << samplesLine[0] << "'");
	return false;
      }
    }

    // Finally, we read in numCPs lines with one control point defined per line.
    std::vector<Vector<3,double>> cps;
    for(uint i = 0 ; i < numCPs ; i++)
    {
      QStringList coords = readNextLine();
      if(coords.size() != 4)
      {
	REPORT_ERROR(fileName, line_nb, "Control points should be described with four values");
	return false;
      }

      Vector<3,double> cp;
      for(uint d = 0 ; d < 3 ; d++) {
	cp[d] = coords[d].toDouble(&ok);
	if(!ok)
	{
	  REPORT_ERROR(fileName, line_nb, "Coordinate " << (d+1) << " is not a valid floating point value");
	  return false;
	}
      }
      uint m = coords[3].toUInt(&ok);
      if(!ok)
      {
	REPORT_ERROR(fileName, line_nb, "Could not read control point multiplicity");
	return false;
      }
      if(m < 1)
      {
	REPORT_ERROR(fileName, line_nb, "Invalid control point multiplicity");
	return false;
      }

      // Add the point m times to the control point list.
      for(uint k = 0 ; k < m ; k++) cps.push_back(cp);
    }

    // Check the total multiplicity matches what we expected.
    if(cps.size() != totalMult)
    {
      REPORT_ERROR(fileName, line_nb, "Computed a total multiplicity of " << cps.size() << ", expected " << totalMult);
      return false;
    }

    // For a closed contour, copy the first three points to the back.
    if (closed) {
      cps.push_back(cps[0]);
      cps.push_back(cps[1]);
      cps.push_back(cps[2]);
    }

    // Set the control points to the read values.
    controlPoints = cps;

    // Close the file.
#if defined(__APPLE__) || defined(__linux__)
    flock(f.handle(), LOCK_UN);
#endif
    f.close();

    return true;
  }

  // Coefficient matrices used to evaluate cubic B-splines.
  // These are such that (s^3 s^2 s 1) * M * (P0 P1 P2 P3)^T = curve point at s
  //  (where s in [0,1] defines the whole of
  //   the section of the curve defined by P0..P3).
  static const double Spline[16] =
  {
    -1.0f/6.0f,  3.0f/6.0f, -3.0f/6.0f,  1.0f/6.0f,   // s^3
     3.0f/6.0f, -6.0f/6.0f,  3.0f/6.0f,  0.0f/6.0f,   // s^2
    -3.0f/6.0f,  0.0f/6.0f,  3.0f/6.0f,  0.0f/6.0f,   // s
     1.0f/6.0f,  4.0f/6.0f,  1.0f/6.0f,  0.0f/6.0f    // 1
  };
  const Matrix<4,4,double> SplineMatrix(Spline);

  // (s^3 s^2 s 1) * M' = (3s^2 2s 1 0) * M:
  static const double SplineDeriv[16] =
  {
     0.0f/6.0f,  0.0f/6.0f,  0.0f/6.0f, 0.0f/6.0f, // no s^3 part
    -3.0f/6.0f,  9.0f/6.0f, -9.0f/6.0f, 3.0f/6.0f, // s^2 part is 3 * top row
     6.0f/6.0f,-12.0f/6.0f,  6.0f/6.0f, 0.0f/6.0f, // s part is 2 * second row
    -3.0f/6.0f,  0.0f/6.0f,  3.0f/6.0f, 0.0f/6.0f  // constant part is third row
  };
  const Matrix<4,4,double> SplineDerivMatrix(SplineDeriv);

  // Helper function for spline evaluation.
  // Given a parameter t in [0,1] and a list of control points,
  // find the correct knot interval and the scaled parameter s,
  // and compute the spline point for the given coefficient matrix.
  Vector<3,double> evalSpline(double t,
			      const std::vector<Vector<3,double>> &cps,
			      const Matrix<4,4,double> &coeffMatrix)
  {
    // Clamp to [0,1].
    if (t < 0.0) t = 0.0;
    else if (t > 1.0) t = 1.0;

    // Determine the interval the given parameter is contained in.
    double tScale = double(cps.size() - 3) * t;
    uint interval(tScale);
    double s = tScale - interval;

    // Create the matrix (s^3 s^2 s 1).
    double s2 = s*s;
    Matrix<1,4,double> powers;
    powers(0,0) = s2 * s;
    powers(0,1) = s2;
    powers(0,2) = s;
    powers(0,3) = 1;

    // Create the control point matrix.
    Matrix<4,3,double> cpMatrix;
    for(uint i = 0 ; i < 4 ; i++)
      cpMatrix[i] = cps[interval + i];

    // Multiply to find the point.
    return (powers * coeffMatrix * cpMatrix)[0];
  }

  // Evaluate the curve at a given parameter.
  Vector<3,double> Contour::point(double t) const
  {
    return evalSpline(t, controlPoints, SplineMatrix);
  }

  // Find the tangent to the curve.
  Vector<3,double> Contour::tangent(double t, double dt) const
  {
    // Usually the derivative is defined analytically.
    Vector<3,double> tangent = evalSpline(t, controlPoints, SplineDerivMatrix);
    double tanSz = norm(tangent);
    if(tanSz > 1e-7) return tangent / tanSz;

    // If the analytically computed vector doesn't exist,
    // we'll instead compute the tangent numerically.
    do {
      double tLo = std::max(t - dt, 0.0), tHi = std::min(t + dt, 1.0);
      tangent = point(tHi) - point(tLo);
      tanSz = norm(tangent);
      dt *= 2;
    } while (tanSz < 1e-7 && dt <= 1.0);

    return tangent / tanSz;
  }

  // Compute the length between the two parameters.
  double Contour::length(double a, double b, double dt) const
  {
    // Ensure our input requirements: 0 <= a < b <= 1, dt < (b-a).
    if (a < 0.0) a = 0.0;
    else if (a > 1.0) a = 1.0;
    if (b > 1.0) b = 1.0;
    if (b < a + dt) return 0.0;

    // Step along the curve in parameter increments of dt.
    double l = 0.0;
    Vector<3,double> p = point(a), q;
    double s = a + dt;
    while (s <= b) {
      q = point(s);
      l += norm(p-q);
      p = q;
      s += dt;
    }
    // Add on the last step, from (s-dt) to b.
    l += norm(p - point(b));

    return l;
  }

  // Compute the parameter of the point reached
  // after travelling a distance l along the curve.
  double Contour::travel(double t, double l, double dt) const
  {
    // Check our requirements: t in [0,1], l > 0.
    if (t < 0.0) t = 0.0;
    else if (t > 1.0) return 1.0;
    if (l < dt) return t;

    // Step along the curve in parameter steps of dt
    // until we've moved a distance of l.
    double t_length = 0.0, u = t + dt;
    Vector<3,double> pt = point(t), pu = point(u);
    while (t_length < l && t < 1.0) {
      t_length += norm(pu - pt);
      t = u;
      pt = pu;
      u += dt;
      pu = point(u);
    }
    return std::min(t, 1.0);
  }
}
