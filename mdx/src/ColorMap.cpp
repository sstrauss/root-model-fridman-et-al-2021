//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "ColorMap.hpp"
#include "Geometry.hpp"
#include "Util.hpp"
#include "Random.hpp"
#include "Information.hpp"
#include "PresetColors.hpp"

#define NO_COLOR Colorb(0,0,0,0)

namespace mdx
{
  const ColorMap::ChannelMap &ColorMap::channelMap(uint channel) const
  {
    const static ChannelMap defaultChannel(false);
    if(channel < channelMaps.size())
      return channelMaps[channel];
    else
    {
      return defaultChannel;
      chErr = true;
    }
  }
  ColorMap::ChannelMap &ColorMap::channelMap(uint channel)
  {
    reserveChannels(channel + 1);
    return channelMaps[channel];
  }

  // getRawColor methods
  Colorb ColorMap::getRawColor(int index) const
  {
    if(colors.empty())
      return defaultColor;
    else
      return colors[index % colors.size()];
  }

  Colorb ColorMap::getRawColor(double s, const Point2d &bounds) const
  {
    return getRawColor(s,bounds,Point2i(0,colors.size() - 1));
  }

  Colorb ColorMap::getRawColor(double s, const Point2d &bounds, const Point2i &indices) const
  {
    if(colors.empty() || isNan(s))
      return defaultColor;

    double s0 = bounds[0], s1 = bounds[1];
    if(isNan(s0) || isNan(s1))
      return defaultColor;

    int i0 = indices[0], i1 = indices[1];
    if(i0 < 0 || i1 < 0)
    {
      i0 = 0;
      i1 = colors.size() - 1;
    }
    else
    {
      i0 = trim(i0, 0, int(colors.size() - 1));
      i1 = trim(i1, 0, int(colors.size() - 1));
    }

    if(i0 == i1)
      return colors[i0];

    // If bounds are the same, return the middle color
    if(s0 == s1)
      return clerp(0.5, colors[i0], colors[i1]);

    s = trim((s - s0) / (s1 - s0) , 0.0d, 1.0d);
    int interval = i1 - i0;
    int idx = i0 + s * interval;
    int maxI = std::max(i0,i1);
    if(idx >= maxI)
      return colors[maxI];

    s = i0 + s * interval - idx;
    return clerp(s, colors[idx], colors[idx+1]);
  }

  // getColor methods
  Colorb ColorMap::getColor(uint channelOffset, const Colorb& inColor) const
  {
    if(!channelMap(channelOffset).visible)
      return NO_COLOR;
    else
      return inColor;
  }

  Colorb ColorMap::getColor(uint channelOffset, int index) const
  {
    const ChannelMap &chMap = channelMap(channelOffset);

    if(!chMap.visible)
      return NO_COLOR;
    if(colors.empty())
      return defaultColor;

    int i0 = chMap.indices[0], i1 = chMap.indices[1];
    if(i0 < 0 || i1 < 0)
      return getRawColor(index);

    i0 = trim(i0, 0, int(colors.size() - 1));
    i1 = trim(i1, 0, int(colors.size() - 1));
    if(i0 == i1)
      return getRawColor(i0);

    int idxX = index % (i1 - i0 + 1);
    return getRawColor(i0 + idxX);
  }

  Colorb ColorMap::getColorImpl(uint channelOffset, size_t sz, const double *vec) const
  {
    std::vector<Colorf> visCol;
    for(uint i = 0 ; i < sz ; i++)
    {
      const ChannelMap &cmap = channelMap(i + channelOffset);
      if(!cmap.visible) continue;
      visCol.push_back(Colorf(getRawColor(vec[i],cmap.bounds,cmap.indices)));
    }

    if(visCol.empty())
      return NO_COLOR;

    // composite the colours together in even measure
    Colorf color(0,0,0,0);
    for(uint i = 0 ; i < visCol.size() ; i++) {
//      visCol[i][3] = 1. / (i + 1);
      visCol[i][3] = 1.;
      color = composite(color, visCol[i], comp::LIGHTEN);
    }

    return Colorb(color);
  }

  // Preset colors available in dropdown list, note there are some in setColors that are not in this list
  QStringList ColorMap::getColorMapNames() const
  {
    return QStringList() 
      << "Labels" << "Greyscale" << "Red" << "Green" << "Green Red" << "Uniform Jet"
      << "Warm Helix" << "Cool Helix" << "Blackbody" << "Jet" << "Random";
  }

  bool ColorMap::setColors(const QString &map, int sz)
  {
    if(sz == 0) sz = 16;

    if(map == "Default") {                         // simple color maps
      colors.resize(2);
      colors[0] = Colorb(0,0,0,255);
      colors[1] = Colorb(255,255,255,255);
    } else if(map == "Single Red") {
      colors.resize(1);
      colors[0] = Colorb(255,0,0,255);
    } else if(map == "Single Green") {
      colors.resize(1);
      colors[0] = Colorb(0,255,0,255);
    } else if(map == "Single Cyan") {
      colors.resize(1);
      colors[0] = Colorb(0,255,255,255);
    } else if(map == "Greyscale") {
      colors.resize(2);
      colors[0] = Colorb(0,0,0,255);
      colors[1] = Colorb(255,255,255,255);
    } else if(map == "Red") {
      colors.resize(2);
      colors[0] = Colorb(64,0,0,255);
      colors[1] = Colorb(255,0,0,255);
    } else if(map == "Green") {
      colors.resize(2);
      colors[0] = Colorb(0,64,0,255);
      colors[1] = Colorb(0,255,0,255);
    } else if(map == "Green Red") {
      colors.resize(4);
      colors[0] = Colorb(0,64,0,255);
      colors[1] = Colorb(0,255,0,255);
      colors[2] = Colorb(0,0,0,255);
      colors[3] = Colorb(255,0,0,255);
    } else if(map == "Labels")                     // Preset color maps
      colors = PresetColors::labels();
    else if(map == "Jet")
      colors = PresetColors::jet();
    else if(map == "Uniform Jet")
      colors = PresetColors::uniformJet();
    else if(map == "Warm Helix")
      colors = PresetColors::helixWarm();
    else if(map == "Cool Helix")
      colors = PresetColors::helixCool();
    else if(map == "Blackbody")
      colors = PresetColors::blackbody();
    else if(map == "Random") {
      colors.resize(sz);
      for(size_t i = 0; i < colors.size(); ++i) {
        Point3f r = ran(Point3f(1, 1, 1));
        Colorb col(r.x() * 255, r.y() * 255, r.z() * 255, 255);
        colors[i] = col;
      } 
    } else if(map == "Vertex Index") { 
      colors.resize(6);
      colors[0] = Colorb(0,255,0,255);
      colors[1] = Colorb(0,255,255,255);
      colors[2] = Colorb(255,255,0,255);
      colors[3] = Colorb(192,192,192,255);
      colors[4] = Colorb(64,64,255,255);
      colors[5] = Colorb(255,0,0,255);
    } else if(map == "Axis") { 
      colors.resize(6);
      colors[0] = Colorb(255,255,255,255);
      colors[1] = Colorb(255,0,0,255);
      colors[2] = Colorb(228,228,228,255);
      colors[3] = Colorb(228,0,0,255);
      colors[4] = Colorb(200,200,200,255);
      colors[5] = Colorb(200,0,0,255);
    } else {
      mdxWarning << "ColorMap::setColorMap Type '" << map << "' not found" << endl;
      if(colors.size() == 0) {
        colors.resize(2);
        colors[0] = Colorb(0, 0, 0, 255);
        colors[1] = Colorb(255, 255, 255, 255);
      }
    }

    return true;
  }

  // creating color maps
  bool ColorMap::makeLabelMap(void)
  {
    channelMap(0).cmType = cmType = Label;

    return true;
  }

  bool ColorMap::makePassthroughMap(void)
  {
    channelMap(0).cmType = cmType = Color;

    return true;
  }

  bool ColorMap::makeIndexMap(void)
  {
    channelMap(0).cmType = cmType = Index;

    return true;
  }

  bool ColorMap::makeRangeMap(uint numCh)
  {
    channelMap(0).cmType = cmType = Range;
    reserveChannels(numCh);

    return true;
  }

  bool ColorMap::makeRangeMap(const Point2d &bounds)
  {
    ChannelMap &chMap = channelMap(0);

    chMap.bounds = bounds;
    chMap.cmType = cmType = Range;
    return true;
  }

  bool ColorMap::makeRangeMap(const Point2d &bounds, const QString &unit)
  {
    ChannelMap &chMap = channelMap(0);

    chMap.bounds = bounds;
    chMap.unit = unit;
    chMap.cmType = cmType = Range;
    return true;
  }

  bool ColorMap::reserveChannels(uint numCh)
  {
    if(numCh > numChannels())
    {
      // All new channel maps get the same cmType as the ColorMap
      uint lastCM = numChannels() - 1;
      channelMaps.resize(numCh);
      for(uint i = lastCM ; i < numCh ; i++)
        channelMaps[i].cmType = cmType;
    }
    return true;
  }

  bool ColorMap::isVisible(uint channel) const
  {
    if(channel >= channelMaps.size())
      return false;
    return channelMaps[channel].visible;
  }

  bool ColorMap::setName(uint channel, const QString &name)
  {
    channelMap(channel).name = name;
    return true;
  }

  bool ColorMap::setBounds(uint channel, const Point2d &bounds)
  {
    ChannelMap &chMap = channelMap(channel);

    chMap.bounds = bounds;
    chMap.cmType = cmType = Range;
    return true;
  }

  bool ColorMap::setUnit(uint channel, const QString &unit)
  {
    channelMap(channel).unit = unit;
    return true;
  }

  bool ColorMap::setIndices(uint channel, const Point2i &indices)
  {
    channelMap(channel).indices = indices;
    return true;
  }
}
