#include <Shapes.hpp>
#include <CCF.hpp>
#include <Attributes.hpp>

namespace mdx
{
  // Approximates a unit sphere as a midpoint subdivision of an octahedron.
  void sphereOct(CCStructure &cs, CCIndexDataAttr &idxAttr, uint subdivs)
  {
    if(cs.maxDimension() < 2)
      throw(QString("Can't create sphere in cell complex of dimension %1")
            .arg(cs.maxDimension()));

    // Initial sphere: six points, 12 edges, 8 faces.
    // And maybe a volume, if the cell complex is of high enough dimension.
    {
      // Points
      CCIndex vs[3][2];  // [xyz][pm]
      CCIndex eqPts[4];  // points on y == 0
      for(uint d = 0 ; d < 3 ; d++)
        for(uint pm = 0 ; pm < 2 ; pm++)
        {
          vs[d][pm] = CCIndexFactory.getIndex();
          cs.addCell(vs[d][pm]);
          idxAttr[vs[d][pm]].pos[d] = (pm == 0) ? 1 : -1;

          // assign equatorial points if we can
          if(d != 1)
          {
            uint idx = (d >> 1) + ((d >> 1) == pm ? 0 : 2); // xp zm xm zp
            eqPts[idx] = vs[d][pm];
          }
        }

      // Edges
      CCIndex els[2][4], ees[4];  // [ns][lon]   [lon]
      for(uint l = 0 ; l < 4 ; l++)
      {
        // add longitudinal edges
        for(uint h = 0 ; h < 2 ; h++)
        {
          els[h][l] = CCIndexFactory.getIndex();
          cs.addCell(els[h][l] , +vs[1][h] -eqPts[l]);
        }

        // add equatorial edge
        ees[l] = CCIndexFactory.getIndex();
        cs.addCell(ees[l] , +eqPts[l] -eqPts[(l+1)%4]);
      }

      // Faces
      CCIndex fs[2][4];  // [ns][lon]
      CCStructure::BoundaryChain volBound; // needed if we want to make a volume
      for(uint h = 0 ; h < 2 ; h++)
        for(uint l = 0 ; l < 4 ; l++)
        {
          fs[h][l] = CCIndexFactory.getIndex();
          cs.addCell(fs[h][l] , (h == 0 ? ccf::POS : ccf::NEG) * (+ees[l] +els[h][l] -els[h][(l+1)%4]));
          volBound += fs[h][l];
        }

      // Volume if we can
      if(cs.maxDimension() >= 3)
      {
        CCIndex vol = CCIndexFactory.getIndex();
        cs.addCell(vol , volBound);
      }
    }

    // Now we subdivide subdivs times.
    for(uint c = 0 ; c < subdivs ; c++)
    {
      std::set<CCIndex> newVertices;
      // Split edges
      std::vector<CCIndex> oldEdges = cs.cellsOfDimension(1);
      for(CCIndex edge : oldEdges)
      {
        std::pair<CCIndex,CCIndex> eps = cs.edgeBounds(edge);
        Point3d midpoint = idxAttr[eps.first].pos + idxAttr[eps.second].pos;

        CCStructure::SplitStruct ss(edge);
        CCIndexFactory.fillSplitStruct(ss);
        cs.splitCell(ss);

        newVertices.insert(ss.membrane);
        idxAttr[ss.membrane].pos = midpoint.normalized();
      }

      // Split faces
      std::vector<CCIndex> oldFaces = cs.cellsOfDimension(2);
      for(CCIndex face : oldFaces)
      {
        CCStructure::CellTuple tuple(cs,face);
        // move tuple to new vertex
        if(newVertices.count(tuple[0]) == 0)
          tuple.flip(0);
        // find all three new vertices
        CCIndex vs[3];
        vs[0] = tuple[0];
        tuple.flip(0,1,0,1);
        vs[1] = tuple[0];
        tuple.flip(0,1,0);
        vs[2] = tuple[0];

        // split face into four
        for(uint i = 0 ; i < 3 ; i++)
        {
          CCStructure::SplitStruct ss(cs.join(vs[i],vs[(i+1) % 3]));
          CCIndexFactory.fillSplitStruct(ss);
          cs.splitCell(ss , +vs[i] -vs[(i+1) % 3]);
        }
      }
    }

    return;
  }
};
