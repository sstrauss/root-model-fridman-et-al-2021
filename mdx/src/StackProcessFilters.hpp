//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_FILTERS_HPP
#define STACK_PROCESS_FILTERS_HPP

#include <Process.hpp>

namespace mdx 
{
  ///\addtogroup Process
  ///@{
  
 /**
   * \class AutoScaleStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Scale the stack intensity to fill exactly the whole range.
   */
  class mdxBase_EXPORT AutoScaleStack : public Process 
  {
  public:
    AutoScaleStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Autoscale Stack");
      setDesc("Scale the stack intensity to fill exactly the whole range.");
      setIcon(QIcon(":/images/Palette.png"));
    }
  
    bool run()
    {
      Store* input = currentStack()->currentStore();
      Store* output = currentStack()->work();
      bool res = run(input, output);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
  
    /**
     * Scale the stack values, so as to use the whole range.
     */
    bool run(const Store* store, Store* output);
  };
  
  /**
   * \class ApplyTransferFunction StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Transform the stack to reflect the transfer function in use.
   */
  class mdxBase_EXPORT ApplyTransferFunction : public Process 
  {
  public:
    ApplyTransferFunction(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Apply Transfer Function");
      setDesc("Apply the transfer function to the stack (modifies voxel values).");
      setIcon(QIcon(":/images/Palette.png"));
  
      addParm("Red", "Red", "0");
      addParm("Green", "Green", "0");
      addParm("Blue", "Blue", "0");
      addParm("Alpha", "Alpha", "1");
    }
  
    bool run()
    {
      Store* input = currentStack()->currentStore();
      Store* output = currentStack()->work();
      bool res = run(input, output, parm("Red").toFloat(), parm("Green").toFloat(), 
                                       parm("Blue").toFloat(), parm("Alpha").toFloat());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
  
    /**
     * Apply the transfer function. The final value is computed as a linear combination 
     * of the red, green, blue and alpha channels.
     * \param store Input store
     * \param output Output store
     * \param red Coefficient for the red channel
     * \param green Coefficient for the green channel
     * \param blue Coefficient for the blue channel
     * \param alpha Coefficient for the alpha channel
     * \note If the sum of the coefficient is not 1, values can globally increase/decrease
     */
    bool run(Store* store, Store* output, float red, float green, float blue, float alpha);
  };
  
  /**
   * \class StackSwapBytes SystemProcessLoad.hpp <SystemProcessLoad.hpp>
   *
   * Process that swap bytes
   *
   * \ingroup StackProcessFilters
   */
  class mdxBase_EXPORT StackSwapBytes : public Process 
  {
  public:
    StackSwapBytes(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Swap Bytes");
      setDesc("Swap the bytes of the values in the stack.");
      setIcon(QIcon(":/images/SwapBytes.png"));
    }
    bool run();
    bool run(const Store* input, Store* output);
  };

  /**
   * \class AverageStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Compute the local average of each voxel of the stack.
   *
   * More precisely, this module perform the convolution of the stack with the
   * square kernel of given radius.
   */
  class mdxBase_EXPORT AverageStack : public Process 
  {
  public:
    AverageStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Average");
      setDesc("Average stack data with a square filter.");
      setIcon(QIcon(":/images/AvgPix.png"));

      addParm("X Radius", "Radius in X axis", "1");
      addParm("Y Radius", "Radius in Y axis", "1");
      addParm("Z Radius", "Radius in Z axis", "1");
      addParm("Steps", "Number of times to average", "1");
    }
  
    bool run()
    {
      Store* input = currentStack()->currentStore();
      Store* output = currentStack()->work();
      Point3i r(parm("X Radius").toInt(), parm("Y Radius").toInt(), parm("Z Radius").toInt());
      bool res = run(input, output, r, parm("Steps").toUInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, Point3i radius, uint steps);
  };
  
  /**
   * Uses quickselect to compute the local median of each voxel of the stack.
   */
  class mdxBase_EXPORT MedianStack : public Process 
  {
  public:
    MedianStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Median");
      setDesc("Median of stack data.");
      setIcon(QIcon(":/images/AvgPix.png"));

      addParm("X Radius", "Radius in X axis", "1");
      addParm("Y Radius", "Radius in Y axis", "1");
      addParm("Z Radius", "Radius in Z axis", "1");
    }
  
    bool run()
    {
      Store* input = currentStack()->currentStore();
      Store* output = currentStack()->work();
      Point3i r(parm("X Radius").toInt(), parm("Y Radius").toInt(), parm("Z Radius").toInt());
      bool res = run(input, output, r);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, Point3i radius);
  };
  
  /**
   * Sepearable median-of-medians approximation to median filter
   */
  class mdxBase_EXPORT MedianOfMediansStack : public Process 
  {
  public:
    MedianOfMediansStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Median-of-Medians");
      setDesc("Median-of-medians of stack data.");
      setIcon(QIcon(":/images/AvgPix.png"));

      addParm("X Radius", "Radius in X axis", "1");
      addParm("Y Radius", "Radius in Y axis", "1");
      addParm("Z Radius", "Radius in Z axis", "1");
    }
  
    bool run()
    {
      Store* input = currentStack()->currentStore();
      Store* output = currentStack()->work();
      Point3i r(parm("X Radius").toInt(), parm("Y Radius").toInt(), parm("Z Radius").toInt());
      bool res = run(input, output, r);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, Point3i radius);
  };
  
  /**
   * \class BrightenStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Change the global luminosity of the stack. The value of each voxel will be
   * multiplied by the brightening constant. So a value > 1 will brighten the
   * stack, while a value < 1 will darken the stack
   *
   * Useful examples:
   *  - to convert a stack from 12bits to 16bits, use 16;
   *  - to convert a stack from 8bits to 16bits, use 256.
   */
  class mdxBase_EXPORT BrightenStack : public Process {
  public:
    BrightenStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Brighten Darken");
      setDesc("Brighten or Darken stack.\n"
              "A value > 1 will brighten the stack, < 1 will darken it.\n"
              "To convert from a 12bit to a 16bit stack, use 16.\n"
              "To convert from a 8bit to a 16bit stack, use 256.\n");
      setIcon(QIcon(":/images/Brightness.png"));

      addParm("Amount", "Amount to multiply voxels", "16.0");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output, parm("Amount").toDouble());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, double brightness);
  };
  
  /**
   * \class BinarizeStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Binarize the stack: set the intensity of any voxel greater than the
   * threshold to 65535 and all other voxels to 0.
   */
  class mdxBase_EXPORT BinarizeStack : public Process 
  {
  public:
    BinarizeStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Binarize");
      setDesc("Transform the stack to binary (65535 or 0).\n"
              "All voxels with an intensity greater than the threshold to 65535, "
              "while to others are set to 0.");
      setIcon(QIcon(":/images/Brightness.png"));

      addParm("Threshold", "Voxels above this threshold will be assigned 1.", "5000");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output, parm("Threshold").toUShort());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, ushort threshold);
  };
  
  /**
   * \class ColorGradient StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Compute the gradient of the image in the Z axis of the image.
   */
  class mdxBase_EXPORT ColorGradient : public Process 
  {
  public:
    ColorGradient(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Color Gradient");
      setDesc("Compute color gradient in Z direction");
      setIcon(QIcon(":/images/ColorGrad.png"));

      addParm("Z Divisor", "Factor by which the gradient is divided by.", "5.0");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output, parm("Z Divisor").toFloat());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, float colorGradDiv);
  };
  
  /**
   * \class InvertStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Invert the intensity of all voxels in the stack.
   */
  class mdxBase_EXPORT InvertStack : public Process 
  {
  public:
    InvertStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Invert");
      setDesc("Invert the stack");
      setIcon(QIcon(":/images/Invert.png"));
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output);
  };
  
  /**
   * \class FilterStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Clip the intensity of the stack to the interval [Low Threshold, High Threshold].
   */
  class mdxBase_EXPORT FilterStack : public Process 
  {
  public:
    FilterStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Trim high-low values");
      setDesc("Clip the voxel intensities to the interval [Low Threshold, High Threshold].");
      setIcon(QIcon(":/images/Filter.png"));
  
      addParm("Low Threshold", "Lower bound on voxel value", "1000");
      addParm("High Threshold", "Upper bound on voxel value", "0");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output, parm("Low Threshold").toUInt(), parm("High Threshold").toUInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, uint lowFilter, uint highFilter);
  };
  
  /**
   * \class GaussianBlurStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Blur the stack using a Gaussian kernel or specified radius.
   *
   * The radius given correspond to the standard deviation of the Gaussian
   * kernel along the various axes. The kernel itself will be 3 time as big.
   */
  class mdxBase_EXPORT GaussianBlurStack : public Process 
  {
  public:
    GaussianBlurStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Gaussian Blur Stack");
      setDesc("Blur the stack, radius = 3 x Sigma");
      setIcon(QIcon(":/images/Gaussian.png"));

      addParm("X Sigma", "Sigma in X axis", "0.3");
      addParm("Y Sigma", "Sigma in Y axis", "0.3");
      addParm("Z Sigma", "Sigma in Z axis", "0.3");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      Point3f sigma(parm("X Sigma").toFloat(), parm("Y Sigma").toFloat(), parm("Z Sigma").toFloat());
      bool res = run(input, output, sigma);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, Point3f sigma);
  };

  /**
   * \class DiffGaussiansStack StackProcessFilters.hpp
   *
   * Calculate the difference of Gaussians, i.e. first - second.
   *
   * The radius given correspond to the standard deviation of the Gaussian
   * kernels along the various axes. The kernel itself will be 3 times as big.
   */
  class mdxBase_EXPORT DiffGaussians : public Process 
  {
  public:
    DiffGaussians(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Diff Gaussians");
      setDesc("Difference of Gaussians, first - second");
      setIcon(QIcon(":/images/DiffGaussians.png"));

      addParm("X1 Sigma", "First sigma in X axis", "0.3");
      addParm("Y1 Sigma", "First sigma in Y axis", "0.3");
      addParm("Z1 Sigma", "First sigma in Z axis", "0.3");
      addParm("X2 Sigma", "Second sigma in X axis", "10.0");
      addParm("Y2 Sigma", "Second sigma in Y axis", "10.0");
      addParm("Z2 Sigma", "Second sigma in Z axis", "10.0");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      Point3f sigma1(parm("X1 Sigma").toFloat(), parm("Y1 Sigma").toFloat(), parm("Z1 Sigma").toFloat());
      Point3f sigma2(parm("X2 Sigma").toFloat(), parm("Y2 Sigma").toFloat(), parm("Z2 Sigma").toFloat());
      bool res = run(input, output, sigma1, sigma2);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, Point3f sigma1, Point3f sigma2);
  };

  /**
   * \class SharpenStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Apply an "unsharp" filter to the stack.
   *
   * The unsharp filter consists in subtracting from the image the blured
   * version of itself. The parameters of the filter correspond to the blurring
   * and a multiplicating factor for the blured image.
   */
  class mdxBase_EXPORT SharpenStack : public Process 
  {
  public:
    SharpenStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Sharpen Stack");
      setDesc("Sharpen the stack, radius = 3 x Sigma");
      setIcon(QIcon(":/images/Sharpen.png"));

      addParm("X Sigma", "Sigma in X axis", "0.2");
      addParm("Y Sigma", "Sigma in Y axis", "0.2");
      addParm("Z Sigma", "Sigma in Z axis", "1.0");
      addParm("Amount", "Amount to sharpen", "1.0");
      addParm("Passes", "Number of passes to make", "1");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      Point3f sigma(parm("X Sigma").toFloat(), parm("Y Sigma").toFloat(), parm("Z Sigma").toFloat());
      bool res = run(input, output, sigma, parm("Amount").toFloat(), parm("Passes").toInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, const Point3f &sigma, float amount, int passes);
  };
  
  /**
   * \class ApplyKernelStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Apply a user-defined convolution kernel to the current image.
   *
   * The kernel must be separable, so only 1D kernels for the X, Y and Z axes
   * are specified.
   */
  class mdxBase_EXPORT ApplyKernelStack : public Process 
  {
  public:
    ApplyKernelStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Apply Separable Kernel");
      setDesc("Kernel must have odd number of values");
      setIcon(QIcon(":/images/Kernel.png"));

      addParm("X Kernel", "Kernel in X axis", "-.1 1.2 -.11");
      addParm("Y Kernel", "Kernel in Y axis", "-.1 1.2 -.11");
      addParm("Z Kernel", "Kernel in Z axis", "-.1 1.2 -.11");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      HVecF kernels[3];
      bool ok;
      QString fieldNames[3] = { "X Kernel", "Y Kernel", "Z Kernel" };
      for(int i = 0; i < 3; i++) {
        QString str = QString(parm(fieldNames[i])).trimmed();
        QStringList field_values = str.split(" ", QString::SkipEmptyParts);
        kernels[i].resize(field_values.size());
        for(size_t j = 0; j < (size_t)field_values.size(); ++j) {
          kernels[i][j] = field_values[j].toFloat(&ok);
          if(not ok)
            return setErrorMessage(
              QString("Value %1 of %2 not a valid number.").arg(j).arg(fieldNames[i]));
        }
      }
  
      bool res = run(input, output, kernels[0], kernels[1], kernels[2]);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, const HVecF& kernelX, const HVecF& kernelY, const HVecF& kernelZ);
  };
  
  /**
   * \class NormalizeStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   * Normalize the intensity of the stack.
   *
   * This process first dilates, then blur the image to obtain a local maximum
   * voxel intensity. The value of the current voxel is then divided by this
   * local maximum.
   */
  class mdxBase_EXPORT NormalizeStack : public Process 
  {
  public:
    NormalizeStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Normalize Stack");
      setDesc("Normalize the stack");
      setIcon(QIcon(":/images/Normalize.png"));

      addParm("X Sigma", "Sigma in X axis for pre-blur", "5.0");
      addParm("Y Sigma", "Sigma in Y axis for pre-blur", "5.0");
      addParm("Z Sigma", "Sigma in Z axis for pre-blur", "5.0");
      addParm("X Radius", "Radius in X axis for nomalization", "0.5");
      addParm("Y Radius", "Radius in Y axis for nomalization", "0.5");
      addParm("Z Radius", "Radius in Z axis for nomalization", "0.5");
      addParm("Threshold", "Threshold under which pixels are cleared considered as background", "10000");
      addParm("Blur factor", "Relative contribution of blurred vs unblurred dilation", "0.7");
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      Point3f radius(parm("X Radius").toFloat(), parm("Y Radius").toFloat(), parm("Z Radius").toFloat());
      Point3f sigma(parm("X Sigma").toFloat(), parm("Y Sigma").toFloat(), parm("Z Sigma").toFloat());
      bool res = run(input, output, radius, sigma, parm("Threshold").toUInt(), parm("Blur Factor").toFloat());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, Point3f radius, Point3f sigma, uint threshold, float blurFactor);
  };
  
   /**
   * \class TopHatStack StackProcessFilters.hpp <StackProcessFilters.hpp>
   *
   *
   * Perform the top-hat transformation on a stack. This filter is often used
   * for background subtraction
   */
  class mdxBase_EXPORT TopHatStack : public Process 
  {
  public:
    TopHatStack(const Process& process) : Process(process) 
    {
      setName("Stack/Filters/Top-hat");
      setDesc("Perform Top-hat transformation on stack, used for background subtraction");
      setIcon(QIcon(":/images/TopHat.png"));

      addParm("X Radius", "Radius in X axis", "10");
      addParm("Y Radius", "Radius in Y axis", "10");
      addParm("Z Radius", "Radius in Z axis", "10");
      addParm("Round Nhbd","Use a round (Ellipsoidal) neighborhood", "No", booleanChoice());
    }
  
    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(s, input, output, parm("X Radius").toInt(), parm("Y Radius").toInt(), 
                                parm("Z Radius").toInt(), stringToBool(parm("Round Hnbd")));
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(Stack* stack, const Store* input, Store* output, 
                                uint xradius, uint yradius, uint zradius, bool roundNhbd);
  }; 


  ///@}
}

#endif 
