//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "CuttingSurface.hpp"
#include "Information.hpp"

namespace mdx {
  
  using qglviewer::Vec;
  
  CuttingSurface::CuttingSurface()
    : _mode(PLANE), _drawGrid(false), _size(Point3d(1.0, 1.0, 1.0)), _draw(false),
      _surfSize(15, 15), _changed(false) {} 
  
  CuttingSurface::~CuttingSurface() {}
  
  void CuttingSurface::getSurfPoints(const qglviewer::Frame* stk_frame, 
  								           std::vector<Point3d>& points, int& uSize, int& vSize)
  {
    uSize = _surfSize.x();
    vSize = _surfSize.y();
  
    for(int u = 0; u < uSize; u++)
      for(int v = 0; v < vSize; v++) {
        Point3d p = evalCoord(double(u) / double(uSize - 1), double(v) / double(vSize - 1));
        if(stk_frame)
          points.push_back(Point3d(stk_frame->coordinatesOf(_frame.inverseCoordinatesOf(Vec(p)))));
        else
          points.push_back(p);
      }
  }
  
  
  Point3d CuttingSurface::evalCoord(double u, double v) const
  {
    // Make sure u and v between 0.0 and 1.0
    u = trim(u, 0.0, 1.0);
    v = trim(v, 0.0, 1.0);

    if(_mode == BEZIER)
      return Point3d(bezier().evalCoord(u, v));

    Point3d pos;
    pos.x() = 2 * (u - .5) * _size.x();
    pos.y() = 2 * (v - .5) * _size.y();
    return pos;
  }
  
  Point3d CuttingSurface::evalNormal(double u, double v) const
  {
    // Make sure u and v between 0.0 and 1.0
    u = trim(u, 0.0, 1.0);
    v = trim(v, 0.0, 1.0);
    Point3d du;
    Point3d dv;
    if(u < 0.05)
      du = (evalCoord(u + 0.05, v) - evalCoord(u, v)) / 0.05;
    else if(u > 0.95)
      du = (evalCoord(u, v) - evalCoord(u - 0.05, v)) / 0.05;
    else
      du = (evalCoord(u + 0.05, v) - evalCoord(u - 0.05, v)) / 0.1;
    if(v < 0.05)
      dv = (evalCoord(u, v + 0.05) - evalCoord(u, v)) / 0.05;
    else if(v > 0.95)
      dv = (evalCoord(u, v) - evalCoord(u, v - 0.05)) / 0.05;
    else
      dv = (evalCoord(u, v + 0.05) - evalCoord(u, v - 0.05)) / 0.1;
    return normalized(du ^ dv);
  }
  

  
}
