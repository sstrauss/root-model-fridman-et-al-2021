//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_TOPOLOGY_HPP
#define CC_TOPOLOGY_HPP

#include <CCF.hpp>
#include <Geometry.hpp>
#include <Attributes.hpp>
#include <MDXSubdivide.hpp>

/*
 * Routines to modify the topology of cell complexes
 */
namespace mdx 
{
  /**
   *  Split the edges in a cell complex over a certain max distance in length.
   *  Edges are split in the middle
   *
   * \param cs The cell complex to split edges
   * \param indexAttr The mdx attribute that holds the positions
   * \param edges The edges to be split
   * \param sDiv The subdivision object to propagate values
   *
   * \ingroup CCTopology
   */
  mdxBase_EXPORT
  bool splitEdges(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &edges, Subdivide *sDiv = 0);

  /**
   *  Subdivide triangle with a propagating bisection that gives good triangles
   *
   * \param cs The cell complex for subdivision
   * \param indexAttr The attribute that holds the positions
   * \param faces The faces to be split
   * \param sDiv The subdivision object to propagate values
   *
   * \ingroup CCTopology
   */
  mdxBase_EXPORT 
  bool subdivideBisectTriangle(CCStructure &cs, CCIndexDataAttr &indexData, const CCIndexVec &faces, Subdivide *sDiv = 0,CCIndexTbbSet *propFaces = NULL);

  /**
   *  Subdivide wedges with a propagating bisection that gives good wedges
   *
   * \param cs The cell complex for subdivision
   * \param indexAttr The mdx attribute that holds the positions
   * \param faces The volumes to be split
   * \param sDiv The subdivision object to propagate values
   *
   * \ingroup CCTopology
   */
  mdxBase_EXPORT 
  bool subdivideBisectWedge(CCStructure &cs, CCIndexDataAttr &indexData, const CCIndexVec &volumes, Subdivide *sDiv = 0);

  /**
   *  Convert faces to polygons by label
   *
   * \param cs The input cell complex
   * \param csOut A new cell complex with the cells converted to polygons by label
   * \param indexAttr The mdx attribute that holds the positions
   * \param maxLength The manLength of an edge, 0 keeps all vertices, -1 keeps only junctions
   *
   * \ingroup CCTopology
   */
  bool polygonizeTriangles(const CCStructure &cs, CCStructure &csOut, 
                       CCIndexDataAttr &indexData, const CCIndexVec &faces, double maxLength);

  /**
   *  Subdivide triangles by splitting all edged in half making 4 new triangles.
   *
   * \param cs The cell complex for subdivision
   * \param indexAttr The mdx attribute that holds the positions
   * \param sDiv The subdivision object to propagate values
   *
   * \ingroup CCTopology
   */
  bool subdivideTriangles(CCStructure &cs, CCIndexDataAttr &indexAttr, Subdivide *sDiv);
}

#endif
