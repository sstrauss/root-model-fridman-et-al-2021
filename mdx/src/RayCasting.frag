#define EPSILON 0.00001

uniform sampler2D solid;
uniform sampler2D back;
uniform sampler2D front;
uniform sampler2D frontColor;
uniform sampler2D occlusion;
uniform float slices;

varying vec3 objectPos;

vec4 correctColor(vec4 color, float thickness)
{
  float eqa = (color.a < 1.0) ? ((pow(color.a, thickness)-1.0)/(color.a-1.0)) : 1.0;
  color.rgb *= eqa;
  color.a = pow(color.a, thickness);
  return color;
}

vec4 blendColor(vec4 src, vec4 dst)
{
  return src.a*src + (1-src.a)*dst;
}

vec4 backBlend(vec4 src, vec4 dst)
{
  vec4 ret;
  ret.rgb = src.a*src.rgb*(1-dst.a);
  ret.a = (1-dst.a)*(1-src.a);
  return ret;
}

vec4 blendPremulColor(vec4 src, vec4 tgt)
{
  vec4 ret;
  ret.a = src.a*tgt.a;
  ret.rgb = src.rgb + src.a * tgt.rgb;
  return ret;
}

float equivAlpha(float a0, float a1)
{
  return a0+a1-a0*a1;
}

vec4 equivColor(vec4 far, vec4 near)
{
  float ea = equivAlpha(far.a, near.a);
  vec4 ret;
  ret.rgb = far.rgb + (near.a*near.rgb - near.a*near.rgb)/ea;
  ret.a = ea;
  return ret;
}

void main()
{
  float alpha = 1.0;

  vec3 bboxMax = origin + imageSize;
  vec3 bboxMin = origin;
  vec3 pos = objectPos; // A point on the ray

  vec3 viewDir = vec3(gl_ModelViewMatrix[0][2], gl_ModelViewMatrix[1][2], gl_ModelViewMatrix[2][2]);
  viewDir /= -1.0*length(viewDir); // Orientation of the ray

  // Find intersection of the current ray with the cube, in image coordinate system
  vec2 lspan = vec2(-1e20, 1e20);

  for(int i = 0 ; i < 3 ; ++i) {
    if(fabs(viewDir[i]) > 1e-8) {
      bool inc = viewDir[i] > 0;
      float bbMin = inc ? bboxMin[i] : bboxMax[i];
      float bbMax = inc ? bboxMax[i] : bboxMin[i];
      float minIntersection = (bbMin - pos[i]) / viewDir[i];
      float maxIntersection = (bbMax - pos[i]) / viewDir[i];
      if(minIntersection > lspan.x) lspan.x = minIntersection;
      if(maxIntersection < lspan.y) lspan.y = maxIntersection;
    } else {
      if(pos[i] > bboxMax[i] || pos[i] < bboxMin[i]) {
        discard;
        return;
      }
    }
  }

  if(lspan.x > lspan.y) { 
    discard; 
    return; 
  }
  vec3 startPos = pos + lspan.x * viewDir;
  vec3 endPos = pos + lspan.y * viewDir;

  vec4 realStart = gl_ModelViewMatrix * vec4(startPos, 1);
  vec4 realEnd = gl_ModelViewMatrix * vec4(endPos, 1);

  realStart /= realStart.w;
  realEnd /= realEnd.w;

  // Compute intersection with clipping planes
  rayClip(realStart, realEnd);

  vec4 startPos4 = gl_ModelViewMatrixInverse * realStart;
  vec4 endPos4 = gl_ModelViewMatrixInverse * realEnd;
  startPos = startPos4.xyz / startPos4.w;
  endPos = endPos4.xyz / endPos4.w;

  vec4 screenPos = gl_ModelViewProjectionMatrix * startPos4;
  screenPos.xy = (screenPos.xy / screenPos.w + 1.0) / 2.0;

  float frontAlpha = texture2D(frontColor, screenPos.xy).a;

  // Check if occluded
  bool occluded = texture2D(occlusion, screenPos.xy).a == 0;

  if(occluded || frontAlpha > 0.99) {
    discard;
    return;
  }

  // Check if within projection limits, and adjust start/endpositions
  {
    float depthBack = texture2D(back, screenPos.xy).a;
    float depthSolid = texture2D(solid, screenPos.xy).a;
    float maxDepth = min(depthBack, depthSolid);

    vec4 realPos = screenPos;
    realPos.z = maxDepth;
    realPos.xyz = 2.0*realPos.xyz - 1.0;
    realPos.w = 1.0;
    vec4 limitPos4 = gl_ModelViewProjectionMatrixInverse * realPos;

    vec3 limitPos = limitPos4.xyz / limitPos4.w;
    vec3 dv = endPos - startPos;

    float dv2 = dot(dv, dv);
    float dist = dot(limitPos - startPos, dv);

    if(dist < 0) {
      discard;
      return;
    } else if(dist < dv2)
      endPos = limitPos;
  }

  {
    vec4 realPos = screenPos;
    realPos.z = texture2D(front, screenPos.xy).a;
    realPos.xyz = 2.0*realPos.xyz - 1.0;
    realPos.w = 1.0;
    vec4 limitPos4 = gl_ModelViewProjectionMatrixInverse * realPos;

    vec3 limitPos = limitPos4.xyz / limitPos4.w;
    vec3 dv = startPos - endPos;

    float dv2 = dot(dv, dv);
    float dist = dot(limitPos - endPos, dv);

    if(dist < 0) {
      discard;
      return;
    } else if(dist < dv2)
      startPos = limitPos;
  }

  {
    vec3 startTex = (startPos - origin) / imageSize;
    vec3 endTex = (endPos - origin) / imageSize;

    vec3 dTex = (endTex - startTex);
    vec3 dPos = (endPos - startPos);

    /*float squares = dot(vabs(dTex), texSize);
     */
    vec3 texStep = imageSize / vec3(texSize);
    float unitTexSize = min(texStep.x, min(texStep.y, texStep.z));
    vec3 cs = dPos / unitTexSize;
    float squares = max(abs(cs.x), max(abs(cs.y), abs(cs.z)));

    int sliceCount = int(8*ceil(squares*slices));
    if(sliceCount < 1) sliceCount = 1;

    vec3 curTex = startTex;// + start*dTex;
    vec4 c = vec4(0,0,0,1);// color_premul(curTex);
    vec3 curPos = startPos;// + start*dPos;
    dTex /= float(sliceCount);
    dPos /= float(sliceCount);

    curPos += dPos/2;

    float thickness = length(dPos) / unitTexSize; // squares/float(sliceCount);

    frontAlpha = 1-frontAlpha;

    for(int n = 0 ; n < sliceCount ; ++n) {
      vec4 nc = color(curTex);
      vec4 rc = correctColor(nc, thickness);
      c = blendPremulColor(c, rc);
      /*c = maxColor(c, nc);*/
      /*c = equivColor(c, nc);*/
      /*c = blendColor(nc, c);*/
      /*c = backBlend(nc, c);*/
      /*if(c.a > .95) break;*/
      frontAlpha *= rc.a;
      if(frontAlpha < 0.01)
        break;
      curPos += dPos;
      curTex += dTex;
    }
    c.a = 1-c.a;
    if(c.a > 0)
      c.rgb /= c.a;
    gl_FragColor = c;
  }
}
