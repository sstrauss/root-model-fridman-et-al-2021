//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "ToolsProcessTransform.hpp"
#include <QFileDialog>

using qglviewer::Vec;

namespace mdx 
{
  bool SaveGlobalTransform::initialize(QWidget* parent)
  {
    QString fileName = parm("File Name");
    if(fileName.isEmpty())
      fileName = QFileDialog::getSaveFileName(parent, "Choose transform file to save", QDir::currentPath(),
                                              "Text files (*.txt)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".txt", Qt::CaseInsensitive))
      fileName += ".txt";
    setParm("File Name", fileName);
    return true;
  }
  
  bool SaveGlobalTransform::run(const QString& fileName)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly)) {
      setErrorMessage(QString("File '%1' cannot be opened for writing").arg(fileName));
      return false;
    }
    QTextStream out(&file);
  
    // Get difference between stacks
    qglviewer::Frame f1(getStack(0)->getFrame().worldInverse()), f2;
    f2.setFromMatrix(getStack(1)->getFrame().worldMatrix());
    f2.setReferenceFrame(&f1);
  
    // Write to file
    Matrix4d m(f2.worldMatrix());
    out << transpose(m) << endl;
    file.close();
  
    setStatus(QString("Transform saved to: %1").arg(fileName));
    return true;
  }
  REGISTER_PROCESS(SaveGlobalTransform);
}
