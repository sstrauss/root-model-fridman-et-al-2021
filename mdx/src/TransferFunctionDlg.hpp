//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TRANSFERFUNCTIONDLG_HPP
#define TRANSFERFUNCTIONDLG_HPP

#include <Config.hpp>

#include <QStringList>
#include <QList>

#include <ui_TransferFunctionDlg.h>

class QAbstractButton;

namespace mdx {
  class mdx_EXPORT TransferFunctionDlg : public QDialog {
    Q_OBJECT
  public:
    TransferFunctionDlg(QWidget* parent = 0, Qt::WindowFlags f = 0);
  
    const TransferFunction& transferFunction() const;
  
  public slots:
    void loadSettings(bool changeFunction = true);
    void setTransferFunction(const TransferFunction& fct);
    void setDefaultTransferFunction(const TransferFunction& fct);
    void setStickers(const std::vector<double>& pos);
    void changeHistogram(const std::vector<double>& h);
    void changeBounds(const std::pair<double, double>& bounds);
    void apply();
    void reset();
    void accept();
    void reject();
    int exec();
  
  signals:
    void changedTransferFunction(const TransferFunction& fct);
    void appliedTransferFunction(const TransferFunction& fct);
  
  protected slots:
    void changeTransferFunction(const QString& name);
    void changeTransferFunction(const TransferFunction& fct);
    void on_useChecks_toggled(bool on);
    void on_useWhite_toggled(bool on);
    void on_useBlack_toggled(bool on);
    void on_useRGB_toggled(bool on);
    void on_useHSV_toggled(bool on);
    void on_useCyclicHSV_toggled(bool on);
    void on_selectSelectionColor_clicked();
    void on_functionList_currentIndexChanged(const QString& name);
    void on_exportFunction_clicked();
    void on_importFunction_clicked();
    void on_saveFunction_clicked();
    void on_renameFunction_clicked();
    void on_deleteFunction_clicked();
    void resetFunctionList();
    void on_buttonBox_clicked(QAbstractButton* btn);
  
  protected:
    QColor getColor(QWidget* w);
    void setColor(QWidget* w, const QColor& col);
    bool changeColor(QWidget* w);
  
    TransferFunction current, default_fct;
    Ui::TransferFunctionDlg ui;
    QStringList fct_names;
    QList<TransferFunction> fcts;
  };
}
#endif
