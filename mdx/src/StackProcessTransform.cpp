//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <StackProcessTransform.hpp>

#include <QFileDialog>

namespace mdx 
{
  bool SaveTransform::initialize(QWidget* parent)
  {
    QString fileName = parm("File Name");
    if(fileName.isEmpty())
      fileName = QFileDialog::getSaveFileName(parent, "Choose transform file to save", 
                                          QDir::currentPath(), "Text files (*.txt)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".txt", Qt::CaseInsensitive))
      fileName += ".txt";
    setParm("File Name", fileName);
    return true;
  }
  
  bool SaveTransform::run(Stack* stack, const QString& fileName)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly)) {
      setErrorMessage(QString("File '%1' cannot be opened for writing").arg(fileName));
      return false;
    }
    QTextStream out(&file);
  
    // Write matrix to file
    Matrix4d m(stack->getFrame().worldMatrix());
    out << transpose(m) << endl;
    file.close();
  
    setStatus(QString("Transform saved to file: %1").arg(fileName));
    return true;
  }
  REGISTER_PROCESS(SaveTransform);
  
  bool LoadTransform::initialize(QWidget* parent)
  {
    QString fileName = parm("File Name");
    if(fileName.isEmpty())
      fileName = QFileDialog::getOpenFileName(parent, "Choose transform file to load", QDir::currentPath(),
                                              "Text files (*.txt)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".txt", Qt::CaseInsensitive))
      fileName += ".txt";
    setParm("File Name", fileName);
    return true;
  }
  
  bool LoadTransform::run(Stack* stack, const QString& fileName)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
      setErrorMessage(QString("File '%1' cannot be opened for reading").arg(fileName));
      return false;
    }
    QTextStream out(&file);
  
    // Read in transform
    Matrix4d m;
    QTextStream in(&file);
    in >> m;
    file.close();
  
    // Set frame or transform
    if(stack->showTrans())
      stack->trans().setFromMatrix((~m).c_data());
    else
      stack->frame().setFromMatrix((~m).c_data());
  
    setStatus(QString("Transform loaded from file: %1").arg(fileName));
    return true;
  }
  REGISTER_PROCESS(LoadTransform);
}
