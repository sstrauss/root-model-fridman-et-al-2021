//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef COLOREDITORDLG_HPP
#define COLOREDITORDLG_HPP

#include <Config.hpp>

#include <ui_ColorEditorDlg.h>

namespace mdx 
{
  class Colors;
}

class ColorEditorDlg : public QDialog {
  Q_OBJECT
public:
  ColorEditorDlg(mdx::Colors* colors, QWidget* parent = 0, Qt::WindowFlags f = 0);

protected slots:
  void on_colorsView_doubleClicked(const QModelIndex& idx);
  void on_buttonBox_clicked(QAbstractButton* btn);

protected:
  mdx::Colors* model;
  Ui::ColorEditorDlg ui;
};

#endif
