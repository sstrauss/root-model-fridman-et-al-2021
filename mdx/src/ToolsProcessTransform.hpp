//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TOOLS_PROCESS_TRANSFORM_HPP
#define TOOLS_PROCESS_TRANSFORM_HPP

#include <Process.hpp>

namespace mdx
{
  ///\addtogroup Stack Processes
  ///@{
  
  /**
   * \class SaveGlobalTransform ToolsProcessTransform.hpp <ToolsProcessTransform.hpp>
   *
   * Save the transformation from stack 1 to stack 2 in a file. The file contains 16 numbers:
   *
   * ``m11 m21 m31 m41 m21 m22 m23 m24 m31 m32 m33 m34 m41 m42 m43 m44``
   *
   * The transformation is then defined by the matrix:
   *
   * \f$ M = \left[\begin{array}{cccc} m11 & m12 & m13 & m14 \\
   *                                  m21 & m22 & m23 & m24 \\
   *                                  m31 & m32 & m33 & m34 \\
   *                                  m41 & m42 & m43 & m44 \end{array} \right] \f$
   */
  class mdxBase_EXPORT SaveGlobalTransform : public Process {
    Q_OBJECT
  
  public:
    SaveGlobalTransform(const Process& process) : Process(process) 
    {
      setName("Tools/Transform/Save Global Transform");
      setDesc("Save the global alignment (transform) matrix from one stack to the other into a file");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "Name of file to save transform", "");
    }
    bool initialize(QWidget* parent);
  
    bool run() { return run(parm("File Name")); }
    bool run(const QString& filename);
  };

///@}
}

#endif
