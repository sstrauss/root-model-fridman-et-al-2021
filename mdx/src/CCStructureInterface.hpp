//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2021 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_STRUCTURE_INTERFACE_HPP
#define CC_STRUCTURE_INTERFACE_HPP

#include <Config.hpp>
#include <Types.hpp>
#include <CCIndex.hpp>
#include <CCFClasses.hpp>

namespace mdx {

  template <typename Index>
  struct mdx_EXPORT CCStructureInterface
  {
  public:
    typedef unsigned short Dimension;
    typedef ccf::Flip<Index> FlipI;
    typedef std::vector<FlipI> FlipVectorI;
    typedef typename FlipVectorI::const_iterator FlipVectorIterator;
    typedef ccf::OrientedObject<Index> OrientedCell;
    typedef ccf::OrientedSet<Index> BoundaryChain;
    typedef typename BoundaryChain::const_iterator BoundaryIterator;

    typedef ccf::DimensionMapG<Index> DimensionMap;
    typedef ccf::FlipTowerG<Index> FlipTower;
    typedef ccf::CellTupleG<Index> CellTuple;
    typedef ccf::SplitStructG<Index> SplitStruct;

    // ** Identify this CCStructure

    // Return a text representation of the type of a particular CCStructure.
    virtual QString csType(void) const = 0;

    // ** Cell and dimension queries.

    /// Return the maximum dimension of cells in the CCStructure.
    virtual Dimension maxDimension(void) const = 0;

    /// Queries if the cell with the given index is in the cell complex.
    virtual bool hasCell(Index cell) const = 0;

    /// Returns the dimension of the given cell in this cell complex.
    virtual Dimension dimensionOf(Index cell) const = 0;

    /// Returns a vector of all cells of the given dimension.
    virtual const std::vector<Index>& cellsOfDimension(Dimension d) const = 0;

    /// Returns a vector of all vertices.
    /// (Equivalent to cellsOfDimension(0).)
    inline const std::vector<Index>& vertices() const
    { return cellsOfDimension(0); }

    /// Returns a vector of all edges.
    /// (Equivalent to cellsOfDimension(1).)
    inline const std::vector<Index>& edges() const
    { return cellsOfDimension(1); }

    /// Returns a vector of all faces.
    /// (Equivalent to cellsOfDimension(2).)
    inline const std::vector<Index>& faces() const
    { return cellsOfDimension(2); }

    /// Returns a vector of all volumes.
    /// (Equivalent to cellsOfDimension(3).)
    inline const std::vector<Index>& volumes() const
    { return cellsOfDimension(3); }

    /// Returns the total number of cells in the cell complex.
    virtual unsigned int cellCount(void) const = 0;

    /// Returns the number of cells of the given dimension in the cell complex.
    virtual unsigned int cellCount(Dimension dim) const;

    // ** Queries to the (perhaps synthetic) flip table.

    /// Match the given flip query, returning the matching flips in a std::vector.
    virtual std::vector<FlipI> matchV(const FlipI &query) const = 0;

    /// Match the given flip query, returning the matching flips in a std::vector.
    std::vector<FlipI> matchV
      (Index joint, Index facet0, Index facet1, Index interior) const
    { return matchV(FlipI(joint,facet0,facet1,interior)); }

    /// Return the first match for the given flip query.
    virtual FlipI matchFirst(const FlipI &query) const = 0;

    /// Return the first match for the given flip query.
    FlipI matchFirst
      (Index joint, Index facet0, Index facet1, Index interior) const
    { return matchFirst(FlipI(joint,facet0,facet1,interior)); }

    // ** Topological queries.

    /// Reports whether the given cells are incident.
    virtual bool incident(Index c1, Index c2) const;

    /// Reports whether the given cells are adjacent, i.e. flip to one another.
    virtual bool adjacent(Index c1, Index c2) const;

    /// Reports whether the given cell is on the border of the cell complex.
    /// For maximal-dimensional cells, reports instead if the cell is
    /// adjacent to the outside of the cell complex.
    virtual bool onBorder(Index cell) const;

    /// Returns the relative orientation of the two cells.
    virtual ccf::RO ro(Index c1, Index c2) const;

    /// Returns a set of the cells in the boundary of the given cell.
    virtual std::set<Index> bounds(Index cell) const;

    /// Returns a set of the cells in whose boundary the given cell lies.
    virtual std::set<Index> cobounds(Index cell) const;

    /// Returns a set of the cells adjacent to the given cell.
    virtual std::set<Index> neighbors(Index cell) const;

    /// Returns all cells of the given dimension incident on the given cell.
    virtual std::set<Index> incidentCells(Index cell, Dimension dim) const;

    /// Returns the endpoints of the given edge.
    /// The first element of the pair has positive relative orientation
    /// with respect to the edge.
    virtual std::pair<Index,Index> edgeBounds(Index edge) const;

    /// Returns the oriented boundary chain of the given oriented cell.
    virtual BoundaryChain boundary(OrientedCell ocell) const;

    /// Returns the oriented coboundary chain of the given oriented cell.
    virtual BoundaryChain coboundary(OrientedCell ocell) const;

    /// Returns the highest-dimension common bounding cell of the given cells.
    virtual Index meet(Index c1, Index c2) const;

    /// Returns the highest-dimension common bounding cell of the given cells.
    inline Index meet(Index c1, Index c2, Index c3) const
    { return meet(c1,meet(c2,c3)); }

    /// Returns the highest-dimension common bounding cell of the given cells.
    inline Index meet(Index c1, Index c2, Index c3, Index c4) const
    { return meet(meet(c1,c2),meet(c3,c4)); }

    /// Returns the lowest-dimension common cobounding cell of the given cells.
    virtual Index join(Index c1, Index c2) const;

    /// Returns the lowest-dimension common cobounding cell of the given cells.
    inline Index join(Index c1, Index c2, Index c3) const
    { return join(c1,join(c2,c3)); }

    /// Returns the lowest-dimension common cobounding cell of the given cells.
    inline Index join(Index c1, Index c2, Index c3, Index c4) const
    { return join(join(c1,c2),join(c3,c4)); }

    /// Returns the oriented edge from the first vertex to the second.
    virtual OrientedCell orientedEdge(Index c1, Index c2) const;

    /// Returns the facet between the two maximum-dimension cells,
    /// oriented towards the first.
    virtual OrientedCell orientedFacet(Index c1, Index c2) const;

    /// Returns all cells of the given dimension which are
    /// incident to both input cells.
    virtual std::set<Index> coincidentCells(Index c1, Index c2, Dimension dim) const;


    // ** Alteration operations
    //    These are not expected to be reasonably implementable
    //    in every instance (maybe not any except the original CCF),
    //    but are here so we can provide a useful error message
    //    if the user tries a function that requires them.

    /// Empty a CCStructure, removing all of its cells.
    virtual void clear(void);

    /// Reverses all relative orientations with repsect to the given cell.
    virtual bool reverseOrientation(Index cell);

    /// Adds a new cell to the cell complex, given its boundary.
    virtual bool addCell(Index newCell,
			 BoundaryChain boundary = BoundaryChain(),
			 ccf::RO topRO = ccf::POS);

    /// Deletes the given cell from the cell complex.
    virtual bool deleteCell(Index oldCell);

    /// Splits a cell in the cell complex, given the boundary of the membrane cell.
    virtual bool splitCell(const SplitStruct& ss, BoundaryChain membraneBoundary = BoundaryChain());

    /// Merges two adjacent cells in the cell complex.
    virtual bool mergeCells(SplitStruct& ss);
  };

} // end namespace mdx

#endif // CC_STRUCTURE_INTERFACE_HPP

