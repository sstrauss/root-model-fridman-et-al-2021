//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PRESET_COLORS_HPP
#define PRESET_COLORS_HPP

#include <Color.hpp>

namespace mdx 
{
  namespace PresetColors
  {
    const ColorbVec& labels(void);
    const ColorbVec& jet(void);
    const ColorbVec& frenchFlag(void);
    const ColorbVec& blackbody(void);
    const ColorbVec& uniformJet(void);
    const ColorbVec& helixWarm(void);
    const ColorbVec& helixCool(void);
    const ColorbVec& helixFull(void);
  } // end namespace PresetColors
}
#endif // PRESET_COLORS_HPP
