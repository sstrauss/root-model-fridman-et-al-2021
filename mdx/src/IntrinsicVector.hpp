//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef INTRINSIC_VECTOR_HPP
#define INTRINSIC_VECTOR_HPP

#include <CCF.hpp>
#include <Geometry.hpp>
#include <Attributes.hpp>

#include <CCUtils.hpp>
#include <MeshUtils.hpp>

namespace mdx
{
  // An IntrinsicVector is a vector defined intrinsically in a cell complex;
  // it consists of a cell tuple in the complex and a coordinate in
  // the associated tuple frame.
  // The integer template argument is the intrinsic dimension,
  // not the dimension of the embedding space (which is usually 3).
  template<size_t iDim, typename T = double>
  struct IntrinsicVector
  {
    typedef Vector<iDim,T> VectorT;
    typedef typename VectorT::value_type ScalarT;
    typedef typename CCStructure::CellTuple CellTuple;

    mutable CellTuple tuple;   // mutable because we may have to set the CellStructure
    VectorT vec;

    IntrinsicVector(void) {}
    IntrinsicVector(const IntrinsicVector &iv) : tuple(iv.tuple), vec(iv.vec) {}
    IntrinsicVector(const CellTuple &_tuple, const VectorT &_vec = VectorT())
      : tuple(_tuple), vec(_vec) {}

    bool operator==(const IntrinsicVector &iv) const
    { return tuple == iv.tuple && vec == iv.vec; }

    inline friend QDebug& operator<<(QDebug &out, const IntrinsicVector &iv)
    {
      return (out << "(" << iv.vec << ")@" << ccf::to_string(iv.tuple).c_str());
    }

    // The embed method comes in several flavours:
    // we distinguish between embedding a point and embedding a direction,
    // and also provide methods for pulling positions out of different Attributes.

    // The first pair of methods takes the cell complex the IntrinsicVector is based on
    // and a CCIndexDataAttr for positions.
    inline Point3d embedPoint(const CCStructure &cs,
                              const CCIndexDataAttr &idxAttr) const
    {
      return embedPoint(cs,idxAttr,&CCIndexData::pos);
    }
    inline Point3d embedDirection(const CCStructure &cs,
                                  const CCIndexDataAttr &idxAttr) const
    {
      return embedDirection(cs,idxAttr,&CCIndexData::pos);
    }

    // The second pair of methods pulls positions out of an AttrMap of the vector type.
    // The integral template argument is the dimension of the embedding space,
    // while the typename argument is the scalar of the embedding space,
    // which is not necessarily the same as the IntrinsicVector's scalar.
    template<size_t eDim, typename EScalarT>
    Vector<eDim,EScalarT> embedPoint(const CCStructure &cs,
                                     const AttrMap<CCIndex,Vector<eDim,EScalarT> > &posAttr) const
    {
      typedef Vector<eDim,EScalarT> VectorT;
      tuple.setCellStructure(cs);
      VectorT p0 = posAttr[tuple[0]], emb = p0;
      for(uint i = 0 ; i < iDim ; i++)
      {
        CCStructure::CellTuple tup(tuple);
        for(uint j = i ; j > 0 ; j--) tup.flip(j);
        const VectorT &pi = posAttr[tup.other(0)];
        emb += vec[i] * (pi - p0);
      }
      return emb;
    }

    template<size_t eDim, typename EScalarT>
    Vector<eDim,EScalarT> embedDirection(const CCStructure &cs,
                                         const AttrMap<CCIndex,Vector<eDim,EScalarT> > &posAttr) const
    {
      typedef Vector<eDim,EScalarT> VectorT;
      tuple.setCellStructure(cs);
      VectorT p0 = posAttr[tuple[0]], emb;
      for(uint i = 0 ; i < iDim ; i++)
      {
        CCStructure::CellTuple tup(tuple);
        for(uint j = i ; j > 0 ; j--) tup.flip(j);
        const VectorT &pi = posAttr[tup.other(0)];
        emb += vec[i] * (pi - p0);
      }
      return emb;
    }

    // The third pair of methods pulls positions out of an AttrMap of some struct type,
    // with a pointer-to-member giving the field containing the Vector.
    template<size_t eDim, typename EScalarT, typename EDataT>
    Vector<eDim,EScalarT> embedPoint(const CCStructure &cs,
                                     const AttrMap<CCIndex,EDataT> &posAttr,
                                     Vector<eDim,EScalarT> EDataT::*posMember) const
    {
      typedef Vector<eDim,EScalarT> VectorT;
      tuple.setCellStructure(cs);
      VectorT p0 = posAttr[tuple[0]].*posMember, emb = p0;
      for(uint i = 0 ; i < iDim ; i++)
      {
        CCStructure::CellTuple tup(tuple);
        for(uint j = i ; j > 0 ; j--) tup.flip(j);
        const VectorT &pi = posAttr[tup.other(0)].*posMember;
        emb += vec[i] * (pi - p0);
      }
      return emb;
    }

    template<size_t eDim, typename EScalarT, typename EDataT>
    Vector<eDim,EScalarT> embedDirection(const CCStructure &cs,
                                         const AttrMap<CCIndex,EDataT> &posAttr,
                                         Vector<eDim,EScalarT> EDataT::*posMember) const
    {
      typedef Vector<eDim,EScalarT> VectorT;
      tuple.setCellStructure(cs);
      VectorT p0 = posAttr[tuple[0]].*posMember, emb;
      for(uint i = 0 ; i < iDim ; i++)
      {
        CCStructure::CellTuple tup(tuple);
        for(uint j = i ; j > 0 ; j--) tup.flip(j);
        const VectorT &pi = posAttr[tup.other(0)].*posMember;
        emb += vec[i] * (pi - p0);
      }
      return emb;
    }
  };

  // CellTuple isn't POD, so we have to provide readAttr and writeAttr for IntrinsicVector ourselves.
  template<size_t dim, typename T>
  bool readAttr(IntrinsicVector<dim,T> &data, const QByteArray &ba, size_t &pos)
  {
    bool succ = readAttr(data.vec,ba,pos);
    succ |= readAttr(data.tuple.tuple,ba,pos);
    return succ;
  }

  template<size_t dim, typename T>
  bool writeAttr(const IntrinsicVector<dim,T> &data, QByteArray &ba)
  {
    bool succ = writeAttr(data.vec,ba);
    succ |= writeAttr(data.tuple.tuple,ba);
    return succ;
  }

  // Embed a cell complex with intrinsically-defined positions.
  // Intrinsically-defined positions are held in a IntrinsicVector-keyed AttrMap.
  // Uses the CCIndexDataAttr for both looking up the positions of the embedding cell complex
  // and storing the embedded positions of the intrinsic cell complex.
  template<size_t iDim, typename IScalarT>
  void embedIntrinsicCC(const CCStructure &ics,
                        const AttrMap<CCIndex,IntrinsicVector<iDim,IScalarT> > &iPos,
                        const CCStructure &ecs,
                        CCIndexDataAttr &idxAttr)
  {
    const std::vector<CCIndex> &vertices = ics.vertices();
    #pragma omp parallel for
    for(uint i = 0 ; i < vertices.size() ; i++)
    {
      CCIndex vtx = vertices[i];
      idxAttr[vtx].pos = iPos[vtx].embedPoint(ecs,idxAttr);
    }
    updateGeometry(ics,idxAttr);
  }

  // Typedefs for two and three dimensions.
  typedef IntrinsicVector<2,double> IntrinsicPoint2d;
  typedef IntrinsicVector<3,double> IntrinsicPoint3d;

  typedef AttrMap<CCIndex,IntrinsicPoint2d> IntrinsicPosMap2d;
  typedef AttrMap<CCIndex,IntrinsicPoint3d> IntrinsicPosMap3d;

} // end namespace mdx

#endif // INTRINSIC_VECTOR_HPP
