//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "Clip.hpp"
#include "Geometry.hpp"

namespace mdx {
  Clip::Clip(int id, QObject* parent)
    : QObject(parent), _enable(false), _showGrid(false), _width(0.5f), _gridSize(1.0f),
      _normal(0.f, 0.f, 1.f), _gridSquares(3u), _changed(false)
  {
    computeBasis();
    _clipNo = id;
    _frame.setWheelSensitivity(0.01);
  }
  
  bool Clip::isClipped(const Point3f& p)
  {
    if(!_enable and !_showGrid)
      return false;
  
    Point3f cp(Point3f(_frame.coordinatesOf(qglviewer::Vec(p))));
  
    float d = fabs(cp * _normal);
    return d > _width;
  }
  
  void Clip::computeBasis() {
    getBasisFromPlane(_normal, _xb, _yb, _zb);
  }
  
  Point4f Clip::normalFormPos() const
  {
    Point4f result(normal());
    result[3] = width();
    return result;
  }
  
  Point4f Clip::normalFormNeg() const
  {
    Point4f result(-normal());
    result[3] = width();
    return result;
  }
}
