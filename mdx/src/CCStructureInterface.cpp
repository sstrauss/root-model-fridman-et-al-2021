#include <CCStructureInterface.hpp>

using namespace mdx;

// ** Default implementations of virtual functions.

template <typename Index>
unsigned int CCStructureInterface<Index>::cellCount(Dimension dim) const
{
  return cellsOfDimension(dim).size();
}

template <typename Index>
bool CCStructureInterface<Index>::incident(Index c1, Index c2) const
{
  if(!hasCell(c1))
    throw std::domain_error
      (std::string("Called incident on nonexistent cell ")+ccf::to_string(c1));
  if(!hasCell(c2))
    throw std::domain_error
      (std::string("Called incident on nonexistent cell ")+ccf::to_string(c2));
  if(c1 == c2) return true;
  if(c1 == Index::TOP || c1 == Index::BOTTOM ||
     c2 == Index::TOP || c2 == Index::BOTTOM) return true;
  Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
  if(dim1 == dim2) return false;

  FlipTower tower(*this,c1,c2);
  return tower.isValid();
}

template <typename Index>
bool CCStructureInterface<Index>::adjacent(Index c1, Index c2) const
{
  FlipI ans = matchFirst(Index::Q, c1, c2, Index::Q);
  return ans.isValid();
}

template <typename Index>
bool CCStructureInterface<Index>::onBorder(Index cell) const
{
  if(!hasCell(cell))
    throw std::domain_error
      (std::string("Called onBorder for nonexistent cell ")+ccf::to_string(cell));
  if(!hasCell(Index::INFTY)) return false;
  if(cell == Index::TOP || cell == Index::BOTTOM || cell == Index::INFTY )
    return true;
  Dimension dim = dimensionOf(cell);
  if(dim < maxDimension()) return incident(cell,Index::INFTY);
  else return adjacent(cell,Index::INFTY);
}

template <typename Index>
ccf::RO CCStructureInterface<Index>::ro(Index c1, Index c2) const
{
  // CellStructure \operation{ro} validity checks
  if(!hasCell(c1))
    throw std::domain_error
      (std::string("CellStructure: Called ro "
                   "on nonexistent cell ")+ccf::to_string(c1));
  if(!hasCell(c2))
    throw std::domain_error
      (std::string("CellStructure: Called ro "
                   "on nonexistent cell ")+ccf::to_string(c2));
  if(c1 == c2) throw std::invalid_argument
		 (std::string("CellStructure: Can't find ro "
			      "between cell ")+ccf::to_string(c1)+" and itself");


  if(c1 == Index::BOTTOM || c2 == Index::BOTTOM)
    {
      // CellStructure \operation{ro} special case for \BOTTOM
      Index other = (c1 == Index::BOTTOM) ? c2 : c1;
      Dimension dim = dimensionOf(other);
      if(dim != 0)
	throw std::invalid_argument
	  (std::string("CellStructure: Called for ro "
		       "between BOTTOM and non-vertex ")+ccf::to_string(other));
      return ccf::POS;

    }
  else if(c1 == Index::TOP || c2 == Index::TOP)
    {
      // CellStructure \operation{ro} special case for \TOP
      Index other = (c1 == Index::TOP) ? c2 : c1;
      Dimension dim = dimensionOf(other);
      if(dim != maxDimension())
	throw std::invalid_argument
	  (std::string("CellStructure: Called for ro "
		       "between TOP and non-maxdim cell ")+ccf::to_string(other));
      FlipTower tower(*this,other);
      return tower.ro(dim);

    }
  else
    {
      // CellStructure \operation{ro} general case
      Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
      if(dim2-dim1 != 1 && dim1-dim2 != 1)
	throw std::invalid_argument
	  (std::string("CellStructure: Called ro "
		       "on cells of non-adjacent dimension: ")+ccf::to_string(c1)+
	   " of dimension "+ccf::to_string(dim1)+" and "+ccf::to_string(c2)+
	   " of dimension "+ccf::to_string(dim2));
      FlipTower tower(*this,c1,c2);
      if(tower.isValid())
	return tower.ro(std::min(dim1,dim2));
      else
	throw std::invalid_argument
	  (std::string("CellStructure: Flip tower created to find "
		       "ro was invalid, cells were ")+
	   ccf::to_string(c1)+" and "+ccf::to_string(c2));
    }
}

template <typename Index>
std::set<Index> CCStructureInterface<Index>::bounds(Index cell) const
{
  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::bounds: cell ")+
       ccf::to_string(cell)+" is not in complex");

  std::set<Index> answer;
  std::vector<FlipI> matches = matchV(Index::Q,Index::Q,Index::Q,cell);
  for(typename std::vector<FlipI>::const_iterator iter = matches.begin() ;
      iter != matches.end() ; iter++)
    {
      answer.insert(iter->facet[0]);
      answer.insert(iter->facet[1]);
    }
  // Remove pseudocells from answer in boundary query operation
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}

template <typename Index>
std::set<Index> CCStructureInterface<Index>::cobounds(Index cell) const
{
  // Special case for coboundary of \BOTTOM{}
  if(cell == Index::BOTTOM)
    {
      std::set<Index> answer(cellsOfDimension(0).begin(),
			     cellsOfDimension(0).end());
      return answer;
    }

  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::cobounds: cell ")+
       ccf::to_string(cell)+" is not in complex");

  std::set<Index> answer;
  std::vector<FlipI> matches = matchV(Index::Q,cell,Index::Q,Index::Q);
  for(typename std::vector<FlipI>::const_iterator iter = matches.begin() ;
      iter != matches.end() ; iter++)
    answer.insert(iter->interior);
  // Remove pseudocells from answer in boundary query operation
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}

template <typename Index>
std::set<Index> CCStructureInterface<Index>::neighbors(Index cell) const
{
  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::neighbors: cell ")+
       ccf::to_string(cell)+" is not in complex");

  std::set<Index> answer;
  std::vector<FlipI> matches = matchV(Index::Q,cell,Index::Q,Index::Q);
  for(typename std::vector<FlipI>::const_iterator iter = matches.begin() ;
      iter != matches.end() ; iter++)
    answer.insert(iter->otherFacet(cell));
  // Remove pseudocells from answer in boundary query operation
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}

template <typename Index>
std::set<Index> CCStructureInterface<Index>::incidentCells(Index cell, Dimension dim) const
{
  // Check validity of dimension in incidentCells
  if(dim > maxDimension())
    throw std::domain_error
      (std::string("In CellStructure::incidentCells: dimension ")+
       ccf::to_string(dim)+" higher than maximum dimension of complex");

  // Check validity of cell in incidentCells
  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::incidentCells: cell ")+
       ccf::to_string(cell)+" is not in complex");

  // Special case: all cells are incident to \TOP{} or \BOTTOM{}
  if(cell == Index::TOP || cell == Index::BOTTOM)
    {
      std::set<Index> answer(cellsOfDimension(dim).begin(),
			     cellsOfDimension(dim).end());
      return answer;
    }


  std::set<Index> answer;
  Dimension cellDim = dimensionOf(cell);
  // Indicent cells at same dimension as given cell
  if(dim == cellDim)
    {
      answer.insert(cell);
    }

  else
    // Incident cells one dimension above given cell
    if(dim == (cellDim + 1))
      {
	return cobounds(cell);
      }

    else
      // Incident cells one dimension below given cell
      if(dim == (cellDim - 1))
	{
	  return bounds(cell);
	}

      else
	// Incident cells more than one dimension above given cell
	if(dim > cellDim)
	  {
	    std::set<Index> cells0, cells1;
	    cells1.insert(cell);
	    int currentDimension = cellDim;
	    while(dim > currentDimension)
	      {
		std::swap(cells0,cells1);
		cells1.clear();
		boundUp(cells0,cells1);
		currentDimension++;
	      }
	    std::swap(answer,cells1);
	  }

	else
	  // Incident cells more than one dimension below given cell
	  if(dim < cellDim)
	    {
	      std::set<Index> cells0, cells1, cells2;
	      cells2.insert(cell);
	      int currentDimension = cellDim;
	      while(dim < currentDimension)
		{
		  cells1.clear();
		  std::swap(cells0,cells2);
		  cells2.clear();
		  boundDown(cells0,cells1,cells2);
		  currentDimension -= 2;
		}
	      if(currentDimension == dim) std::swap(answer,cells2);
	      else std::swap(answer,cells1);
	    }


  // Remove pseudocells from answer in boundary query operation
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}

template <typename Index>
std::pair<Index,Index> CCStructureInterface<Index>::edgeBounds(Index edge) const
{
  if(!hasCell(edge))
    throw std::domain_error
      (std::string("In CellStructure::edgeBounds: cell ")+ccf::to_string(edge)+
       " is not in complex");
  else if(dimensionOf(edge) != 1)
    throw std::domain_error
      (std::string("In CellStructure::edgeBounds: cell ")+ccf::to_string(edge)+
       " is not an edge");

  FlipI flip = matchFirst(Index::BOTTOM,Index::Q,Index::Q,edge);
  if(!flip.isValid())
    throw std::runtime_error
      (std::string("In CellStructure::edgeBounds: edge ")+ccf::to_string(edge)+
       " has no 0-flips");
  return std::make_pair(flip.facet[0],flip.facet[1]);
}

template <typename Index>
typename CCStructureInterface<Index>::BoundaryChain
  CCStructureInterface<Index>::boundary(OrientedCell ocell) const
{
  std::set<Index> bd = bounds(~ocell);
  BoundaryChain ans;
  for(typename std::set<Index>::const_iterator iter = bd.begin() ;
      iter != bd.end() ; iter++)
    ans += ocell.orientation() * ro(*iter,~ocell) * (*iter);
  return ans;
}

template <typename Index>
typename CCStructureInterface<Index>::BoundaryChain
  CCStructureInterface<Index>::coboundary(OrientedCell ocell) const
{
  std::set<Index> cobd = cobounds(~ocell);
  BoundaryChain ans;
  for(typename std::set<Index>::const_iterator iter = cobd.begin() ;
      iter != cobd.end() ; iter++)
    ans += ocell.orientation() * ro(~ocell,*iter) * (*iter);
  return ans;
}

template <typename Index>
typename CCStructureInterface<Index>::OrientedCell
  CCStructureInterface<Index>::orientedEdge(Index c1, Index c2) const
{
  std::vector<FlipI> matches = matchV(Index::Q,c1,c2,Index::Q);
  for(unsigned int i = 0 ; i < matches.size() ; i++)
    {
      ccf::RO ornt1 = ro(c1,matches[i].interior), ornt2 = ro(c2,matches[i].interior);
      if(ornt1 != ornt2)
	{
	  if(ornt1 == ccf::POS) return +(matches[i].interior);
	  else return -(matches[i].interior);
	}
    }
  return OrientedCell(Index::UNDEF);
}

template <typename Index>
typename CCStructureInterface<Index>::OrientedCell
  CCStructureInterface<Index>::orientedFacet(Index c1, Index c2) const
{
  std::vector<FlipI> matches = matchV(Index::Q,c1,c2,Index::Q);
  for(unsigned int i = 0 ; i < matches.size() ; i++)
    {
      ccf::RO ornt1 = ro(c1,matches[i].joint), ornt2 = ro(c2,matches[i].joint);
      if(ornt1 != ornt2)
	{
	  if(ornt1 == ccf::POS) return +(matches[i].joint);
	  else return -(matches[i].joint);
	}
    }
  return OrientedCell(Index::UNDEF);
}

template <typename Index>
std::set<Index> CCStructureInterface<Index>::coincidentCells(Index c1, Index c2, Dimension dim) const
{
  std::set<Index> incident1 = incidentCells(c1,dim);
  std::set<Index> incident2 = incidentCells(c2,dim);
  std::set<Index> answer;
  std::set_intersection(incident1.begin(),incident1.end(),
                        incident2.begin(),incident2.end(),
                        std::inserter(answer,answer.begin()));
  return answer;
}

template <typename Index>
void boundDown(const CCStructureInterface<Index> *cs,
	       const std::set<Index>& cells,
	       std::set<Index>& down1, std::set<Index>& down2)
{
  typedef typename CCStructureInterface<Index>::FlipI FlipI;
  for(typename std::set<Index>::iterator icell = cells.begin() ;
      icell != cells.end() ; icell++)
    {
      std::vector<FlipI> matches = cs->matchV(Index::Q,Index::Q,Index::Q,*icell);
      for(typename std::vector<FlipI>::const_iterator iflip = matches.begin() ;
	  iflip != matches.end() ; iflip++)
	{
	  down1.insert(iflip->facet[0]);
	  down1.insert(iflip->facet[1]);
	  down2.insert(iflip->joint);
	}
    }
}

template <typename Index>
Index CCStructureInterface<Index>::meet(Index c1, Index c2) const
{
  // Check for \TOP\ special cases in meet
  if(c1 == Index::TOP && (c2 == Index::TOP || hasCell(c2))) return c2;
  if(c2 == Index::TOP && hasCell(c1)) return c1;

  // Check cells for existence in meet
  if(!hasCell(c1))
    throw std::domain_error
      (std::string("Nonexistent cell ")+ccf::to_string(c1)+" passed to meet");
  if(!hasCell(c2))
    throw std::domain_error
      (std::string("Nonexistent cell ")+ccf::to_string(c2)+" passed to meet");

  // Check for special cases in meet
  if(c1 == Index::BOTTOM || c2 == Index::BOTTOM) return Index::BOTTOM;

  if(c1 == c2) return c1;

  Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
  if(dim1 == 0 && dim2 == 0) return Index::BOTTOM;
  if(dim1 == dim2)
    {
      FlipI flipM = matchFirst(Index::Q,c1,c2,Index::Q);
      if(flipM.isValid()) return flipM.joint;
    }

  // Standardize dimensions of query cells in meet/join
  Index cLow = c1, cHigh = c2;
  Dimension dimLow = dim1, dimHigh = dim2;
  if(dimHigh < dimLow) { std::swap(cHigh,cLow); std::swap(dimHigh,dimLow); }

  // Create lists of bounding cells in meet/join
  std::vector< std::set<Index> > boundLow(maxDimension()+1), boundHigh(maxDimension()+1);
  { std::set<Index> cLowSet; cLowSet.insert(cLow); boundLow[dimLow] = cLowSet; }
  { std::set<Index> cHighSet; cHighSet.insert(cHigh); boundHigh[dimHigh] = cHighSet; }
  int dLow = dimLow, dHigh = dimHigh;

  // Work downward from dimHigh to dimLow in meet
  while(dHigh > dLow)
    {
      // Iterate downward two steps in boundHigh
      std::set<Index> down1, down2;
      boundDown(this,boundHigh[dHigh],down1,down2);
      boundHigh[dHigh-1] = down1;
      if(dHigh != 1) boundHigh[dHigh-2] = down2;
      dHigh -= 2;

    }
  if(boundHigh[dimLow].count(cLow) > 0) return cLow;
  // Work downward from dimLow in meet
  int d = dimLow;
  while(d > 0)
    {
      if(dLow >= d)
	{
	  // Iterate downward two steps in boundLow
	  std::set<Index> down1, down2;
	  boundDown(this,boundLow[dLow],down1,down2);
	  boundLow[dLow-1] = down1;
	  if(dLow != 1) boundLow[dLow-2] = down2;
	  dLow -= 2;

	}
      if(dHigh >= d)
	{
	  // Iterate downward two steps in boundHigh
	  std::set<Index> down1, down2;
	  boundDown(this,boundHigh[dHigh],down1,down2);
	  boundHigh[dHigh-1] = down1;
	  if(dHigh != 1) boundHigh[dHigh-2] = down2;
	  dHigh -= 2;

	}

      while(d > std::max(dLow,dHigh))
	{
	  d -= 1;
	  if(d < 0) return Index::BOTTOM;
	  // Check for common element of boundLow[d] and boundHigh[d]
	  typename std::set<Index>::iterator common =
	    greatestCommonElement(boundLow[d],boundHigh[d]);
	  if(common != boundLow[d].end()) return *common;

	}
    }

  return Index::BOTTOM;
}

template <typename Index>
void boundUp(const CCStructureInterface<Index> *cs,
	     const std::set<Index>& cells,
	     std::set<Index>& up1)
{
  typedef typename CCStructureInterface<Index>::FlipI FlipI;
  for(typename std::set<Index>::iterator icell = cells.begin() ;
      icell != cells.end() ; icell++)
    {
      std::vector<FlipI> matches = cs->matchV(Index::Q,*icell,Index::Q,Index::Q);
      for(typename std::vector<FlipI>::const_iterator iflip = matches.begin() ;
	  iflip != matches.end() ; iflip++)
	up1.insert(iflip->interior);
    }
}

template <typename Index>
Index CCStructureInterface<Index>::join(Index c1, Index c2) const
{
  // Check for \TOP\ special cases in join
  if(c1 == Index::TOP || c2 == Index::TOP) return Index::TOP;

  // Check cells for existence in join
  if(!hasCell(c1))
    throw std::domain_error
      (std::string("Nonexistent cell ")+ccf::to_string(c1)+" passed to join");
  if(!hasCell(c2))
    throw std::domain_error
      (std::string("Nonexistent cell ")+ccf::to_string(c2)+" passed to join");

  // Check for special cases in join
  if(c1 == Index::BOTTOM) return c2;
  if(c2 == Index::BOTTOM) return c1;

  if(c1 == c2) return c1;

  Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
  if(dim1 == maxDimension() && dim2 == maxDimension()) return Index::TOP;
  if(dim1 == dim2)
    {
      std::vector<FlipI> matches = matchV(Index::Q,c1,c2,Index::Q);
      if(!matches.empty())
	{
	  typename std::vector<FlipI>::const_iterator iter = matches.begin();
	  while(iter != matches.end() && iter->interior.isPseudocell()) iter++;
	  if(iter == matches.end()) return Index::INFTY;
	  return iter->interior;
	}
    }

  // Standardize dimensions of query cells in meet/join
  Index cLow = c1, cHigh = c2;
  Dimension dimLow = dim1, dimHigh = dim2;
  if(dimHigh < dimLow) { std::swap(cHigh,cLow); std::swap(dimHigh,dimLow); }

  // Create lists of bounding cells in meet/join
  std::vector< std::set<Index> > boundLow(maxDimension()+1), boundHigh(maxDimension()+1);
  { std::set<Index> cLowSet; cLowSet.insert(cLow); boundLow[dimLow] = cLowSet; }
  { std::set<Index> cHighSet; cHighSet.insert(cHigh); boundHigh[dimHigh] = cHighSet; }
  int dLow = dimLow, dHigh = dimHigh;

  // Work upward from dimLow to dimHigh in join
  while(dLow < dHigh)
    {
      // Iterate upward one step in boundLow
      std::set<Index> up1;
      boundUp(this,boundLow[dLow],up1);
      boundLow[dLow+1] = up1;
      dLow += 1;
    }
  if(boundLow[dimHigh].count(cHigh) > 0) return cHigh;

  // Work upward from dimHigh in join
  int d = dimHigh;
  while(d < maxDimension())
    {
      if(dLow <= d)
	{
	  // Iterate upward one step in boundLow
	  std::set<Index> up1;
	  boundUp(this,boundLow[dLow],up1);
	  boundLow[dLow+1] = up1;
	  dLow += 1;
	}
      if(dHigh <= d)
	{
	  // Iterate upward one step in boundHigh
	  std::set<Index> up1;
	  boundUp(this,boundHigh[dHigh],up1);
	  boundHigh[dHigh+1] = up1;
	  dHigh += 1;
	}

      while(d < std::min(dLow,dHigh))
	{
	  d += 1;
	  if(d > maxDimension()) return Index::TOP;
	  // Check for common element of boundLow[d] and boundHigh[d]
	  typename std::set<Index>::iterator common =
	    greatestCommonElement(boundLow[d],boundHigh[d]);
	  if(common != boundLow[d].end()) return *common;

	}
    }

  return Index::TOP;
}


// ** Alteration operations

template <typename Index>
void alterDefault(CCStructureInterface<Index> *cs, const QString &fname)
{
  throw(QString("Could not alter a CCStructure of type %2 with a call to %1;\n"
		"convert to a CCF_CellStructure first.").arg(fname).arg(cs->csType()));
}

template <typename Index>
void CCStructureInterface<Index>::clear(void)
{ alterDefault(this, "clear"); }

template <typename Index>
bool CCStructureInterface<Index>::reverseOrientation(Index)
{ alterDefault(this, "reverseOrientation"); return false; }

template <typename Index>
bool CCStructureInterface<Index>::addCell(Index, BoundaryChain, ccf::RO)
{ alterDefault(this, "addCell"); return false; }

template <typename Index>
bool CCStructureInterface<Index>::deleteCell(Index)
{ alterDefault(this, "deleteCell"); return false; }

template <typename Index>
bool CCStructureInterface<Index>::splitCell(const SplitStruct&, BoundaryChain)
{ alterDefault(this, "splitCell"); return false; }

template <typename Index>
bool CCStructureInterface<Index>::mergeCells(SplitStruct&)
{ alterDefault(this, "mergeCells"); return false; }
