//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators (e.g. Adam Runions).
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef GEOMMATHUTILS_HPP
#define GEOMMATHUTILS_HPP

#include<Geometry.hpp>

namespace mdx
{

//----Matrix operations and decompositions---//

 void PolarDecomp(Matrix3d &M,Matrix3d &S, Matrix3d &U);

 void SVDDecomp(const Matrix3d &M, Matrix3d &U, Matrix3d &S, Matrix3d &V);

//----Triangle based computations----//

//Compute the deformation gradient of a triangle given current (p[]) and reference (q[]) configurations
//needs to be tested
Matrix3d DefGradient(Point3d p[],Point3d q[]);

//Convienience function for the case where reference configuration is given in 2D only
Matrix3d DefGradient(Point3d p[],Point2d q[]);

//Convienience function for the case where both configurations are given in 2D
//Matrix2d DefGradient(Point2d p[],Point2d q[]);


//A measure of triangle quality (Scale-invariant (smooth) interpolation quality measure)
//Shewchuk, Jonathan. "What is a good linear finite element? interpolation, conditioning, anisotropy, and quality measures (preprint)." University of California at Berkeley 73 (2002): 137.
double triangleQuality(Point3d a,Point3d b,Point3d c);


}

#endif