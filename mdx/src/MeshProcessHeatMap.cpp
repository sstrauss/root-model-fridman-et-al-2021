//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessHeatMap.hpp>
#include <MeshProcessSystem.hpp>

#include <QMessageBox>

#include "ui_HeatMap.h"
#include "ui_LoadHeatMap.h"

#include <cmath>

namespace mdx 
{
  bool HeatMapScale::run(Mesh* mesh, const QString &heatName, const QString &type)
  {
    const IntDoubleAttr& heatAttr = mesh->heatAttr<double>(heatName);
    if(heatAttr.empty())
      return false;

    newHeatName = QString("%1-%2").arg(heatName).arg(type);
    IntDoubleAttr& newHeatAttr = mesh->heatAttr<double>(newHeatName);
    if(type == "Ordinal"){
      std::multimap<double, int> dataMap;
      // Insert into the map
      for(auto pr : heatAttr) 
        dataMap.insert({pr.second, pr.first});

      int counter = 1; // Why does the counter start at 1?
      for(auto pr : dataMap)
        newHeatAttr[pr.second] = counter++;

    } else {
      if(type == "Log")
        for(auto pr : heatAttr)
          newHeatAttr[pr.first] = log(pr.second)/log(10);
      else if(type == "Square")
        for(auto pr : heatAttr)
          newHeatAttr[pr.first] = pr.second*pr.second;
      else if(type == "Sqrt")
        for(auto pr : heatAttr)
          newHeatAttr[pr.first] = sqrt(pr.second);
      else if(type == "Inverse")
        for(auto pr : heatAttr)
          newHeatAttr[pr.first] = pr.second == 0 ? 0 : 1.0/pr.second;
      else if(type == "Pow")
        for(auto pr : heatAttr)
          newHeatAttr[pr.first] = pow(10,pr.second);
      else
        throw(QString("scaleheatMap: Unknown type: %1").arg(type));
    }

    mesh->setHeatBounds(calcBounds(newHeatAttr), newHeatName);
    mesh->setHeatUnit(type + " " + mesh->heatUnit(heatName), newHeatName);
    mesh->updateProperties();

    return true;
  }
  REGISTER_PROCESS(HeatMapScale);

  
//  bool DeleteHeatRangeLabels::run(Mesh* mesh, bool rescale, bool deleteCells, float min, float max)
//  {
//    Point2f& heatMapBounds = mesh->heatMapBounds();
//    if(isNan(min))
//      min = heatMapBounds[0];
//    if(isNan(max))
//      max = heatMapBounds[1];
//    if(min >= max) {
//      setErrorMessage("Error, the minimum must be strictly less than the maximum");
//      return false;
//    }
//    IntFloatAttr& heatAttr = mesh->heatAttr();
//    if(heatAttr.empty())
//      return true;
//    vvGraph& S = mesh->graph();
//    std::vector<vertex> T;
//
//    float vmin = 1, vmax = 0;
//
//    IntSet deleteLabel;
//    for(IntFloatAttr::iterator it = heatAttr.begin(); it != heatAttr.end(); ++it) {
//      float v = it->second;
//      if(v >= min and v <= max)
//        deleteLabel.insert(it->first);
//      else if(rescale) {
//        if(v < vmin)
//          vmin = v;
//        if(v > vmax)
//          vmax = v;
//      }
//    }
//    if(deleteLabel.empty())
//      return true;
//
//    if(rescale and vmin < vmax)
//      heatMapBounds = Point2f(vmin, vmax);
//
//    forall(const vertex& v, S) {
//      if(deleteLabel.count(v->label)) {
//        if(deleteCells)
//          T.push_back(v);
//      }
//    }
//    forall(const vertex& v, T)
//        S.erase(v);
//    forall(int label, deleteLabel)
//        heatAttr.erase(label);
//
//    if(deleteCells) {
//      // At last, erase all vertices not part of a triangle
//      do {
//        T.clear();
//        forall(const vertex& v, S) {
//          if(S.valence(v) < 2)
//            T.push_back(v);
//          else if(S.valence(v) == 2) {
//            const vertex& n = S.anyIn(v);
//            const vertex& m = S.nextTo(v, n);
//            if(!S.edge(n, m))
//              T.push_back(v);
//          }
//        }
//        forall(const vertex& v, T)
//            S.erase(v);
//      } while(!T.empty());
//      mesh->updateLines();
//    }
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(DeleteHeatRangeLabels);
  
  REGISTER_PROCESS(HeatMapSetRange);
  
  bool HeatMapSave::run(Mesh &mesh, const QString &heatName, const QString &labeling, const QString& fileName, 
                                                                          bool useSelection, bool exportLabelings)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
      throw QString("%1: cannot opened file (%2) for writing").arg(name()).arg(fileName);
    QTextStream out(&file);

    CCIndexDataAttr *indexAttr = 0;
    CCStructure *cs = 0;
    if(useSelection) {
      QString ccName = mesh.ccName();
      if(ccName.isEmpty())
        throw QString("%1: No cell complex").arg(name());
      cs = &mesh.ccStructure(ccName);
      indexAttr = &mesh.indexAttr();
    }
    QStringList heatAttrList = heatName == "All" ? mesh.heatAttrList(labeling) : QStringList() << heatName;

    // Get all the labels and check the type
    IntSet labels;
    std::vector<IntDoubleAttr *> heatAttrs;
    std::vector<QString> heatNames;
    for(auto &heat : heatAttrList)
      if(mesh.heatType(heat, labeling) == "Double") {
        heatNames.push_back(heat);
        auto &heatAttr = mesh.heatAttr<double>(heat, labeling);
        if(!useSelection) // If not using the selection, get labels from attributes
          for(auto &pr : heatAttr)
            labels.insert(pr.first);
 
        heatAttrs.push_back(&heatAttr);
      }
    if(heatNames.size() == 0)
      throw QString("%1: No double heat maps available").arg(name());

    // get labels from selection
    if(useSelection) {
      if(!cs or !indexAttr)
        throw QString("%1: Cell complex not set").arg(name());

      for(CCIndex c : selectedCells(*cs, *indexAttr))
        labels.insert((*indexAttr)[c].label);
    }

    std::vector<QString> labelings;
    std::vector<IntIntAttr *> labelingAttrs;
    std::vector<IntQStringAttr *> labelingNames;
    if(exportLabelings) {
      for(auto &labeling :  mesh.labelingAttrList()) {
        if(labeling != "Labels") {
          auto *attr = &mesh.labelMap(labeling);
          if(attr->size() > 0) {
            labelings.push_back(labeling);
            labelingAttrs.push_back(attr);
            labelingNames.push_back(&mesh.labelName(labeling));
          }
        }
      }
      if(labelings.size() == 0)
        exportLabelings = false;
    }

    // Write header
    out << QString("%1,Description").arg(labeling);
    if(exportLabelings)
      for(uint i = 0; i < labelings.size(); i++)
        out << QString(",%1,%1-Label").arg(labelings[i]);

    for(auto &heat : heatNames)
      out << "," << heat;
    out << endl;

    auto &labelName = mesh.labelName(labeling);
    for(int label : labels) {
      out << label << "," << labelName[label];

      if(exportLabelings) {
        for(uint i = 0; i < labelings.size(); i++) {
          auto lp = labelingAttrs[i]->find(label);
          if(lp != labelingAttrs[i]->end())
            out << "," << (*labelingNames[i])[lp->second] << "," << lp->second;
          else
            out << ",,";
        }
      }
      for(uint i = 0; i < heatNames.size(); i++) {
        out << ",";
        auto itr = heatAttrs[i]->find(label);
        if(itr != heatAttrs[i]->end())
          out << itr->second;
      }
      out << endl;
    }

    setStatus(QString("Heat map CSV file written with %1 rows").arg(labels.size()));
    return true;
  }
  REGISTER_PROCESS(HeatMapSave);
  
  bool HeatMapLoad::initialize(QWidget* parent)
  {
    if(!parent) return true;
    Ui_LoadHeatMap ui;
    dlg = new QDialog(parent);
    ui.setupUi(dlg);
    connect(ui.SelectHeatMapFile, SIGNAL(clicked()), this, SLOT(selectFile()));
    connect(ui.HeatMapColumn, SIGNAL(currentIndexChanged(int)), this, SLOT(selectColumn(int)));
    this->ui = &ui;
    ui.HeatMapColumn->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    if(!parm("File Name").isEmpty())
      selectFile(parm("File Name"));
    if(dlg->exec() == QDialog::Accepted) {
      setParm("File Name", ui.HeatMapFile->text());
      setParm("Column", QString::number(ui.HeatMapColumn->currentIndex()));
      return true;
    }
    return false;
  }
  
  void HeatMapLoad::selectFile(const QString& fileName)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
      QMessageBox::critical(dlg, "Error opening file",
                            QString("File '%1' cannot be opened for reading").arg(fileName));
      return;
    }
    QTextStream ss(&file);
    QString line = ss.readLine();
    QStringList fields = line.split(",");
    if(fields.size() < 2) {
      QMessageBox::critical(dlg, "Error reading file",
                            QString("File '%1' should be a CSV file with at least 3 columns").arg(fileName));
      return;
    }
    QRegExp unit(".*\\((.*)\\)\\s*");
    ui->HeatMapColumn->clear();
    column_unit.clear();
    for(int i = 0; i < fields.size(); ++i) {
      QString txt = fields[i].trimmed();
      if(txt[0] == '"' and txt[txt.size() - 1] == '"')
        txt = txt.mid(1, txt.size() - 2).trimmed();
      if(unit.indexIn(txt) == 0)
        column_unit << unit.cap(1);
      else
        column_unit << "";
      ui->HeatMapColumn->addItem(txt);
    }
    if(fields[1].trimmed() == "Neighbor")
      ui->HeatMapColumn->setCurrentIndex(3);
    else
      ui->HeatMapColumn->setCurrentIndex(2);
    ui->HeatMapFile->setText(fileName);
  }
  
  void HeatMapLoad::selectFile()
  {
    QString fileName = QFileDialog::getOpenFileName(dlg, "Choose spreadsheet to load", ui->HeatMapFile->text(),
                                                    "CSV files (*.csv);;All files (*.*)");
    if(!fileName.isEmpty())
      selectFile(fileName);
  }
  
  void HeatMapLoad::selectColumn(int i)
  {
    if(i >= 0 and i < column_unit.size())
      ui->unitLabel->setText(column_unit[i]);
  }
  
  bool HeatMapLoad::run(Mesh* mesh, const QString& fileName, int column, float border_size)
  {
    if(column < 0) {
      setErrorMessage(QString("Invalid column '%1'").arg(column));
      return false;
    }

    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
      setErrorMessage(QString("File '%1' cannot be opened for reading").arg(fileName));
      return false;
    }
    QTextStream ss(&file);
    QString line = ss.readLine();
    QStringList fields = line.split(",");
    if(fields.size() < column + 1) {
      setErrorMessage(
            QString("File '%1' should be a CSV file with at least %2 columns").arg(fileName).arg(column + 1));
      return false;
    }
    QRegExp re_unit(".*\\((.*)\\)\\s*");
    QString unit;
    QString txt = fields[column].trimmed();
    int nb_fields = fields.size();
    if(re_unit.indexIn(txt) == 0)
      unit = re_unit.cap(1);
    QString col2 = fields[1];
    if(col2[0] == '"' and col2[col2.size() - 1] == '"')
      col2 = col2.mid(1, col2.size() - 2).trimmed();
    bool is_wall = col2.trimmed() == "Neighbor";
//    if(is_wall)
//      mesh->updateWallGeometry(border_size);
    float minValue = HUGE_VAL, maxValue = -HUGE_VAL;
    int line_nb = 1;
    IntDoubleAttr &labelMap = mesh->heatAttr<double>("HeatMap");
    IntIntFloatAttr wallMap; // FIXME  = mesh->wallHeat();
    while(ss.status() == QTextStream::Ok) {
      ++line_nb;
      fields = ss.readLine().split(",");
      if(fields.size() != nb_fields or fields[0].isEmpty())
        break;
      bool ok;
      int neighbor = 0;
      int label = fields[0].toInt(&ok);
      if(!ok) {
        setErrorMessage(QString("Error line %1: the first column is not an integer number but '%2'")
                                                           .arg(line_nb).arg(fields[0].trimmed()));
        return false;
      }
      if(is_wall) {
        neighbor = fields[1].toInt(&ok);
        if(!ok) {
          setErrorMessage(QString("Error line %1: the second column is not an integer number but '%2'")
                                                           .arg(line_nb) .arg(fields[1].trimmed()));
          return false;
        }
      }
      float value = fields[column].toFloat(&ok);
      if(!ok) {
        setErrorMessage(QString("Error line %1: the column %2 is not an floating point number but '%3'")
                                           .arg(line_nb) .arg(column) .arg(fields[column].trimmed()));
        return false;
      }
      if(is_wall)
        wallMap[std::make_pair(label, neighbor)] = value;
      else
        labelMap[label] = value;
      if(value < minValue)
        minValue = value;
      if(value > maxValue)
        maxValue = value;
    }
    if(minValue >= maxValue)
      return setErrorMessage("The file contains 0 or 1 value only. We cannot create a heat map from that.");
    mesh->setHeatUnit(unit);
    mesh->setHeatBounds(Point2d(minValue, maxValue));
    mesh->updateProperties();
    setStatus(QString("Loaded heat map with values ranging from %1%3 to %2%3").arg(minValue).arg(maxValue).arg(unit));
    return true;
  }
  REGISTER_PROCESS(HeatMapLoad);

  bool SelectByHeat::run(Mesh* m, double lowerThreshold, double upperThreshold, bool resetSelection)
  {

    // check whats the current heat map
    QString ccName = m->ccName();
    if(ccName.isEmpty())
      throw QString("%1:run No cell complex").arg(name());

    CCStructure &cs = m->ccStructure(ccName);
    CCIndexDataAttr &indexAttr = m->indexAttr();

    QString currentHeatName = m->heat();
    IntDoubleAttr &currentHeatMap = m->heatAttr<double>(currentHeatName);

    // reset if required
    if(cs.maxDimension() == 3 and resetSelection){
      for(CCIndex vol : cs.volumes()){
        indexAttr[vol].selected = false;
      }
    } else if(cs.maxDimension() == 2 and resetSelection){
      for(CCIndex f : cs.faces()){
        indexAttr[f].selected = false;
      }
    }


    if(m->useParents() and cs.maxDimension() == 3){
      for(CCIndex vol : cs.volumes()){
        if(indexAttr[vol].label < 1) continue;
        if(currentHeatMap[m->labelMap("Parents")[indexAttr[vol].label]] >= lowerThreshold and
           currentHeatMap[m->labelMap("Parents")[indexAttr[vol].label]] <= upperThreshold){
          indexAttr[vol].selected = true;
        }
      }
    } else if(m->useParents() and cs.maxDimension() == 2){
      for(CCIndex f : cs.faces()){
        if(indexAttr[f].label < 1) continue;
        if(currentHeatMap[m->labelMap("Parents")[indexAttr[f].label]] >= lowerThreshold and
           currentHeatMap[m->labelMap("Parents")[indexAttr[f].label]] <= upperThreshold){
          indexAttr[f].selected = true;
        }
      }
    } else if(cs.maxDimension() == 3){
      for(CCIndex vol : cs.volumes()){
        if(indexAttr[vol].label < 1) continue;
        if(currentHeatMap[indexAttr[vol].label] >= lowerThreshold and 
           currentHeatMap[indexAttr[vol].label] <= upperThreshold){
          indexAttr[vol].selected = true;
        }
      }
    } else if(cs.maxDimension() == 2){
      for(CCIndex f : cs.faces()){
        if(indexAttr[f].label < 1) continue;
        if(currentHeatMap[indexAttr[f].label] >= lowerThreshold and 
           currentHeatMap[indexAttr[f].label] <= upperThreshold){
          indexAttr[f].selected = true;
        }
      }
    }

    m->updateProperties();

    return true;
  }

 REGISTER_PROCESS(SelectByHeat);

  void fillTreeWidgetWithMeasureProcesses(Mesh* m, QTreeWidget* tree)
  {
    QStringList procs = mdx::listProcesses();

    //ui.mapTree->header()->resizeSection(1, 50);
    //ui.mapTree->header()->resizeSection(0, 300);

    //populateMapTree(mesh);

    forall(const QString& name, procs) {
      mdx::ProcessDefinition* def = mdx::getProcessDefinition(name);
      QStringList list = def->name.split("/");

      if(list[0] == "Mesh" and list[1] == "Heat Map" and list[2].startsWith("Measure")) {
        QTreeWidgetItem *item2 = new QTreeWidgetItem(QStringList() << list[3] + "/" + list[4]);
        item2->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
        tree->addTopLevelItem(item2);
      }
    }

    //ui.mapTree->sortItems(0, Qt::AscendingOrder);
    tree->sortItems(0, Qt::AscendingOrder);
    //for(int i = 0; i < 1; i++)
    //  ui.mapTree->resizeColumnToContents(i);
  }

  void HeatMap::onTreeWidgetClicked(QTreeWidgetItem *item, int column)
  {
  }

  bool HeatMap::initialize(QWidget *parent)
  {
    Ui_HeatMapDialog ui;
    QDialog dlg(parent);
    ui.setupUi(&dlg);

    Mesh* mesh1 = currentMesh();
    if(!mesh1)
      throw(QString("Mesh 1 invalid"));
    Mesh* mesh2 = otherMesh();
    if(!mesh2)
      throw(QString("Mesh 2 invalid"));

    fillTreeWidgetWithMeasures(mesh1, ui.measureProcessTree);

    // fill gui with current parameter
    QTreeWidgetItemIterator it(ui.measureProcessTree);
    while (*it) {
      if ((*it)->text(0) == parm("Measure"))
        (*it)->setSelected(true);
      ++it;
    }

    ui.Mesh1Labeling->setText(mesh1->labeling());
    ui.Mesh2Labeling->setText(mesh2->labeling());
    ui.HeatMapChange->setChecked(stringToBool(parm("Change Map")));

    uint changeDirIndex = ui.ChangeDir->findText(parm("Change Dir"), Qt::MatchExactly);
    if(changeDirIndex >= 0)
      ui.ChangeDir->setCurrentIndex(changeDirIndex);
    uint changeTypeIndex = ui.ChangeType->findText(parm("Change Type"), Qt::MatchExactly);
    if(changeTypeIndex >= 0)
      ui.ChangeType->setCurrentIndex(changeTypeIndex);

    ui.GrowthTime->setValue(parm("Growth Time").toDouble());
    ui.HeatMapRange->setChecked(stringToBool(parm("Custom Range")));
    ui.RangeMin->setValue(parm("Range Min").toDouble());
    ui.RangeMax->setValue(parm("Range Max").toDouble());

    if(dlg.exec() == QDialog::Accepted) {
      // fill parameters
      if(ui.measureProcessTree->currentItem())
        setParm("Measure", ui.measureProcessTree->currentItem()->text(0));

      if(ui.HeatMapChange->isChecked())
        setParm("Change Map", "Yes");
      else
        setParm("Change Map", "No");

      setParm("Change Dir", ui.ChangeDir->currentText());
      setParm("Change Type", ui.ChangeType->currentText());
      setParm("Growth Time", QString::number(ui.GrowthTime->value()));

      if(ui.HeatMapRange->isChecked())
        setParm("Custom Range", "Yes");
      else 
        setParm("Custom Range", "No");

      setParm("Range Min", QString::number(ui.RangeMin->value()));
      setParm("Range Max", QString::number(ui.RangeMax->value()));

      return true;
    }
    return false;
  }

  bool HeatMap::getHeatAttr(Mesh *mesh, const QString &processName, QString &heatName)
  {
    // Make the process
    Process *process = getProcess(processName);
    if(!process){
      // try to get the attr map
      IntDoubleAttr &currentHeatMap = mesh->heatAttr<double>(parm("Measure"));
      heatName = parm("Measure");

      if(currentHeatMap.empty())
        throw(QString("Heat Map doesn't exist and there is no measure process with this name: HeatMap::getHeatAttr Cannot make process: %1").arg(processName));
      return true;
    }

    // Set the current mesh for the measure process
    int currentId = currentMeshId();
    int otherId = otherMeshId();

    if(mesh != currentMesh())
      process->setCurrentMeshId(otherId);
    process->initialize(0);
    process->run();
    heatName = process->parm("Heat Name");
    process->setCurrentMeshId(currentId);

    return true;
  }

  bool HeatMap::run(Mesh* mesh1, Mesh* mesh2, const QString &measure, bool changeMap, 
                              const QString &changeDir, const QString &changeType, double growthTime, QString &heatName)
  {
    qDebug() << "run " << "\n";

    if(measure == "") 
      return setErrorMessage("No measure selected");

    QString measureFolder = "Mesh/Heat Map/";
   
    // Get the measure for mesh 1
    QString heat1Name;
    if(!getHeatAttr(mesh1, measureFolder + measure, heat1Name)) 
      return false;
    IntDoubleAttr &attr1 = mesh1->heatAttr<double>(heat1Name);

    // Get the mesh heat
    if(changeMap) {
      // If doing a change map, get the measure for mesh 1
      QString heat2Name;
      if(!getHeatAttr(mesh2, measureFolder + measure, heat2Name)) 
        return setErrorMessage("Attribute Map does not exist for Mesh 2 and is not a valid Measure Process!");
      IntDoubleAttr &attr2 = mesh2->heatAttr<double>(heat2Name);

      if(changeType == "Ratio") {
        heatName = heat1Name + " " + "Ratio";
        IntDoubleAttr &heatAttr = mesh1->heatAttr<double>(heatName);
        heatAttr.clear();
        if(changeDir == "Decreasing") {
          for(auto &a1 : attr1) {
            auto a2 = attr2.find(a1.first);
            if(a2 == attr2.end())
              continue;
            if(a2->second != 0)
              heatAttr[a1.first] = a1.second/a2->second;
          }
        } else {
          for(auto &a1 : attr1) {
            auto a2 = attr2.find(a1.first);
            if(a2 == attr2.end())
              continue;
            if(a1.second != 0)
              heatAttr[a1.first] = a2->second/a1.second;
          }
        }
      } else if(changeType == "Difference") {
        heatName = heat1Name + " " + "Difference";
        IntDoubleAttr &heatAttr = mesh1->heatAttr<double>(heatName);
        heatAttr.clear();
        if(changeDir == "Decreasing") {
          for(auto &a1 : attr1) {
            auto a2 = attr2.find(a1.first);
            if(a2 == attr2.end())
              continue;
            heatAttr[a1.first] = a2->second - a1.second;
          }
        } else {
          for(auto &a1 : attr1) {
            auto a2 = attr2.find(a1.first);
            if(a2 == attr2.end())
              continue;
            heatAttr[a1.first] = a1.second - a2->second;
          }
        }
      }
    } else // no change map, regular heat map
      heatName = heat1Name;

    return true;
  }
  REGISTER_PROCESS(HeatMap);
}


// Old heat map process (heat map classic in MGX)
//
//  bool ComputeHeatMap::initialize(QStringList& parms, QWidget* parent)
//  {
//    if(!parent) return true;
//    dlg = new QDialog(parent);
//    Ui_HeatMapDialog ui;
//    ui.setupUi(dlg);
//
//    connect(ui.HeatMapType, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(changeMapType(const QString &)));
//    connect(ui.SelectSpreadsheetFile, SIGNAL(clicked()), this, SLOT(selectSpreadsheetFile()));
//
//    for(int i = 0; i < ui.HeatMapType->count(); ++i) {
//      if(ui.HeatMapType->itemText(i) == parms[0]) {
//        ui.HeatMapType->setCurrentIndex(i);
//        break;
//      }
//    }
//    for(int i = 0; i < ui.HeatMapSignal->count(); ++i) {
//      if(ui.HeatMapSignal->itemText(i) == parms[1]) {
//        ui.HeatMapSignal->setCurrentIndex(i);
//        break;
//      }
//    }
//    ui.SpreadsheetFile->setText(parms[2]);
//    if(parms[3].isEmpty()) {
//      ui.CreateSpreadsheet->setChecked(false);
//    } else {
//      QStringList spread = parms[3].split(",");
//      ui.HeatMapRepGeom->setChecked(spread.contains("Geometry"));
//      ui.HeatMapRepSig->setChecked(spread.contains("Signal"));
//      ui.HeatMapRepBord->setChecked(spread.contains("Border"));
//    }
//    ui.HeatMapRange->setChecked(stringToBool(parms[4]));
//    ui.RangeMin->setValue(parms[5].toFloat());
//    ui.RangeMax->setValue(parms[6].toFloat());
//    ui.HeatMapAvg->setChecked(stringToBool(parms[7]));
//    ui.GlobalCoordinates->setChecked(stringToBool(parms[8]));
//    ui.HeatMapPolarity->setChecked(parms[9] != "None");
//    if(ui.HeatMapPolarity->isChecked()) {
//      for(int i = 0; i < ui.HeatMapPolarityType->count(); ++i) {
//        if(ui.HeatMapPolarityType->itemText(i) == parms[9]) {
//          ui.HeatMapPolarityType->setCurrentIndex(i);
//          break;
//        }
//      }
//    }
//
//    ui.HeatMapChange->setChecked(stringToBool(parms[10]));
//    if(parms[11] == "Increasing")
//      ui.HeatMapIncr->setCurrentIndex(0);
//    else
//      ui.HeatMapIncr->setCurrentIndex(1);
//    if(parms[12] == "Ratio")
//      ui.HeatMapDiffType->setCurrentIndex(0);
//    else if(parms[12] == "Difference")
//      ui.HeatMapDiffType->setCurrentIndex(1);
//    else if(parms[12] == "Growth")
//      ui.HeatMapDiffType->setCurrentIndex(2);
//
//    ui.HeatMapGrowthTime->setValue(parms[13].toFloat());
//    borderSize = parms[14].toFloat();
//
//    bool go = dlg->exec() == QDialog::Accepted;
//    if(go) {
//      parms.clear();
//      QStringList spread;
//      if(ui.CreateSpreadsheet->isChecked()) {
//        if(ui.HeatMapRepGeom->isChecked())
//          spread << "Geometry";
//        if(ui.HeatMapRepSig->isChecked())
//          spread << "Signal";
//        if(ui.HeatMapRepBord->isChecked())
//          spread << "Border";
//      }
//      parms << ui.HeatMapType->currentText() << ui.HeatMapSignal->currentText() << ui.SpreadsheetFile->text()
//            << spread.join(",") << (ui.HeatMapRange->isChecked() ? "Yes" : "No")
//            << QString::number(ui.RangeMin->value()) << QString::number(ui.RangeMax->value())
//            << (ui.HeatMapAvg->isChecked() ? "Yes" : "No") << (ui.GlobalCoordinates->isChecked() ? "Yes" : "No")
//            << (ui.HeatMapPolarity->isChecked() ? ui.HeatMapPolarityType->currentText() : QString("None"))
//            << (ui.HeatMapChange->isChecked() ? "Yes" : "No") << ui.HeatMapIncr->currentText()
//            << ui.HeatMapDiffType->currentText() << QString::number(ui.HeatMapGrowthTime->value())
//            << QString::number(borderSize);
//    }
//    delete dlg;
//    dlg = 0;
//    return go;
//  }
//  
//  bool ComputeHeatMap::run(const QStringList &parms)
//  {
//    MapType map;
//    if(parms[0] == "Area")
//      map = AREA;
//    else if(parms[0] == "Volume")
//      map = VOLUME;
//    else if(parms[0] == "Walls")
//      map = WALL;
//    else {
//      setErrorMessage("Error, first string must be one of 'Area', 'Volume' or 'Walls'");
//      return false;
//    }
//    SignalType signal;
//    if(parms[1] == "Geometry")
//      signal = GEOMETRY;
//    else if(parms[1] == "Border signal")
//      signal = BORDER;
//    else if(parms[1] == "Interior signal")
//      signal = INTERIOR;
//    else if(parms[1] == "Border/total")
//      signal = BORDER_TOTAL;
//    else if(parms[1] == "Interior/total")
//      signal = INTERIOR_TOTAL;
//    else if(parms[1] == "Total signal")
//      signal = TOTAL;
//    else {
//      setErrorMessage("Error, second string must be one of 'Geometry', 'Border signal',"
//                      " 'Interior signal', 'Border/total', 'Interior/total' or 'Total signal'");
//      return false;
//    }
//    QString reportFile = parms[2];
//    if(!reportFile.isEmpty() and !reportFile.endsWith(".csv", Qt::CaseInsensitive))
//      reportFile += ".csv";
//    QStringList spread = parms[3].split(",");
//    int report = 0;
//    if(spread.contains("Geometry"))
//      report |= GEOMETRY;
//    if(spread.contains("Signal"))
//      report |= TOTAL;
//    if(spread.contains("Border"))
//      report |= BORDER;
//    bool manualRange = stringToBool(parms[4]);
//    float rangeMin = parms[5].toFloat();
//    float rangeMax = parms[6].toFloat();
//    if(manualRange and rangeMin >= rangeMax) {
//      setErrorMessage("If range is manual, then rangeMin (2nd value) must be less than rangeMax (3rd value)");
//      return false;
//    }
//    bool signalAverage = stringToBool(parms[7]);
//    bool globalCoordinates = stringToBool(parms[8]);
//    PolarityType polarity = NONE;
//    if(parms[9] == "None")
//      polarity = NONE;
//    else if(parms[9] == "Cell Average")
//      polarity = CELL_AVERAGE;
//    else if(parms[9] == "Wall/Min")
//      polarity = WALL_MIN;
//    else {
//      setErrorMessage("Error, 9th string must be one of 'None', 'Cell Average or 'Wall/Min'");
//      return false;
//    }
//
//    MultiMapType multiMapType;
//    float growthTime = parms[13].toFloat();
//    bool hasChange = stringToBool(parms[10]);
//    if(!hasChange)
//      multiMapType = SINGLE;
//    else {
//      bool incr = parms[11] == "Increasing";
//      bool ratio = parms[12] == "Ratio";
//      bool diff = parms[12] == "Difference";
//      bool growth = parms[12] == "Growth";
//      if(growth and growthTime <= 0) {
//        setErrorMessage("Time for growth rate cannot be <= 0)");
//        return false;
//      }
//      if(incr) {
//        if(ratio)
//          multiMapType = INCREASING_RATIO;
//        else if(diff)
//          multiMapType = INCREASING_DIFF;
//        else
//          multiMapType = INCREASING_GROWTH;
//      } else {
//        if(ratio)
//          multiMapType = DECREASING_RATIO;
//        else if(diff)
//          multiMapType = DECREASING_DIFF;
//        else
//          multiMapType = DECREASING_GROWTH;
//      }
//    }
//    float borderSize = parms[14].toFloat();
//
//    // Check the mesh state
//    Mesh* mesh1 = currentMesh(), *mesh2 = 0;
//    if(!checkState().mesh(MESH_NON_EMPTY, mesh1->id())) {
//      setErrorMessage("Current mesh is empty");
//      return false;
//    }
//
//    if(multiMapType != SINGLE) {
//      if(currentMesh() == getMesh(0))
//        mesh2 = getMesh(1);
//      else if(currentMesh() == getMesh(1))
//        mesh2 = getMesh(0);
//      else
//        return false;
//
//      if(!checkState().mesh(MESH_NON_EMPTY, mesh2->id())) {
//        setErrorMessage("Second mesh is empty");
//        return false;
//      }
//    }
//
//    bool res = run(mesh1, mesh2, map, signal, reportFile, report, manualRange, rangeMin, rangeMax, signalAverage,
//                   globalCoordinates, polarity, multiMapType, growthTime, borderSize);
//    return res;
//  }
//  
//  bool ComputeHeatMap::run(Mesh* mesh1, Mesh* mesh2, MapType _map, SignalType _signal, const QString& reportFile,
//                           int _report, bool _manualRange, float _rangeMin, float _rangeMax, bool _signalAverage,
//                           bool _globalCoordinates, PolarityType _polarity, MultiMapType _multiMapType,
//                           float _growthTime, float _borderSize)
//  {
//    mapType = _map;
//    signal = _signal;
//    report = _report;
//    manualRange = _manualRange;
//    rangeMin = _rangeMin;
//    rangeMax = _rangeMax;
//    signalAverage = _signalAverage;
//    globalCoordinates = _globalCoordinates;
//    polarity = _polarity;
//    multiMapType = _multiMapType;
//    growthTime = _growthTime;
//    borderSize = _borderSize;
//
//    bool multiMap = multiMapType != SINGLE;
//    if(multiMap and (!mesh2 or mesh2->graph().empty())) {
//      setErrorMessage("Error, to compute a heat map based on two maps, you must specify two meshes");
//      return false;
//    }
//
//    // Check if the report/signal can actually be computed
//
//    if(mapType == VOLUME) {
//      int anysignal = (BORDER | INTERIOR | TOTAL | BORDER_TOTAL | INTERIOR_TOTAL);
//      if((mesh1 and (mesh1->stack()->empty() or mesh1->stack()->currentStore()->labels()))
//         or (mesh2 and (mesh2->stack()->empty() or mesh2->stack()->currentStore()->labels()))) {
//        if((signal & anysignal)or (report & anysignal))
//          return setErrorMessage("Error, trying to measure 3D signal without a stack being loaded");
//      }
//    }
//
//    QString sigStr = "";
//    if(signalAverage and signal != GEOMETRY)
//      sigStr += "Average ";
//
//    // Heat map signal type
//    switch(signal) {
//    case GEOMETRY:
//      switch(mapType) {
//      case AREA:
//        sigStr += "Area";
//        break;
//      case VOLUME:
//        sigStr += "Volume";
//        break;
//      case WALL:
//        sigStr += "Wall";
//      }
//      break;
//    case BORDER:
//      sigStr += "Border Signal";
//      break;
//    case INTERIOR:
//      sigStr += "Interior Signal";
//      break;
//    case BORDER_TOTAL:
//      sigStr += "Border/Total Signal";
//      break;
//    case INTERIOR_TOTAL:
//      sigStr += "Interior/Total Signal";
//      break;
//    case TOTAL:
//      sigStr += "Total Signal";
//    }
//
//    ImageHeatMaps map1, map2;
//    if(!getLabelMaps(map1, mesh1))
//      return false;
//
//    IntFloatAttr& LabelHeat = mesh1->heatAttr();
//    IntIntFloatAttr& WallHeat = mesh1->wallHeat();
//
//    // If we are doing a change map, get it from the other mesh and take the ratio as the signal
//    // Otherwise just copy the info
//    LabelHeat.clear();
//    WallHeat.clear();
//    if(!multiMap) {
//      if(mapType == AREA or mapType == VOLUME)
//        LabelHeat = map1.LabelHeatAmt;
//      else if(mapType == WALL)
//        WallHeat = map1.WallHeatAmt;
//    } else {
//      switch(multiMapType) {
//      case INCREASING_RATIO:
//        sigStr += " increase ratio";
//        break;
//      case INCREASING_DIFF:
//        sigStr += " increase difference";
//        break;
//      case INCREASING_GROWTH:
//        sigStr += QString(" increase growth(%1)").arg(growthTime);
//        break;
//      case DECREASING_RATIO:
//        sigStr += " decrease ratio";
//        break;
//      case DECREASING_DIFF:
//        sigStr += " decrease difference";
//        break;
//      case DECREASING_GROWTH:
//        sigStr += QString(" decrease growth(%1)").arg(growthTime);
//        break;
//      case SINGLE:
//        break;
//      }
//
//      // Get values from other mesh
//      if(!getLabelMaps(map2, mesh2))
//        return false;
//
//      // Change map for area and volume
//      switch(mapType) {
//      case AREA:
//      case VOLUME: {
//        std::vector<int> LabelDel;
//        forall(const IntFloatPair& p, map1.LabelHeatAmt) {
//          int i = p.first;
//          // Calculate change
//          if(map2.LabelHeatAmt.find(i) != map2.LabelHeatAmt.end()) {
//            switch(multiMapType) {
//            case INCREASING_RATIO:
//              LabelHeat[i] = (map1.LabelHeatAmt[i] == 0 ? 0 : map2.LabelHeatAmt[i] / map1.LabelHeatAmt[i]);
//              break;
//            case DECREASING_RATIO:
//              LabelHeat[i] = (map2.LabelHeatAmt[i] == 0 ? 0 : map1.LabelHeatAmt[i] / map2.LabelHeatAmt[i]);
//              break;
//            case INCREASING_DIFF:
//              LabelHeat[i] = map2.LabelHeatAmt[i] - map1.LabelHeatAmt[i];
//              break;
//            case DECREASING_DIFF:
//              LabelHeat[i] = map1.LabelHeatAmt[i] - map2.LabelHeatAmt[i];
//              break;
//            case INCREASING_GROWTH:
//              if(growthTime == 0 or map1.LabelHeatAmt[i] == 0)
//                LabelHeat[i] = 0;
//              else
//                LabelHeat[i] = (map2.LabelHeatAmt[i] / map1.LabelHeatAmt[i] - 1) / growthTime;
//              break;
//            case DECREASING_GROWTH:
//              if(growthTime == 0 or map2.LabelHeatAmt[i] == 0)
//                LabelHeat[i] = 0;
//              else
//                LabelHeat[i] = (map1.LabelHeatAmt[i] / map2.LabelHeatAmt[i] - 1) / growthTime;
//              break;
//            case SINGLE:
//              break;
//            }
//          } else
//            LabelDel.push_back(i);
//        }
//        // Delete labels not in both meshes
//        forall(int i, LabelDel) {
//          LabelHeat.erase(i);
//          setStatus("MakeHeatMap Warning mesh " << mesh1->userId() << " erasing label " << i);
//        }
//      } break;
//      case WALL: {     // Calculate wall change map
//        std::vector<IntIntPair> WallDel;
//        forall(const IntIntFloatPair& p, map1.WallHeatAmt) {
//          IntIntPair i = p.first;
//          // Calculate change
//          if(map2.WallHeatAmt.find(i) != map2.WallHeatAmt.end()) {
//            switch(multiMapType) {
//            case INCREASING_RATIO:
//              WallHeat[i] = (map1.WallHeatAmt[i] == 0 ? 0 : map2.WallHeatAmt[i] / map1.WallHeatAmt[i]);
//              break;
//            case DECREASING_RATIO:
//              WallHeat[i] = (map2.WallHeatAmt[i] == 0 ? 0 : map1.WallHeatAmt[i] / map2.WallHeatAmt[i]);
//              break;
//            case INCREASING_DIFF:
//              WallHeat[i] = map2.WallHeatAmt[i] - map1.WallHeatAmt[i];
//              break;
//            case DECREASING_DIFF:
//              WallHeat[i] = map1.WallHeatAmt[i] - map2.WallHeatAmt[i];
//            case INCREASING_GROWTH:
//              if(growthTime == 0 or map1.WallHeatAmt[i] == 0)
//                WallHeat[i] = 0;
//              else
//                WallHeat[i] = (map2.WallHeatAmt[i] / map1.WallHeatAmt[i] - 1) / growthTime;
//              break;
//            case DECREASING_GROWTH:
//              if(growthTime == 0 or map2.WallHeatAmt[i] == 0)
//                WallHeat[i] = 0;
//              else
//                WallHeat[i] = (map1.WallHeatAmt[i] / map2.WallHeatAmt[i] - 1) / growthTime;
//              break;
//            case SINGLE:
//              break;
//            }
//          } else
//            WallDel.push_back(i);
//        }
//        // Delete labels not in both meshes
//        forall(IntIntPair i, WallDel) {
//          WallHeat.erase(i);
//          setStatus("MakeHeatMap::Warning:Mesh" << mesh1->userId() << " erasing wall " << i.first << "->"
//                    << i.second);
//        }
//      }
//      }
//    }
//
//    // Calculate ranges if not using user defined range
//    float heatMinAmt = rangeMin;
//    float heatMaxAmt = rangeMax;
//    if(!manualRange) {
//      heatMinAmt = HUGE_VAL, heatMaxAmt = -HUGE_VAL;
//
//      // Find max and min
//      switch(mapType) {
//      case AREA:
//      case VOLUME:
//        forall(const IntFloatPair& p, LabelHeat) {
//          float amt = LabelHeat[p.first];
//          if(amt < heatMinAmt)
//            heatMinAmt = amt;
//          if(amt > heatMaxAmt)
//            heatMaxAmt = amt;
//        }
//        break;
//      case WALL:
//        forall(const IntIntFloatPair& p, WallHeat) {
//          float amt = WallHeat[p.first];
//          if(amt < heatMinAmt)
//            heatMinAmt = amt;
//          if(amt > heatMaxAmt)
//            heatMaxAmt = amt;
//        }
//      }
//    }
//    mesh1->heatMapBounds() = Point2f(heatMinAmt, heatMaxAmt);
//
//    // Find units
//    QString gunits(""), hunits("");
//    switch(mapType) {
//    case WALL:
//      gunits = UM;
//      break;
//    case AREA:
//      gunits = UM2;
//      break;
//    case VOLUME:
//      gunits = UM3;
//      break;
//    }
//    if(!multiMap and signal == GEOMETRY)
//      hunits = gunits;
//
//    mesh1->heatMapUnit() = hunits;
//
//    // Write to file
//    if(report != 0) {
//      QFile file(reportFile);
//      if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
//        setStatus("makeHeatMap::Error:Cannot open output file:" << reportFile);286.7335
//      } else {
//        QTextStream out(&file);
//
//        // Area heat map
//        switch(mapType) {
//        case AREA:
//        case VOLUME: {
//          // Write header
//          out << "Label,Value";
//          if(mapType == AREA) {
//            out << QString(",Area (%1)").arg(UM2);
//            if(report & BORDER) {
//              out << QString(",Border Area (%1),Interior Area (%1)").arg(UM2);
//            }
//          } else {
//            out << QString(",Volume (%1)").arg(UM3);
//            if(report & BORDER) {
//              out << QString(",Border Volume (%1),Interior Volume (%1)").arg(UM3);
//            }
//          }
//          if((report & BORDER)or (report & TOTAL)) {
//            out << ",Total Signal";
//            if(report & BORDER) {
//              out << ",Border Signal,Interior Signal";
//            }
//          }
//          out << ",Center_X,Center_Y,Center_Z";
//          out << endl;
//
//          // Write data
//          IntPoint3fAttr& LabelCenter = mesh1->labelCenter();
//          const qglviewer::Frame& frame = mesh1->stack()->getFrame();
//          forall(const IntFloatPair& p, LabelHeat) {
//            int label = p.first;
//            out << label << "," << LabelHeat[label];
//            out << "," << map1.LabelGeom[label];
//            if(report & BORDER) {
//              out << "," << map1.LabelBordGeom[label];
//              out << "," << map1.LabelIntGeom[label];
//            }
//            if((report & SINGLE)or (report & TOTAL)) {
//              out << "," << map1.LabelTotalSig[label];
//              if(report & BORDER) {
//                out << "," << map1.LabelBordSig[label];
//                out << "," << map1.LabelIntSig[label];
//              }
//            }
//            // Write cell centers
//            if(globalCoordinates) {
//              Point3f tr = Point3f(frame.inverseCoordinatesOf(qglviewer::Vec(LabelCenter[label])));
//              out << "," << tr.x();
//              out << "," << tr.y();
//              out << "," << tr.z();
//            } else {
//              out << "," << LabelCenter[label].x();
//              out << "," << LabelCenter[label].y();
//              out << "," << LabelCenter[label].z();
//            }
//
//            out << endl;
//          }
//        } break;
//        case WALL: {
//          IntIntFloatAttr & WallGeom = mesh1->wallGeom();
//          // Write header
//          out << QString("Label,Neighbor,Value,Wall Length (%1)").arg(UM);
//          if(signal == TOTAL or (report & TOTAL))
//            out << QString(",Wall Border Area (%1),Wall Signal").arg(UM2);
//          out << endl;
//
//          // Write data
//          forall(const IntIntFloatPair& p, WallHeat) {
//            IntIntPair wall = p.first;
//            out << wall.first << "," << wall.second << ",";
//            out << WallHeat[wall] << "," << WallGeom[wall];
//            if(signal == TOTAL or (report & TOTAL))
//              out << "," << map1.WallBordGeom[wall] << "," << map1.WallBordSig[wall];
//            out << endl;
//          }
//        }
//        }
//        // Footer same for all heat maps
//        QString msg = QString("%1 HeatMap results for mesh %2").arg(sigStr).arg(mesh1->userId());
//        out << endl << endl << qPrintable(msg) << endl;
//
//        out << "Signal range: " << heatMinAmt << "-" << heatMaxAmt << " " << hunits << endl;
//        file.close();
//      }
//    }
//
//    // Print out stats
//    int ncells = map1.LabelGeom.size();
//    int acells = map1.LabelTotalSig.size();
//    double totageom = 0, totgeom = 0;
//    if(mapType == AREA or mapType == VOLUME) {
//      forall(const IntFloatPair& p, map1.LabelGeom) {
//        totgeom += map1.LabelGeom[p.first];
//        if(map1.LabelTotalSig.find(p.first) != map1.LabelTotalSig.end())
//          totageom += map1.LabelGeom[p.first];
//      }
//      setStatus("Mesh " << mesh1->userId() << " " << sigStr << " analysis, Cells: " << acells << "/" << ncells << ", "
//                << totageom << "/" << totgeom << " " << gunits);
//    }
//    mdxInfo << "Range min: " << heatMinAmt << " max: " << heatMaxAmt << " " << hunits << endl;
//
//    if(mapType == WALL) {
//      mesh1->setShowLabel("Wall Heat");
//      mesh1->updateLines();
//    } else
//      mesh1->setShowLabel("Label Heat");
//
//    mesh1->updateTriangles();
//    return true;
//  }
//  
//  void ComputeHeatMap::selectSpreadsheetFile()
//  {
//    QLineEdit* edit = dlg->findChild<QLineEdit*>("SpreadsheetFile");
//    if(edit) {
//      QString fileName = edit->text();
//      if(fileName.isEmpty())
//        fileName = "spreadsheet.csv";
//      fileName = QFileDialog::getSaveFileName(dlg, "Select file to save report in", fileName, "CSV files (*.csv)");
//      if(!fileName.isEmpty()) {
//        if(!fileName.endsWith(".csv", Qt::CaseInsensitive))
//          fileName += ".csv";
//        edit->setText(fileName);
//      }
//    } else
//      setStatus("SpreadsheetFile not found ...");
//  }
//  
//  void ComputeHeatMap::changeMapType(const QString& type)
//  {
//    QCheckBox* pol = dlg->findChild<QCheckBox*>("HeatMapPolarity");
//    if(pol)
//      pol->setEnabled(type == "Walls");
//    else
//      setStatus("HeatMapPolarity not found");
//    QComboBox* signal = dlg->findChild<QComboBox*>("HeatMapSignal");
//    if(signal) {
//      QString curtext = signal->currentText();
//      signal->clear();
//      if(type == "Area") {
//        signal->addItem("Geometry");
//        signal->addItem("Border signal");
//        signal->addItem("Interior signal");
//        signal->addItem("Border/total");
//        signal->addItem("Interior/total");
//        signal->addItem("Total signal");
//      } else if(type == "Volume") {
//        signal->addItem("Geometry");
//        signal->addItem("Border signal");
//        signal->addItem("Interior signal");
//        signal->addItem("Border/total");
//        signal->addItem("Interior/total");
//        signal->addItem("Total signal");
//      } else {
//        signal->addItem("Geometry");
//        signal->addItem("Total signal");
//      }
//      for(int i = 0; i < signal->count(); ++i) {
//        if(signal->itemText(i) == curtext) {
//          signal->setCurrentIndex(i);
//          break;
//        }
//      }
//    } else
//      setStatus("HeatMapSignal not found");
//    QCheckBox* border = dlg->findChild<QCheckBox*>("HeatMapRepBord");
//    if(border)
//      border->setEnabled(type == "Area" || type == "Volume");
//    else
//      setStatus("HeatMapRepBord not found");
//  }
//  
//  void getBBox(const HVec3F& pts, Point3f* bBox)
//  {
//    bBox[0] = Point3f(1e10, 1e10, 1e10);
//    bBox[1] = -bBox[0];
//    forall(const Point3f& pt, pts)
//        for(int i = 0; i < 3; i++) {
//      if(pt[i] < bBox[0][i])
//        bBox[0][i] = pt[i];
//      if(pt[i] > bBox[1][i])
//        bBox[1][i] = pt[i];
//    }
//  }
//  
//  bool ComputeHeatMap::getLabelMaps(ImageHeatMaps& map, Mesh* mesh)
//  {
//    const Stack* stack = mesh->stack();
//    int label;
//
//    IntFloatAttr& LabelGeom = map.LabelGeom;
//    IntFloatAttr& LabelBordGeom = map.LabelBordGeom;
//    IntFloatAttr& LabelIntGeom = map.LabelIntGeom;
//    IntFloatAttr& LabelTotalSig = map.LabelTotalSig;
//    IntFloatAttr& LabelBordSig = map.LabelBordSig;
//    IntFloatAttr& LabelIntSig = map.LabelIntSig;
//    IntFloatAttr& LabelHeatAmt = map.LabelHeatAmt;
//    IntHVec3uMap& LabelTris = map.LabelTris;
//    IntVIdSetMap& LabelPts = map.LabelPts;
//
//    IntIntFloatAttr& WallBordGeom = map.WallBordGeom;
//    IntIntFloatAttr& WallBordSig = map.WallBordSig;
//    IntIntFloatAttr& WallHeatAmt = map.WallHeatAmt;
//
//    IntPoint3fAttr& LabelCenter = mesh->labelCenter();
//
//    LabelGeom.clear();
//    LabelBordGeom.clear();
//    LabelIntGeom.clear();
//    LabelTotalSig.clear();
//    LabelBordSig.clear();
//    LabelIntSig.clear();
//    LabelTris.clear();
//    LabelPts.clear();
//    LabelHeatAmt.clear();
//    LabelCenter.clear();
//
//    WallBordGeom.clear();
//    WallBordSig.clear();
//    WallHeatAmt.clear();
//
//    // Get the graph
//    vvGraph& S = mesh->graph();
//
//    // Get the parents
//    IntIntAttr & parents = mesh->labelMap("Parents");
//
//    // Find selected labels
//    std::set<int> SelectedLabels;
//    std::vector<vertex> av = mesh->activeVertices();
//    bool DoSelected = false;
//    if(av.size() != S.size()) {
//      DoSelected = true;
//      forall(const vertex v, av)
//          forall(const vertex& n, S.neighbors(v)) {
//        vertex m = S.nextTo(v, n);
//        if(!S.uniqueTri(v, n, m))
//          continue;
//
//        if(v->label > 0)
//          SelectedLabels.insert(v->label);
//      }
//      setStatus("Notice: Computing heatmap only for " << SelectedLabels.size() << " selected labels.");
//    }
//
//    // Find border vertices for wall maps or area maps that require it
//    bool doborder = false;
//    QList<SignalType> needBorder = QList<SignalType>() << BORDER << BORDER_TOTAL << INTERIOR << INTERIOR_TOTAL;
//    if((mapType == AREA or mapType == VOLUME)and ((report & BORDER) or needBorder.contains(signal)))
//      doborder = true;
//
//    // Find all vertices within a limited distance from the walls
//    if(doborder or mapType == WALL)
//      mesh->updateWallGeometry(borderSize);
//
//    if(mapType == AREA or mapType == VOLUME) {
//      // For volumes, delete labels with open meshes
//      std::set<int> LabelDel;
//
//      // Loop over triangles
//      forall(const vertex& v, S) {
//        forall(const vertex& n, S.neighbors(v)) {
//          vertex m = S.nextTo(v, n);
//          if(!S.uniqueTri(v, n, m))
//            continue;
//          label = mesh->getLabel(v, n, m, parents);
//          if(label <= 0)
//            continue;
//          if(DoSelected and SelectedLabels.count(label) == 0)
//            continue;
//
//          if(mapType == AREA) {
//            // Total area, signal, and label center
//            float area = triangleArea(v->pos, n->pos, m->pos);
//            LabelGeom[label] += area;
//            LabelCenter[label] += area * Point3f(v->pos + n->pos + m->pos) / 3.0;
//            LabelTotalSig[label] += area * (v->signal + n->signal + m->signal) / 3.0;
//
//            // Calculate how much of triangle is in border area
//            if(doborder) {
//              int border = 0;
//              if(v->minb != 0)
//                border++;
//              if(n->minb != 0)
//                border++;
//              if(m->minb != 0)
//                border++;
//              // Border area and signal
//              float bamt = float(border) / 3.0;
//              LabelBordSig[label] += area * bamt * (v->signal + n->signal + m->signal) / 3.0;
//              LabelBordGeom[label] += area * bamt;
//
//              // Interior area and signal
//              float iamt = float(3 - border) / 3.0;
//              LabelIntSig[label] += area * iamt * (v->signal + n->signal + m->signal) / 3.0;
//              LabelIntGeom[label] += area * iamt;
//            }
//          } else if(mapType == VOLUME) {
//            // For volume just grab the triangle list
//            // If any vertices of triangle are not the same as the label, then the label
//            // does not define a closed volume, add it to deletion list
//            // RSS: Fix this for parents
//            if(v->label != label or n->label != label or m->label != label)
//              LabelDel.insert(label);
//
//            // Calculate volume enclosed by mesh and save triangles
//            float volume = signedTetraVolume(v->pos, n->pos, m->pos);
//            LabelGeom[label] += volume;
//            LabelCenter[label] += volume * Point3f(v->pos + n->pos + m->pos) / 4.0;
//
//            // Save triangles and points if processing volume signal
//            if(signal != GEOMETRY or (report & TOTAL) or (report & BORDER)) {
//              LabelTris[label].push_back(Point3u(v.id(), n.id(), m.id()));
//              LabelPts[label].insert(v.id());
//              LabelPts[label].insert(n.id());
//              LabelPts[label].insert(m.id());
//            }
//          }
//        }
//      }
//
//      // Do the volume heat map signal calculation
//      if(mapType == VOLUME) {
//        // Remove labels which define volume calculations on open meshes
//        forall(int i, LabelDel) {
//          LabelTotalSig.erase(i);
//          setStatus("MakeHeatMap::Warning: Mesh " << mesh->userId() << " open mesh for Vol calc, erasing label "
//                    << i);
//        }
//
//        // Get mesh volume fluorescence
//        if(signal != GEOMETRY or (report & TOTAL) or (report & BORDER)) {
//          // Get current store
//          const Store* store = mesh->stack()->currentStore();
//          if(!store) {
//            setErrorMessage("Volume signal calculation requires associated store");
//            return false;
//          }
//          const HVecUS& data = store->data();
//
//          // Clear previous geometries and use pixel based volumes, keep the centers
//          LabelGeom.clear();
//          LabelBordGeom.clear();
//          LabelIntGeom.clear();
//
//          // Loop over labels
//          uint step = 0;
//          progressStart(QString("Finding signal inside mesh %1").arg(mesh->userId()), LabelGeom.size());
//
//          forall(const IntPoint3fPair& p, LabelCenter) {
//            int label = p.first;
//
//            // Process triangle and point lists
//            IntIntMap vmap;
//            HVec3F pts(LabelPts[label].size());
//            int idx = 0;
//            forall(const intptr_t u, LabelPts[label]) {
//              vertex v(u);
//              pts[idx] = Point3f(v->pos);
//              vmap[u] = idx++;
//            }
//            // Calculate triangle normals.
//            idx = 0;
//            HVec3F nrmls(LabelTris[label].size());
//            // Convert triangle index list to new vertex list
//            forall(Point3u& tri, LabelTris[label]) {
//              tri = Point3u(vmap[tri.x()], vmap[tri.y()], vmap[tri.z()]);
//              if(tri.x() > pts.size() or tri.y() > pts.size() or tri.z() > pts.size()) {
//                setErrorMessage("Error, bad triangle index");
//                return false;
//              }
//              Point3f a = pts[tri.x()], b = pts[tri.y()], c = pts[tri.z()];
//              nrmls[idx++] = ((b - a) ^ (c - a)).normalize();
//            }
//
//            // Get bounding box
//            Point3f bBoxW[2];
//            getBBox(pts, bBoxW);
//            BoundingBox3i bBoxI(stack->worldToImagei(bBoxW[0]),
//                stack->worldToImagei(bBoxW[1]) + Point3i(1, 1, 1));
//            Point3i imgSize(stack->size());
//            bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
//            Point3i size = bBoxI.size();
//
//            // call cuda
//            if(!progressAdvance(step++))
//              userCancel();
//            HVecUS totMask(size_t(size.x()) * size.y() * size.z(), 0),
//                bordMask(size.x() * size.y() * size.z(), 0);
//            if(insideMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, LabelTris[label], nrmls,
//                             totMask))
//              return false;
//            if(!progressAdvance(0))
//              userCancel();
//
//            if(doborder) {
//              if(nearMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, nrmls, 0, borderSize,
//                             bordMask))
//                return false;
//              if(!progressAdvance(0))
//                userCancel();
//            }
//
//            // Count interior, border, and total pixels
//            uint borderPix = 0, interiorPix = 0, totalPix = 0;
//
//            // sum results
//            for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
//              for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
//                int xIdx = stack->offset(bBoxI[0].x(), y, z);
//                for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
//                  size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
//                      - bBoxI[0].x();
//                  if(totMask[mIdx]) {
//                    LabelTotalSig[label] += data[xIdx];
//                    totalPix++;
//                    if(doborder and bordMask[mIdx]) {
//                      borderPix++;
//                      LabelBordSig[label] += data[xIdx];
//                    } else {
//                      interiorPix++;
//                      LabelIntSig[label] += data[xIdx];
//                    }
//                  }
//                }
//              }
//
//            // Calculate volume and average signal per label
//            float pixVolume = stack->step().x() * stack->step().y() * stack->step().z();
//
//            LabelGeom[label] = totalPix * pixVolume;
//            LabelBordGeom[label] = borderPix * pixVolume;
//            LabelIntGeom[label] = interiorPix * pixVolume;
//
//            LabelTotalSig[label] *= pixVolume;
//            LabelBordSig[label] *= pixVolume;
//            LabelIntSig[label] *= pixVolume;
//          }
//        }
//      }
//
//      // Fill in heatmap color
//      forall(const IntFloatPair& p, LabelGeom) {
//        int label = p.first;
//
//        // Calculate label center
//        LabelCenter[label] = (LabelGeom[label] == 0 ? Point3f(0, 0, 0) : LabelCenter[label] / LabelGeom[label]);
//
//        float totalSignal, bordSignal = 0, intSignal = 0;
//
//        // Set signal amounts, average over area if required
//        totalSignal = LabelTotalSig[label];
//        if(doborder) {
//          bordSignal = LabelBordSig[label];
//          intSignal = LabelIntSig[label];
//        }
//        if(signalAverage) {
//          totalSignal = (LabelGeom[label] == 0 ? 0 : totalSignal / LabelGeom[label]);
//          if(doborder) {
//            bordSignal = (LabelBordGeom[label] == 0 ? 0 : bordSignal / LabelBordGeom[label]);
//            intSignal = (LabelIntGeom[label] == 0 ? 0 : intSignal / LabelIntGeom[label]);
//          }
//        }
//
//        // Fill in heat depending on user choice
//        switch(signal) {
//        case GEOMETRY:
//          LabelHeatAmt[label] = LabelGeom[label];
//          break;
//        case BORDER:
//          LabelHeatAmt[label] = bordSignal;
//          break;
//        case BORDER_TOTAL:
//          LabelHeatAmt[label] = (totalSignal == 0 ? 0 : bordSignal / totalSignal);
//          break;
//        case INTERIOR:
//          LabelHeatAmt[label] = intSignal;
//          break;
//        case INTERIOR_TOTAL:
//          LabelHeatAmt[label] = (totalSignal == 0 ? 0 : intSignal / totalSignal);
//          break;
//        case TOTAL:
//          LabelHeatAmt[label] = totalSignal;
//          break;
//        }
//      }
//    } else if(mapType == WALL) {   // Do Wall heatmap
//      IntIntFloatAttr& WallGeom = mesh->wallGeom();
//
//      forall(const IntIntFloatPair& wall, WallGeom)
//          LabelGeom[wall.first.first] += wall.second;
//
//      // Find area, and signal on each wall
//      if(signal == TOTAL or (report & TOTAL)) {
//        forall(const vertex& v, S) {
//          forall(const vertex& n, S.neighbors(v)) {
//            vertex m = S.nextTo(v, n);
//            if(!S.uniqueTri(v, n, m))
//              continue;
//
//            // Sum signal and area
//            IntIntPair wall;
//            int label = mesh->getLabel(v, n, m, parents);
//            if(mesh->isBordTriangle(label, v, n, m, wall)) {
//              float area = triangleArea(v->pos, n->pos, m->pos);
//              WallBordGeom[wall] += area;
//              WallBordSig[wall] += (v->signal + n->signal + m->signal) / 3.0 * area;
//              if(polarity != NONE) {
//                LabelBordGeom[wall.first] += area;
//                LabelBordSig[wall.first] += (v->signal + n->signal + m->signal) / 3.0 * area;
//              }
//            }
//          }
//        }
//      }
//
//      forall(const IntIntFloatPair& wall, WallGeom) {
//        // Fill in heat depending on user choice
//        switch(signal) {
//        case GEOMETRY:
//          // Heat based on wall length
//          if(polarity == CELL_AVERAGE)
//            WallHeatAmt[wall.first]
//                = (LabelGeom[wall.first.first] == 0 ? 0 : WallGeom[wall.first] / LabelGeom[wall.first.first]);
//          else
//            WallHeatAmt[wall.first] = WallGeom[wall.first];
//          break;
//        case TOTAL:
//          // Heat based on wall signal
//          WallHeatAmt[wall.first] = WallBordSig[wall.first];
//          // Divide by area if required
//          if(signalAverage)
//            WallHeatAmt[wall.first]
//                = (WallBordGeom[wall.first] == 0 ? 0 : WallHeatAmt[wall.first] / WallBordGeom[wall.first]);
//          // Do relative to cell average
//          if(polarity == CELL_AVERAGE) {
//            if(signalAverage)
//              WallHeatAmt[wall.first]
//                  = (LabelBordSig[wall.first.first] == 0 or LabelBordGeom[wall.first.first] == 0
//                  ? 0
//                  : WallHeatAmt[wall.first]
//                  / (LabelBordSig[wall.first.first] / LabelBordGeom[wall.first.first]));
//            else
//              WallHeatAmt[wall.first]
//                  = (LabelBordSig[wall.first.first] == 0 ? 0 : WallHeatAmt[wall.first]
//                  / LabelBordSig[wall.first.first]);
//          }
//          break;
//        case BORDER:
//        case INTERIOR:
//        case BORDER_TOTAL:
//        case INTERIOR_TOTAL:
//          break;
//        }
//      }
//
//      // Maps for polarity calc relative to minimum wall
//      if(polarity == WALL_MIN) {
//        IntFloatAttr LabelMin;
//        // Find min heat per cell
//        forall(const IntIntFloatPair& wall, WallHeatAmt)
//            if(LabelMin.find(wall.first.first) == LabelMin.end())
//            LabelMin[wall.first.first] = wall.second;
//        else if(LabelMin[wall.first.first] > wall.second)
//          LabelMin[wall.first.first] = wall.second;
//        // Divide heat amounts
//        forall(const IntIntFloatPair& wall, WallHeatAmt)
//            WallHeatAmt[wall.first]
//            = (LabelMin[wall.first.first] == 0 ? 0 : WallHeatAmt[wall.first] / LabelMin[wall.first.first]);
//      }
//    }
//    return true;
//  }
//  REGISTER_PROCESS(ComputeHeatMap);

