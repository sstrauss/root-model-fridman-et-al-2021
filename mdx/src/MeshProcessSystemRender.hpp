//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef MESH_PROCESS_SYSTEM_RENDER_HPP
#define MESH_PROCESS_SYSTEM_RENDER_HPP

/**
 * \file MeshProcessSystem.hpp
 *
 * System processes for cell complexes
 */
#include <Process.hpp>
#include <ColorMap.hpp>

namespace mdx
{
  // Process to render cell complexes
  class mdxBase_EXPORT Render : public Process
  {
  public:
    Render(const Process &process) : Process(process)
    {
      setName("Mesh/System/Render");
      setDesc("Draw a cell complex.");
      setIcon(QIcon(":/images/Default.png"));

      addParm("Mesh Id", "Id of the mesh to draw, empty means current", "");
      addParm("CC Name", "Cell complex name to render", "");
      addParm("Default Choices", "Include default draw choices", "Yes", booleanChoice());
      addParm("Selected Edges", "Include a choice for selected edges", "No", booleanChoice());
      addParm("Cell Edges", "Include a choice for drawing cell outlines (edges)", "Yes", booleanChoice());
      addParm("Face Normals", "Use face normals instead of vertex normals", "No", booleanChoice());
      addParm("Triangle Fan", "Render faces with a triangle fan from the centroid", "Yes", booleanChoice());
      addParm("Blending", "Blend label color with signal when blending enabled, (0.0-1.0)", "0.8");
      addParm("Point Size", "Size of points (vertices)", "1.0");
      addParm("Line Width", "Width of lines (edges)", "1.0");
      addParm("Axis Offset", "Amount to offset axis lines", "0.02");
      addParm("Default Color", "Default color", "128 128 128 255");
    }

    // Initialize
    bool initialize(QWidget* parent);

    // Finalize is where the work is done
    virtual bool finalize(QWidget* parent);

    // Run just sets the render process
    bool run();

    // Rewind, this will reset the color maps to default
    bool rewind(QWidget* parent);

    /// These can be overriden to change the drawing behavior
    virtual bool defaultDrawChoices(Mesh &mesh, const QString &ccName);

    virtual bool updateRenderChoices(Mesh &mesh, const QString &ccName);

    virtual bool topologyChanged(Mesh &mesh, const QString &ccName);
    virtual bool positionsChanged(Mesh &mesh, const QString &ccName);
    virtual bool propertiesChanged(Mesh &mesh, const QString &ccName);
    virtual bool vertexSelectChanged(Mesh &mesh, const QString &ccName);
    virtual bool faceSelectChanged(Mesh &mesh, const QString &ccName);
    virtual bool volumeSelectChanged(Mesh &mesh, const QString &ccName);
    virtual bool selectPositionsChanged(Mesh &mesh, const QString &ccName);
    virtual bool faceLabelChanged(Mesh &mesh, const QString &ccName);
    virtual bool cellEdgesChanged(Mesh &mesh, const QString &ccName);

    // These can be used just to override the colors

    //virtual bool setVertexColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset);
    //virtual Colorb setVertexColor(CCIndex v, const ColorMap &colorMap);
    //virtual bool setFaceVertexColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset);
    //virtual Colorb setFaceVertexColor(CCIndex f, const ColorMap &colorMap);
    //virtual bool setFaceSignalColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset);
    virtual Colorb setFaceSignalColor(CCIndex f, VizAttribute<CCIndex> &signalAttr);
    //virtual bool setFaceHeatColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset);
    //virtual Colorb setFaceHeatColor(CCIndex f, const ColorMap &colorMap);
    //virtual bool setVolumeSignalColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset);
    //virtual Colorb setVolumeSignalColor(CCIndex f, const ColorMap &colorMap);
    //virtual bool setVolumeHeatColorChoice(Mesh &mesh, const QString &ccName, const QString &colorMapName, bool reset);
    //virtual Colorb setVolumeHeatColor(CCIndex f, const ColorMap &colorMap);

  protected:
    void faceLabelSignalHeatColor(Mesh &mesh, CCIndexData &fIdx, Colorb &signalColor, Colorb &labelColor, Colorb &heatColor);
 
    /// The selection color
    virtual Colorb selectionColor(const Colorb &color);

    Mesh *mesh = 0;
    QString ccName;
    bool DefaultChoices;
    bool SelectedEdges;
    bool CellEdges;
    bool FaceNormals;
    bool TriangleFan;
    double PointSize = 1.0;
    double LineWidth = 1.0;
    double AxisOffset = 1.0;
    Colorb DefaultColor = Colorb(128, 128, 128, 255);

    CCIndexDataAttr *indexAttr = 0;
    QString oldSignal, oldHeat;   // used to reselect changed signal/heat choices
  };

  // Process to render cell complexes with polarity
  class mdxBase_EXPORT RenderPolarity : public Render
  {
  public:
    // Process to render mesh visualizing cell polarity
    RenderPolarity(const Process &process) : Render(process)
    {
      setName("Mesh/System/Render Polarity");
      setDesc("Draw a cell complex.");
      setIcon(QIcon(":/images/Default.png"));

      addParm("Width", "Width of area to visualize polarity", "0.5");
      addParm("Round Corners", "Round the corners of the cells", "Yes", booleanChoice());
      addParm("Polarity Attribute", "Attribute to get polarity values", "Polarity");
    }
    virtual bool defaultDrawChoices(Mesh &mesh, const QString &ccName);

    virtual Colorb setFacePolarityColor(CCIndex f, VizAttribute<CCIndex> &signalAttr);

    virtual bool positionsChanged(Mesh &mesh, const QString &ccName);
    virtual bool propertiesChanged(Mesh &mesh, const QString &ccName);
    virtual bool faceSelectChanged(Mesh &mesh, const QString &ccName);
    virtual bool volumeSelectChanged(Mesh &mesh, const QString &ccName);

    CCSignedIndexDoubleAttr *signedSignalAttrDouble = 0;

  protected:
    double Width;
    bool DoCorners;

    ColorMap *polColors = 0;
  };

  // Process to render cell complexes
  class mdxBase_EXPORT RenderSetEmptyColor : public Process
  {
  public:
    RenderSetEmptyColor(const Process &process) : Process(process)
    {
      setName("Mesh/System/Render Set Empty Color");
      setDesc("Set the default color on a signal colormap");
      setIcon(QIcon(":/images/Default.png"));

      addParm("Empty Color", "Color to use for empty signal", "128 128 128 255");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh)
        throw QString("RenderSetEmptyColor::run No current mesh");
      mesh->signalColorMap().setDefaultColor(stringToColorb(parm("Empty Color")));
      mesh->updateProperties();

      return true;
    }
  };
}
#endif

