//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "StackProcessAnalyses.hpp"

#include <QFile>
#include <QTextStream>
#include "Misc.hpp"

namespace mdx 
{
  bool ComputeVolume::run()
  {
    Store* input = currentStack()->currentStore();
    return run(input, parm("File Name"));
  }
  bool ComputeVolume::run(const Store* input, const QString &fileName)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
      return setErrorMessage(QString("Error, cannot open file '%1' for writing").arg(fileName));
    std::vector<unsigned long> volumes(1 << 16);   // List of volumes per label
    const Stack* stk = input->stack();
    float dv = stk->step().x() * stk->step().y() * stk->step().z();   // volume of a voxel
    const HVecUS& data = input->data();
    for(uint i = 0; i < data.size(); ++i) {
      ushort label = data[i];
      ++volumes[label];
    }
  
    QTextStream ts(&file);
    ts << QString("Label,Volume (%1)").arg(UM3) << endl;
    for(uint lab = 1; lab < 1 << 16; ++lab) {
      if(volumes[lab] > 0)
        ts << lab << "," << (volumes[lab] * dv) << endl;
    }
    file.close();
    return true;
  }
  
  REGISTER_PROCESS(ComputeVolume);
}
