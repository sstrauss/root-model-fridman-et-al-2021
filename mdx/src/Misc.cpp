//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.

/**
 * Misc.cpp - handling of resource paths.
 * Note: paths returned by these functions should not change depending on the filesystem state - from they user's perspective they should be hardcoded (e.g. don't use QDir.cd()).
 * That will reduce the number of possible places where files can hide and make installations uniform.
 */
#include "Misc.hpp"

#include <QTextStream>
#include <QCoreApplication>
#include <QString>
#include <QDir>
#include <QStandardPaths>
#include <iostream>
#include <QRegExp>
#include <QFileInfo>

#include <Config.hpp>
#include "Information.hpp"
#include "Dir.hpp"

#ifndef WIN32
#  include <unistd.h>
#  include <errno.h>
#endif


using namespace std;

namespace mdx 
{
  /**
   * @brief Returns path to resources
   */
  QDir resourceDir()
  {
    return QDir(MDXRESPATH);
  }
  
  /**
   * @brief Returns paths where the user can keep their processes.
   */
  QList<QDir> userProcessDirs()
  {
    QList<QDir> result;

    QDir dir = QDir::home().absoluteFilePath(".mdx/processes");
    result << dir;
    // This check doesn't really belong here inside the call. The create should be attempted once at most.
    if (!QDir().mkpath(dir.path())) {
      qWarning() << "Failed to create processes directory " << dir.path();
    }

    result << QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation)).absoluteFilePath("processes");
    return result;
  }
  
  /**
   * @brief Returns directories where process files should be placed.
   *
   * Searches directories passed in MDXPROCPATH env variable in addition to default ones.
   */
  QList<QDir> processDirs()
  {
    QStringList paths = QString(MDXPROCPATH).split(":");
    const char *mdxprocpath = getenv("MDXPROCPATH");
    if (mdxprocpath != NULL) {
        paths << QString(mdxprocpath).split(":");
    }

    QList<QDir> result;
    foreach (QString path, paths) {
      result << QDir(path); // resolves paths relative to working directory
    }
    result << userProcessDirs();
    return result;
  }
  
  QDir includeDir()
  {
    return QDir(MDXPROCINCLPATH);
  }
  
  QDir libDir()
  {
    return QDir(MDXLIBPATH);
  }
  
  QDir docDir()
  {
    return QDir(MDXDOCPATH);
  }
}
