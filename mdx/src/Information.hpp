//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef INFORMATION_HPP
#define INFORMATION_HPP

#include <Config.hpp>

#include <QString>
#include <QtDebug>
#include <QtGlobal>
#include <Vector.hpp>
#include <CCIndex.hpp>

#include <QLoggingCategory>

#include <functional>

class QMainWindow;

// Macros for logging to the terminal
Q_DECLARE_LOGGING_CATEGORY(mdxLogging)
#if QT_VERSION < QT_VERSION_CHECK(5, 5, 0)
#  define mdxInfo qWarning(mdxLogging).nospace()
#else
#  define mdxInfo qInfo(mdxLogging).nospace()
#endif
#define mdxWarning qWarning(mdxLogging).nospace()
#define mdxCritical qCritical(mdxLogging).nospace()
#define mdxFatal qFatal(mdxLogging).nospace()
#define mdxDebug qDebug(mdxLogging).nospace()

Q_DECLARE_LOGGING_CATEGORY(mdxStatusBarLog)
#define mdxStatus qWarning(mdxStatusBarLog).nospace()

#define DebugOut printf("DebugOut: %s %d\n", __FILE__, __LINE__);
// This is slow because it opens the file each time, so it should be used sparingly
#define DebugFile {FILE *f = fopen("MDXDebug.txt", "a"); if(f) {fprintf(f, "DebugFile: %s %d\n", __FILE__, __LINE__); fclose(f);}}

namespace mdx 
{
  mdx_EXPORT extern bool DEBUG;

  namespace Information 
  {
    void mdx_EXPORT msgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    
    mdx_EXPORT void init(std::function<void(QString,bool)> _setStatus);
    mdx_EXPORT void setStatus(const QString& text, bool alsoPrint = true);
  }

  // Put these here, Vector class cannot include Information (and Qt stuff)
  template <size_t dim, class T> 
  inline QString toQString(const Vector<dim, T> &vec)
  {
    QString result;
    for(size_t i = 0; i < dim; i++) {
      result.append(QString::number(vec.c_data()[i]));
      if(i != (dim - 1))
        result.append(" ");
    }
    return result;
  }
  template <size_t dim, class T> 
  inline QDebug& operator<<(QDebug& output, const Vector<dim, T> &vec)
  {
    return output << toQString(vec);
  }
}
#endif
