//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CLIP_REGION_H
#define CLIP_REGION_H

/**
 * \file ClipRegion.hpp
 * This file contains the definition of a clipping region
 */

#include <Config.hpp>
#include <GL.hpp>

#include <Clip.hpp>
#include <Colors.hpp>
#include <Geometry.hpp>
#include <MDXViewer/qglviewer.h>
#include <Parms.hpp>

#include <iostream>

namespace mdx 
{
  typedef Color<float> Color3f;
  
  /**
   * \class ClipRegion ClipRegion.hpp <ClipRegion.hpp>
   *
   * Class to handle rotatable pair of clipping planes.
   */
  class mdx_EXPORT ClipRegion {
  public:
    ClipRegion();
    ~ClipRegion() {}
  
    Clip* clip;
  
    void setClip(Clip* c);
  
    /**
     * OpenGL identifier for the first clipping plane
     */
    GLenum clip0;
    /**
     * OpenGL identifier for the second clipping plane
     */
    GLenum clip1;
  
    Colors::ColorType GridColor;   ///< Color used to draw the grid
  
    /// Read clipping plane parameters
    void readParms(Parms& parms, QString section);
  
    /// write parms to file
    void writeParms(QTextStream& pout, QString section);
  
    /// Draw (use) clipping place (ax + by + cz + d = 0)
    void disable()
    {
      glfuncs->glDisable(clip0);
      glfuncs->glDisable(clip1);
    }
  
    /// Draw (use) clipping place (ax + by + cz + d = 0)
    void drawClip();
  
    /// Draw (use) clipping place (ax + by + cz + d = 0)
    void drawGrid(float width);
  };
}
#endif 
