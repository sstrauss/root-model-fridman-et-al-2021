#ifndef CCF_CELL_STRUCTURE_HPP
#define CCF_CELL_STRUCTURE_HPP

#include <CCIndex.hpp>
#include <CCFClasses.hpp>
#include <CCStructureInterface.hpp>

namespace mdx {
  namespace ccf {
    /***
     *** CellStructure and all of its methods
     ***/
    /* CellStructure class declaration */
    /// A CellStructure collects all of the information about
    /// a cell complex's structure and implements methods for
    /// modifying and querying its structure.
    template<typename Index>
    struct mdx_EXPORT CellStructure : public CCStructureInterface<Index>
    {
      /* CellStructure type aliases */
      typedef Flip<Index> FlipI;
      typedef FlipTable<Index> FlipTableI;
      typedef typename FlipTableI::const_iterator FlipTableIterator;
      typedef std::vector<FlipI> FlipVectorI;
      typedef typename FlipVectorI::const_iterator FlipVectorIterator;
      typedef unsigned short Dimension;
      typedef OrientedObject<Index> OrientedCell;
      typedef OrientedSet<Index> BoundaryChain;
      typedef typename BoundaryChain::const_iterator BoundaryIterator;

      typedef DimensionMapG<Index> DimensionMap;
      typedef FlipTowerG<Index> FlipTower;
      typedef CellTupleG<Index> CellTuple;
      typedef SplitStructG<Index> SplitStruct;

      /* CellStructure member variables */
      FlipTable<Index> flips;
      DimensionMap dimensionMap;
      Dimension highestDimension;
      const static Dimension HIGHEST_DIMENSION = 64;
      mutable std::string error;

      // The name of this type of CCStructure
      static QString csTypeName;
      inline QString csType(void) const { return csTypeName; }

      /* CellStructure accessor functions */
      inline Dimension maxDimension(void) const { return highestDimension; }

      /* CellStructure buildBoundary method declaration */
      bool buildBoundary(Index newCell, BoundaryChain cellBoundary,
                         FlipVectorI& addFlips,
                         FlipVectorI& delFlips, RO topRO = POS) const;

      /* CellStructure boundUp method declaration */
      void boundUp(const std::set<Index>& cells, std::set<Index>& up1) const;

      /* CellStructure boundDown method declaration */
      void boundDown(const std::set<Index>& cells,
                     std::set<Index>& down1, std::set<Index>& down2) const;

      /* CellStructure constructor */
      CellStructure(Dimension _maxDim = 2)
        : dimensionMap(_maxDim), highestDimension(_maxDim) {}

      /* CellStructure clear operation */
      void clear(void)
      {
        flips.clear();
        dimensionMap.clear();
      }

      /* CellStructure reflected methods */
      /// Matches the given flip query, returning the matching flips in a std::vector.
      using CCStructureInterface<Index>::matchV;
      std::vector<FlipI> matchV(const FlipI& query) const { return flips.matchV(query); }

      /// Returns the first match for the given flip query.
      using CCStructureInterface<Index>::matchFirst;
      FlipI matchFirst(const FlipI& query) const { return flips.matchFirst(query); }

      /* CellStructure query operations */
      /* CellStructure hasCell operation */
      /// Queries if the cell with the given index is in the cell complex.
      inline bool hasCell(Index cell) const
      {
        if(cell.isPseudocell())
          return flips.hasCell(cell);
        else
          return dimensionMap.hasCell(cell);
      }

      /* CellStructure dimensionOf operation */
      /// Returns the dimension of the given cell in this cell complex.
      Dimension dimensionOf(Index cell) const
      {
        if(cell == Index::INFTY) return maxDimension();
        else if(cell.isPseudocell())
          throw std::domain_error
            (std::string("Called dimensionOf on pseudocell ")+to_string(cell));

        return dimensionMap.dimensionOf(cell);
      }

      /* CellStructure cells operation */
      /// Returns a vector of all cells of the given dimension.
      const std::vector<Index>& cellsOfDimension(Dimension d) const
      {
        return dimensionMap.cellsOfDimension(d);
      }

      /* CellStructure cellCount operation */
      /// Returns the total number of cells in the cell complex.
      unsigned int cellCount(void) const
      {
        return dimensionMap.size();
      }
      /// Returns the total number of cells of the given dimension.
      unsigned int cellCount(Dimension dim) const
      {
        if(dim <= maxDimension())
          return dimensionMap.cellsByDimension[dim].size();
        else return 0;
      }

      /* CellStructure incident operation */
      /// Reports whether the given cells are incident.
      bool incident(Index c1, Index c2) const
      {
        if(!hasCell(c1))
          throw std::domain_error
            (std::string("Called incident on nonexistent cell ")+to_string(c1));
        if(!hasCell(c2))
          throw std::domain_error
            (std::string("Called incident on nonexistent cell ")+to_string(c2));
        if(c1 == c2) return true;
        if(c1 == Index::TOP || c1 == Index::BOTTOM ||
           c2 == Index::TOP || c2 == Index::BOTTOM) return true;
        Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
        if(dim1 == dim2) return false;

        FlipTower tower(*this,c1,c2);
        return tower.isValid();
      }

      /* CellStructure adjacent operation */
      /// Reports whether the given cells are adjacent, i.e. flip to one another.
      bool adjacent(Index c1, Index c2) const
      {
        FlipI ans = matchFirst(Index::Q, c1, c2, Index::Q);
        return ans.isValid();
      }

      /* CellStructure onBorder operation */
      /// Reports whether the given cell is on the border of the cell complex.
      /// For maximal-dimensional cells, reports instead if the cell is
      /// adjacent to the outside of the cell complex.
      bool onBorder(Index cell) const
      {
        if(!hasCell(cell))
          throw std::domain_error
            (std::string("Called onBorder for nonexistent cell ")+to_string(cell));
        if(!hasCell(Index::INFTY)) return false;
        if(cell == Index::TOP || cell == Index::BOTTOM || cell == Index::INFTY )
          return true;
        Dimension dim = dimensionOf(cell);
        if(dim < maxDimension()) return incident(cell,Index::INFTY);
        else return adjacent(cell,Index::INFTY);
      }

      /* CellStructure ro operation */
      /// Returns the relative orientation of the two cells.
      RO ro(Index c1, Index c2) const;

      /* CellStructure boundary queries */
      /// Returns a set of the cells in the boundary of the given cell.
      std::set<Index> bounds(Index cell) const;
      /// Returns a set of the cells in whose boundary the given cell lies.
      std::set<Index> cobounds(Index cell) const;
      /// Returns a set of the cells adjacent to the given cell.
      std::set<Index> neighbors(Index cell) const;
      /// Returns all cells of the given dimension incident on the given cell.
      std::set<Index> incidentCells(Index cell, Dimension dim) const;

      /* CellStructure edgeBounds operation */
      /// Returns the endpoints of the given edge.
      /// The first element of the pair has positive relative orientation
      /// with respect to the edge.
      std::pair<Index,Index> edgeBounds(Index edge) const
      {
        if(!hasCell(edge))
          throw std::domain_error
            (std::string("In CellStructure::edgeBounds: cell ")+to_string(edge)+
             " is not in complex");
        else if(dimensionOf(edge) != 1)
          throw std::domain_error
            (std::string("In CellStructure::edgeBounds: cell ")+to_string(edge)+
             " is not an edge");

        FlipI flip = matchFirst(Index::BOTTOM,Index::Q,Index::Q,edge);
        if(!flip.isValid())
          throw std::runtime_error
            (std::string("In CellStructure::edgeBounds: edge ")+to_string(edge)+
             " has no 0-flips");
        return std::make_pair(flip.facet[0],flip.facet[1]);
      }

      /* CellStructure oriented boundary operation */
      /// Returns the oriented boundary chain of the given oriented cell.
      BoundaryChain boundary(OrientedCell ocell) const
      {
        std::set<Index> bd = bounds(~ocell);
        BoundaryChain ans;
        for(typename std::set<Index>::const_iterator iter = bd.begin() ;
            iter != bd.end() ; iter++)
          ans += ocell.orientation() * ro(*iter,~ocell) * (*iter);
        return ans;
      }

      /* CellStructure oriented coboundary operation */
      /// Returns the oriented coboundary chain of the given oriented cell.
      BoundaryChain coboundary(OrientedCell ocell) const
      {
        std::set<Index> cobd = cobounds(~ocell);
        BoundaryChain ans;
        for(typename std::set<Index>::const_iterator iter = cobd.begin() ;
            iter != cobd.end() ; iter++)
          ans += ocell.orientation() * ro(~ocell,*iter) * (*iter);
        return ans;
      }

      /* CellStructure reverseOrientation operation declaration */
      /// Reverses all relative orientations with repsect to the given cell.
      bool reverseOrientation(Index cell);

      /* CellStructure meet and join operation declarations */
      using CCStructureInterface<Index>::meet;
      using CCStructureInterface<Index>::join;
      /// Returns the highest-dimension common bounding cell of the given cells.
      Index meet(Index c1, Index c2) const;
      /// Returns the lowest-dimension common cobounding cell of the given cells.
      Index join(Index c1, Index c2) const;

      /// Returns the oriented edge from the first vertex to the second.
      OrientedCell orientedEdge(Index c1, Index c2) const;
      OrientedCell orientedFacet(Index c1, Index c2) const;

      std::set<Index> coincidentCells(Index c1, Index c2, Dimension dim) const;

      /* CellStructure addCell operation declaration */
      /// Adds a new cell to the cell complex, given its boundary.
      bool addCell(Index newCell, BoundaryChain boundary = BoundaryChain(), RO topRO = POS);

      /* CellStructure deleteCell operation declaration */
      /// Deletes the given cell from the cell complex.
      bool deleteCell(Index oldCell);

      /* CellStructure splitCell operation declaration */
      /// Splits a cell in the cell complex, given the boundary of the membrane cell.
      bool splitCell(const SplitStruct &ss, BoundaryChain membraneBoundary = BoundaryChain());

      /* CellStructure mergeCells operation declaration */
      /// Merges two adjacent cells in the cell complex.
      bool mergeCells(SplitStruct& ss);

      /* CellStructure collapseCell operation declaration */
      bool collapseCell(SplitStruct &ss);

      /* CellStructure glue operation declaration */
      bool glue(Index keepCell, Index removeCell);
    };

    // So the cell structure can more easily be referred to as
    // ccf::CCStructure.
    typedef CellStructure<CCIndex> CCStructure;

  } // end namespace ccf

} // end namespace mdx

#endif // CCF_CELL_STRUCTURE_HPP
