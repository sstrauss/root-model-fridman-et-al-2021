//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CCRenderGroupWidget.hpp>

namespace mdx {
  CCRenderGroupWidget::CCRenderGroupWidget(QWidget *_parent)
    : QWidget(_parent), cmapEnabled(false), groupEnabled(false)
  {
    setupUi(this);
    setName("###");
    connect(visibleName, SIGNAL(toggled(bool)), this, SIGNAL(toggled(bool)));
    connect(renderChoose, SIGNAL(currentIndexChanged(const QString&)),
            this, SIGNAL(indexChanged(const QString&)));
    connect(colorMapButton, SIGNAL(clicked()), this, SIGNAL(colorMapClicked()));
  }

  CCRenderGroupWidget::CCRenderGroupWidget(QWidget *_parent, const QString &_name)
    : QWidget(_parent), cmapEnabled(false), groupEnabled(false)
  {
    setupUi(this);
    setName(_name);
    connect(visibleName, SIGNAL(toggled(bool)), this, SIGNAL(toggled(bool)));
    connect(renderChoose, SIGNAL(currentIndexChanged(const QString&)),
            this, SIGNAL(indexChanged(const QString&)));
    connect(colorMapButton, SIGNAL(clicked()), this, SIGNAL(colorMapClicked()));
  }

  void CCRenderGroupWidget::setChecked(bool checked)
  {
    if(!groupEnabled) checked = false;
    visibleName->setChecked(checked);
  }

  void CCRenderGroupWidget::setEnabled(bool enabled)
  {
    if(renderChoose->count() == 0)
      enabled = false;

    groupEnabled = enabled;

    visibleName->setEnabled(groupEnabled);
    renderChoose->setEnabled(groupEnabled);
    colorMapButton->setEnabled(groupEnabled && cmapEnabled);

    if(!groupEnabled) setChecked(false);
  }

  QString CCRenderGroupWidget::currentChoice(void) const
  {
    return renderChoose->currentText();
  }

  // Update choices in combo box
  bool CCRenderGroupWidget::setRCTo(const QString &name)
  {
    for(int idx = 0 ; idx < renderChoose->count() ; idx++)
    {
      if(renderChoose->itemText(idx) == name) {
        renderChoose->setCurrentIndex(idx);
        return true;
      }
    }
    return false;
  }

  QString CCRenderGroupWidget::updateChoices(const QStringList &choices, const QString &pcurrent)
  {
    QString current = pcurrent;
    // Save original choice since clearing list will change it
    if(choices.count() == 0) {
      renderChoose->clear();
      setEnabled(false);
      return current;
    }

    bool doUpdate = false;
    int count = renderChoose->count();
    if(count != choices.count())
      doUpdate = true;
    else {
      for(int i = 0; i < count; i++)
        if(choices[i] != renderChoose->itemText(i)) {
          doUpdate = true;
          break;
        }
    }

    // Only clear and rebuild list if required
    if(doUpdate) {
      renderChoose->clear();
      renderChoose->addItems(choices);
      int index = choices.indexOf(current);
      if(index >= 0)
        renderChoose->setCurrentIndex(index);
    }
    if(current.isEmpty() or !choices.contains(current)) {
      renderChoose->setCurrentIndex(0);
      current = renderChoose->currentText();
    }
    setRCTo(current);
    setEnabled(true);
    return current;
  }

  // Make colormap button visible/invisible
  void CCRenderGroupWidget::updateCmapEnabled(const ColorMap &colorMap)
  {
    cmapEnabled = true;
    if(groupEnabled) colorMapButton->setEnabled(true);
  }
}
