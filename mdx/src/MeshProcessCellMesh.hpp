//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_CELL_MESH_HPP
#define MESH_PROCESS_CELL_MESH_HPP

#include <Process.hpp>

#include <MeshProcessSegmentation.hpp>

namespace mdx 
{
  ///\addtogroup MeshProcess
  ///@{
  /**
   * \class FixMeshCorners ProcessCellMesh.hpp <MeshProcessCellMesh.hpp>
   *
   * Fix labelling of cell corners. Depending on the topology, it may be that a
   * triangle between three segmented cells cannot be labeled correctly. This
   * process identifies such triangle and sub-divide them in the hope of
   * changing the topology and solve the problem. This process may need to be
   * called many times to solve all the issues as this is only a heuristic that
   * tends to improve the situation.
   */
  class mdxBase_EXPORT FixMeshCorners : public Process 
  {
  public:
    FixMeshCorners(const Process& process) : Process(process) {}
  
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      if(parms[2].toInt() < 1) {
        setErrorMessage("Number of runs must be at least one.");
        return false;
      }
      return run(mesh, stringToBool(parms[0]), stringToBool(parms[1]), parms[2].toInt());
    }
  
    bool run(Mesh* mesh, bool autoSegment, bool selectBadVertices, int maxRun);
  
    QString name() const { return "Mesh/Cell Mesh/Fix Corners"; }
    QString description() const { return "Fix labelling of cell corners."; }
    QStringList parmNames() const {
      return QStringList() << "Auto segmentation?" << "Select bad vertices" << "Max iterations"; }
    QStringList parmDescs() const {
      return QStringList() << "Re-run watershed automatically at each turn."
                           << "Select remaining bad vertices at the end.\n"
                "Helps in deleting bad vertices that cannot be segmented properly."
                           << "Maximal number of turns."; }
    QStringList parmDefaults() const { return QStringList() << "Yes" << "Yes" << "5"; }
    ParmChoiceMap parmChoice() const
    {
      ParmChoiceMap map;
      map[0] = booleanChoice();
      map[1] = booleanChoice();
      return map;
    }
    QIcon icon() const { return QIcon(":/images/FixCorners.png"); }
  };

  /**
   * \class FixMeshCorners ProcessCellMesh.hpp <MeshProcessCellMesh.hpp>
   *
   * Fix border triangles, that is triangles with -1 on all 3 vertices. The fixing
   * is done by merging the vertices.
   */
  class mdxBase_EXPORT FixBorderTriangles : public Process 
  {
  public:
    FixBorderTriangles(const Process& process) : Process(process) {}
  
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh);
    }
  
    bool run(Mesh* mesh);
  
    QString name() const { return "Mesh/Cell Mesh/Fix Border Triangles"; }
    QString description() const { return "Fix labelling of border triangles in the cell corners."; }
    QStringList parmNames() const { return QStringList(); }
    QStringList parmDescs() const { return QStringList(); }
    QStringList parmDefaults() const { return QStringList(); }
    QIcon icon() const { return QIcon(":/images/FixCorners.png"); }
  };
   
  // Convert to a normal mesh
  class mdxBase_EXPORT ConvertToMgxm : public Process 
	{
  public:
    ConvertToMgxm(const Process &process) : Process(process) {}
 
    // Process default parameters
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh);
    }
    // Convert the mesh
    bool run(Mesh* mesh);

    // Plug-in name
    QString name() const { return "Mesh/Cell Mesh/Convert to a normal mesh"; }
    QString description() const { return "Convert any mesh back to a normal (MDXM) mesh"; }
    QStringList parmNames() const { return QStringList(); }
    QStringList parmDescs() const { return QStringList(); }
    QStringList parmDefaults() const { return QStringList(); }
    QIcon icon() const { return QIcon(":/images/MakeCells.png"); }
  };

  /**
   * \class ConvertToMgxc MeshProcessCellMesh.hpp <MeshProcessCellMesh.hpp>
   * 
	 * Convert a segmented mesh into a cell mesh (MDXC).
	 * The simplified mesh contains only vertices for cell centers and cell outlines.
	 * The process will also convert a 2D cell tissue (MDX2D) back to a cell mesh (MDXC)
   */
  class mdxBase_EXPORT ConvertToMgxc : public Process 
	{
  public:
    ConvertToMgxc(const Process &process) : Process(process) {}
 
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, parms[0].toDouble());
    }
  
    bool run(Mesh* mesh, double wallMax);

    // Plug-in name
    QString name() const { return "Mesh/Cell Mesh/Convert to a cell mesh"; }
    QString description() const { return 
      "Convert a segmented mesh into a cell mesh (MDXC).\n"
      "The simplified mesh contains only vertices for cell centers and cell outlines.\n"
			"The process will also convert a 2D cell tissue (MDX2D) back to a cell mesh (MDXC)."; }
    QStringList parmNames() const {
      return QStringList() << QString("Max Wall Length (%1)").arg(UM); }
    QStringList parmDescs() const { return QStringList() 
      << "Length between vertices to keep, 0 keeps only junctions, -1 keeps everything."; }
    QStringList parmDefaults() const { return QStringList() << "-1"; }
    QIcon icon() const { return QIcon(":/images/MakeCells.png"); }
  };

  // Convert to 2D cell tissue
  class mdxBase_EXPORT ConvertToMgx2d : public Process 
	{
  public:
    ConvertToMgx2d(const Process &process) : Process(process) {}
 
    // Process default parameters
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, parms[0].toDouble());
    }
    // Convert the mesh
    bool run(Mesh* mesh, double wallMax);

    // Plug-in name
    QString name() const { return "Mesh/Cell Mesh/Convert to 2D Cell Tissue"; }
    QString description() const { return 
      "Convert a cell (MDXC) mesh to a 2D Cellular Tissue (MDX2D)"; }
    QStringList parmNames() const {
      return QStringList() << QString("Max Wall Length (%1)").arg(UM); }
    QStringList parmDescs() const { return QStringList() 
      << "Length between vertices to keep, 0 keeps only junctions, -1 keeps everything."; }
    QStringList parmDefaults() const { return QStringList() << "-1"; }
    QIcon icon() const { return QIcon(":/images/MakeCells.png"); }
  };

  // Convert to 3D cell tissue
  class mdxBase_EXPORT ConvertToMgx3d : public Process 
	{
  public:
    ConvertToMgx3d(const Process &process) : Process(process) {}
 
    // Process default parameters
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));;
      return run(mesh, parms[0].toDouble(), parms[1].toDouble());
    }
    // Convert the mesh
    bool run(Mesh* mesh, double tolerance, double neighborMinArea);

    // Plug-in name
    QString name() const { return "Mesh/Cell Mesh/Convert to 3D Cell Tissue"; }
    QString description() const { return 
      "Convert a (MDXM) mesh that is a group of closed 3D cells to a 3D Cellular Tissue (MDX3D)"; }
    QStringList parmNames() const {
      return QStringList() << QString("Tolerance (%1)").arg(UM) << "Neighbour Min Area"; }
    QStringList parmDescs() const { return QStringList() 
      << "Tolerance for merging vertices. Vertices closer than this will be merged." 
			<< "Cells with at least this shared area will be considered as neighbors"; }
    QStringList parmDefaults() const { return QStringList() << ".01" << ".01"; }
    QIcon icon() const { return QIcon(":/images/MakeCells.png"); }
  };

	 /**
   * \class SurfaceFromMDX3D ProcessSelection.hpp <MeshProcessCellMesh.hpp>
   *
   * Extracts a normal MDXM mesh from a 3D tissue mesh (MDX3D), keeping only 
	 * vertices which do not belong to shared faces. New mesh is stored in inactive
	 * stack. Vertices label in the new mesh correspond to cell labels in the original 
	 * 3D mesh. 
   */
  class SurfaceFromMDX3D : public Process 
  {
  public:
    SurfaceFromMDX3D(const Process& process) : Process(process) {}
  
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));

      Mesh* mesh1, *mesh2;
      if(currentMesh() == mesh(0)) {
        mesh1 = mesh(0);
        mesh2 = mesh(1);
      } else if(currentMesh() == mesh(1)) {
        mesh2 = mesh(0);
        mesh1 = mesh(1);
			} else
      return false;	
      
			return run(mesh1, mesh2);
    }
  
    bool run(Mesh* mesh1, Mesh* mesh2);
  
    QString name() const { return "Mesh/Cell Mesh/Surface mesh from MDX3D"; }
    QString description() const { return "Convert MDX3D to normal mesh, keeping only triangles not shared between cells."; }
    QStringList parmNames() const { return QStringList(); }
    QStringList parmDescs() const { return QStringList(); }
    QIcon icon() const { return QIcon(":/images/MakeCells.png"); }
  };
  ///@}
}
#endif
