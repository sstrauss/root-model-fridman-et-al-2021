//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_MESH_INTERACTION_HPP
#define STACK_PROCESS_MESH_INTERACTION_HPP

/**
 * \file StackProcessMeshInteraction.hpp
 * Contains stack processes that interact with meshes, normally using the mesh
 * to edit them
 */

#include <Process.hpp>

namespace mdx 
{

  void getTriangleListVolume(const CCStructure &cs, CCIndexDataAttr &indexAttr, int &label,
                                                                 HVec3F& pts, HVec3U& tris, HVec3F& nrmls);
  ///\addtogroup StackProcess
  ///@{
  /**
   * \class Annihilate StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
   *
   * Delete all but a layour of the stack just "below" the mesh.
   */
  class mdxBase_EXPORT Annihilate : public Process 
  {
  public:
    Annihilate(const Process& process) : Process(process)
    {
      setName("Stack/Mesh Interaction/Annihilate");
      setDesc("Keep or fill a layer near the mesh");
      setIcon(QIcon(":/images/Annihilate.png"));

      addParm("Fill", "Fill the layer with specified value, or keep the original data.", "No", booleanChoice());
      addParm("Fill Val", "Value to fill the volume with.", "30000");
      addParm("Min Dist", "Minimal distance from layer to mesh.", "1.0");
      addParm("Max Dist", "Maximal distance from layre to mesh", "5.0");
    }
  
    bool run();
    /**
     * Annihilate the stack
     * \param input Input stack
     * \param output Stack storing the result
     * \param mesh Mesh used
     * \param minDist Distance from the mesh, taken "below" (i.e. against the normal)
     * \param maxDist Distance from the mesh, taken "below" (i.e. against the normal)
     */
    bool run(const Store* input, Store* output, const CCStructure &cs, CCIndexDataAttr &indexAttr, 
           double minDist, double maxDist, bool fill, uint fillval);
  
  };
  
  // Shared by next 2 processes
  bool stackFromMesh(const Store* input, Store* output,const CCStructure &cs, 
           CCIndexDataAttr &indexAttr, const IntSet &labels, bool fill, uint fillValue);
  /**
   * \class FillStackToMesh StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
   *
   * Fill the volume contained by a closed mesh with a pre-defined intensity.
   */
  class mdxBase_EXPORT FillStackToMesh : public Process 
  {
  public:
    FillStackToMesh(const Process& process) : Process(process) 
    {
      setName("Stack/Mesh Interaction/Fill Stack from Mesh");
      setDesc("Fill volume contained by closed mesh");
      setIcon(QIcon(":/images/TrimStack.png"));

      addParm("Fill Value", "The voxel value to fill", "32767");
      addParm("Label", "If >= 0 only this label is used", "-1");
    }
    bool run();
  };
  
  /**
   * \class TrimStackProcess StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
   *
   * Set to 0 any voxel not contained within the closed mesh.
   */
  class mdxBase_EXPORT TrimStackToMesh : public Process 
  {
  public:
    TrimStackToMesh(const Process& process) : Process(process) 
    {
      setName("Stack/Mesh Interaction/Trim Stack");
      setDesc("Trim parts of stack which are not contained within closed mesh.");
      setIcon(QIcon(":/images/TrimStack.png"));

      addParm("Label", "If >= 0 only this label is used", "-1");
    }
    bool run();
  };
  
  /**
   * \class FillStackToMesh3D StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
   *
   * Fill the stack with labels from a labeled 3D mesh.
   */
  class mdxBase_EXPORT FillStackToMesh3D : public Process 
  {
  public:
    FillStackToMesh3D(const Process& process) : Process(process) 
    {
      setName("Stack/Mesh Interaction/Fill Stack from 3D Mesh");
      setDesc("Fill stack contained by labeled 3D mesh");
      setIcon(QIcon(":/images/FillStack3D.png"));
    }
    bool run();
  };

  /**
   * \class FillStackToMesh3D StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
   *
   * Fill the stack with labels from a labeled 3D mesh.
   */
  class mdxBase_EXPORT RandomVoxelInCell3D : public Process 
  {
  public:
    RandomVoxelInCell3D(const Process& process) : Process(process) 
    {
      setName("Stack/Test/Random Voxel in 3D Cell");
      setDesc("TBD");
      setIcon(QIcon(":/images/FillStack3D.png"));
      addParm("Cell Label", "Cell Label", "0");
      addParm("Ignore Z", "Ignore Z", "No", booleanChoice());
    }
    bool run(){

      Stack *stack = currentStack();
      if(!stack or stack->empty())
        throw(QString("No current stack"));
      Store* input = stack->currentStore();
      if(!input or input->empty())
        throw(QString("No current store (stack)"));

      Mesh* mesh = currentMesh();
      if(!mesh or mesh->empty())
        throw(QString("No current mesh"));

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) throw(QString("No mesh selected"));

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      int label = parm("Cell Label").toInt();
      bool ignoreZ = stringToBool(parm("Ignore Z"));

      Point3d p;


      return run(stack, cs, indexAttr, label, ignoreZ, p);
    }

    bool run(Stack *stack, CCStructure &cs, CCIndexDataAttr &indexAttr, int label, bool ignoreZ, Point3d& randPoint);
  };


  /**
   * \class FillStackToMesh3D StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
   *
   * Fill the stack with labels from a labeled 3D mesh.
   */
  class mdxBase_EXPORT RandPointsInCell : public Process 
  {
  public:
    RandPointsInCell(const Process& process) : Process(process) 
    {
      setName("Stack/Test/Create Rand Attr Map");
      setDesc("TBD");
      setIcon(QIcon(":/images/FillStack3D.png"));
      addParm("Number of Points", "Number of Points", "1");
      addParm("Create Point Cloud Mesh", "Create Point Cloud Mesh", "No", booleanChoice());
    }
    bool run();
  };

  /**
   * \class DeleteOutsideMesh StackProcess.hpp <StackProcess.hpp>
   *
   * Delete labels outside the mesh.
   */
  class mdxBase_EXPORT DeleteOutsideMesh : public Process 
  {
  public:
    DeleteOutsideMesh(const Process& process) : Process(process) 
    {
      setName("Stack/Mesh Interaction/Delete Outside Mesh");
      setDesc("Delete labels outside a mesh of 3D cells based on a threshold distance from the centers.");
      setIcon(QIcon(":/images/Relabel.png"));

      addParm("Threshold", "Threshold Distance (um)", "1");
	  }
  
    bool run()
    {
      Stack* stack = currentStack();
      if(!stack)
        throw QString("%1::run No current stack").arg(name());
      Store* input = stack->currentStore();
      if(!input)
        throw QString("%1::run No current store").arg(name());
      if(!input->labels())
        throw QString("%1::run Store must be labeled").arg(name());
      Store* output = stack->work();
      Mesh* mesh = currentMesh();
      if(!mesh)
        throw QString("%1::run No current mesh").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      if(run(stack, input, output, cs, indexAttr, parm("Threshold").toDouble())) {
        input->hide();
        output->show();
        return true;
      }
      return false;
    }
  
    bool run(Stack* stack, const Store* input, Store* output, const CCStructure &cs, const CCIndexDataAttr &indexAttr, double threshold);
  }; 

//  /**
//   * \class FillStack3D StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
//   *
//   * Fill the stack with labels from a labeled 3D mesh.
//   */
//  class mdxBase_EXPORT FillStack3D : public Process 
//  {
//  public:
//    FillStack3D(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      Store* input = currentStack()->currentStore();
//      Store* output = currentStack()->work();
//      Mesh* mesh = currentMesh();
//      bool res = run(input, output, mesh);
//      if(res) {
//        input->hide();
//        output->show();
//        output->setLabels(true);
//      }
//      return res;
//    }
//  
//    bool run(const Store* input, Store* output, Mesh* mesh);
//  
//    QString name() const { return "Stack/Mesh Interaction/Fill Stack from 3D Mesh"; }
//    QString description() const { return "Fill stack contained by labeled 3D mesh"; }
//    QStringList parmNames() const { return QStringList(); }
//    QStringList parmDescs() const { return QStringList(); }
//    QList<float> default_values() const { return QList<float>(); }
//    QIcon icon() const { return QIcon(":/images/FillStack3D.png"); }
//  };
  
  
//  /**
//   * \class StackRelabelFromMesh StackProcessMeshInteraction.hpp <StackProcessMeshInteraction.hpp>
//   *
//   * Change the labels of a stack to match the ones of a labeled 3D cell mesh.
//   * Unknown cells (i.e. cells in the stack, not in the mesh), can be either
//   * kept or deleted. If kept, they will be relabeled to not conflict with
//   * existing cells.
//   */
//  class mdxBase_EXPORT StackRelabelFromMesh : public Process 
//  {
//  public:
//    StackRelabelFromMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      Stack* s = currentStack();
//      Store* store = s->currentStore();
//      Store* output = s->work();
//      const Mesh* m = currentMesh();
//      if(run(s, store, output, m, stringToBool(parms[0]))) {
//        store->hide();
//        output->show();
//        return true;
//      }
//      return false;
//    }
//  
//    bool run(Stack* stack, const Store* store, Store* output, const Mesh* mesh,
//                    bool delete_unknown);
//  
//    QString name() const { return "Stack/Mesh Interaction/Relabel From Mesh"; }
//    QString description() const { return 
//      "Relabel a 3D stack reusing the same labels as in the stack.\n"
//      "Unknown cells (i.e. cells in the stack, not in the mesh), \n"
//      "can be either kept or deleted. If kept, they will be relabeled\n"
//      "to not conflict with existing cells."; }
//    QStringList parmNames() const { return QStringList() << "Delete unknown"; }
//    QStringList parmDescs() const { return QStringList() << "Delete unknown"; }
//    QStringList parmDefaults() const { return QStringList() << "Yes"; }
//    ParmChoiceMap parmChoice() const
//    {
//      ParmChoiceMap map;
//      map[0] = booleanChoice();
//      return map;
//    }
//    QIcon icon() const { return QIcon(":/images/Relabel.png"); }
//  };
  
  ///@}
}

#endif
