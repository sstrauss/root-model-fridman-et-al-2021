//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_SHAPE_ANALYSIS_HPP
#define STACK_PROCESS_SHAPE_ANALYSIS_HPP

#include <Process.hpp>
#include <MeshUtils.hpp>

// #include <Information.hpp>

// #include <QRegExp>
// #include <QString>
// #include <QStringList>

namespace mdx
{
  
//   /**
//    * \class PCAnalysis StackProcessShapeAnalysis <StackProcessShapeAnalysis>
//    *
//    * Perform a PCA on each labeled regions of the segmented stack, and create
//    * shapes in the mesh reflecting the main components.
//    *
//    * \ingroup ToolsProcess
//    */
//   class PCAnalysis : public Process 
//   {
//   public:
//     PCAnalysis(const Process& process) : Process(process) {}
  
//     bool run(const QStringList &parms)
//     {
//       Stack* stk = currentStack();
//       Store* store = stk->currentStore();
//       QString fn = parms[0];
//       QString correction = parms[1].trimmed();
//       Point3f correcting_factor;
//       if(correction == "Ellipsoid")
//         correcting_factor = sqrt(5);
//       else if(correction == "Cuboid")
//         correcting_factor = sqrt(3);
//       else if(correction == "Elliptical Cylinder")
//         correcting_factor = Point3f(sqrt(3), sqrt(4), sqrt(4));
//       else if(correction == "Maximum Span")
//         correcting_factor = 0;
//       else if(correction.endsWith('%')) {
//         QString cor = correction.left(correction.size() - 1);
//         bool ok;
//         float cr = cor.toFloat(&ok);
//         if(ok) {
//           correcting_factor = -cr / 100.f;
//         } else {
//           setErrorMessage(QString("Error, percentage value is not valid: '%1'").arg(cor));
//           return false;
//         }
//       } else {
//         bool ok;
//         float cr = correction.toFloat(&ok);
//         if(ok)
//           correcting_factor = cr;
//         else {
//           QStringList vs = correction.split(QRegExp("[ ,-;:-]"));
//           if(vs.size() == 3) {
//             correcting_factor.x() = vs[0].toFloat(&ok);
//             if(!ok) {
//               setErrorMessage(QString("Invalid x value for correction factor: '%1'.").arg(vs[0]));
//               return false;
//             }
//             correcting_factor.y() = vs[1].toFloat(&ok);
//             if(!ok) {
//               setErrorMessage(QString("Invalid y value for correction factor: '%1'.").arg(vs[1]));
//               return false;
//             }
//             correcting_factor.z() = vs[2].toFloat(&ok);
//             if(!ok) {
//               setErrorMessage(QString("Invalid z value for correction factor: '%1'.").arg(vs[2]));
//               return false;
//             }
//           } else {
//             setErrorMessage(QString("Invalid correction string '%1', expected one of 'Ellipsoid'"
//                ", 'Cuboid', 'Elliptical Cylinder', Maximum Span', a percentage, a single "
//                                     "value or three values"));
//             return false;
//           }
//         }
//       }
//       bool draw_result = false;
//       bool res
//         = run(stk, store, fn, parms[3].toInt(), parms[2], correcting_factor, 
//             parms[4].toInt(), draw_result);
//       if(res and draw_result) {
//         Mesh* m = mesh(stk->id());
//         m->setShowSurface();
//         m->setShowLabel("Label");
//       }
//       return res;
//     }
  
//     /**
//      * @brief operator ()
//      * @param stack Stack to act on
//      * @param store Store containing the data to process
//      * @param filename File to save the result to
//      * @param treshold Threshold for volume detection, used if the store is not labeled
//      * @param shape Shape used to draw the cells, "No" or empty string for no drawing.
//      * @param correcting_factor Vector giving the correction factor to apply for eigen values
//      * if the x coordinate is 0, then maximum span is used instead, and negative values 
//      * (between 0 and 1) correspond to extracting the percentile of the span.
//      * @param number of elements used to discretize the shape (if any)
//      * @param draw_result Return whether the mesh has been replaced by cell shapes or not
//      * @return
//      */
//     bool run(Stack* stack, Store* store, QString filename, int treshold, QString shape,
//                     Point3f correcting_factor, int slices, bool& draw_result);
  
//     QString name() const { return "Stack/Shape Analysis/PCAnalysis"; }
//     QString description() const { return 
//       "Compute the principle components of the image. If the threshold is -1, then all\n"
//       "the values are used, as is. If 'Draw Result' is set to true, the current mesh\n"
//       "will be erased and replaced with shapes representing the cells fit. 'Splan Correction'\n"
//       "can be either a shape, a single value of a vector of 3 values, corresponding to the\n"
//       "correction to apply for the eigen-values on all three directions.";
//     }
//     QStringList parmNames() const { return QStringList() 
//       << "Output" << "Span Correction" << "Draw Result" << "Threshold" << "Shape Details"; }
//     QStringList parmDescs() const { return QStringList() 
//       << "File to write the output to"
//       << "Span correction can be made using pre-computed formula for regular shapes, or a "
//          "percentage of the PC."
//       << "Shape used to display the result"
//       << "If the stack is not labeled, a single volume is considered with all voxels of "
//          "intensity greater than this threshold."
//       << "How finely to draw the shape (for cylinders or ellipsoids)"; }
//     QStringList parmDefaults() const { return QStringList() 
//       << "output.csv" << "Ellipsoid" << "No" << "100" << "3"; }
//     ParmChoiceMap parmChoice() const
//     {
//       ParmChoiceMap map;
//       map[1] = QStringList() << "Ellipsoid" << "Cuboid" << "Elliptical Cylinder" << "Maximum Span";
//       map[2] = QStringList() << "No" << "Ellipsoids" << "Cuboids" << "Cylinder";
//       return map;
//     }
//     QIcon icon() const { return QIcon(":/images/PCAnalysis.png"); }
//   };

   // FIXME Why is this in StackProcesses?
   /*
    * \class PCAnalysis2D StackProcessShapeAnalysis <StackProcessShapeAnalysis>
    *
    * Perform a PCA on each labeled regions of the segmented stack, and create
    * shapes in the mesh reflecting the main components.
    *
    * \ingroup ShapeAnalysis
    */
  class PCAnalysis2D : public Process 
  {
  public:
    PCAnalysis2D(const Process& process) : Process(process)
    {
      setName("Mesh/Cell Axis/Shape Analysis/Compute PCA 2D");
      setDesc("Compute the principle components of 2D cell shapes");
      setIcon(QIcon(":/images/PDG.png"));

      addParm("Display", "Display for heat map", "Ratio", QStringList() << "Ratio" << "Max" << "Min" << "None");
      addParm("Heat Name", "Name of the heat attribute", "PCA 2D");
      addParm("Scale", "Default amount to scale the line length (um)", "2.0");
      addParm("Line Width", "Axis Line Width", "2.0");
      addParm("PCA Attribute", "Name of the attribute for PCA", "PCA 2D");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No cell complex").arg(name());

      QString pcaName = parm("PCA Attribute");
      if(pcaName.isEmpty())
        throw QString("%1:run PCA attribute name is empty").arg(name());
      QString display = parm("Display");
      if(display.isEmpty())
        throw QString("%1:run Heat display type is empty").arg(name());
      QString heatName = parm("Heat Name");
      if(display != "None" and heatName.isEmpty())
        throw QString("%1:run Heat name is empty").arg(name());
      double scale = parm("Scale").toDouble();
      if(scale <= 0)
        throw QString("%1:run Scale must be > 0").arg(name());
      double lineWidth = parm("Line Width").toDouble();
      if(lineWidth <= 0)
        throw QString("%1:run Line width must be > 0").arg(name());

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      auto &pcaAttr = mesh->cellAxisAttr(ccName, pcaName);
      bool result = run(cs, indexAttr, pcaAttr);
      if(result) {
        if(display != "None") {
          auto &heatAttr = mesh->heatAttr<double>(heatName);
          heatFromCellAxis(display, pcaAttr, heatAttr);
          mesh->setHeatBounds(calcBounds(heatAttr), heatName);
        }
        double bounds = axisBounds(pcaAttr);
        mesh->cellAxisSetBounds(ccName, pcaName, 0, Point2d(scale, bounds));
        mesh->cellAxisSetBounds(ccName, pcaName, 1, Point2d(scale, bounds));
        mesh->cellAxisSetBounds(ccName, pcaName, 2, Point2d(0, 0));
        mesh->cellAxisSetLineWidth(ccName, pcaName, lineWidth);
      }
      mesh->updateProperties();

      return true;
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, IntSymTensorAttr &cellAxisAttr);
  };

// FIXME Move the Bezier part to a separate process to create a new cellAxis map
// class DisplayPCA : public Process 
//  {
//  public:
//
//    enum ParmNames { pDisplayHeatMap,  pScaleHeatMap, pRangeHeatLow, pRangeHeatHigh, pDisplayAxis,
//     pAxisColor, pAxisWidth, pAxisScale, pAxisOffset, pAspectRatioThreshold, pNumParms}; 
//
//    DisplayPCA(const Process& process) : Process(process)
//    {
//      setName("Mesh/Cell Axis/Shape Analysis/Display PCA");
//      setDesc("Display the 2D shape approximation axis");
//      setIcon(QIcon(":/images/PDG.png"));
//
//      addParm("Heatmap", "Display as a color map PCA values in max or min direction, or max*min, or max/min.", "Ratio", QStringList() 
//        << "None" 
//        << "PCAMax" 
//        << "PCAMin" 
//        << "Ratio"
//        << "Product"
//        << "PCABezierX"
//        << "PCABezierY"
//        << "ShearPCABezier");
//      addParm("ScaleHeat", "Scale heat map", "Auto", QStringList() 
//        << "None"
//        << "Auto"
//        << "Manual");
//
//      addParm("Heat min", "High bound heat map", "1");
//      addParm("Heat max", "Low bound heat map", "3");
//      addParm("Show Axis", "Draw pca directions as vectors.", "Both", QStringList() 
//        << "Both" 
//        << "PCAMax" 
//        << "PCAMin" 
//        << "PCABezierX" 
//        << "PCABezierY"  
//        << "None");
//      addParm("Axis Color", "Color used to draw pca", "white", QColor::colorNames());
//      addParm("Line Width", "Line Width", "2.0");
//      addParm("Line Scale", "Length of the vectors = Scale * Strain.", "1.0");
//      addParm("Line Offset", "Draw the vector ends a bit tilted up for proper display on surfaces.", "0.1");
//      addParm("Threshold", "Minimal value of aspect ratio (= PCAMax/PCAMin) required for drawing PCA.", "0.0");
//
//    }
//  
//    bool run()
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) 
//        throw(QString("%1:run No mesh selected").arg(name()));
//
//      QString ccName = mesh->ccName();
//      if(ccName.isEmpty())
//        throw(QString("%1:run No cell complex").arg(name()));
//
//      CCStructure &cs = mesh->ccStructure(ccName);
//      CCIndexDataAttr &indexAttr = mesh->indexAttr();
//
//      QString outp;
//      IntFloatAttr heatAttr;
//      bool runDisplay;
//
//      bool result;// = run(cs, indexAttr, parm("Output"), parm("Axis Line Scale").toDouble(), parm("Axis Line Width").toDouble(), outp, heatAttr, runDisplay);
//
//      return result;
//    }
//    bool run(CCStructure &cs, CCIndexDataAttr &indexAttr, QString filename, double axisLineScale, double axisLineWidth, QString outp, IntFloatAttr &outputHeatMap, bool runDisplay);
//
//    bool run(Mesh* mesh, const QString displayHeatMap, const QString scaleHeatMap, const Point2d &rangeHeat,
//    const QString displayAxis, const QColor& axisColor, float axisWidth, float axisScale, 
//    float axisOffset, float aspectRatioThreshold);
//
//  private:
//    QString DisplayHeatMap;
//   QString ScaleHeatMap;
//   Point2d RangeHeat;
//   QString DisplayAxis;
//   QColor AxisColor;
//   float AxisWidth;
//    float AxisScale;
//   float AxisOffset;
//   float AspectRatioThreshold;
//
//  };


//   class mdxBase_EXPORT DisplayPCA : public Process 
//   {
//   public:
//     enum ParmNames { pDisplayHeatMap,	pScaleHeatMap, pRangeHeatLow, pRangeHeatHigh, pDisplayAxis,
// 		  pAxisColor, pAxisWidth, pAxisScale, pAxisOffset, pAspectRatioThreshold, pNumParms};		

//     DisplayPCA(const Process& process) : Process(process) {}
   
// 	  bool initialize(QStringList& parms, QWidget* parent);

// 		// Process the parameters
//     void processParms(const QStringList &parms);
  
//     bool run(const QStringList &parms)
//     {
//       Mesh *mesh = currentMesh();
//       if(!mesh) throw(QString("No current mesh"));
//       return run(mesh, parms);
//     }
 
//     bool run(Mesh* mesh, const QStringList &parms) 
//     {
//       processParms(parms);
//       return run(mesh, DisplayHeatMap, ScaleHeatMap, RangeHeat, DisplayAxis,
// 			  AxisColor, AxisWidth, AxisScale, AxisOffset, AspectRatioThreshold);
//     }

//     bool run(Mesh* mesh, const QString displayHeatMap, const QString scaleHeatMap, const Point2d &rangeHeat,
// 		  const QString displayPCA, const QColor& axisColor, float axisLineWidth, 
//       float scaleAxisLength, float axisOffset, float aspectRatioThreshold);
  
//     QString name() const { return "Mesh/Cell Axis/Shape Analysis/Display PCA"; }
//     QString description() const { return "Display the principle growth directions"; }
//     QStringList parmNames() const 
// 		{ 
// 		  QVector <QString> vec(pNumParms);
//       vec[pDisplayHeatMap]       = "Heatmap"; 
//       vec[pScaleHeatMap]         = "ScaleHeat";
//       vec[pRangeHeatLow]         = "Heat min";
//       vec[pRangeHeatHigh]        = "Heat max"; 
//       vec[pDisplayAxis]          = "Show Axis";
//       vec[pAxisColor]            = "Axis Color";
//       vec[pAxisWidth]            = "Line Width";
//       vec[pAxisScale]            = "Line Scale";
//       vec[pAxisOffset]           = "Line Offset";
//       vec[pAspectRatioThreshold] = "Threshold";
// 			return vec.toList();
//     }

//     QStringList parmDescs() const 
// 		{ 
// 		 QVector <QString> vec(pNumParms);
// 		 vec[pDisplayHeatMap]       = "Display as a color map PCA values in max or min direction, or max*min, or max/min.\n";
// 		 vec[pScaleHeatMap]         = "Scale heat map";
// 		 vec[pRangeHeatLow]         = "High bound heat map";
// 		 vec[pRangeHeatHigh]        = "Low bound heat map";
//      vec[pDisplayAxis]          = "Draw pca directions as vectors.\n";
//      vec[pAxisColor]            = "Color used to draw pca" ;
//      vec[pAxisWidth]            = "Line Width" ;
// 		 vec[pAxisScale]            = "Length of the vectors = Scale * Strain.";
//      vec[pAxisOffset]           = "Draw the vector ends a bit tilted up for proper display on surfaces.";
//      vec[pAspectRatioThreshold] = "Minimal value of aspect ratio (= PCAMax/PCAMin) required for drawing PCA.\n"; 
// 		 return vec.toList();
//     }

//     QStringList parmDefaults() const 
// 		{ 
// 		 QVector <QString> vec(pNumParms);
//      vec[pDisplayHeatMap]       = "Ratio";
// 		 vec[pScaleHeatMap]         = "Auto";
// 		 vec[pRangeHeatLow]         = "1";
// 		 vec[pRangeHeatHigh]        = "3";
// 		 vec[pDisplayAxis]          = "Both"; 
// 		 vec[pAxisColor]            = "white";
// 		 vec[pAxisWidth]            = "2.0"; 
// 		 vec[pAxisScale]            = "1.0"; 
// 		 vec[pAxisOffset]           = "0.1";
// 		 vec[pAspectRatioThreshold] = "0.0";
// 		 return vec.toList();
//     }

//     ParmChoiceMap parmChoice() const
//     {
//       ParmChoiceMap map;
			
// 			map[pDisplayHeatMap] = 
// 			  QStringList() 
//         << "None" 
// 				<< "PCAMax" 
// 				<< "PCAMin" 
// 				<< "Ratio"
//         << "Product"
// 				<< "PCABezierX"
// 				<< "PCABezierY"
// 				<< "ShearPCABezier";
// 			map[pDisplayAxis] = 
// 			  QStringList() 
// 			  << "Both" 
// 				<< "PCAMax" 
// 				<< "PCAMin" 
// 				<< "PCABezierX" 
// 				<< "PCABezierY"  
// 				<< "None";
// 		  map[pScaleHeatMap] = 
// 			  QStringList() 
// 				<< "None"
// 				<< "Auto"
// 				<< "Manual";
//       map[pAxisColor] = QColor::colorNames();
//       return map;
//     }
//     QIcon icon() const { return QIcon(":/images/PDG.png"); }

//   private:
//     QString DisplayHeatMap;
// 		QString ScaleHeatMap;
// 		Point2d RangeHeat;
// 		QString DisplayAxis;
// 		QColor AxisColor;
// 		float AxisWidth;
//     float AxisScale;
// 		float AxisOffset;
// 		float AspectRatioThreshold;
//   };
//   ///@}
}

#endif
