//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef MDX_PROCESS_MORPHOGENS_HPP
#define MDX_PROCESS_MORPHOGENS_HPP

#include <CCUtils.hpp>
#include <MDXProcessTissue.hpp>
#include <Solver.hpp>

#include <Random.hpp>

namespace mdx
{
  // Process to calculate diffusion derivatives
  template <typename _VectorT>
  class DiffusionDerivs : public Process, public SolverDerivs<_VectorT>
  {
  public:
    typedef Solver<_VectorT> SolverT;
    typedef typename SolverT::VectorT VectorT;
    typedef typename SolverT::ScalarT ScalarT;
    typedef typename SolverT::MatrixT MatrixT;
    typedef typename SolverT::VectorUIntT VectorUIntT;
    typedef typename SolverT::SolverVertexData SolverVertexData;
    typedef typename SolverT::VertexAttr VertexAttr;
    typedef typename SolverT::SolverEdgeData SolverEdgeData;
    typedef typename SolverT::EdgeAttr EdgeAttr;
    static const int dim = SolverT::dim; 

    // Structure to store the cell data
    struct CellData
    {
      VectorT a;                     /// Morphogen concentrations
      double interfaceMeasure = 4.0; /// default to square grid
      double measure = 1.0;
      VectorUIntT labels;            /// Labels for dirichlet conditions

      CellData() {}

      bool operator==(const CellData &other) const
      {
        if(a == other.a and measure == other.measure and interfaceMeasure == other.interfaceMeasure and labels == other.labels)
          return true;
        return false;
      }
    };
    typedef AttrMap<CCIndex, CellData> CellDataAttr;

    // Class for subdivision of cells
    class Subdivide : virtual public mdx::Subdivide
    {
    public:
      Subdivide() {}
      Subdivide(CellDataAttr &attr) : cellDataAttr(&attr) {}

      void splitCellUpdate(Dimension d, const CCStructure &cs, const CCStructure::SplitStruct& ss, 
                                        CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(), double interpPos = 0.5)
      {
        if(d == 1 and (otherP != CCIndex::UNDEF or otherN != CCIndex::UNDEF)) { // Propagate values when edges split
          // Return if not set up
          if(!cellDataAttr) {
            mdxInfo << "DiffusionDerivs::Subdivide:splitCellUpdate Error, CellData attribute not set" << endl;
            return;
          }
          // See if the parent has cell data
          CellData &mCD = (*cellDataAttr)[ss.membrane];
          CellData &pCD = (*cellDataAttr)[otherP];
          CellData &nCD = (*cellDataAttr)[otherN];
          for(uint i = 0; i < dim; i++) {
            mCD.a[i] = (pCD.a[i] + nCD.a[i])/2.0; // Average the concentrations
            mCD.labels[i] = std::max(nCD.labels[i], pCD.labels[i]); // Take the max label
          }
          // Do wee need to propagate the measures?
        }
      }

      CellDataAttr *cellDataAttr = 0;
    };

    // Constructor
    DiffusionDerivs(const Process &process) : Process(process) 
    {
      setName("Diffusion Derivs");
      setDesc("Diffusion parameters for derivatives");
      setIcon(QIcon(":/images/CellGrid.png"));

      insertParm("Production", "Production coefficient(s)", "", 0);
      insertParm("Diffusion", "Diffusion coefficient(s)", "", 1);
      insertParm("Decay", "Decay coefficient(s)", "", 2);
    }

    // Initialize the process, called from solver
    using Process::initialize;
    // Initialize the derivatives process, called by solver
    bool initialize(CCIndexDataAttr &_indexAttr, CellDataAttr &_cellAttr)
    {
      indexAttr = &_indexAttr;
      cellAttr = &_cellAttr;
      return true;
    }
  
    // Initialize sources and sinks
    void initDerivs(SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr)
    {
      if(!indexAttr)
        throw QString("Diffusion::initialize Index data attribute map empty");
      if(!cellAttr)
        throw QString("Diffusion::initialize Diffusion data attribute map empty");
  
      // Read the parameters
      Prod = stringTo<VectorT>(parm("Production"));
      Diff = stringTo<VectorT>(parm("Diffusion"));
      Decay = stringTo<VectorT>(parm("Decay"));
  
      const CCIndexVec &cells = solver.graph().vertices();
      #pragma omp parallel for
      for(uint i = 0; i < cells.size(); i++) {
        CCIndex c = cells[i];
        CellData &cCD = (*cellAttr)[c];
        CCIndexData &cIdx = (*indexAttr)[c];
        if(cIdx.measure <= 0.0) // possible for vertex to vertex diffusion
          cIdx.measure = 1.0;
        cCD.measure = cIdx.measure;
  
        // Find the interface size, assumes it is in the measure of the solver graph edges
        cCD.interfaceMeasure = 0.0;
        for(const Flip &flip : neighborVertexFlips(solver.graph(), c)) {
          CCIndex w = flip.interior;
  
          CCIndexData &wIdx = (*indexAttr)[w];
          if(wIdx.measure <= 0.0)
            wIdx.measure = 1.0;
          cCD.interfaceMeasure += wIdx.measure;
        }
  
        // Do dirichlet
        auto &cV = vAttr[c];
        for(int i = 0; i < dim; i++)
          if(cCD.labels[i] > 0) {
            cV.dirichlet[i] = cCD.labels[i];
            cV.x[i] = cCD.a[i];
          } else
            cV.dirichlet[i] = 0;
      }
    }

    void setValues(const SolverT &solver, CCIndex c, const VectorT &values)
    {
      CellData &cCD = (*cellAttr)[c];
      cCD.a = values;
    }
  
    void getValues(const SolverT &solver, CCIndex c, VectorT &values)
    {
      CellData &cCD = (*cellAttr)[c];
      values = cCD.a;
    }

    // Calculate derivatives for a cell
    void calcDerivatives(const SolverT &solver, CCIndex c, VectorT &dx)
    {
      if(!indexAttr)
        throw QString("Diffusion::initialize Index data attribute map empty");
      if(!cellAttr)
        throw QString("Diffusion::initialize Diffusion data attribute map empty");
  
      CellData &cCD = (*cellAttr)[c];
  
      for(int i = 0; i < dim; i++) {
        // Production
        dx[i] += Prod[i];
  
        // Decay
        dx[i] -= Decay[i] * cCD.a[i];
      }
  
      // Calculate diffusion
      CCIndexData &cIdx = (*indexAttr)[c];
      for(const Flip &flip : neighborVertexFlips(solver.graph(), c)) {
        CCIndex w = flip.interior;
        CCIndex n = flip.otherFacet(c);
  
        CellData &nCD = (*cellAttr)[n];
        CCIndexData &wIdx = (*indexAttr)[w];
        for(int i = 0; i < dim; i++)
          dx[i] += Diff[i] * (nCD.a[i] - cCD.a[i]) * wIdx.measure / cIdx.measure;
      }
    }
  
    // Calculate Jacobian for cell
    void calcJacobian(const SolverT &solver, CCIndex c, MatrixT &j)
    {
      if(!cellAttr)
        throw QString("Diffusion::initialize Diffusion data attribute map empty");
  
      CellData &cCD = (*cellAttr)[c];
  
      double geoFactor = cCD.interfaceMeasure / cCD.measure;
      for(int i = 0; i < dim; i++)
        j[i][i] += -Decay[i] - Diff[i] * geoFactor;
    }
  
    // Calculate Jacobian for neighbors
    void calcJacobian(const SolverT &solver, CCIndex c, CCIndex nb, MatrixT &j)
    {
      if(!indexAttr)
        throw QString("Diffusion::initialize Index data attribute map empty");
      if(!cellAttr)
        throw QString("Diffusion::initialize Diffusion data attribute map empty");
  
      CCIndexData &cIdx = (*indexAttr)[c];
      //CCIndex w = tissue->dualGraph().join(c, nb);
      CCIndex w = edgeBetween(solver.graph(), c, nb);
      CCIndexData &wIdx = (*indexAttr)[w];
  
      double geoFactor = wIdx.measure / cIdx.measure;
      for(int i = 0; i < dim; i++)
        j[i][i] += Diff[i] * geoFactor;
    }

  private:
    CCIndexDataAttr *indexAttr = 0;
    CellDataAttr *cellAttr = 0;

    // Parameters
    VectorT Prod;
    VectorT Diff;
    VectorT Decay;
  };

  /*
   * Meinhardt activator-inhibitor system
   */
  class MeinhardtAI : public Process, public SolverDerivs<Point2d>
  {
  public:
    // Morphogen data stored in cells
    struct CellData
    {
      bool prodA = true, prodH = true;   // cell produces activator-inhibitor
      double a = 0, h = 0;               // activator-inhibitor concentrations
      double perimeter = 0, area = 0;    // copied from indexAttr to speed lookup

      CellData() {}

      bool operator==(const CellData &other) const
      {
        if(prodA == other.prodA and prodH == other.prodH 
            and a == other.a and h == other.h 
            and perimeter == other.perimeter and area == other.area)
          return true;
        return false;
      }
    };
    typedef AttrMap<CCIndex,CellData> AIAttr;

    // Class for subdivision of cells
    class Subdivide : virtual public mdx::Subdivide
    {
    public:
      Subdivide() : aiAttr(0) {}
      Subdivide(AIAttr &attr) : aiAttr(&attr) {}
      void splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct& ss, 
          CCIndex otherP = CCIndex(), CCIndex otherN = CCIndex(), double interpPos = 0.5);

      AIAttr *aiAttr;
    };

    // Constructor
    MeinhardtAI(const Process &process) : Process(process) 
    {
      setName("Meinhardt AI");
      setDesc("Meinhardt-style activator-inhibitor system");
      setIcon(QIcon(":/images/MeinhardtAI.png"));

      insertParm("AProd", "Activator default production", "0.1", 0);
      insertParm("AAProd", "Activator production, self enhanced", "10.0", 1);
      insertParm("AProdDist", "Distance from origin where production starts", "10.0 40.0", 2);
      insertParm("ADecay", "Activator decay", "0.1", 3);
      insertParm("ADiff", "Activator diffusion", "0.1", 4);
      insertParm("ANoise", "Activator noise", "0.001", 5);
      insertParm("AInit", "Activator initial concentration", "3.0", 6);
      insertParm("AMax", "Activator max concentration", "20000.0", 7);
      insertParm("HProd", "Inhibitor default production", "0.0", 9);
      insertParm("HAProd", "Inhibitor production, self enhanced", "5.0", 10);
      insertParm("HDecay", "Inhibitor decay", "0.1", 11);
      insertParm("HDiff", "Inhibitor diffusion", "15.0", 12);
      insertParm("HInit", "Inhibitor initial concentration", "300.0", 13);
      insertParm("HMax", "Inhibitor max concentration", "20000.0", 14);
    }

    // Set the attribute maps
    using Process::initialize;
    bool initialize(Mesh &mesh, CellTissue &tissue, CCIndexDataAttr &indexAttr, AIAttr &aiAttr);

    void initDerivs(SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr);

    // Step does nothing
    bool step()
    {
      throw(QString("MeinhardAI::step This process defines derivatives, please run solver process")); 
    }

    // Reset the concentrations
    bool initValues();

    // Reset the concentrations
    bool printStats();

  private:
    // Process parameters
    void processParms(); 

    // Check for invalid concentrations
    bool checkConcentrations(const QString &varName, double &s, double min, double max);

    // Update GUI colors
    void updateSignal(void);

    // Reimplemented SolverDerivs methods
    void setValues(const SolverT &solver, CCIndex cell, const VectorT &values)
    {
      if(!aiAttr)
        throw(QString("MeinhardAI::setValues Attribute map not set"));
      CellData &cAI = (*aiAttr)[cell];
      cAI.a = values[0];
      cAI.h = values[1];
    }
    void getValues(const SolverT &solver, CCIndex cell, VectorT &values)
    {
      if(!aiAttr)
        throw(QString("MeinhardAI::getValues Attribute map not set"));
      const CellData &cAI = (*aiAttr)[cell];
      values[0] = cAI.a;
      values[1] = cAI.h;
    }
    void calcDerivatives(const SolverT &solver, CCIndex c, VectorT &values);
    void calcJacobian(const SolverT &solver, CCIndex cell, CCIndex nb, MatrixT &j);
    void calcJacobian(const SolverT &solver, CCIndex cell, MatrixT &j);

  private:
    Mesh *mesh = 0;
    CellTissue *tissue = 0;
    AIAttr *aiAttr = 0;
    CCIndexDataAttr *indexAttr = 0;

    // Parameters
    double aProd;
    double aaProd;
    Point2d aProdDist;
    double aDecay;
    double aDiff;
    double aNoise;
    double aInit;
    double aMax;

    double hProd;
    double haProd;
    double hDecay;
    double hDiff;
    double hInit;
    double hMax;
  };

  bool inline readAttr(MeinhardtAI::CellData &m, const QByteArray &ba, size_t &pos)
  {
    return readChar((char *)&m, sizeof(MeinhardtAI::CellData), ba, pos);
  }
  bool inline writeAttr(const MeinhardtAI::CellData &m, QByteArray &ba) 
  {
    return writeChar((char *)&m, sizeof(MeinhardtAI::CellData), ba);
  }

  class MeinhardtSolver : public Process, public Solver<Point2d>
  {
  public:
    MeinhardtSolver(const Process &process) : Process(process) 
    {
      setName("Meinhardt Solver");
      setDesc("Solver for Meinhardt system");
      setIcon(QIcon(":/images/Process.png"));

      addParm("Tissue Process", "Name of the Cell Tissue process", "Cell Tissue");
      addParm("Meinhardt Process", "Process that calculates derivatives", "Meinhardt AI");
      addParm("Cell Data", "Name of the Cell Data attribute", "MeinhardtAI");
      addParm("Print Meinhardt Stats", "Print stats for Meinhardt simulations", "Yes", booleanChoice());
    }

    // Initialize simulation, called from GUI thread
    bool initialize(QWidget* parent);

    // Run a step of the simulation
    bool step();

    // Rewind the morphogen values (sets initial values)
    bool rewind(QWidget *parent);

    // Get the tissue
    CellTissue &tissue();

    // Get the meinHardt attributes
    MeinhardtAI::AIAttr &attr() 
    { 
      if(!aiAttr)
        throw(QString("MeinhardtSolver::attr aiAttr is null"));
      else
        return *aiAttr;
    }

  private:
    CellTissueProcess *tissueProcess = 0;
    MeinhardtAI *meinhardtProcess = 0;
    MeinhardtAI::AIAttr *aiAttr = 0;
  };

}
#endif
