//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_HEAT_MAP_HPP
#define MESH_PROCESS_HEAT_MAP_HPP

#include <limits>

#include <QTreeWidgetItem>
#include <QFileDialog>

#include <Process.hpp>
#include <Progress.hpp>
#include <Information.hpp>
#include <MeshUtils.hpp>

class Ui_LoadHeatMap;

namespace mdx
{
  mdxBase_EXPORT void getBBox(const HVec3F& pts, Point3f* bBox);

  void fillTreeWidgetWithMeasures(Mesh* mesh, QTreeWidget* tree);

  ///\addtogroup MeshProcess
  ///@{

   /**
   * \class HeatMapSetRange ProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
   *
   * Set the range for a heat map, specifying new lower and upper bound.
   */
  class mdxBase_EXPORT HeatMapSetRange : public Process
  {
  public:
    HeatMapSetRange(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Tools/Set Range");
      setDesc("Change the range of the current heat map for display.");
      setIcon(QIcon(":/images/Scale.png"));

      addParm("Min", "Lower bound (empty for current)", "");
      addParm("Max", "Upper bound (empty for current)", "");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No mesh selected"));

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) throw(QString("No mesh selected"));

      Point2d bounds = mesh->heatBounds();
      if(!parm("Min").isEmpty())
         bounds[0] = parm("Min").toDouble();
      if(!parm("Max").isEmpty())
         bounds[1] = parm("Max").toDouble();
      if(bounds[0] >= bounds[1])
        throw(QString("Min must be less than max"));

      mesh->setHeatBounds(bounds);
      mesh->updateProperties(ccName);

      return true;
    }
  };
  
  /**
   * \class HeatMapSave ProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
   *
   * Save the current heat map in a CSV file.
   */
  class mdxBase_EXPORT HeatMapSave : public Process
  {
    Q_OBJECT
  public:
    HeatMapSave(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Tools/Heat Map Save");
      setDesc("Save heat map to a file");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Heat Name", "Name of heat map to save, empty for current, or All", ""); 
      addParm("Labeling", "Labeling to use, empty for current", "");
      addParm("File Name", "Name of file to save heat map to", ""); 
      addParm("Use Selection", "Export selected cells only?\n"
              "'No' exports the whole attribute, whether the cells exist or not", "No", booleanChoice()); 
      addParm("Export Labelings", "If Labeling is 'Labels', export the other labelings as well?\n", "No", booleanChoice()); 
    }
 
    bool processParms()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::processParms No current mesh").arg(name());

      QString heatName = parm("Heat Name");
      if(heatName.isEmpty())
        heatName = mesh->heat();
      if(heatName.isEmpty())
        throw QString("%1::processParms No heat map specified").arg(name());
      if(heatName != "All" and !mesh->heatExists(heatName))
        throw QString("%1::run No heat map named (%2) exists").arg(name()).arg(heatName);
      setParm("Heat Name", heatName);

      QString labeling = parm("Labeling");
      if(labeling.isEmpty())
        labeling = mesh->labeling();
      if(labeling.isEmpty())
        throw QString("%1::processParms No labeling specified").arg(name());
      if(!mesh->labelingExists(labeling)) 
        throw QString("%1::processParms No labeling named (%2) exists").arg(name()).arg(labeling);
      setParm("Labeling", labeling);

      if(labeling != "Labels" and stringToBool("Export Labelings"))
        throw QString("%1::processParms Labeling must be 'Labels' to export labelings").arg(name());
      return true;
    }

    bool initialize(QWidget* parent)
    {
      processParms();
      QString fileName = parm("File Name");
      if(fileName.isEmpty()) {
        fileName = parm("Heat Name") + "-" + parm("Labeling") + ".csv";
        if(parent) {
          fileName = QFileDialog::getSaveFileName(parent, "Choose spreadsheet file to save", 
              QDir::currentPath() + "/" + fileName, "CSV files (*.csv)");
          if(fileName.isEmpty())
            return false;
          if(!fileName.endsWith(".csv", Qt::CaseInsensitive))
            fileName += ".csv";
        }
      }
      setParm("File Name", fileName);
      return true;
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());

      processParms();
      return run(*mesh, parm("Heat Name"), parm("Labeling"), parm("File Name"), stringToBool(parm("Use Selection")), 
                                                                                    stringToBool(parm("Export Labelings")));
    }
    bool run(Mesh &mesh, const QString &heatName, const QString &labeling, const QString &fileName, bool useSelection, bool exportLabelings);
  };
  
  /**
   * \class HeatMapLoad ProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
   *
   * Load the heat map from a CSV file. The CSV file must have the following structure:
   *
   *  - The first column must be cell labels (the name doesn't matter)
   *  - If the heat map is on walls, the second column must be called "Neighbor"
   *    and also be cell labels.
   *  - Each other column is a potential heat. The column name is structured
   *    either as "name" or "name (unit)". In the second case, the unit will be
   *    extracted for display.
   */
  class mdxBase_EXPORT HeatMapLoad : public Process 
  {
    Q_OBJECT
  public:
    HeatMapLoad(const Process& process) : Process(process)  
    {
      setName("Mesh/Heat Map/Tools/Heat Map Load");
      setDesc("Load a heat map file and set the corresponding heat for each label");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("File Name", "Path to spreadsheet file.", "");
      addParm("Column", "Column of the csv file to load for display.\n"
              "Label: cell labels, Value: original computed values (e.g. growth or signal), Heat: "
              "values scaled for visualization (always between 0-1).", "2");
      addParm("Border Size", "Width of cell outline used for vizualization of 'wall' colormaps in microns", "1.0");
    }

    bool initialize(QWidget* parent);

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, parm("File Name"), parm("Column").toInt(), parm("Border Size").toFloat());
    }
    bool run(Mesh* mesh, const QString& filename, int column, float border_size);

  public slots:
    void selectFile(const QString& filename);
    void selectFile();
    void selectColumn(int c);

  protected:
    QDialog* dlg;
    Ui_LoadHeatMap* ui;
    QList<QString> column_unit;
  };
  
  /**
   * \class SaveParents ProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
   *
   * Store mapping from each label to a label in a different mesh (parent).
   */
  // DUPLICATED CODE?
  /* class mdxBase_EXPORT SaveParents : public Process
  {
  public:
    SaveParents(const Process& process) : Process(process) 
    {
    }

    bool initialize(QStringList& parms, QWidget* parent);

    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, parms[0]);
    }

    bool run(Mesh* mesh, const QString& filename);

      setName("Mesh/Lineage Tracking/Save Parents");
      setDesc("Save map of labels to parents labels to a file");
    QStringList parmNames() const { return QStringList() << "Filename");
    QStringList parmDescs() const { return QStringList() << "Path to label parents file.");
    QStringList parmDefaults() const { return QStringList() << "");
      setIcon(QIcon(":/images/Parents.png"));
  };
  */
  /**
   * Clear label to parent mapping
   */
  /* *** Not currently implemented?
  class mdxBase_EXPORT ResetParents : public Process
  {
  public:
    ResetParents(const Process& process) : Process(process) 
    {
      setName("Mesh/Lineage Tracking/Clear Parents");
      setDesc("Clear mapping from parents to labels");
      setIcon(QIcon(":/images/Parents.png"));
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh);
    }
    bool run(Mesh* mesh);
  };
  */

  class HeatMapScale : public Process
  {
  public:
    HeatMapScale(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Tools/Heat Map Scale");
      setDesc("Scales the current heat map values according to the chosen type.\n"
              "The types log and pow work with the base of 10. inverse of 0 is 0");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Type", "Type of scaling", "Inverse", 
              QStringList() << "Inverse" << "Square" << "Sqrt" << "Log" << "Pow" << "Ordinal");
      addParm("Heat Name", "Name of heat map to scale, empty for current", ""); 
    }

    bool run()
    {
      mesh = currentMesh();
      ccName = mesh->ccName();

      if(!mesh) 
        throw(QString("HeatMapScale:run No current mesh"));

      QString heatName = parm("Heat Name");
      if(heatName.isEmpty())
        heatName = mesh->heat();

      return run(mesh, heatName, parm("Type"));
    }

    bool finalize(QWidget *parent)
    {
      if(parent and mesh and !newHeatName.isEmpty()) {
        mesh->drawParms(ccName).setGroupVisible("Faces", true);
        mesh->drawParms(ccName).setRenderChoice("Faces", newHeatName);
        mesh->setHeat(newHeatName);
      }
      return true;
    }

    bool run(Mesh* mesh, const QString &heatName, const QString &type);

  private:
    Mesh *mesh = 0;
    QString ccName, newHeatName;
  };

  class SelectByHeat : public Process
  {
  public:
    SelectByHeat(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Tools/Select By Heat");
      setDesc("Select Cells according to their heat value.");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Lower Threshold", "Lower threshold of hear for cells to select", "0.0");
      addParm("Upper Threshold", "Upper threshold of hear for cells to select", "1.0");
      addParm("Reset Selection", "Reset existing selection first", "Yes", booleanChoice());
    }
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, parm("Lower Threshold").toDouble(), 
                            parm("Upper Threshold").toDouble(), stringToBool(parm("Reset Selection")));
    }
    bool run(Mesh* m, double lowerThreshold, double upperThreshold, bool resetSelection);
  };

  class HeatMap : public Process
  {
    Q_OBJECT
  public:
    HeatMap(const Process& process) : Process(process) 
    {
      setName("Mesh/Heat Map/Heat Map");
      setDesc("New Heat Map process that supports Attribute Maps used for:\n"
              "- generation of heat maps of any measure or importing attribute maps as heat maps\n"
              "- generation of change maps of any measure or attribute maps\n"
              "- generation of heat maps based on label or parent");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Measure", "Measure process to use for heat map", "Geometry/Area");
      addParm("Force Recalc", "Force the recalculation, instead of using existing attribute map", "Yes", booleanChoice());
      addParm("Change Map", "Compute a change map between this and the other mesh", "No", booleanChoice());
      addParm("Change Dir", "Direction for the change map computation", "Increasing",
                                                   QStringList() << "Increasing" << "Decreasing" << "Growth");
      addParm("Change Type", "Type of the change map computation", "Ratio", QStringList() << "Ratio" << "Difference");
      addParm("Growth Time", "Time for heat map of growth", "0.0001");
      addParm("Custom Range", "Use a custom range for the heat values", "No", booleanChoice());
      addParm("Range Min", "Custom range minimum", "0.0");
      addParm("Range Max", "Custom range maximum", "1.0");
      addParm("Heat Map", "Name of the output heat map, empty for measure name", "");
    }

    bool initialize(QWidget *parent);
    bool run()
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No cell complex").arg(name());

      heatName = parm("Heat Map");
      Mesh *mesh2 = otherMesh();
      
      // Get the measure and set the type
      QString measure = parm("Measure");
      if(measure.startsWith("Measures3D"))
        measureType = "Volumes";
      else
        measureType = "Faces";

      bool result = run(mesh, mesh2, measure, stringToBool(parm("Change Map")), 
                             parm("Change Dir"), parm("Change Type"), parm("Growth Time").toDouble(), heatName); 

      IntDoubleAttr &heatAttr = mesh->heatAttr<double>(heatName);
      if(stringToBool(parm("Custom Range")))
        mesh->setHeatBounds(Point2d(parm("Range Min").toDouble(), parm("Range Max").toDouble()), heatName);
      else
        mesh->setHeatBounds(calcBounds(heatAttr), heatName);

      return result;
    }
    bool run(Mesh* currentMesh, Mesh* otherMesh, const QString &measure, bool changeMap, const QString &changeDir, 
                                                          const QString &changeType, double growthTime, QString &heatName);

    // If called from the GUI, make it visible
    bool finalize(QWidget *parent)
    {
      if(parent and mesh and !ccName.isEmpty() and !heatName.isEmpty() and errorMessage().isEmpty()) {
        mesh->setHeat(heatName);
        mesh->updateProperties(ccName);
        if(!measureType.isEmpty()) {
          mesh->drawParms(ccName).setGroupVisible("Faces", false);
          if(measureType != "Volumes" and mesh->ccStructure(ccName).maxDimension() > 2)
            mesh->drawParms(ccName).setGroupVisible("Volumes", false);
          mesh->drawParms(ccName).setGroupVisible(measureType, true);
          mesh->drawParms(ccName).setRenderChoice(measureType, heatName);
        }
      }
      return true;
    }

    bool getHeatAttr(Mesh *mesh, const QString &processName, QString &heatName);

  public slots:
    void onTreeWidgetClicked(QTreeWidgetItem *, int);

  private:
    Mesh *mesh = 0;
    QString ccName, heatName, measureType;
  };
}


// Old heat map process (heat map classic in MGX)

//  /**
//   * \class ComputeHeatMap ProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
//   *
//   * Compute heat map on the current mesh. For details, look at the description
//   * of the various parameters.
//   */
//  class mdxBase_EXPORT ComputeHeatMap : public Process
//  {
//    Q_OBJECT
//  public:
//    ComputeHeatMap(const Process& process) : Process(process) {}
//
//    bool run(const QStringList &parms);
//
//    enum MapType { AREA, VOLUME, WALL };
//
//    enum SignalType {
//      GEOMETRY = 0x1,
//      BORDER = 0x2,
//      INTERIOR = 0x4,
//      TOTAL = 0x8,
//      BORDER_TOTAL = 0x10,
//      INTERIOR_TOTAL = 0x20
//    };
//
//    enum PolarityType { NONE, CELL_AVERAGE, WALL_MIN };
//
//    enum MultiMapType {
//      SINGLE,
//      INCREASING_RATIO,
//      INCREASING_DIFF,
//      DECREASING_RATIO,
//      DECREASING_DIFF,
//      INCREASING_GROWTH,
//      DECREASING_GROWTH
//    };
//
//    struct ImageHeatMaps {
//      IntFloatAttr LabelGeom;         // Label Areas
//      IntFloatAttr LabelBordGeom;     // Area of label borders
//      IntFloatAttr LabelIntGeom;      // Area of label interiors
//      IntFloatAttr LabelTotalSig;     // Label total signal
//      IntFloatAttr LabelBordSig;      // Label border signal
//      IntFloatAttr LabelIntSig;       // Label interior signal
//      IntFloatAttr LabelHeatAmt;      // Label HeatMap temp for change map
////      IntHVec3uMap LabelTris;        // Label triangle lists
////      IntVIdSetMap LabelPts;         // Label triangle list points
//
//      IntIntFloatAttr WallBordGeom;     // Wall border areas
//      IntIntFloatAttr WallBordSig;      // Wall border signal
//      IntIntFloatAttr WallHeatAmt;      // Wall border signal amt for change map
//    };
//
//    bool run(Mesh* mesh1, Mesh* mesh2, MapType map, SignalType signal,
//             const QString& reportFile, int report, bool manualRange, float rangeMin,
//             float rangeMax, bool signalAverage, bool globalCoordinates, PolarityType polarity,
//             MultiMapType multiMapType, float growthTime, float borderSize);
//
//    bool initialize(QStringList& parms, QWidget* parent);
//
//      setName("Mesh/Heat Map/Heat Map");
//      setDesc("Generate heat map for the current mesh");
//    QStringList parmNames() const { return QStringList()
//          << "Type" << "Visualize" << "FileName" << "ReportFields" << "Man. Range" << "Range Min"
//          << "Range Max" << "Signal Avg" << "Global Coord" << "Polarity Type" << "Change Map"
//          << "Increasing" << "Diff Type" << "Growth Time" << QString("Border Size(%1)").arg(UM));
//    QStringList parmDescs() const { return QStringList()
//          << "Area: signal/geometry on curved surfaces,\n"
//             "Volume: for 3D only,\n"
//             "Walls: quantify signal or geometry at cell borders."
//          << "Geometry: cell areas or volume,\n"
//             "Border signal: signal associated with cell borders, "
//             "within a given distance (Border Size)\n"
//             "Interior signal: total signal of a cell - border signal.\n"
//          << "Path to output file."
//          << "Options to report the following fields in spreadsheet: Geometry, Signal, Border-Interior"
//          << "Manually define the range of the color map."
//          << "Color map lower bound."
//          << "Color map upper bound."
//          << "Option to normalize the signal by cell area or volume"
//          << "Apply the rotation/translation of the stack to the cell center coordinates."
//          << "Experimental option, attempt to determine cell signal polarity based "
//          << "on strength of signal on different walls.\n"
//             "Cell Average: compare each wall signal to signal average,\n"
//             "Wall/Min: compare wall signal to the weakest wall signal in the cell."
//          << "Compare two meshes with each other (deformation or change in signal) .\n"
//          << "Increasing: the current mesh is the reference (T0), the other mesh is the "
//             "changed state (T1),\n"
//             "Decreasing: the other mesh is the reference (T0), the current mesh is the "
//             "changed state (T1)."
//          << "Ratio: area or signal in changed state(T1) / area or signal in reference (T0),\n"
//             "Difference: area or signal in changed state(T1) - area or signal in reference (T0),\n"
//             "Growth: (Ratio -1)  / growth time.\n"
//          << "Time interval between the two samples." << QString("Border Size(%1)").arg(UM));
//    QStringList parmDefaults() const { return QStringList()
//          << "Area" << "Geometry" << "" << "Geometry" << "No" << "0" << "65535" << "Yes"
//          << "No" << "None" << "No" << "Yes" << "Ratio" << ".001" << "1.0");
//      setIcon(QIcon(":/images/MakeHeatMap.png"));
//
//    QDialog* dlg;
//
//    // was protected, now accessed by cell atlas
//    bool getLabelMaps(ImageHeatMaps& map, Mesh* mesh);
//
//  protected slots:
//    void changeMapType(const QString& type);
//    void selectSpreadsheetFile();
//
//  protected:
//    
//    MapType mapType;
//    SignalType signal;
//    int report;
//    bool manualRange;
//    float rangeMin;
//    float rangeMax;
//    bool signalAverage;
//    bool globalCoordinates;
//    PolarityType polarity;
//    bool changeMap;
//    MultiMapType multiMapType;
//    float growthTime;
//    float borderSize;
//  };
  


#endif



