//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#include "PresetColors.hpp"

using namespace mdx;

const ColorbVec& PresetColors::labels(void)
{
  static ColorbVec colors(16);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(255, 0, 0, 255);
    colors[1] = Colorb(0, 255, 0, 255);
    colors[2] = Colorb(0, 0, 255, 255);
    colors[3] = Colorb(255, 255, 0, 255);
    colors[4] = Colorb(255, 0, 255, 255);
    colors[5] = Colorb(0, 255, 255, 255);
    colors[6] = Colorb(128, 0, 0, 255);
    colors[7] = Colorb(0, 128, 0, 255);
    colors[8] = Colorb(0, 0, 128, 255);
    colors[9] = Colorb(128, 0, 128, 255);
    colors[10] = Colorb(128, 128, 0, 255);
    colors[11] = Colorb(255, 0, 128, 255);
    colors[12] = Colorb(128, 128, 255, 255);
    colors[13] = Colorb(0, 128, 255, 255);
    colors[14] = Colorb(128, 0, 255, 255);
    colors[15] = Colorb(128, 255, 0, 255);
  }
  return colors;
}

const ColorbVec& PresetColors::jet(void)
{
  static ColorbVec colors(9);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(0, 0, 128, 255);
    colors[1] = Colorb(0, 0, 255, 255);
    colors[2] = Colorb(0, 128, 255, 255);
    colors[3] = Colorb(0, 255, 255, 255);
    colors[4] = Colorb(128, 255, 128, 255);
    colors[5] = Colorb(255, 255, 0, 255);
    colors[6] = Colorb(255, 128, 0, 255);
    colors[7] = Colorb(255, 0, 0, 255);
    colors[8] = Colorb(128, 0, 0, 255);
  }
  return colors;
}

const ColorbVec& PresetColors::frenchFlag(void)
{
  static ColorbVec colors(3);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(0,0,255,255);
    colors[1] = Colorb(255,255,255,255);
    colors[2] = Colorb(255,0,0,255);
  }
  return colors;
}

const ColorbVec& PresetColors::blackbody(void)
{
  static ColorbVec colors(15);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(16,0,0,255);
    colors[1] = Colorb(58,0,0,255);
    colors[2] = Colorb(85,0,0,255);
    colors[3] = Colorb(113,0,0,255);
    colors[4] = Colorb(143,0,0,255);
    colors[5] = Colorb(174,0,0,255);
    colors[6] = Colorb(206,0,0,255);
    colors[7] = Colorb(239,0,0,255);
    colors[8] = Colorb(255,64,21,255);
    colors[9] = Colorb(242,122,0,255);
    colors[10] = Colorb(242,155,0,255);
    colors[11] = Colorb(250,181,0,255);
    colors[12] = Colorb(255,206,68,255);
    colors[13] = Colorb(255,231,150,255);
    colors[14] = Colorb(255,253,239,255);
  }
  return colors;
}

const ColorbVec& PresetColors::uniformJet(void)
{
  static ColorbVec colors(11);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(49, 49, 183, 255);
    colors[1] = Colorb(0, 110, 169, 255);
    colors[2] = Colorb(0, 145, 176, 255);
    colors[3] = Colorb(0, 171, 179, 255);
    colors[4] = Colorb(15, 188, 163, 255);
    colors[5] = Colorb(94, 190, 128, 255);
    colors[6] = Colorb(142, 179, 82, 255);
    colors[7] = Colorb(169, 156, 0, 255);
    colors[8] = Colorb(171, 126, 0, 255);
    colors[9] = Colorb(160, 86, 0, 255);
    colors[10] = Colorb(146, 7, 7, 255);
  }
  return colors;
}

const ColorbVec& PresetColors::helixWarm(void)
{
  static ColorbVec colors(18);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(0,19,25,255);
    colors[1] = Colorb(0,30,44,255);
    colors[2] = Colorb(0,40,69,255);
    colors[3] = Colorb(31,45,93,255);
    colors[4] = Colorb(66,46,117,255);
    colors[5] = Colorb(101,42,136,255);
    colors[6] = Colorb(136,32,150,255);
    colors[7] = Colorb(169,17,158,255);
    colors[8] = Colorb(198,5,158,255);
    colors[9] = Colorb(223,21,150,255);
    colors[10] = Colorb(244,47,134,255);
    colors[11] = Colorb(255,80,107,255);
    colors[12] = Colorb(255,115,80,255);
    colors[13] = Colorb(255,142,43,255);
    colors[14] = Colorb(252,167,0,255);
    colors[15] = Colorb(247,190,0,255);
    colors[16] = Colorb(241,212,0,255);
    colors[17] = Colorb(232,234,0,255);
  }
  return colors;
}

const ColorbVec& PresetColors::helixCool(void)
{
  static ColorbVec colors(19);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(26,15,0,255);
    colors[1] = Colorb(46,21,0,255);
    colors[2] = Colorb(67,25,7,255);
    colors[3] = Colorb(90,26,30,255);
    colors[4] = Colorb(113,23,56,255);
    colors[5] = Colorb(136,14,86,255);
    colors[6] = Colorb(157,0,119,255);
    colors[7] = Colorb(173,0,155,255);
    colors[8] = Colorb(183,21,194,255);
    colors[9] = Colorb(182,54,232,255);
    colors[10] = Colorb(158,96,255,255);
    colors[11] = Colorb(120,133,255,255);
    colors[12] = Colorb(76,160,255,255);
    colors[13] = Colorb(12,183,251,255);
    colors[14] = Colorb(0,201,245,255);
    colors[15] = Colorb(0,218,243,255);
    colors[16] = Colorb(75,233,239,255);
    colors[17] = Colorb(135,245,235,255);
    colors[18] = Colorb(182,255,237,255);
  }
  return colors;
}

const ColorbVec& PresetColors::helixFull(void)
{
  static ColorbVec colors(19);
  static bool init = false;
  if(!init)
  {
    init = true;

    colors[0] = Colorb(0,21,9,255);
    colors[1] = Colorb(8,33,0,255);
    colors[2] = Colorb(34,41,0,255);
    colors[3] = Colorb(56,49,0,255);
    colors[4] = Colorb(81,54,3,255);
    colors[5] = Colorb(108,57,33,255);
    colors[6] = Colorb(134,58,69,255);
    colors[7] = Colorb(157,57,110,255);
    colors[8] = Colorb(172,63,153,255);
    colors[9] = Colorb(169,82,196,255);
    colors[10] = Colorb(137,115,222,255);
    colors[11] = Colorb(81,148,222,255);
    colors[12] = Colorb(0,171,212,255);
    colors[13] = Colorb(0,189,200,255);
    colors[14] = Colorb(0,207,190,255);
    colors[15] = Colorb(51,224,170,255);
    colors[16] = Colorb(121,236,139,255);
    colors[17] = Colorb(187,244,97,255);
    colors[18] = Colorb(250,248,52,255);
  }
  return colors;
}
