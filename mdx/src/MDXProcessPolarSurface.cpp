//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

// Implement surface and surface point class
#include <iostream>
#include <cstdio>
#include <cmath>

#include <MDXProcessPolarSurface.hpp>

namespace mdx
{
  // Polar surface parms process
  bool PolarSurface2DParms::initialize(QWidget * /*parent*/) 
  { 
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw(QString(" PolarSurface2DParms::initialize No current mesh"));
    processParms();

    // Initialize the tissue
    if(!getProcess(parm("Tissue Process"), cellTissueProcess))
      throw(QString("PolarSurface2DParms::initialize: Unable to create Tissue process '%1'")
                                                                 .arg(parm("Tissue Process")));
    cellTissueProcess->initialize(*mesh);
    CellTissue &tissue = cellTissueProcess->tissue();
    indexAttr = &tissue.indexAttr();
    pData = &mesh->attributes().attrMap<CCIndex,PolarSurface2D::PolarData>("MDXPolarSurface");

    // Defined in PolarSurface2D
    initialize(tissue, pData, indexAttr);
    return true;
  }

  // Surface growth process
  // Initialize the surface
  bool PolarSurface2DGrowth::initialize(QWidget *parent)
  {
    mesh = currentMesh();
    if(!mesh)
      throw(QString("PolarSurface2DGrowth::initialize No current mesh"));

    // Read the parms from the GUI
    processParms();

    // Initialize the polar surface 
    if(!getProcess(SurfaceProcessName, surfaceProcess))
      throw(QString("PolarSurface2DGrowth.initialize: Unable to create Surface process"));
    surfaceProcess->initialize(parent);
    pData = &mesh->attributes().attrMap<CCIndex,PolarSurface2D::PolarData>("MDXPolarSurface");
    polarSurface2D = &surfaceProcess->polarSurface();
    surfaceTissue = &surfaceProcess->tissue();
    indexAttr = &surfaceTissue->indexAttr();

    return true;
  } 

  // Process model parameters
  bool PolarSurface2DGrowth::processParms()
  {
		// Get model parms
    Dt = parm("Dt").toDouble();
    CellKill = parm("Cell Kill").toDouble();
    SurfaceProcessName = parm("Surface Process");

    return true;
  }

  // Main model step
	bool PolarSurface2DGrowth::step()
  {    
    if(!surfaceTissue or !polarSurface2D)
      throw(QString("PolarSurface2DGrowth.step: Invalid tissue or surface"));

    // Grow
    const CCIndexVec &vertices = tissue().cellStructure().vertices(); 
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); i++)
      polarSurface2D->growPoint(vertices[i], Dt, 0);
    const CCIndexVec &cells = tissue().dualGraph().vertices(); 
    #pragma omp parallel for
    for(uint i = 0; i < cells.size(); i++)
      polarSurface2D->growPoint(cells[i], Dt, 0);  

    tissue().updateGeometry();

    // Kill distant cells, can we move this somewhere?
    bool doUpdateAll = false;
    std::vector<CCIndex> faces = tissue().cellStructure().faces();
    if(faces.size() > 0) {
      doUpdateAll = true;
      for(uint i = 0 ; i < faces.size() ; i++)
        if((*pData)[faces[i]].r > CellKill)
          tissue().removeCell(faces[i]);
    }

    // Update GUI
    if(doUpdateAll) {
      mesh->updateAll(tissue().tissueName());
      mesh->updateAll(tissue().tissueDualName());
    } else {
      mesh->updatePositions(tissue().tissueName());
      mesh->updatePositions(tissue().tissueDualName());
    }

    return true;
  }

  // Surface growth process
  // Initialize the surface
  bool PolarSurface2DInitialCell::initialize(QWidget *parent)
  {
    mesh = currentMesh();
    mesh->reset();

    // Read the parms from the GUI
    processParms();

    // Initialize the polar surface 
    if(!getProcess(SurfaceProcessName, surfaceProcess))
      throw(QString("PolarSurface2DInitialCell.initialize: Unable to create Surface process"));
    surfaceProcess->initialize(parent);
    pData = &mesh->attributes().attrMap<CCIndex,PolarSurface2D::PolarData>("MDXPolarSurface");
    polarSurface2D = &surfaceProcess->polarSurface();

    tissue = &surfaceProcess->tissue();
    indexAttr = &tissue->indexAttr();

    return true;
  } 

  // Process model parameters
  bool PolarSurface2DInitialCell::processParms()
  {
		// Get model parms
    cellInitWalls = parm("Initial Walls").toInt();
    cellInitSize = parm("Initial Size").toDouble();

    SurfaceProcessName = parm("Surface Process");

    return true;
  }

  bool PolarSurface2DInitialCell::step()
  {
    // Create initial cell
    if(!surfaceProcess or !tissue)
      throw QString("%1::step: Invalid tissue or surface").arg(name());
    if(!surfaceProcess->polarSurface().initialCell(*tissue, cellInitSize, cellInitWalls))
      throw QString("%1::step Unable to create initial cell").arg(name());

    tissue->updateGeometry();

    // Should only print the one cell
    indexAttr = &tissue->indexAttr();
    forall(CCIndex c, tissue->cellStructure().faces())
      mdxInfo << "Initial cell area:" << (*indexAttr)[c].measure << endl;

    mesh->drawParms(tissue->tissueName()).setGroupVisible("Edges", true);
    mesh->drawParms(tissue->tissueName()).setGroupVisible("Faces", true);
    mesh->updateAll(tissue->tissueName());
    mesh->updateAll(tissue->tissueDualName());    // Update GUI

    return false;
  } 
}

