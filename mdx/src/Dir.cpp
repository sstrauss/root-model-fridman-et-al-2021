//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "Dir.hpp"
#include "Information.hpp"

#include <QFileInfo>
#include <QRegExp>
#include <QProcessEnvironment>

namespace mdx 
{
  // static QString _currentPath;
  
  static QString stripDir(const QDir& dir, QString file)
  {
    // mdxInfo << "stripDir('" << dir.absolutePath() << "', '" << file << "')" << endl;
    QFileInfo fi(file);
    QString result = dir.relativeFilePath(fi.absoluteFilePath());
    // mdxInfo << "  - '" << result << "'" << endl;
    if(result.startsWith(".."))   // If file is not in a subfolder of dir, then try to go up
    {
      QDir cur = fi.dir();
      QString filename = fi.fileName();
      QString r;
      do {
        r = dir.relativeFilePath(QDir(cur.canonicalPath()).filePath(filename));
        // mdxInfo << " # '" << cur.canonicalPath() << "' // '" << filename << "'" << endl;
        // mdxInfo << " - '" << r << "'" << endl;
        if(result.size() > r.size())
          result = r;
  
        filename = QDir(cur.dirName()).filePath(filename);
        if(!cur.cdUp())
          break;
      } while(r.startsWith(".."));
      if(not r.startsWith(".."))
        result = r;
    }
    // mdxInfo << " --> '" << result << "'" << endl;
    if(result == ".")
      result.clear();
    return result;
  }
  
  QString stripCurrentDir(QString file) {
    return stripDir(currentPath(), file);
  }
  
  // Strip directory from name
  QString stripDir(const QString& dir, QString file) {
    return stripDir(QDir(dir), file);
  }
  
  // Return directory from name
  QString getDir(QString file)
  {
    // mdxInfo << "getDir('" << file << "' = '";
    QString realFile = file.trimmed();
    QString result;
    if(QDir::isRelativePath(realFile))
      result = absoluteFilePath(realFile);
    else
      result = realFile;
    // mdxInfo << result << "'\n" << endl;
    return QFileInfo(result).absolutePath();
  }
  
  bool setCurrentPath(const QString& path) {
    return QDir::setCurrent(path);
  }
  
  QString currentPath() {
    return QDir::current().absolutePath();
  }
  
  QDir currentDir() {
    return QDir::current();
  }
  
  QString absoluteFilePath(QString filename)
  {
    if(QDir::isRelativePath(filename))
      return QDir::cleanPath(currentDir().absoluteFilePath(filename));
    else
      return filename;
  }
  
  QString resolvePath(QString path)
  {
    path = path.trimmed();
    if(path.startsWith("~/"))
      path = QDir::home().filePath(path.mid(2));
    QRegExp env("\\$(\\w+)");
    QProcessEnvironment environment = QProcessEnvironment::systemEnvironment();
    int idx;
    while((idx = env.indexIn(path)) != -1) {
      QString rep = environment.value(env.cap(1));
      path = path.left(idx) + rep + path.mid(idx + env.matchedLength());
    }
    return absoluteFilePath(path);
  }
}
