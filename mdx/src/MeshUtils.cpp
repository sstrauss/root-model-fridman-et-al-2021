//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2019 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MeshUtils.hpp>

namespace mdx
{
  // Note this routine relies on faces being calculated
  bool updateVertexNormal(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex v)
  {
    CCIndexData &vIdx = indexAttr[v];
    Point3d nrml(0,0,0);
    
    for(CCIndex f : vertexFaces(cs, v)) {
      auto &fIdx = indexAttr[f];
      nrml += fIdx.nrml * fIdx.measure;
    }
    double nrm = norm(nrml);
    if(nrm > 0)
      vIdx.nrml = nrml/nrm;
    else if(isNan(norm(vIdx.nrml)))
      vIdx.nrml = Point3d(0,0,1.0);

    return true;
  }

  bool updateVertexNormals(const CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    // If there are no faces, just return
    if(cs.faces().size() == 0)
      return true;

    // Calculate vertex normals from faces
    const CCIndexVec &vertices = cs.vertices();
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); i++)
      updateVertexNormal(cs, indexAttr, vertices[i]);

    return true;
  }

  bool updateFaceGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex f)
  {
    if(cs.dimensionOf(f) != 2) {
      printf("updateFaceGeometry Bad face passed, dimension %d\n", cs.dimensionOf(f)); 
      return false;
    }
    CCIndexVec nbs = faceVertices(cs, f);
    CCIndexData &fIdx = indexAttr[f];

    if(nbs.size() < 3) {
      printf("updateFaceGeometry Bad face with only %d sides\n", int(nbs.size())); 
      return false;
    } else if(nbs.size() == 3) {
      CCIndexData &vIdx = indexAttr[nbs[0]];
      CCIndexData &mIdx = indexAttr[nbs[1]];
      CCIndexData &nIdx = indexAttr[nbs[2]];
      fIdx.pos = (vIdx.pos + mIdx.pos + nIdx.pos) / 3.0;
      Point3d cross = (mIdx.pos - vIdx.pos) ^ (nIdx.pos - vIdx.pos);
      double crossNorm = norm(cross);
      fIdx.measure = crossNorm/2.0;
      if(crossNorm > 0)
        fIdx.nrml = cross/crossNorm;
      else
        fIdx.nrml = Point3d(0,0,1.0);
    } else {
      // Estimate center
      Point3d center(0,0,0);
      for(CCIndex v : nbs)
        center += indexAttr[v].pos;
      center /= nbs.size();

      // Triangle fan to calculate the rest
      fIdx.measure = 0; 
      fIdx.pos = Point3d(0,0,0);
      fIdx.nrml = Point3d(0,0,0);
      for(uint i = 0; i < nbs.size(); i++) {
        CCIndexData &mIdx = indexAttr[nbs[i]];
        CCIndexData &nIdx = indexAttr[nbs[(i+1)%nbs.size()]];
        Point3d cross = (mIdx.pos - center) ^ (nIdx.pos - center);
        fIdx.nrml += cross;
        double area = norm(cross)/2.0;
        fIdx.pos += (center + mIdx.pos + nIdx.pos)/3.0 * area;
        fIdx.measure += area;
      }
      if(fIdx.measure > 0)
        fIdx.pos /= fIdx.measure;
      double nrm = norm(fIdx.nrml);
      if(nrm > 0)
        fIdx.nrml /= nrm;
      else
        fIdx.nrml = Point3d(0,0,1.0);
    }

    return true;
  }

  bool updateFaceGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    // If there are no faces, just return
    if(cs.faces().size() == 0)
      return true;

    // Calculate face values
    const CCIndexVec &faces = cs.faces();
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++)
      updateFaceGeometry(cs, indexAttr, faces[i]);

    return true;
  }

  bool updateGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    // If there are no faces, just return
    if(cs.faces().size() == 0)
      return true;

    bool result = updateFaceGeometry(cs, indexAttr);
    //result &= updateVolumeGeometry(cs, indexAttr);

    const CCIndexVec &volumes = cs.volumes();
    #pragma omp parallel for
    for(uint i = 0; i < volumes.size(); i++) {
      CCIndex l = volumes[i];
      CCIndexData &lIdx = indexAttr[l];
      lIdx.pos.zero();
      auto bounds = cs.bounds(l);
      for(CCIndex f : bounds)
        lIdx.pos += indexAttr[f].pos;
      lIdx.pos /= bounds.size();
    }

		result &= updateVertexNormals(cs, indexAttr);
    return result;
  }

  bool updateVolumeGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    const CCIndexVec &volumes = cs.volumes();
    #pragma omp parallel for
    for(uint i = 0; i < volumes.size(); i++) {
      CCIndex l = volumes[i];

      CentroidData<Point3d> ccd = cellCentroidData(l, cs, indexAttr);
      CCIndexData &indexData = indexAttr[l];
      indexData.measure = ccd.measure;
      indexData.pos = ccd.centroid;
    }
    return true;
  }

  bool faceAttrFromVertices(const CCStructure &cs, CCIndexDoubleAttr &attr, const CCIndexVec &faces)
  {
    #pragma omp parallel for
    for(size_t i = 0; i < cs.faces().size(); i++) {
      CCIndex f = cs.faces()[i];
      CCIndexSet nbs = cs.incidentCells(f, 0);
      if(nbs.size() == 0)
        continue;
      double &s = attr[f];
      s = 0;
      for(CCIndex n : nbs)
        s += attr[n];
      s /= nbs.size();
    }
    return true;
  }

  bool faceAttrFromVertices(Mesh &mesh, const CCStructure &cs, const CCIndexVec &faces)
  {
    bool result = true;
    for(const QString &signal : mesh.signalAttrList())
      result &= faceAttrFromVertices(cs, mesh.signalAttr<double>(signal), faces);

    return result;
  }

  bool faceAttrToVertices(const CCStructure &cs, CCIndexDoubleAttr &attr, const CCIndexVec &vertices)
  {
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); i++) {
      CCIndex v = cs.vertices()[i];
      CCIndexSet nbs = cs.incidentCells(v, 2);
      if(nbs.size() == 0)
        continue;
      double &s = attr[v];
      s = 0;
      for(CCIndex n : nbs)
        s += attr[n];
      s /= nbs.size();
    }
    return true;
  }

  bool faceAttrToVertices(Mesh &mesh, const CCStructure &cs, const CCIndexVec &vertices)
  {
    bool result = true;
    for(const QString &signal : mesh.signalAttrList())
      result &= faceAttrToVertices(cs, mesh.signalAttr<double>(signal), vertices);

    return result;
  }

  bool vertexLabelsFromVolumes(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &vertices)
  {
    // First clear the labels
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); ++i)
      indexAttr[vertices[i]].label = 0;

    // Push the labels from the volumes to the vertices
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); ++i) {
      CCIndex v = vertices[i];
      auto &vIdx = indexAttr[v];
      // Get the label from the face, vertices with faces 
      // that have more than one label get -1
      for(CCIndex f : cs.incidentCells(v, 3)) { // TODO make the dimension a parameter? and merge with face function?
        auto &fIdx = indexAttr[f];
        if(fIdx.label != 0) {
          if(vIdx.label == 0)
            vIdx.label = fIdx.label;
          else if(fIdx.label != vIdx.label) {
            vIdx.label = -1;
            break;
          }
        }
      }
    }
    return true;
  }

  bool faceLabelsFromVertices(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &faces)
  {
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); ++i) {
      CCIndex f = faces[i];
      auto &fIdx = indexAttr[f];
 
      CCIndexVec fVertices = faceVertices(cs, f);
      for(CCIndex v : fVertices) {
        auto &vIdx = indexAttr[v];
        if(vIdx.label > 0) {
          fIdx.label = vIdx.label;
          break;
        } 
      }
    }
    return true;
  }


  bool vertexLabelsFromFaces(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &vertices)
  {
    // First clear the labels
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); ++i)
      indexAttr[vertices[i]].label = 0;

    // Push the labels from the faces to the vertices
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); ++i) {
      CCIndex v = vertices[i];
      auto &vIdx = indexAttr[v];
      // Get the label from the face, vertices with faces 
      // that have more than one label get -1
      for(CCIndex f : cs.incidentCells(v, 2)) {
        auto &fIdx = indexAttr[f];
        if(fIdx.label != 0) {
          if(vIdx.label == 0)
            vIdx.label = fIdx.label;
          else if(fIdx.label != vIdx.label) {
            vIdx.label = -1;
            break;
          }
        }
      }
    }
    return true;
  }

  bool faceLabelsFromVolumes(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &faces, IntIntAttr *labelMap)
  {
    // First clear the labels
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); ++i)
      indexAttr[faces[i]].label = 0;

    // Push the labels from the faces to the vertices
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); ++i) {
      CCIndex f = faces[i];
      auto &fIdx = indexAttr[f];
      // Get the label from the face, vertices with faces 
      // that have more than one label get -1
      for(CCIndex l : cs.cobounds(f)) {
        int label = indexAttr[l].label;
        if(labelMap)
          label = (*labelMap)[label];
        if(label != 0) {
          if(fIdx.label == 0)
            fIdx.label = label;
          else if(label != fIdx.label) {
            fIdx.label = -1; // Not sure if this makes sense for faces, but it is what was done in MGX for vertices
            break;
          }
        }
      }
    }
    return true;
  }

  // Return active vertices, selected or all if none selected 
  CCIndexVec activeVertices(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex v : cs.vertices())
      if(indexAttr[v].selected)
        selected.push_back(v);
    if(selected.size() > 0)
      return selected;
    return cs.vertices();
  }

  // Return active edges, edges with both vertices selected or all if none selected 
  CCIndexVec activeEdges(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex e : cs.edges()) {
      auto eb = cs.edgeBounds(e);
      if(indexAttr[eb.first].selected and indexAttr[eb.second].selected)
        selected.push_back(e);
    }
    if(selected.size() > 0)
      return selected;
    return cs.edges();
  }

  // Return active faces, selected or all if none selected 
  CCIndexVec activeFaces(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex f : cs.faces())
      if(indexAttr[f].selected)
        selected.push_back(f);
    if(selected.size() > 0)
      return selected;
    return cs.faces();
  }

  // Return active volumes, selected or all if none selected 
  CCIndexVec activeVolumes(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex f : cs.volumes())
      if(indexAttr[f].selected)
        selected.push_back(f);
    if(selected.size() > 0)
      return selected;
    return cs.volumes();
  }

  // Return selected vertices 
  CCIndexVec selectedVertices(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex v : cs.vertices())
      if(indexAttr[v].selected)
        selected.push_back(v);
    return selected;
  }

  // Return selected edges
  CCIndexVec selectedEdges(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex e : cs.edges()) {
      auto eb = cs.edgeBounds(e);
      if(indexAttr[eb.first].selected and indexAttr[eb.second].selected)
        selected.push_back(e);
    }
    return selected;
  }

  // Return selected faces
  CCIndexVec selectedFaces(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex f : cs.faces())
      if(indexAttr[f].selected)
        selected.push_back(f);
    return selected;
  }

  // Return selected volumes
  CCIndexVec selectedVolumes(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(CCIndex vol : cs.volumes())
      if(indexAttr[vol].selected)
        selected.push_back(vol);
    return selected;
  }

  // Return selected cells 
  CCIndexVec selectedCells(const CCStructure &cs, const CCIndexDataAttr &indexAttr)
  {
    CCIndexVec selected;
    for(int dim = 0; dim <= cs.maxDimension(); dim++)
      for(CCIndex c : cs.cellsOfDimension(dim)) {
        auto it = indexAttr.find(c);
        if(it != indexAttr.end() and it->second.selected)
          selected.push_back(c);
      }
    return selected;
  }

  bool ccUpdateDrawParms(Mesh &mesh, const QString &ccIn, const QString &ccOut)
  {
    bool doCCIn = true;
    if(ccIn.isEmpty())
      doCCIn = false;

    if(ccOut.isEmpty())
      throw QString("ccUpdateDrawParms Output CC is empty");

    // Get the draw parms
    auto &inp = mesh.drawParms(ccIn);
    auto &outp = mesh.drawParms(ccOut);

    if(doCCIn) {
      // Copy edge and vertex states
      outp.setGroupVisible("Vertices", inp.isGroupVisible("Vertices"));
      outp.setGroupVisible("Edges", inp.isGroupVisible("Edges"));
      // Turn input cc off
      inp.setAllGroupsVisible(false);
    }

    if(mesh.ccStructure(ccOut).maxDimension() > 2) {
      outp.setGroupVisible("Faces", false);
      outp.setGroupVisible("Volumes", true);
    } else {
      outp.setGroupVisible("Faces", true);
      outp.setGroupVisible("Volumes", false);
    }
    // Update views
    mesh.setCCName(ccOut);
    mesh.updateAll(ccOut);

    return true;
  }

  // Calculate heat from cell axis
  bool heatFromCellAxis(const QString &display, const IntSymTensorAttr &cellAxisAttr, IntDoubleAttr &heatAttr)
  {
    for(auto &pr : cellAxisAttr) {
      if(display == "Ratio")
        heatAttr[pr.first] = max(pr.second.evals()[0], pr.second.evals()[1])/min(pr.second.evals()[0], pr.second.evals()[1]);
      else if(display == "Max")
        heatAttr[pr.first] = max(pr.second.evals()[0], pr.second.evals()[1]);
      else if(display == "Min")
        heatAttr[pr.first] = min(pr.second.evals()[0], pr.second.evals()[1]);
      else {
        mdxInfo << "heatFromAxis Unknown display method: " << display << endl;
        return false;
      }
    }
    return true;
  }

  // Find centers and normals for axis data
  bool calcCellAxisCentersNormals(const CCStructure &cs, const CCIndexDataAttr &indexAttr, IntIntAttr *labelMap, IntMatrix2x3dAttr &centerNormalAttr)
  {
    centerNormalAttr.clear();
    // First find centers
    IntDoubleAttr labelCenterWeight;
    tbb::concurrent_unordered_map<int, CCIndexTbbVec, std::hash<int>, std::equal_to<int> > labelFaces;
    #pragma omp parallel for
    for(uint i = 0; i < cs.faces().size(); i++) {
      CCIndex f = cs.faces()[i];
      auto &fIdx = indexAttr[f];
      int label = fIdx.label;
      if(labelMap)
        label = (*labelMap)[fIdx.label];
      if(label <= 0)
        continue;
      auto &centerNormal = centerNormalAttr[label];
      Point3f pos(fIdx.pos * fIdx.measure);
      Point3f nrml(fIdx.nrml * fIdx.measure);
      for(uint j = 0; j < 3; j++) {
        #pragma omp atomic
        centerNormal[0][j] += pos[j];
        #pragma omp atomic
        centerNormal[1][j] += nrml[j];
      }
      #pragma omp atomic
      labelCenterWeight[label] += fIdx.measure;
      labelFaces[label].push_back(f);
    }
    for(auto &pr : centerNormalAttr) {
      pr.second[0] /= labelCenterWeight[pr.first];
      pr.second[1] /= labelCenterWeight[pr.first];
    }

    // Push centers to surface, use closest triangle to center
    for(auto &pr : centerNormalAttr)  {
      for(CCIndex f : labelFaces[pr.first]) {
        Point3d intp;
        CCIndexVec fVertices = faceVertices(cs, f);
        if(fVertices.size() == 3 and rayTriangleIntersect(pr.second[0], pr.second[0] + pr.second[1], 
             indexAttr[fVertices[0]].pos, indexAttr[fVertices[1]].pos, indexAttr[fVertices[2]].pos, intp) == 1) {
          pr.second[0] = intp;
          break;
        }
      }
    }
    return true;
  }

  IntSet faceLabels(const CCStructure &cs, const CCIndexDataAttr &indexAttr, bool selected, bool zero)
  {
    IntSet labels;
    for(CCIndex f : cs.faces()) {
      auto &fIdx = indexAttr[f];
      int label = fIdx.label;
      if(selected and !fIdx.selected)
        continue;
      if(zero or label > 0)
        labels.insert(label);
    }
    return labels;
  }

  IntSet volumeLabels(const CCStructure &cs, const CCIndexDataAttr &indexAttr, bool selected, bool zero)
  {
    IntSet labels;
    for(CCIndex l : cs.volumes()) {
      auto &lIdx = indexAttr[l];
      int label = lIdx.label;
      if(selected and !lIdx.selected)
        continue;
      if(zero or label > 0)
        labels.insert(label);
    }
    return labels;
  }
}
