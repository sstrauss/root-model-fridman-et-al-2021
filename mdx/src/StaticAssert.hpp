//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STATIC_ASSERT_H
#define STATIC_ASSERT_H
/**
 * \file StaticAssert.hpp
 *
 * Define the STATIC_ASSERT macro
 */
#include <boost/static_assert.hpp>

#define STATIC_ASSERT(v) BOOST_STATIC_ASSERT(v)

#endif
