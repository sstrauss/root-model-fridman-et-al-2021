//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "MeshProcessCellAxis.hpp"

// #include "Dir.hpp"
// #include "Triangulate.hpp"
// #include "MeshProcessCurv.hpp"
// #include "MeshProcessFibril.hpp"
// #include "MeshProcessPDG.hpp"
// #include "MeshProcessWallSignalOrientation.hpp"
// #include "PCAnalysis.hpp"
// #include "Progress.hpp"
#include <QFileDialog>

namespace mdx {
  
  bool SaveCellAxis::initialize(QWidget* parent)
  {
    QString fileName = parm("Output File");
    if(fileName.isEmpty() and parent)
      fileName = QFileDialog::getSaveFileName(parent, "Choose spreadsheet file to save", QDir::currentPath(),
                                              "CSV files (*.csv)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".csv", Qt::CaseInsensitive))
      fileName += ".csv";
    setParm(parm("Output File"), fileName);
    return true;
  }
  
  bool SaveCellAxis::run(Mesh* mesh, const QString& filename)
  {
    QFile outputFile;
    QTextStream ts;
    if(filename.isEmpty()) {
      setErrorMessage(QString("Unable to write to output file"));
      return false;
    }
  
    outputFile.setFileName(filename);
    if(!outputFile.open(QIODevice::WriteOnly)) {
      setErrorMessage(QString("Unable to write to output file"));
      return false;
    }

//     //Load cellAxisBezier and the bezier coordimates of cell centers.  
// 		const IntSymTensorAttr& cellAxisBezier = mesh->attributes().attrMap<int, SymmetricTensor>("Cell Axis Bezier");
// 		const IntPoint3fAttr& coordBezier = mesh->attributes().attrMap<int, Point3f>("Cell Coordinates Bezier");

    ts.setDevice(&outputFile);
//     if(mesh->cellAxisType() == "PDG"){
// 		  if(cellAxisBezier.size() == 0) 
//         ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,stretchRatioMax,stretchRatioMin,stretchRatioNormal") << endl;
// 			else
//         ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,stretchRatioMax,stretchRatioMin,stretchRatioNormal,") 
// 				   << QString("bezCoordX,bezCoordY,xBezX,yBezX,xBezY,yBezY,stretchBezX,stretchBezY,shearBez") << endl;
// 			 // (bezCoordX,bezCoordY) =  coordinates of cell center in global bezier coordinates
// 			 // (xBezX,yBezX) =  direction of local vector BezX on which PDG is projected
// 			 // stretchBezX =  stretch in direction of BezX
// 		}
//     else if(mesh->cellAxisType() == "curvature")
//       ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,1/radiusCurvMax,1/radiusCurvMin,1/radiusCurvNormal") << endl;
//     else if(mesh->cellAxisType() == "fibril")
//       ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,alignmentMax,alignmentMin,alignmentNormal") << endl;
// 		else if(mesh->cellAxisType() == "polarization"){// from the addOn "WallSignalOrientation" (for PIN signal)
// 		  if(cellAxisBezier.size() == 0) 
//         ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,signalMax,signalMin,signalNormal") << endl;	
// 			else
//         ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,signalMax,signalMin,signalNormal,") 
// 				   << QString("bezCoordX,bezCoordY,xBezX,yBezX,xBezY,yBezY,signalBezX,signalBezY,misalignmentBez") << endl;
// 			}
//     else if(mesh->cellAxisType() == "PCA"){
// 		  if(cellAxisBezier.size() == 0) 
//         ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,pcaMax,pcaMin,pcaNormal") << endl;
// 			else
//         ts << QString("Label,xMax,yMax,zMax,xMin,yMin,zMin,pcaMax,pcaMin,pcaNormal,") 
// 				   << QString("bezCoordX,bezCoordY,xBezX,yBezX,xBezY,yBezY,pcaBezX,pcaBezY,misalignmentBez") << endl;
// 			}
// 		else {
// 		  setErrorMessage(QString("Unknown cell axis type"));
//       return false;
//     }
  
//     forall(const IntSymTensorPair& p, mesh->cellAxis()) {
//       int label = p.first;
//       const SymmetricTensor& tensor = p.second;
//       const Point3f& vMax = tensor.ev1();
//       const Point3f& vMin = tensor.ev2();
//       const Point3f& eig = tensor.evals();
// 			if(cellAxisBezier.size() != 0 and coordBezier.size() != 0) {
// 		  	if(cellAxisBezier.find(label) == cellAxisBezier.end() or coordBezier.find(label) == coordBezier.end())
// 		  	  continue;
//         const SymmetricTensor& tensorBez = cellAxisBezier.find(label)->second;
//         const Point3f& vX = tensorBez.ev1();
//         const Point3f& vY = tensorBez.ev2();
//         const Point3f& eigBez = tensorBez.evals();
//         const Point3f c = coordBezier.find(label)->second;
//         const Point2f bezCoord = Point2f(c.x(), c.y());
//         ts << label << "," << vMax.x() << "," << vMax.y() << "," << vMax.z() << "," << vMin.x() << "," << vMin.y()
//            << "," << vMin.z() << "," << eig.x() << "," << eig.y() << "," << eig.z() << ","
// 		  		 << bezCoord.x() << "," <<  bezCoord.y() << "," << vX.x() << "," << vX.y() << ","
// 		  		 << vY.x() << "," << vY.y() << "," << eigBez.x() << "," << eigBez.y() << "," << eigBez.z() << endl;
// 		  		 endl;
// 			} else
// 			  ts << label << "," << vMax.x() << "," << vMax.y() << "," << vMax.z() << "," << vMin.x() << "," << vMin.y()
//            << "," << vMin.z() << "," << eig.x() << "," << eig.y() << "," << eig.z() << endl;

//     }
    return true;
  }
  REGISTER_PROCESS(SaveCellAxis);
  
  bool LoadCellAxis::initialize(QWidget* parent)
  {
//     QString& filename = parms[1];
//     if(filename.isEmpty() and parent)
//       filename = QFileDialog::getOpenFileName(parent, "Choose spreadsheet to load", QDir::currentPath(),
//                                               "CSV files (*.csv);;All files (*.*)");
//     if(filename.isEmpty())
//       return false;
//     if(!filename.endsWith(".csv", Qt::CaseInsensitive))
//       filename += ".csv";
//     parms[1] = stripCurrentDir(filename);
    return true;
  }
  
  bool LoadCellAxis::run(Mesh* mesh, const QString& type, const QString& filename)
  {
//     QFile file(filename);
//     if(!file.open(QIODevice::ReadOnly)) {
//       setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
//       return false;
//     }
//     QTextStream ts(&file);
//     QString line = ts.readLine();
//     QStringList fields = line.split(",");
//     QHash<QString, int> fieldMap;
//     int field_xMax, field_yMax, field_zMax;
//     int field_xMin, field_yMin, field_zMin;
//     int field_k1, field_k2, field_k3;
//     int field_label;
//     if(fields.size() < 10) {
//       setErrorMessage(QString("File '%1' should be a CSV file with at least 10 columns").arg(filename));
//       return false;
//     }
//     // First 7 columns of csv files are the same for all cell axis types: 
// 		// Label, first direction (x,y,z), second direction (x,y,z)
//     field_label = fields.indexOf("Label");
//     if(field_label < 0)
//       return setErrorMessage("Error, no field 'Label' in the CSV file");
//     field_xMax = max(fields.indexOf("xMax"), fields.indexOf("x1"));
//     if(field_xMax < 0)
//       return setErrorMessage("Error, no field 'xMax' nor 'x1' in the CSV file");
//     field_yMax = max(fields.indexOf("yMax"), fields.indexOf("y1"));
//     if(field_yMax < 0)
//       return setErrorMessage("Error, no field 'yMax' nor 'y1' in the CSV file");
//     field_zMax = max(fields.indexOf("zMax"), fields.indexOf("z1"));
//     if(field_zMax < 0)
//       return setErrorMessage("Error, no field 'zMax' nor 'z1' in the CSV file");
//     field_xMin = max(fields.indexOf("xMin"), fields.indexOf("x2"));
//     if(field_xMin < 0)
//       return setErrorMessage("Error, no field 'xMin' nor 'x2' in the CSV file");
//     field_yMin = max(fields.indexOf("yMin"), fields.indexOf("y2"));
//     if(field_yMin < 0)
//       return setErrorMessage("Error, no field 'yMin' nor 'y2' in the CSV file");
//     field_zMin = max(fields.indexOf("zMin"), fields.indexOf("z2"));
//     if(field_zMin < 0)
//       return setErrorMessage("Error, no field 'zMin' nor 'z2' in the CSV file");

//     // Next columns of csv files are called differently depending on cell axis types.  
// 		// norm of first vector, norm of second vector, norm of third vector (= 0 for 2D cell axis) 
//     if(type == QString("PDG")) {
//       field_k1 = max(fields.indexOf("stretchRatioMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'stretchRatioMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("stretchRatioMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'stretchRatioMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("stretchRatioNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'stretchRatioNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("curvature")) {
//       field_k1 = max(fields.indexOf("1/radiusCurvMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field '1/radiusCurvMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("1/radiusCurvMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field '1/radiusCurvMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("1/radiusCurvNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field '1/radiusCurvNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("fibril")) {
//       field_k1 = max(fields.indexOf("alignmentMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'alignmentMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("alignmentMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'alignmentMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("alignmentNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'alignmentNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("polarization")) {
//       field_k1 = max(fields.indexOf("signalMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'signalMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("signalMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'signalMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("signalNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'signalNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("PCA")) {
//       field_k1 = max(fields.indexOf("pcaMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'pcaMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("pcaMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'pcaMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("pcaNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'pcaNormal' nor 'k3' in the CSV file");
//     }	else {
// 		  setErrorMessage(QString("Unknown cell axis type"));
//       return false;
//     }
 
//     int nb_fields = fields.size();
  
//     mesh->clearCellAxis();
//     IntSymTensorAttr& cellAxis = mesh->cellAxis();
  
//     int linenum = 1;
//     while(ts.status() == QTextStream::Ok) {
//       ++linenum;
//       fields = ts.readLine().split(",");
//       if(fields.empty() or fields.size() == 1)
//         break;
  
//       if(fields.size() < nb_fields)
//         return setErrorMessage(
//           QString("Error on line %1: not enough fields (Expected: %2, Read: %3)").arg(linenum).arg(nb_fields).arg(
//             fields.size()));
  
//       bool ok;
//       int label = fields[field_label].toInt(&ok);
//       if(!ok) {
//         setErrorMessage(
//           QString("Error line %1: invalid label '%2'").arg(linenum).arg(fields[field_label].trimmed()));
//         return false;
//       }
  
//       Point3f ev1, ev2, evals;
//       ev1.x() = fields[field_xMax].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid xMax '%2'").arg(linenum).arg(fields[field_xMax]));
//       ev1.y() = fields[field_yMax].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid yMax '%2'").arg(linenum).arg(fields[field_yMax]));
//       ev1.z() = fields[field_zMax].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid zMax '%2'").arg(linenum).arg(fields[field_zMax]));
  
//       ev2.x() = fields[field_xMin].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid xMin '%2'").arg(linenum).arg(fields[field_xMin]));
//       ev2.y() = fields[field_yMin].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid yMin '%2'").arg(linenum).arg(fields[field_yMin]));
//       ev2.z() = fields[field_zMin].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid zMin '%2'").arg(linenum).arg(fields[field_zMin]));
  
//       evals.x() = fields[field_k1].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid k1 '%2'").arg(linenum).arg(fields[field_k1]));
//       evals.y() = fields[field_k2].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid k2 '%2'").arg(linenum).arg(fields[field_k2]));
//       evals.z() = fields[field_k3].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid k3 '%2'").arg(linenum).arg(fields[field_k3]));
  
//       // make sure ev1 and ev2 are unit vectors before storing in cellAxis. It is not the case in the older csv files
//       ev1 = ev1 / norm(ev1);
//       ev2 = ev2 / norm(ev2);
  
//       cellAxis[label] = SymmetricTensor(ev1, ev2, evals);
//     }
  
//     mesh->setCellAxisType(type);

//     // Display cell axis according to their type, with the parameters from the GUI
//     QStringList parms;
//     if(type == "curvature") {
// 			DisplayTissueCurvature *proc = getProcessParms<DisplayTissueCurvature>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "PDG") {
// 			DisplayPDGs *proc = getProcessParms<DisplayPDGs>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
// 			// DisplayPDGs::initialize() changes the display parms in case they commend to draw the PDGs projected 
// 			// on a Bezier and cellAxisBezier doesn't exist yet. 
//   		// ALR: I don't know how to update the GUI with the new parms. 
//       proc->initialize(parms, 0);
//       return proc->run(parms);
//     } else if(type == "fibril") {
//       DisplayFibrilOrientations *proc = getProcessParms<DisplayFibrilOrientations>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "polarization") {
//       QStringList parms;
//       DisplayWallSignalOrientation *proc = getProcessParms<DisplayWallSignalOrientation>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display signal orientation process"));
//       return proc->run(parms);
//     } else if(type == "PCA") {
//       DisplayPCA *proc = getProcessParms<DisplayPCA>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display PCA process"));
//       return proc->run(parms);
// 		}

//     mesh->setShowAxis("Cell Axis");
  
//     setStatus(QString("Loaded directions for %1 cells").arg(cellAxis.size()));
    return true;
  }
  REGISTER_PROCESS(LoadCellAxis);
  
//   bool ClearCellAxis::run(Mesh* mesh)
//   {
//     mesh->clearCellAxis();
//     mesh->updateTriangles();
//     return true;
//   }
//   REGISTER_PROCESS(ClearCellAxis);

//  // Project the cell axis on local coordinates defined by Bezier. NB: we update the cell axis themselves, not cellAxisVis! 
//  bool CustomizeDirections::run(Mesh *mesh, QString projectOnSurface)
//   {           
//     // if there is no cell axis stored, display error message
//     const IntSymTensorAttr& cellAxis = mesh->cellAxis();
//     if(cellAxis.size() == 0){
//       setErrorMessage(QString("No cell axis stored in active mesh!"));
//       return false;
//     }
//     // get the bezier grid
//     CuttingSurface* cutSurf = cuttingSurface();
//     P2iP3dMap bezGridMap, diffBezGridMapX, diffBezGridMapY;
//     double bezStart = 0.0;
//     double bezEnd = 1.0;
//     int dataPointsBezier = 200; // number of intermediate points
//     double stepSize = (double)(bezEnd-bezStart)/(double)(dataPointsBezier-1);
//     // correct directions, take into account the rotations/translations
//     Matrix4d rotMatrixS1, rotMatrixCS;
//     mesh->stack()->getFrame().getMatrix(rotMatrixS1.data());
//     //currentMesh()->stack()->getFrame().getMatrix(rotMatrixS1.data());
//     cutSurf->frame().getMatrix(rotMatrixCS.data());
//     Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);

//     // create bezier map
//     for(int i = 0; i < dataPointsBezier; i++){
//       double u = bezStart + i*stepSize;
//       for(int j = 0; j < dataPointsBezier; j++){
//         double v = bezStart+j*stepSize;
//         Point3d p(cutSurf->evalCoord(v,u)); // get bez world coordinate
//         p = multMatrix4Point3(mGLTot,p); // correct rotations
//         Point2i a(j,i);
//         bezGridMap[a] = p;
//       }
//     }
//     // create derivatives in X and Y direction of the bezier
//     for(int i=1; i<dataPointsBezier-1; i++){
//       for(int j=1; j<dataPointsBezier-1; j++){
//         Point2i a (j,i);
//         Point2i a1 (j-1,i);
//         Point2i a2 (j+1,i);
//         Point2i aY1 (j,i-1);
//         Point2i aY2 (j,i+1);
//         diffBezGridMapX[a] = bezGridMap[a2] - bezGridMap[a1];
//         diffBezGridMapY[a] = bezGridMap[aY2] - bezGridMap[aY1];
//       }
//     }
//     // Update the normals and center of the cell for visualisation.
//     bool useParents = !mesh->labelMap("Parents").empty();
//     if(mesh->labelCenterVis().empty() or (useParents and mesh->parentCenterVis().empty()))
//       mesh->updateCentersNormals();

//     // maps to store cell label -> local bezier vector
//     IntP3dMap cellBezVecMapX, cellBezVecMapY;

//     // cell centers
//     IntPoint3fAttr centers = (useParents? mesh->parentCenterVis():mesh->labelCenterVis());
    
// 		// Transform cellAxis tensor to express it in local coordinates of the Bezier 
// 		IntSymTensorAttr& cellAxisBezier = mesh->attributes().attrMap<int, SymmetricTensor>("Cell Axis Bezier");
// 		IntPoint3fAttr& coordBezier = mesh->attributes().attrMap<int, Point3f>("Cell Coordinates Bezier");
//     // loop over all cells, get cell centers and bezier vectors for each cell
//     forall(const IntSymTensorPair &p, cellAxis) {
//       int cell = p.first; // cell label
//       const SymmetricTensor& tensor = p.second;
//       Point3d cellCenter(centers[cell]);
//       // this code comes from CellNetworks
//       double minDis = 1E20;
//       Point3d bezInterpPoint (0,0,0); 
//       Point3d diffBezInterpPointX (0,0,0); 
//       Point3d diffBezInterpPointY (0,0,0); 
//       // find nearest bez grid point and take its derivatives
//       for(int j=1; j<dataPointsBezier-1; j++){
//         for(int k=1; k<dataPointsBezier-1; k++){
//           Point2i c(k,j);
//           double currentDistance = norm(bezGridMap[c] - cellCenter);
//           if(currentDistance < minDis){
//             minDis = currentDistance;
//             bezInterpPoint = bezGridMap[c];
//             diffBezInterpPointX = diffBezGridMapX[c];
//             diffBezInterpPointY = diffBezGridMapY[c];
// 						coordBezier[cell] = Point3f(c.x(), c.y(), 0);
//           }
//         }

//         // normalize derivative
//         cellBezVecMapX[cell] = diffBezInterpPointX/norm(diffBezInterpPointX);
//         cellBezVecMapY[cell] = diffBezInterpPointY/norm(diffBezInterpPointY);

//         if(projectOnSurface == "Yes"){
//           Point3d normalVectorOfCell, bez, bezPlusDifX, bezPlusDifY;
//           normalVectorOfCell = Point3d(tensor.ev1() % tensor.ev2());//cross product

//           bez = projectPointOnPlane(bezInterpPoint,cellCenter,normalVectorOfCell);
//           bezPlusDifX = projectPointOnPlane(bezInterpPoint + diffBezInterpPointX,cellCenter,normalVectorOfCell) - bez;
//           bezPlusDifY = projectPointOnPlane(bezInterpPoint + diffBezInterpPointY,cellCenter,normalVectorOfCell) - bez;

//           cellBezVecMapX[cell] = bezPlusDifX/norm(bezPlusDifX);
//           cellBezVecMapY[cell] = bezPlusDifY/norm(bezPlusDifY);
//         }
//       }

// 			// Project cell axis on the local bezier coordinates
//       // PDGs =  principal strains = strains expressed in local coordinates for which shear = 0
//       // Given pricipal strains, we can compute normal and shear strains in coordinates defined by bezier
//       // transformation explained here: 
//       // http://www.continuummechanics.org/principalstressesandstrains.html
//       Point3f bezierTensorEvals = Point3f(0,0,0);
//       float cos, sin; 
//       cos = cellBezVecMapX[cell] * Point3d(normalized(tensor.ev1())); 
//       sin = cellBezVecMapX[cell] * Point3d(normalized(tensor.ev2()));
//       float strainMax = tensor.evals()[0] - 1;
//       float strainMin = tensor.evals()[1] - 1;
      
// 			float shear = (strainMin - strainMax) * sin * cos; 
//       float strainBezierX = cos * cos * strainMax + sin * sin * strainMin + 2 * shear * sin * cos;
//       float strainBezierY = sin * sin * strainMax + cos * cos * strainMin + 2 * shear * sin * cos;
//       bezierTensorEvals = Point3f(strainBezierX+1, strainBezierY+1, shear);

// 			SymmetricTensor& tensorBez = cellAxisBezier[cell];
// 			tensorBez.ev1() = Point3f(cellBezVecMapX[cell]);
// 			tensorBez.ev2() = Point3f(cellBezVecMapY[cell]);
// 			tensorBez.evals() = bezierTensorEvals;
//     }

//     // Display cell axis according to their type, with the parameters from the GUI
// 		const QString& type = mesh->cellAxisType();
//     QStringList parms;
//     if(type == "curvature") {
// 			DisplayTissueCurvature *proc = getProcessParms<DisplayTissueCurvature>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "PDG") {
// 			DisplayPDGs *proc = getProcessParms<DisplayPDGs>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
// 			// DisplayPDGs::initialize() changes the display parms in case they commend to draw the PDGs projected 
// 			// on a Bezier and cellAxisBezier doesn't exist yet. 
//   		// ALR: I don't know how to update the GUI with the new parms. 
//       proc->initialize(parms, 0);
//       return proc->run(parms);
//     } else if(type == "fibril") {
//       DisplayFibrilOrientations *proc = getProcessParms<DisplayFibrilOrientations>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "polarization") {
//       QStringList parms;
//       DisplayWallSignalOrientation *proc = getProcessParms<DisplayWallSignalOrientation>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display signal orientation process"));
//       return proc->run(parms);
//     } else if(type == "PCA") {
//       DisplayPCA *proc = getProcessParms<DisplayPCA>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display PCA process"));
//       return proc->run(parms);
// 		}


// 	return true; 
//   }
//   REGISTER_PROCESS(CustomizeDirections);


//  // Project the cell axis on local coordinates defined by Bezier. NB: we update the cell axis themselves, not cellAxisVis! 
//  bool CellAxisAttrMapExport::run(Mesh *mesh, QString prefix, QString name)
//   {           

//   const QString attrMapName = prefix + name;

//   AttrMap<int, SymmetricTensor>& attrData = mesh->attributes().attrMap<int, SymmetricTensor>(attrMapName);

//   forall(const IntSymTensorPair& p, mesh->cellAxis()){
//     attrData[p.first] = p.second;
//   }

//   return true; 
//   }
//   REGISTER_PROCESS(CellAxisAttrMapExport);

//  // Project the cell axis on local coordinates defined by Bezier. NB: we update the cell axis themselves, not cellAxisVis! 
//  bool CellAxisAttrMapImport::run(Mesh *mesh, QString type, QString name)
//   {           

//   AttrMap<int, SymmetricTensor>& attrData = mesh->attributes().attrMap<int, SymmetricTensor>(name);
//   if(attrData.size() == 0) return setErrorMessage("No Attribute Map with this name!");

//   forall(const IntSymTensorPair& p, attrData){
//     mesh->cellAxis()[p.first] = p.second;
//   }


//     // Display cell axis according to their type, with the parameters from the GUI
//     QStringList parms;
//     if(type == "curvature") {
//       DisplayTissueCurvature *proc = getProcessParms<DisplayTissueCurvature>(this, parms);
//       if(!proc)
//         throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "PDG") {
//       DisplayPDGs *proc = getProcessParms<DisplayPDGs>(this, parms);
//       if(!proc)
//         throw(QString("Error, unable to create display tissue curvature process"));
//       // DisplayPDGs::initialize() changes the display parms in case they commend to draw the PDGs projected 
//       // on a Bezier and cellAxisBezier doesn't exist yet. 
//       // ALR: I don't know how to update the GUI with the new parms. 
//       proc->initialize(parms, 0);
//       return proc->run(parms);
//     } else if(type == "fibril") {
//       DisplayFibrilOrientations *proc = getProcessParms<DisplayFibrilOrientations>(this, parms);
//       if(!proc)
//         throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "polarization") {
//       QStringList parms;
//       DisplayWallSignalOrientation *proc = getProcessParms<DisplayWallSignalOrientation>(this, parms);
//       if(!proc)
//         throw(QString("Error, unable to create display signal orientation process"));
//       return proc->run(parms);
//     } else if(type == "PCA") {
//       DisplayPCA *proc = getProcessParms<DisplayPCA>(this, parms);
//       if(!proc)
//         throw(QString("Error, unable to create display PCA process"));
//       return proc->run(parms);
//     }


//   return true; 
//   }
//   REGISTER_PROCESS(CellAxisAttrMapImport);

//  // 
//  bool CellAxisAngle::run(Mesh *m, QString attrMap1, QString attrMap2, QString attrMapOut)
//   {           


//     AttrMap<int, SymmetricTensor>& attrData1 = m->attributes().attrMap<int, SymmetricTensor>(attrMap1);

//     AttrMap<int, SymmetricTensor>& attrData2 = m->attributes().attrMap<int, SymmetricTensor>(attrMap2);

//     if(attrData1.empty()) return setErrorMessage("Attr Map 1 doesn't exist!");
//     if(attrData2.empty()) return setErrorMessage("Attr Map 2 doesn't exist!");

//     AttrMap<int, double>& angleMap = m->attributes().attrMap<int, double>(attrMapOut);
//     angleMap.clear();

//     IntFloatAttr data;

//     forall(IntSymTensorPair p, attrData1){
//       Point3f dir1 = p.second.ev1();
//       Point3f dir2 = attrData2[p.first].ev1();

//       double scalarProd = dir1 * dir2;
//       if(scalarProd < 0) scalarProd*=-1;

//       double vecAngle =  180 * std::acos(scalarProd / norm(dir1) / norm(dir2)) / 3.1415;
//       data[p.first] = vecAngle;
//       angleMap[p.first] = vecAngle;

//     }

//     m->labelHeat() = data;
//     m->showHeat();
//     m->setHeatMapBounds(m->calcHeatMapBounds());
//     updateState();
//     updateViewer();
//     return true; 
//   }
//   REGISTER_PROCESS(CellAxisAngle);


}
