//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MeshProcessSystem.hpp>
#include <StackProcessFilters.hpp>
#include <StackProcessSystem.hpp>
#include <MeshProcessLineage.hpp>
#include <MeshProcessSignal.hpp>

#include <Dir.hpp>
#include <Image.hpp>
#include <MDXViewer/qglviewer.h>
#include <PlyFile.hpp>
#include <CCUtils.hpp>
#include <tbb/concurrent_unordered_set.h>

#include <QCheckBox>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QHash>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QRegExp>
#include <QtConcurrent/QtConcurrent>
#include <future>

#include <ui_ImportMesh.h>
#include <ui_LoadMesh.h>
#include <ui_SaveMesh.h>
#include <ui_ExportMesh.h>
#include <ui_PlyCellGraphDlg.h>
#include <ui_RenameDlg.h>

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
#  define FileDialogOptions 0
#endif

using namespace qglviewer;

#include <QTextStream>
#include <stdio.h>

static QTextStream err(stderr);

namespace mdx 
{
  void MeshImport::selectMeshType(const QString& type)
  {
    QString fileName = ui->MeshFile->text();
    if(fileName.isEmpty())
      return;
    QFileInfo fi(fileName);
    QString suf = fi.suffix();
    if(!suf.isEmpty())
      fileName = fileName.left(fileName.size() - suf.size() - 1);
    if(type == "PLY")
      fileName += ".ply";
    else if(type == "Text" or type == "Cells")
      fileName += ".txt";
    else if(type == "Keyence")
      fileName += ".jpg";
    else if(type == "MeshEdit")
      fileName += ".mesh";
    else if(type == "OBJ")
      fileName += ".obj";
    ui->MeshFile->setText(fileName);
  }
  
  void MeshImport::selectMeshFile()
  {
    QString fileName = ui->MeshFile->text();
    QStringList filters;
    filters << "Stanford Polygon files (*.ply)"
            << "VTK Mesh files (*.vtu)"
            << "Text or cell files (*.txt)"
            << "Keyence files (*.jpg)"
            << "MeshEdit files (*.mesh)"
            << "OBJ files (*.obj)"
            << "All Mesh Files (*.ply *.vtu *.txt *.jpg *.mesh *.obj)";
    QString filter;
    {
      QFileInfo fi(fileName);
      QString suf = fi.suffix();
      if(suf == "vtu")
        filter = filters[0];
      else if(suf == "vtu")
        filter = filters[1];
      else if(suf == "txt")
        filter = filters[2];
      else if(suf == "jpg")
        filter = filters[3];
      else if(suf == "mesh")
        filter = filters[4];
      else if(suf == "obj")
        filter = filters[5];
      else
        filter = filters[6];
    }
    fileName = QFileDialog::getOpenFileName(dlg, QString("Select mesh file"), fileName, filters.join(";;"), &filter,
                                            FileDialogOptions);
    if(!fileName.isEmpty()) {
      if(!QFile::exists(fileName)) {
        QMessageBox::information(dlg, "Error selecting file",
                                 "You must select an existing file. Your selection is ignored.");
        return;
      }
      setMeshFile(fileName);
    }
  }
  
  QString MeshImport::properFile(QString fileName, const QString& type) const
  {
    QFileInfo fi(fileName);
    QString suf = fi.suffix();
    if(!suf.isEmpty())
      fileName = fileName.left(fileName.size() - suf.size() - 1);
    if(type == "PLY")
      fileName += ".ply";
    else if(type == "VTK Mesh")
      fileName += ".vtu";
    else if(type == "Text" or type == "Cells")
      fileName += ".txt";
    else if(type == "MeshEdit")
      fileName += ".mesh";
    else if(type == "Keyence")
      fileName += ".jpg";
    else if(type == "OBJ")
      fileName += ".obj";
    return fileName;
  }
  
  void MeshImport::setMeshFile(const QString& fileName, const QString& type)
  {
    QFileInfo fi(fileName);
    int tid = 0;
    // Note: these ids need to match those in importmesh.ui
    if(type.isEmpty()) {
      QString suf = fi.suffix();
      if(suf == "ply")
        tid = 0;
      else if(suf == "vtu")
        tid = 1;
      else if(suf == "txt")
        tid = 2;
      else if(suf == "jpg")
        tid = 4;
      else if(suf == "mesh")
        tid = 5;
      else if(suf == "obj")
        tid = 6;
      ui->MeshFile->setText(fileName);
    } else {
      if(type == "PLY")
        tid = 0;
      else if(type == "VTK Mesh")
        tid = 1;
      else if(type == "Text")
        tid = 2;
      else if(type == "Cells")
        tid = 3;
      else if(type == "Keyence")
        tid = 4;
      else if(type == "MeshEdit")
        tid = 5;
      else if(type == "OBJ")
        tid = 6;
      ui->MeshFile->setText(properFile(fileName, type));
    }
    ui->MeshType->setCurrentIndex(tid);
  }

  bool MeshImport::initialize(QWidget* parent)
  {
    if(!parent) 
      return true;

    QString fileName = parm("File Name");

    QDialog dlg(parent);
    Ui_ImportMeshDialog ui;
    ui.setupUi(&dlg);
  
    this->ui = &ui;
    this->dlg = &dlg;
  
    QObject::connect(ui.SelectMeshFile, SIGNAL(clicked()), this, SLOT(selectMeshFile()));
    QObject::connect(ui.MeshType, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(selectMeshType(const QString &)));
  
    setMeshFile(properFile(fileName, parm("File Name")), parm("File Name"));

    // For now just hide the scale
    ui.ScaleUm->setVisible(false);

    ui.Transform->setChecked(stringToBool(parm("Transform")));
    ui.Add->setChecked(stringToBool(parm("Add")));
  
    bool res = false;
    if(dlg.exec() == QDialog::Accepted) {
      setParm("File Name", ui.MeshFile->text());
      setParm("Transform", ui.Transform->isChecked() ? "yes" : "no");
      setParm("Add", ui.Add->isChecked() ? "yes" : "no");
      res = true;
    }
    this->ui = 0;
    this->dlg = 0;
    return res;
  }


  bool MeshImport::run()
  {
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("Invalid mesh Id: %1").arg(parm("Mesh Id").toInt()));

    bool result = run(*mesh, parm("Cell Complex"), parm("File Name"),
                      stringToBool(parm("Transform")), stringToBool(parm("Add")));
    if(result)
      ;// set cell complex visible
    // update Normals
 
    return result;
  } 

  bool MeshImport::run(Mesh &mesh, const QString &ccName, const QString &fileName, bool transform, bool add)
  {
    progressStart(QString("Loading cell complex into Mesh %1").arg(mesh.userId()), 0, false);
    actingFile(fileName);
    QFileInfo fi(fileName);
    QString suf = fi.suffix();
    QString type;
    bool success = false;
    QString low_fn = fileName.toLower();
    if(low_fn.endsWith(".ply", Qt::CaseInsensitive))
      type = "PLY";
    else if(low_fn.endsWith(".vtu", Qt::CaseInsensitive))
      type = "VTK Mesh";
    else if(low_fn.endsWith(".txt", Qt::CaseInsensitive))
      type = "Text";
    else if(low_fn.endsWith(".jpg", Qt::CaseInsensitive) or low_fn.endsWith(".jpeg", Qt::CaseInsensitive))
      type = "Keyence";
    else if(low_fn.endsWith(".mesh", Qt::CaseInsensitive))
      type = "MeshEdit";
    else if(low_fn.endsWith(".obj", Qt::CaseInsensitive))
      type = "OBJ";
    else 
      throw(QString("Cannot find the type of the file '%1'").arg(fileName));

    if(type == "PLY")
      success = loadMeshPly(mesh, ccName, fileName, transform, add);
//    else if(type == "VTK Mesh")
//      success = loadMeshVTK(mesh, fileName, scale, transform, add);
//    else if(type == "Text")
//      success = loadMeshText(mesh, fileName, scale, transform, add);
//    else if(type == "Cells")
//      success = loadMeshCells(mesh, fileName, scale, transform, add);
//    else if(type == "Keyence")
//      success = loadMeshKeyence(mesh, fileName, scale, transform, add);
//    else if(type == "MeshEdit")
//      success = loadMeshEdit(mesh, fileName, scale, transform, add);
    else if(type == "OBJ")
      success = loadMeshOBJ(mesh, fileName, transform);
    else
      throw(QString("Unknown file type '%1'").arg(type));

    if(success) {
      mesh.setFile(properFile(fileName, "Mesh"));
      mesh.setScaled(true);
      mesh.setTransformed(transform);
    }
    return success;
  }

  bool MeshImport::loadMeshPly(Mesh &mesh, const QString &ccName, const QString& fileName, bool transform, bool add)
  {
    CCStructure &cs = mesh.ccStructure(ccName);
    CCDrawParms &cdp = mesh.drawParms(ccName);
    if(!add)
      cs = CCStructure(2);
    mesh.ccAttr(ccName, "RenderProcess") = "Mesh/System/Render";
    progressStart(QString("Loading PLY Mesh %1").arg(mesh.userId()), 0);
    mesh.clearImgTex();

    loadPly(mesh, cs, fileName, transform, add);
    setStatus(QString("Loaded cell complex from file: %1, vertices %2, edges %3, faces %4, volumes %5")
      .arg(mesh.file()).arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()).arg(cs.volumes().size()));

    if(cs.maxDimension() == 2)
      cdp.setGroupVisible("Faces", true);
    else {
      cdp.setGroupVisible("Faces", false);
      cdp.setGroupVisible("Volumes", true);
    }
    mesh.updateAll(ccName);

    return true;
  }

  bool loadPly(Mesh &mesh, CCStructure &cs, const QString& fileName, bool transform, bool add) 
  {
    CCIndexDataAttr &indexAttr = mesh.indexAttr();
    CCIndexDoubleAttr &signalAttr = mesh.signalAttr<double>("Signal");

    // Define the reader and read header
    PlyFile ply;
    if(!ply.parseHeader(fileName))
      throw QString("loadPly Cannot parse PLY file header");
  
    // Get vertices and properties
    PlyFile::Element* vertexElement = ply.element("vertex");
    if(!vertexElement)
      throw QString("loadPly No vertex element in PLY file");

    PlyFile::Property *vx = vertexElement->property("x");  
    PlyFile::Property *vy = vertexElement->property("y");
    PlyFile::Property *vz = vertexElement->property("z");
    if(!vx or !vy or !vz)
      throw QString("loadPly x, y or z missing from vertex element in PLY file");
  
    vx->setMemType(PlyFile::FLOAT);
    vy->setMemType(PlyFile::FLOAT);
    vz->setMemType(PlyFile::FLOAT);
  
    PlyFile::Property *vLabel = vertexElement->property("label");  
    if(vLabel)
      vLabel->setMemType(PlyFile::INT);

    // Vertex signal, use color if no signal present
    PlyFile::Property *vSignal = vertexElement->property("signal");
    PlyFile::Property *vRed = vertexElement->property("red");
    PlyFile::Property *vGreen = vertexElement->property("green");
    PlyFile::Property *vBlue = vertexElement->property("blue");
    if(vSignal)
      vSignal->setMemType(PlyFile::FLOAT); 
    else if(vRed and vGreen and vBlue) {
      vRed->setMemType(PlyFile::UCHAR);
      vGreen->setMemType(PlyFile::UCHAR);
      vBlue->setMemType(PlyFile::UCHAR);
    }
  
    // Get edge and properties
    PlyFile::Element *edgeElement = ply.element("edge"); 
    PlyFile::Property *eSource = 0, *eTarget = 0;
    bool hasEdges = false;
    if(edgeElement) {
      eSource = edgeElement->property("source");
      if(!eSource)
        throw QString("loadPly Source property does not exist for edge element");
      eTarget = edgeElement->property("target");
      if(!eTarget)
        throw QString("loadPly Target property does not exist for edge element");
      hasEdges = true;
    }

    // Get faces and properties
    PlyFile::Element* faceElement = ply.element("face"); 
    if(!faceElement)
      throw QString("loadPly No face element in PLY file");
  
    PlyFile::Property *vi = faceElement->property("vertex_index");
    if(!vi) {
      vi = faceElement->property("vertex_indices");
      if(!vi)
        throw QString("loadPly Neither vertex_index or vertex_indices are present in face element");
    }
    vi->setMemType(PlyFile::INT);
  
    PlyFile::Property *fLabel = faceElement->property("label"); 
    if(fLabel)
      fLabel->setMemType(PlyFile::INT);

    // Face signal, use color if no signal present
    // FIXME Color could go into a 3 channel color map
    PlyFile::Property *fSignal = faceElement->property("signal");
    PlyFile::Property *fRed = faceElement->property("red");
    PlyFile::Property *fGreen = faceElement->property("green");
    PlyFile::Property *fBlue = faceElement->property("blue");
    if(fSignal)
      fSignal->setMemType(PlyFile::FLOAT); 
    else if(fRed and fGreen and fBlue) {
      fRed->setMemType(PlyFile::UCHAR);
      fGreen->setMemType(PlyFile::UCHAR);
      fBlue->setMemType(PlyFile::UCHAR);
    }

    // Get volumes and properties
    PlyFile::Element *volumeElement = ply.element("volume");   // volumes
    PlyFile::Property *li = 0, *lLabel = 0;
    PlyFile::Property *lSignal = 0, *lRed = 0, *lGreen = 0, *lBlue = 0;
    bool hasVolumes = false;
    if(volumeElement) {
      li = volumeElement->property("face_index");
      if(!li) {
        li = volumeElement->property("face_indices");
        if(!li)
          throw QString("loadPly Neither face_index or face_indices are present in volume element");
      }
      li->setMemType(PlyFile::INT);
      hasVolumes = true;
  
      lLabel = volumeElement->property("label");   // volume labels
      if(lLabel)
        lLabel->setMemType(PlyFile::INT);

      // Volume signal, use color if no signal present
      // FIXME Color could go into a 3 channel color map
      lSignal = volumeElement->property("signal");
      lRed = volumeElement->property("red");
      lGreen = volumeElement->property("green");
      lBlue = volumeElement->property("blue");
      if(lSignal)
        lSignal->setMemType(PlyFile::FLOAT); 
      else if(lRed and lGreen and lBlue) {
        lRed->setMemType(PlyFile::UCHAR);
        lGreen->setMemType(PlyFile::UCHAR);
        lBlue->setMemType(PlyFile::UCHAR);
      }
    }

    // Parse file
    if(!ply.parseContent())
      throw QString("loadPly Unable to parse contents of PLY file");
  
    // Get vertex data
    CCIndexVec vertices(vertexElement->size());
    const std::vector<float>& px = *vx->value<float>();
    const std::vector<float>& py = *vy->value<float>();
    const std::vector<float>& pz = *vz->value<float>();
  
    IntVec* vLabelp = 0;   // vertex labels
    if(vLabel)
      vLabelp = vLabel->value<int>();
  
    std::vector<float> *vSignalp = 0;   // vertex signal
    std::vector<uchar> *vRedp = 0;   // or vertex colors
    std::vector<uchar> *vGreenp = 0;
    std::vector<uchar> *vBluep = 0;
    if(vSignal) {
      mdxInfo << "loadPly Has vertex signal information" << endl;
      vSignalp = vSignal->value<float>();
    } else if(vRed and vGreen and vBlue) {
      mdxInfo << "loadPly Has vertex RGB information" << endl;
      vRedp = vRed->value<uchar>();
      vGreenp = vGreen->value<uchar>();
      vBluep = vBlue->value<uchar>();
    }
  
    // Process vertices
    progressStart(QString("Loading PLY Mesh %1 vertices").arg(mesh.userId()), 0, false);
    for(uint i = 0; i < vertexElement->size(); ++i) {
      progressAdvance();
      CCIndex v = CCIndexFactory.getIndex();
      CCIndexData &vIdx = indexAttr[v];
      //vIdx.pos = realPos(Point3d(px[i], py[i], pz[i]), true, transform, mesh);
      vIdx.pos = Point3d(px[i], py[i], pz[i]);
      vertices[i] = v;
      if(vLabelp)
        vIdx.label = (*vLabelp)[i];
  
      double signal = 0.5;
      if(vSignalp)
        signal = (*vSignalp)[i];
      else if(vRedp and vGreenp and vBluep) 
        signal = (0.21 * (*vRedp)[i] + 0.71 * (*vGreenp)[i] + 0.08 * (*vBluep)[i]) / 255.0;
      signalAttr[v] = signal;

      if(vIdx.label > mesh.viewLabel())
        mesh.setLabel(vIdx.label);
    }

    // Get edge data
    UIntPairVec edgeVertices;
    CCIndexVec edges;
    std::unordered_map<UIntPair, uint> edgeMap;
    if(eSource and eTarget) {
      if(!eSource->value<int>() or !eTarget->value<int>())
        throw QString("loadPly: Source or target edge pointer empty");

      IntVec &eSources = *eSource->value<int>();
      IntVec &eTargets = *eTarget->value<int>();
      if(eSources.size() != eTargets.size())
        throw QString("loadPly: Source property size (%1) does not equal target (%2)").arg(eSources.size()).arg(eTargets.size());
  
      // Process edges
      edgeVertices.resize(eSources.size());
      edges.resize(eSources.size());
      for(size_t i = 0; i < eSources.size(); ++i) {
        CCIndex e = CCIndexFactory.getIndex();
        edges[i] = e;
        auto ep = std::make_pair(eSources[i], eTargets[i]);
        edgeVertices[i] = ep;
        if(ep.first > ep.second)
          std::swap(ep.first, ep.second);
        edgeMap[ep] = i;
      }
    }

    // Get face data
    const std::vector<std::vector<int32_t> > &faceVertexIds = *vi->list<int32_t>();
  
    IntVec* fLabelp = 0;   // face labels
    if(fLabel)
      fLabelp = fLabel->value<int>();

    std::vector<float> *fSignalp = 0;   // vertex signal
    std::vector<uchar> *fRedp = 0;   // or vertex colors
    std::vector<uchar> *fGreenp = 0;
    std::vector<uchar> *fBluep = 0;
    if(fSignal) {
      mdxInfo << "loadPly Has face signal information" << endl;
      fSignalp = fSignal->value<float>();
    } else if(fRed and fGreen and fBlue) {
        mdxInfo << "loadPly Has face RGB information" << endl;
      fRedp = fRed->value<uchar>();
      fGreenp = fGreen->value<uchar>();
      fBluep = fBlue->value<uchar>();
    }

    // Process faces
    UIntVecVec faceVertices(faceVertexIds.size());
    CCIndexVec faces(faceVertexIds.size());
    UIntVecVec faceEdges;
    if(hasEdges)
      faceEdges.resize(faceVertexIds.size());
    for(size_t i = 0; i < faceVertexIds.size(); ++i) {
      CCIndex f = CCIndexFactory.getIndex();
      CCIndexData &fIdx = indexAttr[f];
      if(fLabelp)
        fIdx.label = (*fLabelp)[i];
      faces[i] = f;
      for(size_t j = 0; j < faceVertexIds[i].size(); ++j) {
        faceVertices[i].push_back(faceVertexIds[i][j]);
        if(hasEdges) {
          int k = (j+1) % faceVertexIds[i].size();
          auto ep = std::make_pair(faceVertexIds[i][j], faceVertexIds[i][k]);
          if(ep.first > ep.second)
            std::swap(ep.first, ep.second);
          faceEdges[i].push_back(edgeMap[ep]); 
        }
      }

      double signal = 0.5;
      if(fSignalp)
        signal = (*fSignalp)[i];
      else if(fRedp and fGreenp and fBluep)
        signal = (0.21 * (*fRedp)[i] + 0.71 * (*fGreenp)[i] + 0.07 * (*fBluep)[i]) / 256.0;
      
      signalAttr[f] = signal;

      if(fIdx.label > mesh.viewLabel())
        mesh.setLabel(fIdx.label);
    }

    // Get volume data
    IntVecVec volumeFaces;
    CCIndexVec volumes;
    if(hasVolumes) {
      const std::vector<std::vector<int32_t> > &volumeFaceIds = *li->list<int32_t>();
    
      IntVec* lLabelp = 0;   // face labels
      if(lLabel)
        lLabelp = lLabel->value<int>();
  
      std::vector<float> *lSignalp = 0;   // vertex signal
      std::vector<uchar> *lRedp = 0;   // or vertex colors
      std::vector<uchar> *lGreenp = 0;
      std::vector<uchar> *lBluep = 0;
      if(lSignal) {
        mdxInfo << "loadPly Has volume signal information" << endl;
        lSignalp = lSignal->value<float>();
      } else if(lRedp and lGreenp and lBluep) {
        mdxInfo << "loadPly Has volume RGB information" << endl;
        lRedp = lRed->value<uchar>();
        lGreenp = lGreen->value<uchar>();
        lBluep = lBlue->value<uchar>();
      }
  
      // Process volumes
      volumeFaces.resize(volumeFaceIds.size());
      volumes.resize(volumeFaceIds.size());
      for(size_t i = 0; i < volumeFaceIds.size(); ++i) {
        CCIndex l = CCIndexFactory.getIndex();
        CCIndexData &lIdx = indexAttr[l];
        if(lLabelp)
          lIdx.label = (*lLabelp)[i];
        volumes[i] = l;
        for(size_t j = 0; j < volumeFaceIds[i].size(); ++j)
          volumeFaces[i].push_back(volumeFaceIds[i][j]);
  
        double signal = 0.5;
        if(lSignalp)
          signal = (*lSignalp)[i];
        else if(lRedp and lGreenp and lBluep)
          signal = (0.21 * (*lRedp)[i] + 0.71 * (*lGreenp)[i] + 0.07 * (*lBluep)[i]) / 256.0;
        
        signalAttr[l] = signal;
  
        if(lIdx.label > mesh.viewLabel())
          mesh.setLabel(lIdx.label);
      }
    }

    // Create cell complex
    if(hasVolumes) {
      if(cs.maxDimension() == 2 and volumes.size() > 0 and cs.cellCount() > 0)
        throw QString("loadPly: Trying to load a 3D ply file into a 2D cell complex");
      cs = CCStructure(3);

      // Add the vertices, edges and faces
      if(hasEdges) {
        // Create faceEdges
        if(!ccFromFacesAndVolumes(cs, vertices, edges, edgeVertices, faces, faceVertices, faceEdges, volumes, volumeFaces))
          mdxInfo << "loadPly ccFromFacesAndVolumes returned an error" << endl;

      } else if(!ccFromFacesAndVolumes(cs, vertices, faces, faceVertices, volumes, volumeFaces))
        mdxInfo << "loadPly ccFromFaces returned an error" << endl;

    } else if(!ccFromFaces(cs, vertices, faces, faceVertices)) // For 2D we'll skip the edges
      mdxInfo << "loadPly ccFromFaces returned an error" << endl;

    mesh.setSignalBounds(calcBounds(signalAttr), "Signal");

    faceAttrFromVertices(cs, signalAttr, cs.faces());

    return true;
  }
  
  static Point3d realPos(Point3d pos, bool scale, bool transform, const Mesh &mesh)
  {
    if(!scale)
      pos = mesh.stack()->abstractToWorld(pos);
    if(transform)
      pos = Point3d(mesh.stack()->frame().coordinatesOf(Vec(pos)));
    return pos;
  }
  
  bool MeshImport::loadMeshText(Mesh &mesh, const QString& fileName, bool scale, bool transform, bool add)
  {
//    QFile file(fileName);
//    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
//      setErrorMessage(QString("loadText::Error:Cannot open input file: %1").arg(fileName));
//      return false;
//    }
//  
//    vvGraph& S = mesh->graph();
//  
//    if(!add)
//      mesh->reset();
//  
//    IntVtxMap vMap;
//  
//    QTextStream in(&file);
//    int vCnt;
//    in >> vCnt;
//    progressStart(QString("Loading Mesh %1").arg(mesh->userId()), vCnt * 2);
//    S.reserve(vCnt);
//    for(int i = 0; i < vCnt; i++) {
//      if(in.atEnd())
//        throw QString("Premature end of file reading vertices");
//      vertex v;
//      S.insert(v);
//      in >> v->saveId >> v->pos >> v->label;
//      v->pos = realPos(v->pos, scale, transform, mesh);
//      vMap[v->saveId] = v;
//      if(v->label > mesh->viewLabel())
//        mesh->setLabel(v->label);
//      if(!progressAdvance(i))
//        userCancel();
//    }
//    for(int i = 0; i < vCnt; i++) {
//      if(in.atEnd())
//        throw QString("Premature end of file reading neighborhoods");
//      uint vId, nCnt;
//      in >> vId >> nCnt;
//      IntVtxMap::const_iterator vIt = vMap.find(vId);
//      if(vIt == vMap.end())
//        throw QString("Invalid vertex id: %1").arg(vId);
//      vertex v = vIt->second;
//      vertex pn(0);
//      for(uint j = 0; j < nCnt; j++) {
//        uint nId;
//        in >> nId;
//        IntVtxMap::const_iterator nIt = vMap.find(nId);
//        if(nIt == vMap.end())
//          throw QString("Invalid vertex id: %1").arg(nId);
//        vertex n = nIt->second;
//        if(S.valence(v) == 0)
//          S.insertEdge(v, n);
//        else
//          S.spliceAfter(v, pn, n);
//        pn = n;
//      }
//      if(!progressAdvance(vCnt + i))
//        userCancel();
//    }
//    if(!progressAdvance(2 * vCnt))
//      userCancel();
//    file.close();
//    mesh->clearImgTex();
//    setNormals(mesh->graph());
//    mesh->setMeshType("MDXM");
//
//    setStatus(QString("Loaded mesh, file: %1").arg(mesh->file()));
    return true;
  }
  
  void showPLYHeader(PlyFile& ply)
  {
    mdxInfo << "PLY file content";
    mdxInfo << "format = " << PlyFile::formatNames[ply.format()] << endl;
    mdxInfo << "version = " << ply.version() << endl;
    mdxInfo << "content position = " << ply.contentPosition() << endl;
    mdxInfo << endl;
    mdxInfo << "# elements = " << ply.nbElements() << endl;
    for(size_t i = 0; i < ply.nbElements(); ++i) {
      const PlyFile::Element& element = *ply.element(i);
      mdxInfo << "Element '" << element.name() << "' - " << element.size()
                       << " # properties = " << element.nbProperties() << endl;
      for(size_t j = 0; j < element.nbProperties(); ++j) {
        const PlyFile::Property& prop = *element.property(j);
        mdxInfo << "Property '" << prop.name() << "'";
        if(prop.kind() == PlyFile::Property::LIST)
          mdxInfo << " list " << PlyFile::typeNames[prop.sizeType()][0];
        mdxInfo << " " << PlyFile::typeNames[prop.fileType()][0] << " / "
                         << PlyFile::typeNames[prop.memType()][0] << endl;
      }
    }
  }
  
  bool MeshImport::loadMeshCells(Mesh &mesh, const QString& fileName, bool scale, bool transform, bool add)
  {
//    QFile file(fileName);
//    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
//      setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(fileName));
//      return false;
//    }
//  
//    vvGraph& S = mesh->graph();
//  
//    if(!add)
//      mesh->reset();
//  
//    IntVtxMap vMap;
//  
//    QTextStream in(&file);
//    int vCnt;
//    in >> vCnt;
//    progressStart(QString("Loading Cellular Mesh %1").arg(mesh->userId()), vCnt * 2);
//    S.reserve(vCnt);
//    for(int i = 0; i < vCnt; i++) {
//      if(in.atEnd())
//        throw QString("Premature end of file reading vertices");
//      vertex v;
//      S.insert(v);
//      char tchar;
//      in >> v->saveId >> v->pos >> v->label >> tchar >> v->type;
//      v->pos = realPos(v->pos, scale, transform, mesh);
//      vMap[v->saveId] = v;
//      if(v->label > mesh->viewLabel())
//        mesh->setLabel(v->label);
//      if(!progressAdvance(i))
//        userCancel();
//    }
//    for(int i = 0; i < vCnt; i++) {
//      if(in.atEnd())
//        throw QString("Premature end of file reading neighborhoods");
//      uint vId, nCnt;
//      in >> vId >> nCnt;
//      IntVtxMap::const_iterator vIt = vMap.find(vId);
//      if(vIt == vMap.end())
//        throw QString("Invalid vertex id: %1").arg(vId);
//      vertex v = vIt->second;
//      vertex pn(0);
//      for(uint j = 0; j < nCnt; j++) {
//        uint nId;
//        in >> nId;
//        IntVtxMap::const_iterator nIt = vMap.find(nId);
//        if(nIt == vMap.end())
//          throw QString("Invalid vertex id: %1").arg(nId);
//        vertex n = nIt->second;
//        if(S.valence(v) == 0)
//          S.insertEdge(v, n);
//        else
//          S.spliceAfter(v, pn, n);
//        pn = n;
//      }
//      if(!progressAdvance(vCnt + i))
//        userCancel();
//    }
//    if(!progressAdvance(2 * vCnt))
//      userCancel();
//    file.close();
//    mesh->setMeshType("MDXC");
//    mesh->clearImgTex();
//    setNormals(mesh->graph());
//    SETSTATUS("Loaded cell mesh, file:" << mesh->file() << ", vertices:" << S.size());
    return true;
  }
  
  bool MeshImport::loadMeshKeyence(Mesh &mesh, const QString& fileName, bool scale, bool transform, bool /*add*/)
  {
//    QFile file(fileName);
//    if(!file.open(QIODevice::ReadOnly)) {
//      setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(fileName));
//      return false;
//    }
//  
//    vvGraph& S = mesh->graph();
//  
//    mesh->reset();
//  
//    // First read in the entire file
//    QByteArray buff = file.readAll();
//    if(buff.size() != file.size())
//      throw QString("Could not read the whole file");
//    file.close();
//    // Grab the image size
//    int xsize = *(short*)(buff.data() + 47);
//    int ysize = *(short*)(buff.data() + 49);
//    // Height map uses every 4th pixel
//    int xsz = xsize / 4;
//    int ysz = ysize / 4;
//    progressStart(QString("Loading Height Map %1").arg(mesh->userId()), ysz);
//  
//    // Find second image file
//    int imgpos = buff.indexOf(QByteArray("JFIF"), 16);
//    if(imgpos < 0)
//      throw QString("Can't find color image");
//    imgpos -= 6;
//
//    QByteArray imgba(buff.data() + imgpos, buff.size() - imgpos - xsz * ysz * 4);
//    QImage image;
//    image.loadFromData(imgba);
//   
//    if(xsize != image.width() or ysize != image.height())
//      throw QString("Invalid image size");
//    mesh->setImgTex(image);
//  
//    // Find max/min z values to center image in z
//    float maxz = -1e10, minz = 1e10;
//    float* htp = (float*)(buff.data() + buff.size() - xsz * ysz * 4);
//    S.reserve(xsz * ysz);
//    for(int i = 0; i < xsz * ysz; i++) {
//      float z = *htp++;
//      if(z > maxz)
//        maxz = z;
//      if(z < minz)
//        minz = z;
//    }
//    float zshift = (minz + maxz) / 2.0;
//  
//    // Save the vertex ids for connecting
//    // vertex vtx[xsz][ysz];
//    std::vector<std::vector<vertex> > vtx(xsz, std::vector<vertex>(ysz, vertex(0)));
//  
//    // Create the vertices and fill in height and texture coords
//    VtxPoint2fAttr& txPos = mesh->texCoord2d();
//    htp = (float*)(buff.data() + buff.size() - xsz * ysz * 4);
//    for(int y = ysz - 1; y >= 0; y--) {
//      for(int x = xsz - 1; x >= 0; x--) {
//        float z = *htp++;
//        vertex v;
//        S.insert(v);
//        vtx[x][y] = v;
//        v->pos.x() = (double(x) / double(xsz) - .5) * 4 / 3;
//        v->pos.y() = double(y) / double(ysz) - .5;
//        v->pos.z() = (z - zshift) * .03;
//        v->pos = realPos(v->pos, scale, transform, mesh);
//        txPos[v] = Point2f(float(x * 4) / float(xsize), float(y * 4) / float(ysize));
//      }
//    }
//  
//    // Connect neighborhoods
//    for(int y = 0; y < ysz; y++) {
//      if(!progressAdvance(y + 1))
//        throw(QString("Keyence load cancelled"));
//      for(int x = 0; x < xsz; x++) {
//        std::vector<vertex> nhbs;
//        nhbs.push_back(vtx[x][y]);
//        if(x == 0 and y == ysz - 1) {       // top left corner;
//          nhbs.push_back(vtx[x][y - 1]);
//          nhbs.push_back(vtx[x + 1][y - 1]);
//          nhbs.push_back(vtx[x + 1][y]);
//        } else if(x == 0 and y == 0) {       // bottom left corner
//          nhbs.push_back(vtx[x + 1][y]);
//          nhbs.push_back(vtx[x][y + 1]);
//        } else if(x == xsz - 1 and y == 0) {       // bottom right corner
//          nhbs.push_back(vtx[x][y + 1]);
//          nhbs.push_back(vtx[x - 1][y + 1]);
//          nhbs.push_back(vtx[x - 1][y]);
//        } else if(x == xsz - 1 and y == ysz - 1) {       // top right corner
//          nhbs.push_back(vtx[x - 1][y]);
//          nhbs.push_back(vtx[x][y - 1]);
//        } else if(y == ysz - 1) {       // top edge
//          nhbs.push_back(vtx[x - 1][y]);
//          nhbs.push_back(vtx[x][y - 1]);
//          nhbs.push_back(vtx[x + 1][y - 1]);
//          nhbs.push_back(vtx[x + 1][y]);
//        } else if(x == 0) {       // left edge
//          nhbs.push_back(vtx[x][y - 1]);
//          nhbs.push_back(vtx[x + 1][y - 1]);
//          nhbs.push_back(vtx[x + 1][y]);
//          nhbs.push_back(vtx[x][y + 1]);
//        } else if(y == 0) {       // bottom edge
//          nhbs.push_back(vtx[x + 1][y]);
//          nhbs.push_back(vtx[x][y + 1]);
//          nhbs.push_back(vtx[x - 1][y + 1]);
//          nhbs.push_back(vtx[x - 1][y]);
//        } else if(x == xsz - 1) {       // right edge
//          nhbs.push_back(vtx[x][y - 1]);
//          nhbs.push_back(vtx[x - 1][y + 1]);
//          nhbs.push_back(vtx[x - 1][y]);
//          nhbs.push_back(vtx[x][y + 1]);
//        } else {       // Interior vertex
//          nhbs.push_back(vtx[x + 1][y - 1]);
//          nhbs.push_back(vtx[x + 1][y]);
//          nhbs.push_back(vtx[x][y + 1]);
//          nhbs.push_back(vtx[x - 1][y + 1]);
//          nhbs.push_back(vtx[x - 1][y]);
//          nhbs.push_back(vtx[x][y - 1]);
//        }
//        vertex v = nhbs[0];
//        vertex pn(0);
//        for(uint i = 1; i < nhbs.size(); i++) {
//          vertex n = nhbs[i];
//          if(i == 1)
//            S.insertEdge(v, n);
//          else
//            S.spliceAfter(v, pn, n);
//          pn = n;
//        }
//      }
//    }
//    mesh->setMeshType("MDXM");
//    setNormals(mesh->graph());
//    SETSTATUS("Loaded Keyence mesh, file:" << mesh->file() << ", vertices:" << S.size());
    return true;
  }
  
  bool MeshImport::loadMeshOBJ(Mesh &mesh, const QString& fileName, bool transform)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
      setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(fileName));
      return false;
    }
  
    std::vector<Point3f> points;
    std::vector<float> signal;
    IntVec labels;
    IntDoubleAttr heatAttr;
    std::vector<Point3u> triangles;
    float minSignal = FLT_MAX, maxSignal = -FLT_MAX;
    float minHeat = FLT_MAX, maxHeat = -FLT_MAX;
  
    QTextStream ts(&file);
    int line_count = 0;
    bool fixedBound = false;
  
    mdxInfo << "Reading the file" << endl;
    QString signalDescription;
    QString heatDescription;
  
    while(!ts.atEnd()) {
      ++line_count;
      QString line = ts.readLine().trimmed();
      int idx = line.indexOf('#');
      if(idx != -1)
        line = line.left(idx).trimmed();
      if(!line.isEmpty()) {
        QStringList fields = line.split(QRegExp("[ \t]"), QString::SkipEmptyParts);
        if(fields[0] == "v") {
          if(fields.size() < 4)
            throw QString("Error reading file '%1' on line %2: vertex must have at least 3 values (x y z [w] ...), got %3")
              .arg(fileName).arg(line_count).arg(fields.size() - 1);

          Point3f pos;
          bool ok;
          pos.x() = fields[1].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex x: '%3'")
                            .arg(fileName)
                            .arg(line_count)
                            .arg(fields[1]));
            return false;
          }
          pos.y() = fields[2].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex y: '%3'")
                            .arg(fileName)
                            .arg(line_count)
                            .arg(fields[2]));
            return false;
          }
          pos.z() = fields[3].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex z: '%3'")
                            .arg(fileName)
                            .arg(line_count)
                            .arg(fields[3]));
            return false;
          }
          points.push_back(pos);
  
          // If there is a 5th field, this is w, so we need to normalize the position with this
          if(fields.size() == 5) {
            float w = fields[4].toFloat(&ok);
            if(!ok) {
              setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex w: '%3'")
                              .arg(fileName)
                              .arg(line_count)
                              .arg(fields[4]));
              return false;
            }
            if(w != 0)
              pos /= w;
            else {
              setErrorMessage(
                QString("Error reading file '%1' on line %2: MorphoDynamX cannot interpret a vertex w of 0")
                .arg(fileName)
                .arg(line_count));
              return false;
            }
          }
        } else if(fields[0] == "f") {
          IntVec poly(fields.size() - 1);
          bool ok;
          int psz = points.size();
          for(int i = 0; i < fields.size() - 1; ++i) {
            poly[i] = fields[i + 1].toInt(&ok) - 1;
            if(!ok or poly[i] < 0 or poly[i] >= psz) {
              setErrorMessage(
                QString("Error reading file '%1' on line %2: the first vertex id is not valid: '%3'")
                .arg(fileName)
                .arg(line_count)
                .arg(fields[i + 1]));
              return false;
            }
          }
          // If a triangle, just add it
          if(poly.size() == 3)
            triangles.push_back(Point3u(poly[0], poly[1], poly[2]));
          else {         // Otherwise make a triangle fan
                         // Find center
            Point3f c(0, 0, 0);
            for(size_t i = 0; i < poly.size(); ++i)
              c += points[poly[i]];
            c /= poly.size();
            points.push_back(c);
  
            // Add tris
            for(size_t i = 0; i < poly.size(); ++i)
              if(i == poly.size() - 1)
                triangles.push_back(Point3u(psz, poly[i], poly[0]));
              else
                triangles.push_back(Point3u(psz, poly[i], poly[i + 1]));
          }
        } else if(fields[0] == "label") {
          if(fields.size() != 2) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the label field must have 1 value, got %3")
              .arg(fileName)
              .arg(line_count)
              .arg(fields.size() - 1));
            return false;
          }
          int lab;
          bool ok;
          lab = fields[1].toInt(&ok);
          if(!ok) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the vertex label has invalid value: '%3'")
              .arg(fileName)
              .arg(line_count)
              .arg(fields[1]));
            return false;
          }
          labels.push_back(lab);
        } else if(fields[0] == "vv") {
          if(fields.size() != 2) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the vv field must have 1 value, got %3")
              .arg(fileName)
              .arg(line_count)
              .arg(fields.size() - 1));
            return false;
          }
          float value;
          bool ok;
          value = fields[1].toFloat(&ok);
          if(!ok) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the vertex value has invalid value: '%3'")
              .arg(fileName)
              .arg(line_count)
              .arg(fields[1]));
            return false;
          }
          signal.push_back(value);
          if(!fixedBound) {
            if(value > maxSignal)
              maxSignal = value;
            if(value < minSignal)
              minSignal = value;
          }
        } else if(fields[0] == "vv_desc") {
          fields.pop_front();
          signalDescription = fields.join(" ");
        } else if(fields[0] == "vv_range") {
          if(fields.size() != 3) {
            return setErrorMessage(
              QString("Error reading file '%1' on line %2: the vv_range needs 2 values, not %3")
              .arg(fileName)
              .arg(line_count)
              .arg(fields.size() - 1));
          }
          float vmin, vmax;
          bool ok;
          vmin = fields[1].toFloat(&ok);
          if(!ok) {
            return setErrorMessage(
              QString("Error reading file '%1' on line %2: min value for vv_range is not a value float: %3")
              .arg(fileName)
              .arg(line_count)
              .arg(fields[1]));
          }
          vmax = fields[2].toFloat(&ok);
          if(!ok) {
            return setErrorMessage(
              QString("Error reading file '%1' on line %2: max value for vv_range is not a value float: %3")
              .arg(fileName)
              .arg(line_count)
              .arg(fields[2]));
          }
          minSignal = vmin;
          maxSignal = vmax;
          mdxInfo << "vv_range = " << minSignal << " - " << maxSignal << endl;
          fixedBound = true;
        } else if(fields[0] == "heat") {
          if(fields.size() != 3) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the vv field must have 2 value, got %3")
              .arg(fileName)
              .arg(line_count)
              .arg(fields.size() - 1));
            return false;
          }
          int lab;
          float value;
          bool ok;
          lab = fields[1].toInt(&ok);
          if(!ok) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the heat label has invalid value: '%3'")
              .arg(fileName)
              .arg(line_count)
              .arg(fields[1]));
            return false;
          }
          value = fields[2].toFloat(&ok);
          if(!ok) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the heat value has invalid value: '%3'")
              .arg(fileName)
              .arg(line_count)
              .arg(fields[2]));
            return false;
          }
          heatAttr[lab] = value;
          if(value < minHeat)
            minHeat = value;
          if(value > maxHeat)
            maxHeat = value;
        } else if(fields[0] == "heat_desc") {
          fields.pop_front();
          heatDescription = fields.join(" ");
        }
        // Ignore anything else
      }
    }
  
    if(!signal.empty() and signal.size() != points.size()) {
      setErrorMessage(
        QString(
          "Error reading file '%1', signal array size mismatch. Current file has %2 vs fields and %3 vertices")
        .arg(fileName)
        .arg(signal.size())
        .arg(points.size()));
      return false;
    }
    if(!labels.empty() and labels.size() != points.size()) {
      setErrorMessage(QString("Error reading file '%1', labels array size mismatch. Current file has %2 labels "
                              "fields and %3 vertices") .arg(fileName).arg(labels.size()).arg(points.size()));
      return false;
    }
  
    mdxInfo << "Extracted " << points.size() << " points and " << triangles.size() << " triangles" << endl;
  
    QString ccName("OBJ Import");
    CCStructure &cs = mesh.ccStructure(ccName);
  
    if(signal.empty() and not fixedBound) {
      minSignal = 0.0;
      maxSignal = 1.0;
    } else if(maxSignal == minSignal)
      minSignal -= 1.0f;

    // First add vertices
    CCIndexDataAttr &indexAttr = mesh.indexAttr();
    CCIndexDoubleAttr &signalAttr = mesh.signalAttr<double>("Signal");
    CCIndexVec vertices;
    for(size_t i = 0; i < points.size(); ++i) {
      CCIndex v = CCIndexFactory.getIndex();
      vertices.push_back(v);
      CCIndexData &vIdx = indexAttr[v];
      vIdx.pos = realPos(Point3d(points[i]), false, transform, mesh);
      if(!signal.empty())
        signalAttr[v] = signal[i];

      if(!labels.empty())
        vIdx.label = labels[i];
    }
  
    if(!ccFromTriangles(cs, vertices, triangles))
      mdxInfo << "MeshImport::loadMeshOBJ Problem inserting triangles" << endl;
  
    std::cout << "MinSignal:" << minSignal << ", MaxSignal:" << maxSignal << std::endl;
  
    if(!heatAttr.empty()) {
      if(minHeat >= maxHeat)
        maxHeat += 1;
      mesh.setHeatBounds(Point2d(minHeat, maxHeat), "Heat");
      mesh.heatAttr<double>("Heat") = heatAttr;
    }
    if(!heatDescription.isEmpty())
      mesh.setHeatUnit(heatDescription, "Heat");
  
    mesh.setSignalBounds(Point2d(minSignal, maxSignal), "Signal");
    mesh.setSignalUnit(signalDescription, "Signal");
    mesh.updateAll(ccName);
  
    return true;
  }
  
  bool MeshImport::loadMeshEdit(Mesh &mesh, const QString& fileName, bool scale, bool transform, bool add)
  {
//    QFile file(fileName);
//    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
//      setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(fileName));
//      return false;
//    }
//  
//    vvGraph& S = mesh->graph();
//  
//    if(!add)
//      mesh->reset();
//    else
//      forall(const vertex& v, S)
//        v->saveId = -1;
//  
//    std::vector<vertex> vertices;
//    std::vector<Point3u> triangles;
//  
//    QTextStream in(&file);
//    QString tmp;
//    tmp = in.readLine();
//    tmp = in.readLine();
//    tmp = in.readLine();
//    int vCnt;
//    in >> vCnt;
//    progressStart(QString("Loading Triangle Mesh %1").arg(mesh->userId()), vCnt);
//    int prog = 1;
//    uint saveId = 0;
//    S.reserve(vCnt);
//    for(int i = 0; i < vCnt; i++) {
//      if(in.atEnd())
//        throw(QString("Premature end of file reading vertices"));
//      vertex v;
//      in >> v->pos >> v->label;
//      v->pos = realPos(v->pos, scale, transform, mesh);
//      v->saveId = saveId++;
//      vertices.push_back(v);
//      if(v->label > mesh->viewLabel())
//        mesh->setLabel(v->label);
//      if(!progressAdvance(prog++))
//        userCancel();
//    }
//    tmp = in.readLine();
//    tmp = in.readLine();
//    int tCnt, t;
//  
//    in >> tCnt;
//    progressStart(QString("Loading Triangle Mesh %1").arg(mesh->userId()), tCnt);
//    for(int i = 0; i < tCnt; i++) {
//      if(in.atEnd())
//        throw QString("Premature end of file reading triangles");
//      Point3u tri;
//      in >> tri.x() >> tri.y() >> tri.z() >> t;
//      tri -= Point3u(1, 1, 1);
//      triangles.push_back(tri);
//      if(!progressAdvance(prog++))
//        userCancel();
//    }
//    file.close();
//  
//    if(!Mesh::meshFromTriangles(S, vertices, triangles, false)) {
//      setErrorMessage("Error, cannot add all the triangles");
//      return false;
//    }
//    mesh->setMeshType("MDXM");
//    mesh->clearImgTex();
//    setNormals(mesh->graph());
//    setStatus(QString("Loaded mesh, file: %1").arg(mesh->file()));
    return true;
  }
  REGISTER_PROCESS(MeshImport);
  
  bool MeshLoad::initialize(QWidget* parent)
  {
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("MeshLoad::initialize Invalid mesh Id: %1").arg(parm("Mesh Id").toInt()));

    QString fileName = mesh->file();
    if(fileName.isEmpty())
      fileName = parm("File Name");

    if(!parent) 
      return true;  

    QDialog dlg(parent);
    Ui_LoadMeshDialog ui;
    ui.setupUi(&dlg);
  
    this->ui = &ui;
    this->dlg = &dlg;
  
    QObject::connect(ui.SelectMeshFile, SIGNAL(clicked()), this, SLOT(selectMeshFile()));
  
    setMeshFile(fileName);
    ui.Transform->setChecked(stringToBool(parm("Transform")));
    ui.Add->setChecked(stringToBool(parm("Add")));
  
    bool res = false;
    if(dlg.exec() == QDialog::Accepted) {
      setParm("File Name", ui.MeshFile->text());
      setParm("Transform", ui.Transform->isChecked() ? "yes" : "no");
      setParm("Add", ui.Add->isChecked() ? "yes" : "no");
      res = true;
    }
    this->ui = 0;
    this->dlg = 0;
    return res;
  }
  
  void MeshLoad::selectMeshFile()
  {
    QString fileName = ui->MeshFile->text();
    fileName = QFileDialog::getOpenFileName(dlg, QString("Select mesh file"), fileName,
                        "Mesh files (*.mdxm);;MorphoGraphX Mesh files (*.mgxm);;All files (*.*)", 0, FileDialogOptions);
    if(!fileName.isEmpty()) {
      if(!QFile::exists(fileName)) {
        QMessageBox::information(dlg, "Error selecting file",
                                 "You must select an existing file. Your selection is ignored.");
        return;
      }
      setMeshFile(fileName);
    }
  }
  
  void MeshLoad::setMeshFile(const QString& fileName) {
    ui->MeshFile->setText(fileName);
  }
  
  bool MeshLoad::run()
  {
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("MeshLoad::initialize Invalid mesh Id: %1").arg(parm("Mesh Id").toInt()));

    bool transform = stringToBool(parm("Transform"));
    bool add = stringToBool(parm("Add"));
    bool res = run(*mesh, parm("File Name"), transform, add);
    return res;
  }

  bool MeshLoad::loadMGXM_1_3(QIODevice& file, Mesh &mesh, bool& scale, bool& transform)
  {
    return loadMGXM_1_2(file, mesh, scale, transform, false);
  }
  
  bool MeshLoad::loadMGXM_1_2(QIODevice& file, Mesh &mesh, bool& scale, bool& transform, bool has_color)
  {
    bool isCells;
    file.read((char*)&isCells, sizeof(bool));
    return loadMGXM_1_1(file, mesh, scale, transform, has_color);
  }
  
  bool MeshLoad::loadMGXM_1_1(QIODevice& file, Mesh &mesh, bool& scale, bool& transform, bool has_color)
  {
    file.read((char*)&scale, sizeof(bool));
    file.read((char*)&transform, sizeof(bool));
    Point2f signalBounds;
    file.read((char*)&signalBounds[0], sizeof(float));
    file.read((char*)&signalBounds[1], sizeof(float));
    if(signalBounds[0] == signalBounds[1]) {
      signalBounds[0] = 0;
      signalBounds[1] = 65535;
    }
    mesh.setSignalBounds(Point2d(signalBounds), "Signal");
    uint stringSize;
    file.read((char*)&stringSize, sizeof(uint));
    QByteArray string(stringSize, 0);
    file.read(string.data(), stringSize);
    mesh.setSignalUnit(QString::fromUtf8(string));
    bool res = loadMGXM_0(file, mesh, scale, transform, has_color);
    if(res and has_color) {
      float deltaSignal = signalBounds[1] - signalBounds[0];
      CCIndexDoubleAttr &signalAttr = mesh.signalAttr<double>("Signal");
      for(CCIndex v : mesh.ccStructure("MeshMGXM").vertices()) {
        double &signal = signalAttr[v];
        signal = signalBounds[0] + signal * deltaSignal;
      }
    }
    return res;
  }
  
  bool MeshLoad::loadMGXM_1_0(QIODevice& file, Mesh &mesh, bool& scale, bool& transform, bool )
  {
    file.read((char*)&scale, sizeof(bool));
    file.read((char*)&transform, sizeof(bool));
    return loadMGXM_0(file, mesh, scale, transform);
  }
  
  bool MeshLoad::loadMGXM_0(QIODevice& file, Mesh &mesh, bool scale, bool transform, bool has_color)
  {
    uint hSz, vCnt, vSz, eSz;
    file.read((char*)&hSz, 4);
    file.read((char*)&vCnt, 4);
    file.read((char*)&vSz, 4);
    file.read((char*)&eSz, 4);
    bool has_label = vSz >= 4;
    if(has_label)
      vSz -= 4;
    if(has_color) {
      has_color = vSz >= 4;
      if(has_color)
        vSz -= 4;
    }
    bool has_signal = vSz >= 4;
    if(has_signal)
      vSz -= 4;
    bool has_type = vSz >= 1;
    if(has_type)
      vSz -= 1;
    bool has_selected = vSz >= 1;
    if(has_selected)
      vSz -= 1;
  
    if(hSz > 0) {
      std::vector<char> t(hSz);
      file.read(&t[0], hSz);
    }
  
    progressStart(QString("Reading Mesh %1").arg(mesh.userId()), 0);
    CCIndexVec vertices(vCnt);
    CCIndexDataAttr &indexAttr = mesh.indexAttr();
    CCIndexDoubleAttr &signalAttr = mesh.signalAttr<double>("Signal");
    for(uint i = 0; i < vCnt; i++) {
      if(file.atEnd())
        throw(QString("Premature end of file reading vertices"));
      CCIndex v = CCIndexFactory.getIndex();
      vertices[i] = v;
      auto &vIdx = indexAttr[v];

      int saveId;
      file.read((char*)&saveId, 4);
      Point3f pos;
      file.read((char*)&pos, 12);
      vIdx.pos = realPos(Point3d(pos), scale, transform, mesh);
      if(has_label) {
        int label;
        file.read((char*)&label, 4);
        vIdx.label = label;
      }
      if(has_color or has_signal) {
        float signal;
        file.read((char*)&signal, 4);
        signalAttr[v] = signal;
      } else
        signalAttr[v] = 0.5;
      if(has_color and has_signal) {
        float tmp;
        file.read((char*)&tmp, 4);
      }
      if(has_type) {
        char type;
        file.read((char*)&type, 1);
      }
      if(has_selected) {
        bool selected;
        file.read((char*)&selected, 1);
        vIdx.selected = selected;
      }
      if(vSz > 0) {
        std::vector<char> t(vSz);
        file.read(&t[0], vSz);
      }
      if(vIdx.label > mesh.viewLabel())
        mesh.setLabel(vIdx.label);
      if(!progressAdvance())
        userCancel();
    }
    UIntVecVec neighbors(vCnt);
    for(uint i = 0; i < vCnt; i++) {
      if(file.atEnd())
        throw(QString("Premature end of file reading neighborhoods"));
      uint vId, nCnt;
      file.read((char*)&vId, 4);
      file.read((char*)&nCnt, 4);
      if(eSz > 0) {
        std::vector<char> t(eSz);
        file.read(&t[0], eSz);
      }
      neighbors[i].resize(nCnt);
      for(uint j = 0; j < nCnt; j++) {
        uint nId;
        file.read((char*)&nId, 4);
        neighbors[i][j] = nId;
      }
      if(!progressAdvance())
        userCancel();
    }
    CCIndexVec faces;
    UIntVecVec faceVertices;
    progressStart(QString("Converting to cell complex"), 0);
    makeTriangles(vertices, neighbors, faces, faceVertices, indexAttr);
    CCStructure &cs = mesh.ccStructure("MeshMGXM");
    cs = CCStructure(2);
    ccFromFaces(cs, vertices, faces, faceVertices);
    return true;
  }

  // MDX meshes and some MGX ones
  bool MeshLoad::loadMDXM_2_x(QIODevice& file, Mesh &mesh, bool& transform)
  {
    // Read the header
    char magic[5];
    file.read(magic, 5);
    if(strncmp(magic, "MDXM", 4) and strncmp(magic, "MGXM", 4))
      throw(QString("MeshLoad: Error, bad header"));

    // Get the revision numbers, these will change when the structure changes
    QList<int> version = extractVersion(file);
    int major = version[0];
    int minor = version[1];
    if(major < 2)
      throw(QString("MeshLoad: Error, bad major version number: %1").arg(major));
  
    // Start progress bar, we'll allow cancel so to catch errors and clean up
    progressStart(QString("Reading Mesh %1").arg(mesh.userId()), 0, true);
    mdxInfo << QString("Loading Mesh %1, version %2.%3")
                                       .arg(mesh.userId()).arg(major).arg(minor) << endl;
    try {
      // Read in the mesh type
      QString mType;
      readFile(file, mType);

      // Save the data from the mesh, needs to go in after attributes and CCs are read
      CCIndexDataAttr indexAttr;
      CCIndexDoubleAttr signalAttr;
      CCIndexVec vertices, faces, volumes;
      UIntVecVec faceVertices;
      IntVecVec volumeFaces;
      bool foundVVMesh = readCellMesh(file, mesh, vertices, faces, faceVertices, volumes, volumeFaces, indexAttr, signalAttr, transform);

      bool doCC = major > 2 || (major == 2 and minor >= 1); // cell complexes only in v2.1 and up
      if(doCC)
        doCC = mesh.readCCStructures(file);
 
      mesh.attributes().read(file);

      // Update the parents
      IntIntAttr &parents = mesh.attributes().attrMap<int, int>("Cell Parent");
      if(parents.size() > 0) {
        mesh.labelMap("Parents") = parents;
        mesh.attributes().erase("Cell Parent");
      }

      // Don't load the VV mesh if there is a cell complex, the indices will be overlapping
      if(foundVVMesh and not doCC) {
        // Write index data
        mesh.indexAttr() = indexAttr;
        mesh.signalAttr<double>("Signal") = signalAttr;

        // Create cell complex, 2 or 3D
        CCStructure &cs = mesh.ccStructure("MeshMGXM");
        cs = volumes.size() > 0 ? CCStructure(3) : CCStructure(2);

        // Create the cell complex from the vertices and faces
        ccFromFaces(cs, vertices, faces, faceVertices);

        // If there are volumes, add them
        for(uint i = 0; i < volumes.size(); i++) {
          BoundaryChain bc;
          for(int f : volumeFaces[i]) {
            if(f > 0)
              bc += +faces[f - 1]; // Index for faces is not 0 based, needs to be + or -
            else if(f < 0)
              bc += -faces[-f - 1];
            else
              throw(QString("MeshLoad::loadMDXM_2_x Bad face index (0) in volume"));
         }

          cs.addCell(volumes[i], bc);
        }
        mdxInfo << QString("Read cell complex: vertices %1, edges %2, faces %3, volumes %4")
          .arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()).arg(cs.volumes().size()) << endl;
  
        mesh.updateAll("MeshMGXM");
        mesh.drawParms("MeshMGXM").setGroupVisible("Faces", true);
      }
      mesh.updateAll();

      // Read any attributes that are not live
      if(mesh.hasImgTex()) {
        QByteArray ba =
          mesh.attributes().attrMap<QString, QByteArray>("MeshQByteArray")["ImageTex"];
        if(ba.size() == 0)
          mesh.clearImgTex();
        else {
          QImage image;
          image.loadFromData(ba);
          mdxInfo << QString("Loaded image texture size %1 x %2.")
                                                .arg(image.width()).arg(image.height()) << endl;
          if(image.width() * image.height() > 0)
            mesh.setImgTex(image);
          else
            mesh.clearImgTex();
        } 
      }

    } catch(const QString &s) {
      mesh.reset();
      throw(s);
    } catch(UserCancelException &e) { 
      mesh.reset();
      throw(e);
    } catch(...) {
      mesh.reset();
      throw(QString("Unknown exception loading mesh"));
    }
    return true;
  }

  // Returns the face from the face map or creating it if not there.
  int getFace(AttrMap<Point3u, int> &faceMap, uint v1, uint v2, uint v3, CCIndexVec &faces, UIntVecVec &faceVertices)
  {
    // Sort vertices for key, v1 is known to be lowest
    Point3u key(v1, v2, v3);
    if(v2 > v3)
      key = Point3u(v1, v3, v2);

    // Find the face and create it if it doesn't exist
    int f;
    if(faceMap.count(key) > 0)
      f = -faceMap[key]; // If already there, assume negative orientation, note index starts at 1 not 0
    else {
      f = faces.size() + 1;
      faceMap[key] = f;
      faces.push_back(CCIndexFactory.getIndex());
      faceVertices.emplace_back(UIntVec({v1, v2, v3}));
    }
    return f;
  }
            
  bool MeshLoad::readCellMesh(QIODevice &file, Mesh &mesh, CCIndexVec &vertices, CCIndexVec &faces, UIntVecVec &faceVertices, 
                     CCIndexVec &volumes, IntVecVec &volumeFaces, CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, bool transform)
  {
    // Read in the vertices
    readVertices(file, mesh, indexAttr, signalAttr, vertices, transform);
    UIntVecVec neighbors;
    readNhbds(file, neighbors);
    if(neighbors.size() != vertices.size())
      throw(QString("Neighbor list size %1 does not match vertice list size %2")
                                        .arg(neighbors.size()).arg(vertices.size()));

    // Skip the junction graph, it was never used
    int cInt;
    readFile(file, cInt);
    size_t cSizet;
    readFile(file, cSizet);

    // Read in the volumes
    int lCnt;
    readFile(file, lCnt);

    // If no cells, convert a "normal" VV mesh
    if(lCnt <= 0)
      makeTriangles(vertices, neighbors, faces, faceVertices, indexAttr);

    // Face map
    AttrMap<Point3u, int> faceMap;

    volumes.resize(lCnt);
    volumeFaces.resize(lCnt);
    for(int l = 0; l < lCnt; l++) {
      if(file.atEnd())
        throw(QString("Premature end of file reading cells"));

      volumes[l] = CCIndexFactory.getIndex();
      CCIndexData &lIdx = indexAttr[volumes[l]];

      // CellData
      Point3d pos;
      readFile(file, lIdx.pos);
      if(transform)       // Transform the point if required
        lIdx.pos = Point3d(mesh.stack()->frame().coordinatesOf(qglviewer::Vec(lIdx.pos)));
      // Skip area
      double area;
      readFile(file, area);
      // Read in the volume
      readFile(file, lIdx.measure);

      // Skip associated vertex Id (for now, needed for cell graph)
      int dualId = 0;
      readFile(file, dualId);

      // Read the label, and bump next label if required
      readFile(file, lIdx.label);
      if(lIdx.label > mesh.viewLabel())
        mesh.setLabel(lIdx.label);

      // Skip color and saveId
      Colorb color;
      readFile(file, color);
      int saveId;
      readFile(file, saveId); 
      // Should be the same as lCnt?
      if(l != saveId)
        mdxInfo << "SaveId does not match lCnt" << endl;

      // Read in the indices for the vertex list and neighborhoods
      UIntVec vIds;
      readFile(file, vIds);

      UIntVecVec nIds;
      readNhbds(file, nIds); //, c->S);

      // Create the faces
      for(uint i = 0; i < vIds.size(); i++) {
        uint vId = vIds[i];
        for(uint j = 0; j < nIds[i].size(); j++) {
          uint nId = nIds[i][j];
          uint mId = nIds[i][j + 1 == nIds[i].size() ? 0 : j+1];
          if(vId >= nId or vId >= mId or mId == nId)
            continue;

          // Create the edges if they don't already exist
          int f = getFace(faceMap, vId, nId, mId, faces, faceVertices);
          volumeFaces[l].push_back(f);
        }
      }
  
      if(!progressAdvance())
        throw(UserCancelException());
    }

    // Read the neighborhoods of the cell graph
    // FIXME Currently no cell graph is created, is there a use case to read this as well?
    for(int i = 0; i < lCnt; i++) {
      if(file.atEnd())
        throw(QString("Premature end of file reading cell neighborhoods"));
  
      int lId, nCnt;
      readFile(file, lId);
      readFile(file, nCnt);
        
//      IntCellMap::const_iterator cIt = _cMap.find(cId);
//      if(cIt == _cMap.end())
//        throw QString("Invalid cell id: %1").arg(cId);
//      cell c = cIt->second;
//      cell pn(0);
      for(int j = 0; j < nCnt; j++) {
        int nId;
        readFile(file, nId);
//        IntCellMap::const_iterator nIt = _cMap.find(nId);
//        if(cIt == _cMap.end())
//          throw QString("Invalid cell id: %1").arg(nId);
//        cell n = nIt->second;
//
//        if(C.valence(c) == 0)
//          C.insertEdge(c, n);
//        else
//          C.spliceAfter(c, pn, n);
//        pn = n;

        // WallData
//        readFile(file, C.edge(c, n)->length);
//        readFile(file, C.edge(c, n)->area);
//        readFile(file, C.edge(c, n)->color);
        double length; /// the length of the cell wall
        double area;   /// are of the cell interface
        Colorb color;  /// Wall edge colo
        readFile(file, length);
        readFile(file, area);
        readFile(file, color);
      }
      if(!progressAdvance())
        throw(UserCancelException());
    }

    return vertices.size() > 0;
  }

  // Get the faces for a VV mesh, this tosses dangling edges
  bool MeshLoad::makeTriangles(const CCIndexVec &vertices, const UIntVecVec &neighbors, 
                             CCIndexVec &faces, UIntVecVec &faceVertices, CCIndexDataAttr &indexAttr)
  {
    // Make edge list. This is needed to determine if a face exists ie. m,n is an edge.
    std::unordered_map<std::pair<uint, uint>, uint> edgeMap;
    uint eCount = 0;
    uint vSize = neighbors.size();
    for(uint v = 0; v < vSize; v++) {
      auto nbs = neighbors[v];
      for(uint i = 0; i < nbs.size(); i++) {
        auto e = v < nbs[i] ? std::make_pair(v, nbs[i]) : std::make_pair(nbs[i], v);
        auto it = edgeMap.find(e);
        if(it == edgeMap.end())
          edgeMap[e] = eCount++;
      }
    }
    // Make triangle (face) list
    for(uint v = 0; v < vSize; v++) {
      auto nbs = neighbors[v];
      if(nbs.size() <= 1)
        continue;
      for(uint i = 0; i < nbs.size(); i++) {
        uint n = nbs[i];
        uint m = nbs[(i + 1) % nbs.size()];
        // Only make triangle once
        if(v <= n or v <= m or n == m)
          continue;
        auto e = n < m ? std::make_pair(n, m) : std::make_pair(m, n); 
        // Check if e1 exists
        auto it = edgeMap.find(e);
        if(it == edgeMap.end())
          continue;
        // Now create the triangle
        faceVertices.emplace_back(UIntVec({v, n, m}));
        CCIndex f = CCIndexFactory.getIndex();
        faces.emplace_back(f);
        auto &vIdx = indexAttr[vertices[v]];
        auto &nIdx = indexAttr[vertices[n]];
        auto &mIdx = indexAttr[vertices[m]];
        auto &fIdx = indexAttr[f];
        // Set the face label
        if(vIdx.label > 0)
          fIdx.label = vIdx.label;
        else if(nIdx.label > 0)
          fIdx.label = nIdx.label;
        else if(mIdx.label > 0)
          fIdx.label = mIdx.label;
      }
    }
    return true;
  }

  // Read vertices into a vector
  bool MeshLoad::readVertices(QIODevice &file, Mesh &mesh, CCIndexDataAttr &indexAttr, 
                                CCIndexDoubleAttr &signalAttr, CCIndexVec &vertices, bool transform)
  {
    progressStart(QString("Loading Mesh %1 Vertices").arg(mesh.userId()), 0);

    // Get the size
    int vCnt;
    readFile(file, vCnt);

    // Resive vector
    uint vPrevSize = vertices.size();
    vertices.resize(vPrevSize + vCnt);

    // Read in the vertices
    for(uint i = vPrevSize; i < vPrevSize + vCnt; ++i) {
      CCIndex v = CCIndexFactory.getIndex();
      vertices[i] = v;
      
      auto &vIdx = indexAttr[v];

      // VertexData
      Point3d pos;
      readFile(file, pos);
      if(transform)       // Transform the point if required
        vIdx.pos = Point3d(mesh.stack()->frame().coordinatesOf(qglviewer::Vec(pos)));
      else
        vIdx.pos = pos;
      int cId = 0;
      readFile(file, cId); //v->cId = cId; // id in cellgraph
      float signal;
      readFile(file, signal);
      signalAttr[v] = signal;
      int label;
      readFile(file, label);
      vIdx.label = label;
      Colorb col; // Ignore for now
      readFile(file, col);
      int saveId;
      readFile(file, saveId);
      if(saveId != int(i))
        mdxInfo << "SaveId " << saveId << " doesn't match index " << i << endl;
      char type;
      readFile(file, type);
      bool selected;
      readFile(file, selected);
      vIdx.selected = selected;

      if(vIdx.label > mesh.viewLabel())
        mesh.setLabel(vIdx.label);

      if(!progressAdvance())
        throw(QString("Mesh load canceled"));
    }
    return true;
  }

  bool MeshLoad::readNhbds(QIODevice &file, UIntVecVec &neighbors) 
  {
    int vCnt;
    readFile(file, vCnt);
    neighbors.resize(vCnt);

    for(int v = 0; v < vCnt; v++) {
      if(file.atEnd())
        throw(QString("Premature end of file reading neighborhoods at position %1").arg(v));

      int vId, nCnt;
      readFile(file, vId);
      readFile(file, nCnt);
      //if(vId != v)
        //throw(QString("Vertex id %1 does not match vertex order %2").arg(vId).arg(v));
 
      neighbors[v].resize(nCnt);
      
      for(int j = 0; j < nCnt; j++) {
        int nId;
        readFile(file, nId);
        neighbors[v][j] = nId;
        //if(nId > vCnt)
          //throw(QString("nId %1 > vCnt %2").arg(nId).arg(vCnt));

        // Skip the color
        Colorb col;
        readFile(file, col);
      }
      if(!progressAdvance())
        throw(QString("Mesh load canceled"));
    }
    return true;
  }

  bool MeshLoad::readVertexList(QIODevice &file)
  {
    // Read in the  vertex list
    IntVec vIds;
    readFile(file, vIds);

    // Now convert saveIds to vertices and insert, they should be unique
//    for(uint i = 0; i < vIds.size(); ++i) {
//      IntVtxMap::const_iterator vIt = _vMap.find(vIds[i]);
//      if(vIt == _vMap.end())
//        throw QString("Invalid vertex id in cell list: %1").arg(vIds[i]);
//      S.insert(vIt->second, false);
//    }
    return true;
  }

  bool MeshLoad::readNhbds(QIODevice &file) 
  {
    int vCnt;
    readFile(file, vCnt);

    for(int i = 0; i < vCnt; i++) {
      if(file.atEnd())
        throw(QString("Premature end of file reading neighborhoods"));

      int vId, nCnt;
      readFile(file, vId);
      readFile(file, nCnt);
      
//      IntVtxMap::const_iterator vIt = _vMap.find(vId);
//      if(vIt == _vMap.end())
//        throw QString("Invalid vertex id: %1").arg(vId);
//      vertex v = vIt->second;
//      vertex pn;
      for(int j = 0; j < nCnt; j++) {
        int nId;
        readFile(file, nId);
//        IntVtxMap::const_iterator nIt = _vMap.find(nId);
//        if(vIt == _vMap.end())
//          throw QString("Invalid vertex id: %1").arg(nId);
//        vertex n = nIt->second;
//
//        if(S.valence(v) == 0)
//          S.insertEdge(v, n);
//        else
//          S.spliceAfter(v, pn, n);
//
        // Read in EdgeData
        //readFile(file, S.edge(v, n)->color);
        Colorb color;
        readFile(file, color);

//        pn = n;
      }
      if(!progressAdvance())
        throw(QString("Mesh load canceled"));
    }
    return true;
  } 

  bool MeshLoad::run(Mesh &mesh, QString fileName, bool transform, bool add)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)) {
      setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(fileName));
      return false;
    }
    char magic[8];
    file.read(magic, 8);

    bool scale;

    // If we are not adding to it, reset the mesh
    // RSS FIXME This is now broken, how to fix? Maybe as a different process?
    //if(!add)
      mesh.reset();
  
    QList<int> version;
    int major = 0, minor = 0;
    if(strncmp(magic, "MGX MESH", 8) == 0) {
      scale = false;
      setStatus(QString("Found mesh version 0.1"));
      if(!loadMGXM_0(file, mesh, scale, transform))
        return false;
    } else {
      file.seek(0);
      file.read(magic, 5);
      if(strncmp(magic, "MDXM", 4) == 0 or strncmp(magic, "MGXM", 4) == 0) {
        version = extractVersion(file);
        if(version.size() != 2) {
          QStringList str;
          foreach(int i, version)
          str << QString::number(i);
          throw QString("MeshLoad::Error:Invalid file version format (%1), expected: MAJOR.MINOR")
                .arg(str.join("."));
        }
        bool success = false;
        major = version[0];
        minor = version[1];
        setStatus(QString("Found mesh version %1.%2").arg(major).arg(minor));
        if(major == 1 and minor == 0) {
          success = loadMGXM_1_0(file, mesh, scale, transform);
        } else if(major == 1 and minor == 1) {
          success = loadMGXM_1_1(file, mesh, scale, transform);
        } else if(major == 1 and minor == 2) {
          success = loadMGXM_1_2(file, mesh, scale, transform);
        } else if(major == 1 and minor == 3) {
          success = loadMGXM_1_3(file, mesh, scale, transform);
        } else if(major == 2 and (minor == 0 or minor == 1)) {
          file.seek(0);
          success = loadMDXM_2_x(file, mesh, transform);
        } else if(major == 2 and minor > 1) {
          file.seek(0);
          success = mesh.read(file, transform);
        } else {
           throw QString( "MeshLoad::Error:Unknown file version %1.%2, please upgrade "
             "to a newer version of MorphGraphX").arg(version[0]).arg(version[1]);
        }
        if(!success)
          return false;


      } else {
        setErrorMessage(QString("The file '%1' is not a mdx mesh file").arg(fileName));
        return false;
      }
    }
    file.close();
    actingFile(fileName);
    mesh.setFile(fileName);

    if(!Attributes::versionAtLeast(2, 1)) {
      //mesh.clearImgTex();
      CCStructure &cs = mesh.ccStructure("MeshMGXM");
      CCIndexDoubleAttr &signalAttr = mesh.signalAttr<double>("Signal");
      faceAttrFromVertices(cs, signalAttr, cs.faces());
      if(mesh.signalBounds("Signal").x() == mesh.signalBounds("Signal").y())
        mesh.setSignalBounds(calcBounds(signalAttr), "Signal");
      mesh.updateAll("MeshMGXM");
      mesh.drawParms("MeshMGXM").setGroupVisible("Faces", true);
      mesh.signalColorMap("Signal").makeRangeMap();
      mesh.signalColorMap("Signal").setColors("Greyscale");
    }
    if(mesh.stack()->empty())
      mesh.setScaled(scale);
    else
      mesh.setScaled(true);
    mesh.setTransformed(transform);
    setStatus(QString("Loaded mesh, file: %1").arg(mesh.file()));
    return true;
  }
  REGISTER_PROCESS(MeshLoad);

  bool ResetMesh::run()
  {
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("ResetMesh::run Invalid mesh Id: %1").arg(parm("Mesh Id").toInt()));

    mesh->reset();
    return true;
  }
  REGISTER_PROCESS(ResetMesh);

  // Rewind will set the delete process for the current tissue
  bool DeleteSelection::rewind(QWidget *parent)
  {
    // If no mesh, exit quietly
    Mesh *mesh = currentMesh();
    if(!mesh) 
      return true;
    QString ccName = mesh->ccName();
    if(ccName.isEmpty()) 
      return true;

    mesh->ccAttr(ccName, "DeleteProcess") = name();

    return true;
  }

  bool DeleteSelection::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh) 
      throw("No mesh selected");

    QString ccName = mesh->ccName();
    if(ccName.isEmpty()) 
      throw("No mesh selected");

    QString dimension = parm("Dimension");

    CCStructure &cs = mesh->ccStructure(ccName); 
    CCIndexDataAttr &indexAttr = mesh->indexAttr();
  
    mesh->updateAll(ccName);

    return run(cs, indexAttr, dimension);
  };

  bool DeleteSelection::run(CCStructure &cs, CCIndexDataAttr &indexAttr, const QString dimension)
  {
    if(cs.maxDimension() > 3 or cs.maxDimension() < 2)
      throw QString("DeleteSelection::run 3D cell complex with dimension %1").arg(cs.maxDimension());

    CCIndexTbbSet delCells;
    const CCIndexVec &edges = cs.edges();
    const CCIndexVec &vertices = cs.vertices();
    const CCIndexVec &faces = cs.faces();
    const CCIndexVec &volumes = cs.volumes();

    // Find what to delete
    bool deleteV = dimension == "Vertices" or dimension == "All";
    bool deleteE = dimension == "Edges" or dimension == "All";
    bool deleteF = dimension == "Faces" or dimension == "All";
    bool deleteL = dimension == "Volumes" or dimension == "All";

    // Find all the vertices to delete
    if(deleteV) {
      #pragma omp parallel for
      for(uint i = 0; i < vertices.size(); i++) {
        CCIndex v = vertices[i];
        if(indexAttr[v].selected)
          delCells.insert(v);
      }
    }

    // Find all the edges to delete
    if(deleteV or deleteE) {
      #pragma omp parallel for
      for(uint i = 0; i < edges.size(); i++) {
        CCIndex e = edges[i];
        auto eb = cs.edgeBounds(e);
        // Include edges when either vertex is deleted or if both vertices selected
        if(delCells.count(eb.first) > 0 or delCells.count(eb.second) > 0)
          delCells.insert(e);
        else if(deleteE and indexAttr[eb.first].selected and indexAttr[eb.second].selected)
          delCells.insert(e);
      }
    }

    // Find all the faces to delete
    if(deleteF or deleteV or deleteE) {
      #pragma omp parallel for
      for(uint i = 0; i < faces.size(); i++) {
        CCIndex f = faces[i];
        // Include selected faces if required
        if(deleteF and indexAttr[f].selected) {
          delCells.insert(f);
          continue;
        }
        // Include faces when any edge is deleted
        for(CCIndex e : cs.bounds(f)) {
          if(delCells.count(e) > 0) {
            delCells.insert(f);
            break;
          }
        }
      }
    }

    // Find all the volumes
    if(cs.maxDimension() == 3) {
      #pragma omp parallel for
      for(uint i = 0; i < volumes.size(); i++) {
        CCIndex l = volumes[i];
        // Include selected volumes if required
        if(deleteL and indexAttr[l].selected) {
          delCells.insert(l);
          continue;
        }
        // Include volumes when any face is deleted
        for(CCIndex f : cs.bounds(l)) {
          if(delCells.count(f) > 0) {
            delCells.insert(l);
            break;
          }
        }
      }
    }

    // Leave if no cells selected for deletion
    if(delCells.size() == 0)
      return true;

    // Add infty
    delCells.insert(CCIndex::INFTY);

    // Delete dangling cells if asked to
    if(stringToBool(parm("Delete Dangling Cells")))
    {
      // Delete the dangling faces
      if(cs.maxDimension() > 2) {
        #pragma omp parallel for
        for(uint i = 0; i < faces.size(); i++) {
          CCIndex f = faces[i];
          bool keep = false;
          for(CCIndex l : cs.cobounds(f))
            if(delCells.count(l) == 0) {
              keep = true;
              break;
            }
          if(!keep)
            delCells.insert(f);
        }
      }

      // Delete the dangling edges
      #pragma omp parallel for
      for(uint i = 0; i < edges.size(); i++) {
        CCIndex e = edges[i];
        bool keep = false;
        for(CCIndex f : cs.cobounds(e))
          if(delCells.count(f) == 0) {
            keep = true;
            break;
          }
        if(!keep)
          delCells.insert(e);
      }

      // Delete the dangling vertices
      #pragma omp parallel for
      for(uint i = 0; i < vertices.size(); i++) {
        CCIndex v = vertices[i];
        bool keep = false;
        for(CCIndex e : cs.cobounds(v))
          if(delCells.count(e) == 0) {
            keep = true;
            break;
          }
        if(!keep)
          delCells.insert(v);
      }
    }

    // Move delCells into a CCIndexSet.
    CCIndexSet delCellsSet(delCells.begin(), delCells.end());

    return deleteCells(cs, delCellsSet, indexAttr);
  }
  REGISTER_PROCESS(DeleteSelection);

  bool CCDelete::run(Mesh &mesh, const QString &ccName)
  {
    mesh.erase(ccName);
    return true;
  }
  REGISTER_PROCESS(CCDelete);

  bool CCRename::initialize(QWidget *parent)
  {
    mesh = currentMesh();
    if(!mesh) 
      throw(QString("No mesh selected"));

    oldName = parm("Old Name");
    if(oldName.isEmpty())
      oldName = mesh->ccName();
    if(oldName.isEmpty()) 
      throw(QString("No cell complex selected"));
    newName = parm("New Name");
    if(newName.isEmpty())  {
      if(!parent)
        throw(QString("New name empty"));
      QDialog dlg(parent);
      Ui_RenameDlg ui;
      ui.setupUi(&dlg);
      if(dlg.exec() == QDialog::Accepted)
        newName = ui.newName->text();
      if(newName.isEmpty())
        throw(QString("New name empty"));
      this->ui = 0;
      this->dlg = 0;
    }
    return true;
  }

  bool CCRename::run(Mesh &mesh, const QString &oldName, const QString &newName)
  {
    mesh.ccStructure(newName) = mesh.ccStructure(oldName);
    mesh.drawParms(newName) = mesh.drawParms(oldName);
    mesh.erase(oldName);
    return true;
  }
  REGISTER_PROCESS(CCRename);

  QString MeshExport::properFile(QString fileName, const QString& type) const
  {
    QFileInfo fi(fileName);
    QString suf = fi.suffix();
    if(!suf.isEmpty())
      fileName = fileName.left(fileName.size() - suf.size() - 1);
    if(type == "Text" or type == "Cells")
      fileName += ".txt";
    else if(type == "MeshEdit")
      fileName += ".mesh";
    else if(type == "STL")
      fileName += ".stl";
    else if(type.startsWith("VTK Mesh"))
      fileName += ".vtu";
    else if(type == "OBJ")
      fileName += ".obj";
    else if(type.startsWith("PLY"))
      fileName += ".ply";
    return fileName;
  }
  
  bool MeshExport::initialize(QWidget* parent)
  {
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("MeshExport::initialize Invalid mesh Id: %1").arg(parm("Mesh Id").toInt()));

    QDialog dlg(parent);
    Ui_ExportMeshDialog ui;
    ui.setupUi(&dlg);

    this->ui = &ui;
    this->dlg = &dlg;

    connect(ui.SelectMeshFile, SIGNAL(clicked()), this, SLOT(selectMeshFile()));
    connect(ui.MeshType, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(selectMeshType(const QString &)));

    QString fileName = mesh->file();
    if(fileName.isEmpty())
      fileName = parm("File Name");

    setMeshFile(fileName);
    ui.Transform->setChecked(stringToBool(parm("Transform")));

    bool res = false;
    if(dlg.exec() == QDialog::Accepted) {
      setParm("File Name", ui.MeshFile->text());
      setParm("File Type", ui.MeshType->currentText());
      setParm("Transform", boolToString(ui.Transform->isChecked()));
      res = true;
    }
    this->ui = 0;
    this->dlg = 0;
    return res;
  }
  
  void MeshExport::selectMeshType(const QString& type)
  {
    QString fileName = ui->MeshFile->text();
    if(fileName.isEmpty())
      return;
    ui->MeshFile->setText(properFile(fileName, type));
  }
  
  void MeshExport::selectMeshFile()
  {
    QString fileName = ui->MeshFile->text();
    QStringList filters;
    filters << "Stanford Polygon file (*.ply)"
            << "VTK Mesh File (*.vtu)"
            << "Text or cell files (*.txt)"
            << "MeshEdit files (*.mesh)"
            << "STL CAD files (*.stl)"
            << "Wavefront Object files (*.obj)"
            << "All Mesh Files (*.ply *.vtu *.txt *.mesh *.stl *.obj)";
    QString filter;
    {
      QFileInfo fi(fileName);
      QString suf = fi.suffix();
      if(suf == "ply")
        filter = filters[0];
      else if(suf == "vtu")
        filter = filters[1];
      else if(suf == "txt")
        filter = filters[2];
      else if(suf == "mesh")
        filter = filters[3];
      else if(suf == "stl")
        filter = filters[4];
      else if(suf == "obj")
        filter = filters[5];
      else
        filter = filters[6];
    }
    fileName = QFileDialog::getSaveFileName(dlg, QString("Select mesh file"), fileName, filters.join(";;"), &filter,
                                            FileDialogOptions);
    if(!fileName.isEmpty())
      setMeshFile(fileName);
  }
  
  void MeshExport::setMeshFile(const QString& fileName)
  {
    QFileInfo fi(fileName);
    QString suf = fi.suffix();
    // This is nasty, if you add/change anything here you'll have to change the exportmesh.ui file
    if(suf == "ply")
      ui->MeshType->setCurrentIndex(0);
    else if(suf == "vtu")
      ui->MeshType->setCurrentIndex(2);
    else if(suf == "txt")
      ui->MeshType->setCurrentIndex(4);
    else if(suf == "mesh")
      ui->MeshType->setCurrentIndex(6);
    else if(suf == "stl")
      ui->MeshType->setCurrentIndex(7);
    else if(suf == "obj")
      ui->MeshType->setCurrentIndex(8);
    else {
      ui->MeshType->setCurrentIndex(0);
      ui->MeshFile->setText(properFile(fileName, "PLY Binary"));
      return;
    }
    ui->MeshFile->setText(fileName);
  }
  
  bool MeshExport::run()
  {
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("MeshExport::run Invalid mesh Id: %1").arg(parm("Mesh Id").toInt()));

    QString fileType = parm("File Type");
    if(fileType != "PLY Binary" and fileType != "PLY Ascii" and fileType != "VTK Mesh Binary"
       and fileType != "VTK Mesh Ascii" and fileType != "Text" and fileType != "Cells" and fileType != "MeshEdit"
       and fileType != "STL" and fileType != "OBJ") {
      setErrorMessage("Error, type must be one of 'PLY Binary', 'PLY Ascii',"
                      " 'VTK Mesh Binary', 'VTK Mesh Ascii', 'Text', 'Cells', 'MeshEdit', 'STL', 'OBJ'.");
      return false;
    }
    return run(mesh, parm("File Name"), fileType, stringToBool(parm("Transform")));
  }
  
  bool MeshExport::run(Mesh* mesh, const QString& fileName, const QString& type, bool transform)
  {
    if(fileName.isEmpty())
      throw QString("%1 Filename is empty").arg(name());

    QString ccName = mesh->ccName();
    if(ccName.isEmpty())
      throw QString("%1 No current cell complex").arg(name());
    const auto &cs = mesh->ccStructure(ccName);

    actingFile(fileName);
    progressStart(QString("Saving Mesh %1").arg(mesh->userId()), 0, false);
    QFileInfo fi(fileName);
    QString suf = fi.suffix();
    bool success = false;
    if(type == "PLY Binary")
      success = savePLY(*mesh, cs, fileName, transform, true);
    else if(type == "PLY Ascii")
      success = savePLY(*mesh, cs, fileName, transform, false);
    if(type == "VTK Mesh Binary")
      success = saveVTU(mesh, fileName, transform, true);
    else if(type == "VTK Mesh Ascii")
      success = saveVTU(mesh, fileName, transform, false);
    else if(type == "Text")
      success = saveText(mesh, fileName, transform);
    else if(type == "Cells")
      success = saveCells(mesh, fileName, transform);
    else if(type == "MeshEdit")
      success = saveMeshEdit(mesh, fileName, transform);
    else if(type == "STL")
      success = saveMeshSTL(mesh, fileName, transform);
    else if(type == "OBJ")
      success = saveOBJ(mesh, fileName, transform);
    if(success) 
      mesh->setTransformed(transform);

    return success;
  }
  
  Point3d MeshExport::savedPos(Point3d pos, bool transform, const Stack* stack)
  {
    if(transform)
      pos = Point3d(stack->getFrame().inverseCoordinatesOf(Vec(pos)));
    return pos;
  }

  bool MeshSaveExtendedPly::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh) throw(QString("No current mesh"));
    return run(mesh, parm("File Name"));
  }
  bool MeshSaveExtendedPly::run(Mesh* mesh, const QString& fileName)
  {
    
//    bool transform = false;
//    bool binary = false;
//
//    bool success = savePLY(mesh, fileName, transform, binary);
//
    return true;
  }
  REGISTER_PROCESS(MeshSaveExtendedPly);


//  static QString vtkBinaryEncoding(const char* data, int size)
//  {
//    QByteArray ba = (QByteArray::fromRawData((const char*)&size, 4)
//                     + QByteArray::fromRawData(data, size)).toBase64();
//    return QString::fromLocal8Bit(ba);
//  }
  
  template <typename T>
  static QDomText vtkBinaryEncoding(const std::vector<T>& data, QDomDocument& doc)
  {
    QDomText text = doc.createTextNode(vtkBinaryEncoding((const char*)&data[0], data.size()
                                       * sizeof(T)));
    return text;
  }
  
  QString vtkAsciiEncoding(const int& i)
  {
    return QString::number(i);
  }
  
  QString vtkAsciiEncoding(const float& f)
  {
    return QString::number(f, 'g', 10);
  }
  
  QString vtkAsciiEncoding(const Point3f& p)
  {
    return QString("%1 %2 %3").arg(p.x(), 0, 'g', 10).arg(p.y(), 0, 'g', 10).arg(p.z(), 0, 'g', 10);
  }
  
  QString vtkAsciiEncoding(const Point2i& p)
  {
    return QString("%1 %2").arg(p.x()).arg(p.y());
  }
  
  QString vtkAsciiEncoding(const Point3i& p)
  {
    return QString("%1 %2 %3").arg(p.x()).arg(p.y()).arg(p.z());
  }
  
  template <typename T>
  static QDomText vtkAsciiEncoding(const std::vector<T>& values, QDomDocument& doc)
  {
    QStringList strings;
#if QT_VERSION >= 0x040700
    strings.reserve(values.size());
#endif
    forall(const T& v, values)
        strings << vtkAsciiEncoding(v);
    return doc.createTextNode(strings.join("\n"));
  }
  
  template <typename T>
  static QDomText vtkEncoding(const std::vector<T>& values, QDomDocument& doc, bool binary)
  {
    if(binary)
      return vtkBinaryEncoding(values, doc);
    else
      return vtkAsciiEncoding(values, doc);
  }
  
  bool MeshExport::saveOBJ(Mesh* mesh, const QString& fileName, bool transform)
  {
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//
//    QTextStream ts(&file);
//
//    const vvGraph& S = mesh->graph();
//
//    std::unordered_map<vertex, int> inv_vs;
//
//    ts << "# Triangular mesh created by MorphoDynamX" << endl;
//    ts << QString::fromWCharArray(L"# Length unit: \xb5m") << endl << endl;
//
//    ts << "# Vertices" << endl;
//
//    // write vertices
//    int i = 0;
//    forall(const vertex& v, S) {
//      inv_vs[v] = i + 1;
//      ts << "v " << savedPos(v->pos, transform, mesh->stack()) << endl;
//      ts << "vn " << normalized(savedPos(v->nrml, transform, mesh->stack())) << endl;
//      ++i;
//    }
//
//    ts << endl << "# Triangles" << endl;
//
//    // write triangles
//    forall(const vertex& v, S) {
//      forall(const vertex& n, S.neighbors(v)) {
//        const vertex& m = S.nextTo(v, n);
//        if(S.uniqueTri(v, n, m)) {
//          ts << "f " << inv_vs[v] << " " << inv_vs[n] << " " << inv_vs[m] << endl;
//        }
//      }
//    }
//
//    file.close();
    return true;
  }
  
  bool MeshExport::saveVTU(Mesh* mesh, const QString& fileName, bool transform, bool binary)
  {
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//
//    QDomDocument doc("VTKFile");
//    QDomElement root = doc.createElement("VTKFile");
//    root.setAttribute("type", "UnstructuredGrid");
//    root.setAttribute("byte_order", "LittleEndian");
//    root.setAttribute("version", "0.1");
//    doc.appendChild(root);
//    // Add the grid
//    QDomElement grid = doc.createElement("UnstructuredGrid");
//    root.appendChild(grid);
//    // Add the pieces
//    QDomElement piece = doc.createElement("Piece");
//    grid.appendChild(piece);
//    const vvGraph& S = mesh->graph();
//    piece.setAttribute("NumberOfPoints", (uint)S.size());
//    QString format;
//    if(binary)
//      format = "binary";
//    else
//      format = "ascii";
//    // Points
//    QDomElement points = doc.createElement("Points");
//    piece.appendChild(points);
//    QDomElement points_pos = doc.createElement("DataArray");
//    points_pos.setAttribute("NumberOfComponents", "3");
//    points_pos.setAttribute("type", "Float32");
//    points_pos.setAttribute("format", format);
//    std::vector<Point3f> points_pos_data(S.size());
//    points.appendChild(points_pos);
//    // Points attributes
//    QDomElement points_data = doc.createElement("PointData");
//    piece.appendChild(points_data);
//    QString signal_name;
//    if(mesh->signalUnit().isEmpty())
//      signal_name = "Signal";
//    else
//      signal_name = QString("Signal (%1)").arg(mesh->signalUnit());
//    points_data.setAttribute("Scalars", signal_name);
//    points_data.setAttribute("Normals", "Normals");
//    QDomElement points_signal = doc.createElement("DataArray");
//    points_signal.setAttribute("Name", signal_name);
//    points_signal.setAttribute("type", "Float32");
//    points_signal.setAttribute("NumberOfComponents", "1");
//    points_signal.setAttribute("format", format);
//    std::vector<float> points_signal_data(S.size());
//    QDomElement points_normal = doc.createElement("DataArray");
//    points_normal.setAttribute("Name", "Normals");
//    points_normal.setAttribute("type", "Float32");
//    points_normal.setAttribute("NumberOfComponents", "3");
//    points_normal.setAttribute("format", format);
//    std::vector<Point3f> points_normal_data(S.size());
//    QDomElement points_label = doc.createElement("DataArray");
//    points_label.setAttribute("Name", "Label");
//    points_label.setAttribute("type", "Int32");
//    points_label.setAttribute("NumberOfComponents", "1");
//    points_label.setAttribute("format", format);
//    IntVec points_label_data(S.size());
//    points_data.appendChild(points_signal);
//    points_data.appendChild(points_normal);
//    points_data.appendChild(points_label);
//    // Initialize vertex number and counts cells
//    int saveId = 0;
//    size_t count_cells = 0;
//    forall(const vertex& v, S) {
//      v->saveId = saveId++;
//      forall(const vertex& n, S.neighbors(v)) {
//        const vertex& m = S.nextTo(v, n);
//        if(S.uniqueTri(v, n, m))
//          ++count_cells;
//      }
//    }
//    // Cells
//    piece.setAttribute("NumberOfCells", (uint)count_cells);
//    QDomElement cells = doc.createElement("Cells");
//    piece.appendChild(cells);
//    QDomElement cells_connectivity = doc.createElement("DataArray");
//    cells_connectivity.setAttribute("type", "Int32");
//    cells_connectivity.setAttribute("Name", "connectivity");
//    cells_connectivity.setAttribute("format", format);
//    std::vector<Point3i> cells_connectivity_data(count_cells);
//    QDomElement cells_offsets = doc.createElement("DataArray");
//    cells_offsets.setAttribute("type", "Int32");
//    cells_offsets.setAttribute("Name", "offsets");
//    cells_offsets.setAttribute("format", format);
//    IntVec cells_offsets_data(count_cells);
//    QDomElement cells_types = doc.createElement("DataArray");
//    cells_types.setAttribute("type", "UInt8");
//    cells_types.setAttribute("Name", "types");
//    cells_types.setAttribute("format", format);
//    std::vector<quint8> cells_types_data(count_cells, quint8(5));
//    cells.appendChild(cells_connectivity);
//    cells.appendChild(cells_offsets);
//    cells.appendChild(cells_types);
//    // Cells attributes
//    QDomElement cells_data = doc.createElement("CellData");
//    piece.appendChild(cells_data);
//    QDomElement cells_label = doc.createElement("DataArray");
//    cells_label.setAttribute("Name", "Label");
//    cells_label.setAttribute("type", "Int32");
//    cells_label.setAttribute("NumberOfComponents", "1");
//    cells_label.setAttribute("format", format);
//    cells_data.appendChild(cells_label);
//    IntVec cells_label_data(count_cells);
//    QDomElement cells_heat = doc.createElement("DataArray");
//    const IntFloatAttr& mesh_cell_heat = mesh->labelHeat();
//    const IntIntFloatAttr& mesh_wall_heat = mesh->wallHeat();
//    bool has_cell_heat = !mesh_cell_heat.empty();
//    bool has_wall_heat = !mesh_wall_heat.empty();
//    QString heat_name = "Heat";
//    if(has_cell_heat)
//      heat_name = "Cell " + heat_name;
//    else if(has_wall_heat)
//      heat_name = "Wall " + heat_name;
//    if(!mesh->heatMapUnit().isEmpty())
//      heat_name += QString(" (%1)").arg(mesh->heatMapUnit());
//
//    float minHeat = mesh->heatMapBounds()[0];
//    float maxHeat = mesh->heatMapBounds()[1];
//
//    cells_heat.setAttribute("Name", heat_name);
//    cells_heat.setAttribute("type", "Float32");
//    cells_heat.setAttribute("NumberOfComponents", "1");
//    cells_heat.setAttribute("format", format);
//    cells_heat.setAttribute("RangeMin", minHeat);
//    cells_heat.setAttribute("RangeMax", maxHeat);
//    std::vector<float> cells_heat_data;
//    if(has_cell_heat or has_wall_heat) {
//      cells_heat_data.resize(count_cells);
//      cells_data.appendChild(cells_heat);
//      cells_data.setAttribute("Scalars", heat_name);
//    } else
//      cells_data.setAttribute("Scalars", "Label");
//
//    std::unordered_set<int> labels;
//
//    // Fills in everything
//    int cell_id = 0;
//    forall(const vertex& v, S) {
//      int id = v->saveId;
//      Point3f pos(savedPos(v->pos, transform, mesh->stack()));
//      Point3f nrml(normalized(savedPos(v->nrml, transform, mesh->stack())));
//      points_pos_data[id] = pos;
//      points_label_data[id] = v->label;
//      points_signal_data[id] = v->signal;
//      points_normal_data[id] = nrml;
//      forall(const vertex& n, S.neighbors(v)) {
//        const vertex& m = S.nextTo(v, n);
//        if(S.uniqueTri(v, n, m)) {
//          cells_connectivity_data[cell_id] = Point3i(id, n->saveId, m->saveId);
//          cells_offsets_data[cell_id] = 3 * (cell_id + 1);
//          int label = getLabel(v, n, m);
//          labels.insert(label);
//          cells_label_data[cell_id] = label;
//          if(has_cell_heat) {
//            IntFloatAttr::const_iterator found = mesh_cell_heat.find(label);
//            if(found != mesh_cell_heat.end())
//              cells_heat_data[cell_id] = found->second;
//            else
//              cells_heat_data[cell_id] = std::numeric_limits<float>::quiet_NaN();
//          } else if(has_wall_heat) {
//            IntIntPair wall;
//            if(mesh->isBordTriangle(label, v, n, m, wall)) {
//              IntIntFloatAttr::const_iterator found = mesh_wall_heat.find(wall);
//              if(found != mesh_wall_heat.end())
//                cells_heat_data[cell_id] = found->second;
//              else
//                cells_heat_data[cell_id] = std::numeric_limits<float>::quiet_NaN();
//            } else
//              cells_heat_data[cell_id] = std::numeric_limits<float>::quiet_NaN();
//          }
//          cell_id++;
//        }
//      }
//    }
//
//    // Global fields
//    QDomElement global_fields = doc.createElement("FieldData");
//    grid.appendChild(global_fields);
//
//    QDomElement scale = doc.createElement("DataArray");
//    scale.setAttribute("Name", "Scale");
//    scale.setAttribute("type", "Float32");
//    scale.setAttribute("NumberOfTuples", 1);
//    scale.setAttribute("format", "ascii");
//    scale.appendChild(doc.createTextNode("1e-6"));
//    global_fields.appendChild(scale);
//
//    QDomElement labels_field = doc.createElement("DataArray");
//    labels_field.setAttribute("Name", "Labels");
//    labels_field.setAttribute("type", "Int32");
//    labels_field.setAttribute("NumberOfTuples", (int)labels.size());
//    labels_field.setAttribute("format", format);
//    IntVec labels_fields_data(labels.begin(), labels.end());
//    std::sort(labels_fields_data.begin(), labels_fields_data.end());
//    labels_field.appendChild(vtkEncoding(labels_fields_data, doc, binary));
//    global_fields.appendChild(labels_field);
//
//    if(has_cell_heat) {
//      QDomElement cell_heat = doc.createElement("DataArray");
//      QString cell_heat_name = "Cell Heat";
//      if(!mesh->heatMapUnit().isEmpty())
//        cell_heat_name += QString(" (%1)").arg(mesh->heatMapUnit());
//      cell_heat.setAttribute("Name", cell_heat_name);
//      cell_heat.setAttribute("type", "Float32");
//      cell_heat.setAttribute("NumberOfTuples", (int)labels.size());
//      cell_heat.setAttribute("format", format);
//      cell_heat.setAttribute("RangeMin", minHeat);
//      cell_heat.setAttribute("RangeMax", maxHeat);
//      std::vector<float> cell_heat_data(labels.size());
//      for(size_t i = 0; i < labels.size(); ++i) {
//        int label = labels_fields_data[i];
//        IntFloatAttr::const_iterator found = mesh_cell_heat.find(label);
//        if(found == mesh_cell_heat.end())
//          cell_heat_data[i] = std::numeric_limits<float>::quiet_NaN();
//        else
//          cell_heat_data[i] = found->second;
//      }
//      cell_heat.appendChild(vtkEncoding(cell_heat_data, doc, binary));
//      global_fields.appendChild(cell_heat);
//    } else if(has_wall_heat) {
//      QDomElement walls_fields = doc.createElement("DataArray");
//      walls_fields.setAttribute("Name", "Walls");
//      walls_fields.setAttribute("type", "Int32");
//      walls_fields.setAttribute("NumberOfTuples", (int)mesh_wall_heat.size());
//      walls_fields.setAttribute("format", format);
//      std::vector<Point2i> walls_fields_data(mesh_wall_heat.size());
//
//      QDomElement wall_heat = doc.createElement("DataArray");
//      QString wall_heat_name = "Wall Heat";
//      if(!mesh->heatMapUnit().isEmpty())
//        wall_heat_name += QString(" (%1)").arg(mesh->heatMapUnit());
//      wall_heat.setAttribute("Name", wall_heat_name);
//      wall_heat.setAttribute("type", "Float32");
//      wall_heat.setAttribute("NumberOfTuples", (int)mesh_wall_heat.size());
//      wall_heat.setAttribute("format", format);
//      wall_heat.setAttribute("RangeMin", minHeat);
//      wall_heat.setAttribute("RangeMax", maxHeat);
//      std::vector<float> wall_heat_data(labels.size());
//      int i = 0;
//      forall(const IntIntFloatPair& vs, mesh_wall_heat) {
//        const IntIntPair& w = vs.first;
//        float value = vs.second;
//        walls_fields_data[i] = Point2i(w.first, w.second);
//        wall_heat_data[i] = value;
//        ++i;
//      }
//      walls_fields.appendChild(vtkEncoding(walls_fields_data, doc, binary));
//      wall_heat.appendChild(vtkEncoding(wall_heat_data, doc, binary));
//      global_fields.appendChild(walls_fields);
//      global_fields.appendChild(wall_heat);
//    }
//
//    points_pos.appendChild(vtkEncoding(points_pos_data, doc, binary));
//    points_label.appendChild(vtkEncoding(points_label_data, doc, binary));
//    points_signal.appendChild(vtkEncoding(points_signal_data, doc, binary));
//    points_normal.appendChild(vtkEncoding(points_normal_data, doc, binary));
//
//    cells_connectivity.appendChild(vtkEncoding(cells_connectivity_data, doc, binary));
//    cells_offsets.appendChild(vtkEncoding(cells_offsets_data, doc, binary));
//    cells_types.appendChild(vtkEncoding(cells_types_data, doc, binary));
//
//    if(has_cell_heat or has_wall_heat)
//      cells_heat.appendChild(vtkEncoding(cells_heat_data, doc, binary));
//
//    cells_label.appendChild(vtkEncoding(cells_label_data, doc, binary));
//
//    QString xmlString = doc.toString();
//    QByteArray xmlArray = xmlString.toUtf8();
//    qint64 qty = file.write(xmlArray);
//    if(qty != xmlArray.size())
//      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(xmlArray.size());
//
//    file.close();
//
    return true;
  }
  
  bool MeshExport::saveText(Mesh* mesh, const QString& fileName, bool transform)
  {
//    const Stack* stack = mesh->stack();
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//
//    const vvGraph& S = mesh->graph();
//
//    QTextStream out(&file);
//    int saveId = 0;
//    int i = 0;
//
//    out << S.size() << endl;
//
//    progressStart(QString("Saving Text Mesh %1").arg(mesh->userId()), S.size() * 2);
//    forall(const vertex& v, S) {
//      v->saveId = saveId++;
//      Point3f pos(savedPos(v->pos, transform, stack));
//      out << v->saveId << " " << pos << " " << v->label << endl;
//      if(!progressAdvance(i))
//        userCancel();
//      ++i;
//    }
//    forall(const vertex& v, S) {
//      out << v->saveId << " " << S.valence(v);
//      forall(const vertex& n, S.neighbors(v))
//          out << " " << n->saveId;
//      out << endl;
//      if(!progressAdvance(i))
//        userCancel();
//      ++i;
//    }
//    if(!progressAdvance(S.size() * 2))
//      userCancel();
//    file.close();
//    SETSTATUS("Saved mesh, file:" << mesh->file() << ", vertices:" << S.size());
    return true;
  }
  
  bool MeshExport::saveCells(Mesh* mesh, const QString& fileName, bool transform)
  {
//    const Stack* stack = mesh->stack();
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//
//    const vvGraph& S = mesh->graph();
//
//    QTextStream out(&file);
//    int saveId = 0;
//    progressStart(QString("Saving Cells %1").arg(mesh->userId()), S.size() * 2);
//    out << S.size() << endl;
//    int i = 0;
//    forall(const vertex& v, S) {
//      v->saveId = saveId++;
//      Point3f pos(savedPos(v->pos, transform, stack));
//      out << v->saveId << " " << pos << " " << v->label << " " << v->type << endl;
//      if(!progressAdvance(i))
//        userCancel();
//      ++i;
//    }
//    forall(const vertex& v, S) {
//      out << v->saveId << " " << S.valence(v);
//      forall(const vertex& n, S.neighbors(v))
//          out << " " << n->saveId;
//      out << endl;
//      if(!progressAdvance(i))
//        userCancel();
//      ++i;
//    }
//    if(!progressAdvance(S.size() * 2))
//      userCancel();
//    SETSTATUS("Saved cellular mesh, file:" << mesh->file() << ", vertices:" << S.size());
    return true;
  }
  
  bool MeshExport::saveMeshEdit(Mesh* mesh, const QString& fileName, bool transform)
  {
//    const Stack* stack = mesh->stack();
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//
//    const vvGraph& S = mesh->graph();
//
//    QTextStream out(&file);
//    int saveId = 1;
//    out << "MeshVersionFormatted 1" << endl;
//    out << "Dimension 3" << endl;
//    out << "Vertices" << endl;
//    out << S.size() << endl;
//    progressStart(QString("Saving Mesh Edit %1").arg(mesh->userId()), S.size());
//    int i = 0;
//    forall(const vertex& v, S) {
//      v->saveId = saveId++;
//      Point3f pos(savedPos(v->pos, transform, stack));
//      out << pos << " " << v->label << endl;
//    }
//    // Count the triangles
//    int count = 0;
//    forall(const vertex& v, S)
//        forall(const vertex& n, S.neighbors(v)) {
//      vertex m = S.nextTo(v, n);
//      if(!S.uniqueTri(v, n, m))
//        continue;
//      count++;
//    }
//    out << "Triangles" << endl;
//    out << count << endl;
//    forall(const vertex& v, S) {
//      forall(const vertex& n, S.neighbors(v)) {
//        vertex m = S.nextTo(v, n);
//        if(!S.uniqueTri(v, n, m))
//          continue;
//        out << v->saveId << " " << n->saveId << " " << m->saveId << " ";
//        out << getLabel(v, n, m) << endl;
//      }
//      if(!progressAdvance(i))
//        userCancel();
//    }
//    if(!progressAdvance(S.size()))
//      userCancel();
//    out << "End" << endl;
//    SETSTATUS("Saving triangle mesh, file:" << mesh->file() << ", vertices:" << S.size());
    return true;
  }
  
  bool MeshExport::saveMeshSTL(Mesh* mesh, const QString& fileName, bool transform)
  {
//    const Stack* stack = mesh->stack();
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//
//    const vvGraph& S = mesh->graph();
//
//    QTextStream out(&file);
//    progressStart(QString("Saving STL Mesh %1").arg(mesh->userId()), S.size());
//    int i = 0;
//
//    out << "solid mdx" << endl;
//    const std::vector<vertex>& av = mesh->activeVertices();
//    forall(const vertex& v, av) {
//      forall(const vertex& n, S.neighbors(v)) {
//        vertex m = S.nextTo(v, n);
//        if(!S.uniqueTri(v, n, m))
//          continue;
//        Point3f vpos(savedPos(v->pos, transform, stack));
//        Point3f npos(savedPos(n->pos, transform, stack));
//        Point3f mpos(savedPos(m->pos, transform, stack));
//        Point3f nrml(((npos - vpos) ^ (mpos - vpos)).normalize());
//        out << "facet normal " << nrml.x() << " " << nrml.y() << " " << nrml.z() << endl;
//        out << "  outer loop" << endl;
//        out << "    vertex " << vpos.x() << " " << vpos.y() << " " << vpos.z() << endl;
//        out << "    vertex " << npos.x() << " " << npos.y() << " " << npos.z() << endl;
//        out << "    vertex " << mpos.x() << " " << mpos.y() << " " << mpos.z() << endl;
//        out << "  endloop" << endl;
//        out << "endfacet" << endl;
//      }
//      if(!progressAdvance(i))
//        userCancel();
//    }
//    out << "endsolid mdx" << endl;
//    if(!progressAdvance(S.size()))
//      userCancel();
//    SETSTATUS("Saving STL mesh, file:" << mesh->file() << ", vertices:" << S.size());
    return true;
  }
  
  bool MeshExport::savePLY(Mesh &mesh, const CCStructure &cs, const QString& fileName, bool transform, bool binary)
  {
    auto &indexAttr = mesh.indexAttr();
    const Stack* stack = mesh.stack();
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
      throw QString("MeshExport::Error:Cannot open output file: %1").arg(fileName);

    QTextStream outText(&file);
    QDataStream outBinary(&file);
    outBinary.setByteOrder(QDataStream::LittleEndian);
    outBinary.setFloatingPointPrecision(QDataStream::SinglePrecision);

    bool doSignal = mesh.signalExists() and mesh.signalType() == "Double";
    const CCIndexDoubleAttr &signalAttr = doSignal ? mesh.signalAttr<double>(mesh.signal()) : CCIndexDoubleAttr();
    bool doVolumes = cs.maxDimension() > 2;

    // Write header
    outText << "ply" << endl;
    if(binary)
      outText << "format binary_little_endian 1.0" << endl;
    else
      outText << "format ascii 1.0" << endl;
    outText << "comment Exported from MorphoDynamX " << VERSION << " " REVISION << endl;
    outText << "element vertex " << cs.vertices().size() << endl;
    outText << "property float x" << endl;
    outText << "property float y" << endl;
    outText << "property float z" << endl;
    outText << "property int label" << endl;
    if(doSignal)
      outText << "property float signal" << endl;
    outText << "element face " << cs.faces().size() << endl;
    outText << "property list uint int vertex_index" << endl;
    outText << "property int label" << endl;
    if(doSignal)
      outText << "property float signal" << endl;
    if(doVolumes) {
      // RSS Do we really need to write the edges?
      outText << "element edge " << cs.edges().size() << endl;
      outText << "property int source" << endl;
      outText << "property int target" << endl;
      outText << "element volume " << cs.volumes().size() << endl;
      outText << "property list uint int face_index" << endl;
      outText << "property int label" << endl;
      if(doSignal)
        outText << "property float signal" << endl;
    }
    outText << "end_header" << endl;

    // Write the vertices
    std::unordered_map<CCIndex, uint> vMap;
    for(uint i = 0; i < cs.vertices().size(); i++) {
      if(!progressAdvance(0))
        userCancel();

      auto v = cs.vertices()[i];
      vMap[v] = i;
      const auto &vIdx = indexAttr[v];
      Point3f pos(savedPos(vIdx.pos, transform, stack));
      if(binary) {
        outBinary << float(pos.x()) << float(pos.y()) << float(pos.z()) << qint32(vIdx.label); 
        if(doSignal)
          outBinary << float(signalAttr[v]);
      } else {
        outText << pos << " " << vIdx.label; 
        if(doSignal)
          outText << " " << signalAttr[v];
        outText << endl;
      }
    }

    // Write the faces
    std::unordered_map<CCIndex, uint> fMap;
    for(uint i = 0; i < cs.faces().size(); i++) {
      if(!progressAdvance(0))
        userCancel();

      auto f = cs.faces()[i];
      fMap[f] = i;
      const auto &fIdx = indexAttr[f];
      auto fVertices = faceVertices(cs, f);
      if(binary)
        outBinary << quint32(fVertices.size());
      else
        outText << fVertices.size();
      for(auto v : fVertices) {
        if(binary)
          outBinary << qint32(vMap[v]);
        else
          outText << " " << vMap[v];
      }
      if(binary) {
        outBinary << qint32(fIdx.label);
        if(doSignal)
          outBinary << float(signalAttr[f]);
      } else {
        outText << " " << fIdx.label;
        if(doSignal)
          outText << " " << signalAttr[f];
        outText << endl;
      }
    }

    if(doVolumes) {
      // Write the edges
      for(auto e : cs.edges()) {
        if(!progressAdvance(0))
          userCancel();
  
        auto eb = cs.edgeBounds(e);
        if(binary)
          outBinary << qint32(vMap[eb.first]) << qint32(vMap[eb.second]);
        else
          outText << vMap[eb.first] << " " << vMap[eb.second] << endl;
      }
  
      // Write the volumes 
      for(auto l : cs.volumes()) {
        if(!progressAdvance(0))
          userCancel();
  
        const auto &lIdx = indexAttr[l];
        auto lFaces = cs.bounds(l);
        if(binary)
          outBinary << quint32(lFaces.size());
        else
          outText << lFaces.size();
        for(auto f : lFaces) {
          int fInd = fMap[f] + 1;
          if(cs.ro(l, f) == ccf::NEG)
            fInd = -fInd;
          if(binary)
            outBinary << qint32(fInd);
          else
            outText << " " << fInd;
        }
        if(binary) {
          outBinary << qint32(lIdx.label);
          if(doSignal)
            outBinary << float(signalAttr[l]);
        } else {
          outText << " " << lIdx.label;
          if(doSignal)
            outText << " " << signalAttr[l];
          outText << endl;
        }
      }
    }
    setStatus(QString("Saved mesh to file: %1 ").arg(mesh.file()));

    return true;
  }
  REGISTER_PROCESS(MeshExport);
  
  bool MeshSave::initialize(QWidget* parent)
  {
    Mesh *mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("MeshSave::initialize Invalid mesh Id %1").arg(parm("Mesh Id").toInt()));

    QString fileName = mesh->file();
    if(fileName.isEmpty())
      fileName = parm("File Name");

    if(!parent) return true;

    QDialog dlg(parent);
    Ui_SaveMeshDialog ui;
    ui.setupUi(&dlg);

    this->ui = &ui;
    this->dlg = &dlg;

    connect(ui.SelectMeshFile, SIGNAL(clicked()), this, SLOT(selectMeshFile()));

    setMeshFile(fileName);
    ui.Transform->setChecked(stringToBool(parm("Transform")));

    bool res = false;
    if(dlg.exec() == QDialog::Accepted) {
      setParm("File Name", properFile(ui.MeshFile->text()));
      setParm("Transform", ui.Transform->isChecked() ? "yes" : "no");
      res = true;
    }
    this->ui = 0;
    this->dlg = 0;
    return res;
  }
  
  QString MeshSave::properFile(QString fileName) const
  {
    QFileInfo fi(fileName);
    QString suf = fi.suffix();
    if(!suf.isEmpty())
      fileName = fileName.left(fileName.size() - suf.size() - 1);
    return fileName + ".mdxm";
  }
  
  void MeshSave::selectMeshFile()
  {
    QString fileName = ui->MeshFile->text();
    fileName = QFileDialog::getSaveFileName(dlg, 
						     QString("Save mesh as ..."), fileName, "Mesh files (*.mdxm)", 0, FileDialogOptions);
    if(!fileName.isEmpty())
      setMeshFile(fileName);
  }
  
  void MeshSave::setMeshFile(const QString& fileName)
  {
		if(fileName.endsWith(".mgxm", Qt::CaseInsensitive)) 
      ui->MeshFile->setText(fileName.left(fileName.length() - 5) + ".mdxm");
	  else if(!fileName.endsWith(".mdxm", Qt::CaseInsensitive))
      ui->MeshFile->setText(fileName + ".mdxm");
    else
      ui->MeshFile->setText(fileName);
  }
  
  bool MeshSave::run()
  {
    Mesh* mesh = currentMesh();
    if(!parm("Mesh Id").isEmpty())
      mesh = getMesh(parm("Mesh Id").toInt());
    if(!mesh)
      throw(QString("MeshSave::initialize Invalid mesh Id %1").arg(parm("Mesh Id").toInt()));

    bool res = run(mesh, parm("File Name"), stringToBool(parm("Transform")));
    return res;
  }
  
  Point3d MeshSave::savedPos(Point3d pos, bool transform, const Stack* stack)
  {
    if(transform)
      pos = Point3d(stack->getFrame().inverseCoordinatesOf(Vec(pos)));
    return pos;
  }
  
  bool MeshSave::run(Mesh* mesh, const QString& fileName, bool transform)
  {
    actingFile(fileName);
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly)) {
      setErrorMessage(QString("MeshSave::Error:Cannot open output file: %1").arg(fileName));
      return false;
    }

    // Write out the mesh
    mesh->write(file, transform);

    file.close();
    mesh->setFile(stripCurrentDir(fileName));

    setStatus(QString("Saved mesh, file: %1").arg(mesh->file()));

    return true;
  }
  REGISTER_PROCESS(MeshSave);

  bool savePLYCellGraphMarion(Mesh* mesh, const QString& fileName, bool binary)
  {
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      //setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//
//    const vvGraph& S = mesh->graph();
//    const CellTissue& T = mesh->tissue();
//    QTextStream outText(&file);
//    QDataStream outBinary(&file);
//    outBinary.setByteOrder(QDataStream::LittleEndian);
//    outBinary.setFloatingPointPrecision(QDataStream::SinglePrecision);
//
//    if(mesh->meshType() != "MDX2D") return false;
//    progressStart(QString("Saving Cell Graph").arg(mesh->userId()), 0);
//    // First count the junctions
//    int cells = 0;
//    int junctions = 0;
//    // Cell mesh
//    forall(const vertex& v, S) {
//      if(v->type == 'c')
//        ++cells;
//      else if(v->type == 'j')
//        // We are not writing cell centers
//        v->saveId = junctions++;
//      else {
//        //setErrorMessage(QString("MeshExport::Error:Bad vertex type:%1").arg(v->type));
//        return false;
//      }
//    }
//
//    //IntVec allLabels = findAllLabels(S);
//
//    int cellNr = 0;
//    int edges = 0;
//    forall(const cell& c, T.C) {
//      cellNr++;
//      forall(const cell& n, T.C.neighbors(c)) {
//        edges++;
//      }
//    }
//
//    // Write header
//    outText << "ply" << endl;
//    if(binary)
//      outText << "format binary_little_endian 1.0" << endl;
//    else
//      outText << "format ascii 1.0" << endl;
//    outText << "comment Exported from MorphoDynamX " << VERSION << " " REVISION << endl;
//    outText << "comment Cell Graph: each vertex is a cell, each edge a wall" << endl;
//    outText << "element vertex " << cellNr << endl;
//    outText << "property int label" << endl;
//    outText << "property float x" << endl;
//    outText << "property float y" << endl;
//    outText << "property float z" << endl;
//    outText << "property int parent" << endl;
//    outText << "property float cellArea" << endl; // TODO
//    outText << "property list uchar float cellCurvature" << endl; // TODO
//    outText << "property float aspectRatio" << endl; // TODO
//    outText << "property float growthRate" << endl; // TODO
//    outText << "property list uchar float growthDirections" << endl; // TODO
//    outText << "element edge " << edges << endl;
//    outText << "property uint source" << endl;
//    outText << "property uint target" << endl;
//    outText << "property float wallLength" << endl; // TODO
//    outText << "property int newWall" << endl; // TODO
//    outText << "property float wallElongation" << endl; // TODO
//    outText << "property float angle" << endl; // TODO
//    outText << "end_header" << endl;
//
//    float placeHolder = 0.f;
//
//    AttrMap<int, double>& cellArea                  = mesh->attributes().attrMap<int, double>("Measure Label Double cellArea");
//    AttrMap<int, SymmetricTensor>& cellCurvature    = mesh->attributes().attrMap<int, SymmetricTensor>("Measure Label Tensor cellCurvature");
//    AttrMap<int, double>& aspectRatio               = mesh->attributes().attrMap<int, double>("Measure Label Double aspectRatio");
//    AttrMap<int, double>& growthRate                = mesh->attributes().attrMap<int, double>("Measure Label Double growthRate");
//    AttrMap<int, SymmetricTensor>& growthDirections = mesh->attributes().attrMap<int, SymmetricTensor>("Measure Label Tensor growthDirections");
//
//    AttrMap<std::pair<int, int>, double>& wallLength    = mesh->attributes().attrMap<std::pair<int, int>, double>("Measure Edge Double wallLength");
//    AttrMap<std::pair<int, int>, double>& newWall       = mesh->attributes().attrMap<std::pair<int, int>, double>("Measure Edge Double newWall");
//    AttrMap<std::pair<int, int>, double>& wallElongation= mesh->attributes().attrMap<std::pair<int, int>, double>("Measure Edge Double wallElongation");
//    AttrMap<std::pair<int, int>, double>& angle         = mesh->attributes().attrMap<std::pair<int, int>, double>("Measure Edge Double angle");
//
//
//    // First write vertices
//    forall(const cell& c, T.C) {
//      int lengthTensor = 9;
//
//      Point3f pos = Point3f(0,0,0);//v->pos;//savedPos(v->pos, transform, stack);
//      if(binary){
//        outBinary << c->label << c->pos.x() << c->pos.y() << c->pos.z() << mesh->labelMap("Parents")[c->label];
//        outBinary << placeHolder
//                  << lengthTensor << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder
//                  << placeHolder<< placeHolder
//                  << lengthTensor << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << endl;// << v->signal;
//      } else {
//        // until cellArea
//        outText << c->label << " " << c->pos << " " << mesh->labelMap("Parents")[c->label] << " " << cellArea[c->label] << " ";
//        // cellCurvature
//        outText << lengthTensor << " " << cellCurvature[c->label].ev1() << " " << cellCurvature[c->label].ev2() << " " << cellCurvature[c->label].evals() << " ";
//        // aspectRatio/growthRate
//        outText<< aspectRatio[c->label] << " " << growthRate[c->label] << " ";
//        // growthDirections
//        outText << lengthTensor << " " << growthDirections[c->label].ev1() << " " << growthDirections[c->label].ev2() << " " << growthDirections[c->label].evals() << " ";
//        //if(!progressAdvance(0))
//        //  userCancel();
//      }
//    }
//    // Now write walls (edges)
//    forall(const cell& c, T.C) {
//      forall(const cell& n, T.C.neighbors(c)) {
//        std::pair<int,int> currentEdge = std::make_pair(c->label, n->label);
//        if(binary)
//          outBinary << c->label << n->label << wallLength[currentEdge] << newWall[currentEdge] << wallElongation[currentEdge] << angle[currentEdge] << endl;
//        else
//          outText << c->label << " " << n->label << " " << wallLength[currentEdge] << " " << newWall[currentEdge] << " " << wallElongation[currentEdge] << " " << angle[currentEdge] << endl;
//      }
//    }
//
    return true;
  }


  bool savePLYCellGraphGeneral(Mesh* mesh, std::vector<PlyCellData>& cellVec, std::vector<PlyWallData>& wallVec, const QString& fileName, bool binary)
  {
//    std::cout << "save ply " << std::endl;
//    QFile file(fileName);
//    if(!file.open(QIODevice::WriteOnly)) {
//      //setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(fileName));
//      return false;
//    }
//    std::cout << "go " << std::endl;
//    const vvGraph& S = mesh->graph();
//    const CellTissue& T = mesh->tissue();
//    QTextStream outText(&file);
//    QDataStream outBinary(&file);
//    outBinary.setByteOrder(QDataStream::LittleEndian);
//    outBinary.setFloatingPointPrecision(QDataStream::SinglePrecision);
//
//    if(mesh->meshType() != "MDX2D") return false;
//    //progressStart(QString("Saving Cell Graph").arg(mesh->userId()), 0);
//    // First count the junctions
//    int cells = 0;
//    int junctions = 0;
//    // Cell mesh
//    forall(const vertex& v, S) {
//      if(v->type == 'c')
//        ++cells;
//      else if(v->type == 'j')
//        // We are not writing cell centers
//        v->saveId = junctions++;
//      else {
//        //setErrorMessage(QString("MeshExport::Error:Bad vertex type:%1").arg(v->type));
//        return false;
//      }
//    }
//
//
//    int cellNr = 0;
//    int edges = 0;
//    forall(const cell& c, T.C) {
//      cellNr++;
//      forall(const cell& n, T.C.neighbors(c)) {
//        edges++;
//      }
//    }
//    std::cout << "write file " << std::endl;
//    // Write header
//    outText << "ply" << endl;
//    if(binary)
//      outText << "format binary_little_endian 1.0" << endl;
//    else
//      outText << "format ascii 1.0" << endl;
//    outText << "comment Exported from MorphoDynamX " << VERSION << " " REVISION << endl;
//    outText << "comment Cell Graph: each vertex is a cell, each edge a wall" << endl;
//    outText << "element vertex " << cellNr << endl;
//    outText << "property int label" << endl;
//    outText << "property float x" << endl;
//    outText << "property float y" << endl;
//    outText << "property float z" << endl;
//    forall(const PlyCellData& data, cellVec){
//      if(data.type == "uint" or data.type == "float"){
//        outText << "property " << data.type << " " << data.name << endl;
//      } else if(data.type == "vector"){
//        outText << "property list uchar float " << data.name << endl;
//      } else if(data.type == "tensor"){
//        outText << "property list uchar float " << data.name << endl;
//      }
//    }
//    outText << "element edge " << edges << endl;
//    outText << "property uint source" << endl;
//    outText << "property uint target" << endl;
//    forall(const PlyWallData& data, wallVec){
//      outText << "property " << data.type << " " << data.name << endl;
//    }
//    outText << "end_header" << endl;
//
//    float placeHolder = 0.f;
//    int placeHInt = 0;
//
//    AttrMap<int, double>& areaMap = mesh->attributes().attrMap<int, double>("Measure Label Double /Geometry/Area");
//
//    std::cout << "write data " << std::endl;
//    // First write vertices
//    forall(const cell& c, T.C) {
//      int lengthTensor = 9;
//
//      Point3f pos = Point3f(0,0,0);//v->pos;//savedPos(v->pos, transform, stack);
//      if(binary){
//        /*   outBinary << c->label << c->pos.x() << c->pos.y() << c->pos.z() << mesh->labelMap("Parents")[c->label]<< placeHolder
//          << lengthTensor << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder
//          << placeHolder<< placeHolder
//          << lengthTensor << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << placeHolder << endl;// << v->signal;
//      */} else {
//        outText << c->label << " " << c->pos.x() << " " << c->pos.y() << " " << c->pos.z() << " ";
//        forall(PlyCellData& data, cellVec){
//          //qDebug() << "bla2 " << data.type << "\n";
//          if(data.type == "uint"){
//            outText << data.intData[c->label] << " ";
//          } else if(data.type == "float"){
//            outText << data.floatData[c->label] << " ";
//          } else if(data.type == "vector"){
//            outText << "3 " << data.pointData[c->label].x() << " "
//                    << data.pointData[c->label].y() << " "
//                    << data.pointData[c->label].z() << " ";
//          } else if(data.type == "tensor"){
//            outText << "9 " << data.tensorData[c->label].ev1() << " "
//                    << data.tensorData[c->label].ev2() << " "
//                    << data.tensorData[c->label].evals() << " ";
//          }
//        }
//        outText << endl;
//      }
//
//      //if(!progressAdvance(0))
//      //  userCancel();
//    }
//    // Now write walls (edges)
//    forall(const cell& c, T.C) {
//      forall(const cell& n, T.C.neighbors(c)) {
//        if(binary)
//          outBinary << c->label << n->label << endl;//<< placeHolder << placeHInt << placeHolder << placeHolder << endl;
//        else
//          outText << c->label << " " << n->label << endl;//<< " " << placeHolder << " " << placeHInt << " " << placeHolder << " " << placeHolder << endl;
//      }
//    }
//
    return true;
  }

  bool loadCellGraphPLYGeneral(Mesh* mesh, const QString& fileName)
  {
    // Define the reader and read header
    PlyFile ply;
    if(!ply.parseHeader(fileName))
      throw(QString("Cannot parse PLY file header"));

    if(!ply.parseContent())
      throw(QString("Unable to parse contents of PLY file"));

    // go through all cells (vertex)
    PlyFile::Element* vtx = ply.element("vertex");

    // get label property and fill idxLabelMap
    PlyFile::Property* cLabel = vtx->property("label");
    std::map<int,int> idxLabelMap;

    IntVec& labels = *cLabel->value<int>();

    for(size_t i = 0; i<vtx->size(); i++){
      idxLabelMap[i] = labels[i];
    }

    // go through all properties and create data
    size_t nrProp = vtx->nbProperties();

    for(size_t i = 0; i<nrProp; i++){
      PlyFile::Property* p = vtx->property(i);
      if(p->name() == "label") continue;
      if(p->kind() == PlyFile::Property::VALUE){
        if(p->memType() == PlyFile::INT){
          IntVec& values = *p->value<int>();
          QString attrName = "Measure Label Int " + p->name();
          AttrMap<int, int>& attrData = mesh->attributes().attrMap<int, int>(attrName);
          attrData.clear();
          for(size_t j = 0; j<vtx->size(); j++){
            attrData[labels[j]] = values[j];
          }

        } else if(p->memType() == PlyFile::FLOAT){

          //PlyFile::Property* t = vtx->property(p->name());
          const std::vector<float>& values = *p->value<float>();
          QString attrName = "Measure Label Double " + p->name();
          AttrMap<int, double>& attrData = mesh->attributes().attrMap<int, double>(attrName);
          attrData.clear();
          for(size_t j = 0; j<vtx->size(); j++){
            attrData[labels[j]] = values[j];
          }
        }
      } else { // list
        if(p->memType() == PlyFile::FLOAT){
          std::vector<std::vector<float> >& values = *p->list<float>();
          if(values[0].size() > 0 and values[0].size() <=3){ // assume vector
            QString attrName = "Measure Label Vector " + p->name();
            AttrMap<int, Point3d>& attrData = mesh->attributes().attrMap<int, Point3d>(attrName);
            attrData.clear();
            for(size_t j = 0; j<vtx->size(); j++){
              if(values[0].size()>0) attrData[labels[j]].x() = values[j][0];
              if(values[0].size()>1) attrData[labels[j]].y() = values[j][1];
              if(values[0].size()>2) attrData[labels[j]].z() = values[j][2];
            }

          } else if(values[0].size() > 3){ // assume tensor
            QString attrName = "Measure Label Tensor " + p->name();
            IntSymTensorAttr &attrData = mesh->attributes().attrMap<int, SymmetricTensor>(attrName);
            attrData.clear();
            for(size_t j = 0; j<vtx->size(); j++){
              Point3d ev1, ev2, evals;
              ev1.x() = values[j][0];
              ev1.y() = values[j][1];
              ev1.z() = values[j][2];
              ev2.x() = values[j][3];
              if(values[0].size()>4) ev2.y() = values[j][4];
              if(values[0].size()>5) ev2.z() = values[j][5];
              if(values[0].size()>6) evals.x() = values[j][6];
              if(values[0].size()>7) evals.y() = values[j][7];
              if(values[0].size()>8) evals.z() = values[j][8];
              attrData[labels[j]] = SymmetricTensor(ev1,ev2,evals);
            }

          }

        }
      }
    }

    // now cell walls (edge)
    PlyFile::Element* edges = ply.element("edge");

    nrProp = edges->nbProperties();

    // TODO
    for(size_t i = 0; i<nrProp; i++){

    }

    return true;
  }

  // fill a QTreeWidget with all existing measures
  void fillTreeWidgetWithAttrMaps(Mesh* m, QTreeWidget* tree)
  {
    Attributes *attributes;
    attributes = &m->attributes();
    QStringList attr = attributes->attrList();
    QStringList measureAttr;

    forall(const QString &text, attr) {
      QStringList list = text.split(" ");

      if(list.size() < 4) continue;
      if(list[0] != "Measure") continue;
      QString name;
      for(int i = 3; i<list.size(); i++){
        name = name + list[i] + " ";
      }
      QTreeWidgetItem *item = new QTreeWidgetItem(QStringList() << text);
      item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable);
      item->setCheckState(1, Qt::Unchecked);
      tree->addTopLevelItem(item);
    }

  }

  void SavePlyFile::selectAll(Ui_PlyCellGraphDlg &ui)
  {
    for(int itemCount = 0; itemCount < ui.attrMapTree->topLevelItemCount(); ++itemCount)
      ui.attrMapTree->topLevelItem(itemCount)->setCheckState(1, Qt::Checked);
  }

  void SavePlyFile::unselectAll(Ui_PlyCellGraphDlg &ui)
  {
    for(int itemCount = 0; itemCount < ui.attrMapTree->topLevelItemCount(); ++itemCount)
      ui.attrMapTree->topLevelItem(itemCount)->setCheckState(1, Qt::Unchecked);
  }

  // file dialogue for save data
  bool SavePlyFile::initialize(QWidget* parent)
  {
//    Ui_PlyCellGraphDlg ui;
//    Mesh *m = currentMesh();
//    if(m->meshType()!="MDX2D" and m->meshType()!="MDX3D") return true;
//    QString fileName = parms[0];
//    if(fileName.isEmpty() and parent)
//      fileName = QFileDialog::getSaveFileName(0, "Choose spreadsheet file to save", QDir::currentPath(), "PLY files (*.ply)");
//    if(fileName.isEmpty())
//      return false;
//    if(!fileName.endsWith(".ply", Qt::CaseInsensitive))
//      fileName += ".ply";
//    parms[0] = fileName;
//
//    if(!parent) return true;
//
//    // now the new GUI
//    QDialog dlg(parent);
//    ui.setupUi(&dlg);
//
//    connect(ui.selectAllButton, SIGNAL(clicked()), this, SLOT(selectAll()));
//    connect(ui.unselectAllButton, SIGNAL(clicked()), this, SLOT(unselectAll()));
//
//
//    // find attribute maps
//
//    //Mesh* m = currentMesh();
//
//    fillTreeWidgetWithAttrMaps(m,ui.attrMapTree);
//
//    //dlg.exec();
//
//    if(stringToBool(parms[1])) return true;
//
//    if(dlg.exec() == QDialog::Accepted){
//      QStringList newList;
//      for(int itemCount = 0; itemCount < ui.attrMapTree->topLevelItemCount(); ++itemCount){
//        QString currentAttr = ui.attrMapTree->topLevelItem(itemCount)->text(0);
//        if(Qt::Checked == ui.attrMapTree->topLevelItem(itemCount)->checkState(1))
//          newList << currentAttr;
//      }
//      attrMapsToBeSaved = newList;
//
//    }
//
//
    return true;
  }

  void createPlyDataFromAttr(Mesh* m, QStringList attrMapsToBeSaved, std::vector<PlyCellData>& cellVec, std::vector<PlyWallData>& wallVec)
  {

    forall(QString attr, attrMapsToBeSaved){
      PlyCellData cellData;
      PlyWallData wallData;
      QStringList parts = attr.split(" ");

      if(parts[0] != "Measure") continue; //return setErrorMessage("not a measure");
      if(parts[1] == "Label"){

        cellData.name = parts[3];
        for(int i=4; i<parts.size(); i++) cellData.name += " " + parts[i];

        if(parts[2] == "Double"){
          AttrMap<int, double>& currentMap = m->attributes().attrMap<int, double>(attr);
          qDebug() << attr;
          std::cout << " attr " << currentMap.size() << std::endl;
          forall(IntDoublePair p, currentMap){
            cellData.floatData[p.first] = p.second;
            cellData.type = "float";
          }
        } else if(parts[2] == "Vector"){
          AttrMap<int, Point3d>& currentMap = m->attributes().attrMap<int, Point3d>(attr);
          qDebug() << attr;
          std::cout << " attr " << currentMap.size() << std::endl;
          forall(IntPoint3dPair p, currentMap){
            cellData.pointData[p.first] = Point3f(p.second);
            cellData.type = "vector";
          }
        } else if(parts[2] == "Tensor"){
          AttrMap<int, SymmetricTensor>& currentMap = m->attributes().attrMap<int, SymmetricTensor>(attr);
          qDebug() << attr;
          std::cout << " attr " << currentMap.size() << std::endl;
          typedef std::pair<int, SymmetricTensor> IntSymmetricTensorPair;
          forall(IntSymmetricTensorPair p, currentMap){
            cellData.tensorData[p.first] = p.second;
            cellData.type = "tensor";
          }



        } else {
          continue; //return setErrorMessage("wrong type");
        }
        cellVec.push_back(cellData);
      } else if(parts[1] == "Wall"){
      } else {
        continue; //return setErrorMessage("not a label or wall map");
      }
    }
  }

  bool SavePlyFile::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh) 
      throw(QString("No current mesh"));
    return run(mesh, parm("File Name"), stringToBool(parm("Marion's File")));
  }

  bool SavePlyFile::run(Mesh* m, QString fileName, bool marionsFile)
  {
//    if(m->meshType()!="MDX2D" and m->meshType()!="MDX3D") return setErrorMessage("Mesh Type has to be MDX2D or MDX3D");
//
//    // save mesh information to two ply files
//
//    // 1st file standard mesh exported by mdx
//    MeshExport me(*this);
//    me.savePLY(m, fileName, false, false); // TODO make these parameters?
//
//    // 2nd file contains information about cells (cell graph) and walls
//
//    fileName.remove(".ply", Qt::CaseInsensitive);
//    QString fileCell = fileName + "_cellGraph.ply";
//
//    if(marionsFile){
//      savePLYCellGraphMarion(m,fileCell, false);
//    } else {
//      std::vector<PlyCellData> cellVec;
//      std::vector<PlyWallData> wallVec;
//
//      createPlyDataFromAttr(m, attrMapsToBeSaved, cellVec, wallVec);
//
//      if(!savePLYCellGraphGeneral(m,cellVec,wallVec,fileCell,false)) return setErrorMessage("Error writing the file!");
//    }

    return true;
  }
  REGISTER_PROCESS(SavePlyFile);

  // file dialogue for save data
  bool LoadPlyFileCellGraph::initialize(QWidget* parent)
  {
    QString fileName = parm("File Name");
    if(fileName.isEmpty() and parent)
      fileName = QFileDialog::getSaveFileName(0, "Choose spreadsheet file to save", QDir::currentPath(), "PLY files (*.ply)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".ply", Qt::CaseInsensitive))
      fileName += ".ply";
    setParm("File Name", fileName);
    return true;
  }

  bool LoadPlyFileCellGraph::run()
  {
    Mesh *mesh = currentMesh();
    if(!mesh) throw(QString("No current mesh"));
    return run(mesh, parm("File Name"));
  }

  bool LoadPlyFileCellGraph::run(Mesh* m, const QString& fileName)
  {

    // save mesh information to two ply files

    // 1st file standard mesh exported by mdx
    /*   MeshExport me(*this);
    me.savePLY(m, fileName, false, false); // TODO make these parameters?

    // 2nd file contains information about cells (cell graph) and walls

    QString fileCell = fileName + "_cellGraph.ply";

    savePLYCellGraph(m,fileCell, false);
*/
    loadCellGraphPLYGeneral(m, fileName);

    return true;
  }
  REGISTER_PROCESS(LoadPlyFileCellGraph);
}
