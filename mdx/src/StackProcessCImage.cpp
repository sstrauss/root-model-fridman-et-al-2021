//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <StackProcessCImage.hpp>
//#include <StackProcessSystem.hpp>
//#include <Thrust.hpp>
#include <CImg.h>

using namespace cimg_library;

typedef CImg<ushort> CImgUS;

namespace mdx 
{
  // CImg filters
  bool CImgGaussianBlurStack::run(const Store* input, Store* output, uint radius)
  {
    if(input != output)
      output->data() = input->data();
    Point3u size = input->stack()->size();
    CImgUS image(output->data().data(), size.x(), size.y(), size.z(), 1, true);
    image.blur(radius);
    output->changed();
    output->copyMetaData(input);
    return true;
  }
  REGISTER_PROCESS(CImgGaussianBlurStack);

  bool CImgMedianBlurStack::run(const Store* input, Store* output, uint radius)
  {
    if(input != output)
      output->data() = input->data();
    Point3u size = input->stack()->size();
    CImgUS image(output->data().data(), size.x(), size.y(), size.z(), 1, true);
    image.blur_median(radius);
    output->changed();
    output->copyMetaData(input);
    return true;
  }
  REGISTER_PROCESS(CImgMedianBlurStack); 

  bool CImgLaplaceStack::run(const Store* input, Store* output)
  {
    if(input != output)
      memcpy(output->data().data(), input->data().data(), input->data().size() * 2);
    Point3u size = input->stack()->size();
    CImgUS image(output->data().data(), size.x(), size.y(), size.z(), 1, true);
    image.laplacian();
    output->changed();
    output->copyMetaData(input);
    return true;
  }
  REGISTER_PROCESS(CImgLaplaceStack);
}
