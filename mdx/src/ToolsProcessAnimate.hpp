//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TOOLS_PROCESS_ANIMATE_HPP
#define TOOLS_PROCESS_ANIMATE_HPP

#include <Process.hpp>
#include <Quaternion.hpp>

namespace mdx
{
  typedef std::vector<Quaternion> QuaternionVec;
  ///\addtogroup Tools Processes
  ///@{

  /**
   * \class AddKeyFrame ToolsProcessAnimate.hpp <ToolsProcessAnimate.hpp>
   *
   * Add a key frame to the animation
   */
  class mdxBase_EXPORT AnimationAddKeyFrame : public Process 
  {
  public:
    AnimationAddKeyFrame(const Process& process) : Process(process) 
    {
      setName("Tools/Animate/Add Key Frame");
      setDesc("Add a key frame to the animation");
      setIcon(QIcon(":/images/AverageStacks.png"));

      addParm("Steps", "Steps from previous frame", "100");
    }
    bool run() { return run(parm("Steps").toInt()); }
    bool run(int steps);
  };

  /**
   * \class AnimationPlay ToolsProcessAnimate.hpp <ToolsProcessAnimate.hpp>
   *
   * Play the animation
   */
  class mdxBase_EXPORT AnimationPlay : public Process 
  {
  public:
    AnimationPlay(const Process& process) : Process(process) 
    {
      setName("Tools/Animate/Play");
      setDesc("Add a key frame to the animation");
      setIcon(QIcon(":/images/Process.png"));

      addParm("Loop", "Loop animation", "No", booleanChoice());
    }
    bool run() { return run(stringToBool(parm("Loop"))); }
    bool run(bool loop);
  };

  /**
   * \class AnimationClear ToolsProcessAnimate.hpp <ToolsProcessAnimate.hpp>
   *
   * Clear the animation
   */
  class mdxBase_EXPORT AnimationClear : public Process 
  {
  public:
    AnimationClear(const Process& process) : Process(process) 
    {
      setName("Tools/Animate/Clear");
      setDesc("Add a key frame to the animation");
      setIcon(QIcon(":/images/Clear.png"));
    }
    bool run();
  };

  /**
   * \class AnimationSave ToolsProcessAnimate.hpp <ToolsProcessAnimate.hpp>
   *
   * Save the animation
   */
  class mdxBase_EXPORT AnimationSave : public Process 
  {
  public:
    AnimationSave(const Process& process) : Process(process) 
    {
      setName("Tools/Animate/Save");
      setDesc("Save the key frames to a file");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "File name to save key frames", "Animation.txt");
    }
    bool run() { return run(parm("File Name")); }
    bool run(const QString &fileName);
  };
  
  /**
   * \class AnimationLoad ToolsProcessAnimate.hpp <ToolsProcessAnimate.hpp>
   *
   * Load the animation
   */
  class mdxBase_EXPORT AnimationLoad : public Process 
  {
  public:
    AnimationLoad(const Process& process) : Process(process) 
    {
      setName("Tools/Animate/Load");
      setDesc("Load the key frames to a file");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "File name to load key frames", "Animation.txt");
    }
    bool run() { return run(parm("File Name")); }
    bool run(const QString &fileName);
  };
///@}
}

#endif
