//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef RENDER_UTILS_HPP
#define RENDER_UTILS_HPP

#include <Config.hpp>
#include <CCUtils.hpp>

namespace mdx
{
  /// Compute the bounds for heat or signal map
  // FIXME This has nothing to do with cell complexes, maybe move to another file?
  template <typename T> 
  Point2d calcBounds(AttrMap<T, double> &attr)
  {
    Point2d bounds;

    bounds[0] = std::numeric_limits<double>::max();
    bounds[1] = std::numeric_limits<double>::lowest();
    for(auto &pr : attr) {
      if(bounds[0] > pr.second)
        bounds[0] = pr.second;
      if(bounds[1] < pr.second)
        bounds[1] = pr.second;
    }
    if(bounds[0] < std::numeric_limits<float>::lowest()) {
      mdxInfo << "calcBounds Bounds smaller than min float" << endl;
      bounds[0] = std::numeric_limits<float>::lowest();
    }
    if(bounds[1] > std::numeric_limits<float>::max()) {
      mdxInfo << "calcBounds Bounds greater than max float" << endl;
      bounds[1] = std::numeric_limits<float>::max();
    }
    if(bounds[0] > bounds[1]) {
      mdxInfo << "calcBounds Bounds min cannot be greater than max, clearing" << endl;
      bounds[0] = bounds[1] = 0;
    }
    if(isNan(bounds[0])) {
      mdxInfo << "calcBounds Bounds min nan, clearing" << endl;
      bounds[0] = 0;
    }
    if(isNan(bounds[1])) {
      mdxInfo << "calcBounds Bounds max nan, clearing" << endl;
      bounds[1] = 0;
    }
    return bounds;
  }

  // Used for cell axis
  template <typename T> 
  double axisBounds(AttrMap<T, SymmetricTensor> &attr)
  {
    Point2d bounds;

    bounds[0] = std::numeric_limits<double>::max();
    bounds[1] = std::numeric_limits<double>::lowest();
    for(auto &pr : attr)
      for(int i = 0; i < 3; i++) {
        if(bounds[0] > pr.second.evals()[i])
          bounds[0] = pr.second.evals()[i];
        if(bounds[1] < pr.second.evals()[i])
          bounds[1] = pr.second.evals()[i];
      }
    if(bounds[0] < std::numeric_limits<float>::lowest()) {
      mdxInfo << "axisBounds Bounds smaller than min float" << endl;
      bounds[0] = std::numeric_limits<float>::lowest();
    }
    if(bounds[1] > std::numeric_limits<float>::max()) {
      mdxInfo << "axisBounds Bounds greater than max float" << endl;
      bounds[1] = std::numeric_limits<float>::max();
    }
    if(bounds[0] > bounds[1]) {
      mdxInfo << "axisBounds Bounds min cannot be greater than max, clearing" << endl;
      bounds[0] = bounds[1] = 0;
    }
    if(isNan(bounds[0])) {
      mdxInfo << "axisBounds Bounds min nan, clearing" << endl;
      bounds[0] = 0;
    }
    if(isNan(bounds[1])) {
      mdxInfo << "axisBounds Bounds max nan, clearing" << endl;
      bounds[1] = 0;
    }
    // Return the max absolute value of the eigenvalues
    return std::max(fabs(bounds[0]), fabs(bounds[1]));
  }

  /// Update the normal of a vertex
  mdx_EXPORT bool updateVertexNormal(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex v);
  mdx_EXPORT bool updateVertexNormals(const CCStructure &cs, CCIndexDataAttr &indexAttr);

  mdx_EXPORT bool updateFaceGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex f);
  mdx_EXPORT bool updateFaceGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr);

  mdx_EXPORT bool updateGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr);

  mdx_EXPORT bool updateVolumeGeometry(const CCStructure &cs, CCIndexDataAttr &indexAttr);

  mdx_EXPORT bool faceAttrFromVertices(const CCStructure &cs, CCIndexDoubleAttr &indexAttr, const CCIndexVec &faces);
  mdx_EXPORT bool faceAttrFromVertices(Mesh &mesh, const CCStructure &cs, const CCIndexVec &faces);
  mdx_EXPORT bool faceAttrToVertices(const CCStructure &cs, CCIndexDoubleAttr &indexAttr, const CCIndexVec &vertices);
  mdx_EXPORT bool faceAttrToVertices(Mesh &mesh, const CCStructure &cs, const CCIndexVec &vertices);

  mdx_EXPORT bool vertexLabelsFromVolumes(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &vertices);

  mdx_EXPORT bool faceLabelsFromVertices(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &faces);
  mdx_EXPORT bool vertexLabelsFromFaces(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &vertices);

  /// Set face labels 
  mdx_EXPORT bool faceLabelsFromVolumes(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &faces, IntIntAttr *labelMap = 0);

  /// Return active vertices, selected or all if none selected 
  mdx_EXPORT CCIndexVec activeVertices(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
  /// Return active edges, those with both vertices selected or all if none selected 
  mdx_EXPORT CCIndexVec activeEdges(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
  /// Return active faces selected or all if none selected 
  mdx_EXPORT CCIndexVec activeFaces(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
  /// Return active volumes selected or all if none selected 
  mdx_EXPORT CCIndexVec activeVolumes(const CCStructure &cs, const CCIndexDataAttr &indexAttr);

  /// Return CCIndex Vec of selected vertices
  mdx_EXPORT CCIndexVec selectedVertices(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
  /// Return CCIndex Vec of selected edges, with both vertices selected
  mdx_EXPORT CCIndexVec selectedEdges(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
  /// Return CCIndex Vec of selected faces
  mdx_EXPORT CCIndexVec selectedFaces(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
  /// Return CCIndex Vec of selected volumes
  mdx_EXPORT CCIndexVec selectedVolumes(const CCStructure &cs, const CCIndexDataAttr &indexAttr);
  /// Return CCIndex Vec of selected cells
  mdx_EXPORT CCIndexVec selectedCells(const CCStructure &cs, const CCIndexDataAttr &indexAttr);

  /// Helper function to set draw parms when creating new CC from old
  bool ccUpdateDrawParms(Mesh &mesh, const QString &ccIn, const QString &ccOut);

  /// Create a heat map from a cell axis tensor
  bool heatFromCellAxis(const QString &display, const IntSymTensorAttr &cellAxisAttr, IntDoubleAttr &heatAttr);

  /// Find centers and normals for axis data
  bool calcCellAxisCentersNormals(const CCStructure &cs, const CCIndexDataAttr &indexAttr, IntIntAttr *labelMap, IntMatrix2x3dAttr &centerNormalAttr);

  /// Find the labels in a mesh from faces
  mdx_EXPORT IntSet faceLabels(const CCStructure &cs, const CCIndexDataAttr &indexAttr, bool selected = false, bool zero = false);
  /// Find the labels in a mesh from volumes
  mdx_EXPORT IntSet volumeLabels(const CCStructure &cs, const CCIndexDataAttr &indexAttr, bool selected = false, bool zero = false);
}

#endif

