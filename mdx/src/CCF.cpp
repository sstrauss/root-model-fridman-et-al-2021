/*
 * The Cell Complex Framework
 * Brendan Lane 2016
 */

#include "CCIndex.hpp"
#include "CCF.hpp"
extern int cmds[];

namespace mdx {
namespace ccf {

// Get flips with a single index
template<typename Index, typename FlipMap>
const tbb::concurrent_vector<uint, TBBALLOC<uint> > &getFlips(const FlipMap &map, Index key)
{
  static tbb::concurrent_vector<uint, TBBALLOC<uint> > empty;
  auto it = map.find(key);
  if(it != map.end())
    return it->second.get();
  return empty; 
}

// Get flips from two indices, choose smallest
template<typename Index, typename FlipMap>
const tbb::concurrent_vector<uint, TBBALLOC<uint> > &getFlips(const FlipMap &map, Index key1, Index key2)
{
  static tbb::concurrent_vector<uint, TBBALLOC<uint> > empty;
  auto it1 = map.find(key1);
  auto it2 = map.find(key2);
  if(it1 == map.end()) {
    if(it2 != map.end())
      return it2->second.get();
  } else {
    if(it2 == map.end())
      return it1->second.get();
    if(it1->second.size() < it2->second.size())
      return it1->second.get();
    else
      return it2->second.get();
  }
  return empty; 
}

template<typename Index>
uint FlipTable<Index>::findIndex(const FlipI &flip)
{
  // See if flip exists and finds its location
  uint pos = flipVec.size();
  for(uint i : getFlips(flipMap, flip.facet[0], flip.facet[1])) {
    auto f = flipVec[i];
    if(f.facet[0] == flip.facet[0] and f.facet[1] == flip.facet[1] and
        f.joint == flip.joint and f.interior == flip.interior) {
      pos = i;
      break;
    }
  }
  return pos;
}

template<typename Index>
typename FlipTable<Index>::const_iterator FlipTable<Index>::find(const FlipI &flip)
{
  uint pos = findIndex(flip);
  if(pos == flipVec.size())
    return flipVec.end();
  return flipVec.begin() + pos;
}

// FlipTable match method definitions 
// This is almost the same as matchFirst, is there a way to combine them?
template<typename Index>
typename std::vector< Flip<Index> > FlipTable<Index>::matchV(Flip<Index> query) const
{
  /* Ensure that facet 0 is not \QUERY, if possible */
  if((query.facet[0] == Index::Q or query.facet[0] == Index::INFTY) and query.facet[1] != Index::Q) 
    std::swap(query.facet[0], query.facet[1]);

  /* Create mask based on non-\QUERY{} values */
  unsigned char givenMask = 0;
  if(query.interior != Index::Q) givenMask = givenMask | 0x1;
  if(query.joint != Index::Q) givenMask = givenMask | 0x2;
  if(query.facet[0] != Index::Q) givenMask = givenMask | 0x4;
  if(query.facet[1] != Index::Q) givenMask += 4;

  const_cast<int&>(cmds[givenMask])++;
  std::vector< Flip<Index> > answer;
  switch(givenMask)
  {
    /* Create answer based on mask */
    case 0: { // all query, return everything
      for(auto &flip : flipVec)
        answer.emplace_back(flip);
      break;
    }
    case 1: { // interior only
      for(uint i : getFlips(flipMap, query.interior))
        if(flipVec[i].interior == query.interior)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 2: { // joint only
      for(uint i : getFlips(flipMap, query.joint))
        if(flipVec[i].joint == query.joint)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 3: { // interior + joint 
      for(uint i : getFlips(flipMap, query.interior, query.joint))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 4: { // only facet
      for(uint i : getFlips(flipMap, query.facet[0]))
        if(flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0])
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 5: { // facet + interior
      for(uint i : getFlips(flipMap, query.interior, query.facet[0]))
        if(flipVec[i].interior == query.interior and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 6: { // facet + joint
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 7: { // facet + joint + interior
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 8: { // two facets
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
           (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0]))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 9: { // two facets + interior
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].interior == query.interior)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 10: { // two facets + joint
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].joint == query.joint)
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 11: { // all given
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
           ((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])))
          answer.emplace_back(flipVec[i]);
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
  }
  return answer;
}

template<typename Index>
const Flip<Index> &FlipTable<Index>::matchFirst(Flip<Index> query) const
{
  static const Flip<Index> InvalidFlip(Index::UNDEF);

  /* Ensure that facet 0 is not \QUERY or \INFTY, if possible */
  if((query.facet[0] == Index::Q or query.facet[0] == Index::INFTY) and query.facet[1] != Index::Q) 
    std::swap(query.facet[0], query.facet[1]);

  /* Create mask based on non-\QUERY{} values */
  unsigned char givenMask = 0;
  if(query.interior != Index::Q) givenMask = givenMask | 0x1;
  if(query.joint != Index::Q) givenMask = givenMask | 0x2;
  if(query.facet[0] != Index::Q) givenMask = givenMask | 0x4;
  if(query.facet[1] != Index::Q) givenMask += 4;

  const_cast<int &>(cmds[givenMask])++;
  switch(givenMask)
  {
    /* Create answer based on mask */
    case 0: { // all query, return everything
      for(auto &flip : flipVec)
        return flip;
      break;
    }
    case 1: { // interior only
      for(uint i : getFlips(flipMap, query.interior))
        if(flipVec[i].interior == query.interior)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 2: { // joint only
      for(uint i : getFlips(flipMap, query.joint))
        if(flipVec[i].joint == query.joint)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 3: { // interior + joint 
      for(uint i : getFlips(flipMap, query.interior, query.joint))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 4: { // only facet
      for(uint i : getFlips(flipMap, query.facet[0]))
        if(flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0])
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 5: { // facet + interior
      for(uint i : getFlips(flipMap, query.interior, query.facet[0]))
        if(flipVec[i].interior == query.interior and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 6: { // facet + joint
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 7: { // facet + joint + interior
      for(uint i : getFlips(flipMap, query.joint, query.facet[0]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
              (flipVec[i].facet[0] == query.facet[0] or flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 8: { // two facets
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
           (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0]))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 9: { // two facets + interior
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].interior == query.interior)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 10: { // two facets + joint
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])) and
             flipVec[i].joint == query.joint)
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
    case 11: { // all given
      for(uint i : getFlips(flipMap, query.facet[0], query.facet[1]))
        if(flipVec[i].interior == query.interior and flipVec[i].joint == query.joint and
           ((flipVec[i].facet[0] == query.facet[0] and flipVec[i].facet[1] == query.facet[1]) or
            (flipVec[i].facet[0] == query.facet[1] and flipVec[i].facet[1] == query.facet[0])))
          return flipVec[i];
        else
          const_cast<int&>(loops[givenMask])++;
      break;
    }
  }
  return InvalidFlip;
}

/* CellStructure helpers */
/* Helper class DisjointSet */
template <typename Index>
struct DisjointSet
{
  /* DisjointSet type definitions */
  typedef std::pair<Index,unsigned int> DisjointData;
  typedef std::map<Index,DisjointData> DisjointMap;
  typedef typename DisjointMap::iterator DisjointIterator;
  
  /* DisjointSet member variables */
  DisjointMap sets;
  
  /* DisjointSet FindSet of iterator */
  Index FindSet(DisjointIterator iter)
  {
    if(iter->second.first != iter->first)
      iter->second.first = FindSet(iter->second.first);
    return iter->second.first;
  }
  
  /* DisjointSet Link */
  void Link(DisjointIterator it1, DisjointIterator it2)
  {
    unsigned int &rank1 = it1->second.second, &rank2 = it2->second.second;
    if(rank1 > rank2) it2->second.first = it1->first;
    else if(rank1 < rank2) it1->second.first = it2->first;
    else { it1->second.first = it2->first; rank2++; }
  }
  
public:
  /* DisjointSet MakeSet */
  void MakeSet(Index t)
  { if(sets.count(t) == 0) sets[t] = std::make_pair(t,0); }
  
  /* DisjointSet FindSet of Index */
  Index FindSet(Index t)
  {
    DisjointIterator iter = sets.find(t);
    if(iter == sets.end()) { MakeSet(t); return t; }
    else return FindSet(iter);
  }
  
  /* DisjointSet Union */
  void Union(Index t1, Index t2)
  { Link(sets.find(FindSet(t1)), sets.find(FindSet(t2))); }
  
  /* DisjointSet Components */
  std::set<Index> Components(void)
  {
    std::set<Index> components;
    for(DisjointIterator iter = sets.begin() ; iter != sets.end() ; iter++)
      components.insert(FindSet(iter));
    return components;
  }
  
  /* DisjointSet CountComponents */
  unsigned int CountComponents(void)
  {
    return Components().size();
  }
};
 
/* Helper function greatestCommonElement */
template<typename Index>
typename std::set<Index>::const_iterator
greatestCommonElement(const std::set<Index>& set1,
                      const std::set<Index>& set2)
// Adapted from http://stackoverflow.com/a/1964252
{
  if(set1.empty() || set2.empty()) return set1.end();
  
  typename std::set<Index>::const_reverse_iterator
    it1 = set1.rbegin(), it1End = set1.rend(),
    it2 = set2.rbegin(), it2End = set2.rend();
  
  if(*it1 < *set2.begin() || *it2 < *set1.begin())
    return set1.end();
  
  while(it1 != it1End && it2 != it2End)
  {
    if(*it1 == *it2) return (--it1.base());
    if(*it1 < *it2) { it2++; }
    else { it1++; }
  }
  
  return set1.end();
}

/* CellStructure method definitions */
/* CellStructure buildBoundary method */
template<typename Index>
bool CellStructure<Index>::buildBoundary
  (Index newCell, typename CellStructure<Index>::BoundaryChain cellBoundary,
   std::vector< Flip<Index> >& addFlips,
   std::vector< Flip<Index> >& delFlips, RO topRO) const
{
  /* Check that boundary dimension is consistent */
  Dimension boundaryDim = maxDimension();
  for(BoundaryIterator iter = cellBoundary.begin() ; iter != cellBoundary.end() ; iter++)
  {
    Index cell = ~(*iter);
    /* Check validity of boundary cell */
    if(cell.isPseudocell())
    {
      error = std::string("buildBoundary: Supplied boundary includes pseduocell ")+
        to_string(cell);
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
    if(!hasCell(cell))
    {
      error = std::string("buildBoundary: Supplied boundary cell ")+
        to_string(cell)+" is not in the complex.";
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }


    if(iter == cellBoundary.begin())
      boundaryDim = dimensionOf(cell);
    else if(dimensionOf(cell) != boundaryDim)
    {
      error = std::string("buildBoundary: One boundary cell ")+
        to_string(~(*(cellBoundary.begin())))+" has dimension "+
        to_string(boundaryDim)+" while another boundary cell "+to_string(cell)+
        " has dimension "+to_string(dimensionOf(cell));
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
  }


  if(boundaryDim == 0)
  {
    /* Build boundary for a new edge */
    /* Get boundary vertices of new edge */
    if(cellBoundary.size() != 2)
    {
      error = std::string("buildBoundary: Boundary supplied for edge has ")+
        to_string(cellBoundary.size())+" vertices, not 2.";
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
    OrientedCell ov[2] = { *(cellBoundary.begin()), *(++(cellBoundary.begin())) };
    if(ov[0].orientation() == ov[1].orientation())
    {
      error = std::string("buildBoundary: Boundary vertices ")+to_string(ov[0])+" and "+
        to_string(ov[1])+" are of the same orientation.";
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
    if(ov[0].orientation() == NEG) std::swap(ov[0],ov[1]);

    /* Add new flip with new edge in interior position */
    addFlips.push_back(FlipI(Index::BOTTOM, ~ov[0], ~ov[1], newCell));


    if(maxDimension() == 1)
    {
      /* Handle \TOP{} flips for a new edge in a 1-dimensional cell complex */
      for(int i = 0 ; i < 2 ; i++)
      {
        Index vertex = ~(ov[i]);
        RO totalOrientation = ov[i].orientation() * topRO;
        FlipI flip = matchFirst(vertex,Index::Q,Index::Q,Index::TOP);
        if(flip.isValid())
        {
          /* Replace \INFTY{} with new edge in flip */
          delFlips.push_back(flip);
          flip *= totalOrientation;
          if(flip.facet[0] != Index::INFTY)
          {
            if(flip.facet[1] == Index::INFTY)
              error = std::string("buildBoundary: Orientation mismatch between edges ")+
                to_string(newCell)+" and "+to_string(flip.facet[0])+" around vertex "+
                to_string(vertex);
            else
              error = std::string("buildBoundary: Trying to add a third edge ")+
                to_string(newCell)+" to vertex "+to_string(vertex)+
                " in a one-dimensional complex.";
            mdxWarning << QString::fromStdString(error) << endl;
            return false;
          }
          else flip.facet[0] = newCell;
          flip *= totalOrientation;
          addFlips.push_back(flip);

        }
        else
        {
          /* Add \INFTY{} flip for the first edge on vertex */
          addFlips.push_back(totalOrientation * FlipI(vertex,newCell,Index::INFTY,Index::TOP));

        }
      }

    }

  }
  else
  {
    /* Build boundary for a new cell of dimension greater than 1 */
    /* Create data structures to store information about new cell */
    std::map<Index,FlipI> byJoint;
    DisjointSet<Index> bComponents;
    std::set<Index> inftyIncident;

    for(BoundaryIterator ofacet = cellBoundary.begin() ;
        ofacet != cellBoundary.end() ; ofacet++)
    {
      Index facet = ~(*ofacet);
      /* Register a new component containing this facet */
      bComponents.MakeSet(facet);

      BoundaryChain facetBoundary = boundary(*ofacet);
      for(BoundaryIterator ojoint = facetBoundary.begin() ;
          ojoint != facetBoundary.end() ; ojoint++)
      {
        /* Process a joint in the facet's own boundary */
        Index joint = ~(*ojoint);
        typename std::map<Index,FlipI>::iterator findJoint = byJoint.find(joint);
        if(findJoint == byJoint.end())
          /* Create a new flip with this joint and one unknown facet */
          byJoint[joint] = (*ojoint).orientation() * FlipI(joint,facet,Index::UNDEF,newCell);

        else
        {
          /* Replace \UNDEF{} with this facet in the flip for this joint */
          FlipI &flip = findJoint->second;
          flip *= (*ojoint).orientation();

          if(flip.facet[0] == Index::UNDEF) flip.facet[0] = facet;
          else
          {
            if(flip.facet[1] == Index::UNDEF)
              error = std::string("buildBoundary: Orientation mismatch in boundary ")+
                "between facets "+to_string(facet)+" and "+to_string(flip.facet[0])+
                " around joint "+to_string(joint);
            else
              error = std::string("buildBoundary: Boundary joint ")+to_string(joint)+
                " adjoins three facets "+to_string(facet)+", "+to_string(flip.facet[0])+
                ", and "+to_string(flip.facet[1]);
            mdxWarning << QString::fromStdString(error) << endl;
            return false;
          }

          flip *= (*ojoint).orientation();
          addFlips.push_back(flip);

          /* Register that these two facets are in the same connected component */
          bComponents.Union(flip.facet[0],flip.facet[1]);
        }
      }

      if(boundaryDim == maxDimension()-1)
      {
        /* Handle $n$-flips for a new $n$-cell in an $n$-dimensional cell complex */
        RO totalOrientation = (*ofacet).orientation() * topRO;
        FlipI flip = matchFirst(facet,Index::Q,Index::Q,Index::TOP);
        if(flip.isValid())
        {
          /* Replace \INFTY{} with new cell in flip */
          delFlips.push_back(flip);
          flip *= totalOrientation;
          if(flip.facet[0] != Index::INFTY)
          {
            if(flip.facet[1] == Index::INFTY)
              error = std::string("buildBoundary: Orientation mismatch between cells ")+
                to_string(newCell)+" and "+to_string(flip.facet[0])+" around facet "+
                to_string(facet);
            else
              error = std::string("buildBoundary: Trying to add a third max-dimensional cell ")
                +to_string(newCell)+" to (n-1)-cell "+to_string(facet);
            mdxWarning << QString::fromStdString(error) << endl;
            return false;
          }
          else flip.facet[0] = newCell;
          flip *= totalOrientation;
          addFlips.push_back(flip);

        }
        else
        {
          /* Add \INFTY{} flip for the first $n$-cell on facet */
          addFlips.push_back(totalOrientation * FlipI(facet,newCell,Index::INFTY,Index::TOP));
          inftyIncident.insert(facet);
        }
      }
    }

    /* Check that the boundary is in no more than one piece */
    if(bComponents.CountComponents() > 1)
    {
      error = std::string("buildBoundary: Supplied boundary is in ")+
        to_string(bComponents.CountComponents())+" disjoint components";
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }

    /* Check that the boundary is closed */
    for(typename std::map<Index,FlipI>::const_iterator iter = byJoint.begin() ;
        iter != byJoint.end() ; iter++)
      if(iter->second.facet[0] == Index::UNDEF || iter->second.facet[1] == Index::UNDEF)
      {
        error = std::string("buildBoundary: Boundary joint ")+to_string(iter->first)+
          " has only one facet "+to_string(iter->second.otherFacet(Index::UNDEF));
        mdxWarning << QString::fromStdString(error) << endl;
        return false;
      }

    if(boundaryDim == maxDimension()-1)
    {
      /* Handle \INFTY{} $(n-1)$-flips involving the boundary cells */
      for(typename std::map<Index,FlipI>::const_iterator iter = byJoint.begin() ;
          iter != byJoint.end() ; iter++)
      {
        FlipI flip = iter->second;
        /* Create \INFTY{} flip mirroring existing flip */
        flip.interior = Index::INFTY;
        flip *= -topRO;


        bool externalFacet[2];
        /* Find out which facets are incident on \INFTY{} */
        for(int i = 0 ; i < 2 ; i++)
          externalFacet[i] = (inftyIncident.count(flip.facet[i]) > 0);

        /* Add new flip if both facets are incident on \INFTY{} */
        if(externalFacet[0] && externalFacet[1])
        {
          addFlips.push_back(flip);
          continue;
        }


        for(int i = 0 ; i < 2 ; i++)
        {
          if(!(externalFacet[i]))
          {
            /* Remove old \INFTY{} flip for non-external facet i */
            FlipI flipM = matchFirst(flip.joint,flip.facet[i],Index::Q,Index::INFTY);
            if(flipM.isValid())
            {
              flip.facet[i] = flipM.otherFacet(flip.facet[i]);
              delFlips.push_back(flipM);
            }
            else
              throw std::runtime_error
                ("buildBoundary: No INFTY flips in unlifted space");
          }
          /* Check if we've removed the last \INFTY{} flip on this joint */
          if(flip.facet[0] == flip.facet[1]) break;

        }
        /* Add back an \INFTY{} flip if we have any \INFTY{} left */
        if(flip.facet[0] != flip.facet[1]) addFlips.push_back(flip);

      }

    }

  }
  return true;
}

/* CellStructure boundUp method */
template<typename Index>
void CellStructure<Index>::boundUp
  (const std::set<Index>& cells, std::set<Index>& up1) const
{
  for(typename std::set<Index>::iterator icell = cells.begin() ;
      icell != cells.end() ; icell++)
  {
    FlipVectorI matches = matchV(Index::Q,*icell,Index::Q,Index::Q);
    for(FlipVectorIterator iflip = matches.begin() ;
        iflip != matches.end() ; iflip++)
      up1.insert(iflip->interior);
  }
}

/* CellStructure boundDown method */
template<typename Index>
void CellStructure<Index>::boundDown
  (const std::set<Index>& cells,
   std::set<Index>& down1, std::set<Index>& down2) const
{
  for(typename std::set<Index>::iterator icell = cells.begin() ;
      icell != cells.end() ; icell++)
  {
    FlipVectorI matches = matchV(Index::Q,Index::Q,Index::Q,*icell);
    for(FlipVectorIterator iflip = matches.begin() ;
        iflip != matches.end() ; iflip++)
    {
      down1.insert(iflip->facet[0]);
      down1.insert(iflip->facet[1]);
      down2.insert(iflip->joint);
    }
  }
}


/* CellStructure reverseOrientation operation */
template<typename Index>
bool CellStructure<Index>::reverseOrientation(Index cell)
{
  /* Check validity of cell for orientation reversion */
  if(cell.isPseudocell())
  {
    error = std::string("Cannot reverse orientation of pseudocell ")+
      to_string(cell)+".";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(!hasCell(cell))
  {
    error = std::string("Cannot reverse orientation of cell ")+
      to_string(cell)+" which is not in the cell complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  
  if(dimensionOf(cell) == 0)
  {
    error = std::string("Cannot reverse orientation of vertex ")+
      to_string(cell)+".";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  /* Reverse flips with cell in joint and interior position */
  FlipVectorI flipsToReverse = matchV(Index::Q,Index::Q,Index::Q,cell);
  FlipVectorI flipsJ = matchV(cell,Index::Q,Index::Q,Index::Q);
  flipsToReverse.insert(flipsToReverse.end(),flipsJ.begin(),flipsJ.end());
  for(FlipVectorIterator iter = flipsToReverse.begin() ;
      iter != flipsToReverse.end() ; iter++)
  {
    flips.erase(*iter);
    flips.insert(-(*iter));
  }

  return true;
}

/* CellStructure orientedEdge method */
template <typename Index>
typename CellStructure<Index>::OrientedCell
CellStructure<Index>::orientedEdge(Index c1, Index c2) const
{
  std::vector<FlipI> matches = matchV(Index::Q,c1,c2,Index::Q);
  for(unsigned int i = 0 ; i < matches.size() ; i++)
  {
    RO ornt1 = ro(c1,matches[i].interior), ornt2 = ro(c2,matches[i].interior);
    if(ornt1 != ornt2)
    {
      if(ornt1 == POS) return +(matches[i].interior);
      else return -(matches[i].interior);
    }
  }
  return OrientedCell(Index::UNDEF);
}

/* CellStructure orientedFacet method */
template <typename Index>
typename CellStructure<Index>::OrientedCell
CellStructure<Index>::orientedFacet(Index c1, Index c2) const
{
  std::vector<FlipI> matches = matchV(Index::Q,c1,c2,Index::Q);
  for(unsigned int i = 0 ; i < matches.size() ; i++)
  {
    RO ornt1 = ro(c1,matches[i].joint), ornt2 = ro(c2,matches[i].joint);
    if(ornt1 != ornt2)
    {
      if(ornt1 == POS) return +(matches[i].joint);
      else return -(matches[i].joint);
    }
  }
  return OrientedCell(Index::UNDEF);
}

/* CellStructure meet operation */
template<typename Index>
Index CellStructure<Index>::meet(Index c1, Index c2) const
{
  /* Check for \TOP\ special cases in meet */
  if(c1 == Index::TOP && (c2 == Index::TOP || hasCell(c2))) return c2;
  if(c2 == Index::TOP && hasCell(c1)) return c1;
  
  /* Check cells for existence in meet */
  if(!hasCell(c1))
    throw std::domain_error
      (std::string("Nonexistent cell ")+to_string(c1)+" passed to meet");
  if(!hasCell(c2))
    throw std::domain_error
      (std::string("Nonexistent cell ")+to_string(c2)+" passed to meet");

  /* Check for special cases in meet */
  if(c1 == Index::BOTTOM || c2 == Index::BOTTOM) return Index::BOTTOM;

  if(c1 == c2) return c1;

  Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
  if(dim1 == 0 && dim2 == 0) return Index::BOTTOM;
  if(dim1 == dim2)
  {
    FlipI flipM = matchFirst(Index::Q,c1,c2,Index::Q);
    if(flipM.isValid()) return flipM.joint;
  }

  /* Standardize dimensions of query cells in meet/join */
  Index cLow = c1, cHigh = c2;
  Dimension dimLow = dim1, dimHigh = dim2;
  if(dimHigh < dimLow) { std::swap(cHigh,cLow); std::swap(dimHigh,dimLow); }

  /* Create lists of bounding cells in meet/join */
  std::vector< std::set<Index> > boundLow(maxDimension()+1), boundHigh(maxDimension()+1);
  { std::set<Index> cLowSet; cLowSet.insert(cLow); boundLow[dimLow] = cLowSet; }
  { std::set<Index> cHighSet; cHighSet.insert(cHigh); boundHigh[dimHigh] = cHighSet; }
  int dLow = dimLow, dHigh = dimHigh;

  /* Work downward from dimHigh to dimLow in meet */
  while(dHigh > dLow)
  {
    /* Iterate downward two steps in boundHigh */
    std::set<Index> down1, down2;
    boundDown(boundHigh[dHigh],down1,down2);
    boundHigh[dHigh-1] = down1;
    if(dHigh != 1) boundHigh[dHigh-2] = down2;
    dHigh -= 2;

  }
  if(boundHigh[dimLow].count(cLow) > 0) return cLow;

  /* Work downward from dimLow in meet */
  int d = dimLow;
  while(d > 0)
  {
    if(dLow >= d)
    {
      /* Iterate downward two steps in boundLow */
      std::set<Index> down1, down2;
      boundDown(boundLow[dLow],down1,down2);
      boundLow[dLow-1] = down1;
      if(dLow != 1) boundLow[dLow-2] = down2;
      dLow -= 2;

    }
    if(dHigh >= d)
    {
      /* Iterate downward two steps in boundHigh */
      std::set<Index> down1, down2;
      boundDown(boundHigh[dHigh],down1,down2);
      boundHigh[dHigh-1] = down1;
      if(dHigh != 1) boundHigh[dHigh-2] = down2;
      dHigh -= 2;

    }

    while(d > std::max(dLow,dHigh))
    {
      d -= 1;
      if(d < 0) return Index::BOTTOM;
      /* Check for common element of boundLow[d] and boundHigh[d] */
      typename std::set<Index>::iterator common =
        greatestCommonElement(boundLow[d],boundHigh[d]);
      if(common != boundLow[d].end()) return *common;

    }
  }

  return Index::BOTTOM;
}

/* CellStructure join operation */
template<typename Index>
Index CellStructure<Index>::join(Index c1, Index c2) const
{
  /* Check for \TOP\ special cases in join */
  if(c1 == Index::TOP || c2 == Index::TOP) return Index::TOP;
  
  /* Check cells for existence in join */
  if(!hasCell(c1))
    throw std::domain_error
      (std::string("Nonexistent cell ")+to_string(c1)+" passed to join");
  if(!hasCell(c2))
    throw std::domain_error
      (std::string("Nonexistent cell ")+to_string(c2)+" passed to join");

  /* Check for special cases in join */
  if(c1 == Index::BOTTOM) return c2;
  if(c2 == Index::BOTTOM) return c1;

  if(c1 == c2) return c1;

  Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
  if(dim1 == maxDimension() && dim2 == maxDimension()) return Index::TOP;
  if(dim1 == dim2)
  {
    FlipVectorI matches = matchV(Index::Q,c1,c2,Index::Q);
    if(!matches.empty())
    {
      FlipVectorIterator iter = matches.begin();
      while(iter != matches.end() && iter->interior.isPseudocell()) iter++;
      if(iter == matches.end()) return Index::INFTY;
      return iter->interior;
    }
  }

  /* Standardize dimensions of query cells in meet/join */
  Index cLow = c1, cHigh = c2;
  Dimension dimLow = dim1, dimHigh = dim2;
  if(dimHigh < dimLow) { std::swap(cHigh,cLow); std::swap(dimHigh,dimLow); }

  /* Create lists of bounding cells in meet/join */
  std::vector< std::set<Index> > boundLow(maxDimension()+1), boundHigh(maxDimension()+1);
  { std::set<Index> cLowSet; cLowSet.insert(cLow); boundLow[dimLow] = cLowSet; }
  { std::set<Index> cHighSet; cHighSet.insert(cHigh); boundHigh[dimHigh] = cHighSet; }
  int dLow = dimLow, dHigh = dimHigh;

  /* Work upward from dimLow to dimHigh in join */
  while(dLow < dHigh)
  {
    /* Iterate upward one step in boundLow */
    std::set<Index> up1;
    boundUp(boundLow[dLow],up1);
    boundLow[dLow+1] = up1;
    dLow += 1;
  }
  if(boundLow[dimHigh].count(cHigh) > 0) return cHigh;

  /* Work upward from dimHigh in join */
  int d = dimHigh;
  while(d < maxDimension())
  {
    if(dLow <= d)
    {
      /* Iterate upward one step in boundLow */
      std::set<Index> up1;
      boundUp(boundLow[dLow],up1);
      boundLow[dLow+1] = up1;
      dLow += 1;
    }
    if(dHigh <= d)
    {
      /* Iterate upward one step in boundHigh */
      std::set<Index> up1;
      boundUp(boundHigh[dHigh],up1);
      boundHigh[dHigh+1] = up1;
      dHigh += 1;
    }

    while(d < std::min(dLow,dHigh))
    {
      d += 1;
      if(d > maxDimension()) return Index::TOP;
      /* Check for common element of boundLow[d] and boundHigh[d] */
      typename std::set<Index>::iterator common =
        greatestCommonElement(boundLow[d],boundHigh[d]);
      if(common != boundLow[d].end()) return *common;

    }
  }

  return Index::TOP;
}

/* CellStructure coincidentCells operation */
template<typename Index>
std::set<Index> CellStructure<Index>::coincidentCells
  (Index c1, Index c2, CellStructure<Index>::Dimension dim) const
{
  std::set<Index> incident1 = incidentCells(c1,dim);
  std::set<Index> incident2 = incidentCells(c2,dim);
  std::set<Index> answer;
  std::set_intersection(incident1.begin(),incident1.end(),
                        incident2.begin(),incident2.end(),
                        std::inserter(answer,answer.begin()));
  return answer;
}

/* CellStructure addCell operation */
template<typename Index>
bool CellStructure<Index>::addCell(Index newCell,
                                   CellStructure<Index>::BoundaryChain boundary,
                                   RO topRO)
{
  /* Check validity of new index in addCell */
  if(newCell.isPseudocell())
  {
    error = std::string("addCell: Cannot add new cell ")+
      to_string(newCell)+" as it is a pseudocell.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  else if(hasCell(newCell))
  {
    error = std::string("addCell: Cannot add new cell ")+
      to_string(newCell)+" as the cell already exists in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  if(boundary.empty())
  {
    /* Add a new vertex with index newCell */
    dimensionMap.insert(newCell,0);

    return true;
  }

  /* Add a new cell with nonempty boundary */
  FlipVectorI addFlips, delFlips;
  if(buildBoundary(newCell,boundary,addFlips,delFlips,topRO))
  {
    /* Remove flips in delFlips from flips */
    for(const auto &flip : delFlips)
      flips.erase(flip);

    /* Add flips in addFlips to flips */
    flips.insert(addFlips.begin(),addFlips.end());

    /* Record the dimension of the new cell */
    Index firstBoundaryCell = ~(*(boundary.begin()));
    dimensionMap.insert(newCell,dimensionOf(firstBoundaryCell) + 1);
    return true;
  }
  else return false;
}

/* CellStructure \operation{deleteCell} operation */
template<typename Index>
bool CellStructure<Index>::deleteCell(Index oldCell)
{
  /* Check that cell to be deleted exists */
  if(oldCell.isPseudocell())
  {
    error = std::string("deleteCell: Cannot remove cell ")+to_string(oldCell)+
      " as it is a pseudocell.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(!hasCell(oldCell))
  {
    error = std::string("deleteCell: Cannot remove cell ")+to_string(oldCell)+
      " as it is not in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  /* Check that cell to be deleted is not in any boundary */
  if(!cobounds(oldCell).empty())
  {
    std::set<Index> cob = cobounds(oldCell);
    error = std::string("deleteCell: Cannot remove cell ")+to_string(oldCell)+
      " as it is in the boundary of "+to_string(cob.size())+" cells, including "+
      to_string(*(cob.begin()));
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  FlipVectorI addFlips,delFlips;

  /* Find all flips which involve the cell to be deleted */
  FlipVectorI interiorFlips = matchV(Index::Q,Index::Q,Index::Q,oldCell);
  FlipVectorI    facetFlips = matchV(Index::Q,Index::Q,oldCell,Index::Q);
  delFlips.insert(delFlips.end(),interiorFlips.begin(),interiorFlips.end());
  delFlips.insert(delFlips.end(),facetFlips.begin(),facetFlips.end());

  Dimension dim = dimensionOf(oldCell);
  if(dim == maxDimension())
  {
    /* Declare list of interior bounding cells of oldCell */
    std::set<Index> insideFacets;

    /* Add in new $n$-flips across formerly internal boundaries */
    for(FlipVectorIterator iter = facetFlips.begin() ;
        iter != facetFlips.end() ; iter++)
    {
      if(iter->otherFacet(oldCell) != Index::INFTY)
      {
        addFlips.push_back(iter->replaceIndex(oldCell,Index::INFTY));
        /* Register bounding cell being inside the cell complex */
        insideFacets.insert(iter->joint);
      }
    }

    /* Handle $(n-1)$-flips when deleting a cell of dimension $n$ */
    if(maxDimension() > 1)
    {
      for(FlipVectorIterator iter = interiorFlips.begin() ; iter != interiorFlips.end() ; iter++)
      {
        /* Create mirrored \INFTY{} $(n-1)$-flip by replacing oldCell */
        FlipI flip = ro(oldCell,Index::TOP) *
          iter->replaceIndex(oldCell,Index::INFTY);

        /* Determine which facets are interior */
        std::map<Index,bool> inside;
        for(int i = 0 ; i < 2 ; i++)
          inside[flip.facet[i]] = (insideFacets.count(flip.facet[i]) > 0);

        if(inside[flip.facet[0]] && inside[flip.facet[1]])
          /* Add mirrored \INFTY{} $(n-1)$-flip in new external space */
          addFlips.push_back(flip);
        else
        {
          /* Expand mirrored \INFTY{} flip by existing flips */
          for(typename std::map<Index,bool>::const_iterator it2 = inside.begin() ;
              it2 != inside.end() ; it2++)
          {
            if(!it2->second)
            {
              /* Remove old \INFTY{} flip for non-external facet in deleteCell */
              FlipI flipM = matchFirst(flip.joint,it2->first,Index::Q,Index::INFTY);
              if(flipM.isValid())
              {
                flip = flipM.replaceIndex(it2->first,flip.otherFacet(it2->first));
                delFlips.push_back(flipM);
              }
              else
                throw std::runtime_error
                  ("deleteCell: No INFTY flips in unlifted space");

              /* Check if we've removed the last \INFTY{} flip on this joint in deleteCell */
              if(flip.facet[0] == flip.facet[1]) break;
            }
          }
          /* Add back an \INFTY{} flip if we have any \INFTY{} left in deleteCell */
          if(flip.facet[0] != flip.facet[1]) addFlips.push_back(flip);
        }
      }
    }
  }

  /* Remove flips in delFlips from flips */
  for(const auto &flip : delFlips)
    flips.erase(flip);

  /* Add flips in addFlips to flips */
  flips.insert(addFlips.begin(),addFlips.end());

  /* Erase the cell from the dimension map */
  dimensionMap.erase(oldCell);

  return true;
}


/* CellStructure splitCell operation */
template<typename Index>
bool CellStructure<Index>::splitCell(CellStructure<Index>::SplitStruct ss,
                                     CellStructure<Index>::BoundaryChain membraneBoundary)
{
  /* Validate the SplitStruct before splitting */
  if(ss.parent.isPseudocell() || ss.membrane.isPseudocell() ||
     ss.childP.isPseudocell() || ss.childN.isPseudocell())
  {
    error = std::string("splitCell: SplitStruct ")+to_string(ss)+
      " contains pseudocells.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(!hasCell(ss.parent))
  {
    error = std::string("splitCell: Cannot split nonexistent cell ")+
      to_string(ss.parent);
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(hasCell(ss.membrane) || hasCell(ss.childP) || hasCell(ss.childN))
  {
    error = std::string("splitCell: One or more target cells in SplitStruct ")+
      to_string(ss)+" already exist.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  /* Find dimension of cell to split */
  Dimension parentDim = dimensionOf(ss.parent);
  if(parentDim == 0)
  {
    error = std::string("splitCell: Cannot split vertex ")+to_string(ss.parent);
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }


  FlipVectorI addFlips, delFlips;
  if(parentDim == 1)
  {
    /* Special case: split an edge */
    if(!membraneBoundary.empty())
    {
      error = std::string("splitCell: non-empty boundary supplied for vertex membrane");
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }

    /* Handle 0-flips in edge splitting */
    FlipI vtxFlip = matchFirst(Index::BOTTOM,Index::Q,Index::Q,ss.parent);
    if(!vtxFlip.isValid())
      throw std::runtime_error
        (std::string("Edge ")+to_string(ss.parent)+" is interior in no 0-flips");
    delFlips.push_back(vtxFlip);

    Index vN = vtxFlip.facet[0], vP = vtxFlip.facet[1];
    Index eN = ss.childN, v = ss.membrane, eP = ss.childP;
    addFlips.push_back(FlipI(Index::BOTTOM,vN,v,eN));
    addFlips.push_back(FlipI(Index::BOTTOM,v,vP,eP));

    /* Handle 1-flips in edge splitting */
    /* Handle 1-flips of childN about vertex vN */
    FlipVectorI edgeNFlips = matchV(vN,ss.parent,Index::Q,Index::Q);
    for(FlipVectorIterator iter = edgeNFlips.begin() ;
        iter != edgeNFlips.end() ; iter++)
    {
      delFlips.push_back(*iter);
      addFlips.push_back(iter->replaceIndex(ss.parent,eN));
    }

    /* Handle 1-flips of childP about vertex vP */
    FlipVectorI edgePFlips = matchV(vP,ss.parent,Index::Q,Index::Q);
    for(FlipVectorIterator iter = edgePFlips.begin() ;
        iter != edgePFlips.end() ; iter++)
    {
      delFlips.push_back(*iter);
      addFlips.push_back(iter->replaceIndex(ss.parent,eP));
    }

    /* Add new 1-flips across the new membrane v */
    BoundaryChain edgeCoboundary = coboundary(+ss.parent);
    if(maxDimension() == 1)
      edgeCoboundary += ro(ss.parent,Index::TOP) * Index::TOP;
    else if(maxDimension() == 2 && onBorder(ss.parent))
      edgeCoboundary += ro(ss.parent,Index::INFTY) * Index::INFTY;
    for(BoundaryIterator iter = edgeCoboundary.begin() ;
        iter != edgeCoboundary.end() ; iter++)
      addFlips.push_back((*iter).orientation() * FlipI(v,eP,eN,~(*iter)));

    /* Handle 2-flips in edge splitting */
    FlipVectorI faceFlips = matchV(ss.parent,Index::Q,Index::Q,Index::Q);
    for(FlipVectorIterator iter = faceFlips.begin() ;
        iter != faceFlips.end() ; iter++)
    {
      delFlips.push_back(*iter);
      addFlips.push_back(iter->replaceIndex(ss.parent,eP));
      addFlips.push_back(iter->replaceIndex(ss.parent,eN));
    }
  }
  else
  {
    /* Special case: split an edge */
    if(membraneBoundary.empty()) {
      error = std::string("splitCell: empty membrane boundary");
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
    for(auto c : membraneBoundary)
      if((~c).isPseudocell()) {
        error = std::string("splitCell: Pseudocell in membrane boundary");
        mdxWarning << QString::fromStdString(error) << endl;
        return false;
      }

    /* General case: split a cell of dimension > 1 */
    /* Validate membrane boundary */
    if(parentDim - dimensionOf(~(*(membraneBoundary.begin()))) != 2)
    {
      error = std::string("splitCell: parent cell ")+to_string(ss.parent)+
        " is of dimension "+to_string(parentDim)+
        " but supplied membrane boundary is of dimension "+
        to_string(dimensionOf(~(*(membraneBoundary.begin()))));
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
    if(!buildBoundary(ss.membrane,membraneBoundary,addFlips,delFlips)) return false;

    /* Sort the boundary of the parent into two parts */
    DisjointSet<Index> bParent;
    FlipVectorI nm1Flips = matchV(Index::Q,Index::Q,Index::Q,ss.parent);
    for(FlipVectorIterator iter = nm1Flips.begin() ;
        iter != nm1Flips.end() ; iter++)
    {
      /* Make entries for boundary cells in the disjoint-set data structure */
      bParent.MakeSet(iter->facet[0]);
      bParent.MakeSet(iter->facet[1]);

      /* Join boundary cells on the same side of the membrane */
      if(membraneBoundary.count(iter->joint) == 0)
      {
        bParent.MakeSet(iter->joint);
        bParent.Union(iter->joint,iter->facet[0]);
        bParent.Union(iter->joint,iter->facet[1]);
      }

    }
    /* Make sure that the membrane splits the boundary in two */
    if(bParent.CountComponents() != 2)
    {
      error = std::string("splitCell: The supplied membrane boundary ")+
        "divides the boundary of parent cell "+to_string(ss.parent)+
        " into "+to_string(bParent.CountComponents())+" pieces.";
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }

    /* Figure out which component corresponds to which child cell */
    /* Find a flip midFlip across the membrane boundary */
    FlipI midFlip = matchFirst(~(*(membraneBoundary.begin())),Index::Q,Index::Q,ss.parent);
    if(!midFlip.isValid())
      throw std::runtime_error
        (std::string("There are no flips of ")+
         to_string(~(*(membraneBoundary.begin())))+" across "+to_string(ss.parent));

    RO midOrientation = membraneBoundary[midFlip.joint];
    std::map<Index,Index> cellSide;
    cellSide[bParent.FindSet(midFlip.facet[0])] = ss.child(-midOrientation);
    cellSide[bParent.FindSet(midFlip.facet[1])] = ss.child(midOrientation);

    /* Handle $(n-1)$-flips in cell splitting */
    for(FlipVectorIterator iter = nm1Flips.begin() ; iter != nm1Flips.end() ; iter++)
    {
      delFlips.push_back(*iter);
      if(membraneBoundary.count(iter->joint) > 0)
      {
        /* Handle $(n-1)$-flip across the membrane */
        for(int i = 0 ; i < 2 ; i++)
        {
          FlipI modFlip = *iter;
          modFlip.facet[i] = ss.membrane;
          modFlip.interior = cellSide[bParent.FindSet(modFlip.facet[1-i])];
          addFlips.push_back(modFlip);
        }

      }
      else
      {
        /* Handle regular $(n-1)$-flip in cell splitting */
        Index thisChild = cellSide[bParent.FindSet(iter->joint)];
        addFlips.push_back(iter->replaceIndex(ss.parent,thisChild));

      }
    }
    /* Handle $n$-flips in cell splitting */
    FlipVectorI nFlips = matchV(Index::Q,ss.parent,Index::Q,Index::Q);
    for(FlipVectorIterator iter = nFlips.begin() ;
        iter != nFlips.end() ; iter++)
    {
      delFlips.push_back(*iter);
      Index thisChild = cellSide[bParent.FindSet(iter->joint)];
      addFlips.push_back(iter->replaceIndex(ss.parent,thisChild));
    }
    /* Add new $n$-flips across the new membrane */
    BoundaryChain parentCoboundary = coboundary(+ss.parent);
    if(parentDim == maxDimension())
      parentCoboundary += ro(ss.parent,Index::TOP) * +Index::TOP;
    else if(parentDim == (maxDimension() - 1) && onBorder(ss.parent))
      parentCoboundary += ro(ss.parent,Index::INFTY) * +Index::INFTY;
    for(BoundaryIterator iter = parentCoboundary.begin() ;
        iter != parentCoboundary.end() ; iter++)
      addFlips.push_back((*iter).orientation() *
                         FlipI(ss.membrane,ss.childP,ss.childN,~(*iter)));


    /* Handle $(n+1)$-flips in cell splitting */
    FlipVectorI np1Flips = matchV(ss.parent,Index::Q,Index::Q,Index::Q);
    for(FlipVectorIterator iter = np1Flips.begin() ;
        iter != np1Flips.end() ; iter++)
    {
      delFlips.push_back(*iter);
      addFlips.push_back(iter->replaceIndex(ss.parent,ss.childP));
      addFlips.push_back(iter->replaceIndex(ss.parent,ss.childN));
    }
  }

  /* Remove flips in delFlips from flips */
  for(const auto &flip : delFlips)
    flips.erase(flip);

  /* Add flips in addFlips to flips */
  flips.insert(addFlips.begin(),addFlips.end());

  /* Remove parent cell from dimension map */
  dimensionMap.erase(ss.parent);

  /* Add child cells and membrane to dimension map */
  dimensionMap.insert(ss.childP,parentDim);
  dimensionMap.insert(ss.childN,parentDim);
  dimensionMap.insert(ss.membrane,parentDim - 1);

  return true;
}

/* CellStructure \operation{mergeCells} operation */
template<typename Index>
bool CellStructure<Index>::mergeCells(CellStructure<Index>::SplitStruct& ss)
{
  Index oldMembrane = ss.membrane, oldCell[2], newCell = ss.parent;
  /* Validate the SplitStruct before merging */
  if(oldMembrane.isPseudocell())
  {
    error = std::string("merge: Cannot remove cell ")+to_string(oldMembrane)+
      " as it is a pseudocell.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(newCell.isPseudocell())
  {
    error = std::string("merge: Cannot merge two cells into ")+to_string(newCell)+
      " as it is a pseudocell.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(!hasCell(oldMembrane))
  {
    error = std::string("merge: Cannot remove cell ")+to_string(oldMembrane)+
      " as it is not in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(hasCell(newCell))
  {
    error = std::string("merge: Cannot merge two cells into ")+to_string(newCell)+
      " as it is already in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  /* Find the two child cells childP and childN for merge */
  BoundaryChain cob = coboundary(oldMembrane);
  if(cob.size() != 2)
  {
    error = std::string("merge: Coboundary of old membrane ")+to_string(oldMembrane)+
      " must have exactly two cells, not "+to_string(cob.size());
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  OrientedCell ch1 = *(cob.begin()), ch2 = *(++cob.begin());
  if(ch1.orientation() == ch2.orientation())
  {
    error = std::string("merge: Coboundary cells ")+to_string(ch1)+" and "+
      to_string(ch2)+" must have opposite orientations "+
      "with respect to membrane "+to_string(oldMembrane);
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(ch1.orientation() == NEG) std::swap(ch1,ch2);
  ss.childP = oldCell[0] = ~ch1;
  ss.childN = oldCell[1] = ~ch2;

  /* Make sure that childP $\meet$ childN is just membrane */
  FlipVectorI oldPFlips = matchV(Index::Q,Index::Q,oldMembrane,oldCell[0]);
  for(FlipVectorIterator iter = oldPFlips.begin() ;
      iter != oldPFlips.end() ; iter++)
  {
    FlipI oldNFlip = matchFirst(iter->replaceIndex(oldCell[0],oldCell[1]));
    if(oldNFlip.isValid())
    {
      error = std::string("merge: cell ")+to_string(oldMembrane)+" flips to "+
        to_string(iter->otherFacet(oldMembrane))+" in both "+to_string(oldCell[0])+
        " and "+to_string(oldCell[1]);
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
  }

  FlipVectorI addFlips, delFlips;
  /* Get all flips related to child and membrane cells for merge */
  FlipVectorI childJ[2], childF[2], membraneJ, membraneF, membraneI;
  std::set<FlipI> childI[2];

  membraneJ = matchV(oldMembrane,Index::Q,Index::Q,Index::Q);
  membraneF = matchV(Index::Q,oldMembrane,Index::Q,Index::Q);
  membraneI = matchV(Index::Q,Index::Q,Index::Q,oldMembrane);
  for(int i = 0 ; i < 2 ; i++)
  {
    childJ[i] = matchV(oldCell[i],Index::Q,Index::Q,Index::Q);
    childF[i] = matchV(Index::Q,oldCell[i],Index::Q,Index::Q);
    FlipVectorI cI = matchV(Index::Q,Index::Q,Index::Q,oldCell[i]);
    childI[i].insert(cI.begin(),cI.end());
  }

  delFlips.insert(delFlips.end(),membraneJ.begin(),membraneJ.end());
  delFlips.insert(delFlips.end(),membraneF.begin(),membraneF.end());
  delFlips.insert(delFlips.end(),membraneI.begin(),membraneI.end());
  for(int i = 0 ; i < 2 ; i++)
  {
    delFlips.insert(delFlips.end(),childJ[i].begin(),childJ[i].end());
    delFlips.insert(delFlips.end(),childF[i].begin(),childF[i].end());
    delFlips.insert(delFlips.end(),childI[i].begin(),childI[i].end());
  }


  /* Handle $n$-flips in cell merging */
  for(int i = 0 ; i < 2 ; i++)
    for(FlipVectorIterator iter = childF[i].begin() ; iter != childF[i].end() ; iter++)
      if(iter->otherFacet(oldCell[i]) != oldCell[1-i])
        addFlips.push_back(iter->replaceIndex(oldCell[i],newCell));
  /* Confirm that there are as many parent $n$-flips as child $n$-flips */
  if(childF[0].size() + childF[1].size() - 2 * membraneJ.size() != addFlips.size())
  {
    error = std::string("merge: Accumulated too few flips of ")+
      to_string(newCell)+" from flips of children "+to_string(oldCell[0])+
      " and "+to_string(oldCell[1])+"; do they share a joint?";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }


  /* Handle $(n-1)$-flips in cell merging */
  FlipVectorI mF0 = matchV(Index::Q,oldMembrane,Index::Q,oldCell[0]);
  FlipVectorI mF1 = matchV(Index::Q,oldMembrane,Index::Q,oldCell[1]);
  if(mF0.size() != mF1.size())
    throw std::runtime_error
      (std::string("merge: There are ")+to_string(mF0.size())+" flips of "+
       to_string(oldMembrane)+" across "+to_string(oldCell[0])+" and "+
       to_string(mF1.size())+" flips across "+to_string(oldCell[1])+
       "; these numbers should be the same.");
  /* Pair together flips from mF0 and mF1 to make complete parent flips */
  for(FlipVectorIterator iter = mF0.begin() ; iter != mF0.end() ; iter++)
  {
    FlipI c1Match = matchFirst(iter->joint,oldMembrane,Index::Q,oldCell[1]);
    if(!c1Match.isValid())
      throw std::runtime_error
        (std::string("merge: Could not find flip in ")+to_string(oldCell[1])+
         " corresponding to "+to_string(*iter));

    Index otherFacet = c1Match.otherFacet(oldMembrane);
    FlipI parentFlip = iter->replaceIndex(oldCell[0],newCell);
    addFlips.push_back(parentFlip.replaceIndex(oldMembrane,otherFacet));

    /* Remove paired flips from childI */
    childI[0].erase(*iter);
    childI[1].erase(c1Match);

  }

  /* Modify the rest of the flips with child cells in interior position */
  for(int i = 0 ; i < 2 ; i++)
    for(typename std::set<FlipI>::iterator iter = childI[i].begin() ; iter != childI[i].end() ; iter++)
      addFlips.push_back(iter->replaceIndex(oldCell[i],newCell));


  /* Handle $(n+1)$-flips in cell merging */
  if(childJ[0].size() != childJ[1].size())
  {
    error = std::string("merge: different number of flips with joint ")+
      to_string(oldCell[0])+" and joint "+to_string(oldCell[1]);
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  for(FlipVectorIterator iter = childJ[0].begin() ; iter != childJ[0].end() ; iter++)
    addFlips.push_back(iter->replaceIndex(oldCell[0],newCell));

  /* Remove flips in delFlips from flips */
  for(const auto &flip : delFlips)
    flips.erase(flip);

  /* Add flips in addFlips to flips */
  flips.insert(addFlips.begin(),addFlips.end());


  /* Add new parent cell to the dimension map */
  dimensionMap.insert(ss.parent,dimensionOf(ss.childP));

  /* Remove deleted child and membrane cells from the dimension map */
  dimensionMap.erase(ss.membrane);
  dimensionMap.erase(ss.childP);
  dimensionMap.erase(ss.childN);


  return true;
}

/* CellStructure \operation{collapseCell} operation */
template<typename Index>
bool CellStructure<Index>::collapseCell(CellStructure<Index>::SplitStruct &ss)
{
  Index toCollapse = ss.membrane, newCell = ss.parent, oldCell[2];
  /* Validate the SplitStruct before collapsing */
  if(toCollapse.isPseudocell())
  {
    error = std::string("collapse: Cannot remove cell ")+to_string(toCollapse)+
      " as it is a pseudocell.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(newCell.isPseudocell())
  {
    error = std::string("collapse: Cannot merge two cells into ")+to_string(newCell)+
      " as it is a pseudocell.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(!hasCell(toCollapse))
  {
    error = std::string("collapse: Cannot remove cell ")+to_string(toCollapse)+
      " as it is not in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(hasCell(newCell))
  {
    error = std::string("collapse: Cannot merge two cells into ")+to_string(newCell)+
      " as it is already in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  /* Find the two child cells childP and childN for collapse */
  BoundaryChain bd = boundary(toCollapse);
  if(bd.size() != 2)
  {
    error = std::string("collapse: Boundary of cell to collapse ")+to_string(toCollapse)+
        " must have exactly two cells, not "+to_string(bd.size());
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  OrientedCell ch1 = *(bd.begin()), ch2 = *(++bd.begin());
  if(ch1.orientation() == ch2.orientation())
  {
    error = std::string("collapse: Boundary cells ")+to_string(ch1)+" and "+
      to_string(ch2)+" must have opposite orientations "+
      "with respect to cell to collapse "+to_string(toCollapse);
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  if(ch1.orientation() == NEG) std::swap(ch1,ch2);
  ss.childP = oldCell[0] = ~ch1;
  ss.childN = oldCell[1] = ~ch2;

  /* Make sure that childP $\join$ childN is just toCollapse */
  FlipVectorI oldPFlips = matchV(Index::Q,oldCell[0],oldCell[1],Index::Q);
  for(FlipVectorIterator iter = oldPFlips.begin() ;
      iter != oldPFlips.end() ; iter++)
  {
    if(iter->interior != toCollapse)
    {
      error = std::string("collapse: cells ")+to_string(oldCell[0])+
        " and "+to_string(oldCell[1])+" flip through "+to_string(iter->interior)+
        " in addition to "+to_string(toCollapse);
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }
  }


  FlipVectorI addFlips, delFlips;
  /* Get all flips related to child and membrane cells for collapse */
  FlipVectorI childF[2], childI[2], collapseJ, collapseF, collapseI;
  std::set<FlipI> childJ[2];

  collapseJ = matchV(toCollapse,Index::Q,Index::Q,Index::Q);
  collapseF = matchV(Index::Q,toCollapse,Index::Q,Index::Q);
  collapseI = matchV(Index::Q,Index::Q,Index::Q,toCollapse);
  for(int i = 0 ; i < 2 ; i++)
  {
    FlipVectorI cJ = matchV(oldCell[i],Index::Q,Index::Q,Index::Q);
    childJ[i].insert(cJ.begin(),cJ.end());
    childF[i] = matchV(Index::Q,oldCell[i],Index::Q,Index::Q);
    childI[i] = matchV(Index::Q,Index::Q,Index::Q,oldCell[i]);
  }

  delFlips.insert(delFlips.end(),collapseJ.begin(),collapseJ.end());
  delFlips.insert(delFlips.end(),collapseF.begin(),collapseF.end());
  delFlips.insert(delFlips.end(),collapseI.begin(),collapseI.end());
  for(int i = 0 ; i < 2 ; i++)
  {
    delFlips.insert(delFlips.end(),childJ[i].begin(),childJ[i].end());
    delFlips.insert(delFlips.end(),childF[i].begin(),childF[i].end());
    delFlips.insert(delFlips.end(),childI[i].begin(),childI[i].end());
  }

  /* Handle $n$-flips in cell collapse */
  for(int i = 0 ; i < 2 ; i++)
    for(FlipVectorIterator iter = childF[i].begin() ; iter != childF[i].end() ; iter++)
      if(iter->otherFacet(oldCell[i]) != oldCell[1-i])
        addFlips.push_back(iter->replaceIndex(oldCell[i],newCell));
  /* Confirm that there are as many parent $n$-flips as child $n$-flips for collapse */
  if(childF[0].size() + childF[1].size() - 2 * collapseI.size() != addFlips.size())
  {
    error = std::string("collapse: Accumulated too few flips of ")+
      to_string(newCell)+" from flips of children "+to_string(oldCell[0])+
      " and "+to_string(oldCell[1])+"; do they share a joint?";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }


  /* Handle $(n+1)$-flips in cell collapse */
  FlipVectorI cF0 = matchV(oldCell[0],toCollapse,Index::Q,Index::Q);
  FlipVectorI cF1 = matchV(oldCell[1],toCollapse,Index::Q,Index::Q);
  if(cF0.size() != cF1.size())
    throw std::runtime_error
      (std::string("collapse: There are ")+to_string(cF0.size())+" flips of "+
       to_string(toCollapse)+" over "+to_string(oldCell[0])+" and "+
       to_string(cF1.size())+" flips over "+to_string(oldCell[1])+
       "; these numbers should be the same.");
  /* Pair together flips from cF0 and cF1 to make complete parent flips */
  for(FlipVectorIterator iter = cF0.begin() ; iter != cF0.end() ; iter++)
  {
    FlipI c1Match = matchFirst(oldCell[1],toCollapse,Index::Q,iter->interior);
    if(!c1Match.isValid())
      throw std::runtime_error
        (std::string("collapse: Could not find flip in ")+to_string(oldCell[1])+
         " corresponding to "+to_string(*iter));

    Index otherFacet = c1Match.otherFacet(toCollapse);
    FlipI parentFlip = iter->replaceIndex(oldCell[0],newCell);
    addFlips.push_back(parentFlip.replaceIndex(toCollapse,otherFacet));

    /* Remove paired flips from childJ */
    childJ[0].erase(*iter);
    childJ[1].erase(c1Match);

  }

  /* Modify the rest of the flips with child cells in joint position */
  for(int i = 0 ; i < 2 ; i++)
    for(typename std::set<FlipI>::iterator iter = childJ[i].begin() ; iter != childJ[i].end() ; iter++)
      addFlips.push_back(iter->replaceIndex(oldCell[i],newCell));


  /* Handle $(n-1)$-flips in cell collapse */
  if(childI[0].size() != childI[1].size())
  {
    error = std::string("collapse: different number of flips with interior ")+
      to_string(oldCell[0])+" and interior "+to_string(oldCell[1]);
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  for(FlipVectorIterator iter = childI[0].begin() ; iter != childI[0].end() ; iter++)
    addFlips.push_back(iter->replaceIndex(oldCell[0],newCell));

  /* Remove flips in delFlips from flips */
  for(const auto &flip : delFlips)
    flips.erase(flip);

  /* Add flips in addFlips to flips */
  flips.insert(addFlips.begin(),addFlips.end());


  /* Add new parent cell to the dimension map */
  dimensionMap.insert(ss.parent,dimensionOf(ss.childP));

  /* Remove deleted child and membrane cells from the dimension map */
  dimensionMap.erase(ss.membrane);
  dimensionMap.erase(ss.childP);
  dimensionMap.erase(ss.childN);


  return true;
}

/* CellStructure \operation{glue} operation */
template<typename Index>
bool CellStructure<Index>::glue(Index keepCell, Index removeCell)
{
  /* Confirm eligibility of cells for gluing */
  if(keepCell.isPseudocell() || removeCell.isPseudocell())
  {
    error = std::string("glue: Cannot glue cells ")+to_string(keepCell)+" and "+
      to_string(removeCell)+" as one is a pseudocell.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  else if(keepCell == removeCell)
  {
    error = std::string("glue: Cannot glue cell ")+to_string(keepCell)+" to itself.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  else if(!hasCell(keepCell))
  {
    error = std::string("glue: Cannot glue cell ")+to_string(keepCell)+
      " which is not in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  else if(!hasCell(removeCell))
  {
    error = std::string("glue: Cannot glue cell ")+to_string(removeCell)+
      " which is not in the complex.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  else if(dimensionOf(keepCell) != dimensionOf(removeCell))
  {
    error = std::string("glue: Cannot glue cells ")+to_string(keepCell)+" (dimension "+
      to_string(dimensionOf(keepCell))+") and "+to_string(removeCell)+" (dimension "+
      to_string(dimensionOf(removeCell))+").";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  /* Confirm that cells to be glued join at \TOP{} or \INFTY{} */
  Index joinCell = join(keepCell,removeCell);
  if(joinCell != Index::TOP && joinCell != Index::INFTY)
  {
    error = std::string("glue: Cannot glue two cells ")+to_string(keepCell)+" and "+
      to_string(removeCell)+" whose join is "+to_string(joinCell);
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  FlipVectorI addFlips, delFlips;

  Dimension dimension = dimensionOf(keepCell);
  /* Find flips involving removeCell */
  FlipVectorI removeI = matchV(Index::Q,Index::Q,Index::Q,removeCell);
  FlipVectorI removeF = matchV(Index::Q,removeCell,Index::Q,Index::Q);
  FlipVectorI removeJ = matchV(removeCell,Index::Q,Index::Q,Index::Q);

  delFlips.insert(delFlips.end(),removeI.begin(),removeI.end());
  delFlips.insert(delFlips.end(),removeF.begin(),removeF.end());
  delFlips.insert(delFlips.end(),removeJ.begin(),removeJ.end());

  if(dimension != 0)
  {
    /* Confirm that cells to be glued have the same boundary */
    FlipVectorI keepI = matchV(Index::Q,Index::Q,Index::Q,keepCell);
    bool sameBoundary = true;
    if(keepI.size() != removeI.size()) sameBoundary = false;
    else
    {
      FlipVectorIterator iterK = keepI.begin(), iterR = removeI.begin();
      while(iterK != keepI.end())
      {
        FlipI rToK = (iterR++)->replaceIndex(removeCell,keepCell);
        if(rToK != *(iterK++)) { sameBoundary = false; break; }
      }
    }
    if(!sameBoundary)
    {
      error = std::string("glue: Cells ")+to_string(keepCell)+" and "+
        to_string(removeCell)+" have different boundaries and cannot be glued.";
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }

  }

  if(dimension + 2 <= maxDimension())
  {
    /* Glue cells at least two dimensions below maxDimension() */
    for(FlipVectorIterator iter = removeF.begin() ; iter != removeF.end() ; iter++)
      /* Add *iter to addFlips, replacing removeCell with keepCell */
      addFlips.push_back(iter->replaceIndex(removeCell,keepCell));

    for(FlipVectorIterator iter = removeJ.begin() ; iter != removeJ.end() ; iter++)
      /* Add *iter to addFlips, replacing removeCell with keepCell */
      addFlips.push_back(iter->replaceIndex(removeCell,keepCell));
  }
  else if(dimension + 1 == maxDimension())
  {
    /* Glue cells one dimension below maxDimension() */
    /* Make sure keepCell and removeCell have no more than one incident $n$-cell */
    std::set<Index> keepCob = cobounds(keepCell), remCob = cobounds(removeCell);
    if(keepCob.size() > 1 || remCob.size() > 1)
    {
      error = std::string("glue: Cannot glue cell ")+to_string(removeCell)+
        " into "+to_string(keepCell)+" as the complex would become nonmanifold.";
      mdxWarning << QString::fromStdString(error) << endl;
      return false;
    }

    for(FlipVectorIterator iter = removeF.begin() ; iter != removeF.end() ; iter++)
    {
      if(iter->interior == Index::INFTY)
      {
        /* Glue together $(n-1)$-flips across \INFTY{} */
        if(iter->otherFacet(removeCell) != keepCell)
        {
          FlipI keepFlip = matchFirst(iter->joint,keepCell,Index::Q,Index::INFTY);
          if(!keepFlip.isValid())
            addFlips.push_back(iter->replaceIndex(removeCell,keepCell));
          else
          {
            delFlips.push_back(keepFlip);
            if(keepFlip.facet[0] == keepCell && iter->facet[1] == removeCell)
              addFlips.push_back(FlipI(iter->joint,iter->facet[0],keepFlip.facet[1],Index::INFTY));
            else if(keepFlip.facet[1] == keepCell && iter->facet[0] == removeCell)
              addFlips.push_back(FlipI(iter->joint,keepFlip.facet[0],iter->facet[1],Index::INFTY));
            else
            {
              error = std::string("glue: Orientation mismatch between ")+to_string(keepCell)+
                " and "+to_string(removeCell)+" with respect to INFTY.";
              mdxWarning << QString::fromStdString(error) << endl;
              return false;
            }
          }
        }

      }
      else
        /* Add *iter to addFlips, replacing removeCell with keepCell */
        addFlips.push_back(iter->replaceIndex(removeCell,keepCell));

    }
    for(FlipVectorIterator iter = removeJ.begin() ; iter != removeJ.end() ; iter++)
    {
      if(iter->facet[0] == Index::INFTY || iter->facet[1] == Index::INFTY)
      {
        /* Glue together $n$-flips to \INFTY{} */
        if(!keepCob.empty())
        {
          FlipI keepFlip = matchFirst(keepCell,*(keepCob.begin()),Index::INFTY,Index::TOP);
          if(!keepFlip.isValid())
          {
            throw std::runtime_error("glue: Could not match keepJ as border facet.");
          }
          delFlips.push_back(keepFlip);
          if(iter->facet[0] == Index::INFTY && keepFlip.facet[1] == Index::INFTY)
            addFlips.push_back(FlipI(keepCell,keepFlip.facet[0],iter->facet[1],Index::TOP));
          else if(iter->facet[1] == Index::INFTY && keepFlip.facet[0] == Index::INFTY)
            addFlips.push_back(FlipI(keepCell,iter->facet[0],keepFlip.facet[1],Index::TOP));
          else
          {
            error = std::string("glue: Incompatible flip orientations between ")+
              to_string(*iter)+" and "+to_string(keepFlip);
            mdxWarning << QString::fromStdString(error) << endl;
            return false;
          }
        }

      }
      else
      {
        error = std::string("glue: Unexpected ")+to_string(dimension+1)+
          "-flip across "+to_string(removeCell)+": "+to_string(*iter);
        mdxWarning << QString::fromStdString(error) << endl;
        return false;
      }
    }
  }
  else if(dimension == maxDimension())
  {
    error = std::string("glue: You cannot join cells of maximal dimension.");
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }
  else
  {
    error = std::string("glue: Gluing cells of dimension ")+to_string(dimension)+
      " in a complex of dimension "+to_string(maxDimension())+" not implemented.";
    mdxWarning << QString::fromStdString(error) << endl;
    return false;
  }

  /* Remove flips in delFlips from flips */
  for(const auto &flip : delFlips)
    flips.erase(flip);

  /* Add flips in addFlips to flips */
  flips.insert(addFlips.begin(),addFlips.end());

  /* Remove removeCell from the dimension map */
  dimensionMap.erase(removeCell);

  return true;
}

/* CellStructure unoriented \operation{bounds} operation */
template<typename Index>
std::set<Index> CellStructure<Index>::bounds(Index cell) const
{
  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::bounds: cell ")+
       to_string(cell)+" is not in complex");

  std::set<Index> answer;
  std::vector<FlipI> matches = matchV(Index::Q,Index::Q,Index::Q,cell);
  for(FlipVectorIterator iter = matches.begin() ;
      iter != matches.end() ; iter++)
  {
    answer.insert(iter->facet[0]);
    answer.insert(iter->facet[1]);
  }
  /* Remove pseudocells from answer in boundary query operation */
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}

/* CellStructure unoriented \operation{cobounds} operation */
template<typename Index>
std::set<Index> CellStructure<Index>::cobounds(Index cell) const
{
  /* Special case for coboundary of \BOTTOM{} */
  if(cell == Index::BOTTOM)
  {
    std::set<Index> answer(cellsOfDimension(0).begin(),
                           cellsOfDimension(0).end());
    return answer;
  }


  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::cobounds: cell ")+
       to_string(cell)+" is not in complex");

  std::set<Index> answer;
  std::vector<FlipI> matches = matchV(Index::Q,cell,Index::Q,Index::Q);
  for(FlipVectorIterator iter = matches.begin() ;
      iter != matches.end() ; iter++)
    answer.insert(iter->interior);
  /* Remove pseudocells from answer in boundary query operation */
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}

/* CellStructure unoriented \operation{neighbors} operation */
template<typename Index>
std::set<Index> CellStructure<Index>::neighbors(Index cell) const
{
  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::neighbors: cell ")+
       to_string(cell)+" is not in complex");

  std::set<Index> answer;
  std::vector<FlipI> matches = matchV(Index::Q,cell,Index::Q,Index::Q);
  for(FlipVectorIterator iter = matches.begin() ;
      iter != matches.end() ; iter++)
    answer.insert(iter->otherFacet(cell));
  /* Remove pseudocells from answer in boundary query operation */
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}

template<typename Index>
std::set<Index> CellStructure<Index>::incidentCells
  (Index cell, CellStructure<Index>::Dimension dim) const
{
  /* Check validity of dimension in incidentCells */
  if(dim > maxDimension())
    throw std::domain_error
      (std::string("In CellStructure::incidentCells: dimension ")+
      to_string(dim)+" higher than maximum dimension of complex");

  /* Check validity of cell in incidentCells */
  if(!hasCell(cell))
    throw std::domain_error
      (std::string("In CellStructure::incidentCells: cell ")+
      to_string(cell)+" is not in complex");

  /* Special case: all cells are incident to \TOP{} or \BOTTOM{} */
  if(cell == Index::TOP || cell == Index::BOTTOM)
  {
    std::set<Index> answer(cellsOfDimension(dim).begin(),
                           cellsOfDimension(dim).end());
    return answer;
  }


  std::set<Index> answer;
  Dimension cellDim = dimensionOf(cell);
  /* Indicent cells at same dimension as given cell */
  if(dim == cellDim)
  {
    answer.insert(cell);
  }

  else
  /* Incident cells one dimension above given cell */
  if(dim == (cellDim + 1))
  {
    return cobounds(cell);
  }

  else
  /* Incident cells one dimension below given cell */
  if(dim == (cellDim - 1))
  {
    return bounds(cell);
  }

  else
  /* Incident cells more than one dimension above given cell */
  if(dim > cellDim)
  {
    std::set<Index> cells0, cells1;
    cells1.insert(cell);
    int currentDimension = cellDim;
    while(dim > currentDimension)
    {
      std::swap(cells0,cells1);
      cells1.clear();
      boundUp(cells0,cells1);
      currentDimension++;
    }
    std::swap(answer,cells1);
  }

  else
  /* Incident cells more than one dimension below given cell */
  if(dim < cellDim)
  {
    std::set<Index> cells0, cells1, cells2;
    cells2.insert(cell);
    int currentDimension = cellDim;
    while(dim < currentDimension)
    {
      cells1.clear();
      std::swap(cells0,cells2);
      cells2.clear();
      boundDown(cells0,cells1,cells2);
      currentDimension -= 2;
    }
    if(currentDimension == dim) std::swap(answer,cells2);
    else std::swap(answer,cells1);
  }


  /* Remove pseudocells from answer in boundary query operation */
  if(answer.begin() != answer.end() && answer.begin()->isPseudocell())
    answer.erase(answer.begin());

  return answer;
}
template<typename Index>
RO CellStructure<Index>::ro(Index c1, Index c2) const
{
  /* CellStructure \operation{ro} validity checks */
  if(!hasCell(c1))
    throw std::domain_error
      (std::string("CellStructure: Called ro "
                   "on nonexistent cell ")+to_string(c1));
  if(!hasCell(c2))
    throw std::domain_error
      (std::string("CellStructure: Called ro "
                   "on nonexistent cell ")+to_string(c2));
  if(c1 == c2) throw std::invalid_argument
                (std::string("CellStructure: Can't find ro "
                             "between cell ")+to_string(c1)+" and itself");


  if(c1 == Index::BOTTOM || c2 == Index::BOTTOM)
  {
    /* CellStructure \operation{ro} special case for \BOTTOM */
    Index other = (c1 == Index::BOTTOM) ? c2 : c1;
    Dimension dim = dimensionOf(other);
    if(dim != 0)
      throw std::invalid_argument
        (std::string("CellStructure: Called for ro "
                     "between BOTTOM and non-vertex ")+to_string(other));
    return POS;

  }
  else if(c1 == Index::TOP || c2 == Index::TOP)
  {
    /* CellStructure \operation{ro} special case for \TOP */
    Index other = (c1 == Index::TOP) ? c2 : c1;
    Dimension dim = dimensionOf(other);
    if(dim != maxDimension())
      throw std::invalid_argument
        (std::string("CellStructure: Called for ro "
                     "between TOP and non-maxdim cell ")+to_string(other));
    FlipTower tower(*this,other);
    return tower.ro(dim);

  }
  else
  {
    /* CellStructure \operation{ro} general case */
    Dimension dim1 = dimensionOf(c1), dim2 = dimensionOf(c2);
    if(dim2-dim1 != 1 && dim1-dim2 != 1)
      throw std::invalid_argument
        (std::string("CellStructure: Called ro "
                     "on cells of non-adjacent dimension: ")+to_string(c1)+
         " of dimension "+to_string(dim1)+" and "+to_string(c2)+
         " of dimension "+to_string(dim2));
    FlipTower tower(*this,c1,c2);
    if(tower.isValid())
      return tower.ro(std::min(dim1,dim2));
    else
      throw std::invalid_argument
        (std::string("CellStructure: Flip tower created to find "
                     "ro was invalid, cells were ")+
         to_string(c1)+" and "+to_string(c2));
  }
}

/* FlipTower method definitions */
/* FlipTower fillCell methods */
/* FlipTower fillCellsBetween method */
template<typename Index>
bool CellStructure<Index>::FlipTower::fillCellsBetween
  (CellStructure<Index>::FlipTower::CellMapIterator itFrom,
   CellStructure<Index>::FlipTower::CellMapIterator itTo)
{
  if(invalid) return false;
  if(itFrom == itTo) return true;

  /* Split range between itFrom and itTo into simple ranges */
  CellMapIterator nextFrom = itFrom;
  while(++nextFrom != itTo)
    if(!fillCellsBetween(itFrom++,nextFrom)) return false;

  int itDist = itTo->first - itFrom->first;
  if(itDist == 1) return true;
  else if(itDist == 2)
  {
    /* Fill in gap of one dimension */
    FlipI flipM = cs->matchFirst(itFrom->second,Index::Q,Index::Q,itTo->second);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[itFrom->first+1] = flipM;
    cellMap[itFrom->first+1] = flips[itFrom->first+1].realFacet();
    return true;
  }
  else // more than 1 flip in the range; harder!
  {
    /* Fill in gap of more than one dimension */
    /* Create data structures for gap filling */
    Dimension activeDim = itFrom->first;
    std::set<Index> active;
    active.insert(itFrom->second);
    std::map<Index,FlipI> originFlip;

    /* Find cells from the lower-dimensional cell up */
    while(activeDim < itTo->first)
    {
      /* Find active cells two dimensions above the current */
      activeDim += 2;
      std::set<Index> nextActive;
      for(typename std::set<Index>::iterator iter = active.begin() ;
          iter != active.end() ; iter++)
      {
        /* Find the flips from a single cell in the active set */
        FlipVectorI fvec = cs->matchV(*iter,Index::Q,Index::Q,Index::Q);
        if(fvec.empty())
          throw std::runtime_error(std::string("Could not find any coboundary flip for ")+
                                   to_string(*iter)+" in FlipTower::fillCells");

        for(FlipVectorIterator fiter = fvec.begin(); fiter != fvec.end() ; fiter++)
        {
          /* Process a single flip with joint in the active set */
          if((activeDim-1) == itTo->first)
          {
            nextActive.insert(fiter->facet[0]);
            nextActive.insert(fiter->facet[1]);
            /* Assign flip as origin of facet cells */
            originFlip[fiter->facet[0]] = *fiter;
            originFlip[fiter->facet[1]] = *fiter;
          }
          else
          {
            nextActive.insert(fiter->interior);
            /* Assign flip as origin of interior cell */
            originFlip[fiter->interior] = *fiter;
          }
        }
      }
      std::swap(active,nextActive);
    }

    /* Check that the higher-dimensional cell is in the list of incident cells */
    if(active.count(itTo->second) == 0) { invalid = true; return false; }

    /* Fill cell map and flips working down from higher to lower-dimensional cell */
    /* Work down from upper cell to fill in flips */
    Index nextCell = itTo->second;
    for( ; activeDim > itFrom->first ; activeDim -= 2)
    {
      flips[activeDim-1] = originFlip[nextCell];
      nextCell = flips[activeDim-1].joint;
    }

    /* Fill in cell map from flips */
    for(Dimension d = itFrom->first+1 ; d < itTo->first ; d+=2)
    {
      cellMap[d] = flips[d].realFacet();
      cellMap[d+1] = flips[d].interior;
    }
    return true;
  }
}

/* FlipTower fillCellsBelow method */
template<typename Index>
bool CellStructure<Index>::FlipTower::fillCellsBelow
  (CellStructure<Index>::FlipTower::CellMapIterator itTo)
{
  if(invalid) return false;

  /* Split range below itTo into simple ranges */
  CellMapIterator nextTo = itTo;
  while(itTo != cellMap.begin())
    if(!fillCellsBetween(--nextTo,itTo--)) return false;

  if(itTo->first == 0) return true;
  else if(itTo->first == 1)
  {
    /* Fill in lower gap of one dimension */
    FlipI flipM = cs->matchFirst(Index::BOTTOM,Index::Q,Index::Q,itTo->second);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[0] = flipM;
    cellMap[0] = flips[0].realFacet();
    return true;
  }
  else
  {
    /* Fill in lower gap of more than one dimension */
    FlipI flipM = cs->matchFirst(Index::Q,Index::Q,Index::Q,itTo->second);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[itTo->first-1] = flipM;
    cellMap[itTo->first-2] = flips[itTo->first-1].joint;
    cellMap[itTo->first-1] = flips[itTo->first-1].realFacet();
    itTo--;
    itTo--;
    return fillCellsBelow(itTo);
  }
}

/* FlipTower fillCellsAbove method */
template<typename Index>
bool CellStructure<Index>::FlipTower::fillCellsAbove
  (CellStructure<Index>::FlipTower::CellMapIterator itFrom)
{
  if(invalid) return false;

  /* Split range above itFrom into simple ranges */
  CellMapIterator nextFrom = itFrom, lastTo = --(cellMap.end());
  while(itFrom != lastTo)
    if(!fillCellsBetween(itFrom++,++nextFrom)) return false;


  if(itFrom->first == cs->maxDimension()) return true;
  else if(itFrom->first == cs->maxDimension()-1)
  {
    /* Fill in upper gap of one dimension */
    FlipI flipM = cs->matchFirst(itFrom->second,Index::Q,Index::Q,Index::TOP);
    if(!flipM.isValid()) { invalid = true; return false; }
    flips[cs->maxDimension()] = flipM;
    cellMap[cs->maxDimension()] = flips[cs->maxDimension()].realFacet();
    return true;
  }
  else
  {
    /* Fill in upper gap of more than one dimension */
    FlipVectorI matches = cs->matchV(itFrom->second,Index::Q,Index::Q,Index::Q);
    if(matches.empty()) { invalid = true; return false; }
    {
      FlipVectorIterator iter = matches.begin();
      do
      {
        flips[itFrom->first+1] = *iter;
      } while((iter++)->interior.isPseudocell() && iter != matches.end());
    }
    cellMap[itFrom->first+2] = flips[itFrom->first+1].interior;
    cellMap[itFrom->first+1] = flips[itFrom->first+1].realFacet();
    itFrom++;
    itFrom++;
    return fillCellsAbove(itFrom);
  }
}



/* CellTuple method definitions */
/* CellTuple containing method */
template<typename Index>
void CellStructure<Index>::CellTuple::containing(const std::vector<Index>& cells)
{
  /* Add entries from cells to flip tower */
  try
  {
    for(unsigned int i = 0 ; i < cells.size() ; i++)
      tower.addCell(cells[i]);
  }
  catch(std::domain_error& e)
  {
    throw std::domain_error
      (std::string("Could not create CellTuple: invalid cell passed: ")+e.what());
  }

  /* Fill in all cells in tuple */
  if(!tower.fillAllCells() || !tower.isValid())
  {
    std::ostringstream oss;
    oss << "[ ";
    for(unsigned int i = 0 ; i < cells.size() ; i++)
    {
      if(i != 0) oss << ", ";
      oss << cells[i];
    }
    oss << " ]";

    throw std::invalid_argument
      (std::string("Could not create CellTuple with non-incident cells ")+oss.str());
  }

  /* Copy cells from flip tower to tuple */

  for(typename FlipTower::CellMapIterator iter = tower.cellMap.begin() ;
      iter != tower.cellMap.end() ; iter++)
    tuple[iter->first] = iter->second;

}

/* CellTuple flip operation */
template<typename Index>
typename CellStructure<Index>::CellTuple&
  CellStructure<Index>::CellTuple::flip(CellStructure<Index>::Dimension dim)
{
  /* Fill flip at dimension dim */
  /* Validate cached flip at dimension dim */
  if(tower.flips[dim].isValid() && cs->flips.count(tower.flips[dim]) == 0)
    tower.flips[dim] = FlipI();

  if(!tower.fillFlip(dim))
    throw std::runtime_error
      (std::string("Cell tuple ")+to_string(*this)+
       " found to be invalid during execution of flip("+
       to_string(dim)+")");

  tuple[dim] = tower.cellMap[dim] = tower.flips[dim].otherFacet(tuple[dim]);
  /* Invalidate cached flips in neighboring dimensions */
  if(dim > 0) tower.flips[dim-1] = FlipI();
  if(dim < cs->maxDimension()) tower.flips[dim+1] = FlipI();

  return *this;
}

} // end namespace ccf

template class ccf::CellStructure<CCIndex>;
template class ccf::FlipTable<CCIndex>;


CCIndexManager CCIndexFactory;

} // end namespace mdx
