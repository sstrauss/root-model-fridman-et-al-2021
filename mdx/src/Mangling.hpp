//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MANGLING_H
#define MANGLING_H

#include <Config.hpp>

#include <string>

class QString;

namespace mdx 
{
  /**
   * Demangle all the words in the string.
   *
   * \returns The string with all symbols demangled.
   */
  mdx_EXPORT QString qdemangle(std::string s);
  /**
   * Demangle all the words in the string.
   *
   * \returns The string with all symbols demangled.
   */
  mdx_EXPORT std::string demangle(std::string s);
}
#endif
