//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MDXProcessMorphogens.hpp>

namespace mdx
{
  // Initialize variables, mesh may have changed. Called for each solve.
  bool MeinhardtAI::initialize(Mesh &_mesh, CellTissue &_tissue, CCIndexDataAttr &_indexAttr, AIAttr &_aiAttr)
  {
    // Set the tissue and attribute maps
    mesh = &_mesh;
    tissue = &_tissue;
    indexAttr = &_indexAttr;
    aiAttr = &_aiAttr;

    // Read the parameters
    processParms();

    return true;
  }

  // Process model parameters
  void MeinhardtAI::processParms()
  {
    // Should attribute be here?
		aProd = parm("AProd").toDouble();
    aaProd = parm("AAProd").toDouble();
    aProdDist = stringToPoint2d(parm("AProdDist"));
    aDecay = parm("ADecay").toDouble();
    aDiff = parm("ADiff").toDouble();
    aNoise = parm("ANoise").toDouble();
    aInit = parm("AInit").toDouble();
    aMax = parm("AMax").toDouble();
  
    hProd = parm("HProd").toDouble();
    haProd = parm("HAProd").toDouble();
    hDecay = parm("HDecay").toDouble();
    hDiff = parm("HDiff").toDouble();
    hInit = parm("HInit").toDouble();
    hMax = parm("HMax").toDouble();
  }

  // Prepare for solve
  void MeinhardtAI::initDerivs(SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr)
  {
    // Find the perimeter and save area for convenience
    tissue->updateGeometry();
    for(CCIndex c: tissue->dualGraph().vertices()) {
      CellData &cAI = (*aiAttr)[c];
      CCIndexData &cIdx = (*indexAttr)[c];
      cAI.area = cIdx.measure;
      cAI.perimeter = 0;

      for(const Flip &flip : neighborVertexFlips(tissue->dualGraph(), c)) {
        CCIndex w = flip.interior;

        CCIndexData &wIdx = (*indexAttr)[w];
        cAI.perimeter += wIdx.measure;
      }
    }

    // Add some noise to the activator
    if(aNoise > 0.0) {
      for(CCIndex c : tissue->dualGraph().vertices())
        (*aiAttr)[c].a += ran(aNoise) * aNoise;
    }
  }

  // Check for invalid concentrations
  bool MeinhardtAI::checkConcentrations(const QString &varName, double &s, double min, double max)
  {
    if(s < min) {
      mdxInfo << QString("%1 concentration less than minimum, %2 < %3")
           .arg(varName).arg(s).arg(min) << endl; 
      s = min;
    }
    if(s > max) {
      mdxInfo << QString("%1 concentration more than maximum, %2 > %3")
           .arg(varName).arg(s).arg(max) << endl; 
      s = max;
    }
    if(isNan(s)) {
      mdxInfo << QString("%1 concentration is Nan").arg(varName) << endl; 
      s = min;
    }
    return true;
  }

  bool MeinhardtAI::printStats()
  {
    if(!aiAttr)
      throw(QString("MeinhardtAI::printStats Attribute map empty"));
    if(!tissue)
      throw(QString("MeinhardtAI::printStats Tissue empty"));

    // Find max concentrations for display and report concentration errors
    double maxA = 0.0, maxH = 0.0;
    forall(CCIndex c, tissue->dualGraph().vertices()) { // loop over cells
      CellData &cAI = (*aiAttr)[c];
      if(cAI.a > maxA) 
        maxA = cAI.a;
      if(cAI.h > maxH) 
        maxH = cAI.h;

      // Check for bad concentrations
      checkConcentrations("Activator", cAI.a, 0, aMax);
      checkConcentrations("Inhibitor", cAI.h, 0, hMax);
    }
    mdxInfo << "Max morphogen conc: " << maxA << " activator, "
                                                << maxH << " inhibitor." << endl;

    // Update the signal on the mesh
    updateSignal();

    return true;
  }

  // Set initial values
  bool MeinhardtAI::initValues()
  {
    if(!aiAttr)
      throw(QString("MeinhardtAI::initValues Attribute map empty"));
    if(!tissue)
      throw(QString("MeinhardtAI::initValues Tissue empty"));

    // Re-process the parms
    processParms();

    aiAttr->clear();

    // Set initial concentrations
    forall(CCIndex c, tissue->dualGraph().vertices()) {
      CellData &cAI = (*aiAttr)[c];
      cAI.a = aInit;
      cAI.h = hInit;
    }

    // Update the signal on the mesh
    updateSignal();

    return true;
  }

  void MeinhardtAI::calcDerivatives(const SolverT &solver, CCIndex c, VectorT &values)
  {
    const CellData &cAI = (*aiAttr)[c];
    const CCIndexData &cIdx = (*indexAttr)[c];

    // Activator
    if(norm(cIdx.pos) >= aProdDist[0] and (aProdDist[1] == 0 or norm(cIdx.pos) <= aProdDist[1]))
      values[0] = aProd;
    else
      values[0] = 0;
    values[0] += aaProd * cAI.a * cAI.a / (1.0 + cAI.h);
    values[0] -= aDecay * cAI.a;

    // Inhibitor
    values[1] = haProd * cAI.a * cAI.a + hProd;
    values[1] -= hDecay * cAI.h;

    // Calculate diffusion
    forall(const Flip &flip, neighborVertexFlips(tissue->dualGraph(), c)) {
      CCIndex w = flip.interior;
      CCIndex n = flip.otherFacet(c);

      CellData &nAI = (*aiAttr)[n];
      CCIndexData &wV = (*indexAttr)[w];
      values[0] += aDiff * (nAI.a - cAI.a) * wV.measure / cIdx.measure;
      values[1] += hDiff * (nAI.h - cAI.h) * wV.measure / cIdx.measure;
    }
  }

  // Calculate Jacobian for cell, RSS Why is this slower?
  void MeinhardtAI::calcJacobian(const SolverT &solver, CCIndex c, MatrixT &j)
  {
    CellData &cAI = (*aiAttr)[c];

    j[0][0] = aaProd * 2.0 * cAI.a/(1.0 + cAI.h) - aDecay - aDiff * cAI.perimeter / cAI.area;
    j[0][1] = -aaProd * cAI.a * cAI.a/((cAI.h + 1.0) * (cAI.h + 1.0));

    j[1][0] = haProd * 2.0 * cAI.a;
    j[1][1] = -hDecay - hDiff * cAI.perimeter / cAI.area;
  }

  // Calculate Jacobian for neighbors
  void MeinhardtAI::calcJacobian(const SolverT &solver, CCIndex c, CCIndex nb, MatrixT &j)
  {
    j[0][1] = 0;
    j[1][0] = 0;

    CCIndex w = tissue->cellStructure().meet(c, nb);
    CCIndexData &wIdx = (*indexAttr)[w];
    CCIndexData &cIdx = (*indexAttr)[c];

    j[0][0] = aDiff * wIdx.measure / cIdx.measure;
    j[1][1] = hDiff * wIdx.measure / cIdx.measure;
  }
  
  void MeinhardtAI::updateSignal(void)
  {
    if(!aiAttr or !indexAttr)
      throw(QString("MeinhardtAI::initValues Attribute map empty"));
    if(!tissue)
      throw(QString("MeinhardtAI::initValues Tissue empty"));

    // Copy results to display arrays
    CCIndexDoubleAttr &activator = mesh->signalAttr<double>("Activator");
    CCIndexDoubleAttr &inhibitor = mesh->signalAttr<double>("Inhibitor");

    // Put the activator in the signal field
    forall(CCIndex c, tissue->dualGraph().vertices()) {
      CellData &cAI = (*aiAttr)[c];
      activator[c] = cAI.a;
      inhibitor[c] = cAI.h;
    }
  }
  
  void MeinhardtAI::Subdivide::splitCellUpdate(Dimension dim, const CCStructure &cs, 
      const CCStructure::SplitStruct& ss, CCIndex otherP, CCIndex otherN, double interpPos)
  {
    if(dim == 2) {
      CellData &xAI = (*aiAttr)[ss.parent];
      CellData &pAI = (*aiAttr)[ss.childP];
      CellData &nAI = (*aiAttr)[ss.childN];
      pAI.a = nAI.a = xAI.a;
      pAI.h = nAI.h = xAI.h;
    }
  }

  bool MeinhardtSolver::initialize(QWidget *parent)
  {
    // Get the current mesh and the Meinhardt cell data attribute map
    Mesh *mesh = currentMesh();
    aiAttr = &mesh->attributes().attrMap<CCIndex, MeinhardtAI::CellData>(parm("Cell Data"));

    // Initialize the tissue
    if(!getProcess(parm("Tissue Process"), tissueProcess))
      throw(QString("MeinhardtAI::initialize Unable to make Tissue Process: %1").arg(parm("Tissue Process")));
    tissueProcess->initialize(parent);

    // Initialize the Meinhardt derivatives process
    if(!getProcess(parm("Meinhardt Process"), meinhardtProcess))
      throw(QString("MeinhardtAI::initialize Unable to make Meinhart AI Process: %1").arg(parm("Meinhardt Process")));
    meinhardtProcess->initialize(*mesh, tissue(), mesh->indexAttr(), *aiAttr);

	  // Register derivative providers and initialize
    clearDerivs();
    addDerivs(meinhardtProcess);
    initSolver(&tissue().dualGraph());

    return true;
  }

  // Run a step of the simulation
  bool MeinhardtSolver::step() 
  {
    // Run the solver
    solve();

    // Print stats if required
    if(stringToBool(parm("Print Meinhardt Stats")))
      meinhardtProcess->printStats();
      
    // Update the GUI
    currentMesh()->updateProperties(tissue().tissueName());

    return true;
  }

  bool MeinhardtSolver::rewind(QWidget *parent)
  {
    // Reset the concentrations
    meinhardtProcess->initValues();

    // Update the GUI
    currentMesh()->updateProperties(tissue().tissueName());

    return true;
  }

  CellTissue &MeinhardtSolver::tissue()
  {
    if(!tissueProcess)
      throw(QString("MeinhardtAI::initValues Attribute map empty"));
    return tissueProcess->tissue();
  }
}
