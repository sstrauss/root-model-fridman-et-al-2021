//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#ifndef MDX_PROCESS_CELL_DIVIDE_HPP
#define MDX_PROCESS_CELL_DIVIDE_HPP

/**
 * \file MDXProcessCellDivide.hpp
 *
 * This file contains the process to divide Tissue cells.  
 */

#include <Process.hpp>
#include <CellTissue.hpp>
#include <CCDivideCell.hpp>
#include <MDXProcessTissue.hpp>

namespace mdx
{
  // Process to perform cell division in a 2D cell complex tissue
  class mdxBase_EXPORT CellTissueCell2dDivide : public Process, virtual public Cell2dDivideParms
  {
  public:
    CellTissueCell2dDivide(const Process &process) : Process(process) 
    {
      setName("03 Divide 2d Cells");
      setDesc("Divide cells Parameters for 2d cell division on tissue mesh.");
      setIcon(QIcon(":/images/CellDivide.png"));

      addParm("Tissue Process", "Name of the Tissue process", "10 Cell Tissue");
    }

    ~CellTissueCell2dDivide() {}

    // Initialize simulation, called from GUI thread
    bool initialize(QWidget* parent);
  
    // Process the parameters
    bool processParms();

    // Run a step of cell division
    bool step() 
    { 
      Mesh *mesh = currentMesh();
      return step(mesh, &subdiv);
    }

    // Run a step of cell division
    virtual bool step(Mesh *mesh, Subdivide *subdiv);

    // Subdivide object
    MDXSubdivide *subdivider() { return &subdiv; }

  private: 
    CellTissue *tissue;
    MDXSubdivide subdiv;

    // Tissue parms process
    CellTissueProcess *cellTissueProcess;

    // Parameters
    QString TissueName;
    QString TissueDualName;
    QString TissueParmsProcessName;

    double CellMaxArea;
    bool Verbose;
  };
}

#endif

