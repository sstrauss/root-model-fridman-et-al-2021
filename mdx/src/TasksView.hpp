//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef TASKSVIEW_H
#define TASKSVIEW_H

#include <Config.hpp>

#include <QTreeView>

class QKeyEvent;

extern const QString internal_format;
extern const QString itemlist_format;

class TasksView : public QTreeView 
{
  Q_OBJECT
public:
  TasksView(QWidget* parent = 0);

protected:
  void keyPressEvent(QKeyEvent* event);
  void dragEnterEvent(QDragEnterEvent* event);
  void dragLeaveEvent(QDragLeaveEvent* event);
  void dragMoveEvent(QDragMoveEvent* event);

signals:
  void deleteItems(const QModelIndexList& idx);
};

#endif
