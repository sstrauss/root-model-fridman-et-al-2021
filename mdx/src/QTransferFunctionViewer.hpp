//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef QTRANSFERFUNCTIONVIEWER_HPP
#define QTRANSFERFUNCTIONVIEWER_HPP

#include <Config.hpp>

#include <TransferFunction.hpp>

#include <QColor>
#include <QLinearGradient>
#include <QPainterPath>
#include <QWidget>
#include <vector>

class QMouseEvent;
class QPixmap;
class QAction;
class QPaintEvent;

namespace mdx {
  class mdx_EXPORT QTransferFunctionViewer : public QWidget {
    Q_OBJECT
  public:
    typedef TransferFunction::Colorf Colorf;
    enum BackgroundType { BG_CHECKS, BG_WHITE, BG_BLACK };
  
    QTransferFunctionViewer(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~QTransferFunctionViewer() {
    }
  
    size_t nbValues() const;
  
    const TransferFunction& transferFunction() const {
      return transfer_fct;
    }
  
  public slots:
    void changeNbValues(int n);
    void reverseFunction();
    void changeTransferFunction(const TransferFunction& fct);
    void setupGradient();
    void setHistogram(const std::vector<double>& h);
    void setBounds(double min, double max);
    void setStickers(const std::vector<double>& s);
    void setMarkerSize(int s);
    void setCheckSize(int s);
    void setBackgroundType(BackgroundType type);
    void setInterpolation(TransferFunction::Interpolation i);
    void setSelectionColor(QColor col);
    void editMarkers();
    void autoAdjust();
  
  signals:
    void changedTransferFunction(const TransferFunction& fct);
  
  protected:
    void paintEvent(QPaintEvent* event);
    void mouseDoubleClickEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void mouseMoveEvent(QMouseEvent* e);
    void resetMouseInteraction();
  
    void prepareHistogram();
  
    void createBackground();
    QPixmap createChecks();
  
    TransferFunction transfer_fct;
    std::vector<double> histogram, hist_values;
    double minValue, maxValue;
    QPainterPath hist_shape;
    bool use_histogram;
    std::vector<double> stickers;
    bool sticking;
    int marker_size;
    bool select_pos;
    double current_pos;
    int bg_size;
    double bg_bright;
    double bg_dark;
    QColor activ_pos_color;
    Colorf saved_color;
    double saved_pos;
    QAction* reverse_act, *edit_markers;
    QLinearGradient gradient;
    BackgroundType bg_type;
    size_t _nb_values;
  };
}
#endif
