//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MDXSubdivide.hpp>
#include <Attributes.hpp>

// Process to perform cell division.
namespace mdx
{
  // Propagate edge data
  bool MDXSubdivide::updateEdgeData(vertex l, vertex v, vertex r, double s)
  {
    // Propagate texture info?
    v->color = (1.0 - s) * l->color + s * r->color;

    return true;
  }

  // Propagate cell data
  bool MDXSubdivide::updateCellData(cell c, cell cl, cell cr)
  {
    IntIntAttr &parents = mesh->labelMap("Parents");
    parents[cl->label] = parents[cr->label] = parents[c->label];

    return true;
  }
}
