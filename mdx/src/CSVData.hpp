//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CSV_DATA_HPP
#define CSV_DATA_HPP

#include <Config.hpp>
#include <CCF.hpp>
#include <Types.hpp>
#include <Attributes.hpp>

namespace mdx
{
  // Simple CSV data manipulation structure
  class CSVData
  {
  protected:
    struct Column
    {
      QString name;                   // Column name
      std::map<QString, std::set<int> > index;    // Index
      bool doIndex = false;           // Is this column indexed
      bool indexValid = false;        // Is this index up-to-date?

      bool operator==(const Column &other) const
      {
        if(name == other.name and index == other.index and doIndex == other.doIndex and indexValid  == other.indexValid)
          return true;
      
        return false;
      }
    }; 

  public:
    /// Number of rows in table
    size_t rows() const;
    /// Number of columns in table
    size_t columns() const;

    /// Const access to a row of the table
    // This must be const to force update through setValue, so that indexes can be maintained
    const QStringList &operator[](uint i) const;

    /// Set a value in the table
    bool setValue(uint row, uint col, const QString &val);

    /// Return the column name
    QString columnName(uint i) const;

    /// Return the column number for a name, or columns() if it doesn't exist
    uint column(const QString &name) const;
  
    /// Return the list of column names
    QStringList columnList() const;
     
    /// Add a column to the table, pad if required
    int addColumn(const QString &name, bool doIndex = false);

    /// Add a row, pad if required, longer is allowed
    bool addRow(const QStringList &row);

    /// Return if index is active
    bool doIndex(uint col) const;

    /// Set and index active
    bool setDoIndex(uint col, bool value);

    /// Rebuild an index
    bool reindex(uint col);

    /// Rebuild all the row indices
    bool reindex();

    /// Find a value in the table
    IntSet find(uint col, const QString &s);

    bool clear();

    bool operator==(const CSVData &other) const
    {
      if(_columns == other._columns and _rows == other._rows)
        return true;
      
      return false;
    }

  private:
    QList<Column> _columns;
    std::vector<QStringList> _rows;
  };
  typedef AttrMap<QString, CSVData> CSVDataAttr; 

  bool readAttr(CSVData &csv, const QByteArray &ba, size_t &pos);
  bool writeAttr(const CSVData &csv, QByteArray &ba);
}

#endif

