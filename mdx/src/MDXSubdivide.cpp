//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MDXSubdivide.hpp>
#include <Attributes.hpp>

namespace mdx
{
  MDXSubdivide::MDXSubdivide(Mesh &_mesh)
  {
    mesh = &_mesh;
    indexAttr = &mesh->indexAttr();

    for(const QString &signal : mesh->signalAttrList())
      attrVec.push_back(&mesh->signalAttr<double>(signal));
  }

  /* Propagate values when splitting a cell*/
  void MDXSubdivide::splitCellUpdate(Dimension dim, const CCStructure &cs,
               const CCStructure::SplitStruct& ss, CCIndex otherP, CCIndex otherN, double interpPos)
  {
    // Return if not set up
    if(!indexAttr) {
      std::cout << "MDXSubdivide:splitCellUpdate Error, indexAttr not set" << std::endl;
      return;
    }
    // First do membrane part

    // Don't propagate MDX info for edges, it will generate tons of attributes that aren't used
    if(dim != 2 and !otherP.isPseudocell() and !otherN.isPseudocell()) {
      CCIndexData &dP = (*indexAttr)[otherP], &dN = (*indexAttr)[otherN], &dV = (*indexAttr)[ss.membrane];

      // Propagate the label
      if(dN.label > 0)
        dV.label = dN.label;
      else if(dP.label > 0)
        dV.label = dP.label;
  
      // Propagate selection
      if(dN.selected and dP.selected)
        dV.selected = true;
      else
        dV.selected = false;
  
      // Update signal maps for membrane
      for(auto attr : attrVec)
        (*attr)[ss.membrane] = (1.0 - interpPos) * (*attr)[otherP] + interpPos * (*attr)[otherN];
    }

    // Now do cell part

    // Don't propagate MDX info for edges, it will generate tons of attributes that aren't used
    if(dim != 1) {
      CCIndexData &eP = (*indexAttr)[ss.childP], &eN = (*indexAttr)[ss.childN], &ePar = (*indexAttr)[ss.parent];
      // Update signal maps for cell
      for(auto attr : attrVec)
        (*attr)[ss.childP] = (*attr)[ss.childN] = (*attr)[ss.parent];
      eP.label = eN.label = ePar.label;
      eP.selected = eN.selected = ePar.selected;
      eP.measure = (1. - interpPos) * ePar.measure;
      eN.measure = interpPos * ePar.measure;
    }

    if(nextSubdivide)
      nextSubdivide->splitCellUpdate(dim,cs,ss,otherP,otherN,interpPos);
  }
}
