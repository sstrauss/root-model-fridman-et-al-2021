//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <StackProcessMeshInteraction.hpp>
#include <Image.hpp>
#include <MeshUtils.hpp>

#include <QFileDialog>
#include <algorithm>
#include <cfloat>

namespace mdx 
{
  // Get triangle list for cuda operations
  static void getTriangleList(const CCStructure &cs, CCIndexDataAttr &indexAttr, const IntSet &labels,
                                                                 HVec3F& pts, HVec3U& tris, HVec3F& nrmls)
  {
    // Get the faces
    const CCIndexVec &faces = activeFaces(cs, indexAttr);
    tris.clear();
    nrmls.clear();
    uint vCount = 0;
    AttrMap<CCIndex, uint> vMap;

    for(CCIndex f : faces) {
      auto &fIdx = indexAttr[f];
      if(labels.size() > 0 and labels.count(fIdx.label) == 0)
        continue;
      CCIndexVec nbs = faceVertices(cs, f);
      if(nbs.size() != 3)
        throw(QString("Mesh has non-triangle face"));
      // Put in vertex map
      for(uint j = 0; j < 3; j++)
        if(vMap.count(nbs[j]) == 0)
          vMap[nbs[j]] = vCount++;
      // Make triangle
      tris.push_back(Point3u(vMap[nbs[0]], vMap[nbs[1]], vMap[nbs[2]]));
      // Use face normal
      nrmls.push_back(Point3f(fIdx.nrml));
    }

    if(tris.size() == 0)
      return;

    // Now fill in vertex list
    pts.resize(vCount);
    for(auto &pr : vMap)
      pts[pr.second] = Point3f(indexAttr[pr.first].pos);
  }

// Get triangle list for cuda operations
  void getTriangleListVolume(const CCStructure &cs, CCIndexDataAttr &indexAttr, int &label,
                                                                 HVec3F& pts, HVec3U& tris, HVec3F& nrmls)
  {
    // Get the faces
    const CCIndexVec &faces = activeFaces(cs, indexAttr);
    tris.clear();
    nrmls.clear();
    uint vCount = 0;
    AttrMap<CCIndex, uint> vMap;

    if(true){ // mesh 3D : look at volumes and labels
      const CCIndexVec &vols = activeVolumes(cs, indexAttr);
      for(CCIndex v : vols) {
        // only consider tris of that volume
        if(label != indexAttr[v].label) continue;

        // check all tris of that volume and correct orientation if necessary
        for(CCIndex f : cs.bounds(v)) {
          auto &fIdx = indexAttr[f];
          auto fVertices = faceVertices(cs, f);
          Point3dVec pVec(fVertices.size());
          auto ro = cs.ro(v, f);
          if(pVec.size() != 3)
            throw(QString("Mesh has non-triangle face"));

          for(uint j = 0; j < 3; j++)
            if(vMap.count(fVertices[j]) == 0)
              vMap[fVertices[j]] = vCount++;

          if(ro == ccf::NEG){
            tris.push_back(Point3u(vMap[fVertices[1]], vMap[fVertices[0]], vMap[fVertices[2]]));
            nrmls.push_back(Point3f(-1*fIdx.nrml));
          } else {
            tris.push_back(Point3u(vMap[fVertices[0]], vMap[fVertices[1]], vMap[fVertices[2]]));
            nrmls.push_back(Point3f(fIdx.nrml));
          }
        }
      }
    }

    if(tris.size() == 0)
      return;

    // Now fill in vertex list
    pts.resize(vCount);
    for(auto &pr : vMap)
      pts[pr.second] = Point3f(indexAttr[pr.first].pos);
  }

  // Get point list for cuda operations
  static void getPointList(const CCStructure &cs, CCIndexDataAttr &indexAttr, HVec3F& pts, HVec3F& nrmls)
  {
    // Get triangle list
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
    pts.resize(vertices.size());
    nrmls.resize(vertices.size());
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); i++) {
      CCIndex v = vertices[i];
      auto &vIdx = indexAttr[v];
      pts[i] = Point3f(vIdx.pos);
      nrmls[i] = Point3f(vIdx.nrml);
    }
  }

  bool Annihilate::run()
  {
    Stack *stack = currentStack();
    if(!stack or stack->empty())
      throw(QString("No current stack"));
    Store* input = stack->currentStore();
    if(!input or input->empty())
      throw(QString("No current store (stack)"));
    Store* output = stack->work();

    Mesh* mesh = currentMesh();
    if(!mesh or mesh->empty())
      throw(QString("No current mesh"));

    QString ccName = mesh->ccName();
    if(ccName.isEmpty()) throw(QString("No mesh selected"));

    CCStructure &cs = mesh->ccStructure(ccName);
    CCIndexDataAttr &indexAttr = mesh->indexAttr();

    bool res = run(input, output, cs, indexAttr, parm("Min Dist").toDouble(), 
           parm("Max Dist").toDouble(), stringToBool(parm("Fill")), parm("Fill Value").toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  } 

  bool Annihilate::run(const Store* input, Store* output, const CCStructure &cs, CCIndexDataAttr &indexAttr, 
                                          double minDist, double maxDist, bool fill, uint fillval)
  {
    HVecUS tdata(input->data());
  
    const Stack* stack = output->stack();
  
    // Data for cuda
    HVec3F pts;     // Points for triangles
    HVec3F nrmls;   // Normals for triangles
  
    // Get point list, nrmls, and bounding box
    getPointList(cs, indexAttr, pts, nrmls);
    BoundingBox3f bBoxW;   // Bounding box in world coordinates
    forall(const Point3f& p, pts)
      bBoxW |= p;
  
    float distance = std::max(0.0f, std::max(float(-minDist), float(-maxDist)));   // negative distance is outside mesh, expand bBox
    if(distance > 0) {
      Point3f distance3f(distance, distance, distance);
      bBoxW[0] -= distance3f;
      bBoxW[1] += distance3f;
    }
    BoundingBox3i bBoxI(stack->worldToImagei(Point3d(bBoxW[0])), stack->worldToImagei(Point3d(bBoxW[1])));
    Point3i imgSize(stack->size());
    bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
    Point3i size(bBoxI.size());
  
    // Buffer for mask
    HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);
  
    progressStart(QString("Annihilating Stack %1").arg(stack->userId()));
  
    // call cuda to get stack mask
    if(nearMeshGPU(bBoxI[0], size, Point3f(stack->step()), Point3f(stack->origin()), pts, nrmls, float(minDist), float(maxDist), mask))
      return false;
    if(!progressAdvance(0))
      userCancel();
  
    size_t xIdx = 0;
    for(int z = 0; z < imgSize.z(); ++z)
      for(int y = 0; y < imgSize.y(); ++y)
        for(int x = 0; x < imgSize.x(); ++x, ++xIdx) {
          // Inside bBox
          if(x >= bBoxI[0].x() and x < bBoxI[1].x() and y >= bBoxI[0].y() and y < bBoxI[1].y()
             and z >= bBoxI[0].z() and z < bBoxI[1].z()) {
            size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
              - bBoxI[0].x();
            if(mask[mIdx]) {
              if(fill)
                output->data()[xIdx] = fillval;
              else
                output->data()[xIdx] = input->data()[xIdx];
              continue;
            }
          }
          if(fill and fillval == 0)
            output->data()[xIdx] = input->data()[xIdx];
          else
            output->data()[xIdx] = 0;
        }
    output->copyMetaData(input);
    output->changed();
    return true;
  }
  REGISTER_PROCESS(Annihilate);
  
  bool stackFromMesh(const Store* input, Store* output, 
            const CCStructure &cs, CCIndexDataAttr &indexAttr, const IntSet &labels, bool fill, uint fillval)
  {
    const Stack* stack = input->stack();
  
    // Data for cuda
    HVec3F pts;     // Points for triangles
    HVec3U tris;    // Triangle vertices
    HVec3F nrmls;   // Normals for triangles
  
    // Get triangle list and bounding box
    getTriangleList(cs, indexAttr, labels, pts, tris, nrmls);
    if(pts.size() == 0)
      return false;
    BoundingBox3f bBoxW;
    forall(const Point3f &p, pts)
      bBoxW |= p;
  
    BoundingBox3i bBoxI(stack->worldToImagei(Point3d(bBoxW[0])), stack->worldToImagei(Point3d(bBoxW[1])) + Point3i(1, 1, 1));
    Point3i imgSize(stack->size());
    bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
    Point3i size = bBoxI.size();
  
    // Buffer for mask
    HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);
  
    if(insideMeshGPU(bBoxI[0], size, Point3f(stack->step()), Point3f(stack->origin()), pts, tris, nrmls, mask))
      return false;
  
    size_t xIdx = 0;
    for(int z = 0; z < imgSize.z(); ++z)
      for(int y = 0; y < imgSize.y(); ++y)
        for(int x = 0; x < imgSize.x(); ++x, ++xIdx) {
          // Inside bBox
          if(x >= bBoxI[0].x() and x < bBoxI[1].x() and y >= bBoxI[0].y() and y < bBoxI[1].y()
             and z >= bBoxI[0].z() and z < bBoxI[1].z()) {
            size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
              - bBoxI[0].x();
            if(mask[mIdx]) {
              if(fill)
                output->data()[xIdx] = fillval;
              else
                output->data()[xIdx] = input->data()[xIdx];
              continue;
            }
          }
          if(fill and fillval == 0)
            output->data()[xIdx] = input->data()[xIdx];
          else
            output->data()[xIdx] = 0;
        }
  
    output->copyMetaData(input);
    output->changed();
    return true;
  }
  
  bool FillStackToMesh::run()
  {
    Stack *stack = currentStack();
    if(!stack or stack->empty())
      throw(QString("No current stack"));
    Store* input = stack->currentStore();
    if(!input or input->empty())
      throw(QString("No current store (stack)"));
    Store* output = stack->work();

    Mesh* mesh = currentMesh();
    if(!mesh or mesh->empty())
      throw(QString("No current mesh"));

    QString ccName = mesh->ccName();
    if(ccName.isEmpty()) throw(QString("No mesh selected"));

    CCStructure &cs = mesh->ccStructure(ccName);
    CCIndexDataAttr &indexAttr = mesh->indexAttr();

    IntSet labels;
    int label = parm("Label").toInt();
    if(label >= 0)
      labels.insert(label);
    bool result = stackFromMesh(input, output, cs, indexAttr, labels, true, parm("Fill Value").toUInt());
    if(result) {
      input->hide();
      output->show();
    }
    return result;
  }
  REGISTER_PROCESS(FillStackToMesh);

  bool TrimStackToMesh::run()
  {
    Stack *stack = currentStack();
    if(!stack or stack->empty())
      throw(QString("No current stack"));
    Store* input = stack->currentStore();
    if(!input or input->empty())
      throw(QString("No current store (stack)"));
    Store* output = stack->work();

    Mesh* mesh = currentMesh();
    if(!mesh or mesh->empty())
      throw(QString("No current mesh"));

    QString ccName = mesh->ccName();
    if(ccName.isEmpty()) throw(QString("No mesh selected"));

    CCStructure &cs = mesh->ccStructure(ccName);
    CCIndexDataAttr &indexAttr = mesh->indexAttr();

    bool result = stackFromMesh(input, output, cs, indexAttr, IntSet(), false, 0);
    if(result) {
      input->hide();
      output->show();
    }
    return result;
  }
  REGISTER_PROCESS(TrimStackToMesh);
  

  bool FillStackToMesh3D::run()
  {

    Stack *stack = currentStack();
    if(!stack or stack->empty())
      throw(QString("No current stack"));
    Store* input = stack->currentStore();
    if(!input or input->empty())
      throw(QString("No current store (stack)"));
    Store* output = stack->work();

    Mesh* mesh = currentMesh();
    if(!mesh or mesh->empty())
      throw(QString("No current mesh"));

    QString ccName = mesh->ccName();
    if(ccName.isEmpty()) throw(QString("No mesh selected"));

    CCStructure &cs = mesh->ccStructure(ccName);
    CCIndexDataAttr &indexAttr = mesh->indexAttr();

    // Data for cuda
    HVec3F pts;     // Points for triangles
    HVec3U tris;    // Triangle vertices
    HVec3F nrmls;   // Normals for triangles
                   //    Point3f bBoxW[2];  // Bounding box in world coordinates
 
    // Get list of labels
    std::set<int> Labels;
    const CCIndexVec &vols = activeVolumes(cs, indexAttr);
    for(CCIndex v : vols) {
      Labels.insert(indexAttr[v].label);
    }


    // Loop over all labels
    forall(int label, Labels) {
      // Get triangle list and bounding box
      getTriangleListVolume(cs, indexAttr, label, pts, tris, nrmls);
      BoundingBox3f bBoxW;
      forall(const Point3f& p, pts)
        bBoxW |= p;

      BoundingBox3i bBoxI(stack->worldToImagei(Point3d(bBoxW[0])), stack->worldToImagei(Point3d(bBoxW[1])) + Point3i(1, 1, 1));
      Point3i imgSize(stack->size());
      bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
      Point3i size = bBoxI.size();

      // Buffer for mask
      HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);
    
      // call cuda to get stack mask
      if(insideMeshGPU(bBoxI[0], size, Point3f(stack->step()), Point3f(stack->origin()), pts, tris, nrmls, mask))
        return false;

      for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
        for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
          size_t xIdx = stack->offset(bBoxI[0].x(), y, z);
          for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
            size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
              - bBoxI[0].x();
            if(mask[mIdx])
              output->data()[xIdx] = label;
          }
        }
    }
    output->copyMetaData(input);
    output->changed();

    return true;
  }
  REGISTER_PROCESS(FillStackToMesh3D);



  bool RandomVoxelInCell3D::run(Stack *stack, CCStructure &cs, CCIndexDataAttr &indexAttr, int label, bool ignoreZ, Point3d& randPoint)
  {



    // Data for cuda
    HVec3F pts;     // Points for triangles
    HVec3U tris;    // Triangle vertices
    HVec3F nrmls;   // Normals for triangles
                   //    Point3f bBoxW[2];  // Bounding box in world coordinates
 
    // Get list of labels
    //std::set<int> Labels;
    const CCIndexVec &vols = activeVolumes(cs, indexAttr);

//std::cout << "cell " << label << std::endl;

    bool exist = false;

    for(CCIndex v : vols) {
      if(indexAttr[v].label == label) exist = true;
      //label = indexAttr[v].label;
    //  Labels.insert(indexAttr[v].label);
    }

    if(!exist){
      std::cout << "no cell with this label" << std::endl;
      return false;
    } 


    // Loop over all labels
    //forall(int label, Labels) {
      // Get triangle list and bounding box



      

      getTriangleListVolume(cs, indexAttr, label, pts, tris, nrmls);
      BoundingBox3f bBoxW;
      forall(const Point3f& p, pts)
        bBoxW |= p;
//std::cout << "tris " << tris.size() << std::endl;
      std::set<Point3i> cellVoxels;

      BoundingBox3i bBoxI(stack->worldToImagei(Point3d(bBoxW[0])), stack->worldToImagei(Point3d(bBoxW[1])) + Point3i(1, 1, 1));
      Point3i imgSize(stack->size());
      bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
      Point3i size = bBoxI.size();
    //std::cout << "size " << size << std::endl;
      // Buffer for mask
      HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);

      // call cuda to get stack mask
      if(insideMeshGPU(bBoxI[0], size, Point3f(stack->step()), Point3f(stack->origin()), pts, tris, nrmls, mask))
        return false;

      for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
        for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
          size_t xIdx = stack->offset(bBoxI[0].x(), y, z);
          for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
            size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
              - bBoxI[0].x();
            if(mask[mIdx]){
              Point3i p(x,y,z);
              if(ignoreZ) p.z()=0;
              cellVoxels.insert(p);
              //output->data()[xIdx] = label;
            }
          }
        }


    //std::cout << "size " << cellVoxels.size() << std::endl;

    int randInt = std::rand() % cellVoxels.size();

    std::set<Point3i>::iterator it = cellVoxels.begin();
    for(int i = 0; i<randInt; i++){
      it++;
    }

    Point3i p = (*it);


    std::cout << "rand p " << stack->imageToWorld(p) << std::endl;

    randPoint = stack->imageToWorld(p);

    //}
    //output->copyMetaData(input);
    //output->changed();

    return true;
  }
  REGISTER_PROCESS(RandomVoxelInCell3D);


  bool RandPointsInCell::run()
  {

    int nrPoints = parm("Number of Points").toInt();

    bool createMesh = stringToBool(parm("Create Point Cloud Mesh"));


      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No mesh selected"));

      QString ccNameMesh ="Rand Points";
      CCStructure &csMesh = mesh->ccStructure(ccNameMesh);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      csMesh = CCStructure(0);
      mesh->updateProperties(ccNameMesh);
      

    // get all parent labels > 0

    Stack *stack = currentStack();
    if(!stack or stack->empty())
      throw(QString("No current stack"));
    
    if(!mesh or mesh->empty())
      throw(QString("No current mesh"));

    auto& randPos = mesh->attributes().attrMap<CCIndex, std::vector<Point3d> >("Volume Random Positions");

    auto &parents = mesh->labelMap("Parents");
    
    QString ccName = mesh->ccName();
    if(ccName.isEmpty()) throw(QString("No mesh selected"));

    CCStructure &cs = mesh->ccStructure(ccName);
    // 
    for(CCIndex v : cs.volumes()){

      if(parents[indexAttr[v].label] < 1) continue;

      for(int i = 0; i < nrPoints; i++){
        Point3d randP(0,0,0);

        RandomVoxelInCell3D rv(*this);
        rv.run(stack, cs, indexAttr, indexAttr[v].label, "Yes", randP);
        randPos[v].push_back(randP);

        if(createMesh){
          CCIndex w = CCIndexFactory.getIndex();
          CCIndexData &W = indexAttr[w];
          W.pos = randP;
          csMesh.addCell(w);
        }

      }


    }    
      if(createMesh){
        mesh->drawParms(ccNameMesh).setGroupVisible("Vertices", true);
        mesh->updateAll(ccNameMesh);
        mesh->setCCName(ccNameMesh);
      } else {
        mesh->erase(ccNameMesh);
      }

    return true;
  }
  REGISTER_PROCESS(RandPointsInCell);

  bool DeleteOutsideMesh::run(Stack* stack, const Store* input, Store* output, 
                         const CCStructure &cs, const CCIndexDataAttr &indexAttr, double threshold)  
  {
    std::vector<Point3d> centersMesh(65536, Point3d());
    std::vector<double> weightsMesh(65536, 0);
  
    // Calculate mesh centers
    for(auto f : cs.faces()) {
      auto fVertices = faceVertices(cs, f);
      if(fVertices.size() != 3) 
        throw QString("%1::run Only implemented for triangle meshes").arg(name());

      // Calculate volume enclosed by mesh and save triangles
      auto &v0Idx = indexAttr[fVertices[0]];
      int label = v0Idx.label;
      if(label <= 0)
        throw QString("%1::run Unlabeled vertex").arg(name());
      auto &v1Idx = indexAttr[fVertices[1]];
      auto &v2Idx = indexAttr[fVertices[2]];
      if(label != v1Idx.label or label != v2Idx.label)
        throw QString("%1::run Multiply labeled cell").arg(name());

      double volume = signedTetraVolume(v0Idx.pos, v1Idx.pos, v2Idx.pos);
      centersMesh[label] += volume * Point3d(v0Idx.pos + v1Idx.pos + v2Idx.pos) / 4.0;
      weightsMesh[label] += volume; 
    }

    #pragma omp parallel for
    for(size_t label = 1; label < 65536; label++)
      if(weightsMesh[label] <= 0)
        continue;
      else
        centersMesh[label] /= weightsMesh[label];

    const HVecUS& inputData = input->data();
    std::vector<Point3d> centersStack(65536, Point3d());
    std::vector<size_t> weightsStack(65536, 0);
    auto size = stack->size();
    for(uint z = 0; z < size.z(); z++)
      for(uint y = 0; y < size.y(); y++)
        for(uint x = 0; x < size.x(); x++) {
          int label = inputData[stack->offset(x,y,z)];
          if(label <= 0)
            continue;
          centersStack[label] += Point3d(stack->imageToWorld(Point3i(x, y, z)));
          weightsStack[label]++;
        }

    #pragma omp parallel for
    for(int label = 0; label < 65536; label++)
      if(weightsStack[label] > 0)
        centersStack[label] /= weightsStack[label];

    HVecUS& outputData = output->data();
    size_t totalSize = size_t(size.x()) * size.y() * size.z();
    #pragma omp parallel for
    for(size_t i = 0; i < totalSize; i++) {
      int label = inputData[i];
      if(label > 0 and weightsMesh[label] > 0 and weightsStack[label] > 0 and norm(centersMesh[label] - centersStack[label]) <= threshold)
        outputData[i] = label;
      else
        outputData[i] = 0;
    }

    output->copyMetaData(input);
    output->changed();
  
    return true;
  }
  REGISTER_PROCESS(DeleteOutsideMesh);

//  bool FillStack3D::run(const Store* input, Store* output, Mesh* mesh)
//  {
//    const Stack* stack = input->stack();
//  
//    setStatus("Filling stack " << stack->userId() << " from 3D mesh");
//  
//    // Data for cuda
//    HVec3F pts;     // Points for triangles
//    HVec3U tris;    // Triangle vertices
//    HVec3F nrmls;   // Normals for triangles
//                    //    Point3f bBoxW[2];  // Bounding box in world coordinates
//  
//    // Get list of labels
//    std::set<int> Labels;
//    forall(const vertex& v, mesh->graph())
//      if(v->label > 0)
//        Labels.insert(v->label);
//  
//    // Loop over all labels
//    progressStart(QString("Filling Stack %1 from 3D mesh").arg(stack->userId()), 0);
//    forall(int label, Labels) {
//      // Get triangle list and bounding box
//      getTriangleList(mesh, pts, tris, nrmls, label);
//      BoundingBox3f bBoxW;
//      forall(const Point3f& p, pts)
//        bBoxW |= p;
//      BoundingBox3i bBoxI(stack->worldToImagei(bBoxW[0]), stack->worldToImagei(bBoxW[1]) + Point3i(1, 1, 1));
//      Point3i imgSize(stack->size());
//      bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
//      Point3i size = bBoxI.size();
//      // Buffer for mask
//      HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);
//  
//      // call cuda to get stack mask
//      if(insideMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, tris, nrmls, mask))
//        return false;
//      if(!progressAdvance(0))
//        userCancel();
//      for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
//        for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
//          size_t xIdx = stack->offset(bBoxI[0].x(), y, z);
//          for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
//            size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
//              - bBoxI[0].x();
//            if(mask[mIdx])
//              output->data()[xIdx] = label;
//          }
//        }
//    }
//    output->copyMetaData(input);
//    output->changed();
//    return true;
//  }
//  REGISTER_PROCESS(FillStack3D);
  
//  bool StackRelabelFromMesh::run(Stack* stack, const Store* store, Store* output, const Mesh* mesh,
//                                        bool delete_unknown)
//  {
//    VVGraphVec regions;
//    VtxIntMap cells;
//    int n = mesh->getConnectedRegions(mesh->graph(), regions, cells);
//    setStatus("Number of cells = " << n);
//    std::vector<Point3f> centers(regions.size());
//    std::vector<ushort> labels(regions.size());
//    std::map<ushort, ushort> label_mapping;
//    Point3u stkSize = stack->size();
//    const HVecUS& in_data = store->data();
//    HVecUS& data = output->data();
//    ushort max_label = 0;
//  
//    for(size_t i = 0; i < regions.size(); ++i) {
//      const vvGraph& S = regions[i];
//      short lab = ushort(S.any()->label & 0xFFFF);
//      if(lab > max_label)
//        max_label = lab;
//      labels[i] = lab;
//      Point3f& c = centers[i];
//      forall(const vertex& v, S)
//        c += Point3f(v->pos);
//      c /= S.size();
//      Point3i imgPos = stack->worldToImagei(c);
//      if(imgPos.x() >= 0 and imgPos.y() >= 0 and imgPos.z() >= 0 and (size_t) imgPos.x() < stkSize.x()
//         and (size_t) imgPos.y() < stkSize.y() and (size_t) imgPos.z() < stkSize.z()) {
//        ushort l = in_data[stack->offset(imgPos)];
//        label_mapping[l] = lab;
//        setStatus("Mapping " << l << " to " << lab);
//      } else
//        setStatus("No mapping for cell labeled " << lab);
//    }
//  
//    for(size_t i = 0; i < data.size(); ++i) {
//      std::map<ushort, ushort>::const_iterator found = label_mapping.find(in_data[i]);
//      if(found != label_mapping.end())
//        data[i] = found->second;
//      else if(delete_unknown)
//        data[i] = 0;
//      else {
//        label_mapping[in_data[i]] = ++max_label;
//        data[i] = max_label;
//      }
//    }
//  
//    output->copyMetaData(store);
//    output->changed();
//  
//    return true;
//  }
//  REGISTER_PROCESS(StackRelabelFromMesh);
}
