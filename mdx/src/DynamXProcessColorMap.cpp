//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <DynamXProcessColorMap.hpp>

namespace mdx 
{
  bool ColorMapProcess::initialize(QStringList& parms, QWidget* parent)
  {
    ColorEditDlg dlg(colorMap, parent);
    connect(&dlg, SIGNAL(update()), this, SLOT(update()));
    dlg.exec();

    return true;
  }

  void ColorMapProcess::update()
  {
    // Update colors here in derived class, and then call this
    updateState();
    updateViewer();
  }
}
