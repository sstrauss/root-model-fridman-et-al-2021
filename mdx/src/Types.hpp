#ifndef TYPES_HPP
#define TYPES_HPP
/**
 * \file Types.hpp
 *
 * Type definitions
 */
#include <Config.hpp>

#include <map>
#include <set>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <QString>

#include <cuda/CudaExport.hpp>
#include <Geometry.hpp>
#include <SetVector.hpp>

#include <CCIndex.hpp>
#include <CCIndexData.hpp>
#include <tbb/concurrent_vector.h>
#include <tbb/concurrent_unordered_set.h>
#include <tbb/concurrent_unordered_map.h>
#include <tbb/scalable_allocator.h>

namespace mdx 
{
  /**
   * Simpler names for the various containers and iterators
   */
  typedef unsigned short ushort;
  typedef unsigned int uint;
  typedef unsigned long ulong;
  
  typedef Vector<2, bool> Point2b;
  typedef Vector<3, bool> Point3b;

  typedef Vector<2, ulong> Point2ul;
  typedef Vector<3, ulong> Point3ul;

  /// Map of an integer to a float
  typedef std::unordered_map<int, float> IntFloatMap;
  /// Element in IntFloatMap
  typedef std::pair<int, float> IntFloatPair;

  /// Map of an integer to a 3D point
  typedef std::unordered_map<int, Point3f> IntPoint3fMap;
  /// Element in IntPoint3fMap
  typedef std::pair<int, Point3f> IntPoint3fPair;

  /// Map of Point3d to int
  typedef std::map<Point3d, int> Point3dIntMap;
  /// Element in Point3dIntMap
  typedef std::pair<Point3d, int> Point3dIntPair;

  /// Map of an integer to a double
  typedef std::unordered_map<int, double> IntDoubleMap;
  /// Element in IntDoubleMap
  typedef std::pair<int, double> IntDoublePair;
  /// Map of an integer to a double 3D point
  typedef std::unordered_map<int, Point3d> IntPoint3dMap;
  /// Element in IntPoint3dMap
  typedef std::pair<int, Point3d> IntPoint3dPair;

  /// Map of an integer to another one
  typedef std::unordered_map<int, int> IntIntMap;
  /// Element in IntIntMap
  typedef std::pair<int, int> IntIntPair;
  /// Map of an integer to a host vector of 3 unsigned integers
  typedef std::unordered_map<int, HVec3U> IntHVec3uMap;
  /// Element in IntHVec3uMap
  typedef std::pair<int, HVec3U> IntHVec3uPair;
  /// Set of integers
  typedef std::set<int> IntSet;
  /// Vector of integers
  typedef std::vector<int> IntVec;
  /// Vector of Point2i
  typedef std::vector<Point2i> Point2iVec;
  /// Vector of Point3i
  typedef std::vector<Point3i> Point3iVec;
  /// Set of unsigned integers
  typedef std::set<uint> UIntSet;
  /// Vector of unsigned integers
  typedef std::vector<uint> UIntVec;
  /// Vector of Point2u
  typedef std::vector<Point2u> Point2uVec;
  /// Vector of Point3u
  typedef std::vector<Point3u> Point3uVec;
  /// Vector of unsigned long integers
  typedef std::vector<ulong> ULongVec;
  /// Vector of Point2u
  typedef std::vector<Point2ul> Point2ulVec;
  /// Vector of Point3u
  typedef std::vector<Point3ul> Point3ulVec;
  /// Set of unsigned short integers
  typedef std::set<ushort> UShortSet;
  /// Vector of unsigned short integers
  typedef std::vector<ushort> UShortVec;
  /// Vector of Point2us
  typedef std::vector<Point2us> Point2usVec;
  /// Vector of Point3us
  typedef std::vector<Point3us> Point3usVec;
  /// Vector of vector of integers
  typedef std::vector<std::vector<int> > IntVecVec;
  /// Vector of floats
  typedef std::vector<float> FloatVec;
  /// Vector of Point2f
  typedef std::vector<Point2f> Point2fVec;
  /// Vector of Point3f
  typedef std::vector<Point3f> Point3fVec;
  /// Vector of double
  typedef std::vector<double> DoubleVec;
  /// Vector of Point2d
  typedef std::vector<Point2d> Point2dVec;
  /// Vector of Point3d
  typedef std::vector<Point3d> Point3dVec;
  /// Vector of Point4d
  typedef std::vector<Point4d> Point4dVec;
  /// Vector of Matrix2d
  typedef std::vector<Matrix2d> Matrix2dVec;
  /// Vector of Matrix3d
  typedef std::vector<Matrix3d> Matrix3dVec;
  /// Vector of Matrix2x3d
  typedef std::vector<Matrix2x3d> Matrix2x3dVec;
  /// Element in IntIntSetMap
  typedef std::pair<int, IntSet> IntIntSetPair;
  /// Map of an integer to a set of integers
  typedef std::map<int, IntSet> IntIntSetMap;
  /// Element in IntIntFloatMap
  typedef std::pair<IntIntPair, float> IntIntFloatPair;
  /// Map of a pair of integers to a float
  //typedef std::map<IntIntPair, float> IntIntFloatMap;
  typedef std::unordered_map<IntIntPair, float> IntIntFloatMap;
  /// Map of an integer to a 3x3 matrix
  typedef std::unordered_map<int, Matrix3f> IntMatrix3fMap;
  /// Element in IntMatrix3x3fMap
  typedef std::pair<int, Matrix3f> IntMatrix3fPair;
  /// Map an integer to a symmetric tensor
  typedef std::unordered_map<int, SymmetricTensor> IntSymTensorMap;
  /// Element in IntSymTensorMap
  typedef std::pair<int, SymmetricTensor> IntSymTensorPair;

  /// Vector of QString
  typedef std::vector<QString> QStringVec;

  /// Vector unsigned integers
  typedef std::vector<uint> UIntVec;
  /// Pair of unsigned integers
  typedef std::pair<uint, uint> UIntPair;
  /// Vector of pairs of unsigned integers
  typedef std::vector<std::pair<uint, uint> > UIntPairVec;
  /// Vector of vector of unsigned integers
  typedef std::vector<std::vector<uint> > UIntVecVec;
  /// Vector of vector of CCIndex
  typedef std::vector<std::vector<CCIndex> > CCIndexVecVec;
  /// Vector of Point3u
  typedef std::vector<Point3u> Point3uVec;
  /// Map of pairs of unsigned integers to unsigned integer
  typedef std::unordered_map<std::pair<uint, uint>, uint> UIntPairUIntMap;
  /// Map of unsigned integer to vector of unsigned integer
  typedef std::unordered_map<uint, std::vector<uint> > UIntUIntVecMap;

  // CCIndex vector
  typedef std::vector<CCIndex> CCIndexVec;
  // CCIndex set
  typedef std::set<CCIndex> CCIndexSet;
  typedef std::unordered_set<CCIndex> CCIndexUSet;
  // CCIndex pair
  typedef std::pair<CCIndex,CCIndex> CCIndexPair;
  typedef std::vector<CCIndexPair> CCIndexPairVec;

  // CCSignedIndex vector
  typedef std::vector<CCSignedIndex> CCSignedIndexVec;
  // CCSignedIndex set
  typedef std::set<CCSignedIndex> CCSignedIndexSet;
  // CCSignedIndex pair
  typedef std::pair<CCSignedIndex,CCSignedIndex> CCSignedIndexPair;

  /// Tbb types

  // Define allocator for tbb
  #define TBBALLOC tbb::scalable_allocator

  typedef tbb::concurrent_vector<int, TBBALLOC<int> > IntTbbVec;
  typedef tbb::concurrent_vector<uint, TBBALLOC<uint> > UIntTbbVec;
  typedef tbb::concurrent_vector<CCIndex, TBBALLOC<CCIndex> > CCIndexTbbVec;
  typedef tbb::concurrent_unordered_set<int, std::hash<int>, std::equal_to<int>, TBBALLOC<int> > IntTbbSet;
  typedef tbb::concurrent_unordered_set<CCIndex, std::hash<CCIndex>, std::equal_to<CCIndex>, TBBALLOC<CCIndex> > CCIndexTbbSet;
  typedef tbb::concurrent_unordered_map<uint, uint, std::hash<uint>, std::equal_to<uint>,
                                             TBBALLOC<std::pair<const uint, uint> > > UIntUIntTbbMap;
  
}
#endif
