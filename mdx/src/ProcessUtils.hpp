//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PROCESS_UTILS_HPP
#define PROCESS_UTILS_HPP

#include <Config.hpp>

#include <Vector.hpp>
#include <Util.hpp>

#include <QStringList>
#include <QHash>

class QTreeWidgetItem;
class QTreeWidget;
class QRegularExpression;

namespace mdx 
{
  /**
   * Find or create a folder from a process tree.
   *
   * \param name Name of the folder to create, sub-folders are separated by '/'
   * \param folders Map of previously created folders
   * \param tree Tree in which the folders are created
   */
  QTreeWidgetItem* getFolder(QString name, QHash<QString, QTreeWidgetItem*> &folders, 
									                                                       QTreeWidget *tree);
  
  /**
   * Show only processes whose name match the regular expression
   *
   * \param tree Tree of processes to filter
   * \param filter Regular expression to filter with
   */
  void filterProcesses(QTreeWidget *tree, const QString &filter_text);


  /**
   * Convert a QString to a Point
   *
   * \param s String to convert
   */
  template <size_t dim, typename T> 
  bool fromQString(Vector<dim, T> &p, const QString &s, QString sep = " ")
  {
    QStringList l = s.split(sep);
    size_t sz = min(dim, size_t(l.size()));
    for(size_t i = 0; i < sz; ++i)
      p[i] = l[i].toDouble();
    if(sz == dim)
      return true;

    return false;
  }
}
#endif

