//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef VIZ_ATTRIBUTE_HPP
#define VIZ_ATTRIBUTE_HPP

#include <boost/variant.hpp>

#include <Attributes.hpp>
#include <ColorMap.hpp>

namespace mdx
{
  // A VizAttribute packs an Attribute that can be visualized into a boost::variant.
  // The attributes that can be visualized are from the key type to something
  // that can be computed by a ColorMap: integers (as colormap indices),
  // doubles (as interpolated ranges), some number of doubles packed up in a PointXd,
  // or colors (as themselves).
  template<typename Key>
  struct VizAttribute
  {
    typedef Key key_type;

    typedef boost::variant<
      const AttrMap<Key,int>&,
      const AttrMap<Key,double>&,
      const AttrMap<Key,Point2d>&,
      const AttrMap<Key,Point3d>&,
      const AttrMap<Key,Colorb>&
      > AttributeType;

    AttributeType attr;

    const ColorMap &colorMap;
    ColorMap cm;

    // Default constructor so that we can avoid pointers
    inline VizAttribute() : colorMap(cm) {}

    // We initialize the VizAttribute with the particular AttrMap we're using
    // and the ColorMap we will use to visualize it.
    template<typename Data>
    inline VizAttribute(const AttrMap<Key,Data> &_attr, const ColorMap &_cm)
      : attr(_attr), colorMap(_cm) {}

    // A boost::variant needs a "visitor": a unary function that takes in
    // any of the possible AttrMaps and returns a color for that attribute.
    struct Visitor : public boost::static_visitor<Colorb>
    {
      const Key &key;
      const ColorMap &colorMap;
      uint channelOffset;
      inline Visitor(const Key &_key, const ColorMap &_cm, uint _offset = 0)
        : key(_key), colorMap(_cm), channelOffset(_offset) {}

      template<typename Data>
      inline Colorb operator()(const AttrMap<Key,Data> &am) const
      {
        return colorMap.getColor(channelOffset,am[key]);
      }
    };

    // To evaluate the VizAttribute at a given key, we initialize
    // the visitor with the color map and the query index
    // then visit attr to get its color.
    inline Colorb operator[](const Key &key) const
    {
      return boost::apply_visitor(Visitor(key,colorMap) , attr);
    }

    // We also implement this as a method getColor which we can use to
    // access the channel-offset versions of getColor in ColorMap.
    inline Colorb getColor(const Key &key) const { return getColor(0,key); }
    inline Colorb getColor(uint channelOffset, const Key &key) const
    {
      return boost::apply_visitor(Visitor(key,colorMap,channelOffset) , attr);
    }
  };
} // end namespace mdx

#endif // VIZ_ATTRIBUTE_HPP
