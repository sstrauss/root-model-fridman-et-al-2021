//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MISC_H
#define MISC_H
/**
 * \file Misc.hpp
 *
 * Misc. definitions and utilities
 */
#include <Config.hpp>
#include <GL.hpp>

#include <Color.hpp>
#include <MDXViewer/qglviewer.h>
#include <Vector.hpp>
#include <Types.hpp>
#include <Version.hpp>

#include <QDir>
#include <QList>
#include <QtCore>

namespace mdx 
{
  static const QString UM(QString::fromWCharArray(L"\xb5m"));             // um
  static const QString UM2(QString::fromWCharArray(L"\xb5m\xb2"));        // um^2
  static const QString UM3(QString::fromWCharArray(L"\xb5m\xb3"));        // um^3
  static const QString UM_1(QString::fromWCharArray(L"\xb5m\x207B\xb9")); // um^-1
  static const QString UM_2(QString::fromWCharArray(L"\xb5m\x207B\xb2")); // um^-2
  
  typedef Vector<2, int> Point2i;
  typedef Vector<3, GLuint> Point3GLui;
  typedef Vector<4, GLuint> Point4GLui;
  typedef Vector<3, GLubyte> Point3GLub;
  typedef Vector<4, GLubyte> Point4GLub;
  typedef Color<float> Colorf;
  typedef Vector<3, float> Point3f;
  
  /// Returns resource directory
  mdx_EXPORT QDir resourceDir();
  /// Returns user processes directory
  mdx_EXPORT QList<QDir> userProcessDirs();
  /// Returns processes directory
  mdx_EXPORT QList<QDir> processDirs();
  /// Returns the libraries directory
  mdx_EXPORT QDir libDir();
  /// Returns the includes directory
  mdx_EXPORT QDir includeDir();
  /// Returns the documentation directory
  mdx_EXPORT QDir docDir();
  
  /// Map unique colors to indices
  inline Point3GLub vMapColor(uint u)
  {
    u++;
    return (Point3GLub(u / (256 * 256), u / 256 % 256, u % 256));
  }

  /// Map unique colors to indices
  inline uint vMapColor(Point3GLub& p) 
  {
    uint result = uint(p.x()) * 256 * 256 + uint(p.y()) * 256 + uint(p.z());
    if(result == 0)
      return UINT_MAX;
    return result - 1;
  }
  
  /// Create representation of a string that can be written in a single line, without spaces
  inline QString shield(QString s)
  {
    s.replace("\\", "\\\\");
    s.replace(" ", "\\s");
    s.replace("\n", "\\n");
    s.replace("\t", "\\t");
    s.replace("\"", "\\\"");
    s.replace("\'", "\\\'");
    return s;
  }
  
  /// Retrieve a string that has been shielded with shield
  inline QString unshield(QString s)
  {
    s.replace("\\\'", "\'");
    s.replace("\\\"", "\"");
    s.replace("\\s", " ");
    s.replace("\\n", "\n");
    s.replace("\\t", "\t");
    s.replace("\\\\", "\\");
    return s;
  }
  
  /// Shield a string to send it to the python interpreter
  inline QString shield_python(QString s)
  {
    s.replace("\\", "\\\\");
    s.replace("\n", "\\n");
    s.replace("\t", "\\t");
    s.replace("\"", "\\\"");
    s.replace("\'", "\\\'");
    return s;
  }
  
  /// Retrieve a string that is retrieved from the python representation
  inline QString unshield_python(QString s)
  {
    s.replace("\\\'", "\'");
    s.replace("\\\"", "\"");
    s.replace("\\n", "\n");
    s.replace("\\t", "\t");
    s.replace("\\\\", "\\");
    return s;
  }

  /// Returns true if \c string correspond to the main store, false otherwise
  inline bool stringToMainStore(const QString& string)
  {
    QString s = string.toLower();
    if(s == "main")
      return true;
    return false;
  }
  
  /// Returns true if \c string correspond to the work store, false otherwise
  inline bool stringToWorkStore(const QString& string)
  {
    QString s = string.toLower();
    if(s == "work")
      return true;
    return false;
  }
  
  /// Helper function converting a string into a boolean.
  inline bool stringToBool(const QString& string)
  {
    static QStringList t = QStringList() << "t" << "true" << "on" << "yes" << "y" << "1";
    return t.contains(string.toLower());
  } 

  /// Helper function converting a boolean into a string
  inline QString boolToString(bool b) { return (b ? "Yes" : "No"); }

  /// Convert a string to another type
  template<typename VectorT>
  VectorT stringTo(const QString &s)
  {
    QStringList sl = s.split(" ");
    size_t sz = sl.size();
    if(sz > VectorT::numElems)
      sz = VectorT::numElems;
    VectorT result;
    for(size_t i = 0; i < sz; ++i)
      result[i] = sl[i].toDouble();
    return result; 
  }

  inline Point2i stringToPoint2i(const QString &s) { return stringTo<Point2i>(s); }
  inline Point2u stringToPoint2u(const QString &s) { return stringTo<Point2u>(s); }
  inline Point2f stringToPoint2f(const QString &s) { return stringTo<Point2f>(s); }
  inline Point2d stringToPoint2d(const QString &s) { return stringTo<Point2d>(s); }

  inline Point3i stringToPoint3i(const QString &s) { return stringTo<Point3i>(s); }
  inline Point3u stringToPoint3u(const QString &s) { return stringTo<Point3u>(s); }
  inline Point3f stringToPoint3f(const QString &s) { return stringTo<Point3f>(s); }
  inline Point3d stringToPoint3d(const QString &s) { return stringTo<Point3d>(s); }
  inline Point4d stringToPoint4d(const QString &s) { return stringTo<Point4d>(s); }

  inline bool isGuiThread()
  {
    if(QThread::currentThread() == QCoreApplication::instance()->thread())
      return true;
    return false;
  }

  inline QList<int> extractVersion(QIODevice &file)
  {
    QByteArray version;
    version.reserve(10);
    do {
      char c;
      qint64 qty = file.read(&c, 1);
      if(qty == 0) {
        throw QString("extractVersion:Could not read enough data from file "
          "(read = %1, expected = %2)").arg(version.size()).arg(version.size() + 1);
      }
      version.push_back(c);
    } while(version[version.size() - 1] != ' ');
    version.chop(1);
    QList<QByteArray> values = version.split('.');
    QList<int> result;
    foreach(const QByteArray &a, values) {
      bool ok;
      result << a.toInt(&ok);
      if(!ok)
        throw QString("extractVersion:Version number must contain only "
          "integers (read = '%1')").arg(QString(a));
    }
    return result;
  }

  // Calculate the median of a container optionally specifying if it is already sorted or not
  template<typename T>
  typename T::value_type median(T &data, bool sorted = false)
  {
    if(data.size() == 0)
      return typename T::value_type();
    else if(data.size() == 1)
      return *data.begin();

    int pos = data.size() / 2;
    if(sorted) {
      auto iter = data.begin();
      std::advance(iter, pos);
      if(pos % 2 == 0)
        return (*iter++ + *iter) / 2;
      else
        return *iter;
    } else {
      // If not sorted already copy to vector and sort
      std::vector<typename T::value_type> sdata(data.begin(), data.end());
      std::sort(sdata.begin(), sdata.end());
      if(pos % 2 == 0)
        return (sdata[pos] + sdata[pos+1]) / 2;
      else
        return sdata[pos] ;
    }
  }
}
#endif
