//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <Curvature.hpp>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>

namespace mdx
{
  bool calcCurvature(const Point3dVec &pos, const Point3dVec &nrml, Matrix2x3d &eVecs, Point2d &eVals)
  {
    try {
      // Weingarten Matrix
      CIMat W(2, 2);
    
      // Local Basis Vectors
      Point3d b1;
      Point3d b2;
      Point3d b3;
    
      // Principal directions of curvature
      Point3d &ed1 = eVecs[0];
      Point3d &ed2 = eVecs[1];
    
      // Principal values of curvature
      double &ev1 = eVals[0];
      double &ev2 = eVals[1];
     
      if(nrml.size() == 0 or nrml.size() != pos.size()) {
        std::cout << "calcCurvature Invalid size, pos:" << pos.size() << " nrmls:" << nrml.size() << std::endl;
        return false;
      }
  
      // calculate local basis with b1,b2, in-plane and b3 out-of-plane basis vectors
      Point3d normal = normalized(nrml[0]);
      do {
        b1 = Point3d(cimg_library::cimg::rand(), cimg_library::cimg::rand(), cimg_library::cimg::rand());
        b1 = b1 / b1.norm();
      } while(fabs(b1 * normal) == 1);
      b1 = b1 - (b1 * normal) * normal;
      b1 = b1 / b1.norm();
      b2 = b1.cross(normal);
      b3 = normal;
  
      // Set up Least Square Problem
      int cnt = pos.size();
      CIMat X = CIMat(7, 3 * (cnt - 1));
      CIMat b = CIMat(1, 3 * (cnt - 1));
      Point3d vpos = pos[0];
      int id = 0;
  
      for(int i = 1; i < cnt; i++) {
        // Transform into local coordinates
        Point3d vw = pos[i] - vpos;
        double w1 = vw * b1;
        double w2 = vw * b2;
        double w3 = vw * b3;
  
        // Transform into local coordinates
        double n1, n2, n3;
        n1 = nrml[i] * b1;
        n2 = nrml[i] * b2;
        n3 = nrml[i] * b3;
  
        X(0, id) = 0.5 * w1 * w1;
        X(1, id) = w1 * w2;
        X(2, id) = 0.5 * w2 * w2;
        X(3, id) = w1 * w1 * w1;
        X(4, id) = w1 * w1 * w2;
        X(5, id) = w1 * w2 * w2;
        X(6, id) = w2 * w2 * w2;
        b(id) = w3;
        id++;
  
        X(0, id) = w1;
        X(1, id) = w2;
        X(2, id) = 0;
        X(3, id) = 3 * w1 * w1;
        X(4, id) = 2 * w1 * w2;
        X(5, id) = w2 * w2;
        X(6, id) = 0;
        b(id) = -n1 / n3;
        id++;
  
        X(0, id) = 0;
        X(1, id) = w1;
        X(2, id) = w2;
        X(3, id) = 0;
        X(4, id) = w1 * w1;
        X(5, id) = 2 * w1 * w2;
        X(6, id) = 3 * w2 * w2;
        b(id) = -n2 / n3;
        id++;
      }
      CIMat Xt = X.get_transpose();
  
      CIMat Q = Xt * X;
  
      b = Xt * b;
  
      double det = Q.det();
      if(!isNan(det) && !(det == 0)) {
        // solve least-squares problem
        b.solve(Q);
  
        // update Weingarten-matrix

// RSS For some reason vertices randomly crap out with messages saying the eigenvalues are complex
// GSL seems to eliminate this problem
//        W(0, 0) = b[0];
//        W(1, 0) = W(0, 1) = b[1];
//        W(1, 1) = b[2];
//        // calculate eigenvalues and eigenvectors
//        CIMatL bL = W.get_symmetric_eigen();
//        ev1 = - bL(0)(0, 0);// ALR: bulges in direction of increasing Z should be positive
//        ev2 = - bL(0)(1, 0);// Sorry Pierre, it makes more sense this way!
//        ed1 = - bL(1)(0, 0) * b1 - bL(1)(0, 1) * b2;
//        ed2 = - bL(1)(1, 0) * b1 - bL(1)(1, 1) * b2;
    gsl_matrix* mat = gsl_matrix_alloc(2, 2);
    gsl_vector* eval = gsl_vector_alloc(2);
    gsl_matrix* evec = gsl_matrix_alloc(2, 2);
    gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc(2);
    gsl_matrix_set(mat, 0, 0, b[0]);
    gsl_matrix_set(mat, 0, 1, b[1]);
    gsl_matrix_set(mat, 1, 0, b[1]);
    gsl_matrix_set(mat, 1, 1, b[2]);
    gsl_eigen_symmv(mat, eval, evec, w);
    gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_DESC);
      
    // The sign is  backward from the mathematical definition
    ed1 = -gsl_matrix_get(evec, 0, 0) * b1 - gsl_matrix_get(evec, 0, 1) * b1;
    ed2 = -gsl_matrix_get(evec, 1, 0) * b1 - gsl_matrix_get(evec, 1, 1) * b1;
    ev1 = -gsl_vector_get(eval, 0);
    ev2 = -gsl_vector_get(eval, 1);

// Sorted already above by GSL
//        if(fabs(ev1) < fabs(ev2)) {
//          using std::swap;
//          swap(ev1, ev2);
//          swap(ed1, ed2);
//        }
      }
      else
        mdxInfo << "calcCurvature Error computing computing curvature, nhbd size:" << pos.size() << endl; 
    } catch (...) {
      mdxInfo << "calcCurvature Error computing computing curvature, nhbd size:" << pos.size() << endl; 
      return false;
    }
    return true;
  };
}
