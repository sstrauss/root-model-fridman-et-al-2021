//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef TOOLS_PROCESS_CSV_HPP
#define TOOLS_PROCESS_CSV_HPP

/**
 * \file ToolsProcessCSV.hpp
 *
 * Processes for cell tissues
 */

#include <Process.hpp>
#include <CSVData.hpp>
#include <CSVDataWindow.hpp>

namespace mdx
{
  class LoadCSVFile : public Process
  {
  public:
    LoadCSVFile(const Process& process) : Process(process)
    {
      setName("Tools/CSV/Load CSV File");
      setDesc("Read a CSV file into a CSV data attribute");

      addParm("File Name", "Name of CSV file", "");
      addParm("CSV Attribute", "Name of CSV attribute", "CSV");
    }
    bool run();
  };

  class SaveCSVFile : public Process
  {
  public:
    SaveCSVFile(const Process& process) : Process(process)
    {
      setName("Tools/CSV/Save CSV File");
      setDesc("Write a CSV data attribute to file");

      addParm("File Name", "Name of CSV file", "");
      addParm("CSV Attribute", "Name of CSV attribute", "CSV");
    }
    bool run();
  };

  class MakeCSVLabeling : public Process
  {
  public:
    MakeCSVLabeling(const Process& process) : Process(process)
    {
      setName("Tools/CSV/Make CSV Labeling");
      setDesc("Use a column of CSV data to make a labeling");

      addParm("Labeling Column", "Name of column to use for labeling file", "");
      addParm("Labeling Name", "Labeling name, empty for same as column name", "");
      addParm("Label Column", "Name of column that holds label", "Label");
      addParm("CSV Attribute", "Name of CSV attribute", "CSV");
    }
    bool run();
  };

  class ViewCSVData : public Process
  {
  public:
    ViewCSVData(const Process& process) : Process(process)
    {
      setName("Tools/CSV/View CSV Data");
      setDesc("Create a window to view the CSV data");

      addParm("CSV Attribute", "Name of CSV attribute", "CSV");
    }
    bool initialize(QWidget *parent);

  public slots:
    void selectLabels(IntVec labels);

  private:
    Mesh *mesh = 0;
    QString ccName;
  };
}
#endif

