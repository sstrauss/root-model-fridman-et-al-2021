//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessMeasuringTools.hpp>
#include <MeshUtils.hpp>

namespace mdx 
{
  bool VertexPairDistance::run(Mesh &mesh, const CCStructure &cs)
  {
    auto &indexAttr = mesh.indexAttr();
    auto vertices = selectedVertices(cs, indexAttr);
    if(vertices.size() != 2)
      throw QString("%1::run There must be exactly 2 vertices selected").arg(name());

    double distance = norm(indexAttr[vertices[0]].pos - indexAttr[vertices[1]].pos); 

    setStatus(QString("Distance between vertices is: %1").arg(distance));
    return true;
  }
  REGISTER_PROCESS(VertexPairDistance);
}
