//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#ifndef MDX_PROCESS_TISSUE_HPP
#define MDX_PROCESS_TISSUE_HPP

/**
 * \file MDXProcessTissue.hpp
 *
 * This file contains the process to manage the cell tissue. If the mesh has no
 * dual graph, one will be created
 */

#include <Process.hpp>
#include <CellTissue.hpp>

namespace mdx
{
  // Process to hold tissue parms
  class mdxBase_EXPORT CellTissueProcess : public Process, public virtual TissueParms
  {
  public:
    CellTissueProcess(const Process &process) : Process(process) 
    {
      setName("Cell Tissue");
      setDesc("Parameters for cellular tissue mesh.");
      setIcon(QIcon(":/images/CellDivide.png"));
    }

    // Initialize tissue, called from GUI thread
    bool initialize(QWidget* parent)
    {
      Mesh *mesh = currentMesh();
      if(!mesh)
        throw(QString("CellTissueProcess::initialize No current mesh"));

      return initialize(*mesh);
    }
    bool initialize(Mesh &mesh);
  
    /// Run, do nothing
    bool run() { return true; }

    /// Rewind, do nothing
    bool rewind(QWidget* parent) { return true; }
 
    /// Return cell tissue object
    CellTissue &tissue() { return cellTissue; }

    /// Return pointer to mesh
    Mesh *mesh() { return m; }

    /// Return the tissue name
    const QString &tissueName() const { return TissueName; }

    /// Return the tissue dual name
    const QString &tissueDualName() const { return TissueDualName; }

  private:
    QString TissueName;
    QString TissueDualName;

    Mesh *m = 0;
    CellTissue cellTissue;
  };
}

#endif

