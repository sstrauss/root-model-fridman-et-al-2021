//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESSS_PDG_HPP
#define MESH_PROCESSS_PDG_HPP

#include <Process.hpp>

#include <MeshProcessCellMesh.hpp>

namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{
  /**
   * \class CorrespondenceJunctions ProcessPDG.hpp <MeshProcessPDG.hpp>
   */
  class mdxBase_EXPORT CorrespondenceJunctions : public Process 
  {
  public:
    CorrespondenceJunctions(const Process& process) : Process(process) {}
  
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));

      Mesh* mesh1, *mesh2;
      if(currentMesh() == mesh(0)) {
        mesh1 = mesh(0);
        mesh2 = mesh(1);
      } else if(currentMesh() == mesh(1)) {
        mesh2 = mesh(0);
        mesh1 = mesh(1);
      } else
        return false;
  
      bool res = run(mesh1, mesh2, stringToBool(parms[0]), stringToBool(parms[1]));
      return res;
    }
  
    bool run(Mesh* mesh1, Mesh* mesh2, bool ShowVVCorrespondence, bool convert);  //Added convert parameter to enable choice of converting into a cell mesh
    bool checkNhbd(Mesh* mesh1, Mesh* mesh2, vvGraph &T1, vvGraph &T2, bool ShowVVCorrespondence);  //Original Correspondence Junctions  

    QString name() const { return "Mesh/Cell Axis/PDG/Check Correspondence"; }
    QString description() const { return 
      "Find matching cell junctions between 2 meshes based on parent labeling.\n"
      "Both meshes are simplified with Make Cells to keep only the cell junctions and centers.\n"
      "SAVE your meshes before running!"; }
    QStringList parmNames() const { return QStringList() 
      << "Show correspondence" <<"Convert to Cell Mesh"; }
    QStringList parmDescs() const { return QStringList() 
      << "Draw connecting lines between corresponding junctions." <<"Convert the meshes into cell meshes"; }
    QStringList parmDefaults() const { return QStringList() << "No" << "No"; }
    ParmChoiceMap parmChoice() const
    {
      ParmChoiceMap map;
      map[0] = map[1] = booleanChoice();
      return map;
    }
    QIcon icon() const { return QIcon(":/images/CorrespondenceJunctions.png"); }
  };
  
  /**
   * \class GrowthDirections ProcessPDG.hpp <MeshProcessPDG.hpp>
   */
  class mdxBase_EXPORT GrowthDirections : public Process 
  {
  public:
    GrowthDirections(const Process& process) : Process(process) {}
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
  
      Mesh* mesh1, *mesh2;
      if(currentMesh() == mesh(0)) {
        mesh1 = mesh(0);
        mesh2 = mesh(1);
      } else if(currentMesh() == mesh(1)) {
        mesh2 = mesh(0);
        mesh1 = mesh(1);
      } else
        return false;
  
      bool res = run(mesh1, mesh2);
      return res;
    }
  
    bool run(Mesh* mesh1, Mesh* mesh2);
  
    QString name() const { return "Mesh/Cell Axis/PDG/Compute Growth Directions"; }
    QString description() const { return 
      "Compute PDGs based on correspondence between junctions.\n"
      "Adapted from Goodall and Green, 'Quantitative Analysis of Surface Growth.'"
      "Botanical Gazette (1986) \n"
      "and Dumais and Kwiatkowska, 'Analysis of surface growth in shoot apices.'"
      "Plant Journal (2002)"; }
    QStringList parmNames() const { return QStringList(); }
    QStringList parmDescs() const { return QStringList(); }
    QIcon icon() const { return QIcon(":/images/PDG.png"); }
  };
  
  /**
   * \class DisplayPDGs ProcessPDG.hpp <MeshProcessPDG.hpp>
   */
  class mdxBase_EXPORT DisplayPDGs : public Process 
  {
  public:
    enum ParmNames { pDisplayHeatMap,	pScaleHeatMap, pRangeHeatLow, pRangeHeatHigh, pDisplayAxis,
		  pColorPos, pColorNeg, pAxisWidth, pAxisScale, pAxisOffset, pAnisotropyThreshold, pNumParms};		

    DisplayPDGs(const Process& process) : Process(process) {}
   
	  bool initialize(QStringList& parms, QWidget* parent);

		// Process the parameters
    void processParms(const QStringList &parms);
  
    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, parms);
    }
 
    bool run(Mesh* mesh, const QStringList &parms) 
    {
      processParms(parms);
      return run(mesh, DisplayHeatMap, ScaleHeatMap, RangeHeat, DisplayAxis,
			  ColorPos, ColorNeg, AxisWidth, AxisScale, AxisOffset, AnisotropyThreshold);
    }


    bool run(Mesh* mesh, const QString displayHeatMap, const QString scaleHeatMap, const Point2d &rangeHeat,
		  const QString displayPDGs, const QColor& colorPos, const QColor& colorNeg, float axisLineWidth, 
      float scaleAxisLength, float axisOffset, float anisotropyThreshold);
  
    QString name() const { return "Mesh/Cell Axis/PDG/Display Growth Directions"; }
    QString description() const { return "Display the principle growth directions"; }
    QStringList parmNames() const 
		{ 
		  QVector <QString> vec(pNumParms);
      vec[pDisplayHeatMap]      = "Heatmap"; 
      vec[pScaleHeatMap]        = "ScaleHeat";
      vec[pRangeHeatLow]        = "Heat min";
      vec[pRangeHeatHigh]       = "Heat max"; 
      vec[pDisplayAxis]         = "Show Axis";
      vec[pColorPos]            = "Color +";
      vec[pColorNeg]            = "Color -"; 
      vec[pAxisWidth]           = "Line Width";
      vec[pAxisScale]           = "Line Scale";
      vec[pAxisOffset]          = "Line Offset";
      vec[pAnisotropyThreshold] = "Threshold";
			return vec.toList();
    }

    QStringList parmDescs() const 
		{ 
		 QVector <QString> vec(pNumParms);
		 vec[pDisplayHeatMap]      = "Display stretch ratio values in max or min direction as a color map.\n" "stretch ratio = (length after/length before). No deformation means stretch ratio = 1.";
		 vec[pScaleHeatMap]        = "Scale heat map";
		 vec[pRangeHeatLow]        = "High bound heat map";
		 vec[pRangeHeatHigh]       = "Low bound heat map";
     vec[pDisplayAxis]         = "Draw directions as vectors, scaled by strain.\n" "strain = (stretch ratio - 1). No deformation means strain = 0.";
     vec[pColorPos]            = "Color used for expansion (strain > 0)" ;
		 vec[pColorNeg]            = "Color used for shrinkage (strain < 0)";
     vec[pAxisWidth]           = "Line Width" ;
		 vec[pAxisScale]           = "Length of the vectors = Scale * Strain.";
     vec[pAxisOffset]          = "Draw the vector ends a bit tilted up for proper display on surfaces.";
     vec[pAnisotropyThreshold] = "Minimal value of anisotropy (= stretchMax/StretchMin) required for drawing PDGs.\n" "Use a value above 1."; 
		 return vec.toList();
    }

    QStringList parmDefaults() const 
		{ 
		 QVector <QString> vec(pNumParms);
     vec[pDisplayHeatMap]      = "StretchMax";
		 vec[pScaleHeatMap]        = "Auto";
		 vec[pRangeHeatLow]        = "1";
		 vec[pRangeHeatHigh]       = "3";
		 vec[pDisplayAxis]         = "Both"; 
		 vec[pColorPos]            = "white";
		 vec[pColorNeg]            = "red";
		 vec[pAxisWidth]           = "2.0"; 
		 vec[pAxisScale]           = "10.0"; 
		 vec[pAxisOffset]          = "0.1";
		 vec[pAnisotropyThreshold] = "0.0";
		 return vec.toList();
    }

    ParmChoiceMap parmChoice() const
    {
      ParmChoiceMap map;
			
			map[pDisplayHeatMap] = 
			  QStringList() 
        << "None" 
				<< "StretchMax" 
				<< "StretchMin" 
				<< "Aniso=StretchMax/StretchMin"
        << "ProductStretches"
				<< "StretchBezierX"
				<< "StretchBezierY"
				<< "ShearBezier";
			map[pDisplayAxis] = 
			  QStringList() 
			  << "Both" 
				<< "StrainMax" 
				<< "StrainMin" 
				<< "StrainBezierX" 
				<< "StrainBezierY"  
				<< "None";
		  map[pScaleHeatMap] = 
			  QStringList() 
				<< "None"
				<< "Auto"
				<< "Manual";
      map[pColorPos] = QColor::colorNames();
      map[pColorNeg] = QColor::colorNames();
      return map;
    }
    QIcon icon() const { return QIcon(":/images/PDG.png"); }

  private:
    QString DisplayHeatMap;
		QString ScaleHeatMap;
		Point2d RangeHeat;
		QString DisplayAxis;
		QColor ColorPos;
		QColor ColorNeg;
		float AxisWidth;
    float AxisScale;
		float AxisOffset;
		float AnisotropyThreshold;
  };
  ///@}
}
#endif
