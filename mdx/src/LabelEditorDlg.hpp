//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef LABELEDITORDLG_HPP
#define LABELEDITORDLG_HPP

#include <Config.hpp>

#include <Color.hpp>
#include <Misc.hpp>

#include <QAbstractListModel>
#include <vector>
#include <QDialog>

class QAbstractButton;
class QPoint;

class LabelModel : public QAbstractListModel 
{
public:
  LabelModel(std::vector<mdx::Colorf>* colors);

  int rowCount(const QModelIndex& parent = QModelIndex()) const;
  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return 1;
  }

  Qt::ItemFlags flags(const QModelIndex& index) const;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);

  void apply();
  void reset();

  void makeGray();
  void makeRandom();

  void setNbColors(int n);

  bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex());
  bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex());

protected:
  std::vector<mdx::Colorf>* toChange;
  std::vector<mdx::Colorf> localCopy;
};

namespace Ui {
class LabelEditorDlg;
} // namespace Ui

class LabelEditorDlg : public QDialog {
  Q_OBJECT
public:
  LabelEditorDlg(std::vector<mdx::Colorf>* colors, QWidget* parent);
  ~LabelEditorDlg();

public slots:
  void importLabels();
  void exportLabels();
  bool importLabels(QString filename);
  bool exportLabels(QString filename);
  void makeLabelCurrent();
  void selectLabel();
  void setCurrentLabel(int label);

protected slots:
  void on_buttonBox_clicked(QAbstractButton* btn);
  void on_setNbColors_clicked();
  void on_labelsView_doubleClicked(const QModelIndex& idx);
  void on_labelsView_customContextMenuRequested(const QPoint& pos);
  void on_showCurrent_clicked();
  void on_makeGray_clicked();
  void on_makeRandom_clicked();
  void changeNbItems();

signals:
  void update();
  void selectLabel(int label, int repeat, bool replaceSelection);
  void makeLabelCurrent(int label);

protected:
  int currentLabel, selectedLabel;
  LabelModel* _model;
  Ui::LabelEditorDlg* ui;
  QPushButton* importButton, *exportButton;
  QAction* selectLabelAction, *makeLabelCurrentAction;
};

#endif // LABELEDITORDLG_HPP
