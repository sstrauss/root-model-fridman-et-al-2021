//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef ATTRIBUTES_HPP
#define ATTRIBUTES_HPP

/**
 * \file Attributes.hpp
 *
 * This file contains the attribute system
 */

#include <Config.hpp>

#include <QFile>
#include <QTextStream>

#include <Types.hpp>
#include <Information.hpp>
#include <Progress.hpp>
#include <Colors.hpp>
#include <CCIndex.hpp>
#include <Hash.hpp>
#include <tbb/concurrent_unordered_map.h>

#include <omp.h>

#include <Features.hpp>

// GCC support for C++11 lacks is_trivially_copyable before 5.x (which exactly?)
#if __cplusplus >= 201103L && !__GNUC_LESS(5, 0)
  #include <type_traits>
  using std::is_trivially_copyable;
#else
  #include <tr1/type_traits>
  #define is_trivially_copyable std::tr1::has_trivial_copy
#endif

#include <Common.hpp>

// Use TBB concurrent unordered map
#define ATTR_MAP tbb::concurrent_unordered_map

template<bool, typename _Tp = void>
struct enable_if {};

template<typename _Tp>
struct enable_if<true, _Tp> { typedef _Tp type; };

/** Determines if template arguments are plain old data (POD) types.
 * TODO: what exactly does it mean? That they are always save/loadable to/from memory dump?
 */
#define IS_POD(T) typename enable_if<is_trivially_copyable<T>::value, T>::type

namespace  mdx
{
  typedef Vector<2, Colorb> Vec2Colorb;
  typedef Vector<3, Colorb> Vec3Colorb;

  namespace
  {
    // Functions to read/write QByteArray

    /** @brief Read POD type from QByteArray
     * This function does not care about endianness.
     * @param data: output object
     * @param bd: input byte array
     * @param pos: in/out: current position in the array
     */
    template<typename T>
    bool readPOD(IS_POD(T) &data, const QByteArray &ba, size_t &pos)
    {
      if(qint64(pos + sizeof(T)) > ba.size())
        throw(QString("ReadPOD Error reading past end of attribute"));

      memcpy(&data, ba.data() + pos, sizeof(T));
      pos += sizeof(T);
      return true;
    }

    // Read a vector of POD type from QByteArray (use readAttr instead for matrices)
    template<typename T>
    bool readPODVector(IS_POD(T) &data, size_t n, const QByteArray &ba, size_t &pos)
    {
      if(qint64(pos + sizeof(T) * n) > ba.size())
        throw(QString("ReadPODVector Error reading past end of attribute"));

      memcpy(&data, ba.data() + pos, sizeof(T) * n);
      pos += sizeof(T) * n;
      return true;
    }

    inline bool readChar(void *data, uint sz, const QByteArray &ba, size_t &pos)
    {
      if(qint64(pos + sz) > ba.size())
        throw(QString("ReadChar Error reading past end of attribute"));

      memcpy(data, ba.data() + pos, sz);
      pos += sz;
      return true;
    }

    // Write to QByteArray
    template<typename T>
    bool writePOD(const IS_POD(T) &data, QByteArray &ba)
    {
      QByteArray qba(sizeof(T), 0);
      memcpy(qba.data(), &data, sizeof(T));
      ba.append(qba);
      return true;
    }

    template<typename T>
    bool writePODVector(const IS_POD(T) &data, size_t n, QByteArray &ba)
    {
      QByteArray qba(sizeof(T) * n, 0);
      memcpy(qba.data(), &data, sizeof(T) * n);
      ba.append(qba);
      return true;
    }

    // Write an array of char
    inline bool writeChar(const void *data, size_t sz, QByteArray &ba)
    {
      QByteArray qba(sz, 0);
      memcpy(qba.data(), data, sz);
      ba.append(qba);
      return true;
    }

    // File read/write functions

    // Write some (POD) data to a file
    template <typename T>
    bool writeFile(QIODevice &file, const T &data)
    {
      size_t qty = file.write((char*)&data, sizeof(T));
      if(qty != sizeof(T))
        throw QString("Attributes::write:Error on write");
      return true;
    }

    // Write a standard vector to a file
    template <typename T>
    bool writeFile(QIODevice &file, const std::vector<T> &data)
    {
      size_t sz = data.size();
      writeFile(file, sz);
      for(size_t i = 0; i < sz; i++)
        writeFile(file, data[i]);
      return true;
    }

    // Write some data to a file
    inline bool writeFile(QIODevice &file, char *data, size_t size)
    {
      size_t qty = file.write(data, size);
      if(qty != size)
        throw QString("Attributes::write:Error on write");
      return true;
    }

    // Write a QString to a file
    inline bool writeFileQString(QIODevice &file, const QString &s)
    {
      QByteArray ba = s.toUtf8();
      uint sz = ba.size();
      return writeFile(file, sz) and writeFile(file, ba.data(), sz);
    }

    // Specialize for QString. We need to do this for any other complex types required
    inline bool writeFile(QIODevice &file, const QString &s)
    {
      return writeFileQString(file, s);
    }

    // Read some (POD) data from a file
    template <typename T>
    bool readFile(QIODevice &file, T &data)
    {
      size_t count = file.read((char*)&data, sizeof(T));
      if(count != sizeof(T))
        return false;

      return true;
    }

    // Read a standard vector from a file
    template <typename T>
    bool readFile(QIODevice &file, std::vector<T> &data)
    {
      size_t sz;
      bool result = readFile(file, sz);
      data.reserve(data.size() + sz);
      for(size_t i = 0; i < sz; i++) {
        T value;
        result &= readFile(file, value);
        data.push_back(value);
      }
      return result;
    }

    // Read a QString from a file
    inline bool readFileQString(QIODevice &file, QString &s)
    {
      uint sz;
      if(!readFile(file, sz))
        return false;
      QByteArray ba(sz, 0);
      if(file.read(ba.data(), sz) != sz)
        return false;
      s = QString::fromUtf8(ba);
      return true;
    }

    // Specialize for QString. We need to do this for any other complex types required
    inline bool readFile(QIODevice &file, QString &s)
    {
      return readFileQString(file, s);
    }
  }

  /**
    * Read the attribute value from a QByteArray
    */
  template<typename T>
  bool readAttr(T &data, const QByteArray &ba, size_t &pos)
  {
    return readPOD<T>(data, ba, pos);
  }

  template<size_t n, size_t m, typename T>
  bool readAttr(Matrix<n, m, T> &data, const QByteArray &ba, size_t &pos)
  {
    for(size_t i = 0; i < n; ++i)
      if(!readPODVector<T>(data[i][0], m, ba, pos))
        return false;
    return true;
  }

  template<size_t n, typename T>
  bool readAttr(Vector<n, T> &data, const QByteArray &ba, size_t &pos)
  {
    return readPODVector<T>(data[0], n, ba, pos);
  }

  template<size_t n>
  bool readAttr(Vector<n, Colorb> &data, const QByteArray &ba, size_t &pos)
  {
    for(size_t i = 0; i < n; ++i)
      if(!readChar(&data[i][0], sizeof(Colorb), ba, pos))
        return false;
    return true;
  }

  template<typename T, typename U>
  bool readAttr(std::pair<T,U> &data, const QByteArray &ba, size_t &pos)
  {
    bool ok = readAttr(data.first,ba,pos);
    if(ok) ok = readAttr(data.second,ba,pos);
    return ok;
  }

  template<>
  inline bool readAttr(SymmetricTensor &data, const QByteArray &ba, size_t &pos)
  {
    bool ok = readPOD<Point3d>(data.ev1(), ba, pos);
    if(ok) ok = readPOD<Point3d>(data.ev2(), ba, pos);
    if(ok) ok = readPOD<Point3d>(data.evals(), ba, pos);
    return ok;
  }

  // Specialize for cell index and data
  template<>
  bool inline readAttr(CCIndex &c, const QByteArray &ba, size_t &pos)
  {
    uint index = 0;
    if(!readPOD<uint>(index, ba, pos))
      return false;
    c = CCIndex(index);

    return true;
  }
  
  template<>
  bool inline readAttr(CCSignedIndex &c, const QByteArray &ba, size_t &pos)
  {
    ccf::RO ro;
    if(!readPOD<ccf::RO>(ro, ba, pos))
      return false;

    CCIndex idx;
    if(!readAttr(idx, ba, pos))
      return false;

    c = ro * idx;
    return true;
  }

  template<>
  inline bool readAttr(CCIndexData &m, const QByteArray &ba, size_t &pos)
  {
    return readChar((char *)&m, sizeof(CCIndexData), ba, pos);
  }

  // Specialize for QString
  template<>
  inline bool readAttr(QString &data, const QByteArray &ba, size_t &pos)
  {
    // get the string size
    //#pragma GCC diagnostic push
    //#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
    uint sz = 0;
    if(!readPOD<uint>(sz, ba, pos))
      return false;

    // Now read the data
    QByteArray qba(sz, 0);
    readChar(qba.data(), sz, ba, pos);
    data = QString::fromUtf8(qba);
    //#pragma GCC diagnostic pop
    return true;
  }
  // Specialize for QStringList
  template<>
  inline bool readAttr(QStringList &data, const QByteArray &ba, size_t &pos)
  {
    // get the stringList size
    uint sz = 0;
    if(!readPOD<uint>(sz, ba, pos))
      return false;
    // Loop over the QStringList
    for(uint i = 0; i < sz; i++) {
      QString s;
      if(!readAttr(s, ba, pos))
        return false;
      data << s;
    }

    return true;
  }

  // Specialize for QByteArray
  template<>
  inline bool readAttr(QByteArray &qba, const QByteArray &ba, size_t &pos)
  {
    // get the data size
    uint sz = 0;
    if(!readPOD<uint>(sz, ba, pos))
      return false;

    // Now read the data
    qba.resize(sz);
    readChar(qba.data(), sz, ba, pos);
    return true;
  }


  // Specialize for Colorb
  template<>
  inline bool readAttr(Colorb &color, const QByteArray &ba, size_t &pos)
  {
    if(!readChar(&color[0], sizeof(Colorb), ba, pos))
      return false;
    return true;
  }

  // Standard vectors
  template <typename T>
  inline bool readAttr(std::vector<T> &data, const QByteArray &ba, size_t &pos)
  {
    // get the data size
    uint n = 0;
    if(!readPOD<uint>(n, ba, pos))
      return false;

    data.resize(n);
    for(size_t i = 0; i < n; ++i)
      if(!readAttr(data[i], ba, pos))
        return false;
    return true;
  }

  // Standard maps
  template <typename KeyT, typename ValueT>
  inline bool readAttr(std::map<KeyT, ValueT> &data, const QByteArray &ba, size_t &pos)
  {
    // get the data size
    uint n = 0;
    if(!readPOD<uint>(n, ba, pos))
      return false;

    for(size_t i = 0; i < n; ++i) {
      KeyT key;
      if(!readAttr(key, ba, pos))
        return false;
      ValueT value;
      if(!readAttr(value, ba, pos))
        return false;
      data[key] = value;
    }
    return true;
  }

  template <typename KeyT, typename ValueT>
  inline bool readAttr(std::unordered_map<KeyT, ValueT> &data, const QByteArray &ba, size_t &pos)
  {
    // get the data size
    uint n = 0;
    if(!readPOD<uint>(n, ba, pos))
      return false;

    for(size_t i = 0; i < n; ++i) {
      KeyT key;
      if(!readAttr(key, ba, pos))
        return false;
      ValueT value;
      if(!readAttr(value, ba, pos))
        return false;
      data[key] = value;
    }
    return true;
  }

  /**
    * Write the attribute value from a QByteArray
    */
  template<typename T>
  bool writeAttr(const T &data, QByteArray &ba)
  {
    return writePOD<T>(data, ba);
  }

  template<size_t n, size_t m, typename T>
  bool writeAttr(const Matrix<n, m, T> &data, QByteArray &ba)
  {
    for(size_t i = 0; i < n; ++i)
      if(!writePODVector<T>(data[i][0],  m, ba))
        return false;
    return true;
  }

  template<size_t n, typename T>
  bool writeAttr(const Vector<n, T> &data, QByteArray &ba)
  {
    return writePODVector<T>(data[0], n, ba);
  }


  template<size_t n>
  bool writeAttr(const Vector<n, Colorb> &data, QByteArray &ba)
  {
    for(size_t i = 0; i < n; ++i)
      if(!writeChar(&data[i][0], sizeof(Colorb), ba))
        return false;
    return true;
  }

  template<typename T,typename U>
  bool writeAttr(const std::pair<T,U> &data, QByteArray &ba)
  {
    bool ok = writeAttr(data.first,ba);
    if(ok) ok = writeAttr(data.second,ba);
    return ok;
  }

  template<>
  inline bool writeAttr(const SymmetricTensor &data, QByteArray &ba)
  {
    bool ok = writePOD<Point3d>(data.ev1(), ba);
    if(ok) ok = writePOD<Point3d>(data.ev2(), ba);
    if(ok) ok = writePOD<Point3d>(data.evals(), ba);
    return ok;
  }

  // Specialize for QString
  template<>
  inline bool writeAttr(const QString &s, QByteArray &ba)
  {
    QByteArray qba = s.toUtf8();
    uint sz = qba.size();
    return writeChar((char *)&sz, sizeof(uint), ba)
        and writeChar((char *)qba.data(), sz, ba);
  }

  // Specialize for QStringList
  template<>
  inline bool writeAttr(const QStringList &s, QByteArray &ba)
  {
    bool result = true;
    uint sz = s.size();
    writeAttr(sz, ba);
    for(uint i = 0; i < sz; i++)
      result &= writeAttr(s[i], ba);
    return result;
  }

  // Specialize for QByteArray
  template<>
  inline bool writeAttr(const QByteArray &qba, QByteArray &ba)
  {
    uint sz = qba.size();
    return writeChar((char *)&sz, sizeof(uint), ba)
        and writeChar((char *)qba.data(), sz, ba);
  }

  // Specialize for Colorb
  template<>
  inline bool writeAttr(const Colorb &color, QByteArray &ba)
  {
    if(!writeChar(&color[0], sizeof(Colorb), ba))
      return false;
    return true;
  }

  // Standard vectors
  template <typename T>
  bool writeAttr(const std::vector<T> &data, QByteArray &ba)
  {
    uint n = data.size();
    if(!writeAttr(n, ba))
      return false;

    for(size_t i = 0; i < n; ++i)
      if(!writeAttr(data[i], ba))
        return false;
    return true;
  }

  // Standard maps
  template <typename KeyT, typename ValueT>
  bool writeAttr(const std::map<KeyT, ValueT> &data, QByteArray &ba)
  {
    uint n = data.size();
    if(!writeAttr(n, ba))
      return false;

    for(typename std::map<KeyT, ValueT>::const_iterator i = data.begin(); i != data.end(); ++i) {
      if(!writeAttr(i->first, ba))
        return false;
      if(!writeAttr(i->second, ba))
        return false;
    }
    return true;
  }

  template <typename KeyT, typename ValueT>
  bool writeAttr(const std::unordered_map<KeyT, ValueT> &data, QByteArray &ba)
  {
    uint n = data.size();
    if(!writeAttr(n, ba))
      return false;

    for(typename std::unordered_map<KeyT, ValueT>::const_iterator i = data.begin(); i != data.end(); ++i) {
      if(!writeAttr(i->first, ba))
        return false;
      if(!writeAttr(i->second, ba))
        return false;
    }
    return true;
  }
  template<>
  inline bool writeAttr(const CCIndex &c, QByteArray &ba)
  {
    uint val = c.value;
    return writePOD<uint>(val, ba);
  }

  template<>
  inline bool writeAttr(const CCSignedIndex &c, QByteArray &ba)
  {
    if(!writePOD<ccf::RO>(c.orientation() , ba))
      return false;
    return writeAttr(~c,ba);
  }

  template<>
  inline bool writeAttr(const CCIndexData &m, QByteArray &ba)
  {
    return writeChar((char *)&m, sizeof(CCIndexData), ba);
  }

  /**
   * \class AttrMapBase Attributes.hpp <Attributes.hpp>
   *
   * Base class for attribute maps to allow generic pointers
   */
  class AttrMapBase 
  {
  public:
    virtual ~AttrMapBase() {}
  };

  /**
   * \class AttrMap Attributes.hpp <Attributes.hpp>
   * \brief Attribute map wraps std::map
   *
   * The following customizations are made to std::map's behavior:
   * - you can assign a default value, which is normally obtained with the default constructor
   * - if you read an entry (const []) that does not exist, it does not create a new one, it
   *   returns a reference to the default value.
   * - when new entries are added to the map, the insert is locked from other threads
   *
   * Note that this does not make the class thread safe, but reads and writes that go to
   * different entries are safe.
   */
  template<typename KeyT, typename ValueT>
  class AttrMap : public AttrMapBase
  {
  public:
    typedef std::pair<KeyT, ValueT> pair;
    typedef typename ATTR_MAP<KeyT, ValueT>::iterator iterator;
    typedef typename ATTR_MAP<KeyT, ValueT>::const_iterator const_iterator;
    typedef KeyT key_type;
    typedef ValueT value_type;

    // Constructor
    AttrMap() : _defaultVal(ValueT()) {}

    // Destructor
    ~AttrMap() {}

    explicit AttrMap(const ValueT &defaultVal) : _defaultVal(defaultVal) {}

    // Range constructor
    template<typename IterT>
    AttrMap(IterT first, IterT last, const ValueT &defaultVal = ValueT()) : _defaultVal(defaultVal)
    {
      _map.insert(first, last);
    }

    // Copy constructor
    AttrMap(const AttrMap &a) : _map(a._map), _defaultVal(a._defaultVal) {}

    // Assignment
    AttrMap &operator=(const AttrMap &a)
    {
      _map = a._map;
      _defaultVal = a._defaultVal;

      return *this;
    }

    /**
     * const Accessor
     *
     * If value does not exist, return a reference to the default value. Unlike map,
     * this method is const
     */
    const ValueT &operator[](const KeyT &key) const
    {
      const_iterator it = _map.find(key);
      if(it == _map.end())
        return _defaultVal;
      return it->second;
    }

    /**
     * Accessor
     *
     * Put a lock around this for omp. Note that this will not protect an iterator somewhere
     * else.
     */
    ValueT &operator[](const KeyT &key)
    {
      return _map[key];
    }

    /**
     * Counterpart to map::at(), which returns reference to default value if key is not found.
     */
    const ValueT &at(const KeyT &key) const
    {
      return (*this)[key];
    }

    /**
     * Get a reference to the default value
     */
    const ValueT &defaultVal() const
    {
      return _defaultVal;
    };
    ValueT &defaultVal()
    {
      return _defaultVal;
    };

    // The rest are boilerplate methods.
    size_t size() const { return _map.size(); }
    bool empty() const { return _map.empty(); }
    size_t count (const KeyT &key) const { return _map.count(key); }
    iterator begin() { return _map.begin(); }
    const_iterator begin() const { return _map.begin(); }
    iterator end() { return _map.end(); }
    const_iterator end() const { return _map.end(); }
    iterator find(const KeyT &key) { return _map.find(key); }
    const_iterator find(const KeyT &key) const { return _map.find(key); }

    // RSS we need to clear the cache if the methods exist
    void erase(iterator pos) { _map.unsafe_erase(pos); }
    size_t erase(const KeyT &key) { return _map.unsafe_erase(key); }
    void erase(iterator first, iterator last) { _map.unsafe_erase(first, last); }

    void clear() { _map.clear(); }
    template<typename IterT>
    void insert(IterT first, IterT last) { _map.insert(first, last); }

  private:
    ATTR_MAP<KeyT, ValueT, std::hash<KeyT> > _map;
    ValueT _defaultVal;
  };

  // Convenience typedefs
  typedef AttrMap<int, int> IntIntAttr;
  typedef AttrMap<int, float> IntFloatAttr;
  typedef AttrMap<int, double> IntDoubleAttr;
  typedef AttrMap<int, Point2d> IntPoint2dAttr;
  typedef AttrMap<int, Point3f> IntPoint3fAttr;
  typedef AttrMap<int, Point3d> IntPoint3dAttr;
  typedef AttrMap<int, Colorb> IntColorbAttr;
  typedef AttrMap<int, Vec2Colorb> IntVec2ColorbAttr;
  typedef AttrMap<int, Vec3Colorb> IntVec3ColorbAttr;
  typedef AttrMap<int, Matrix3f> IntMatrix3fAttr;
  typedef AttrMap<int, Matrix3d> IntMatrix3dAttr;
  typedef AttrMap<int, Matrix2x3d> IntMatrix2x3dAttr;
  typedef AttrMap<int, SymmetricTensor> IntSymTensorAttr;
  typedef AttrMap<int, QString> IntQStringAttr;
  typedef AttrMap<IntIntPair, float> IntIntFloatAttr;
  typedef AttrMap<IntIntPair, Point3f> IntIntPoint3fAttr;

  typedef AttrMap<QString, int> QStringIntAttr;
  typedef AttrMap<QString, float> QStringFloatAttr;
  typedef AttrMap<QString, Point3f> QStringPoint3fAttr;
  typedef AttrMap<QString, Colorb> QStringColorbAttr;
  typedef AttrMap<QString, Vec2Colorb> QStringVec2ColorbAttr;
  typedef AttrMap<QString, Vec3Colorb> QStringVec3ColorbAttr;
  typedef AttrMap<QString, Matrix3f> QStringMatrix3fAttr;
  typedef AttrMap<QString, SymmetricTensor> QStringSymTensorAttr;
  typedef AttrMap<QString, QString> QStringQStringAttr;

  typedef AttrMap<CCIndex, CCIndexData> CCIndexDataAttr;
  typedef AttrMap<CCIndex, int> CCIndexIntAttr;
  typedef AttrMap<CCIndex, bool> CCIndexBoolAttr;
  typedef AttrMap<CCIndex, double> CCIndexDoubleAttr;
  typedef AttrMap<CCIndex, Point2d> CCIndexPoint2dAttr;
  typedef AttrMap<CCIndex, Point3d> CCIndexPoint3dAttr;
  typedef AttrMap<CCIndex, Colorb> CCIndexColorbAttr;
  typedef AttrMap<CCIndex, Matrix3d> CCIndexMatrix3dAttr;
  typedef AttrMap<CCIndex, SymmetricTensor> CCIndexSymTensorAttr;
  typedef AttrMap<CCIndex, QString> CCIndexQStringAttr;
  typedef AttrMap<CCIndex, CCIndex> CCIndexCCIndexAttr;
  typedef AttrMap<CCIndex, CCIndexPair> CCIndexCCIndexPairAttr;
  typedef AttrMap<CCIndex, CCIndexVec> CCIndexCCIndexVecAttr;

  typedef AttrMap<CCSignedIndex, CCIndexData> CCSignedIndexDataAttr;
  typedef AttrMap<CCSignedIndex, int> CCSignedIndexIntAttr;
  typedef AttrMap<CCSignedIndex, bool> CCSignedIndexBoolAttr;
  typedef AttrMap<CCSignedIndex, float> CCSignedIndexFloatAttr;
  typedef AttrMap<CCSignedIndex, double> CCSignedIndexDoubleAttr;
  typedef AttrMap<CCSignedIndex, Point2d> CCSignedIndexPoint2dAttr;
  typedef AttrMap<CCSignedIndex, Point3d> CCSignedIndexPoint3dAttr;
  typedef AttrMap<CCSignedIndex, Colorb> CCSignedIndexColorbAttr;
  typedef AttrMap<CCSignedIndex, Vec2Colorb> CCSignedIndexVec2ColorbAttr;
  typedef AttrMap<CCSignedIndex, Vec3Colorb> CCSignedIndexVec3ColorbAttr;
  typedef AttrMap<CCSignedIndex, Matrix3d> CCSignedIndexMatrix3dAttr;
  typedef AttrMap<CCSignedIndex, SymmetricTensor> CCSignedIndexSymTensorAttr;
  typedef AttrMap<CCSignedIndex, QString> CCSignedIndexQStringAttr;

  /**
   * \class Attributes Attributes.hpp <Attributes.hpp>
   * \brief Holds a set of attributes
   *
   * Attributes are a collection of maps (Attribute objects).
   * FIXME: what are the constraints on attributes?
   */
  class mdx_EXPORT Attributes
  {
  public:
    static int majorVersion, minorVersion;
    static bool versionAtLeast(int major, int minor)
    {
      if(majorVersion < major)
        return false;
      if(minorVersion < minor)
        return false;
  
      return true;
    } 

  private:
    // These next two classes are only used from Attributes
    /**
     * \class AttributeBase Atributes.hpp <Attributes.hpp>
     * \brief Base class for attributes system.
     */
    class AttributeBase
    {
    public:
      /**
       * Default constructor of named attribute
       */
      AttributeBase(const QString &name) : _name(name)
      {
        //mdxInfo << "Creating attribute:" << _name << endl; //NB
      }

      /**
       * Attribute classes are derived
       */
      virtual ~AttributeBase()
      {
        //mdxInfo << "Deleting attribute:" << _name << endl;
      }

      /**
       * Get the name of the Attribute
       */
      const QString &name() { return _name; }

      /**
       * Write the attribute to a byte array
       */
      virtual bool write(QByteArray &) { return true; }

      /**
       * Clear the map
       */
      virtual void clear() {}

      /**
       * Size
       */
      virtual size_t size() = 0;

      /**
       * Return base pointer to the map
       */
      virtual AttrMapBase *attrMapBase()  = 0;

    private:
      QString _name;
    };
    /**
     * \class Attribute Attributes.hpp <Attributes.hpp>
     * \brief Derived template class for attributes system.
     *
     * Holds a map of key-value pairs.
     */
    template <typename KeyT, typename ValueT>
    class Attribute : public AttributeBase
    {
    public:
      /**
       * Constructor
       * \param name Name of the attribute
       */
      Attribute(const QString &name, const ValueT &defaultVal = ValueT())
        : AttributeBase(name), _map(defaultVal) {}

      /**
       * Destructor
       */
      ~Attribute() {}

      /**
       * Return the map
       */
      AttrMap<KeyT, ValueT> &map() { return _map; }
      const AttrMap<KeyT, ValueT> &map() const { return _map; }

      /**
       * Return base pointer to the map
       */
      AttrMapBase *attrMapBase() { return &_map; }

      /**
       * Write the attribute map a file
       * \param file The file to write
       */
      bool write(QByteArray &ba)
      {
        ba.clear();

        // Count the non-default entries
        size_t count = 0;
        for(typename AttrMap<KeyT, ValueT>::iterator i = _map.begin(); i != _map.end(); ++i)
          if(i->second == _map.defaultVal())
            continue;
          else
            count++;

        // Write the count
        writeAttr(count, ba);

        // Now write the map
        for(typename AttrMap<KeyT, ValueT>::iterator i = _map.begin(); i != _map.end(); ++i) {
          // Ony write the non-default entries.
          if(i->second == _map.defaultVal())
            continue;

          // Write the key and value types
          writeAttr(i->first, ba);
          writeAttr(i->second, ba);
        }

        // Write the default value
        writeAttr(_map.defaultVal(), ba);

        return true;
      }

      /**
       * Read the attribute map from the QByteArray that was loaded from the file
       *
       * \param file The file to write
       */
      bool read(QByteArray &ba, bool fixedSize, const QString &name)
      {
        try {

          // Read the count
          size_t pos = 0;
          size_t count = 0;
          if(!readAttr(count, ba, pos))
            return false;

          #pragma GCC diagnostic push
          #pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
          // Read in the map
          if(fixedSize and count > 0) {
            // Read an entry to get the size
            size_t attrSize = pos;
            KeyT key;
            readAttr(key, ba, pos);
            ValueT value;
            readAttr(value, ba, pos);
            _map[key] = value;
            attrSize = pos - attrSize;
        
					  // For fixed size data we can parse it in parallel
            bool readErr = false;
            QString errMsg;
            #pragma omp parallel for
            for(size_t i = 0; i < count-1; ++i) {
              if(!readErr) {
                try {
                  size_t p = pos + attrSize * i;
                  KeyT key;
                  readAttr(key, ba, p);
                  ValueT value;
                  readAttr(value, ba, p);
                  _map[key] = value;
                } catch (QString &msg) {
                  errMsg = msg;
                  #pragma omp atomic write
                  readErr = true;
                } catch(...) {
                  errMsg = "Unknown exception";
                  #pragma omp atomic write
                  readErr = true;
                }
              }
            }
            if(readErr) {
              std::cout << "Error reading attribute " << name.toStdString() <<  ": " << errMsg.toStdString() << std::endl;
              return false;
            }
            pos += attrSize * (count - 1);
          } else {
            for(size_t i = 0; i < count; ++i) {
              KeyT key;
              readAttr(key, ba, pos);
              ValueT value;
              readAttr(value, ba, pos);
              _map[key] = value;
            }
          
            // Read the default value
            ValueT value;
            if(!readAttr(value, ba, pos))
              return false;
            _map.defaultVal() = value;
          }
        } catch (QString &msg) {
          std::cout << "Error reading attribute " << name.toStdString() <<  ": " << msg.toStdString() << std::endl;
          return false;
        } catch(...) {
          std::cout << "Error reading attribute " << name.toStdString() << ": Unknown exception." << std::endl;
          return false;
        }
        #pragma GCC diagnostic pop

        return true;
      }

      /**
       * Clear the attribute map
       */
      void clear()
      {
        _map.clear();
      }

      /**
       * Size
       */
      virtual size_t size() { return _map.size(); }


    private:
      AttrMap<KeyT, ValueT> _map;
    };

  public:
    /**
     * Constructor
     * \param name Name of the attribute
     */
    Attributes() : _attributes(0), _saveRequired(true) {}

    /**
     * Destructor
     */
    virtual ~Attributes ()
    {
      clear();
    }

    /**
     * Get the attribute, if it does not exist create it and add to the set
     * If it exists, and if so load it from the byte array
     * \param name Name of the attribute
     */
    template <typename KeyT, typename ValueT>
    AttrMap<KeyT, ValueT>&attrMap(const QString &name, bool saveRequired = true)
    {
      if(name.isEmpty())
        throw(QString("Attribute::attrMap Attribute Map name empty")); 
      Attribute<KeyT, ValueT> *a = dynamic_cast<Attribute<KeyT, ValueT> *>(_attributes[name]);
      if(!a) {
        // If the previous attribute with this name was a different type, delete it
        // and print a warning, if it is a system attribute throw an error
        if(_attributes[name]) {
          mdxInfo << "Warning, changing type for attribute: " << name << endl;
          delete _attributes[name];
        }
        a = new Attribute<KeyT, ValueT>(name);
        _attributes[name] = a;
      }
      // Check if attribute is loaded from byte array
      if(_ba[name].size() > 0) {
        // If there is a read error, it probably means it has a different layout, clear it
        // RSS FIXME Only CCIndex uses parallel parsing? Can we fix this? (type traits?)
        if(!a->read(_ba[name], name == "CCIndexData" or name == "CCSignedIndexData", name)) {
          a->clear();
          mdxInfo << "Read error for attribute: " << name << ", clearing" << endl;
        } else
          mdxInfo << "Loaded attribute: " << name << ", Size:" << a->map().size() << endl;
        _ba[name].clear();
      }
      if(saveRequired)
        _saveRequired[name] = true;

      return a->map();
    }

    /**
     * Get a generic pointer to an attribute map. If the attribute map does not exist, 
     * or is the wrong type, or it is unparsed, a null pointer will be returned.
     *
     * \param name Name of the attribute
     */
    AttrMapBase *attrMapBase(const QString &name)
    {
      if(name.isEmpty())
        throw(QString("Attribute::attrMapPtr Attribute Map name empty"));
      auto attr = _attributes.find(name);
      // If it does not exist, return 0
      if(attr == _attributes.end())
        return 0;
      // Check if attribute is loaded from byte array
      if(_ba[name].size() > 0)
        return 0;

      // All OK, return the map
      return attr->second->attrMapBase();
    }

    /**
     * Get a reference to the attribute's save status, it will be created if it doesn't exist
     * \param name Name of the attribute
     */
    bool saveRequired(const QString &name) const
    {
      return _saveRequired[name];
    }
    bool setSaveRequired(const QString &name, bool saveReq)
    {
      return _saveRequired[name] = saveReq;
    }

    /**
     * Erase an attribute
     * \param name Name of the attribute
     */
    void erase(const QString &name)
    {
      AttributeBase *a = _attributes[name];
      if(a)
        delete a;
      _attributes.erase(name);
      _ba.erase(name);
      _saveRequired.erase(name);
    }


    /**
     * Clear the attributes
     * \param name Name of the attribute
     */
    void clear()
    {
      // First erase the allocated items
      for(auto a = _attributes.begin(); a != _attributes.end(); ) {
        if(a->second)
          delete a->second;
        // Remove the maps
        AttrMap<QString, bool>::iterator saveIt = _saveRequired.find(a->first);
        if(saveIt != _saveRequired.end())
          _saveRequired.erase(saveIt);
        AttrMap<QString, QByteArray>::iterator baIt = _ba.find(a->first);
        if(baIt != _ba.end())
          _ba.erase(baIt);

        // and the attribute
        _attributes.erase(a++);
      }
    }

    /**
     * Write the attributes to a file
     * \param file The file to write
     */
    bool write(QIODevice &file)
    {
      progressSetMsg("Writing Attributes");

      // Find the number of attributes to save
      typedef std::pair<QString, bool> QStringBoolPair;
      size_t count = 0;
      forall(const QStringBoolPair &i, _attributes)
        if(_saveRequired[i.first])
          count++;
      writeFile(file, count);

      // Write the starting locations of each attribute, we'll fill them in as we go
      qint64 attrPos = file.pos();
      file.seek(attrPos + (count + 1) * sizeof(size_t));

      if(count > 0)
        mdxInfo << "Writing attributes:";
      
      typedef std::pair<QString, AttributeBase*> QStringAttrBasePr;
      forall(const QStringAttrBasePr &i, _attributes) {
        QString name = i.first;
        if(!_saveRequired[name])
          continue;

        // Put address of attribute in table at the start
        qint64 currentPos = file.pos();
        file.seek(attrPos);
        writeFile(file, size_t(currentPos));
        attrPos = file.pos();
        file.seek(currentPos);
        
        AttributeBase* ab = i.second;

        // Write the name
        writeFile(file, name);

        // If it was converted call the writer, otherwise just write the bytes.
        if(_ba[name].size() == 0) {
          QByteArray ba;
          ab->write(ba);
          file.write((char *)ba.data(),ba.size());
          mdxInfo << " " << name << ":" << ba.size();
        } else {
          file.write((char *)_ba[name].data(),_ba[name].size());
          mdxInfo << " " << name << ":" << _ba[name].size();
        }
      }
      if(count > 0)
        mdxInfo << "." << endl;
      // Put address of attribute in table at the start
      qint64 currentPos = file.pos();
      file.seek(attrPos);
      writeFile(file, size_t(currentPos));
      file.seek(currentPos);

      return true;
    }

    /**
     * Read the attributes from a file.
     * \param file The file to read
     * \param clearAttr Clear the existing attributes first
     */
    bool read(QIODevice &file, bool clearAttr = true)
    {
      progressSetMsg("Reading Attributes");

      // First clear whats there
      if(clearAttr)
        clear();

      // Read the number of attributes
      size_t count;
      readFile(file, count);

      // Read the starting location of each attribute, the last one is the end
      std::vector<size_t> attrPos;
      for(size_t i = 0; i <= count; ++i) {
        size_t pos;
        readFile(file, pos);
        attrPos.push_back(pos);
      }

      if(count > 0)
        mdxInfo << "Read attributes:";

      size_t fileSize = file.size();
      for(size_t i = 0; i < count; ++i) {
        // Go to attribute
        if(attrPos[i] > fileSize or attrPos[i+1] > fileSize + 1) {
          // Something went wrong, skip this attribute
          mdxInfo << endl << QString("Error, attribute location (%1) after end of file (%2), skipping")
             .arg(attrPos[i]).arg(fileSize);
          continue;
        }
        file.seek(attrPos[i]);

        // Read the name
        QString name;
        readFile(file, name);
        if(name.isEmpty()) {
          // Something went wrong, skip this attribute
          mdxInfo << endl << QString("Error, attribute name empty position %1, skipping").arg(attrPos[i]);
          continue;
        }

        // If attributes exist for this name, clear them
        if(_attributes[name]) {
          delete _attributes[name];
          _attributes[name] = 0;
        }

        // Read in the data
        size_t sz = attrPos[i+1] - attrPos[i];
        QByteArray ba(sz, 0);
        file.read(ba.data(), sz);
        _ba[name] = ba;
        _saveRequired[name] = true;
        if(qint64(sz) != _ba[name].size())
          mdxInfo << endl << QString(
                                "Warning, chars read (%1) doesn't match file size (%2) for attribute: %3")
                              .arg(_ba[name].size()).arg(sz).arg(name);
        else
          mdxInfo << " " << name << ":" << ba.size();
      }
      if(count > 0)
        mdxInfo << "." << endl;
      file.seek(attrPos[count]);

      return true;
    }

    /**
     * Size
     */
    virtual size_t size(QString _name)
    {
      if(_name.size() != 0 and _attributes.find(_name) != _attributes.end()){
        if(_attributes[_name] != 0)
          return _attributes[_name]->size();
      }
      return 0;
    }

    /**
     * Return a list of attribute names
     */
    QStringList attrList(const QString &key = QString()) const
    {
      QStringList list;

      if(key.isEmpty()) {
        for(auto &pr : _attributes)
          list << pr.first;
      } else {
        QRegExp regExp(key);
        for(auto &pr : _attributes)
          if(pr.first.indexOf(regExp) >= 0)
            list << pr.first;
      }
      
      return list;
    }

    /**
     * See if an attribute exists
     */
    bool attrExists(const QString &key = QString()) const
    {
      //Iterate over the _attributes map to get list of attributes
      if(key.isEmpty()) {
        if(_attributes.size() > 0)
          return true;
      } else {
        QRegExp regExp(key);
        for(AttrMap<QString, AttributeBase*>::const_iterator i = _attributes.begin(); i != _attributes.end(); ++i)
          if(i->first.indexOf(regExp) >= 0)
            return true;
      }
      return false;
    }

    /**
     * Return a list of unparsed attribute names
     */
    QStringList attrListUnparsed() const
    {
      QStringList list;
      for(auto &pr : _ba)
        if(pr.second.size() > 0) 
          list << pr.first;

      return list;
    }

  private:
    // Attribute data and meta-data
    AttrMap<QString, AttributeBase*> _attributes;
    AttrMap<QString, QByteArray> _ba;
    AttrMap<QString, bool> _saveRequired;
  };

  // Syntax sugar for attributes.
  // FIXME These used to do caching, but should now be removed. They just make the code look confusing to newcomers.
  template<typename KeyT, typename ValueT>
  ValueT __attribute__((deprecated))  &operator->*(const KeyT &key, AttrMap<KeyT, ValueT> &map)
  {
    return map[key];
  }
  template<typename KeyT, typename ValueT>
  ValueT __attribute__((deprecated)) &operator->*(const KeyT &key, AttrMap<KeyT, ValueT> *map) 
  {
    return (*map)[key];
  }
}
#endif 
