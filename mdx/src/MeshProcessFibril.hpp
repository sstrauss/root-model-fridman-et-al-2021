//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef FIBRIL_ORIENTATIONS_HPP
#define FIBRIL_ORIENTATIONS_HPP

#include <Process.hpp>
#include <MeshUtils.hpp>

namespace mdx
{
  ///\addtogroup Process
  ///@{
  /**
   * \class FibrilOrientations <ProcessFibril.hpp>
   *
   * Compute principle orientations of lines in the mesh signal. Based on
   * Boudaoud et al., 'FibrilTool, an ImageJ plug-in to quantify fibrillar
   * structures in raw microscopy images', Nature Protocols 2014
   */
  class FibrilOrientations : public Process 
  {
  public:
    FibrilOrientations(const Process& process) : Process(process) 
    {
      setName("Mesh/Cell Axis/Fibril Orientations");
      setDesc("Compute principle orientations of lines in the mesh signal.\n"
              "Based on Boudaoud et al. 2014, FibrilTool, an ImageJ plug-in to\n"
              "quantify fibrillar structures in raw microscopy images, doi 10.1038/nprot.2014.024");
      setIcon(QIcon(":/images/PrincipalOrientations.png"));

      addParm("Border Size", "Width of cell border that is not taken into account for the computation.", "1.0");
      addParm("Min Ratio", "Minimum ratio of inner area (whole cell - border) vs. total area needed for computation.", "0.25");
      addParm("Blur Radius", "Radius to blur signal before calculating gradient", "1.0");
      addParm("Scale", "Max size in microns for direction lines", "10.0");
      addParm("Axis Name", "Name of cell axis", "Fibril Orientation");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::initialize No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::initialize No cell complex").arg(name());

      if(!mesh->signalExists())
        throw QString("%1:initialize No signal available").arg(name());
      if(mesh->signalType(mesh->signal()) != "Double" )
        throw QString("%1:initialize Invalid signal type (%2), must be double").arg(name()).arg(mesh->signalType(mesh->signal()));

      QString axisName = parm("Axis Name");
      if(axisName.isEmpty())
        throw QString("%1::initialize No cell axis name").arg(name());

      double scale = parm("Scale").toDouble();
      if(scale <= 0 )
        throw QString("%1::initialize Scale must be > 0").arg(name());

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(mesh->signal());
      auto &fibrilAttr = mesh->cellAxisAttr(ccName, axisName);
      
      bool result = run(cs, indexAttr, signalAttr, 
          parm("Border Size").toDouble(), parm("Min Ratio").toDouble(), parm("Blur Radius").toDouble(), fibrilAttr);
      if(result) {
        IntIntAttr *labelMap = 0;
        if(mesh->labeling() != "Labels")
          labelMap = &mesh->labelMap(mesh->labeling());
        calcCellAxisCentersNormals(cs, indexAttr, labelMap, mesh->cellAxisCentersNormals(ccName));
        double bounds = axisBounds(fibrilAttr);
        mesh->cellAxisSetBounds(ccName, axisName, 0, Point2d(scale, bounds));
        mesh->cellAxisSetBounds(ccName, axisName, 1, Point2d(scale, bounds));
        mesh->cellAxisSetBounds(ccName, axisName, 2, Point2d(0, 0));
        auto &cdp = mesh->drawParms(ccName);
        cdp.setGroupVisible("Cell Axis", true);
        cdp.setRenderChoice("Cell Axis", axisName);
      }
      mesh->updateProperties();

      return result;
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr,
                                        double border, double minAreaRatio, double blurRadius, IntSymTensorAttr &fibrilAttr);
  };
  
//  /**
//   * \class DisplayFibrilOrientations <ProcessFibril.hpp>
//   *
//   * Change the representation of the fibril orientation after it has been
//   * computed.
//   */
//  class mdxBase_EXPORT DisplayFibrilOrientations : public Process 
//  {
//  public:
//    DisplayFibrilOrientations(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//  
//      return run(mesh, parms[0], QColor(parms[1]), parms[2].toFloat(), 
//                            parms[3].toFloat(), parms[4].toFloat(), parms[5].toFloat());
//    }
//  
//    bool run(Mesh* mesh, const QString DisplayHeatMap, const QColor& ColorMax, 
//        float AxisLineWidth, float ScaleAxisLength, float AxisOffset, float OrientationThreshold);
//  
//    QString name() const { return 
//      "Mesh/Cell Axis/Fibril Orientations/Display Fibril Orientations"; }
//    QString description() const { return 
//      "Display the orientations of fibrils on the image.\n"
//      "Only the maximal direction (main orientation) is displayed as a vector."; }
//    QStringList parmNames() const { return QStringList() 
//      << "Heatmap" << "Line Color" << "Line Width" << "Line Scale" << "Line Offset" 
//      << "Threshold"; }
//    QStringList parmDescs() const { return QStringList() 
//      << "Display orientation strength (= MaxDirection/MinDirection - 1) as a colormap."
//      << "Line Color" << "Line Width"
//      << "Length of the vectors = Scale * orientation strength."
//      << "Draw the vector ends a bit tilted up for proper display on surfaces."
//      << "Minimal value of orientation strength required for drawing main direction."; }
//    QStringList parmDefaults() const { return QStringList() 
//      << "none" << "red" << "5.0" << "10.0" << "0.1" << "0.0"; }
//    ParmChoiceMap parmChoice() const
//    {
//      ParmChoiceMap map;
//      map[0] = QStringList() << "none" << "Orientation";
//      map[1] = QColor::colorNames();
//      return map;
//    }
//    QIcon icon() const { return QIcon(":/images/PrincipalOrientations.png"); }
//  };
//
// /**
//   * \class FibrilOrientations <ProcessFibril.hpp>
//   *
//   * Compute principle orientations of lines in the mesh signal. Based on
//   * Boudaoud et al., 'FibrilTool, an ImageJ plug-in to quantify fibrillar
//   * structures in raw microscopy images', Nature Protocols 2014
//   */
//  class FibrilOrientationsVertex : public Process {
//  public:
//    FibrilOrientationsVertex(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//       Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//
//      Mesh* m = currentMesh();
//      Mesh *m2;
//      if(currentMesh() == mesh(0))
//        m2 = mesh(1);
//      else
//        m2 = mesh(0);
//      return run(m, m2, parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat());
//    }
//  
//    bool run(Mesh* m, Mesh* m2, float border, float minAreaRatio, float radius);
//  
//    QString name() const { return 
//      "Mesh/Vertex Axis/Fibril Orientation/Compute Fibril Orientations"; }
//    QString description() const { return 
//      "Compute principle orientations of lines in the mesh signal.\n"
//      "Based on Boudaoud et al., 'FibrilTool, an ImageJ plug-in to\n"
//      "quantify fibrillar structures in raw microscopy images', Nature Protocols 2014"; }
//    QStringList parmNames() const { return QStringList() 
//      << "Border Size" << "Minimum inner area ratio" << "Radius"; }
//    QStringList parmDescs() const { return QStringList()
//      << "Width of cell border that is not taken into account for the computation."
//      << "Minimum ratio of inner area (whole cell - border) vs. total area "
//         "needed for compuation."
//      << "Radius"; }
//    QStringList parmDefaults() const { return QStringList() << "1.0" << "0.25" << "1.0"; }
//    QIcon icon() const { return QIcon(":/images/PrincipalOrientations.png"); }
//  };
//
//
//  /**
//   * \class DisplayFibrilOrientations <ProcessFibril.hpp>
//   *
//   * Change the representation of the fibril orientation after it has been
//   * computed.
//   */
//  class mdxBase_EXPORT DisplayFibrilOrientationsVertex : public Process 
//  {
//  public:
//    DisplayFibrilOrientationsVertex(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      Mesh *mesh = currentMesh();
//      if(!mesh) throw(QString("No current mesh"));
//  
//      return run(mesh, parms[0], QColor(parms[1]), parms[2].toFloat(), 
//                            parms[3].toFloat(), parms[4].toFloat(), parms[5].toFloat());
//    }
//  
//    bool run(Mesh* mesh, const QString DisplayHeatMap, const QColor& ColorMax, 
//        float AxisLineWidth, float ScaleAxisLength, float AxisOffset, float OrientationThreshold);
//  
//    QString name() const { return 
//      "Mesh/Vertex Axis/Fibril Orientation/Display Fibril Orientations"; }
//    QString description() const { return 
//      "Display the orientations of fibrils on the image.\n"
//      "Only the maximal direction (main orientation) is displayed as a vector."; }
//    QStringList parmNames() const { return QStringList() 
//      << "Heatmap" << "Line Color" << "Line Width" << "Line Scale" << "Line Offset" 
//      << "Threshold"; }
//    QStringList parmDescs() const { return QStringList() 
//      << "Display orientation strength (= MaxDirection/MinDirection - 1) as a colormap."
//      << "Line Color" << "Line Width"
//      << "Length of the vectors = Scale * orientation strength."
//      << "Draw the vector ends a bit tilted up for proper display on surfaces."
//      << "Minimal value of orientation strength required for drawing main direction."; }
//    QStringList parmDefaults() const { return QStringList() 
//      << "none" << "red" << "5.0" << "10.0" << "0.1" << "0.0"; }
//    ParmChoiceMap parmChoice() const
//    {
//      ParmChoiceMap map;
//      map[0] = QStringList() << "none" << "Orientation";
//      map[1] = QColor::colorNames();
//      return map;
//    }
//    QIcon icon() const { return QIcon(":/images/PrincipalOrientations.png"); }
//  };
  ///@}
}

#endif
