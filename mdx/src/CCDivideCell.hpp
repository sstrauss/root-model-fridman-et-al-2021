//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2021 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_DIVIDE_CELL_HPP
#define CC_DIVIDE_CELL_HPP

#include <CCF.hpp>
#include <Subdivide.hpp>
#include <ProcessParms.hpp>
#include <Attributes.hpp>
#include <Random.hpp>

namespace mdx
{
  // Parameters for two-dimensional cell division
  class Cell2dDivideParms : virtual public ProcessParms
  {
  public:
    enum DivAlg { SHORTEST_WALL_THROUGH_CENTROID = 0 , ASSIGNED_VECTOR_TRHOUGH_CENTROID = 1};

    Cell2dDivideParms()
    {
      setName("Cell 2D Divide Parms");
      addParm("Cell Max Area", "Maximum cell area before division", "50.0");
      addParm("Cell Wall Min", "Minimum distance during division to existing vertex", "0.1");
      addParm("Cell Wall Sample", "Cell wall sampling distance", "0.05");
      addParm("Cell Pinch", "Amount to pinch walls", "0.2");
      addParm("Cell Max Pinch", "Maximum absolute amount to pinch walls", "1.0");
      addParm("Center Noise", "Add noise to cell center", "0.0");
      addParm("Wall Noise", "Add noise to wall postion after having found the shortest wall", "0.0");
      addParm("MinAngle", "MinAngle", "60.0");
      addParm("Verbose", "Print detailed information", "No", booleanChoice());
    }
    ~Cell2dDivideParms() {}
  };

  bool divideCell2d(CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndex cell,
                    const Cell2dDivideParms &divParms, Subdivide *sDiv,
                    Cell2dDivideParms::DivAlg divAlg = Cell2dDivideParms::SHORTEST_WALL_THROUGH_CENTROID,
                    const Point3d &divVector = Point3d(1., 0., 0.));


  // Definition of a plane for splitting a 3D cell
  struct SplittingPlane
  {
    Point3d normal;
    Point3d pointOnPlane;

    SplittingPlane(const Point3d &_normal = Point3d(0,0,1),
		   const Point3d &_pointOnPlane = Point3d(0,0,0))
      : normal(_normal), pointOnPlane(_pointOnPlane)
    {
      normal.normalize();
    }

    void rotate(Point3d axis, double degrees);

    inline bool inside(const Point3d& point) const
    {
      return (pointZ(point) < 0);
    }

    inline double pointZ(const Point3d &point) const
    {
      return (point - pointOnPlane) * normal;
    }

    inline double intersectEdge(const Point3d &p1, const Point3d &p2) const
    {
      return ((pointOnPlane - p1) * normal) / ((p2 - p1) * normal);
    }
  };

  // Split a volume with the given splitting plane
  bool splitVolume(CCStructure &cc, CCIndexDataAttr &indexAttr, CCIndex vol,
		   const SplittingPlane &plane,
		   CCStructure::SplitStruct &ss,
		   bool triangulated = false, Subdivide *subdiv = NULL);

} // end namespace mdx

#endif // CC_DIVIDE_CELL_HPP
