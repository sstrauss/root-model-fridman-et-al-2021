//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <Assert.hpp>
#include <QString>
#include <QCoreApplication>
#include <iostream>
#include <stdlib.h>
#include "Information.hpp"

namespace mdx {
  void __assert_fail(const QString& assertion, const char* file, unsigned int line, const char* function)
  {
    QString app_name = QCoreApplication::applicationName();
    QString txt = QString("%1: %2:%3: %4: Assertion `%5' failed.")
      .arg(app_name)
      .arg(file)
      .arg(QString::number(line))
      .arg(function)
      .arg(assertion);
    mdxInfo << txt << endl;
    abort();
  }
}
