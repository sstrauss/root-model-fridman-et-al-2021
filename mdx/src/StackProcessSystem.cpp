//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <StackProcessSystem.hpp>

#include <Dir.hpp>
#include <Image.hpp>
#include <Misc.hpp>
#include <MDXViewer/qglviewer.h>
#include <PlyFile.hpp>
#include <CCUtils.hpp>
#include <StackProcessFilters.hpp>

#include <QFileInfo>
#include <QMessageBox>
#include <CImg.h>

#include <ui_LoadStack.h>
#include <ui_ExportStack.h>

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
#  define FileDialogOptions 0
#endif

using namespace qglviewer;
using namespace cimg_library;

#include <QTextStream>
#include <stdio.h>

static QTextStream err(stderr);

namespace mdx 
{
  typedef CImg<ushort> CImgUS;

  struct StackHdr {
    uint hdrsz;
    uint xsz, ysz, zsz;
    float xum, yum, zum;
    uint datasz;
  
    StackHdr() : hdrsz(1024) {}
  };
  
  bool StackImportSeries::initialize(QWidget* parent)
  {
    int stackId = parm("Stack").toInt();
    QString storeName = parm("Store");
    Point3d step(parm("X Step").toDouble(), parm("Y Step").toDouble(), parm("Z Step").toDouble());
    double brightness(parm("Brightness").toDouble());
    QString fileName(parm("Profile File"));
  
    // Get stack and store
    Stack* stk = currentStack();
    if(stackId != -1)
      stk = getStack(stackId);
    if(!stk)
      throw(QString("Invalid stack id %1").arg(stackId));
  
    Store* store = stk->currentStore();
    if(storeName == "Main")
      store = stk->main();
    else if(storeName == "Work")
      store = stk->work();
  
    QString currentFile(store->file());
  
    if(currentFile.endsWith(".txt", Qt::CaseInsensitive)) {
      // If current stack file is a .txt file, load it as a profile
      loadProfile(currentFile);
      loadedFile = currentFile;
    } else if(fileName.endsWith(".txt", Qt::CaseInsensitive)) {
      // If file list contains one
      loadProfile(fileName);
      loadedFile = fileName;
    }

    if(!parent) return true; // if called from command line

    // Set up dialog
    QDialog dlg(parent);
    this->dlg = &dlg;
    Ui_LoadStackDialog ui;
    this->ui = &ui;
    ui.setupUi(&dlg);
  
    QObject::connect(ui.AddFiles, SIGNAL(clicked()), this, SLOT(addFiles()));
    QObject::connect(ui.RemoveFiles, SIGNAL(clicked()), this, SLOT(removeFiles()));
    QObject::connect(ui.LoadProfile, SIGNAL(clicked()), this, SLOT(loadProfile()));
    QObject::connect(ui.SaveProfile, SIGNAL(clicked()), this, SLOT(saveProfile()));
    QObject::connect(ui.FilterFiles, SIGNAL(clicked()), this, SLOT(filterFiles()));
    QObject::connect(ui.SortAscending, SIGNAL(clicked()), this, SLOT(sortAscending()));
    QObject::connect(ui.SortDescending, SIGNAL(clicked()), this, SLOT(sortDescending()));
    QObject::connect(ui.ImageFiles, SIGNAL(filesDropped(const QStringList &)), this,
                     SLOT(addFiles(const QStringList &)));
  
    setImageSize(512, 512, 0);
    ui.StepX->setValue(step.x());
    ui.StepY->setValue(step.y());
    ui.StepZ->setValue(step.z());
    ui.Brightness->setValue(brightness);
    ui.ImageFiles->clear();
  
    bool res = true;
    if(dlg.exec() == QDialog::Accepted) {
      // Change parms
      setParm("X Step", QString::number(ui.StepX->value()));
      setParm("Y Step", QString::number(ui.StepY->value()));
      setParm("Z Step", QString::number(ui.StepZ->value()));
      setParm("Brightness", QString::number(ui.Brightness->value()));
      setParm("Profile File", loadedFile);
  
      // Save image files
      for(int i = 0; i < ui.ImageFiles->count(); ++i)
        imageFiles << ui.ImageFiles->item(i)->text();
    } else
      res = false;
  
    this->dlg = 0;
    this->ui = 0;
    return res;
  }
  
  bool StackImportSeries::loadProfile(QString fileName, Point3u& size, Point3d& step, 
                                                  double& brightness, QStringList& files)
  {
    if(QFile::exists(fileName)) {
      Parms parms(fileName);
  
      bool ok = true;
      ok &= parms("Stack", "SizeX", size.x());
      ok &= parms("Stack", "SizeY", size.y());
      // New version: StepX and StepY and separate
      if(parms("Stack", "StepX", step.x())) {
        parms("Stack", "StepY", step.y());
        parms("Stack", "StepZ", step.z());
      } else {
        // This is for backward compatibility
        if(!parms("Stack", "StepXY", step.x()) or !parms("Stack", "StepZ", step.z())) {
          double zScale;
          if(parms("Stack", "ZScale", zScale)) {
            step.z() = .5;
            step.x() = step.z() / zScale;
          } else
            ok = false;
        }
        step.y() = step.x();
      }
      ok &= parms("Stack", "Brightness", brightness);
      if(!ok) {
        mdxWarning << "loadStack::Error:Cannot read parameter file:" << fileName << endl;
        return false;
      }
      parms.all("Stack", "ImageFile", files);
      size.z() = files.size();
      txtFile = fileName;
      return true;
    }
    return false;
  }
  
  void StackImportSeries::loadProfile(QString fileName)
  {
    if(fileName.isEmpty()) {
      fileName = QFileDialog::getOpenFileName(dlg, QString("Select profile file to open"), fileName,
                                              QString("Text files (*.txt)"), 0, FileDialogOptions);
      if(fileName.isEmpty())
        return;
    }
  
    QStringList ImageFiles;
    Point3u size;
    Point3d step;
    double brightness;
    if(loadProfile(fileName, size, step, brightness, imageFiles)) {
      setImageSize(size.x(), size.y(), size.z());
      ui->StepX->setValue(step.x());
      ui->StepY->setValue(step.y());
      ui->StepZ->setValue(step.z());
      ui->Brightness->setValue(brightness);
      QListWidget* lst = ui->ImageFiles;
      lst->clear();
      forall(const QString& s, imageFiles)
        lst->addItem(s);
  
      loadedFile = fileName;
    }
  }
  
  void StackImportSeries::saveProfile()
  {
    // Write to file
    QString fileName = QFileDialog::getSaveFileName(dlg, QString("Select file to save profile for "), loadedFile,
                                                    QString("Text files (*.txt)"), 0, FileDialogOptions);
    if(fileName.isEmpty())
      return;
  
    // Check ending, add suffix if required
    if(fileName.right(4) != ".txt")
      fileName.append(".txt");
  
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
      mdxWarning << "loadStackSaveProfile::Error:Cannot open output file:" << fileName << endl;
      return;
    }
    QString StackFile = stripCurrentDir(fileName);
    actingFile(fileName);
  
    QTextStream out(&file);
    out << "[Stack]" << endl;
    out << "SizeX: " << imageSize.x() << endl;
    out << "SizeY: " << imageSize.y() << endl;
    out << "StepX: " << ui->StepX->value() << endl;
    out << "StepY: " << ui->StepY->value() << endl;
    out << "StepZ: " << ui->StepZ->value() << endl;
    out << "Brightness: " << ui->Brightness->value() << endl;
    QListWidget* lst = ui->ImageFiles;
    for(int i = 0; i < lst->count(); i++) {
      QListWidgetItem* item = lst->item(i);
      out << "ImageFile: " << stripCurrentDir(item->text()) << endl;
    }
    file.close();
    loadedFile = fileName;
    setStatus(QString("Saving profile, file: %1").arg(fileName));
    txtFile = fileName;
  }
  
  void StackImportSeries::addFiles()
  {
    // RSS How to get a complete list from CImg? (there are over 100)
    QStringList files = QFileDialog::getOpenFileNames(dlg, QString("Select stack image files "), currentPath(),
                                                      QString("Any files (*.*)"), 0);
    if(files.empty())
      return;
  
    addFiles(files);
  }
  
  void StackImportSeries::addFiles(const QStringList& files)
  {
    QListWidget* lst = ui->ImageFiles;
    if(lst->count() == 0) {
      QString first = files[0];
      Image3D image;
      image.step = Point3f(imageResolution());
      if(!loadImage(first, image)) {
        QMessageBox::critical(dlg, "Error reading image", QString("Error, cannot read image from '%1'").arg(first));
        return;
      }
      setStatus(QString("Loaded first image: size = %1x%2").arg(image.size.x()).arg(image.size.y()));
      // CImgUS image(qPrintable(first));
      setImageSize(image.size.x(), image.size.y(), 0);
      setImageResolution(Point3d(image.step));
    }
    lst->addItems(files);
    setImageSize(imageSize.x(), imageSize.y(), lst->count());
  }
  
  Point3d StackImportSeries::imageResolution() {
    return Point3d(ui->StepX->value(), ui->StepY->value(), ui->StepZ->value());
  }
  
  void StackImportSeries::setImageResolution(Point3d step)
  {
    ui->StepX->setValue(step.x());
    ui->StepY->setValue(step.y());
    ui->StepZ->setValue(step.z());
  }
  
  void StackImportSeries::setImageSize(Point3u size)
  {
    imageSize = size;
    ui->SizeXY->setText(QString("(%1,%2,%3)").arg(imageSize.x()).arg(imageSize.y()).arg(imageSize.z()));
  }
  
  void StackImportSeries::removeFiles()
  {
    QListWidget* lst = ui->ImageFiles;
    QList<QListWidgetItem*> items = lst->selectedItems();
    for(QList<QListWidgetItem*>::iterator i = items.begin(); i != items.end(); i++) {
      QListWidgetItem* item = lst->takeItem(lst->row(*i));
      delete item;
    }
    setImageSize(imageSize.x(), imageSize.y(), lst->count());
  }
  
  void StackImportSeries::filterFiles()
  {
    QListWidget* lst = ui->ImageFiles;
    QString text = ui->FilterText->text();
  
    QList<QListWidgetItem*> items = lst->findItems(".*", Qt::MatchRegExp);
    for(QList<QListWidgetItem*>::iterator i = items.begin(); i != items.end(); i++) {
      QString itemtext = (*i)->text();
      if(itemtext.indexOf(text) < 0) {
        QListWidgetItem* item = lst->takeItem(lst->row(*i));
        delete item;
      }
    }
  }
  
  void StackImportSeries::sortAscending(bool val)
  {
    QListWidget* lst = ui->ImageFiles;
    lst->sortItems((val ? Qt::AscendingOrder : Qt::DescendingOrder));
  }
  
  void StackImportSeries::sortDescending() 
  {
    sortAscending(false);
  }

  bool StackImportSeries::run()
  {
    int stackId = parm("Stack").toInt();
    QString storeName = parm("Store");
    Point3d step(parm("X Step").toDouble(), parm("Y Step").toDouble(), parm("Z Step").toDouble());
    double brightness(parm("Brightness").toDouble());
    QString fileName(parm("Profile File"));

    // Get stack and store
    Stack* stk = currentStack();
    if(stackId != -1)
      stk = getStack(stackId);
    if(!stk)
      throw(QString("Invalid stack id %1").arg(stackId));

    Store* store = stk->currentStore();
    if(storeName == "Main")
      store = stk->main();
    else if(storeName == "Work")
      store = stk->work();

    return run(stk, store, step, brightness, fileName);
  } 

  bool StackImportSeries::run(Stack* stack, Store* store, Point3d step, double brightness, QString fileName)
  {
    Point3u size;
    if(fileName.size() > 0) {
      loadProfile(fileName, size, step, brightness, imageFiles);
      actingFile(fileName);
    }
  
    HVecUS& data = store->data();
    {
      CImgUS first_image(qPrintable(imageFiles.at(0)));
      size = Point3u(first_image.width(), first_image.height(), imageFiles.size());
    }
    stack->setSize(size);
    stack->setStep(step);
    size_t SizeXYZ = size_t(size.x()) * size.y() * size.z();
    store->setFile(fileName);
    uint filecount = 0;
    setStatus(QString("Loading %1 stack %2").arg(store == stack->main() ? "main" : "work").arg(stack->userId()));
    progressStart(
      QString("Loading %1 Stack %2").arg((store == stack->main()) ? "main" : "work").arg(stack->userId()), size.z());
    memset(&data[0], 0, SizeXYZ * sizeof(ushort));
  
    Image3D image(data, size, Point3f(step));
    image.brightness = brightness;
    image.minc = 65535;
    image.maxc = 0;
    for(uint z = 0; z < size.z(); z++) {
      image.plane = filecount++;
      if(!loadImage(imageFiles.at(z), image)) {
        setErrorMessage(QString("Cannot read image from file '%1'").arg(imageFiles.at(z)));
        return false;
      }
      setStatus(QString("Read File: %1").arg(imageFiles.at(z)));
      if(!progressAdvance(z + 1))
        userCancel();
    }
    if(image.maxc > 0xFFFF)
      mdxWarning << "WARNING: max intensity:" << image.maxc << ", clipped at:" << 0xFFFF << endl;
    else
      mdxInfo << filecount << " of " << size.z() << " files,read intensity range:" << image.minc << "-" << image.maxc << endl;
    stack->center();
    store->changed();
    return true;
  }
  REGISTER_PROCESS(StackImportSeries);
  
  bool StackOpen::initialize(QWidget* parent)
  {
    bool is_work_store = stringToWorkStore(parm("Store"));
    int stackId = parm("Stack Id").toInt();
    QString allFilters("All Stack files (*.mdxs *.mgxs *.inr *.tif *.tiff)");
    QString mdxsFilter("MorphoDynamX Stack files (*.mdxs)"), mgxsFilter("MorphoGraphX Stack files (*.mgxs)");
		QString inrFilter("Inria Stack files (*.inr)"),
    tifFilter("TIFF Image Directory (*.tif *.tiff)"), filter;
    QString fileName = parm("File Name");
  
    if(fileName.isEmpty()) {
      QString currentFile;
      if(is_work_store)
        currentFile = getStack(stackId)->work()->file();
      else
        currentFile = getStack(stackId)->main()->file();
      if(!currentFile.isEmpty())
        fileName = currentFile;
    }
  
    // If we loaded a list of images before, strip of the ending and default type
    if(fileName.right(4) == ".txt") {
      fileName = fileName.left(fileName.length() - 4);
      fileName += ".mdxs";
    }
    if(fileName.endsWith(".inr", Qt::CaseInsensitive))
      filter = inrFilter;
    else if(fileName.endsWith(".tif", Qt::CaseInsensitive) or fileName.endsWith(".tiff", Qt::CaseInsensitive))
      filter = tifFilter;
    else if(fileName.endsWith(".mdxs", Qt::CaseInsensitive))
      filter = mdxsFilter;
    else if(fileName.endsWith(".mgxs", Qt::CaseInsensitive))
      filter = mgxsFilter;
    else
      filter = allFilters;
  
    // Get the file name
    if(parent)
      fileName = QFileDialog::getOpenFileName(
        parent, QString("Select stack file"), fileName,
        QString(allFilters + ";;" + mdxsFilter + ";;" + mgxsFilter + ";;" + inrFilter + ";;" + tifFilter), &filter, FileDialogOptions);
    if(fileName.isEmpty())
      return false;
  
    setParm("File Name", fileName);
    return true;
  }
  
  bool StackOpen::run()
  {
    int stackId = parm("Stack Id").toInt();
    Stack* stack = getStack(stackId);
    Store* store = (stringToWorkStore(parm("Store")) ? stack->work() : stack->main());
    bool res = run(stack, store, parm("File Name"));
    if(res) {
      store->show();
    }
    return res;
  }
  
  bool StackOpen::loadMDXS_0(QIODevice& file, Stack* stack, Store* store)
  {
    HVecUS& data = store->data();
    QByteArray stkdata = file.readAll();
    file.close();
    stkdata = qUncompress(stkdata);
  
    struct StackHdr stkhdr;
    memcpy(&stkhdr, stkdata.constData(), sizeof(stkhdr));
    Point3u size(stkhdr.xsz, stkhdr.ysz, stkhdr.zsz);
    Point3d step(stkhdr.xum, stkhdr.yum, stkhdr.zum);
    stack->setSize(size);
    stack->setStep(step);
    if(stkhdr.datasz / 2 != stack->storeSize())
      throw QString("Datasize does not match dimensions in stack file");
    memcpy(data.data(), stkdata.constData() + stkhdr.hdrsz, stkhdr.datasz);
    return true;
  }
  
  bool StackOpen::loadMDXS_1_3(QIODevice& file, Stack* stack, Store* store)
  {
    bool isLabel;
    float sxum, syum, szum;
    file.read((char*)&isLabel, sizeof(bool));
    file.read((char*)&sxum, sizeof(float));
    file.read((char*)&syum, sizeof(float));
    file.read((char*)&szum, sizeof(float));
    bool result = loadMDXS_1_0(file, stack, store);
    if(result) {
      store->setLabels(isLabel);
      stack->setOrigin(Point3d(sxum, syum, szum));
      mdxInfo << "Set stack step to %1" << stack->origin() << endl;
    }
    return result;
  }
  
  bool StackOpen::loadMDXS_1_2(QIODevice& file, Stack* stack, Store* store)
  {
    bool result = loadMDXS_1_3(file, stack, store);
    if(result and store->labels()) {
      StackSwapBytes swap(*this);
      return swap.run(store, store);
    }
    return result;
  }
  
  bool StackOpen::loadMDXS_1_1(QIODevice& file, Stack* stack, Store* store)
  {
    bool isLabel;
    file.read((char*)&isLabel, sizeof(bool));
    bool result = loadMDXS_1_0(file, stack, store);
    if(result) {
      store->setLabels(isLabel);
      if(store->labels()) {
        StackSwapBytes swap(*this);
        return swap.run(store, store);
      }
    }
    return result;
  }
  
  bool StackOpen::loadMDXS_1_0(QIODevice& file, Stack* stack, Store* store)
  {
    HVecUS& data = store->data();
    quint32 xsz;
    quint32 ysz;
    quint32 zsz;
    float xum;
    float yum;
    float zum;
    quint64 datasz;
    quint8 cl;
    file.read((char*)&xsz, sizeof(quint32));
    file.read((char*)&ysz, sizeof(quint32));
    file.read((char*)&zsz, sizeof(quint32));
    mdxInfo << "Size: " << xsz << " " << ysz << " " << zsz << endl;
    file.read((char*)&xum, sizeof(float));
    file.read((char*)&yum, sizeof(float));
    file.read((char*)&zum, sizeof(float));
    file.read((char*)&datasz, sizeof(quint64));
    file.read((char*)&cl, sizeof(quint8));
    quint64 expected_size = 2ul * (quint64(xsz) * quint64(ysz) * quint64(zsz));
    if(datasz != expected_size)
      throw QString("Datasize does not match dimensions in MDXS file (size = %1, expected from dimension = %2)")
            .arg(datasz)
            .arg(expected_size);
    Point3u size(xsz, ysz, zsz);
    Point3d step(xum, yum, zum);
    stack->setSize(size);
    stack->setStep(step);
    if(cl > 0) {
      progressStart(QString("Loading Stack %1").arg(stack->userId()), expected_size);
      quint64 cur_size = 0;
      char* d = (char*)data.data();
      while(cur_size < 2 * data.size()) {
        if(!progressAdvance(cur_size))
          userCancel();
        // mdxInfo << "advance to " << cur_size << endl;
        quint32 sz;
        qint64 qty = file.read((char*)&sz, sizeof(quint32));
        if(qty != sizeof(quint32)) {
          throw QString("Could not read enough data from MDXS file (read = %1, expected = %2)").arg(qty).arg(
                  sizeof(quint32));
        }
        QByteArray stkdata = file.read(sz);
        if(quint32(stkdata.size()) != sz) {
          throw QString("Could not read enough data from MDXS file (read = %1, expected = %2)")
                .arg(stkdata.size()).arg(sz);
        }
        stkdata = qUncompress(stkdata);
        if(stkdata.isEmpty()) {
          throw QString("Compressed data in MDXS file is corrupted");
        }
        memcpy(d, stkdata.data(), stkdata.size());
        d += stkdata.size();
        cur_size += stkdata.size();
      }
      if(!progressAdvance(cur_size))
        userCancel();
    } else {
      // Ok, if greater than 64MB, read in bits
      quint64 nb_slices = expected_size >> 26;
      quint64 slice_size = 1ul << 26;
      progressStart(QString("Loading Stack %1").arg(stack->userId()), nb_slices);
      mdxInfo << "Nb of slices = " << nb_slices << endl;
      for(quint64 i = 0; i < nb_slices; ++i) {
        qint64 qty = file.read((char*)data.data() + i * slice_size, slice_size);
        if(qty != (int)slice_size)
          throw QString("Could not read enough data from MDXS file (read = %1, expected = %2)")
                  .arg(qty).arg(slice_size);
        if(!progressAdvance(i))
          userCancel();
        // mdxInfo << "advance to " << i << endl;
      }
      quint64 left_size = expected_size - (nb_slices << 26);
      qint64 qty = file.read((char*)data.data() + (nb_slices << 26), left_size);
      if(qty != (int)left_size) {
        throw QString("Could not read enough data from MDXS file (read = %1, expected = %2)")
                .arg(qty).arg(left_size);
      }
      if(!progressAdvance(nb_slices))
        userCancel();
    }
    return true;
  }
  
  bool StackOpen::run(Stack* stack, Store* store, QString fileName)
  {
    if(fileName.isEmpty())
      throw QString("StackOpen::Error:Error, trying to load a stack from an empty fileName.");
  
    actingFile(fileName);
    store->setFile(fileName);
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
      throw QString("StackOpen::Error:Cannot open input file - '%1'").arg(fileName);
  
    progressStart(QString("Loading %1 Stack %2")
                .arg((store == stack->main()) ? "main" : "work").arg(stack->userId()), 0, false);
    // progressStart(QString("Reading stack file %1").arg(fileName), 0, false);
    if(fileName.endsWith(".mdxs", Qt::CaseInsensitive) or fileName.endsWith(".mgxs", Qt::CaseInsensitive)) {
      char magic[5];
      file.read(magic, 5);
      if(memcmp(magic, "MDXS", 4) == 0 or memcmp(magic, "MGXS", 4) == 0) {
        QList<int> version = extractVersion(file);
        if(version.size() != 2) {
          QStringList str;
          foreach(int i, version)
          str << QString::number(i);
          throw QString("StackOpen::Error:Invalid file version format (%1), expected: MAJOR.MINOR")
                .arg(str.join("."));
        }
        bool success = false;
        switch(version[0]) {
        case 1:
          switch(version[1]) {
          case 0:
            success = loadMDXS_1_0(file, stack, store);
            stack->center();
            break;
          case 1:
            success = loadMDXS_1_1(file, stack, store);
            stack->center();
            break;
          case 2:
            success = loadMDXS_1_2(file, stack, store);
            break;
          case 3:
            success = loadMDXS_1_3(file, stack, store);
            break;
          default:
            throw QString("StackOpen::Error:Unknown file version %1.%2, this version of "
              "MorphoDynamX only loads up to version 1.4") .arg(version[0]) .arg(version[1]);
          }
          break;
        default:
          throw QString("StackOpen::Error:Unknown file version %1.%2, this version of "
            "MorphoDynamX only loads up to version 1.4") .arg(version[0]) .arg(version[1]);
        }
        if(!success)
          return false;
      } else {
        file.seek(0);
        if(!loadMDXS_0(file, stack, store))
          return false;
        stack->center();
      }
    } else if(fileName.endsWith(".inr", Qt::CaseInsensitive)) {
      HVecUS& data = store->data();
      QByteArray stkdata = file.readAll();
      file.close();
      CImgUS image;
      Point3f voxelsz;
      image.load_inr(fileName.toLocal8Bit(), voxelsz.data());
      Point3u size(image.width(), image.height(), image.depth());
      Point3d step(voxelsz.x(), voxelsz.y(), voxelsz.z());
      stack->setSize(size);
      stack->setStep(step);
      memcpy(data.data(), image.data(), stack->storeSize() * 2);
      stack->center();
    } else if(fileName.endsWith(".tif", Qt::CaseInsensitive) or fileName.endsWith(".tiff", Qt::CaseInsensitive)) {
      Image3D data(store->data(), stack->size(), Point3f(stack->step()));
      if(!loadTIFFImage(fileName, data, true)) {
        stack->setSize(Point3u(0, 0, 0));
        stack->main()->changed();
        stack->work()->changed();
        setErrorMessage(
          QString("Error, cannot load TIFF file %1. Check standard output for exact error.").arg(fileName));
        return false;
      }
      stack->setSize(data.size);
      stack->setStep(Point3d(data.step));
      stack->center();
      store->setLabels(data.labels);
    } else
      throw QString("StackOpen::Error:Invalid file type - %1").arg(fileName);
  
    if(store->labels()) {
      ushort max_label = 0;
      const HVecUS& data = store->data();
      forall(const ushort& u, data) {
        ushort label = u;
        if(label > max_label)
          max_label = label;
      }
      stack->setLabel(max_label);
    }
  
    store->setFile(fileName);
    store->changed();
    setStatus(QString("Loaded stack %1, file %2, size %3").arg(stack->userId()).arg(store->file()).arg(toQString(stack->size())));
    return true;
  }
  REGISTER_PROCESS(StackOpen);

  bool StackImport::initialize(QWidget* parent)
  {
    bool is_work_store = stringToWorkStore(parm("Store"));
    int stackId = parm("Stack Id").toInt();
    QString filter("All image files (*.*)");
    QString fileName = parm("File Name");
    bool doGui = fileName.isEmpty();
  
    if(fileName.isEmpty()) {
      QString currentFile;
      if(is_work_store)
        currentFile = getStack(stackId)->work()->file();
      else
        currentFile = getStack(stackId)->main()->file();
      if(!currentFile.isEmpty())
        fileName = currentFile;
    }
  
    // Get the file name
    if(parent and doGui)
      fileName = QFileDialog::getOpenFileName(parent, QString("Select stack file"), fileName, filter, 0, FileDialogOptions);
    if(fileName.isEmpty())
      return false;
  
    setParm("File Name", fileName);
    return true;
  }
  
  bool StackImport::run()
  {
    int stackId = parm("Stack Id").toInt();
    Stack* stack = getStack(stackId);
    Store* store = (stringToWorkStore(parm("Store")) ? stack->work() : stack->main());
    bool res = run(stack, store, parm("File Name"));
    if(res) {
      store->show();
    }
    return res;
  }
  
  bool StackImport::run(Stack* stack, Store* store, QString fileName)
  {
    if(fileName.isEmpty())
      throw QString("%1:run File name empty.").arg(name());
  
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
      throw QString("%1::run Cannot open input file: %2").arg(name()).arg(fileName);
  
    // Load the image
    HVecUS &data = store->data();
    Image3D image(data);
    loadImage(fileName, image, true);
    stack->setSize(image.size);
    stack->setStep(Point3d(image.step));
    stack->setOrigin(Point3d(image.origin));
    stack->center();
    actingFile(fileName);
    store->setFile(fileName);
    store->changed();

    if(data.size() > 0 and stringToBool(parm("Auto Scale"))) {
      ushort min = 0xFFFF, max = 0;
      for(size_t i = 0; i < data.size(); i++) {
        if(min > data[i])
          min = data[i];
        if(max < data[i])
          max = data[i];
      }
      if(max > min and (max < 0xFFFF or min > 0))
        for(size_t i = 0; i < data.size(); i++)
          data[i] = ushort((double(data[i]) - double(min))/(double(max) - double(min)) * double(0xFFFF));
    }

    setStatus(QString("Loaded stack %1, file %2, size %3").arg(stack->userId()).arg(store->file()).arg(toQString(stack->size())));
    return true;
  }
  REGISTER_PROCESS(StackImport);

  bool StackSave::run()
  {
    int stackId = parm("Stack Id").toInt();
    int compressionLevel = parm("Compression").toInt();
    Stack* stack = this->getStack(stackId);
    Store* store = (stringToWorkStore(parm("Store")) ? stack->work() : stack->main());
    bool res = run(stack, store, parm("File Name"), compressionLevel);
    return res;
  }
  
  bool StackSave::run(Stack* stack, Store* store, const QString& fileName, int compressionLevel)
  {
    if(fileName.isEmpty()) {
      setErrorMessage("Error, trying to save a stack with an empty fileName.");
      return false;
    }
    actingFile(fileName);

    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
      throw QString("Cannot open output file - %1").arg(fileName);

    store->setFile(stripCurrentDir(fileName));

    HVecUS& data = store->data();

    progressStart(QString("Saving %1 Stack %2").arg(store == stack->main() ? "main" : "work").arg(stack->userId()),
                  0);
    qint64 qty;

    // Process the various types
    if(fileName.endsWith(".mdxs", Qt::CaseInsensitive)) {
      if(compressionLevel > 9)
        compressionLevel = 9;
      if(compressionLevel < -1)
        compressionLevel = -1;
      Point3u size = stack->size();
      Point3d step = stack->step();
      Point3d origin = stack->origin();
      quint32 xsz = size.x();
      quint32 ysz = size.y();
      quint32 zsz = size.z();
      quint8 cl = (quint8)compressionLevel;
      float xum = step.x();
      float yum = step.y();
      float zum = step.z();
      float sxum = origin.x();
      float syum = origin.y();
      float szum = origin.z();
      bool isLabel = store->labels();
      quint64 datasz = quint64(data.size()) * 2ul;

      static const char version[] = "MDXS 1.3 ";
      qty = file.write(version, sizeof(version) - 1);
      if(qty != sizeof(version) - 1)
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(version)
                                                                                                 - 1);
      qty = file.write((const char*)&isLabel, sizeof(bool));
      if(qty != sizeof(bool))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(bool));
      qty = file.write((const char*)&sxum, sizeof(float));
      if(qty != sizeof(float))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
      qty = file.write((const char*)&syum, sizeof(float));
      if(qty != sizeof(float))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
      qty = file.write((const char*)&szum, sizeof(float));
      if(qty != sizeof(float))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
      qty = file.write((const char*)&xsz, sizeof(quint32));
      if(qty != sizeof(quint32))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint32));
      qty = file.write((const char*)&ysz, sizeof(quint32));
      if(qty != sizeof(quint32))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint32));
      qty = file.write((const char*)&zsz, sizeof(quint32));
      if(qty != sizeof(quint32))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint32));
      qty = file.write((const char*)&xum, sizeof(float));
      if(qty != sizeof(float))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
      qty = file.write((const char*)&yum, sizeof(float));
      if(qty != sizeof(float))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
      qty = file.write((const char*)&zum, sizeof(float));
      if(qty != sizeof(float))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
      qty = file.write((const char*)&datasz, sizeof(quint64));
      if(qty != sizeof(quint64))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint64));
      qty = file.write((const char*)&cl, sizeof(quint8));
      if(qty != sizeof(quint8))
        throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint8));

      if(compressionLevel > 0) {
        // QByteArray stkdata = qCompress((const uchar *)data.data(), data.size() * 2ul, 1);
        // file.write(stkdata);
        // Make slices of 64MB
        quint64 nb_slices = datasz >> 26;
        quint64 slice_size = 1ul << 26;
        progressStart("Writing Slices", nb_slices);
        for(quint64 i = 0; i < nb_slices; ++i) {
          QByteArray stkdata
              = qCompress((const uchar*)data.data() + i * slice_size, slice_size, compressionLevel);
          quint32 sz = stkdata.size();
          qty = file.write((const char*)&sz, sizeof(quint32));
          if(qty != sizeof(quint32)) {
            throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                  sizeof(quint32));
          }
          qty = file.write(stkdata);
          if(qty != stkdata.size()) {
            throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                  stkdata.size());
          }
          if(!progressAdvance(i))
            userCancel();
        }
        quint64 left_size = datasz - (nb_slices << 26);
        QByteArray stkdata = qCompress((const uchar*)data.data() + (nb_slices << 26), left_size, compressionLevel);
        quint32 sz = stkdata.size();
        qint64 qty = file.write((const char*)&sz, sizeof(quint32));
        if(qty != sizeof(quint32)) {
          throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                sizeof(quint32));
        }
        qty = file.write(stkdata);
        if(qty != stkdata.size()) {
          throw QString("Could not write enough data from MDXS file (written = %1, expected = %2)").arg(qty).arg(
                stkdata.size());
        }
        if(!progressAdvance(nb_slices))
          userCancel();
      } else {
        // Ok, if greater than 64MB, write in bits
        quint64 nb_slices = datasz >> 26;
        quint64 slice_size = 1ul << 26;
        for(quint64 i = 0; i < nb_slices; ++i) {
          qty = file.write((char*)data.data() + i * slice_size, slice_size);
          if(quint64(qty) != slice_size) {
            throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                  slice_size);
          }
        }
        quint64 left_size = datasz - (nb_slices << 26);
        qty = file.write((char*)data.data() + (nb_slices << 26), left_size);
        if(quint64(qty) != left_size) {
          throw QString("Could not write enough data from MDXS file (written = %1, expected = %2)").arg(qty).arg(
                left_size);
        }
      }
      progressAdvance(0);

      file.close();
    } else if(fileName.endsWith(".inr", Qt::CaseInsensitive)) {
      file.close();
      Point3u size(stack->size());
      Point3f step(stack->step());
      CImgUS image(&data[0], size.x(), size.y(), size.z(), 1, true);
      image.save_inr(fileName.toStdString().data(), step.data());
    } else if(fileName.endsWith(".tif", Qt::CaseInsensitive) or fileName.endsWith(".tiff", Qt::CaseInsensitive)) {
      file.close();
      Point3u size(stack->size());
      Point3f step(stack->step());
      Image3D image(data, size, step, store->labels());
      saveTIFFImage(fileName, image);
    } else
      throw QString("Invalid file type %1").arg(fileName);

    store->setFile(stripCurrentDir(fileName));

    setStatus(QString("Saved Stack %1 size: %2, voxel size: %3").arg(store->file()).arg(toQString(stack->size())).arg(toQString(stack->step())));
    return true;
  }
  
  bool StackSave::initialize(QWidget* parent)
  {
    int stackId = parm("Stack Id").toInt();
    QString mdxsFilter("MorphoDynamX Stack files (*.mdxs)"), inrFilter("Inria Stack files (*.inr)");
    QString tiffFilter("TIFF Stack files (*.tif *.tiff)"), filter;
    Stack* s = getStack(stackId);
    Store* store = (stringToWorkStore(parm("Store")) ? s->work() : s->main());
    QString fileName = parm("File Name");
    if(fileName.isEmpty() and parent)
      fileName = store->file();

    // If we loaded a file before, strip of the ending
    if(fileName.endsWith(".txt", Qt::CaseInsensitive))
      fileName = fileName.left(fileName.length() - 4) + ".mdxs";
    else if(fileName.endsWith(".mgxs", Qt::CaseInsensitive))
      fileName = fileName.left(fileName.length() - 5) + ".mdxs";

    if(fileName.endsWith(".inr", Qt::CaseInsensitive))
      filter = inrFilter;
    else if(fileName.endsWith(".tif", Qt::CaseInsensitive) or fileName.endsWith(".tiff", Qt::CaseInsensitive))
      filter = tiffFilter;
    else
      filter = mdxsFilter;

    // Get the file name
    if(parent)
      fileName = QFileDialog::getSaveFileName(parent, QString("Select stack file"), fileName,
                                              QString(mdxsFilter + ";;" + inrFilter + ";;" + tiffFilter), &filter,
                                              FileDialogOptions);
    if(fileName.isEmpty())
      return false;

    // Check ending
    if(filter == inrFilter and !fileName.endsWith(".inr", Qt::CaseInsensitive))
      fileName += ".inr";
    else if(filter == tiffFilter
            and !(fileName.endsWith(".tif", Qt::CaseInsensitive) or fileName.endsWith(".tiff", Qt::CaseInsensitive)))
      fileName += ".tif";
    else if(filter == mdxsFilter and !fileName.endsWith(".mdxs", Qt::CaseInsensitive))
      fileName += ".mdxs";

    setParm("File Name", fileName);
    return true;
  }
  REGISTER_PROCESS(StackSave);
  
  bool StackExport::initialize(QWidget* parent)
  {
    int stackId = parm("Stack Id").toInt();
    Stack* s = getStack(stackId);
    Store* store = (stringToWorkStore(parm("Store")) ? s->work() : s->main());
    QString fileName = store->file();

    if(!parent) return true;

    QDialog dlg(parent);
    Ui_ExportStackDialog ui;
    ui.setupUi(&dlg);
    this->dlg = &dlg;
    this->ui = &ui;

    connect(ui.SelectImageFile, SIGNAL(clicked()), this, SLOT(selectImageFile()));
    connect(ui.ImageType, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(setImageType(const QString &)));

    QStringList suppTypes = supportedImageWriteFormats();
    QStringList types;
    foreach(QString f, suppTypes) {
      types << f;
    }

    ui.ImageType->addItems(types);
    ui.generateVoxelSpacing->setChecked(stringToBool(parm("Gen Spacing")));

    if(dlg.exec() == QDialog::Accepted) {
      setParm("Type", suppTypes[ui.ImageType->currentIndex()]);
      setParm("File Name", ui.ImageFile->text());
      setParm("Gen Spacing", ui.generateVoxelSpacing->isChecked() ? "Yes" : "No");
      setParm("Num Digits", QString::number(ui.nbDigits->value()));
      return true;
    }
    return false;
  }
  
  void StackExport::selectImageFile()
  {
    QStringList filter;
    foreach(QString f, supportedImageWriteFormats()) {
      if(f != "CImg Auto")
        filter << QString("%1 images (*.%2 *.%1)").arg(f.toUpper()).arg(f.toLower());
      else
        filter << "CImg Auto (*.*)";
    }
    QString fileName = ui->ImageFile->text();
    QString selected;
    fileName = QFileDialog::getSaveFileName(dlg, "Filename to export to", fileName, filter.join(";;"), &selected);
    if(!fileName.isEmpty()) {
      int idx = filter.indexOf(selected);
      ui->ImageType->setCurrentIndex(idx);
      if(!selected.startsWith("CImg Auto")) {
        QString type = selected.left(selected.indexOf(" ")).toLower();
        if(!fileName.endsWith("." + type, Qt::CaseInsensitive)) {
          fileName += "." + type;
        }
      }
      ui->ImageFile->setText(fileName);
    }
  }
  
  void StackExport::setImageType(const QString& type)
  {
    if(!type.startsWith("CImg Auto")) {
      QString t = type.toLower();
      QString fn = ui->ImageFile->text();
      int idx = fn.lastIndexOf(".");
      if(idx != -1)
        fn = fn.left(idx + 1) + t;
      else
        fn += "." + t;
      ui->ImageFile->setText(fn);
    }
  }
  
  bool StackExport::run()
  {
    int stackId = parm("Stack Id").toInt();
    Stack* s = getStack(stackId);
    Store* store = (stringToWorkStore(parm("Store")) ? s->work() : s->main());
    QString fileName = parm("File Name");
    QString type = parm("Type");
    uint nb_digits = parm("Num Digits").toUInt();
    if(fileName.isEmpty()) {
      setErrorMessage("Error, fileName provided is empty");
      return false;
    }
    if(type.isEmpty()) {
      setErrorMessage("Error, type provided is empty");
      return false;
    }
    if(nb_digits > 500) {
      setErrorMessage("Error, the number of digits asked for is greater than 500. It seems unreasonnably high.");
      return false;
    }
    return run(s, store, fileName, type, nb_digits, stringToBool(parm("Gen Spacing")));
  }
  
  bool StackExport::run(Stack* stack, Store* store, const QString& fileName, const QString& type,
                        unsigned int nb_digits, bool generate_voxel_spacing)
  {
    actingFile(fileName);
    Point3u size = stack->size();
    Point3d step = stack->step();
    Image3D image(store->data(), size, Point3f(step));
    saveImage(fileName, image, type, nb_digits);
    if(generate_voxel_spacing) {
      QFileInfo fi(fileName);
      QDir d = fi.dir();
      QFile file(d.filePath("voxelspacing.txt"));
      if(!file.open(QIODevice::WriteOnly)) {
        setErrorMessage(QString("Error, cannot open file '%1' for writing.").arg(file.fileName()));
        return false;
      }
      QTextStream ss(&file);
      ss << "x " << stack->step().x() << endl << "y " << stack->step().y() << endl << "z " << stack->step().z()
         << endl;
      file.close();
    }
    return true;
  }
  REGISTER_PROCESS(StackExport);

  bool ClearWorkStack::run(Stack* stack, uint fillval)
  {
    Store* work = stack->work();
    work->allocate();
    if(fillval > 0xFFFF)
      fillval = 0xFFFF;
    HVecUS& data = work->data();
    std::fill_n(data.begin(), data.size(), ushort(fillval));
    work->changed();
    work->setFile("");
    return true;
  }
  REGISTER_PROCESS(ClearWorkStack);
  
  bool ClearMainStack::run(Stack* stack, uint fillval)
  {
    Store* main = stack->main();
    main->allocate();
    if(fillval > 0xFFFF)
      fillval = 0xFFFF;
    HVecUS& data = main->data();
    std::fill_n(data.begin(), data.size(), ushort(fillval));
    main->changed();
    main->setFile("");
    return true;
  }
  REGISTER_PROCESS(ClearMainStack);

  bool SetCurrentStack::run(bool is_main, int id) 
  {
    return setCurrentStack(id, is_main);
  }
  REGISTER_PROCESS(SetCurrentStack);
}
