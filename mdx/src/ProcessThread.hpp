//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PROCESSTHREAD_HPP
#define PROCESSTHREAD_HPP

#include <Process.hpp>

#include <QEvent>
#include <QMutex>
#include <QThread>

namespace mdx 
{
  class mdx_EXPORT ProcessThread : public QThread 
  {
    Q_OBJECT
  public:
    ProcessThread(Process* proc, Process::ProcessAction pAction, QObject* parent)
      : QThread(parent), process(proc), processAction(pAction)  {}
  
    virtual ~ProcessThread() {}
  
    void run();
  
    bool exitStatus() const 
    {
      return _status;
    }
    QString errorMessage() const 
    {
      return _error;
    }
  
  protected:
    Process* process;
    Process::ProcessAction processAction;
    bool _status;
    QString _error;
  };
}
#endif
