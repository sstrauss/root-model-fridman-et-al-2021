//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CCTopology.hpp>
#include <CCUtils.hpp>
#include <Types.hpp>

namespace mdx
{
  bool splitEdges(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &edges, Subdivide *sDiv)
  {
    for(CCIndex e : edges) {
      auto eb = cs.edgeBounds(e);
      SplitStruct ss(e);
      CCIndexFactory.fillSplitStruct(ss);
      if(!cs.splitCell(ss)) {
        mdxInfo << "SplitEdges::run Problem splitting cell" << endl;
        continue;
      }
      // Set the position
      indexAttr[ss.membrane].pos = (indexAttr[eb.first].pos + indexAttr[eb.second].pos)/2.0; 
      // We'll only update the membrane data
      if(sDiv)
        sDiv->splitCellUpdate(1, cs, ss, eb.first, eb.second, 0.5);
    }

    return true;
  }
  
  // These next functions are for the subdivide bisect (when will c++ get local functions?)
  //
  // Get face vertices with longest edge first
  static void faceVerticesLongEdgeFirst(const CCStructure &cs, const CCIndexDataAttr &indexAttr,
					CCIndex f, CCIndexVec &result)
  {
    CCIndexVec fVtx = faceVertices(cs, f);
    double max = 0;
    uint pos = 0;
    for(uint i = 0; i < fVtx.size(); i++) {
      uint j = i + 1;
      if(j >= fVtx.size())
        j = 0;
      double l = norm(indexAttr[fVtx[i]].pos - indexAttr[fVtx[j]].pos);
      if(l > max) {
        max = l;
        pos = i;
      }
    }

    result.resize(fVtx.size());
    for(uint i = 0; i < fVtx.size(); i++) {
      uint j = i + pos;
      if(j >= fVtx.size())
        j -= fVtx.size();
      result[i] = fVtx[j];
    }
  }
      
  // Returns a map of triangles and their vertices with longest edge first for cascading bisection subdivision
  static CCIndexCCIndexVecAttr triSubdivBisectMap(const CCStructure &cs, const CCIndexDataAttr &indexAttr, const CCIndexVec &faces,CCIndexTbbSet *propFaces = NULL)
  {
    CCIndexCCIndexVecAttr result;

    for(uint i = 0; i < faces.size(); i++) {
      // Find all affected triangles, must divide by longest edge
      CCIndex f = faces[i];
      CCIndexSet fSet;
      fSet.insert(f);
      while(fSet.size() > 0) {
        CCIndex f = *fSet.begin();
        fSet.erase(f);

        // Skip boundary or faces that are already there
        if(f.isPseudocell() or result.count(f) > 0)
          continue;

        CCIndexVec &fVertices = result[f];
	faceVerticesLongEdgeFirst(cs, indexAttr, f, fVertices);
        if(fVertices.size() != 3) {
          mdxInfo << "getTriSubdivBisect Non triangle face found, size:" << fVertices.size() << endl;
          break;
        }
        if(propFaces != NULL && propFaces->find(f)==propFaces->end()){
//            CCIndex sel_edge = CCIndex::UNDEF;
            int sel_index = -1;
            for(uint i=0;i<3 && sel_index == -1;i++){
                CCIndex e = edgeBetween(cs,fVertices[i],fVertices[(i+1)%3]);
                for(CCIndex nf : cs.cobounds(e))
                    if(nf != f && propFaces->find(nf) != propFaces->end()){
//                        sel_edge = e;
                        sel_index = i;
                    }
            }
            if(sel_index != -1)
                std::rotate(fVertices.begin(),fVertices.begin()+sel_index,fVertices.end());
            else
                break;            
        }

        CCIndex e = edgeBetween(cs, fVertices[0], fVertices[1]);
        if(e.isPseudocell())
          break;
        // Insert neighbor on edge to be split
        for(CCIndex nf : cs.cobounds(e))
          if(nf != f)
            fSet.insert(nf);
      }
    }
    return result;
  }

  // Find vertex between v1 and v2 that is not v3
  static CCIndex vertexBetween(const CCStructure &cs, CCIndex f, CCIndex v1, CCIndex v2, CCIndex v3, CCIndex v4 = CCIndex::UNDEF)
  {
    CCIndexVec fVertices = faceVertices(cs, f);
    int sz = int(fVertices.size());
    for(int i = 0; i < sz; i++) {
      if(fVertices[i] != v1)
        continue;
      int j = (i + 2) % sz;
      if(fVertices[j] == v2) {
        j = (i + 1) % sz;
        if(fVertices[j] != v3 and fVertices[j] != v4)
          return fVertices[j];
      }
      // Look the other way
      j = (i + sz - 2) % sz;
      if(fVertices[j] == v2) {
        j = (i + sz - 1) % sz;
        if(fVertices[j] != v3 and fVertices[j] != v4)
          return fVertices[j];
      }
    }
    return CCIndex::UNDEF;
  }

  // Find which face contains v
  static CCIndex faceContains(const CCStructure &cs, CCIndex f1, CCIndex f2, CCIndex v)
  {
    CCStructure::FlipI flip = cs.matchFirst(v, CCIndex::Q, CCIndex::Q, f1);
    if(flip.joint == v)
      return f1;
    flip = cs.matchFirst(v, CCIndex::Q, CCIndex::Q, f2);
    if(flip.joint == v)
      return f2;

    return CCIndex::UNDEF;
  }  

  // Split a triangle face along the long edge, note that the other edges may already be split 
  CCIndex splitFaceLongEdge(CCStructure &cs, CCIndexDataAttr &indexAttr, 
                   CCIndex f, CCIndex v1, CCIndex v2, CCIndex v3, AttrMap<CCIndex, CCIndexVec> &newFaceMap, Subdivide *sDiv)
  {
    // Get edge to split
    CCIndex e = edgeBetween(cs, v1, v2);
    // Vertex splitting edge
    CCIndex v;
    if(!e.isPseudocell() and cs.dimensionOf(e) == 1) {
      // Split the edge
      v = CCIndexFactory.getIndex();
      CCIndex e1 = CCIndexFactory.getIndex();
      CCIndex e2 = CCIndexFactory.getIndex();
      SplitStruct ss(e, v, e1, e2);
      if(!cs.splitCell(ss)) {
        mdxInfo << "splitFaceLongEdge Split of cell failed" << endl;
        return CCIndex::UNDEF;
      }
      indexAttr[v].pos = (indexAttr[v1].pos + indexAttr[v2].pos)/2.0;
      if(sDiv)
        sDiv->splitCellUpdate(1, cs, ss, v1, v2, 0.5); 

    } else {
      // Edge already split
      v = vertexBetween(cs, f, v1, v2, v3);
      if(v.isPseudocell()) {
        mdxInfo << "splitFaceLongEdge Cannot find vertex for split edge v1-v2" << endl;
        return CCIndex::UNDEF;
      }
    }

    // Split the face
    CCIndex f1 = CCIndexFactory.getIndex();
    CCIndex f2 = CCIndexFactory.getIndex();
    CCIndex eb = CCIndexFactory.getIndex();
    SplitStruct ss(f, eb, f1, f2);
    if(!cs.splitCell(ss, +v -v3)) {
      mdxInfo << "splitFaceLongEdge Split of cell failed" << endl;
      return CCIndex::UNDEF;
    }
    if(sDiv)
      sDiv->splitCellUpdate(2, cs, ss); 

    // Fill in new face map
    newFaceMap[f] = CCIndexVec({f1, f2, v});

    return v;
  }

  // Split the neighboring triangle on the long edge (long edge already split)
  CCIndexPair splitNeighborsLongEdge(CCStructure &cs, CCIndexDataAttr &indexAttr, 
                   CCIndex f, CCIndex f1, CCIndex f2, CCIndex v, CCIndex v1, CCIndex v2, CCIndex v3, Subdivide *sDiv)
  {
    CCIndexPair result = std::make_pair(CCIndex::UNDEF, CCIndex::UNDEF);
    while(edgeBetween(cs, v2, v3).isPseudocell()) {
      // If edge between v2 and v3 was split, split the quad v2-vb-v3-v with a vb-v edge
      CCIndex f = faceContains(cs, f1, f2, v2);
      if(f.isPseudocell()) {
        mdxInfo << "splitNeighborLongEdge Cannot find face for split edge, v2-v3" << endl;
        break;
      }
      CCIndex vb = vertexBetween(cs, f, v2, v3, v1, v);
      if(vb.isPseudocell()) {
        mdxInfo << "splitNeighborLongEdge Cannot find vertex for split edge, v2-v3" << endl;
        break;
      }
      CCIndex f1 = CCIndexFactory.getIndex();
      CCIndex f2 = CCIndexFactory.getIndex();
      CCIndex eb = CCIndexFactory.getIndex();
      SplitStruct ss(f, eb, f1, f2);
      if(!cs.splitCell(ss, +vb -v)) {
        mdxInfo << "splitNeighborLongEdge Split of cell failed" << endl;
        break;
      }
      if(sDiv)
        sDiv->splitCellUpdate(2, cs, ss); 

      result.first = vb;

      break;
    }
    while(edgeBetween(cs, v3, v1).isPseudocell()) {
      // If edge between v3 and v1 was split, split the quad v3 vb v1 v with a vb-v edge
      CCIndex f = faceContains(cs, f1, f2, v1);
      if(f.isPseudocell()) {
        mdxInfo << "splitNeighborLongEdge Cannot find face for split edge, v3-v1" << endl;
        break;
      }
      CCIndex vb = vertexBetween(cs, f, v3, v1, v2, v);
      if(vb.isPseudocell()) {
        mdxInfo << "splitNeighborLongEdge Cannot find vertex for split edge, v3-v1" << endl;
        break;
      }
      CCIndex f1 = CCIndexFactory.getIndex();
      CCIndex f2 = CCIndexFactory.getIndex();
      CCIndex eb = CCIndexFactory.getIndex();
      SplitStruct ss(f, eb, f1, f2);
      if(!cs.splitCell(ss, +vb -v)) {
        mdxInfo << "splitNeighborLongEdge Split of cell failed" << endl;
        break;
      }
      if(sDiv)
        sDiv->splitCellUpdate(2, cs, ss); 

      result.second = vb;

      break;
    }
    return result;
  }

  bool subdivideBisectTriangle(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &faces, Subdivide *sDiv,CCIndexTbbSet *propFaces)
  {
    // Get list of triangles to subdivide, and their vertices in order with the long edge first
    auto triMap = triSubdivBisectMap(cs, indexAttr, faces,propFaces);
    // Map to hold new faces that might need to be split when long edge is not shared by neighbor 
    // Indexed by the original face, it contains the two new faces (f1, f2) and the vertex added to split them v
    // If either face is a quad it will be split in next pass 
    AttrMap<CCIndex, CCIndexVec> newFaceMap;
    for(auto pr : triMap) {
      CCIndex f = pr.first;
      CCIndex v1 = pr.second[0];
      CCIndex v2 = pr.second[1];
      CCIndex v3 = pr.second[2];

      // This should not fail
      if(splitFaceLongEdge(cs, indexAttr, f, v1, v2, v3, newFaceMap, sDiv) == CCIndex::UNDEF) {
        mdxInfo << "subdivideBisectTriangle Cannot split long edge face" << endl;
        continue;
      }
    }

    // Split quads formed when neighbor longest edge was different from those in triMap
    for(auto pr : triMap) {
      CCIndex f = pr.first;
      CCIndexVec &fVec = newFaceMap[f];
      CCIndex f1 = fVec[0];
      CCIndex f2 = fVec[1];
      CCIndex v = fVec[2];
      CCIndex v1 = pr.second[0];
      CCIndex v2 = pr.second[1];
      CCIndex v3 = pr.second[2];

      // This only splits quads
      splitNeighborsLongEdge(cs, indexAttr, f, f1, f2, v, v1, v2, v3, sDiv);
    }
    return true;
  }

  // Get face vertices with longest edge first
  static CCIndexVec wedgeCellsLongFaceFirst(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndex w)
  {
    // get the faces and their bounds
    auto wedgeBounds = cs.bounds(w);
    if(wedgeBounds.size() != 5) {
      mdxInfo << QString("wedgeCellsLongFaceFirst Wedge bounds has %1 faces").arg(wedgeBounds.size()) << endl;
      return CCIndexVec();
    }

    CCIndexVec triFaces;
    for(CCIndex f : wedgeBounds) {
      auto bounds = cs.bounds(f);
      if(bounds.size() == 3)
        triFaces.push_back(f);
      else if(bounds.size() != 4) {
        mdxInfo << QString("wedgeCellsLongFaceFirst Wedge face has %1 edges").arg(bounds.size()) << endl;
        return CCIndexVec();
      }
    } 
    // The must be 2 triangles
    if(triFaces.size() != 2) {
      mdxInfo << QString("wedgeCellsLongFaceFirst Wedge has %1 triangle faces").arg(triFaces.size()) << endl;
      return CCIndexVec();
    }
    // If the two triangles share an edge (or a vertex) it is also not a wedge
    if(!cs.meet(triFaces[0], triFaces[1]).isPseudocell()) {
      mdxInfo << QString("wedgeCellsLongFaceFirst Wedge triangles have a shared edge") << endl;
      return CCIndexVec();
    }
      
    // Get the ordered vertices of the faces
    CCIndexVecVec triVertices{faceVertices(cs,triFaces[0]), faceVertices(cs, triFaces[1])};

    // Find triangle with the longest edge (should we use the average?)
    double maxLen = 0;
    uint pos = 0, tri = 0;
    for(uint i = 0; i < 2; i++) {
      for(uint j = 0; j < 3; j++) {
        uint k = (j + 1) % 3;
        double len = norm(indexAttr[triVertices[i][j]].pos - indexAttr[triVertices[i][k]].pos);
        if(len > maxLen) {
          maxLen = len;
          tri = i;
          pos = j;
        }
      }
    }

    // Find the matching vertices in the other face with respect to the tri face
    int other = 1 - tri;
    IntVec otherIndex(3);
    for(uint i = 0; i < 3; i++)
      for(uint j = 0; j < 3; j++) {
        CCIndex e = edgeBetween(cs, triVertices[tri][i], triVertices[other][j]);
        if(!e.isPseudocell()) {
          otherIndex[i] = j;
          break;
        }
      }
    if(otherIndex[0] + otherIndex[1] + otherIndex[2] != 3) {
      mdxInfo << "wedgeCellsLongEdgeFirst Cannot match vertices on opposite face" << endl;
      return CCIndexVec();
    }
     
    CCIndexVec result(8);
    result[0] = triFaces[tri];
    result[1] = triFaces[other];

    // Swap if other triangle goes the other way
    //if(otherIndex[1] != (otherIndex[0] + 1) % 3)

    // Fill in the face vertices
    for(uint i = 0; i < 3; i++) {
     result[2 + i] = triVertices[tri][(pos + i) % 3];
     result[5 + i] = triVertices[other][otherIndex[(pos + i) % 3]];
    }
      
    // Get the long face as well 
    //CCIndex longFace = cs.join(triVertices[tri][0], triVertices[tri][1], triVertices[other][0], triVertices[other][1]);
    //if(longFace.isPseudocell()) {
      //mdxInfo << "wedgeCellsLongEdgeFirst Cannot find face that has longest edge" << endl;
      //return CCIndexVec();
    //}
    //result[0] = longFace;

    return result;
  } 

  // Returns a map of triangles and their vertices with longest edge first for cascading bisection subdivision
  static CCIndexCCIndexVecAttr wedgeSubdivBisectMap(const CCStructure &cs, const CCIndexDataAttr &indexAttr, const CCIndexVec &wedges)
  {
    CCIndexCCIndexVecAttr result;

    #pragma omp parallel for
    for(uint i = 0; i < wedges.size(); i++) {
      // Find all affected volumes, divide by longest quad face
      CCIndex w = wedges[i];
      CCIndexSet wSet;
      wSet.insert(w);
      while(wSet.size() > 0) {
        CCIndex w = *wSet.begin();
        wSet.erase(w);

        // Skip boundary or faces that are already there
        if(w.isPseudocell() or result.count(w) > 0)
          continue;

        CCIndexVec wCells = wedgeCellsLongFaceFirst(cs, indexAttr, w);
        if(wCells.size() != 8) {
          mdxInfo << "wedgeSubdivBisectMap Non wedge volume found" << endl;
          break;
        }
        result[w] = wCells;
        // Insert neighbor on other size of (quad) face to be split
        CCIndex f = cs.join(wCells[2], wCells[6]); // two vertices on diagonal of face
        if(f.isPseudocell()) {
          mdxInfo << "wedgeSubdivBisectMap Cannot find long edge face" << endl;
          break;
        }
        for(CCIndex nw : cs.cobounds(f))
          if(nw != w)
            wSet.insert(nw);
      }
    }
    return result;
  }

  bool subdivideBisectWedge(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &volumes, Subdivide *sDiv)
  {
    // Get list of triangles to subdivide, and their vertices in order with the long edge first
    auto wedgeMap = wedgeSubdivBisectMap(cs, indexAttr, volumes);
    // Map to hold new wedges that might need to be split when long edge is not shared by neighbor 
    // Indexed by the original wedge, it contains the two new wedges (w1, w2) and the edge added to split them e, and its endpoints
    // If either wedge is a hexahedron it will be split in next pass 
    AttrMap<CCIndex, CCIndexVec> newFaceMap;
    AttrMap<CCIndex, CCIndexVec> newWedgeMap;
    for(auto pr : wedgeMap) {
      CCIndex w = pr.first;
      CCIndexVec v(2); // Vertices on long edge
      CCIndexVec vn(2); // Vertices opposite
      // Split both faces
      for(uint i = 0; i < 2; i++) {
        CCIndex f = pr.second[i];
        CCIndex v1 = pr.second[i*3 + 2];
        CCIndex v2 = pr.second[i*3 + 3];
        CCIndex v3 = pr.second[i*3 + 4];
        vn[i] = v3;

        v[i] = splitFaceLongEdge(cs, indexAttr, f, v1, v2, v3, newFaceMap, sDiv);
        if(v[i] == CCIndex::UNDEF) {
          mdxInfo << "subdivideBisectWedge Cannot split long edge face: " << i << endl;
          continue;
        }
      }

      // Split the quad face that had the longest edge if not already
      auto e0 = cs.orientedEdge(v[0], v[1]);
      if((~e0).isPseudocell())  {
        CCIndexSet i0 = cs.incidentCells(v[0], 2);
        CCIndexSet i1 = cs.incidentCells(v[1], 2);
        CCIndexVec f;
        std::set_intersection(i0.begin(), i0.end(), i1.begin(), i1.end(), std::back_inserter(f));
        if(f.size() != 1) {
          // Already split if 3 or more?
          mdxInfo << QString("subdivideBisectWedge Found %1 when trying to split wedge quad, should be 1").arg(f.size()) << endl;
          continue;
        }

        CCIndex f1 = CCIndexFactory.getIndex();
        CCIndex f2 = CCIndexFactory.getIndex();
        CCIndex eb = CCIndexFactory.getIndex();
        e0 = +eb;
        SplitStruct ss(f[0], eb, f1, f2);
        if(!cs.splitCell(ss, +v[0] -v[1])) {
          mdxInfo << "subdivideBisectWedge Split of cell failed" << endl;
          return false;
        }
        if(sDiv)
          sDiv->splitCellUpdate(2, cs, ss); 
      }

      // split the volume
      CCIndex w1 = CCIndexFactory.getIndex();
      CCIndex w2 = CCIndexFactory.getIndex();
      CCIndex fb = CCIndexFactory.getIndex();
      SplitStruct ss(w, fb, w1, w2);

      auto e1 = cs.orientedEdge(v[1], vn[1]);
      auto e2 = cs.orientedEdge(vn[1], vn[0]);
      auto e3 = cs.orientedEdge(vn[0], v[0]);
      if(!cs.splitCell(ss, +e0 +e1 +e2 +e3)) {
        mdxInfo << "subdivideBisectWedge Split of cell failed" << endl;
        return false;
      }
      if(sDiv)
        sDiv->splitCellUpdate(3, cs, ss); 
    }

    // Split quads formed when neighbor longest edge was different from those in triMap
    for(auto pr : wedgeMap) {
      CCIndex w = pr.first;
      CCIndexVec vn(2);
      CCIndexPairVec vb(2);
      for(uint i = 0; i < 2; i++) {
        CCIndex f = pr.second[i];
        CCIndexVec &fVec = newFaceMap[f];
        CCIndex f1 = fVec[0];
        CCIndex f2 = fVec[1];
        CCIndex v = fVec[2];
        CCIndex v1 = pr.second[i*3 + 2];
        CCIndex v2 = pr.second[i*3 + 3];
        CCIndex v3 = pr.second[i*3 + 4];

        vn[i] = v;

        // Either of the two neighbor faces will be split if they are quads
        vb[i] = splitNeighborsLongEdge(cs, indexAttr, f, f1, f2, v, v1, v2, v3, sDiv);
      }
      if(vb[0].first.isPseudocell() != vb[1].first.isPseudocell())
        mdxInfo << "subdivideBisectWedge Top and bottom don't match, first vb0:" << vb[0].first << " " << " vb1:" << vb[1].first << endl;
      if(vb[0].second.isPseudocell() != vb[1].second.isPseudocell())
        mdxInfo << "subdivideBisectWedge Top and bottom don't match, second, vb0:" << vb[0].second << " " << " vb1:" << vb[1].second << endl;

      // Skip the volume if first neighbor triangle is not a quad
      if(!vb[0].first.isPseudocell() and !vb[1].first.isPseudocell()) {
        // split the volume, face is already there
        w = cs.join(vn[0], vb[1].first);
        if(w.isPseudocell()) {
          mdxInfo << "subdivideBisectWedge Cannot find volume to split" << endl;
          continue;
        }
        if(!cs.hasCell(w) or cs.dimensionOf(w) != 3) {
          mdxInfo << "subdivideBisectWedge Volume to split is wrong dimension" << endl;
          continue;
        }
        
        CCIndex w1 = CCIndexFactory.getIndex();
        CCIndex w2 = CCIndexFactory.getIndex();
        CCIndex fb = CCIndexFactory.getIndex();
        SplitStruct ss(w, fb, w1, w2);
  
        auto e0 = cs.orientedEdge(vn[0], vn[1]);
        auto e1 = cs.orientedEdge(vn[1], vb[1].first);
        auto e2 = cs.orientedEdge(vb[1].first, vb[0].first);
        auto e3 = cs.orientedEdge(vb[0].first, vn[0]);
        if(!cs.splitCell(ss, +e0 +e1 +e2 +e3)) {
          mdxInfo << "subdivideBisectWedge Split of cell failed" << endl;
          return false;
        }
        if(sDiv)
          sDiv->splitCellUpdate(3, cs, ss); 
      }
      // Skip the volume if second neighbor triangle is not a quad
      if(!vb[0].second.isPseudocell() and !vb[1].second.isPseudocell()) {
        // split the volume, face is already there
        w = cs.join(vn[0], vb[1].second);
        if(w.isPseudocell()) {
          mdxInfo << "subdivideBisectWedge Cannot find volume to split" << endl;
          continue;
        }
        if(!cs.hasCell(w) or cs.dimensionOf(w) != 3) {
          mdxInfo << "subdivideBisectWedge Volume to split is wrong dimension" << endl;
          continue;
        }
        
        CCIndex w1 = CCIndexFactory.getIndex();
        CCIndex w2 = CCIndexFactory.getIndex();
        CCIndex fb = CCIndexFactory.getIndex();
        SplitStruct ss(w, fb, w1, w2);
  
        auto e0 = cs.orientedEdge(vn[0], vn[1]);
        auto e1 = cs.orientedEdge(vn[1], vb[1].second);
        auto e2 = cs.orientedEdge(vb[1].second, vb[0].second);
        auto e3 = cs.orientedEdge(vb[0].second, vn[0]);
        if(!cs.splitCell(ss, +e0 +e1 +e2 +e3)) {
          mdxInfo << "subdivideBisectWedge Split of cell failed" << endl;
          return false;
        }
        if(sDiv)
          sDiv->splitCellUpdate(3, cs, ss); 
      }
    }
    return true;
  }

  // These next 2 functions are used by polyogonizeTriangles
  //
  // Mark vertices that will be kept
  static void markVertices(const CCStructure &cs, const CCIndexDataAttr &indexAttr, 
                              int label, CCIndex f0, CCIndex v0, CCIndexTbbSet &vKeep, double maxDistance) 
  {
    if(maxDistance < 0)
      maxDistance = std::numeric_limits<double>::max();

    // v0 is on the border of a cell with label l containing f0:
    CellTuple tuple(cs,v0,f0);
    Point3d lastPos = indexAttr[v0].pos;
    do {
      bool keep = false;
      CCIndex v = tuple[0];
      Point3d pos = indexAttr[v].pos;
      // If we are over the distance keep the point
      if(vKeep.count(v) > 0)
        keep = true;
      else if(norm(pos - lastPos) >= maxDistance)
        keep = true;
      else {
        // Keep junction points
        CCIndexSet neighborFaces = cs.incidentCells(tuple[0],2);
        IntSet labels;
        for(CCIndex f : neighborFaces)
          labels.insert(indexAttr[f].label);
        if(labels.size() > 2 or (labels.size() == 2 and cs.onBorder(v))) // keep junctions in the inside and border junctions
          keep = true;
      }
      if(keep) {
        vKeep.insert(v);
        lastPos = pos;
      }

      while(indexAttr[tuple.other(2)].label == label)
        tuple.flip(2,1);  // rotate around vertex to next edge

      // reaching here means the other face is in a different cell
      tuple.flip(0,1); // edge is on boundary of cell; advance v

    } while(tuple[0] != v0);
  }

  // Get cell boundaries
  static CCIndexVec cellBounds(const CCStructure &cs, const CCIndexDataAttr &indexAttr, 
                                               int label, CCIndex f0, CCIndex v0, CCIndexTbbSet &vKeep) 
  {
    std::vector<CCIndex> cellOutline;

    // v0 is on the border of a cell with label l containing f0:
    CellTuple tuple(cs,v0,f0);
    do {
      CCIndex v = tuple[0];
      if(vKeep.count(v) > 0)
        cellOutline.push_back(v);

      while(indexAttr[tuple.other(2)].label == label)
        tuple.flip(2,1);  // rotate around vertex to next edge

      // reaching here means the other face is in a different cell
      tuple.flip(0,1); // edge is on boundary of cell; advance v

    } while(tuple[0] != v0);

    return cellOutline;
  }

  // Make polygon mesh from triangle mesh
  bool polygonizeTriangles(const CCStructure &cs, CCStructure &csOut, CCIndexDataAttr &indexAttr, 
                                                          const CCIndexVec &faceVec, double maxDistance)
  {
    // Create list of labels
    AttrMap<int, CCIndexPair> labelMap;
    #pragma omp parallel for
    for(uint i = 0; i < faceVec.size(); i++) {
      CCIndex f = faceVec[i];
      int label = indexAttr[f].label;
      if(label <= 0)
        continue;

      // Skip if we already have this label
      if(labelMap.count(label) > 0)
        continue;

      // find a face of the cell containing a border vertex
      CCIndexSet edges = cs.incidentCells(f,1);

      bool found = false;
      for(CCIndex e : edges) {
        CCIndexSet neighborFaces = cs.incidentCells(e,2);
        //if(neighborFaces.size() == 0) 
          //found = true; // When does this case happen?
        for(CCIndex nf : neighborFaces) {
          if(indexAttr[nf].label == label) 
            continue;
          found = true;
        }
        if(found) {
          // if we get here then the edge e is at the border of the cell
          CCIndexSet borderVertices = cs.incidentCells(e,0); // now get a vertex of this edge
          labelMap[label] = std::make_pair(f, *borderVertices.begin());
          break;
        }
      }
    }

    // Put labels and associated face and vertex in vectors
    IntVec labels;
    labels.reserve(labelMap.size());
    CCIndexPairVec vfPair;
    vfPair.reserve(labelMap.size());
    for(auto &pr : labelMap) {
      labels.push_back(pr.first);
      vfPair.push_back(pr.second);
    }
      
    // Mark the vertices on the boundaries to keep
    CCIndexTbbSet vKeep;
    #pragma omp parallel for
    for(uint i = 0; i < labels.size(); i++)
      markVertices(cs, indexAttr, labels[i], vfPair[i].first, vfPair[i].second, vKeep, maxDistance);

    // Get the cell boundaries for the cells
    AttrMap<CCIndex, CCIndexVec> faceBounds;
    #pragma omp parallel for
    for(uint i = 0; i < labels.size(); i++) {
      CCIndex f = CCIndexFactory.getIndex();
      faceBounds[f] = cellBounds(cs, indexAttr, labels[i], vfPair[i].first, vfPair[i].second, vKeep);
      indexAttr[f].label = labels[i];
    }

    // Create vertices and faces
    AttrMap<CCIndex, uint> vertexMap;
    CCIndexVec faces;
    CCIndexVec vertices;
    UIntVecVec faceVertices(labels.size());
    for(auto &pr : faceBounds) {
      if(pr.second.size() < 3){ // not a proper face
        continue;
      }
      uint fidx = faces.size();
      faces.push_back(pr.first);
      for(CCIndex v : pr.second) {
        if(vertexMap.count(v) == 0) {
          CCIndex newV = CCIndexFactory.getIndex();
          indexAttr[newV] = indexAttr[v];
          faceVertices[fidx].push_back(vertices.size());
          vertexMap[v] = vertices.size();
          vertices.push_back(newV);
        } else{
          faceVertices[fidx].push_back(vertexMap[v]);
        }
      }
    }
    // Create the cell complex
    return ccFromFaces(csOut, vertices, faces, faceVertices);
  }

  bool subdivideTriangles(CCStructure &cs, CCIndexDataAttr &indexAttr, Subdivide *sDiv)
  {
    std::set<CCIndex> newVertices;

    // Split all edges
    std::vector<CCIndex> oldEdges = cs.edges();
    for(CCIndex edge : oldEdges) {
      CCStructure::SplitStruct ss(edge);
      CCIndexFactory.fillSplitStruct(ss);
      CCIndex v = ss.membrane;

      auto eb = cs.edgeBounds(edge);
      if(!cs.splitCell(ss)) {
        mdxInfo << "Could not split edge " << ccf::to_string(edge).c_str();
	if(confirmCCF(cs)) {
	  ccf::CCStructure &csX = ensureCCF(cs);
          mdxInfo << ", error is " << csX.error.c_str();
	}
	mdxInfo << endl;
        return false;
      } else {
        // Set the position
        indexAttr[ss.membrane].pos = (indexAttr[eb.first].pos + indexAttr[eb.second].pos)/2.0; 
        if(sDiv)
          sDiv->splitCellUpdate(1, cs, ss, eb.first, eb.second, 0.5);
        
      }
      newVertices.insert(newVertices.end(),v);
    }

    // Split faces into pieces
    std::vector<CCIndex> oldFaces = cs.cellsOfDimension(2);
    forall(CCIndex face, oldFaces) {
      CCStructure::CellTuple tuple(cs,face);

      // first make sure that the tuple's vertex is a new one
      if(newVertices.find(tuple[0]) == newVertices.end())
        tuple.flip(0);

      // get the vertices in order
      std::vector<CCIndex> vs;
      do {
        vs.push_back(tuple[0]);
        tuple.flip(0,1,0,1);
      } while(vs[0] != tuple[0]);

      // now split the faces
      uint numVs = vs.size();
      for(uint i = 0 ; i < numVs ; i++) {
        uint i1 = (i + 1) % numVs;
        CCStructure::SplitStruct ss(cs.join(vs[i],vs[i1]));
        CCIndexFactory.fillSplitStruct(ss);
        if(!cs.splitCell(ss, +vs[i] -vs[i1])) {
          mdxInfo << "Could not split face " << ccf::to_string(ss).c_str();
	  if(confirmCCF(cs)) {
	    ccf::CCStructure &csX = ensureCCF(cs);
	    mdxInfo << ", error is " << csX.error.c_str();
	  }
	  mdxInfo << endl;
          return false;
        }
      }
    }
    return true;
  }
}
  
