//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_STRUCTURE_HPP
#define MESH_PROCESS_STRUCTURE_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <MeshUtils.hpp>
#include <MDXSubdivide.hpp>
#include <QtXml>
#include <QRegExp>
#include <MDXViewer/qglviewer.h>
namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{

  /**
   * \class MeshSubdivide MeshProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Subdivide (triangle) faces
   */
  class mdxBase_EXPORT MeshSubdivide : public Process
  {
  // RSS FIXME Does this also convert a mesh to triangles? If so, maybe it could be split
  public:
    MeshSubdivide(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Subdivide");
      setDesc("Subdivide the mesh uniformly");
      setIcon(QIcon(":/images/SubdivideTri.png"));
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updateAll(ccName);
      MDXSubdivide sDiv(*mesh);
      return run(*mesh, cs, &sDiv);
    }
    bool run(Mesh &mesh, CCStructure &cs, Subdivide *sDiv);
  };

  /**
   * \class SubdivideTriangles MeshProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Subdivide an entire triangle mesh
   */
  class mdxBase_EXPORT SubdivideTriangles : public Process
  {
  public:
    SubdivideTriangles(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Subdivide Triangles");
      setDesc("Subdivide the mesh uniformly");
      setIcon(QIcon(":/images/SubdivideTri.png"));
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updateAll(ccName);
      MDXSubdivide sDiv(*mesh);
      run(*mesh, cs, &sDiv);

      setStatus(QString("Subdivide triangled, vertices %1, edges %2, faces %3.")
          .arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()));

      return true;
    }
    bool run(Mesh &mesh, CCStructure &cs, Subdivide *sDiv);
  };

  /**
   * \class SubdivideBisectTriangle MeshProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Subdivide triangles with longest edge propagating bisection.
   */
  class mdxBase_EXPORT SubdivideBisectTriangle : public Process
  {
  public:
    SubdivideBisectTriangle(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Subdivide Bisect Triangle");
      setDesc("Subdivide triangles mesh with longest edge propagating bisection");
      setIcon(QIcon(":/images/SubdivideBisect.png"));

      addParm("Max Area", "Area threshold for subdivision, 0 for all selected", "0.0");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1:run No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updateAll(ccName);
      MDXSubdivide sDiv(*mesh);
      run(*mesh, cs, parm("Max Area").toDouble(), &sDiv);

      setStatus(QString("Subdivide bisect triangles, vertices %1, edges %2, faces %3 volumes %4.")
                      .arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()).arg(cs.volumes().size()));

      return true;
    }
    bool run(Mesh &mesh, CCStructure &cs, double maxArea, Subdivide *sDiv);
  };

  /**
   * \class SubdivideBisectWedge MeshProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Subdivide wedges with longest edge propagating bisection.
   */
  class mdxBase_EXPORT SubdivideBisectWedge : public Process
  {
  public:
    SubdivideBisectWedge(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Subdivide Bisect Wedge");
      setDesc("Subdivide wedges mesh with longest edge propagating bisection");
      setIcon(QIcon(":/images/SubdivideBisect.png"));

      addParm("Max Area", "Area threshold for subdivision, 0 for all selected", "0.0");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updateAll(ccName);
      MDXSubdivide sDiv(*mesh);
      run(*mesh, cs, parm("Max Area").toDouble(), &sDiv);

      setStatus(QString("Subdivide bisect wedges, vertices %1, edges %2, faces %3 volumes %4.")
                      .arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()).arg(cs.volumes().size()));

      return true;
    }
    bool run(Mesh &mesh, CCStructure &cs, double maxArea, Subdivide *sDiv);
  };


  /**
   * \class MergeCells MeshProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Merge faces or volumes 
   */
  class mdxBase_EXPORT MergeCells : public Process
  {
  public:
    MergeCells(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Merge Cells");
      setDesc("Merge faces or volumes");
      setIcon(QIcon(":/images/SubdivideTri.png"));
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();

      MDXSubdivide sDiv(*mesh);
      run(cs, indexAttr, selectedFaces(cs, indexAttr), &sDiv);
      mesh->updateAll(ccName);

      setStatus(QString("Merged cells, vertices %1, edges %2, faces %3 volumes %4")
          .arg(cs.cellCount(0)).arg(cs.cellCount(1)).arg(cs.cellCount(2)).arg(cs.cellCount(3)));

      return true;
    }
    bool run(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &faces, Subdivide *sDiv);
  };
  ///@}
  
  /**
   * \class TransformMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Apply an affine transformation to the vertices of a mesh (all of them or just 
   * the selected ones).
   */
  class mdxBase_EXPORT TransformMesh : public Process 
  {
  public:
    TransformMesh(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Transform Mesh");
      setDesc("Apply an affine transformation to all vertices of a mesh");
      setIcon(QIcon(":/images/Resize.png"));

      addParm("Translation", "Translation in micrometers. Enter x y z separated by spaces", "0.0 0.0 0.0");
      addParm("Rotation Axis", "Vector representing the axis of rotation. Enter x y z separated by spaces", "1.0 0.0 0.0");
      addParm("Angle", "Angle of rotation in degrees", "0.0");
      addParm("Scale", "Global scaling factor", "1.0");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      mesh->updatePositions(ccName);
      return run(mesh->ccStructure(ccName), mesh->indexAttr(), 
          stringToPoint3d(parm("Translation")), stringToPoint3d(parm("Rotation Axis")), parm("Angle").toDouble(), parm("Scale").toDouble());
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, 
          const Point3d &translation, const Point3d &rotation, double angle, double scale);
  };

  /**
   * \class CenterMesh MeshProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Center the mesh
   */
  class mdxBase_EXPORT CenterMesh : public Process
  {
  public:
    CenterMesh(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Center Mesh");
      setDesc("Center the mesh");
      setIcon(QIcon(":/images/Resize.png"));
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updatePositions(ccName);
      return run(cs, mesh->indexAttr());
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr);
  };

   
  /**
   * \class TransformMesh by Frame ProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * transform the mesh coordinates based on the ones visualized on the screen
   * 
   */
  class mdxBase_EXPORT TransformMeshByFrame : public Process 
  {
  public:
    TransformMeshByFrame(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Transform Mesh by frame");
      setDesc("Apply an affine transformation to all vertices of a mesh based on how it get rotated/translated on the screen");
      setIcon(QIcon(":/images/Resize.png"));

      //addParm("Translation", "Translation in micrometers. Enter x y z separated by spaces", "0.0 0.0 0.0");
      //addParm("Rotation Axis", "Vector representing the axis of rotation. Enter x y z separated by spaces", "1.0 0.0 0.0");
      //addParm("Angle", "Angle of rotation in degrees", "0.0");
      //addParm("Scale", "Global scaling factor", "1.0");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());
                      
      //mesh->updatePositions(ccName);
      return run(mesh->ccStructure(ccName), mesh->indexAttr(), mesh, ccName);
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, Mesh *mesh,
          const QString &ccName);
  };


  /**
   * \class SmoothMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Average each vertex position based on its neighbors.
   */
  class mdxBase_EXPORT SmoothMesh : public Process 
  {
  public:
    SmoothMesh(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Smooth Mesh");
      setDesc("Average each vertex position based on its neighbors.");
      setIcon(QIcon(":/images/SmoothMesh.png"));

      addParm("Passes", "Number of smoothing passed to do", "1");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      progressStart("Smoothing mesh");
      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updatePositions(ccName);
      run(cs, mesh->indexAttr(), parm("Passes").toUInt());

      setStatus(QString("Smoothed %1 vertices.").arg(cs.vertices().size()));

      return true;
    }
    bool run(CCStructure &cs, CCIndexDataAttr &indexAttr, uint passes);
  };

  /**
   * \class SmoothBorder ProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Average each vertex position based on its neighbors on the border
   */
  class mdxBase_EXPORT SmoothBorder : public Process 
  {
  public:
    SmoothBorder(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Smooth Border");
      setDesc("Average each vertex position on the border based on its neighbors.");
      setIcon(QIcon(":/images/SmoothMesh.png"));

      addParm("Passes", "Number of smoothing passed to do", "1");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      progressStart("Smoothing mesh");
      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updatePositions(ccName);
      run(cs, mesh->indexAttr(), parm("Passes").toUInt());

      setStatus(QString("Smoothed %1 vertices.").arg(cs.vertices().size()));

      return true;
    }
    bool run(CCStructure &cs, CCIndexDataAttr &indexAttr, uint passes);
  };

  // Split edges over a certain length
  class SplitEdges : public Process
  {
  public:
    SplitEdges(const Process &_proc) : Process(_proc) 
    {
      setName("Mesh/Structure/Split Edges");
      setDesc("Split edges by inserting a new vertices in the middle");
      setIcon(QIcon(":/images/SplitEdge.png"));

      addParm("Max Length", "Length over which edges will be split (um), 0 divides all", "1.0");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      mesh->updateAll(ccName);
      MDXSubdivide sDiv(*mesh);
      run(*mesh, cs, parm("Max Length").toDouble(), &sDiv);

      setStatus(QString("Split Edges, vertices %1, edges %2, faces %3.")
          .arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()));

      return true;
    }
    bool run(Mesh &mesh, CCStructure &cs, double maxLength, Subdivide *sDiv);
  };

//  
//  /**
//   * \class ReverseMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Reverse the orientation of the mesh.
//   */
//  class mdxBase_EXPORT ReverseMesh : public Process 
//  {
//  public:
//    ReverseMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh());
//    }
//    bool run(Mesh* mesh);
//  
//      setName("Mesh/Structure/Reverse Mesh");
//      setDesc("Reverse orientation of the mesh");
//    QStringList parmNames() const { return QStringList());
//    QStringList parmDescs() const { return QStringList());
//    QStringList parmDefaults() const { return QStringList());
//      setIcon(QIcon(":/images/Invert.png"));
//  };
//  

  
//  /**
//   * \class ShrinkMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Displace each vertex towards the mesh center, perpendicular to the surface.
//   */
//  class mdxBase_EXPORT ShrinkMesh : public Process 
//  {
//  public:
//    ShrinkMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh(), parms[0].toFloat());
//    }
//  
//    bool run(Mesh* mesh, float distance);
//  
//      setName("Mesh/Structure/Shrink Mesh");
//    QString description() const { return 
//      "Displace each vertex towards the mesh center, perpendicular to the surface.");
//    QStringList parmNames() const { return QStringList() << QString("Distance(%1)").arg(UM));
//    QStringList parmDescs() const { return QStringList() 
//      << "Vertex displacement. If negtive, the mesh will expand.");
//    QStringList parmDefaults() const { return QStringList() << "1.0");
//      setIcon(QIcon(":/images/ShrinkMesh.png"));
//  };
//  
//  /**
//   * \class LoopSubdivisionMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Subdivide the mesh uniformly using Loop subdivision.
//   */
//  class mdxBase_EXPORT LoopSubdivisionMesh : public Process 
//  {
//  public:
//    LoopSubdivisionMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh());
//    }
//  
//    bool run(Mesh* mesh);
//  
//      setName("Mesh/Structure/Loop Subdivision");
//      setDesc("Subdivide the mesh uniformly using Loop subdivision.");
//    QStringList parmNames() const { return QStringList());
//    QStringList parmDescs() const { return QStringList());
//      setIcon(QIcon(":/images/SubdivideTri.png"));
//  };
//  
//  /**
//   * \class SubdivideMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Subdivide the mesh uniformely, without displacing existing vertices.
//   */
//  class mdxBase_EXPORT SubdivideMesh : public Process 
//  {
//  public:
//    SubdivideMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh());
//    }
//  
//    bool run(Mesh* mesh);
//  
//      setName("Mesh/Structure/Subdivide");
//      setDesc("Subdivide the mesh unifromly");
//    QStringList parmNames() const { return QStringList());
//    QStringList parmDescs() const { return QStringList());
//      setIcon(QIcon(":/images/SubdivideTri.png"));
//  };
//  
//  /**
//   * \class AdaptiveSubdivideBorderMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Subdivide triangles around cell borders.
//   */
//  class mdxBase_EXPORT AdaptiveSubdivideBorderMesh : public Process 
//  {
//  public:
//    AdaptiveSubdivideBorderMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh(), parms[0].toFloat(), parms[1].toFloat());
//    }
//  
//    bool run(Mesh* mesh, float cellMaxArea, float borderDist);
//  
//      setName("Mesh/Structure/Subdivide Adaptive Near Borders");
//      setDesc("Subdivide triangles around cell borders");
//    QStringList parmNames() const { return QStringList() 
//      << QString("Max Area(%1)").arg(UM2) << QString("Border Dist(%1)").arg(UM));
//    QStringList parmDescs() const { return QStringList() 
//      << "Area threshold (in square microns) for subdivision, triangles smaller "
//         "than this won't be subdivided"
//      << "Distance (in microns) from cell borders that triangles will be subdivided");
//    QStringList parmDefaults() const { return QStringList() << "0.1" << "1.0");
//      setIcon(QIcon(":/images/SubdivideTriAdapt.png"));
//  };
//  
//  /**
//   * \class AdaptiveSubdivideSignalMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Subdivide triangles depending on mesh signal. Triangle size is determined
//   * by a high and low area, which is interpolated based on the minimum and
//   * maximum signals
//   */
//  class mdxBase_EXPORT AdaptiveSubdivideSignalMesh : public Process 
//  {
//  public:
//    AdaptiveSubdivideSignalMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh(), parms[0].toFloat(), parms[1].toFloat());
//    }
//  
//    bool run(Mesh* mesh, float cellMaxAreaLow, float cellMaxAreaHigh);
//  
//      setName("Mesh/Structure/Subdivide Adaptive by Signal");
//    QString description() const { return 
//      "Subdivide triangles depending on mesh signal. Triangle size\n"
//      "is determined by a high and low area, which is interpolated\n"
//      "based on the minimum and maximum signals");
//    QStringList parmNames() const { return QStringList() 
//      << QString("Low Max Area(%1)").arg(UM2) << QString("High Max Area(%1)").arg(UM2));
//    QStringList parmDescs() const { return QStringList() 
//      << "Maximum area (square microns) for low instentity voxels"
//      << "Maximum area (square microns) for high instentity voxels");
//    QStringList parmDefaults() const { return QStringList() << "1.0" << "0.1");
//      setIcon(QIcon(":/images/SubdivideTriAdapt.png"));
//  };
//  
//  /**
//   * \class SubdivideBisectMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Subdivide triangles area with bisection.
//   */
//  class mdxBase_EXPORT SubdivideBisectMesh : public Process 
//  {
//  public:
//    SubdivideBisectMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh(), parms[0].toFloat());
//    }
//  
//    bool run(Mesh* mesh, float cellMaxArea);
//  
//      setName("Mesh/Structure/Subdivide with Bisection");
//      setDesc("Subdivide triangles area with bisection");
//    QStringList parmNames() const { return QStringList() << QString("Max Area(%1)").arg(UM2));
//    QStringList parmDescs() const { return QStringList() << QString("Max Area(%1)").arg(UM2));
//    QStringList parmDefaults() const { return QStringList() << "1.0");
//      setIcon(QIcon(":/images/SubdivideTriAdapt.png"));
//  };
//
//  /**
//   * \class SubdivideBisectMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Subdivide triangles area with bisection.
//   */
//  class mdxBase_EXPORT SubdivideBisectMesh3D : public Process 
//  {
//  public:
//    SubdivideBisectMesh3D(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh(), parms[0].toFloat());
//    }
//  
//    bool run(Mesh* mesh, float cellMaxArea);
//  
//      setName("Mesh/Structure/Subdivide with Bisection MDX3D");
//      setDesc("Subdivide triangles area with bisection");
//    QStringList parmNames() const { return QStringList() << QString("Max Area(%1)").arg(UM2));
//    QStringList parmDescs() const { return QStringList() << QString("Max Area(%1)").arg(UM2));
//    QStringList parmDefaults() const { return QStringList() << "1.0");
//      setIcon(QIcon(":/images/SubdivideTriAdapt.png"));
//  };
//  
  /**
   * \class ScaleMesh ProcessStructure.hpp <MeshProcessStructure.hpp>
   *
   * Scale the mesh. It is possible to specify a negative number, in which case the 
   * dimension will be mirrored.
   */
  class mdxBase_EXPORT ScaleMesh : public Process 
  {
  public:
    ScaleMesh(const Process& process) : Process(process) 
    {
      setName("Mesh/Structure/Scale Mesh");
      setDesc("Scale Mesh, or a selected part of it.\nIt is possible to specify a negative "
               "number, in which case the dimension will be mirrored.\n"
               "If either 1 or 3 axis are mirrored, then the whole mesh needs to be scaled, "
               "as these triangles will change orientation");
      setIcon(QIcon(":/images/Scale.png"));

      addParm("X Scale", "Scale in X dimension", "1.0");
      addParm("Y Scale", "Scale in Y dimension", "1.0");
      addParm("Z Scale", "Scale in Z dimension", "1.0");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      mesh->updatePositions();

      return run(cs, indexAttr, parm("X Scale").toDouble(), parm("Y Scale").toDouble(), parm("Z Scale").toDouble());
    }
    bool run(CCStructure &cs, CCIndexDataAttr &indexAttr, double scaleX, double scaleY, double scaleZ);
  };
  
//  /**
//   * \class MeshDeleteValence ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Delete vertices whose valence is wihin a given range.
//   */
//  class mdxBase_EXPORT MeshDeleteValence : public Process 
//  {
//  public:
//    MeshDeleteValence(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh(), parms[0].toInt(), parms[1].toInt());
//    }
//  
//    bool run(Mesh* mesh, int startValence, int endValence);
//  
//      setName("Mesh/Structure/Delete Mesh Vertices by Valence");
//    QString description() const { return 
//      "Delete mesh vertices that have valence within the specified range");
//    QStringList parmNames() const { return QStringList() << "Start Valence" << "End Valence");
//    QStringList parmDescs() const { return QStringList() << "Start Valence" << "End Valence");
//    QStringList parmDefaults() const { return QStringList() << "0" << "2");
//      setIcon(QIcon(":/images/DeleteValence"));
//  };
//  
//  /**
//   * \class MergeVertices ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Merge selected vertices into one.
//   */
//  class MergeVertices : public Process 
//  {
//  public:
//    MergeVertices(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh());
//    }
//  
//    bool run(Mesh* mesh);
//  
//      setName("Mesh/Structure/Merge Vertices");
//      setDesc("Merge selected vertices into one.");
//    QStringList parmNames() const { return QStringList());
//    QStringList parmDescs() const { return QStringList());
//    QStringList parmDefaults() const { return QStringList());
//      setIcon(QIcon(":/images/MergeVertices"));
//  };
//
//  /**
//   * \class DeleteEdge ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Delete the edge between 2 selected vertices.
//   */
//  class DeleteEdge : public Process 
//  {
//  public:
//    DeleteEdge(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY))
//        return false;
//      return run(currentMesh());
//    }
//  
//    bool run(Mesh* mesh);
//  
//      setName("Mesh/Structure/Delete Edge");
//      setDesc("Delete edge between 2 selected vertices.");
//    QStringList parmNames() const { return QStringList());
//    QStringList parmDescs() const { return QStringList());
//    QStringList parmDefaults() const { return QStringList());
//      setIcon(QIcon(":/images/DeleteLabel.png"));
//  };
//  
//  /**
//   * \class MeshDeleteSelection ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Delete vertices selected in the current mesh, preserving cells.
//   */
//  class mdxBase_EXPORT MeshDeleteSelection : public Process 
//  {
//  public:
//    MeshDeleteSelection(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
//        return false;
//      return run(currentMesh());
//    }
//  
//    bool run(Mesh* m);
//  
//      setName("Mesh/Structure/Delete Selection");
//    QString description() const { return 
//      "Delete vertices selected in the current mesh, preserving cells.");
//    QStringList parmNames() const { return QStringList());
//    QStringList parmDescs() const { return QStringList());
//      setIcon(QIcon(":/images/DeleteLabel.png"));
//  };
//  
//  /**
//   * \class MeshKeepVertices ProcessStructure.hpp <MeshProcessStructure.hpp>
//   *
//   * Mark vertices so that they can survive as lines and points. Also prevents labels changing.
//   */
//  class mdxBase_EXPORT MeshKeepVertices : public Process 
//  {
//  public:
//    MeshKeepVertices(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
//        return false;
//      bool keep = stringToBool(parms[0]);
//      return run(currentMesh(), keep);
//    }
//  
//    bool run(Mesh* m, bool keep);
//     
//      setName("Mesh/Structure/Keep Vertices");
//    QString description() const { return 
//      "Mark vertices so that they can survive as lines and points. Also prevents "
//      "labels changing.");
//    QStringList parmNames() const { return QStringList() << "Keep");
//    QStringList parmDescs() const { return QStringList() << "Keep");
//    QStringList parmDefaults() const { return QStringList() << "Yes");
//    ParmChoiceMap parmChoice() const
//    {
//      ParmChoiceMap map;
//      map[0] = booleanChoice();
//      return map;
//    }
//      setIcon(QIcon(":/images/KeepVertices.png"));
//  };
  ///@}
}
#endif
