//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "Information.hpp"

#include <QMainWindow>
#include <QStatusBar>
#include <QThread>
#include <QDebug>
#include <QCoreApplication>

#include <iostream>
using namespace std;

#include <QTextStream>
#include <stdio.h>

Q_LOGGING_CATEGORY(mdxLogging,"mdx")
Q_LOGGING_CATEGORY(mdxStatusBarLog,"mdx.statusBar")

namespace mdx
{
  // RSS: Why is debugging defined here?
  bool DEBUG = false;

  namespace Information 
  {
    std::function<void(QString,bool)> setStatusImpl;

    // Handler for logging to terminal, should be thread safe 
    void msgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
    {
      if(strcmp(context.category,"default") == 0)
      {
        // the default message handler, from qlogging.cpp
        QString formatted = msg; //qFormatLogMessage(type,context,msg);
        fprintf(stderr, "%s\n", formatted.toLocal8Bit().constData());
        fflush(stderr);
      }
      else if(context.category == mdxLogging().categoryName())
      {
        QByteArray ba = msg.toLocal8Bit();
        const char *localMsg = ba.constData();

        switch (type) {
#if QT_VERSION < QT_VERSION_CHECK(5, 5, 0)
        case QtWarningMsg:
          fprintf(stderr, "%s", localMsg);
          break;
#else
        case QtInfoMsg:
          fprintf(stdout, "%s", localMsg);
          fflush(stdout);
          break;
        case QtWarningMsg:
          fprintf(stderr, "Warning: %s", localMsg);
          break;
#endif
        case QtCriticalMsg:
          fprintf(stderr, "Critical: (%s:%u, %s) %s", context.file, context.line, context.function, localMsg);
          break;
        case QtFatalMsg:
          fprintf(stderr, "Fatal: (%s:%u, %s) %s", context.file, context.line, context.function, localMsg);
          break;
        case QtDebugMsg:
          fprintf(stderr, "Debug: (%s:%u, %s) %s", context.file, context.line, context.function, localMsg);
          break;
        default:
          fprintf(stderr, "Unknown: (%s:%u, %s) %s", context.file, context.line, context.function, localMsg);
        }
      }
      else if(context.category == mdxStatusBarLog().categoryName())
      {
        setStatusImpl(msg,false);
      }
    } 

    void init(std::function<void(QString,bool)> _setStatus)
    {
      setStatusImpl = _setStatus;
    }

    void setStatus(const QString& text, bool alsoPrint)
    {
      setStatusImpl(text,alsoPrint);
    }
  }
}
