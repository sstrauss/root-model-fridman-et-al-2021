//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef IMAGE_DATA_H
#define IMAGE_DATA_H

#include <Config.hpp>

#include <ClipRegion.hpp>
#include <ColorBar.hpp>
#include <Colors.hpp>
#include <ColorMap.hpp>
#include <Geometry.hpp>
#include <Mesh.hpp>
#include <MDXViewer/qglviewer.h>
#include <Misc.hpp>
#include <Parms.hpp>
#include <Process.hpp>
#include <ScaleBar.hpp>
#include <Shader.hpp>
#include <TransferFunction.hpp>
#include <ColorEditDlg.hpp>
#include <string>
#include <string.h>

#include <CImg.h>

namespace mdx 
{
  class CutSurf;
  class TransferFunctionDlg;
  
  typedef Vector<3, GLubyte> Point3GLub;
  
  class mdx_EXPORT ImgData : public QObject
  {
    Q_OBJECT
  public:
    static const uint RELOAD_NONE = 0x0;
    static const uint RELOAD_MAIN = 0x1;
    static const uint RELOAD_WORK = 0x2;
    static const uint RELOAD_TRIS = 0x4;
    static const uint RELOAD_LINES = 0x8;
    static const uint RELOAD_POS = 0x10;
    static const uint RELOAD_VBO = 0x20;
    static const uint UPDATE_SELECTION = 0x40;
  
    int StackId;
  
    Stack* stack;
    Mesh* mesh;
  
    // Main and stack paramters
    bool Main16Bit, Work16Bit;
    bool MainClipVoxels, WorkClipVoxels;
    bool MainClipLabels, WorkClipLabels;
    bool MainShading, WorkShading;

    // General
    ColorEditDlg *triangleColorEditDlg;
    TransferFunctionDlg* workTransferDlg, *mainTransferDlg;
    // Function defining the color of the pixels
    std::vector<TransferFunction::Colorf> MainColorMap;   // Color map for the main data
    std::vector<TransferFunction::Colorf> WorkColorMap;   // Color map for the work data
    // If true, a new texture need to be sent for the colormap
    bool newMainColorMap, newWorkColorMap;
    std::vector<double> MainHist;   // Histogram of the main data
    std::vector<double> WorkHist;   // Histogram of the working data
    std::pair<double, double> mainBounds;
    std::pair<double, double> workBounds;

    AttrMap<QString, GLuint> dataTexIds;  // 3D texture ids for stacks

    GLuint dataTexColor;               // 3D texture color
    //GLuint surfTexId;                  // 1D texture map for surface colors.
    //GLuint heatTexId;                  // 1D texture map for heatmap colors.
    GLuint colorMapTexId;              // 1D texture map for colormaps
    GLuint imgTexId;                   // 2D texture map for surface from an image
    GLuint mcmapTexId, wcmapTexId;     // 1D texture map for volume colors.
    GLuint labelTexId;                 // 1D texture map to color indexed texture for labels
    AttrMap<QString, GLuint> labelCenterTexIds;    // Texture maps to hold centers for the labels
    AttrMap<QString, Point3usVec> labelCenters;    // Label centers
    AttrMap<QString, BBox3iVec> labelBBox;         // Label bounding boxes
    AttrMap<QString, bool> labelCentersChanged;

    GLuint selFboId;                   // ID of selection buffer;
    GLuint selFboColor;                // Texture for colors
    GLuint selFboDepth;                // Texture for depth
  
    Colors::ColorType MeshColor;
    Colors::ColorType MeshBorderColor;
    // Colors::ColorType MeshPointColor;
    Colors::ColorType MeshSelectColor;
  
    GLubyte* texMap;   // Texture map to memory
  
    // Parameters from stack file
    QString Section;
    std::vector<QString> ImageFiles;
  
    QWidget* parent;   // Parent widget
    double min, max;
  
    // Vectors to make heat maps
    double labelWallBordMin;
    double labelWallBordMax;
    QString sigStr;          // Description of heat map
    Point3u clipDo;          // Which clipping planes are selected
    Point4f pn[6];           // Clip planes tranformed into frame coordinates
    HVec4F Hpn;              // Host vector for planes for cuda
    Matrix4d frm;            // Frame matrix
    Matrix4d clm[3];         // Clip plane matrices
    double pixelRadius;       // Radius in frame coordinates of pixel edit tool
    BoundingBox3d bBox;      // Bounding box
    BoundingBox3i bBoxTex;   // Bounding box for texture update
  
    double meshShift;   // Shift for the mesh, relative to the scene radius
  
    // Variables to marching cubes (for eval)
    HVecUS* marchData;

    // Parameters shared by all instances
    static int ClearColor;
    static uint Slices;
    static uint TileCount;
    static Point3u MaxTexSize;
    static double SurfOffset;
  
    static double DrawNormals;
    static double DrawOffset;
    static double DrawZeroLabels;
    static double DrawNhbds;
    static bool DeleteBadVertex;
    static bool FillWorkData;
    static bool SeedStack;
    static int VoxelEditRadius;
    static int VoxelEditMaxPix;
    static ColorfVec LabelColors;
    bool LabelColorsChanged;

    static bool MeshSelect;
    static ScaleBar scaleBar;
    static Colorbar colorBar;
  
    static double MeshPointSize;
    static double MeshLineWidth;
  
    bool changed;         // Has the stack (mesh) changed
    bool pixelsChanged;   // Changed flag for pixel editing

    QTimer ccUpdateTimer;

  public:
    ImgData(int id, QWidget* _parent = 0);
  
    ~ImgData();
  
    void init(Stack* s, Mesh* m);
  
    // Set the internal format of the data
    void setMain16Bit(bool val);
    void setWork16Bit(bool val);

    // Set the clipping
    void setMainClipVoxels(bool val);
    void setWorkClipVoxels(bool val);
    void setMainClipLabels(bool val);
    void setWorkClipLabels(bool val);
  
    // Set parameters for function
    void readParms(Parms& parms, QString section);
  
    // Set parameters for function
    void writeParms(QTextStream& pout, QString section);
  
    // Get the frame with or without additional transform
    qglviewer::ManipulatedFrame& getFrame() { return stack->getFrame(); }
  
    // Get the main frame
    qglviewer::ManipulatedFrame& getMainFrame() { return stack->frame(); }
  
    // Get the transform frame
    qglviewer::ManipulatedFrame& getTransFrame() { return stack->trans(); }
  
    // Marching cubes evaluation function
    int eval(Point3d p);
  
    // Draw stack data
    void drawStack(Shader* shader);
  
    // Set the file containing the current color map
    void setColorMap(const QString& pth, bool work);
  
    // Update work histogram
    void updateWorkHistogram();
    // Update main histogram
    void updateMainHistogram();
  
    // Edit the transfer function
    void editMainTransferFunction();
    void editWorkTransferFunction();

    void updateWorkColorMap() 
    {
      updateColorMap(newWorkColorMap, WorkColorMap, stack->work()->transferFct());
    }
    void updateMainColorMap() 
    {
      updateColorMap(newMainColorMap, MainColorMap, stack->main()->transferFct());
    }
    void updateColorMap(bool& newColorMap, std::vector<TransferFunction::Colorf>& ColorMap,
                        const TransferFunction& transferFct);
  
    void setupMainColorMap() 
    {
      setupColorMap(newMainColorMap, mcmapTexId, MainColorMap, Shader::AT_CMAP_TEX);
    }
    void setupMainColorMap2() 
    {
      setupColorMap(newMainColorMap, mcmapTexId, MainColorMap, Shader::AT_SECOND_CMAP_TEX);
    }
    void setupWorkColorMap() 
    {
      setupColorMap(newWorkColorMap, wcmapTexId, WorkColorMap, Shader::AT_CMAP_TEX);
    }
    void setupWorkColorMap2() 
    {
      setupColorMap(newWorkColorMap, wcmapTexId, WorkColorMap, Shader::AT_SECOND_CMAP_TEX);
    }
    void setupLabelColorMap()
    {
      std::vector<TransferFunction::Colorf> empty;
      bool nonew = false;
      setupColorMap(nonew, labelTexId, empty, Shader::AT_LABEL_TEX);
    }
    void setupColorMap(bool& newColorMap, GLuint& cmapTexId, 
          const std::vector<TransferFunction::Colorf>& ColorMap, Shader::ActiveTextures activeTex);

    void setupLabelCenters(const QString &storeName);

    // Setup a 1D texture from a color map vector
    bool setupTexFromColorMap(GLuint &texId, const ColorbVec &colorVec);
  
    // Draw Bounding Box around the stack
    void drawBBox();
  
    // Setup the uniforms and textures to render 3D data
    void setup3DRenderingData(Shader* shader);

    // Change the volume shader depending on options
    void setupVolumeShader(Shader& shader, int pos, bool hasClip);
  
    // Draw one cell complex or all cell complexes
    void drawCCs(Shader *colorShader = 0);
    void drawCC(const QString&, bool select, Shader *colorShader = 0);
  
    // Setup data for clipping test
    void getClipTestData(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3);
  
    // Test if a point (in image coordinates) is inside clipping planes
    bool clipTest(const Point3i& ip);
  
    // Reset stack data
    void resetStack();
  
    // Reset mesh data
    void resetMesh();
  
    // Initialize controls
    void initControls(QWidget* viewer);
  
    // Texture management
    void initTex();

    void loadTex(const QString &storeName, const BoundingBox3i &bbox = BoundingBox3i());
    void unloadTex();
    void loadLabelCenterTex(const QString &storeName, int startLabel = 0, int endLabel = 0);
    void unloadLabelCenterTex(const QString &storeName);
    void loadLabelTex();
    void loadImgTex(const QImage& image);

    void bind2DTex(GLuint texId);
    void unbind2DTex();
    void bind3DTex(GLuint texId, Shader::ActiveTextures atexId = Shader::AT_TEX3D);
    void unbind3DTex();
  
    // Set parent label
    void setParent(int parent, const IntSet &labels);
  
    // Update the texture for height map (Keyence) data
    void updateImageTex2d();

    // Update the cell graph
    void updateAxis();

    // Start select framebuffer 
    bool startSelectFbo();
  
    // Stop select framebuffer 
    bool stopSelectFbo();
  
    // Stop select framebuffer 
    bool readSelectFbo();
  
    // Update Color and texture vertex array
    void updColorVBOs();

    // Find selected face in select mode)
    CCIndex findSelectFace(int x, int y);

    // Average a list of vertices to find select point
    bool findSeedPoint(uint x, uint y, CutSurf& cutSurf, Point3d& p);
  
    // Apply a single clip plane to the bounding box
    void bBoxClip(BoundingBox3d& bBox, Point3d p, Point3d n);
  
    // Make a bounding box from the clipping planes
    void bBoxFromClip();
  
    // Start pixel editing
    void voxelEditStart(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3);
  
    // Pixel editing operations
    // p, px, py and pz should be in world coordinates
    void voxelEdit(double pixelRadius, const Point3d& p, const Point3d& px, const Point3d& py, 
                                      const Point3d& pz, bool doCut, int currentLabel);
  
    // Stop pixel editing
    void voxelEditStop();
  
    bool isMainVisible() const 
    {
      return stack->main()->isVisible() and dataTexIds["Main"];
    }
    bool isWorkVisible() const 
    {
      return stack->work()->isVisible() and dataTexIds["Work"];
    }
    bool isVisible() const 
    {
      return isMainVisible() or isWorkVisible();
    }
  
    HVecUS& currentData()
    {
      if(stack->work()->isVisible())
        return stack->work()->data();
      return stack->main()->data();
    }
    const HVecUS& currentData() const
    {
      if(stack->work()->isVisible())
        return stack->work()->data();
      return stack->main()->data();
    }
    Point3d imageGradientW(Point3d worldpos);
    Point3d imageGradientI(Point3i ipos);
    uint imageLevel(Point3d worldpos);
    Point2i imageMinMax();
    bool fillLabel(const QString &storeName, int oldLabel, int newLabel);

    GLenum interpolation(GLuint texId)
    {
      if(texId == dataTexIds["Main"])
        return stack->main()->labels() ? GL_NEAREST : GL_LINEAR;
      else
        return stack->work()->labels() ? GL_NEAREST : GL_LINEAR;
    }
  
    GLenum internalFormat(GLuint texId)
    {
      // Labeled stack need to be 16 bit or they don't display the right colors
      if(texId == dataTexIds["Main"])
        return Main16Bit or stack->main()->labels() ? GL_ALPHA16 : GL_ALPHA8;
      else
        return Work16Bit or stack->work()->labels() ? GL_ALPHA16 : GL_ALPHA8;
    }
  
    GLenum swapTextureBytes(GLuint texId)
    {
      if(texId == dataTexIds["Main"])
        return stack->main()->labels();
      else
        return stack->work()->labels();
    }
  
    // Set stack sizes
    void updateStackSize();
  
    // Change the texture scale of the stack
    void setTexScale(double s);
  
    bool valid() const 
    {
      return stack->storeSize() > 0 and (dataTexIds["Main"] or dataTexIds["Work"]);
    }
  
  protected:
    // Load 3D texture data, possibly downsampling.
    //void load3DTexData(const GLuint texId, const Point3u size, const ushort* data);
  
    // Get step for texture decimation
    Point3u getTexStep();
  
  public:
    // Clip texture coordinates
    double trimTex(const double val) { return (trim(val, 0.0, 1.0)); }
  
    Point2f trimTex(const Point2f& val) { return (Point2f(trimTex(val.x()), trimTex(val.y()))); }
  
  protected:
  
    // Test if quad out of bounds, used for clipping 3D data
    bool testQuad(double a, double b, double c, double d, double x)
    {
      if(a > x and b > x and c > x and d > x)
        return (true);
      if(a < -x and b < -x and c < -x and d < -x)
        return (true);
      return (false);
    }
  
    // Compute offset for image data including border
    size_t offset(uint x, uint y, uint z) { return stack->offset(x, y, z); }
  
    size_t offset(Point3i ipos) { return stack->offset(ipos); }
  
    // Check if in bounds
    bool boundsOK(int x, int y, int z)
    {
      if(x < 0 or y < 0 or z < 0 or x >= int(stack->size().x()) or y >= int(stack->size().y())
         or z >= int(stack->size().z()))
        return (false);
      else
        return (true);
    }
  
  public:
    /// Go from image coordinates to world coordinates
    template <typename T> const Point3d imageToWorld(const Vector<3, T>& img) const
    {
      return stack->imageToWorld<T>(img);
    }
  
    /// Go from world coordinates to image coordinates
    template<typename T> Vector<3, T>worldToImage(Point3d wrld) const { return stack->worldToImage<T>(wrld); }
    Point3d worldToImaged(const Point3d& a) const { return worldToImage<double>(a); }
    Point3i worldToImagei(const Point3d& a) const { return worldToImage<int>(a); }
    Point3u worldToImageu(const Point3d& a) const { return worldToImage<uint>(a); }
  
    // Map scale slider values
    int toSliderScale(double s);
    double fromSliderScale(int i);
  
  protected:
    void updateHistogram(std::vector<double>& hist, const HVecUS& data, std::pair<double, double>& minMaxValues,
                         int max_data = 1 << 16, int size = 512);
    void invalidateHistogram(std::vector<double>& hist) { hist.clear(); }
  
    // Clear 3D stack texture
    void clearData(HVecUS& data)
    {
      data.resize(stack->storeSize());
      memset(&data[0], 0, data.size() * sizeof(ushort));
    }
  
  signals:
    void changedInterface();
    void changeSize(const Point3u& size, const Point3d& step, const Point3d& origin);
    void stackUnloaded();
    void updateCCData(int stack);
    void loadHeatChoices(int stack);
  
  public slots:
    // Slots for controls
    void mainShow(bool val);
    void mainBright(int val);
    void mainOpacity(int val);
    void mainLabels(bool val);
    void main16Bit(bool val);
    void mainClipVoxels(bool val);
    void mainClipLabels(bool val);
    void mainShading(bool val);
    void mainColorMap();
  
    void workShow(bool val);
    void workBright(int val);
    void workOpacity(int val);
    void workLabels(bool val);
    void work16Bit(bool val);
    void workClipVoxels(bool val);
    void workClipLabels(bool val);
    void workShading(bool val);
    void workColorMap();
  
    void surfBright(int val);
    void surfOpacity(int val);
    void surfBlend(bool val);
    void surfCull(bool val);
    void surfCellClipping(bool val);
    void surfShade(bool val);
    void surfLabeling(const QString &labeling);
    void surfSignal(const QString &labeling);
    void surfHeat(const QString &labeling);
  
    void ccUpdate(void);
    void updateViewer(void);
    void updateColorMap(void);
  
    void showTrans(bool val);
    void showScale(bool val);
    void showBBox(bool val);
    void tieScales(bool val);   // ALR
    void scaleX(int val);
    void scaleY(int val);
    void scaleZ(int val);
  
    // Slots to change transfer functions
    void setWorkColorMap(const TransferFunction& fct);
    void setMainColorMap(const TransferFunction& fct);
  
    // Slots to detect changes in transfer fct dialog boxes
    void updateWorkColorMap(const TransferFunction& fct);
    void updateMainColorMap(const TransferFunction& fct);

  signals:
    void viewerUpdate();
    void updateSliderScale();
    void forceSurfHeat();
    void toggleEditLabels();
  };
}
#endif
