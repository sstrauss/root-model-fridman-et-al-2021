//
// This is Jules Bloomenthal's implicit surface polygonizer from GRAPHICS GEMS IV.
// Converted to C++ by J. Andreas Berentzen 2003.
// Adapted for use in MorphoDynamX by Richard S. Smith 2010
//

//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 

#ifndef POLYGONIZER_H
#define POLYGONIZER_H

#include <Config.hpp>

#include <Geometry.hpp>
#include <Progress.hpp>

#include <set>
#include <vector>

namespace mdx 
{
  //
  // The implicit function class represents the implicit function we wish
  // to polygonize. Derive a class from this one and return true if the point
  // is inside the object at the point passed in.
  //
  class mdx_EXPORT ImplicitFunction {
  public:
    virtual bool eval(Point3d) = 0;
  };
  
  
  // Polygonizer is the class used to perform polygonization.
  class mdx_EXPORT Polygonizer {
    class Process;
    Process* process;
  
  public:
    // Constructor of Polygonizer.
    // Arguments:
    // 1. The ImplicitFunction defining the surface(s)
    // 2. The cube size to scan the space.
    // 3. The size of the voxels.
    // 4. Standard vector to put vertices.
    // 5. Standard vector to put triangles.
    Polygonizer(ImplicitFunction& _func, double _cubeSize, Point3d _voxSize, std::vector<Point3d>& vertices,
                std::vector<Point3u>& triangles);
    ~Polygonizer();
  
    // March adds to the vertex and triangle lists.
    // Arguments:
    // 1. Bounding box of the space to explore.
    void march(Point3d* bBox);
  };
}
#endif
