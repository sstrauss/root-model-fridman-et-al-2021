#ifndef MDX_CONFIG_HPP
#define MDX_CONFIG_HPP

#if (defined(WIN64) || defined(_WIN32)) && !defined(WIN32)
  #define WIN32
#endif

#if defined(WIN32) || defined(WIN64)

  #ifdef mdx_EXPORTS
    #define mdx_EXPORT __declspec(dllexport)
  #else
    #define mdx_EXPORT __declspec(dllimport)
  #endif

  #ifdef mdxBase_EXPORTS
    #define mdxBase_EXPORT __declspec(dllexport)
  #else
    #define mdxBase_EXPORT __declspec(dllimport)
  #endif

  #ifdef mdxPython_EXPORTS
    #define mdxPython_EXPORT __declspec(dllexport)
  #else
    #define mdxPython_EXPORT __declspec(dllimport)
  #endif

  #define WINDOWS
  #include <windows.h>
  #undef max
  #undef min
  #ifdef _MSC_VER
    #define xor ^
    #define and &&
    #define or ||
    #define not !
    #define finite _finite
    #define round _round
  #else
    #define finite isfinite
  #endif

  #include <cmath>
  #include <float.h>
#else
  #define cuda_EXPORT
  #define mdx_EXPORT
  #define mdxBase_EXPORT
  #define mdxPython_EXPORT
#endif

// Backend to use for operations that should be parallelized but not on GPU
#cmakedefine USE_TBB
#cmakedefine USE_OPENMP

#define THRUST_DEVICE_SYSTEM THRUST_DEVICE_SYSTEM_@THRUST_DEVICE_SYSTEM@
#cmakedefine THRUST_BACKEND_CUDA
#cmakedefine THRUST_BACKEND_OMP
#cmakedefine THRUST_BACKEND_TBB

#cmakedefine MDX_USE_TR1

#cmakedefine MDXPROCPATH "@MDXPROCPATH@"
#cmakedefine MDXPROCINCLPATH "@MDXPROCINCLPATH@"
#cmakedefine MDXDOCPATH "@MDXDOCPATH@"
#cmakedefine MDXRESPATH "@MDXRESPATH@"
#cmakedefine MDXLIBPATH "@MDXLIBPATH@"

#endif

