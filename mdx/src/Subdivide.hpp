//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef SUBDIVIDE_HPP
#define SUBDIVIDE_HPP

/**
 * \file Subdivide.hpp
 *
 * This file contains an object to inherit from when calling routines that do subdivision
 */
#include <Config.hpp>
#include <CCF.hpp>

namespace mdx 
{
  typedef CCStructure::Dimension Dimension;

  /* Subclasses of Subdivide provide callback functions which update the
   * attributes when cells in the complex are divided by a division routine. */
  class Subdivide
  {
  public:
    // Update the cell data
    virtual void splitCellUpdate(Dimension, const CCStructure &, const CCStructure::SplitStruct &,
	CCIndex /* otherP */ = CCIndex(), CCIndex /* otherN */ = CCIndex(), double /* interpPos */ = 0.5) {}
  };

  inline CCIndexPair getEdgeOtherPN(const CCStructure &cs, const CCStructure::SplitStruct &ss)
  {
    CCIndexPair pn(CCIndex::UNDEF, CCIndex::UNDEF);

    if(!cs.hasCell(ss.childP)) { // Need to check since dimensionOf can throw
      mdxInfo << "getEdgeOtherPN ChildP not in cell complex" << endl;
      return pn;
    }
    if(!cs.hasCell(ss.childN)) { // Need to check since dimensionOf can throw
      mdxInfo << "getEdgeOtherPN ChildN not in cell complex" << endl;
      return pn;
    }

    if(cs.dimensionOf(ss.childP) != 1) {
      mdxInfo << QString("getEdgeOtherPN ChildP has wrong dimension (%1)").arg(cs.dimensionOf(ss.childP));
      return pn;
    }
    if(cs.dimensionOf(ss.childN) != 1) {
      mdxInfo << QString("getEdgeOtherPN ChildN has wrong dimension (%1)").arg(cs.dimensionOf(ss.childN));
      return pn;
    }

    auto eb = cs.edgeBounds(ss.childP);
    if(eb.first == ss.membrane)
      pn.first = eb.second;
    else if(eb.second == ss.membrane)
      pn.first = eb.first;
    else {
      mdxInfo << "getEdgeOtherPN ChildP other vertex not found" << endl;
      return pn;
    }

    eb = cs.edgeBounds(ss.childN);
    if(eb.first == ss.membrane)
      pn.second = eb.second;
    else if(eb.second == ss.membrane)
      pn.second = eb.first;
    else {
      mdxInfo << "getEdgeOtherPN ChildN other vertex not found" << endl;
      pn.first = CCIndex::UNDEF;
      return pn;
    }
    return pn;
  }
}

#endif
