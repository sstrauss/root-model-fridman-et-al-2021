//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2018 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <CCF.hpp>

#include <ProcessParms.hpp>
#include <Attributes.hpp>

#include <DistObject.hpp>
#include <DistMatrix.hpp>
#include <KrylovMethods.hpp>

#include <Clamp.hpp>

namespace mdx
{
  template<typename _VectorT> class SolverDerivs;

  /**
   * \class SolverParms Solver.hpp
   *
   * Main solver class
   */
  class SolverParms : virtual public ProcessParms
  {
  public:
    SolverParms()
    {
      setName("Solver");
      setDesc("Solve the system");

      addParm("Step", "Total time to advance", "10");
      addParm("Dt", "Timestep of the simulation for Euler, starting step for Adaptive methods\n"
              "Set to 0 for direct solve with Backward Euler", "0.01");
      addParm("Min Dt", "Minimum timestep for adaptive solvers", ".0001");
      addParm("Max Dt", "Maximum timestep for adaptive solvers", "10");
      addParm("Max Norm", "Max error norm for adaptive time stepping", "10");
      addParm("Max Steps", "Maximum number of time steps, even if Step is not reached", "10");
      addParm("Max Newton Step", "Maximal fraction of Dx avancement from total Dx computed in Newton iteration", "0.1");
      addParm("Method", "Method of integration", "Euler",
              QStringList() << "Euler" << "Adaptive Euler" << "Backward Euler" << "BackwardEuler-Newton" << "Newton"
                            << "Trapezoidal" << "Diffusion Direct");
      addParm("Solver Type", "Type of linear solver", "Bi-conjugate Gradient Stabilized",
              QStringList() << "Conjugate Gradient" << "Preconditioned Conjugate Gradient"
                            << "Bi-conjugate Gradient Stabilized" << "Preconditioned Bi-conjugate Gradient Stabilized");
      addParm("CG Max Iter", "Maximum iterations for conjugate gradient", "50");
      addParm("CG Tolerance", "Conjugate gradient tolerance", "1e-6");
      addParm("Print Stats", "Print statistics about solver", "Yes", booleanChoice());
    }
  };

  template<typename _VectorT>
  class Solver : public SolverParms
  {
  public:
    typedef _VectorT VectorT;
    typedef typename VectorT::value_type ScalarT;
    static const int dim = VectorT::numElems;
    typedef Matrix<dim,dim,ScalarT> MatrixT;
    typedef Vector<dim,uint> VectorUIntT;

    // Vector of processes that calculate the derivatives
    typedef SolverDerivs<VectorT> Derivs;

    // Structures for Backward Euler solver
    typedef DistNhbd<CCStructure> DNhbd;
    typedef DVector<DNhbd,MatrixT,VectorT,ScalarT> DVectorT;
    typedef DMatrix<DNhbd,MatrixT,VectorT,ScalarT> DMatrixT;

    // Data for solving attached to vertices and edges
    struct SolverVertexData
    {
      VectorT x, dx, cx, dirx;  // x, derivatives, correction/b-vec, Dirichlet amounts
      VectorUIntT dirichlet;    // Fixed degrees of freedom
      MatrixT j;                // Jacobian diagonal
    };
    typedef AttrMap<DNhbd::Vertex, SolverVertexData> VertexAttr;

    struct SolverEdgeData
    {
      MatrixT j;                // Off diagonal Jacobian entries (neighbors)
    };
    typedef AttrMap<DNhbd::Edge, SolverEdgeData> EdgeAttr;

    enum MethodEnum { Euler, AdaptiveEuler, BackwardEuler, BackwardNewton, Newton, DiffusionDirect, Trapezoidal };
    static MethodEnum stringToMethod(const QString &str)
    {
      if(str == "Euler")
        return(Euler);
      else if(str == "Adaptive Euler")
        return(AdaptiveEuler);
      else if(str == "Backward Euler")
        return(BackwardEuler);
      else if(str == "BackwardEuler-Newton")
        return(BackwardNewton);
      else if(str == "Newton")
        return(Newton);
      else if(str == "Diffusion Direct")
        return(DiffusionDirect);
      else if(str == "Trapezoidal")
        return(Trapezoidal);
      else 
        throw(QString("Solver::stringToMethod Bad solver method %1").arg(str));
    }

    enum SolverTypeEnum { CGSolver, CGPreCondSolver, BiCGStabSolver, BiCGStabPreCondSolver };
    static SolverTypeEnum stringToSolverType(const QString &str)
    {
      if(str == "Conjugate Gradient")
        return(CGSolver);
      else if(str == "Preconditioned Conjugate Gradient")
        return(CGPreCondSolver);
      else if(str == "Bi-conjugate Gradient Stabilized")
        return(BiCGStabSolver);
      else if(str == "Preconditioned Bi-conjugate Gradient Stabilized")
        return(BiCGStabPreCondSolver);
      else 
        throw(QString("Solver::stringToSolverType Bad solver type %1").arg(str));
    }

  public:
    // Solver graph
    CCStructure sg;

    // Distributed objects for backwards Euler
    DNhbd *nhbd = 0;

    VertexAttr vertexAttr;
    EdgeAttr edgeAttr;

    DistVertexAttr<DNhbd,SolverVertexData,MatrixT> *vJ = 0;
    DistEdgeAttr<DNhbd,SolverEdgeData,MatrixT> *eJ = 0;
    DMatrixT *J = 0;

    DistVertexAttr<DNhbd,SolverVertexData,VectorT> *vX = 0, *vB = 0;
    DVectorT *X = 0, *B = 0;

    // Derivative providers
    std::vector<Derivs*> derivs;

    // Parameters
    double Step = 0;
    double Dt = 0, MinDt = 0, MaxDt = 0;
    double MaxNorm = 0;
    uint MaxSteps = 0;
    double MaxNewtStep = 0;

    MethodEnum Method = Euler;
    SolverTypeEnum SolverType = CGSolver;
     
    uint CGMaxIter = 100;
    double CGTolerance = 1e-6;

    // Boundary conditions
    bool hasDirichlet = false;

    bool PrintStats = true;

    // We can only modify the topology (in the addEdge callback) during initialize()
    bool modifyingTopology = false;

  public:
    Solver() {}
    
    virtual ~Solver()
    {
      deallocateDistObjects();
    }

    const CCStructure &graph(void) const { return sg; }

    void allocateDistObjects(void)
    {
      deallocateDistObjects();

      nhbd = new DNhbd(sg);

      vJ = new DistVertexAttr<DNhbd,SolverVertexData,MatrixT>(*nhbd, &vertexAttr, &SolverVertexData::j);
      eJ = new DistEdgeAttr  <DNhbd,SolverEdgeData,MatrixT>(*nhbd, &edgeAttr, &SolverEdgeData::j);
      J = new DMatrixT(*vJ, *eJ);
  
      vX = new DistVertexAttr<DNhbd,SolverVertexData,VectorT>(*nhbd, &vertexAttr, &SolverVertexData::cx);
      vB = new DistVertexAttr<DNhbd,SolverVertexData,VectorT>(*nhbd, &vertexAttr, &SolverVertexData::dx);

      X  = new DVectorT(*vX);
      B  = new DVectorT(*vB);
    }

    void deallocateDistObjects(void)
    {
      if(nhbd) { delete nhbd; nhbd = 0; }

      if(vJ) { delete vJ; vJ = 0; }
      if(eJ) { delete eJ; eJ = 0; }
      if(J) { delete J; J = 0; }

      if(vX) { delete vX; vX = 0; }
      if(vB) { delete vB; vB = 0; }
      if(X) { delete X; X = 0; }
      if(B) { delete B; B = 0; }
    }

    void processParms()
    {
      Step = parm("Step").toDouble();
      Dt = parm("Dt").toDouble();
      MinDt = parm("Min Dt").toDouble();
      MaxDt = parm("Max Dt").toDouble();
      MaxNorm = parm("Max Norm").toDouble();
      MaxSteps = parm("Max Steps").toUInt();
      MaxNewtStep = parm("Max Newton Step").toDouble();
      Method = stringToMethod(parm("Method"));
      SolverType = stringToSolverType(parm("Solver Type"));
      CGMaxIter = parm("CG Max Iter").toUInt();
      CGTolerance = parm("CG Tolerance").toDouble();
      PrintStats = stringToBool(parm("Print Stats"));
    }

    /// Clear the derivatives providers
    void clearDerivs(void)
    {
      derivs.clear();
    }

    /// Add a derivatives provider to the list
    void addDerivs(Derivs *d)
    {
      derivs.push_back(d);
    }

    /// Initialize the solver
    void initSolver(const CCStructure *templateCC = 0)
    {
      processParms();
      sg = CCStructure(2);
      ccf::CCStructure &sgX = ensureCCF(sg);

      // Just copy the vertices, edges, and 0-flips.
      if(templateCC != 0) {
        std::vector< std::vector<CCIndex> > cbd(2);
        cbd[0] = templateCC->vertices();
        cbd[1] = templateCC->edges();
        sgX.dimensionMap.load(cbd);

        std::vector<CCStructure::FlipI> fs =
          templateCC->matchV(CCIndex::BOTTOM,CCIndex::Q,CCIndex::Q,CCIndex::Q);
        sgX.flips.insert(fs.begin(),fs.end());
      }

      // Initialize derivative providers, extra edges must be added here
      vertexAttr.clear();
      edgeAttr.clear();
      modifyingTopology = true;
      for(Derivs *d : derivs) {
        d->processParms();
        d->initDerivs(*this, vertexAttr, edgeAttr);
      }
      setBoundaryFlags();
      modifyingTopology = false;

      // Create distributed objects if necessary
      if(Method == BackwardEuler or Method == BackwardNewton or Method == Newton or Method == DiffusionDirect or Method == Trapezoidal)
        allocateDistObjects();
    }

    void finalizeSolver()
    {
      deallocateDistObjects();
    }

    void addVertex(CCIndex v)
    {
      // Ensure we're not changing the topology in midstream.
      // (We might have an active DNbhd.)
      if(!modifyingTopology)
        throw(QString("Solver::addVertex called outside of SolverDerivs::initDerivs"));

      if(!sg.hasCell(v)) 
        sg.addCell(v);
    }

    void addEdge(CCIndex vFrom, CCIndex vTo)
    {
      // Ensure we're not changing the topology in midstream.
      // (We might have an active DNbhd.)
      if(!modifyingTopology)
        throw(QString("Solver::addEdge called outside of SolverDerivs::initDerivs"));

      // check if the edge is already in the graph
      CCStructure::FlipI eflip = sg.matchFirst(CCIndex::BOTTOM,vFrom,vTo,CCIndex::Q);
      if(!eflip.isValid()) {
        // Add vertices if they're not here yet
        if(!sg.hasCell(vFrom)) sg.addCell(vFrom);
        if(!sg.hasCell(vTo)) sg.addCell(vTo);
        // Add the edge with a new index
        CCIndex e = CCIndexFactory.getIndex();
        if(!sg.addCell(e , +vFrom -vTo))
          throw(QString("Solver::addEdge: Could not add edge in solver graph from %1 to %2")
            .arg(ccf::to_string(vFrom).c_str()).arg(ccf::to_string(vFrom).c_str()));
      }
    }

    double solve(void)
    {
      // Do nothing if Dt is zero
      if(Dt <= 0 or Step <= 0)
        return 0;

      switch(Method) {
        case Euler:
          return solveEuler();
        case AdaptiveEuler:
          return solveAdaptiveEuler(); 
        case Newton:
        case BackwardEuler:
          return solveBackwardEuler();
        case BackwardNewton:
          return solveBackwardNewton();
        case DiffusionDirect:
          return DirectSolver(); 
        case Trapezoidal:
          return solveTrapezoidal();
        default:
          throw(QString("Solver::solve Invalid solver method %1").arg(Method));
      }
    }

    double solveEuler()
    {
      const CCIndexVec &vertices = sg.vertices();
      uint stepCount = 0;
      double elapsed = 0, thisDt = 0;
      while(elapsed < Step and stepCount < MaxSteps) {
        thisDt = Dt;
        if(elapsed + thisDt > Step) {
          thisDt = Step - elapsed;
        }

        // Fist clear derivatives
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          SolverVertexData &vSD = vertexAttr[vertices[i]];
          vSD.dx.zero();
        }

        // Get the values and calculate the derivatives
        for(Derivs *d : derivs) {
          d->getValues(*this, vertexAttr); // FIXME how to avoid dups?
          d->calcDerivatives(*this, vertexAttr);
        }
        calcDirichletDeriv();

        // Update values with single step of Euler
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          SolverVertexData &vSD = vertexAttr[vertices[i]];
          vSD.x += thisDt * vSD.dx;
        }

        elapsed += thisDt;
        stepCount++;

        // Write back to the derivative providers, FIXME how to avoid dups?
        for(Derivs *d : derivs)
          d->setValues(*this, vertexAttr);
      }
      if(PrintStats) {
        mdxInfo << "Solver steps: " << stepCount << " ";
        mdxInfo << "Elapsed t: " << elapsed << " ";
        mdxInfo << "Norm:" << calcDXNorm() << endl;
      }
      return elapsed;
    }

    double solveAdaptiveEuler(void)
    {
      double elapsed = 0, err = 0, thisDt = 0;
      uint steps = 0;
      bool truncatedDt = false;
      const CCIndexVec &vertices = sg.vertices();
      VertexAttr k1(vertexAttr), k2(vertexAttr);
      while(elapsed < Step && steps < MaxSteps) {
        thisDt = Dt;
        if(elapsed + thisDt > Step) {
          thisDt = Step - elapsed;
          truncatedDt = true;
        }

  // Clear derivatives in k1 and k2
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
    k1[vertices[i]].dx.zero();
    k2[vertices[i]].dx.zero();
        }

        // 1. store initial x and find k1 = f(x0)
        for(Derivs *d : derivs) {
          d->getValues(*this,k1);
          d->calcDerivatives(*this,k1);
        }
  calcDirichletDeriv(k1);

        // 2. compute values at ~y = y + k1 dt and find k2 = f(~y)
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          CCIndex v = vertices[i];
          SolverVertexData &k1VD = k1[v];
          k2[v].x = k1VD.x + thisDt * k1VD.dx;
        }
        for(Derivs *d : derivs) {
          d->setValues(*this,k2);
          d->calcDerivatives(*this,k2);
        }
  calcDirichletDeriv(k2);

        // 3. compute new values y += (dt/2) (k1 + k2) and estimate error
        err = 0.0;
        #pragma omp parallel for reduction(+:err)
        for(uint i = 0; i < vertices.size(); i++) {
          CCIndex v = vertices[i];
          SolverVertexData &k1VD = k1[v], &k2VD = k2[v];
          VectorT newVal = k1VD.x + 0.5 * thisDt * (k1VD.dx + k2VD.dx);
          k2VD.x = newVal;   // store new values temporarily in k2

          VectorT errVec = 0.5 * thisDt * (k2VD.dx - k1VD.dx);
          err += norm(errVec);
        }
        for(Derivs *d : derivs)
        {
          d->setValues(*this,k2);
        }

        // 4. change Dt
        elapsed += thisDt;
        steps++;
        if(!truncatedDt) {
          Dt *= 0.9 * clamp(MaxNorm / err ,1./3,2.0);
          Dt = clamp(Dt, MinDt, MaxDt);
        }
      }
      if(PrintStats) {
        mdxInfo << "Solver steps: " << steps << " ";
        mdxInfo << "Elapsed t: " << elapsed << " ";
        mdxInfo << "Final Dt: " << Dt << " ";
        if(truncatedDt) mdxInfo << "(truncated to " << thisDt << ") ";
        mdxInfo << "Est. error:" << err << endl;
      }
      return elapsed;
    }

    /// Solve backward Euler or Newton
    double solveBackwardEuler(void)
    {
      // Write neighborhoods to thrust
      nhbd->write();
      J->alloc();
      X->alloc();
      B->alloc();
      const CCIndexVec &vertices = sg.vertices();
      uint sumIter = 0, stepCount = 0;
      double elapsed = 0, xNorm = 0, thisDt = 0;
      bool truncatedDt = false, hitMinDt = false;
      while(elapsed < Step and stepCount < MaxSteps) {
        if(!progressAdvance())
          break;
        thisDt = Dt;
        if(elapsed + thisDt > Step) {
          thisDt = Step - elapsed;
          truncatedDt = true;
        }
        // Fist clear Jacobian, derivatives and x
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          CCIndex v = vertices[i];
          SolverVertexData &vSD = vertexAttr[v];
          vSD.dx.zero();
          vSD.cx.zero();
          vSD.j.zero();
          for(CCIndex n : sg.neighbors(v))
            edgeAttr[sg.orientedEdge(v, n)].j.zero();
        }
        // Call providers to add their parts to Derivatives and Jacobian
        for(Derivs *d : derivs) {
          d->getValues(*this, vertexAttr);
          d->calcDerivatives(*this, vertexAttr);
          d->calcJacobian(*this, vertexAttr, edgeAttr);
        }
        calcDirichletJac();
        // Put data on GPU
        B->write();
        J->write();
 
        // Adjust for backward Euler
        if(Method == BackwardEuler) {
          *B *= thisDt;
          *J *= -thisDt;
          J->addToDiagonal(1.0);
        } else if(Method == Newton)
          *B *= -1.0;
        else 
          throw QString("Invalid solver method: %1").arg(Method);
        bool stepValid = false;
        double prevNorm = MaxNorm;
        int iters = 0;
        while(!stepValid) {
          if(!progressAdvance())
            break;
          // Clear X
          *X = 0;
          
          // Solve the system
          switch(SolverType) {
            case CGSolver:
              iters = CG(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
            case CGPreCondSolver:
              iters = CGPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0); 
              break;
            case BiCGStabSolver:
              iters = BiCGStab(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
            case BiCGStabPreCondSolver:
              iters = BiCGStabPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
          }
   
          // Read back the x correction
          X->read(); 
          // Get the norm
          xNorm = calcXNorm();
          // Converge or repeat based on xNorm
          if(xNorm < 2 * MaxNorm) {
            stepValid = true;
            // If xNorm is small enough, increase step size for next step
            if(!truncatedDt and xNorm < MaxNorm and xNorm < prevNorm)  {
              prevNorm = xNorm;
              Dt *= 1.1;
              if(Dt > MaxDt)
                Dt = MaxDt;
            }
          } else if(thisDt > MinDt) {
            // thisDt is too big, so we can make a smaller stepsize
            Dt = thisDt;
            truncatedDt = false;
            // If we can reduce Dt further, do so
            double oldDt = Dt;
            Dt *= 0.95;
            if(Dt < MinDt)
              Dt = MinDt;
            thisDt = Dt;
            // Adjust J and B with new Dt if required
            if(Method == BackwardEuler) {
              J->addToDiagonal(-1.0);
              *B *= Dt / oldDt;
              *J *= Dt / oldDt;
              J->addToDiagonal(1.0);
            }
          } else {
            // If we can't reduce Dt, we must move on
            stepValid = true;
            truncatedDt = false;
	    hitMinDt = true;
            break;
          }
        }
        if(stepValid) {
          // Sum CG iterations
          sumIter += iters;
          // Get values from derivative providers, FIXME how to avoid dups?
          for(Derivs *d : derivs)
            d->getValues(*this, vertexAttr);
          // Update values
          #pragma omp parallel for
          for(uint i = 0; i < vertices.size(); i++) {
            SolverVertexData &vSD = vertexAttr[vertices[i]];
            if(hasDirichlet)
              for(uint i = 0; i < dim; i++)
                if(vSD.dirichlet[i] > 0)
                  if(vSD.cx[i] != 0) {
                    // This should not happen, but best to check anyway
                    mdxInfo << QString("Solver::solveBackwardEuler Displacement found for Dirichlet (%1)").arg(vSD.cx[i]) << endl; 
                    vSD.cx[i] = 0;
                  }
            vSD.x += vSD.cx;
          }
          // Write back to the derivative providers, FIXME how to avoid dups?
          for(Derivs *d : derivs)
            d->setValues(*this, vertexAttr);
          // Update step progress
          elapsed += thisDt;
          stepCount++;
        } else
          break; // If step wasn't valid user must have cancelled
      }
      if(PrintStats and stepCount > 0) {
        if(Method == Newton) {
          mdxInfo << "Solver Steps:" << stepCount << " ";
          mdxInfo << "CG Avg. Iter:" << sumIter/stepCount << " ";
          mdxInfo << "Norm:" << xNorm << endl;
        } else { // BackwardEuler
          mdxInfo << "Solver Steps:" << stepCount << " ";
          mdxInfo << "Elapsed t:" << elapsed << " ";
          mdxInfo << "CG Avg. Iter:" << sumIter/stepCount << " ";
          mdxInfo << "Final Dt:" << Dt << " ";
          if(truncatedDt) 
            mdxInfo << "(truncated to " << thisDt << ") ";
          mdxInfo << "Norm:" << xNorm << endl;
        }
	if(hitMinDt)
	  mdxWarning << "Error estimate exceeded MaxNorm even at MinDt. "
		     << "Try lowering MinDt." << endl;
      }
      return elapsed; 
    }
    
    /*double solveBackwardNewton(void)
    {
      // Write neighborhoods to thrust
      nhbd->write();
      J->alloc();
      X->alloc();
      B->alloc();
      //mdxInfo << "stuff has been allocated" << endl;
      const CCIndexVec &vertices = sg.vertices();
      uint sumIter = 0, stepCount = 0;
      double elapsed = 0, xNorm = 0, dxNorm = 0, thisDt = 0;
      bool truncatedDt = false;
      while(elapsed < Step and stepCount < MaxSteps) {
        if(!progressAdvance())
          break;
        thisDt = Dt;
        if(elapsed + thisDt > Step) {
          thisDt = Step - elapsed;
          truncatedDt = true;
        }
        // Fist clear Jacobian, derivatives and x
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          CCIndex v = vertices[i];
          SolverVertexData &vSD = vertexAttr[v];
          vSD.dx.zero();
          vSD.cx.zero();
          vSD.j.zero();
          for(CCIndex n : sg.neighbors(v))
            edgeAttr[sg.orientedEdge(v, n)].j.zero();
        }
        // Call providers to add their parts to Derivatives and Jacobian
        for(Derivs *d : derivs) {
          d->getValues(*this, vertexAttr);
          d->calcDerivatives(*this, vertexAttr);
          d->calcJacobian(*this, vertexAttr, edgeAttr);
        }
        calcDirichletJac();
        // Put data on GPU
        B->write();
        J->write();
 
        bool doneNewton = false;
        // Adjust for backward Euler
        if(thisDt < MaxDt) {
          //mdxInfo << "I am in the Backward Euler situation" << endl;
          // *B *= thisDt;
          *J *= -1;
          J->addToDiagonal(1.0/thisDt);
          mdxInfo << "I am in the backward Euler situation" << endl;
        } else{
          *B *= -1.0;
          doneNewton = true;
          mdxInfo << "I am in the Newton situation" << endl;
        }
        bool stepValid = false;
        double prevNorm = MaxNorm;
        int iters = 0;
        while(!stepValid) {
          if(!progressAdvance())
            break;
          // Clear X
          *X = 0;
          
          // Solve the system
          switch(SolverType) {
            case CGSolver:
              iters = CG(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
            case CGPreCondSolver:
              iters = CGPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0); 
              break;
            case BiCGStabSolver:
              iters = BiCGStab(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
            case BiCGStabPreCondSolver:
              iters = BiCGStabPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
          }
   
          // Read back the x correction
          X->read(); 
          // Get the norm
          //mdxInfo << "Just before calcDXNorm " << endl;
          xNorm = calcXNorm();
          // Converge or repeat based on xNorm
          if(xNorm < 2 * MaxNorm) {
            stepValid = true;
            mdxInfo << "the step is valid " << endl;
            // If xNorm is small enough, increase step size for next step
            if(!truncatedDt and xNorm < MaxNorm and xNorm < prevNorm)  {
              //dilute a bit to allow recovery
              prevNorm = xNorm;
              Dt *= 1.1;
              if(Dt > MaxDt)
                Dt = MaxDt;
            } 
            else if(thisDt > MinDt and xNorm > (prevNorm * 1.1) and doneNewton) {
              // thisDt is too big, so we can make a smaller stepsize
              Dt = thisDt;
              truncatedDt = false;
              // If we can reduce Dt further, do so
              double oldDt = Dt;
              Dt *= 0.95;
              if(Dt < MinDt)
                Dt = MinDt;
              thisDt = Dt;
            }
            //diluite a bit to allow recovery 
            prevNorm = 0.95 * prevNorm + 0.05 * xNorm;
          }
          else if (thisDt > MinDt)
          { 
            Dt = thisDt;
            truncatedDt = false;
            // If we can reduce Dt further, do so
            double oldDt = Dt;
            Dt *= 0.5;
            if(Dt < MinDt)
              Dt = MinDt;
            thisDt = Dt;

            if (!doneNewton){
              J->addToDiagonal(-1.0 * oldDt) ;
              J->addToDiagonal(1.0/Dt);
            }
            //if Newton xNor was not < prevNorm, go back to Bkward Euler
            else{
              //doneNewton == false;
              *J *= -1;
              J->addToDiagonal(1.0/Dt);
            }
          }
          // If we can't reduce Dt, we must move on
          else
            break;

        }
        if(stepValid) {
          // Sum CG iterations
          sumIter += iters;
          // Get values from derivative providers, FIXME how to avoid dups?
          for(Derivs *d : derivs)
            d->getValues(*this, vertexAttr);
          // Update values
          #pragma omp parallel for
          for(uint i = 0; i < vertices.size(); i++) {
            SolverVertexData &vSD = vertexAttr[vertices[i]];
            if(hasDirichlet)
              for(uint i = 0; i < dim; i++)
                if(vSD.dirichlet[i] > 0)
                  if(vSD.cx[i] != 0) {
                    // This should not happen, but best to check anyway
                    mdxInfo << QString("Solver::solveBackwardEuler Displacement found for Dirichlet (%1)").arg(vSD.cx[i]) << endl; 
                    vSD.cx[i] = 0;
                  }
            if (doneNewton)
              vSD.x  += vSD.cx * MaxNewtStep;
            else
              vSD.x += vSD.cx;
          }
          // Write back to the derivative providers, FIXME how to avoid dups?
          for(Derivs *d : derivs)
            d->setValues(*this, vertexAttr);
          // Update step progress
          elapsed += thisDt;
          stepCount++;
        } else
          break; // If step wasn't valid user must have cancelled
      }
      if(PrintStats and stepCount > 0) {
        if(thisDt < MaxDt) {
          mdxInfo << "Solver Steps:" << stepCount << " ";
          mdxInfo << "Elapsed t:" << elapsed << " ";
          mdxInfo << "CG Avg. Iter:" << sumIter/stepCount << " ";
          mdxInfo << "Final Dt:" << Dt << " ";
          if(truncatedDt) mdxInfo << "(truncated to " << thisDt << ") ";
          mdxInfo << "XNorm:" << xNorm << endl;
        } else { 
          mdxInfo << "Solver Steps:" << stepCount << " ";
          mdxInfo << "CG Avg. Iter:" << sumIter/stepCount << " ";
          mdxInfo << "XNorm:" << xNorm << endl;
        }
      }
      return elapsed; 
    }*/

    double solveBackwardNewton(void)
    {
      // Write neighborhoods to thrust
      nhbd->write();
      J->alloc();
      X->alloc();
      B->alloc();
      //mdxInfo << "stuff has been allocated" << endl;
      const CCIndexVec &vertices = sg.vertices();
      uint sumIter = 0, stepCount = 0;
      double xNorm = 0, dxNorm = 0, thisDt = 0;
      if (MaxDt > 1.)
        MaxDt = 1.;

      while(stepCount < MaxSteps) {
        if(!progressAdvance())
          break;
        thisDt = Dt;
        // Fist clear Jacobian, derivatives and x
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          CCIndex v = vertices[i];
          SolverVertexData &vSD = vertexAttr[v];
          vSD.dx.zero();
          vSD.cx.zero();
          vSD.j.zero();
          for(CCIndex n : sg.neighbors(v))
            edgeAttr[sg.orientedEdge(v, n)].j.zero();
        }
        // Call providers to add their parts to Derivatives and Jacobian
        for(Derivs *d : derivs) {
          d->getValues(*this, vertexAttr);
          d->calcDerivatives(*this, vertexAttr);
          d->calcJacobian(*this, vertexAttr, edgeAttr);
        }
        calcDirichletJac();
        // Put data on GPU
        B->write();
        J->write();
 
        bool doneNewton = false;
        // Adjust for backward Euler
        if(thisDt < 1.) {
          //mdxInfo << "I am in the Backward Euler situation" << endl;
          //*B *= thisDt;
          *J *= -1;
          J->addToDiagonal(1.0/thisDt);
          mdxInfo << "I am in the backward Euler situation" << endl;
        } else{
          *B *= -1.0;
          doneNewton = true;
          mdxInfo << "I am in the Newton situation" << endl;
        }
        bool stepValid = false;
        double prevNorm = MaxNorm;
        int iters = 0;
        while(!stepValid) {
          if(!progressAdvance())
            break;
          // Clear X
          *X = 0;
          
          // Solve the system
          switch(SolverType) {
            case CGSolver:
              iters = CG(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
            case CGPreCondSolver:
              iters = CGPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0); 
              break;
            case BiCGStabSolver:
              iters = BiCGStab(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
            case BiCGStabPreCondSolver:
              iters = BiCGStabPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
          }
   
          // Read back the x correction
          X->read(); 
          // Get the norm
          //mdxInfo << "Just before calcDXNorm " << endl;
          xNorm = calcXNorm();
          // Converge or repeat based on xNorm
          if(xNorm < 2 * MaxNorm) {
            stepValid = true;
            mdxInfo << "the step is valid " << endl;
            // If xNorm is small enough, increase step size for next step
            if (xNorm < MaxNorm and xNorm < prevNorm)  {
              //dilute a bit to allow recovery
              prevNorm = xNorm;
              Dt *= 1.1;
              if(Dt > MaxDt)
                Dt = MaxDt;
            } 
            else if(thisDt > MinDt and dxNorm > (prevNorm * 1.1)) {
              // thisDt is too big, so we can make a smaller stepsize
              Dt = thisDt;
              // If we can reduce Dt further, do so
              Dt *= 0.95;
              if(Dt < MinDt)
                Dt = MinDt;
              thisDt = Dt;
            }
            //diluite a bit to allow recovery 
            prevNorm = 0.95 * prevNorm + 0.05 * xNorm;
          }
          else if (thisDt > MinDt)
          { 
            Dt = thisDt;
            // If we can reduce Dt further, do so
            double oldDt = Dt;
            Dt *= 0.5;
            if(Dt < MinDt)
              Dt = MinDt;
            thisDt = Dt;

            if (!doneNewton){
              J->addToDiagonal(-1.0 /oldDt) ;
              J->addToDiagonal(1.0/Dt);
            }
            //if Newton xNor was not < prevNorm, go back to Bkward Euler
            else{
              *B *= -1;
              *J *= -1;
              J->addToDiagonal(1.0/Dt);
            }
          }
          // If we can't reduce Dt, we must move on
          else
            break;

        }
        if(stepValid) {
          // Sum CG iterations
          sumIter += iters;
          // Get values from derivative providers, FIXME how to avoid dups?
          for(Derivs *d : derivs)
            d->getValues(*this, vertexAttr);
          // Update values
          #pragma omp parallel for
          for(uint i = 0; i < vertices.size(); i++) {
            SolverVertexData &vSD = vertexAttr[vertices[i]];
            if(hasDirichlet)
              for(uint i = 0; i < dim; i++)
                if(vSD.dirichlet[i] > 0)
                  if(vSD.cx[i] != 0) {
                    // This should not happen, but best to check anyway
                    mdxInfo << QString("Solver::solveBackwardEuler Displacement found for Dirichlet (%1)").arg(vSD.cx[i]) << endl; 
                    vSD.cx[i] = 0;
                  }
            if (doneNewton)
              vSD.x  += vSD.cx * MaxNewtStep;
            else
              vSD.x += vSD.cx;
          }
          // Write back to the derivative providers, FIXME how to avoid dups?
          for(Derivs *d : derivs)
            d->setValues(*this, vertexAttr);
          // Update step progress
          //elapsed += thisDt;
          stepCount++;
        } else
          break; // If step wasn't valid user must have cancelled
      }
      if(PrintStats and stepCount > 0) {
        if(thisDt < 1.) {
          mdxInfo << "Solver Steps:" << stepCount << " ";
          //mdxInfo << "Elapsed t:" << elapsed << " ";
          mdxInfo << "CG Avg. Iter:" << sumIter/stepCount << " ";
          mdxInfo << "Final Dt:" << Dt << " ";
          //if(truncatedDt) mdxInfo << "(truncated to " << thisDt << ") ";
          mdxInfo << "XNorm:" << xNorm << endl;
        } else { 
          mdxInfo << "Solver Steps:" << stepCount << " ";
          mdxInfo << "CG Avg. Iter:" << sumIter/stepCount << " ";
          mdxInfo << "XNorm:" << xNorm << endl;
        }
      }
      return stepCount; 
    }


    double DirectSolver(void)
    {
      // Write neighborhoods to thrust
      nhbd->write();
      J->alloc();
      X->alloc();
      B->alloc();

      const CCIndexVec &vertices = sg.vertices();

      // Fist clear Jacobian, derivatives and x
      #pragma omp parallel for
      for(uint i = 0; i < vertices.size(); i++) {
          CCIndex v = vertices[i];
          SolverVertexData &vSD = vertexAttr[v];
          vSD.dx.zero();
          vSD.cx.zero();
          vSD.j.zero();
          for(CCIndex n : sg.neighbors(v))
            edgeAttr[sg.orientedEdge(v, n)].j.zero();
      }
      // Call providers to add their parts to Derivatives and Jacobian
      for(Derivs *d : derivs) {
          d->getValues(*this, vertexAttr);
          d->calcDerivatives(*this, vertexAttr);
          d->calcJacobian(*this, vertexAttr, edgeAttr);
      }
      calcDirichletJac();
      
      // Put data on GPU
      B->write();
      J->write();
 
      // Adjust for direct solver the Jacobian (
      *J *= -1.0;

      *X = 0;
       
      double norm = 0;  
      int iters = 0; 
      // Solve the system
      switch(SolverType) {
            case CGSolver:
              iters = CG(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
            case CGPreCondSolver:
              iters = CGPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0); 
              break;
            case BiCGStabSolver:
              iters = BiCGStab(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
            case BiCGStabPreCondSolver:
              iters = BiCGStabPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);  
              break;
       }
   
       // Read back the solution
       X->read(); 

       for(Derivs *d : derivs)
            d->getValues(*this, vertexAttr);

       // Update values
       #pragma omp parallel for
       for(uint i = 0; i < vertices.size(); i++) {
            SolverVertexData &vSD = vertexAttr[vertices[i]];
            if(hasDirichlet) {
              for(uint i = 0; i < dim; i++) {
                if(vSD.dirichlet[i] > 0){
                  if(vSD.cx[i] != 0) {
                    // This should not happen, but best to check anyway
                    mdxInfo << QString("Solver::DirectSolver Displacement found for Dirichlet (%1)").arg(vSD.cx[i]) << endl; 
                    vSD.cx[i] = 0;
                  }
                } else
                  vSD.x = vSD.cx;
	      }
	    }
       }
       

       // Write back to the derivative providers, FIXME how to avoid dups?
       for(Derivs *d : derivs)
         d->setValues(*this, vertexAttr);



       // Get values from derivative providers, FIXME how to avoid dups?
      if(PrintStats) {
          mdxInfo << "CG Iter:" << iters << " ";
      }
      return norm;

    }
    /// Solve with trapezoidal rule
    double solveTrapezoidal(void)
    {
      // Write neighborhoods to thrust
      nhbd->write();
      J->alloc();
      X->alloc();
      B->alloc();

      const CCIndexVec &vertices = sg.vertices();
      uint sumIter = 0, stepCount = 0;
      double elapsed = 0, err = 0, thisDt = 0;
      bool hitMinDt = false, truncatedDt = false;
      while(elapsed < Step and stepCount < MaxSteps) {
        thisDt = Dt;
        if(elapsed + thisDt > Step) {
          thisDt = Step - elapsed;
          truncatedDt = true;
        }

        // Fist clear Jacobian, derivatives and x
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          CCIndex v = vertices[i];
          SolverVertexData &vSD = vertexAttr[v];
          vSD.dx.zero();
          vSD.cx.zero();
          vSD.j.zero();
          for(CCIndex n : sg.neighbors(v))
            edgeAttr[sg.orientedEdge(v, n)].j.zero();
        }
        // Call providers to add their parts to Derivatives and Jacobian
        for(Derivs *d : derivs) {
          d->getValues(*this, vertexAttr);
          d->calcDerivatives(*this, vertexAttr);
          d->calcJacobian(*this, vertexAttr, edgeAttr);
        }
        calcDirichletJac();

        // Put data on GPU
        B->write();
        J->write();

        // Adjust for trapezoid rule: (I - dt/2 J(y))dy = dt f(y)
        *B *= thisDt;
        *J *= -0.5 * thisDt;
        J->addToDiagonal(1.0);

        bool stepValid = false;
        int iters = 0;
        while(!stepValid) {
          // Clear X
          *X = 0;

          // Solve the linear system
          switch(SolverType) {
            case CGSolver:
              iters = CG(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
            case CGPreCondSolver:
              iters = CGPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
            case BiCGStabSolver:
              iters = BiCGStab(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
            case BiCGStabPreCondSolver:
              iters = BiCGStabPreCond(*J, *X, *B, CGTolerance, CGMaxIter, 1e100, 0);
              break;
          }

          // Error estimate is X - B
          *X -= *B;
          X->read();
          err = calcXNorm();
          *X += *B;

          if(isNan(err)) {
            if(isNan(calcDXNorm()))
              throw(QString("Solver::calcTrapezoid: calcDerivatives returned NaN at elapsed time %1.")
                   .arg(elapsed));
            else
              throw(QString("Solver::calcTrapezoid: Derivatives became NaN at elapsed time %1.")
                   .arg(elapsed));
          }

          // Change Dt to get error closer to what we are willing to accept
          if(!truncatedDt && fabs(err) > 1e-10) {
            Dt *= 0.9 * clamp(MaxNorm / err ,1./3, 2.0);
            Dt = clamp(Dt, MinDt, MaxDt);
          }

          // Our next step depends on how err relates to our maximum acceptable error
          if(err < MaxNorm) {
            // Error low enough that we can move on.
            stepValid = true;
          } else {
            // Error was too high.
            // If the last step was already lower than MinDt, we can't reduce any further.
            if(thisDt <= MinDt) {
              hitMinDt = true;
              break;
            }
            // If we were truncated, but error was too high,
            // we can set Dt to be lower than our truncated step.
            if(truncatedDt) {
              Dt = thisDt * 0.9 * clamp(MaxNorm / err, 1./3, 2.0);
              Dt = clamp(Dt, MinDt, MaxDt);
              truncatedDt = false;
            }
            // We have to adjust J and B to use the new Dt on the next attempt.
            J->addToDiagonal(-1.0);
            *B *= Dt / thisDt;
            *J *= Dt / thisDt;
            J->addToDiagonal(1.0);
            thisDt = Dt;
          }
        }
        // Sum CG iterations
        sumIter += iters;

        // Get values from derivative providers, FIXME how to avoid dups?
        X->read();
        for(Derivs *d : derivs)
          d->getValues(*this, vertexAttr);

        // Update values
        #pragma omp parallel for
        for(uint i = 0; i < vertices.size(); i++) {
          SolverVertexData &vSD = vertexAttr[vertices[i]];
          vSD.x += vSD.cx;
        }

        // Write back to the derivative providers, FIXME how to avoid dups?
        for(Derivs *d : derivs)
          d->setValues(*this, vertexAttr);

        // Update step progress
        elapsed += thisDt;
        stepCount++;
      }
      if(hitMinDt) {
        mdxWarning << "solveTrapezoidal: Error estimate exceeded MaxNorm even at MinDt. "
                   << "Try lowering MinDt." << endl;
      }
      if(PrintStats) {
        mdxInfo << "Solver Steps:" << stepCount << " ";
        mdxInfo << "CG Avg. Iter:" << sumIter/stepCount << " ";
        mdxInfo << "Elapsed t:" << elapsed << " ";
        mdxInfo << "Final Dt:" << Dt << " ";
        if(truncatedDt) mdxInfo << "(truncated to " << thisDt << ") ";
        mdxInfo << "Est. error:" << err << endl;
      }
      return elapsed;
    }

    inline double calcXNorm(void) { return calcNorm(&SolverVertexData::cx); }
    inline double calcDXNorm(void) { return calcNorm(&SolverVertexData::dx); }

    double calcNorm(VectorT SolverVertexData::*field)
    {
      // Find the norm of the correction
      const CCIndexVec &vertices = sg.vertices();
      double avgNorm = 0.0, maxNorm = 0.0;
      #pragma omp parallel for reduction(+:avgNorm) reduction(max:maxNorm) 
      for(uint i = 0; i < vertices.size(); i++) {
        auto &vSD = vertexAttr[vertices[i]];
        double s = 0;
        // Skip Dirichlet nodes
        if(hasDirichlet) {
          auto x = vSD.*field;
          for(uint j = 0; j < dim; j++)
            if(vSD.dirichlet[j] != 0)
              if(x[j] != 0) {
                // This should not happen
                mdxInfo << QString("Solver::calcNorm Non zero value found on dirichlet node (%1)").arg(x[j]) << endl;
                x[j] = 0;
              }
          s = norm(x);
        } else
          s = norm(vSD.*field);

        avgNorm += s;
        if(maxNorm < s)
          maxNorm = s;
      }
      avgNorm /= vertices.size();

      return 0.5 * (avgNorm + maxNorm);
    }

    void setBoundaryFlags()
    {
      // See if there are Dirichlet conditions
      hasDirichlet = false;
      const CCIndexVec &vertices = sg.vertices();
      #pragma omp parallel for
      for(uint i = 0; i < vertices.size(); i++) {
        if(hasDirichlet)
          continue;
        SolverVertexData &vSD = vertexAttr[vertices[i]];
        for(uint j = 0; j < dim; j++)
          if(vSD.dirichlet[j] > 0) {
            hasDirichlet = true;
            break;
          }
      }
    }

    virtual void calcDirichletDeriv(VertexAttr &vattr)
    {
      if(!hasDirichlet) 
        return;

      const CCIndexVec &vertices = graph().vertices();
      #pragma omp parallel for
      for(uint i = 0; i < vertices.size(); i++) {
        CCIndex v = vertices[i];
        SolverVertexData &vSD = vattr[v];
        for(uint j = 0; j < dim; j++)
          if(vSD.dirichlet[j] > 0) {
            vSD.dirx[j] = vSD.dx[j];
            vSD.dx[j] = 0;
          } else
            vSD.dirx[j] = 0;
      }
    }

    inline virtual void calcDirichletDeriv(void) {
      calcDirichletDeriv(vertexAttr);
    }

    virtual void calcDirichletJac()
    {
      if(!hasDirichlet) 
        return;

      const CCIndexVec &vertices = graph().vertices();
      #pragma omp parallel for
      for(uint i = 0; i < vertices.size(); i++) {
        CCIndex v = vertices[i];
        SolverVertexData &vSD = vertexAttr[v];
        bool hasD = false;
        for(uint j = 0; j < dim; j++)
          if(vSD.dirichlet[j] > 0) {
            vSD.dirx[j] = vSD.dx[j];
            vSD.dx[j] = 0;
            vSD.j[j].zero();
            vSD.j[j][j] = 1.0;
            hasD = true;
          } else
            vSD.dirx[j] = 0;
        // If any dimensions have dirichlet
        if(hasD) {
          for(CCIndex nb : graph().neighbors(v)) {
            SolverEdgeData &nED = edgeAttr[graph().orientedEdge(v,nb)];
            for(uint j = 0; j < dim; j++)
              if(vSD.dirichlet[j] > 0) // Zero the row, it probably doesn't matter if we skip the columns 
                nED.j[j].zero();
          }
        }
      }
    }
  };

  /**
   * Class for solver derivatives, a system can have more than one.
   */
  class SolverDerivParms : virtual public ProcessParms
  {
  public:
    SolverDerivParms()
    {
      setName("Solver Derivatives");
      setDesc("Calculate solver derivatives and/or Jacobian");

      addParm("Delta", "Delta for numerical differentiation of Jacobian", "1e-6");
      addParm("OMP Get Set", "Use OMP for get/set and other operations", "Yes", booleanChoice());
      addParm("OMP Derivs", "Use OMP for derivatives", "No", booleanChoice());
      addParm("OMP Jac Diag", "Use OMP for the Jacobian diagonal entries (user analytic)", "No", booleanChoice());
      addParm("OMP Jac Nhbs", "Use OMP for the Jacobian for neighbors (user analytic)", "No", booleanChoice());
    }
  };

  template<typename _VectorT>
  class SolverDerivs : virtual public SolverDerivParms
  {
  public:
    typedef Solver<_VectorT> SolverT;
    typedef typename SolverT::VectorT VectorT;
    typedef typename SolverT::ScalarT ScalarT;
    typedef typename SolverT::MatrixT MatrixT;
    typedef typename SolverT::VectorUIntT VectorUIntT;
    typedef typename SolverT::SolverVertexData SolverVertexData;
    typedef typename SolverT::VertexAttr VertexAttr;
    typedef typename SolverT::SolverEdgeData SolverEdgeData;
    typedef typename SolverT::EdgeAttr EdgeAttr;
    static const int dim = SolverT::dim;

    enum JacTypeEnum { Constant, NeighborsConstant, NotConstant };
    static JacTypeEnum stringToJacType(const QString &str)
    {
      if(str == "Constant")
        return(Constant);
      else if(str == "Neighbors Constant")
        return(NeighborsConstant);
      else if(str == "Not Constant")
        return(NotConstant);
      else 
        throw(QString("SolverDerivs::stringToJacType Bad Jacobian type %1").arg(str));
    }

    double Delta = 1e-6;
    bool OMPGetSet = false, OMPDerivs = false, OMPJacDiag = false, OMPJacNhbs = false;

  public:
    // Process parameters
    bool processParms()
    {
      Delta = parm("Delta").toDouble();
      OMPGetSet = stringToBool(parm("OMP Get Set"));
      OMPDerivs = stringToBool(parm("OMP Derivs"));
      OMPJacDiag = stringToBool(parm("OMP Jac Diag"));
      OMPJacNhbs = stringToBool(parm("OMP Jac Nhbs"));

      return true;
    }

    // Initialize derivative provider, all topology changes must be here
    virtual void initDerivs(SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr) {}

    // place current values into vAttr.x
    virtual void getValues(const SolverT &solver, VertexAttr &vAttr)
    {
      const CCIndexVec &vertices = solver.graph().vertices();
      bool error = false;
      QString errorStr;
      #pragma omp parallel for if(OMPGetSet)
      for(uint i = 0; i < vertices.size(); i++) {
        if(error)
          continue;
        try {
          CCIndex v = vertices[i];
          getValues(solver, v, vAttr[v].x);
        } catch(const QString &s) {
          errorStr = s;
          error = true;
        } catch(...) {
          error = true;
        }
      }
      if(error) {
        if(errorStr.isEmpty())
          throw(QString("SolverDerivs::getValues Unknown exception"));
        else
          throw(QString("SolverDerivs::getValues %1").arg(errorStr));
      }
    }
    // place current values for vertex v into x
    virtual void getValues(const SolverT &solver, CCIndex v, VectorT &x)
    { 
      throw(QString("SolverDerivs::getValues called unimplemented getValues(v)")); 
    }

    // place values from vAttr.x into model
    virtual void setValues(const SolverT &solver, VertexAttr &vAttr)
    {
      const CCIndexVec &vertices = solver.graph().vertices();
      bool error = false;
      QString errorStr;
      #pragma omp parallel for if(OMPGetSet)
      for(uint i = 0; i < vertices.size(); i++) {
        if(error)
          continue;
        try {
          CCIndex v = vertices[i];
          setValues(solver, v, vAttr[v].x);
        } catch(const QString &s) {
          errorStr = s;
          error = true;
        } catch(...) {
          error = true;
        }
      }
      if(error) {
        if(errorStr.isEmpty())
          throw(QString("SolverDerivs::setValues Unknown exception"));
        else
          throw(QString("SolverDerivs::setValues %1").arg(errorStr));
      }
    }

    // place values from x into vertex v
    virtual void setValues(const SolverT &solver, CCIndex v, const VectorT &x)
    { 
      throw(QString("SolverDerivs::setValues called unimplemented setValues(v)")); 
    }

    // calc (add) derivatives for current values to vAttr.dx
    virtual void calcDerivatives(const SolverT &solver, VertexAttr &vAttr)
    {
      const CCIndexVec &vertices = solver.graph().vertices();
      bool error = false;
      QString errorStr;
      #pragma omp parallel for if(OMPDerivs)
      for(uint i = 0; i < vertices.size(); i++) {
        if(error)
          continue;
        try {
          CCIndex v = vertices[i];
          SolverVertexData &vSD = vAttr[v];
          calcDerivatives(solver, v, vSD.dx);
        } catch(const QString &s) {
          errorStr = s;
          error = true;
        } catch(...) {
          error = true;
        }
      }
      if(error) {
        if(errorStr.isEmpty())
          throw(QString("SolverDerivs::calcDerivatives Unknown exception"));
        else
          throw(QString("SolverDerivs::calcDerivatives %1").arg(errorStr));
      }
    }

    // Calculate (add) derivatives for vertex v to dx
    virtual void calcDerivatives(const SolverT &solver, CCIndex v, VectorT &dx)
    { 
      throw QString("SolverDerivs::calcDerviatives called unimplemented calcDerivatives(v)"); 
    }

    // Calculate (add) derivatives and Jacobian for current values to vAttr.dx, vAttr.j, and eAttr.j
    virtual void calcJacobian(const SolverT &solver, VertexAttr &vAttr, EdgeAttr &eAttr)
    {
      const CCIndexVec &vertices = solver.graph().vertices();

      // Calculate diagonal values
      bool error = false;
      QString errorStr;
      #pragma omp parallel for if(OMPJacDiag)
      for(uint i = 0; i < vertices.size(); i++) {
        if(error)
          continue;
        try {
          CCIndex v = vertices[i];
          SolverVertexData &vSD = vAttr[v];
          calcJacobian(solver, v, vSD.j);
        } catch(const QString &s) {
          errorStr = s;
          error = true;
        } catch(...) {
          error = true;
        }
      }
      if(error) {
        if(errorStr.isEmpty())
          throw(QString("SolverDerivs::calcJacobian Unknown exception in diagonal"));
        else
          throw(QString("SolverDerivs::calcJacobian Error in diagonal %1").arg(errorStr));
      }

      // Calculate off-diagonal values
      error = false;
      #pragma omp parallel for if(OMPJacNhbs)
      for(uint i = 0; i < vertices.size(); i++) {
        if(error)
          continue;
        try {
          CCIndex v = vertices[i];
          for(CCIndex nb : solver.graph().neighbors(v)) {
            SolverEdgeData &nED = eAttr[solver.graph().orientedEdge(v,nb)];
            calcJacobian(solver, v, nb, nED.j);
          }
        } catch(const QString &s) {
          errorStr = s;
          error = true;
        } catch(...) {
          error = true;
        }
      }
      if(error) {
        if(errorStr.isEmpty())
          throw(QString("SolverDerivs::calcJacobian Unknown exception in off-diagonal"));
        else
          throw(QString("SolverDerivs::calcJacobian Error in off-diagonal %1").arg(errorStr));
      }
    }

    // Calculate (add) diagonal part of Jacobian for vertex v to j
    virtual void calcJacobian(const SolverT &solver, CCIndex v, MatrixT &j)
    {
      // The default implementation calculates the Jacobian numerically
      double delta2 = Delta * 2;

      // Save current values
      VectorT x0;
      getValues(solver, v, x0);
      
      // Partials for current vertex
      MatrixT partials;
      for(uint i = 0 ; i < dim ; i++) {
        VectorT x(x0), dxb, dxf;

        x[i] -= Delta;
        setValues(solver, v, x);
        calcDerivatives(solver, v, dxb);

        x[i] += 2 * Delta;
        setValues(solver, v, x);
        calcDerivatives(solver, v, dxf);

        partials[i] = (dxf - dxb) / delta2;
      }

      // Restore values
      setValues(solver, v, x0);

      j += transpose(partials);
    }

    // add off-diagonal part of Jacobian for vertex v based on vertex nb to j
    virtual void calcJacobian(const SolverT &solver, CCIndex v, CCIndex nb, MatrixT &j)
    {
      // The default implementation calculates the Jacobian numerically
      double delta2 = Delta * 2;

      // Save current values
      VectorT x0;
      getValues(solver, nb, x0);
      
      // Partials for current vertex
      MatrixT partials;
      for(uint i = 0 ; i < dim ; i++) {
        VectorT x(x0), dxb, dxf;

        x[i] -= Delta;
        setValues(solver, nb, x);
        calcDerivatives(solver, v, dxb);

        x[i] += delta2;
        setValues(solver, nb, x);
        calcDerivatives(solver, v, dxf);

        partials[i] = (dxf - dxb) / delta2;
      }

      // Restore values
      setValues(solver, nb, x0);

      j += transpose(partials);
    }
  };
}

#endif
