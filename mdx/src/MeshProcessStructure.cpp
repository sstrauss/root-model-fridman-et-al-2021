//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessStructure.hpp>
#include <CCTopology.hpp>

namespace mdx 
{
  bool MeshSubdivide::run(Mesh &mesh, CCStructure &cs, Subdivide *sDiv)
  {
    CCIndexDataAttr &indexAttr = mesh.indexAttr();
    subdivideTriangles(cs, indexAttr, sDiv);
    faceAttrFromVertices(mesh, cs, cs.faces());

    setStatus(QString("Subdivide triangles, total vertices: %1").arg(cs.cellCount(0)));
    return true;
  }
  REGISTER_PROCESS(MeshSubdivide);

  bool SubdivideTriangles::run(Mesh &mesh, CCStructure &cs, Subdivide *sDiv)
  {
    CCIndexDataAttr &indexAttr = mesh.indexAttr();
    CCIndexVec newVertices;
    AttrMap<CCIndex, uint> vMap;
    AttrMap<CCIndex, CCIndex> eMap;
    // Split all edges
    const CCIndexVec &edges = cs.edges();
    CCIndexVec vertices = cs.vertices();
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); ++i)
      vMap[vertices[i]] = i;
    // We'll add a vertex for each edge
    uint vCount = vertices.size();
    vertices.resize(vCount + edges.size());
    if(!progressAdvance())
      userCancel();
    #pragma omp parallel for
    for(uint i = 0; i < edges.size(); ++i) {
      // Create edge split
      CCIndex e = edges[i];
      CCStructure::SplitStruct ss(e);
      CCIndexFactory.fillSplitStruct(ss);
      // Update the membrane data
      CCIndexPair eBnds = cs.edgeBounds(e);
      indexAttr[ss.membrane].pos = (indexAttr[eBnds.first].pos + indexAttr[eBnds.second].pos)/2.0; 
      if(sDiv)
        sDiv->splitCellUpdate(1, cs, ss, eBnds.first, eBnds.second, 0.5);
      // Save for next step
      eMap[e] = ss.membrane;
      // Add the vertex that split the edge
      vertices[vCount + i] = ss.membrane;
      vMap[ss.membrane] = vCount + i;
    }
    if(!progressAdvance())
      userCancel();
    // Create new triangles
    const CCIndexVec &faces = cs.faces();
    std::vector<std::vector<uint> > faceVtx(faces.size() * 4);
    bool allTris = true;
    CCIndexVec newFaces(faces.size() * 4);
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); ++i) {
      //if(!progressAdvance(0, 10000))
        //continue;
      CCIndex f = faces[i];
      // Grab face vertices (in order)
      CCIndexVec fVertices = faceVertices(cs, f);
      if(allTris and fVertices.size() != 3) {
        #pragma omp atomic write
        allTris = false;
      }
      // Get the vertices from the edges
      CCIndex e0 = cs.join(fVertices[0], fVertices[1]);
      CCIndex e1 = cs.join(fVertices[1], fVertices[2]);
      CCIndex e2 = cs.join(fVertices[2], fVertices[0]);

      // Create the triangles
      uint pos = i * 4;
      faceVtx[pos].resize(3);
      faceVtx[pos][0] = vMap[eMap[e0]];
      faceVtx[pos][1] = vMap[eMap[e1]];
      faceVtx[pos][2] = vMap[eMap[e2]];
      auto &fIdx = indexAttr[f];
      newFaces[pos] = f;

      faceVtx[++pos].resize(3);
      faceVtx[pos][0] = vMap[fVertices[0]];
      faceVtx[pos][1] = vMap[eMap[e0]];
      faceVtx[pos][2] = vMap[eMap[e2]];
      CCIndex f1 = CCIndexFactory.getIndex();
      auto &f1Idx = indexAttr[f1];
      f1Idx = fIdx;
      newFaces[pos] = f1;

      faceVtx[++pos].resize(3);
      faceVtx[pos][0] = vMap[fVertices[1]];
      faceVtx[pos][1] = vMap[eMap[e1]];
      faceVtx[pos][2] = vMap[eMap[e0]];
      CCIndex f2 = CCIndexFactory.getIndex();
      auto &f2Idx = indexAttr[f2];
      f2Idx = fIdx;
      newFaces[pos] = f2;

      faceVtx[++pos].resize(3);
      faceVtx[pos][0] = vMap[fVertices[2]];
      faceVtx[pos][1] = vMap[eMap[e2]];
      faceVtx[pos][2] = vMap[eMap[e1]];
      CCIndex f3 = CCIndexFactory.getIndex();
      auto &f3Idx = indexAttr[f3];
      f3Idx = fIdx;
      newFaces[pos] = f3;
    }
    if(!progressAdvance())
      userCancel();
    // Throw error if not a triangle mesh
    if(!allTris)
      throw(QString("Subdivide Triangles only works on triangle meshes"));

    // Create connectivity from triangle list
    // RSS Note this tosses the edges we created, need to fix
    cs.clear();
       
    if(!progressAdvance())
      userCancel();
    // Create the mesh
    ccFromFaces(cs, vertices, newFaces, faceVtx);

    // Propagate the attributes to the faces
    faceAttrFromVertices(mesh, cs, cs.faces());

    return true;
  }
  REGISTER_PROCESS(SubdivideTriangles);

  bool SubdivideBisectTriangle::run(Mesh &mesh, CCStructure &cs, double maxArea, Subdivide *sDiv)
  {
    if(maxArea == 0)
      return subdivideBisectTriangle(cs, mesh.indexAttr(), activeFaces(cs, mesh.indexAttr()), sDiv);
    else {
      CCIndexVec faces;
      for(CCIndex f : activeFaces(cs, mesh.indexAttr()))
        if(mesh.indexAttr()[f].measure > maxArea)
          faces.push_back(f);
      if(faces.size() == 0)
        return false;
      return subdivideBisectTriangle(cs, mesh.indexAttr(), faces, sDiv);
    }
  }
  REGISTER_PROCESS(SubdivideBisectTriangle);

  bool SubdivideBisectWedge::run(Mesh &mesh, CCStructure &cs, double maxArea, Subdivide *sDiv)
  {
    if(maxArea == 0)
      return subdivideBisectWedge(cs, mesh.indexAttr(), activeVolumes(cs, mesh.indexAttr()), sDiv);
    else {
      CCIndexVec volumes;
      for(CCIndex l : activeVolumes(cs, mesh.indexAttr()))
        for(CCIndex f : cs.bounds(l)) {
          if(cs.bounds(f).size() != 3) // Only check triangle faces
            continue;
          if(mesh.indexAttr()[f].measure > maxArea) {
            volumes.push_back(l);
            break;
          }
        }
      if(volumes.size() == 0)
        return false;
      return subdivideBisectWedge(cs, mesh.indexAttr(), volumes, sDiv);
    }
  }
  REGISTER_PROCESS(SubdivideBisectWedge);

  bool MergeCells::run(CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexVec &faces, Subdivide *sDiv)
  {
    // Merge faces
    CCIndexSet toMerge(faces.begin(), faces.end());
    while(toMerge.size() > 0) {
      CCIndex f = *toMerge.begin();
      if(!cs.hasCell(f)) {
        toMerge.erase(f);
        continue;
      }
      int merged = 0;
      CCIndex m = f;
      for(auto n : cs.neighbors(m)) {
        if(n == m)
          break;
        if(toMerge.count(n)) {
          // Merge the path between them first
          auto mVertices = cs.incidentCells(m, 0);
          auto nVertices = cs.incidentCells(n, 0);
          auto cVertices = intersect(mVertices, nVertices);
          while(cVertices.size() > 0) {
            auto v = *cVertices.begin();
            auto cobounds = cs.cobounds(v);
            if(cobounds.size() != 2) {
              cVertices.erase(v);
              continue;
            }
            auto e0 = *cobounds.begin();
            auto e1 = *cobounds.rbegin();
            if(cs.ro(v, e0) == cs.ro(v, e1))
              cs.reverseOrientation(e1);
            SplitStruct ss(CCIndexFactory.getIndex(), v, *cobounds.begin(), *cobounds.rbegin());
            if(!cs.mergeCells(ss)) {
              mdxInfo << QString("%1::run Merge shared edges failed").arg(name()) << endl;
              return false;
            }
            cVertices.erase(v);
          }

          CCIndex e = cs.meet(m, n);
          if(cs.ro(e, m) == cs.ro(e, n))
            cs.reverseOrientation(n);
          SplitStruct ss(CCIndexFactory.getIndex(), e, m, n);
          if(!cs.mergeCells(ss)) {
            mdxInfo << QString("%1::run Merge cells failed").arg(name()) << endl;
            return false;
          }
          // For now at least set the position
          indexAttr[ss.parent].pos = (indexAttr[m].pos + indexAttr[n].pos)/2.0;;
          toMerge.erase(m);
          toMerge.erase(n);
          m = ss.parent;
          merged++;
        }
      }
      if(merged == 0)
        toMerge.erase(f);
      else
        toMerge.insert(m);
    }
    return true;
  }
  REGISTER_PROCESS(MergeCells);

  bool TransformMesh::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, 
                                                const Point3d &translation, const Point3d &rotation, double angle, double scale)
  {
    Point4d dir = homogeneous(rotation);
    Matrix4d M = Matrix4d::rotation(dir, angle * M_PI / 180.0);
    M(0, 3) = translation.x();
    M(1, 3) = translation.y();
    M(2, 3) = translation.z();
    M(3, 3) = 1.0 / scale;
    const CCIndexVec vs = activeVertices(cs, indexAttr);

    if(scale <= 0) 
      throw(QString("TransformMesh::run Scaling must be > 0"));

    for(CCIndex v : vs) {
      CCIndexData &vIdx = indexAttr[v];
      vIdx.pos = Point3d(cartesian(M * homogeneous(Point3d(vIdx.pos))));
    }
    return true;
  }
  REGISTER_PROCESS(TransformMesh);

  bool CenterMesh::run(const CCStructure &cs, CCIndexDataAttr &indexAttr)
  {
    if(cs.vertices().size() == 0)
      return true;

    Point3d min(std::numeric_limits<double>::max());
    Point3d max(std::numeric_limits<double>::lowest());

    for(CCIndex v : cs.vertices()) {
      CCIndexData &vIdx = indexAttr[v];
      for(uint  i = 0; i < 3; i++) {
        if(min[i] > vIdx.pos[i])
          min[i] = vIdx.pos[i];
        if(max[i] < vIdx.pos[i])
          max[i] = vIdx.pos[i];
      }
    }
    Point3d shift = -(min + max)/2.0;
    if(shift != Point3d(0.0))
      for(CCIndex v : cs.vertices()) {
        CCIndexData &vIdx = indexAttr[v];
        vIdx.pos += shift;
      }
    return true;
  }
  REGISTER_PROCESS(CenterMesh);
//  
//  bool ReverseMesh::run(Mesh* mesh)
//  {
//    if(mesh->activeVertices().size() != mesh->graph().size())
//      return setErrorMessage("All vertices must be reversed at the same.");
//  
//    mesh->graph().reverse();
//    setNormals(mesh->graph());
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(ReverseMesh);
// 

  bool TransformMeshByFrame::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, Mesh *mesh, const QString &ccName)
  {
    /*GLdouble frameMatrix[4][4];
    mesh->stack()->frame().getMatrix(frameMatrix);
    Matrix4d mFrame; 
    mFrame[0] = Point4d(frameMatrix[0][0], frameMatrix[0][1], frameMatrix[0][2], frameMatrix[0][3]);
    mFrame[1] = Point4d(frameMatrix[1][0], frameMatrix[1][1], frameMatrix[1][2], frameMatrix[1][3]);
    mFrame[2] = Point4d(frameMatrix[2][0], frameMatrix[2][1], frameMatrix[2][2], frameMatrix[2][3]);
    mFrame[3] = Point4d(frameMatrix[3][0], frameMatrix[3][1], frameMatrix[3][2], frameMatrix[3][3]);

    mdxInfo << "frame " << mFrame[0] << endl;
    mdxInfo << "frame " << mFrame[1] << endl;
    mdxInfo << "frame " << mFrame[2] << endl;
    mdxInfo << "frame " << mFrame[3] << endl;*/

    for(CCIndex v : cs.vertices()){

      Point3f pos = Point3f(indexAttr[v].pos);
      indexAttr[v].pos = Point3d(mesh->stack()->frame().inverseCoordinatesOf(qglviewer::Vec(pos)));
    }
    Matrix4d m = Matrix4d::identity();
    mesh->stack()->frame().setFromMatrix(m.data());
   

    mesh->updatePositions(ccName);

    return true;
  }
  REGISTER_PROCESS(TransformMeshByFrame);

//  
 
  bool SmoothMesh::run(CCStructure &cs, CCIndexDataAttr &indexAttr, uint passes)
  {
    struct tmpPos {
      Point3d pos;
      int n;
    };
    AttrMap<CCIndex, tmpPos> tPos;

    auto av = activeVertices(cs, indexAttr);
    for(uint i = 0; i < passes; ++i) {
      if(!progressAdvance(i))
        userCancel();
      #pragma omp parallel for
      for(uint i = 0; i < av.size(); i++) {
        CCIndex v = av[i];
        auto &vIdx = indexAttr[v];
        auto &vPos = tPos[v];
        vPos.pos = vIdx.pos;
        vPos.n = 1;
        for(auto n : cs.neighbors(v)) {
          auto &nIdx = indexAttr[n];
          vPos.pos += nIdx.pos;
          vPos.n++;
        }
      }
      #pragma omp parallel for
      for(uint i = 0; i < av.size(); i++) {
        CCIndex v = av[i];
        auto &vIdx = indexAttr[v];
        auto &vPos = tPos[v];
        vIdx.pos = vPos.pos / vPos.n;
      }
    }
    return true;
  }
  REGISTER_PROCESS(SmoothMesh);

  bool SmoothBorder::run(CCStructure &cs, CCIndexDataAttr &indexAttr, uint passes)
  {
    struct tmpPos {
      Point3d pos;
      int n;
    };
    AttrMap<CCIndex, tmpPos> tPos;

    auto av = activeVertices(cs, indexAttr);
    for(uint i = 0; i < passes; ++i) {
      if(!progressAdvance(i))
        userCancel();
      #pragma omp parallel for
      for(uint i = 0; i < av.size(); i++) {
        CCIndex v = av[i];
        if(!cs.onBorder(v))
          continue;
        auto &vIdx = indexAttr[v];
        auto &vPos = tPos[v];
        vPos.pos = vIdx.pos;
        vPos.n = 1;
        for(auto n : cs.neighbors(v)) {
          if(!cs.onBorder(n))
            continue;
          auto &nIdx = indexAttr[n];
          vPos.pos += nIdx.pos;
          vPos.n++;
        }
      }
      #pragma omp parallel for
      for(uint i = 0; i < av.size(); i++) {
        CCIndex v = av[i];
        if(!cs.onBorder(v))
          continue;
        auto &vIdx = indexAttr[v];
        auto &vPos = tPos[v];
        vIdx.pos = vPos.pos / vPos.n;
      }
    }
    return true;
  }
  REGISTER_PROCESS(SmoothBorder);
 
 
  bool SplitEdges::run(Mesh &mesh, CCStructure &cs, double maxLength, Subdivide *sDiv)
  {
    if(maxLength == 0)
      return splitEdges(cs, mesh.indexAttr(), activeEdges(cs, mesh.indexAttr()), sDiv);
    else {
      CCIndexVec edges;
      for(CCIndex e : activeEdges(cs, mesh.indexAttr())) {
        auto eb = cs.edgeBounds(e);
        if(norm(mesh.indexAttr()[eb.first].pos - mesh.indexAttr()[eb.second].pos) > maxLength)
          edges.push_back(e);
      }
      if(edges.size() == 0)
        return false;
      return splitEdges(cs, mesh.indexAttr(), edges, sDiv);
    }
  }
  REGISTER_PROCESS(SplitEdges);

//  
//  bool ShrinkMesh::run(Mesh* mesh, float distance)
//  {
//    const std::vector<vertex>& vs = mesh->activeVertices();
//  
//    forall(const vertex& v, vs)
//      v->pos -= distance * v->nrml;
//  
//    setNormals(mesh->graph());
//    mesh->clearImgTex();
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(ShrinkMesh);
//  
//  bool SubdivideMesh::run(Mesh* mesh)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot subdivide mesh type (%1), must be (MDXM)").arg(mesh->meshType()));
//
//    vvGraph& S = mesh->graph();
//    const std::vector<vertex>& vs = mesh->activeVertices();
//  
//    VtxPoint2fAttr& tex2d = mesh->texCoord2d();
//    bool doTex2d = tex2d.size() > 0;
//  
//    progressStart(QString("Subdividing mesh %1").arg(mesh->userId()), 0);
//    vvGraph SS = S.subgraph(vs);
//    vvGraph T, T1;
//    T = S;
//    Insert<vvGraph> insert;
//    std::vector<vertex> new_vertices;
//  
//    // Add vertex in middle of all existing edges
//    forall(const vertex& n, SS) {
//      if(!progressAdvance()) {
//        S = T;
//        userCancel();
//      }
//      forall(const vertex& m, T.neighbors(n)) {
//        if(SS.contains(m) and m < n)
//          continue;
//        vertex v = insert(n, m, S);
//        new_vertices.push_back(v);
//        if(n->selected or m->selected)
//          v->selected = true;
//        v->margin = false;
//        v->signal = (m->signal + n->signal) / 2.0;
//        v->pos = (m->pos + n->pos) / 2.0;
//        if(doTex2d)
//          tex2d[v] = (tex2d[m] + tex2d[n]) / 2.0;
//        if(m->label == n->label)
//          v->label = m->label;
//      }
//    }
//    progressStart("", new_vertices.size() + 1);
//    T1 = S;
//    // Now add new edges
//    int i = 0;
//    forall(const vertex& v, new_vertices) {
//      ++i;
//      if((i % 1000 == 0)and !progressAdvance(i)) {
//        S = T;
//        userCancel();
//      }
//      forall(const vertex& n, T1.neighbors(v)) {
//        vertex m = T1.nextTo(v, n);
//        vertex nv = T1.nextTo(n, v);
//        vertex mv = T1.prevTo(m, v);
//        vertex nm = T1.nextTo(nv, n);
//        vertex mn = T1.prevTo(mv, m);
//        if(nm != mn) {
//          if(mn == nv)         // it's a border!
//          {
//            if(norm(mv->pos - v->pos) < norm(m->pos - nv->pos)) {
//              S.spliceAfter(v, m, mv);
//              S.spliceBefore(mv, m, v);
//            } else {
//              S.spliceBefore(nv, mv, m);
//              S.spliceAfter(m, mv, nv);
//            }
//          }
//          continue;
//        }
//        S.spliceBefore(nv, nm, mv);
//        S.spliceAfter(mv, mn, nv);
//      }
//    }
//  
//    setStatus("Mesh " << mesh->userId() << " - Subdivide triangles, total vertices:" << S.size());
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(SubdivideMesh);
//  
//  bool LoopSubdivisionMesh::run(Mesh* mesh)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot subdivide mesh type (%1), must be (MDXM)").arg(mesh->meshType()));
//
//    vvGraph& S = mesh->graph();
//    const std::vector<vertex>& vs = mesh->activeVertices();
//  
//    vvGraph OldS;
//    if(vs.size() != S.size())
//      OldS = S.subgraph(S);
//    else
//      OldS = S;
//  
//    size_t ss = OldS.size();
//    size_t disp = ss / 50;
//    VtxPoint2fAttr & tex2d = mesh->texCoord2d();
//    bool doTex2d = tex2d.size() > 0;
//  
//    progressStart(QString("Subdividing mesh %1").arg(mesh->userId()), 2 * ss);
//  
//    Insert<vvGraph> insert;
//    std::vector<vertex> new_vertices;
//    std::vector<Point3d> old_pos;
//    std::map<vertex, size_t> vertex_index;
//    old_pos.reserve(OldS.size());
//  
//    for(size_t i = 0; i < OldS.size(); ++i) {
//      vertex v = OldS[i];
//      vertex_index[v] = i;
//      old_pos[i] = v->pos;
//    }
//  
//    // Add vertex in middle of all existing edges
//    for(size_t i = 0; i < OldS.size(); ++i) {
//      if((i % disp == 0)and not progressAdvance(i))
//        userCancel();
//      bool surface_v = false;     // The vertex is on the border of the surface
//      const vertex& v = OldS[i];
//      size_t n = OldS.valence(v);
//      Point3d q;
//  
//      float w = 0.375 + 0.25 * std::cos(2.0 * M_PI / float(n));
//      w *= w;
//      w = 0.625 - w;
//  
//      forall(const vertex& u, OldS.neighbors(v)) {
//        size_t j = vertex_index[u];
//        bool boundary = true;
//        if((OldS.nextTo(u, v) == OldS.prevTo(v, u))and (OldS.prevTo(u, v) == OldS.nextTo(v, u)))
//          boundary = false;
//        else {
//          if(surface_v)
//            q = 0.0;
//          surface_v = true;
//        }
//        if((surface_v and boundary) or not surface_v)
//          q += old_pos[j];
//  
//        // Only sub-divide every undirected edge once
//        if(u < v)
//          continue;
//  
//        vertex x = insert(u, v, S);
//        new_vertices.push_back(x);
//  
//        // Set properties of the new vertex
//        if(u->selected or v->selected)
//          x->selected = true;
//        x->margin = boundary;
//        if(u->label == v->label)
//          x->label = u->label;
//        if(boundary) {
//          x->pos = (old_pos[i] + old_pos[j]) / 2.0;
//          if(doTex2d)
//            tex2d[x] = (tex2d[u] + tex2d[v]) / 2.0;
//          x->signal = (u->signal + v->signal) / 2.0;
//        } else {
//          const vertex& nv = OldS.nextTo(v, u);
//          const vertex& pv = OldS.prevTo(v, u);
//          size_t kn = vertex_index[nv];
//          size_t kp = vertex_index[pv];
//          x->pos = (old_pos[i] * 3.0 + old_pos[j] * 3.0 + old_pos[kn] + old_pos[kp]) / 8.0;
//          if(doTex2d)
//            tex2d[x] = (tex2d[u] * 3.0 + tex2d[v] * 3.0 + tex2d[pv] + tex2d[nv]) / 8.0;
//          x->signal = (u->signal * 3.0 + v->signal * 3.0 + pv->signal + nv->signal) / 8.0;
//        }
//      }
//      if(surface_v) {
//        v->pos = 0.75 * v->pos + 0.125 * q;
//      } else {
//        v->pos = (1 - w) * v->pos + (w / n) * q;
//      }
//    }
//  
//    progressStart("", ss + new_vertices.size());
//  
//    // Connect the inside triangles
//    size_t i = 0;
//    disp = new_vertices.size() / 50;
//    forall(const vertex& v, new_vertices) {
//      if((i % disp == 0)and not progressAdvance(ss + i))
//        userCancel();
//      ++i;
//      const vertex& a = S.anyIn(v);
//      const vertex& b = S.nextTo(v, a);
//      if(OldS.prevTo(a, b) == OldS.nextTo(b, a)) {
//        S.spliceAfter(v, a, S.prevTo(a, v));
//        S.spliceBefore(v, b, S.nextTo(b, v));
//      }
//      if(OldS.nextTo(a, b) == OldS.prevTo(b, a)) {
//        S.spliceAfter(v, b, S.prevTo(b, v));
//        S.spliceBefore(v, a, S.nextTo(a, v));
//      }
//    }
//  
//    setStatus("Mesh " << mesh->userId() << " - Subdivide triangles, total vertices:" << S.size());
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(LoopSubdivisionMesh);
//  
//  bool AdaptiveSubdivideBorderMesh::run(Mesh* mesh, float cellMaxArea, float borderDist)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot subdivide mesh type (%1), must be (MDXM)").arg(mesh->meshType()));
//
//    vvGraph& S = mesh->graph();
//    const std::vector<vertex>& vs = mesh->activeVertices();
//  
//    // Find triangles on borders
//    mesh->markBorder(borderDist);
//  
//    // Copy to temp graph
//    vvGraph T = S;
//    forall(vertex v, vs)
//      forall(vertex n, T.neighbors(v)) {
//        vertex m = T.nextTo(v, n);
//        if(!S.uniqueTri(v, n, m))
//          continue;
//  
//        // Subdivide triangles on border and clear label
//        if(v->minb == 0 or m->minb == 0 or n->minb == 0)
//          continue;
//        v->label = n->label = m->label = 0;
//        float area = triangleArea(v->pos, n->pos, m->pos);
//        if(area < cellMaxArea)
//          continue;
//  
//        if(!subdivideBisect(S, v, n, m)) {
//          setErrorMessage("Failed bisecting triangle");
//          return false;
//        }
//      }
//    setStatus(S.size() << " total vertices");
//
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(AdaptiveSubdivideBorderMesh);
//  
//  bool AdaptiveSubdivideSignalMesh::run(Mesh* mesh, float cellMaxAreaLow, float cellMaxAreaHigh)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot subdivide mesh type (%1), must be (MDXM)").arg(mesh->meshType()));
//
//    vvGraph& S = mesh->graph();
//    const std::vector<vertex>& vs = mesh->activeVertices();
//  
//    // Find max/min colors
//    float maxc = 0, minc = HUGE_VAL;
//    forall(const vertex& v, vs) {
//      if(v->signal > maxc)
//        maxc = v->signal;
//      if(v->signal < minc)
//        minc = v->signal;
//    }
//    float rangec = maxc - minc;
//    // Return if no real signal difference
//    if(fabs(rangec) < 1e-5) {
//      setErrorMessage("subdivideTriangles::Error:No signal difference");
//      return false;
//    }
//    // Copy to temp graph
//    vvGraph T = S;
//    forall(vertex v, vs)
//      forall(vertex n, T.neighbors(v)) {
//        vertex m = T.nextTo(v, n);
//        if(!S.uniqueTri(v, n, m))
//          continue;
//  
//        // Find max signal on triangle
//        float maxcol = v->signal;
//        if(n->signal > maxcol)
//          maxcol = n->signal;
//        if(m->signal > maxcol)
//          maxcol = m->signal;
//  
//        float val = ((maxcol - minc) / rangec);
//        float area = triangleArea(v->pos, n->pos, m->pos);
//        if(area < interpolate(cellMaxAreaLow, cellMaxAreaHigh, val))
//          continue;
//  
//        if(!subdivideBisect(S, v, n, m)) {
//          setErrorMessage("Failed bisecting triangle");
//          return false;
//        }
//      }
//    setStatus(S.size() << " total vertices");
//
//    mesh->updateAll();
//
//    return true;
//  }
//  REGISTER_PROCESS(AdaptiveSubdivideSignalMesh);
//  
//  bool SubdivideBisectMesh::run(Mesh* mesh, float cellMaxArea)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot subdivide mesh type (%1), must be (MDXM)").arg(mesh->meshType()));
//
//    vvGraph& S = mesh->graph();
//    const std::vector<vertex>& vs = mesh->activeVertices();
//  
//    // Copy to temp graph
//    vvGraph T = S;
//    forall(vertex v, vs)
//      forall(vertex n, T.neighbors(v)) {
//        vertex m = T.nextTo(v, n);
//        if(!S.uniqueTri(v, n, m))
//          continue;
//  
//        float area = triangleArea(v->pos, n->pos, m->pos);
//        if(area < cellMaxArea)
//          continue;
//  
//        if(!subdivideBisect(S, v, n, m)) {
//          setErrorMessage("Failed bisecting triangle");
//          return false;
//        }
//      }
//    setStatus(S.size() << " total vertices");
//
//    mesh->updateAll();
//
//    return true;
//  }
//  REGISTER_PROCESS(SubdivideBisectMesh);
//
//  typedef std::set<vertex> tri;
//
//
//  void longestSide(const tri& t, vertex &a, vertex &b, vertex &c)
//  {
//    std::set<vertex>::iterator it = t.begin();
//    vertex v1 = *it; it++;
//    vertex v2 = *it; it++;
//    vertex v3 = *it;
//    a = v1; b = v2; c = v3;
//    if(norm(v2->pos - v3->pos) > norm(a->pos - b->pos)) {
//      a = v2; b = v3; c = v1;
//    }
//    if(norm(v3->pos - v1->pos) > norm(a->pos - b->pos)) {
//      a = v3; b = v1; c = v2;
//    }
//  }
//
//  void subdivideInsert(CellTissue& T, const tri& t, std::map<std::pair<vertex,vertex>, vertex>& splits)
//  {
//
//    // Find longest edge and insert vertex in all graphs
//    vertex a(0), b(0), c(0);
//    longestSide(t, a, b, c);
//
//    // Create a vertex and set position
//    vertex d;
//    d->pos = (a->pos + b->pos)/2.0;
//    d->selected = true;
//
//    // Put in list of divided edges, leave if already processed
//    std::pair<vertex,vertex> newP = std::make_pair(a,b);
//    if(b<a) newP = std::make_pair(b,a);
//    if(splits.find(newP) == splits.end())
//      splits[newP] = d;
//    else
//      return;
//
//    // Put in all graphs that contain it
//    forall(const cell &ce, T.C)
//      if(ce->S.contains(a) and ce->S.contains(b) and ce->S.edge(a, b)) {
//        ce->S.insert(d);
//        ce->S.insertEdge(d, a);
//        ce->S.insertEdge(d, b);
//        ce->S.replace(a, b, d);
//        ce->S.replace(b, a, d);
//      }
//
//
//  }
//
//
//  // Do triangle subdivsion from 1-4 triangles depending on how many edges are split
//  void subdivideTri(CellTissue& T, const tri& t, std::map<std::pair<vertex,vertex>, vertex>& splits)
//  {
//    // Vertex which was inserted in longest wall (always split)
//    vertex d(0);
//
//    // Find longest edge and divide
//    vertex a(0), a0(0), b(0), b0(0), c(0);
//    longestSide(t, a0, b0, c);
//
//    forall(const cell &ce, T.C) {
//      a = a0; b = b0;
//      if(!ce->S.contains(a) or !ce->S.contains(b) or !ce->S.contains(c) /*or !ce->S.edge(a, b) or !ce->S.edge(b, c) or !ce->S.edge(a, c)*/){
//        //std::cout << "cell doesnt contain tri " << !ce->S.contains(a) << !ce->S.contains(b) << !ce->S.contains(c) << "/" << !ce->S.edge(a, b) << !ce->S.edge(b, c) << !ce->S.edge(a, c)<< std::endl;
//        continue;
//      }
//
//      if(ce->S.edge(a,b)) {
//        std::cout << "long wall not divided" << std::endl;
//        continue;
//      }
//      std::pair<vertex,vertex> newP = std::make_pair(a,b);
//      if(b<a) newP = std::make_pair(b,a);
//      if(splits.find(newP) == splits.end())
//        std::cout << "Error d not found" << std::endl;
//
//      // Get vertex (d) in long wall
//      d = splits[newP];
//      // Order a-b-c correctly
//      vertex e = c;
//      if(!ce->S.edge(a,e)) {
//        std::pair<vertex,vertex> newP2 = std::make_pair(a,e);
//        if(e<a) newP2 = std::make_pair(e,a);
//        if(splits.find(newP2) == splits.end())
//          std::cout << "Error a-t not found" << std::endl;
//        e = splits[newP2];
//      }
//      if(e == ce->S.prevTo(a, d)) {
//        a = b0; b = a0;
//      }
//      // Check if d OK
//      if(!ce->S.edge(a, d) or !ce->S.edge(b, d)) {
//        std::cout << "Missing edge, d not between a and b, d:" << d->pos << " a:" << a->pos << " b:" << b->pos << std::endl;
//        continue;
//      }
//      // Check pos of d
//      if(norm((a->pos + b->pos)/2.0 - d->pos) > .00001) {
//        std::cout << "d in wrong position" << std::endl;
//        continue;
//      }
//      // This triangle is already divided
//      if(ce->S.edge(c, d)) {
//        std::cout << "Triangle already divided (c-d) edge exists" << std::endl;
//        continue;
//      }
//      // If all three vertices inserted, then divide into 4
//      if(!ce->S.edge(a, c) and !ce->S.edge(b, c)) {
//        vertex ac = ce->S.nextTo(a, d);
//        vertex bc = ce->S.prevTo(b, d);
//        ce->S.spliceBefore(d, a, ac);
//        ce->S.spliceAfter(d, b, bc);
//        ce->S.spliceAfter(ac, a, d);
//        ce->S.spliceBefore(ac, c, bc);
//        ce->S.spliceBefore(bc, b, d);
//        ce->S.spliceAfter(bc, c, ac);
//      } else if(ce->S.edge(a, c) and ce->S.edge(b, c)) {
//        ce->S.spliceAfter(c, a, d);
//        ce->S.spliceBefore(d, a, c);
//      } else if(ce->S.edge(a, c)) {
//        ce->S.spliceAfter(c, a, d);
//        ce->S.spliceBefore(d, a, c);
//        vertex v = ce->S.nextTo(c, d);
//        ce->S.spliceAfter(d, b, v);
//        ce->S.spliceBefore(v, b, d);
//      } else if(ce->S.edge(b, c)) {
//        ce->S.spliceBefore(c, b, d);
//        ce->S.spliceAfter(d, b, c);
//        vertex v = ce->S.prevTo(c, d);
//        ce->S.spliceBefore(d, a, v);
//        ce->S.spliceAfter(v, a, d);
//      } else
//        std::cout << "Triangle division case not handled" << std::endl;
//        
//    }
//    
//  }
//
//
//  // Subdivide triangles by bisection propogating split edge to all meshes
//  void subdivideTris(CellTissue& T, std::set<tri> &tris)
//  {
//    bool done = false;
//    while(!done) {
//      done = true;
//      forall(tri t, tris){
//        vertex v1(0), v2(0), v3(0);
//        longestSide(t,v1,v2,v3);
//        forall(const cell& c, T.C){
//          if(!c->S.contains(v1) || !c->S.contains(v2) || !c->S.contains(v3) || !c->S.edge(v1, v2) || !c->S.edge(v2, v3) || !c->S.edge(v1, v3))
//            continue;
//          forall(const cell &c2, T.C) {
//            if(!c2->S.contains(v1) || !c2->S.contains(v2) || !c2->S.edge(v1, v2))
//              continue;
//            // First look on one side
//            vertex d = c2->S.nextTo(v1, v2);
//            if(d == c2->S.prevTo(v2, v1) and d != v3) {
//              tri t2;
//              t2.insert(v1);
//              t2.insert(v2);
//              t2.insert(d);
//              t = t2;
//              if(tris.find(t) == tris.end()) {
//                tris.insert(t);
//                done = false;
//              }
//            }
//            // Now look on the other
//            d = c2->S.prevTo(v1, v2);
//            if(d == c2->S.nextTo(v2, v1) and d != v3) {
//              tri t2;
//              t2.insert(v1);
//              t2.insert(v2);
//              t2.insert(d);
//              t = t2;
//              if(tris.find(t) == tris.end()) {
//                tris.insert(t);
//                done = false;
//              }
//            }
//          }
//      }
//    }
//  }
//
//    // Insert vertices between edges in all graphs
//    std::map<std::pair<vertex,vertex>, vertex> splits;   // List of edges to split
//    forall(const tri &t, tris) {
//      subdivideInsert(T, t, splits);
//    }
//
//    // Connect new vertices
//    forall(const tri &t, tris) 
//      subdivideTri(T, t, splits);
//
//  }
//
//  bool SubdivideBisectMesh3D::run(Mesh* mesh, float cellMaxArea)
//  {
//    if(mesh->meshType() != "MDX3D")
//      throw(QString("Cannot subdivide mesh type (%1), must be (MDX3D)").arg(mesh->meshType()));
//
//    CellTissue& T = mesh->tissue();
//    vvGraph& S = mesh->graph();
//
//    const std::vector<vertex>& vs = mesh->activeVertices();
//    bool keepSel = vs.size() != S.size();
//
//    forall(const vertex& v, vs){
//      v->selected = true;
//    }
//
//    std::set<tri> tris; // set of all its (unique) tris
//
//    // go through all unique triangles with area greate the maxArea
//    forall(const cell& c, T.C){
//      forall(const vertex& v, c->S){
//        if(!v->selected) continue;
//        forall(const vertex& n, c->S.neighbors(v)){
//          if(!n->selected) continue;
//          vertex m = c->S.nextTo(v,n);
//          if(!c->S.uniqueTri(v,n,m)) continue;
//          if(!m->selected) continue;
//          // size too big?
//          if(triangleArea(v->pos, n->pos, m->pos) < cellMaxArea) continue;
//          tri t;
//          t.insert(v);
//          t.insert(n);
//          t.insert(m);
//          tris.insert(t);
//        }
//      }
//    }
//
//    subdivideTris(T, tris);
//
//    T.updateAllGraph();
//    T.updGeometry();
//
//    if(!keepSel) {
//      forall(const vertex& v, S)
//        v->selected = false;
//    }
//
//    mesh->updateAll();
//
//    return true;
//  }
//  REGISTER_PROCESS(SubdivideBisectMesh3D);
//  
  bool ScaleMesh::run(CCStructure &cs, CCIndexDataAttr &indexAttr, double scaleX, double scaleY, double scaleZ)
  {
    if(scaleX < 0 or scaleY < 0 or scaleZ < 0)
      throw QString("%1: Scale cannot be negative");
  
    // Copy to temp graph
    for(CCIndex v : activeVertices(cs, indexAttr)) {
      Point3d &pos = indexAttr[v].pos;
      if(scaleX != 1.0)
        pos.x() *= scaleX;
      if(scaleY != 1.0)
        pos.y() *= scaleY;
      if(scaleZ != 1.0)
        pos.z() *= scaleZ;
    }
    return true;
  }
  REGISTER_PROCESS(ScaleMesh);

//  
//  bool MeshDeleteValence::run(Mesh* mesh, int startValence, int endValence)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot delete vertices in mesh type (%1), must be (MDXM)")
//                                                                     .arg(mesh->meshType()));
//
//    if(startValence < 0 or endValence < 0)
//      return setErrorMessage("Start and end valence must be >= 0.");
//    vvGraph& S = mesh->graph();
//    std::vector<vertex> D;
//    forall(const vertex& v, S)
//      if(S.valence(v) >= (size_t)startValence and S.valence(v) <= (size_t)endValence)
//        D.push_back(v);
//    forall(const vertex& v, D)
//      S.erase(v);
//  
//    setStatus(D.size() << " vertex(es) deleted, total vertices:" << S.size());
//
//    mesh->updateAll();
//
//    return true;
//  }
//  REGISTER_PROCESS(MeshDeleteValence);
//
//  bool MergeVertices::run(Mesh* mesh)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot merge vertices in mesh type (%1), must be (MDXM)")
//                                                                     .arg(mesh->meshType()));
//  
//    vvGraph& S = mesh->graph();
//    const VtxVec &vs = mesh->activeVertices();
//    if(vs.size() < 2)
//      throw QString("Error, Must select at least two connected vertices.");
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Do not know how to delete vertices in mesh type %1").arg(mesh->meshType()));
//
//    progressStart("Merging vertices", vs.size() - 1); // Correct if only one area
//    int count = mergeVertices(S, vs);
//    if(count > 0)
//      mesh->updateAll();
//  
//    mdxInfo << count << " vertices deleted from mesh" << endl;
//    return true;
//  }
//  REGISTER_PROCESS(MergeVertices);
//  bool DeleteEdge::run(Mesh* mesh)
//  {
//    if(mesh->meshType() != "MDXM")
//      throw(QString("Cannot delete edges in mesh type (%1), must be (MDXM)")
//                                                                     .arg(mesh->meshType()));
//    vvGraph& S = mesh->graph();
//    const VtxVec &vs = mesh->activeVertices();
//    if(vs.size() != 2)
//      throw QString("Error, Must have exactly two vertices selected.");
//  
//    vertex v1 = vs[0], v2 = vs[1];
//    if(!S.edge(v1, v2))
//      throw QString("Error, Vertices have no edge between them.");
//  
//    S.eraseEdge(v1, v2);
//    S.eraseEdge(v2, v1);
//  
//    mesh->updateAll();
//    mdxInfo << "Edge deleted from mesh" << endl;
//    return true;
//  }
//  REGISTER_PROCESS(DeleteEdge);
//  
//  bool MeshDeleteSelection::run(Mesh* m)
//  {
//    m->correctSelection(false);
//    if(m->meshType() == "MDXM" or m->meshType() == "MDXC") {
//      vvGraph& S = m->graph();
//      size_t n = S.size();
//      size_t count = 0;
//      for(size_t i = 0; i < n; ++i) {
//        size_t j = n - i - 1;
//        if(S[j]->selected) {
//          S.erase(S.begin() + j);
//          count++;
//        }
//      }
//      mdxInfo << count << " vertices deleted, " << S.size() 
//                                << " vertices remaining in mesh" << endl;
//    } else if(m->meshType() == "MDX2D") {
//      CellTissue &T = m->tissue();
//      CellSet D;
//      forall(const vertex &v, T.S)
//        if(v->selected) {
//          cell c = T.getCell(v);
//          if(c)
//            D.insert(c);
//        }
//      forall(const cell &c, D)
//        T.deleteCell(c);
//      mdxInfo << D.size() << " cells deleted, " << T.C.size() 
//                                  << " cells remaining in mesh" << endl;
//    } else
//      throw(QString("Cannot delete vertices in mesh type (%1), must be (MDXM, MDXC, MDX2D)")
//                                                                     .arg(m->meshType()));
//    m->updateAll();
//  
//    return true;
//  }
//  REGISTER_PROCESS(MeshDeleteSelection);
//  
//  bool MeshKeepVertices::run(Mesh* mesh, bool keep)
//  {
//    const std::vector<vertex>& vs = mesh->activeVertices();
//  
//    forall(const vertex& v, vs)
//      if(keep)
//        v->type = 'l';
//      else if(v->type == 'l')
//        v->type = 'j';
//  
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(MeshKeepVertices);
}
