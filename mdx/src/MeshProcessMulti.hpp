//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_MULTI_HPP
#define MESH_PROCESS_MULTI_HPP

#include <Process.hpp>

namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{
  /**
   * \class CopySwapMeshes <StackProcess.hpp>
   *
   * Copy or swap meshes between stack 1 and 2.
   */
  class mdxBase_EXPORT CopySwapMeshes : public Process 
  {
  public:
    CopySwapMeshes(const Process& process) : Process(process) 
    {
      setName("Mesh/MultiMesh/Swap or Copy Mesh 1 and 2");
      setDesc("Copy or Swap Mesh 1 and 2");
      setIcon(QIcon(":/images/CopySwapStacks.png"));

      addParm("Action", "Direction of copy or swap", "1 -> 2",
              QStringList() << "1 -> 2" << "1 <- 2" << "1 <-> 2");
    }
  
    bool initialize(QWidget *parent)
    {
      bool res = run(parm("Action"));
      return res;
    }
    bool run(const QString& actionStr);
  };
  ///@}
}

#endif
