//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CC_INDEX_DATA_HPP
#define CC_INDEX_DATA_HPP

/**
 * \file CCIndexData.hpp
 *
 * This file contains the definition of a vertex.
 */

#include <Config.hpp>
#include <Geometry.hpp>
#include <stdint.h>
#include <Colors.hpp>

namespace mdx
{
  /**
   * \class CCIndexData CCIndexData.hpp <CCIndexData.hpp>
   *
   * This class defines the properties of a cell in a cell complex
   */
  class CCIndexData
  {
  public:
    Point3d pos;     ///< Position in space
    Point3d nrml;    ///< Normal vector
    double measure;  ///< Length / area / volume

    double extra;    ///< Used to be signal
    int label;       ///< Which cell does element belong to?

    bool selected;   ///< Is cell selected?

    // Constructor, set initial values
    CCIndexData() 
      : pos(0.0, 0.0, 0.0), nrml(0.0, 0.0, 1.0), measure(0), label(0), selected(false) {}

    bool operator==(const CCIndexData &c) const 
    { 
      return this->pos == c.pos and this->nrml == c.nrml and this->measure == c.measure
        and this->label == c.label and this->selected == c.selected;
    }

    ~CCIndexData() {}
  };
}

#endif
