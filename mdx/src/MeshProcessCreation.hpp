//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_CREATION_HPP
#define MESH_PROCESS_CREATION_HPP

#include <Process.hpp>
#include <Polygonizer.hpp>

namespace mdx
{
  // Get voxel from the stack at a given position
  int getStackData(const Stack* stack, const Store* store, Point3d &p);
  
  ///\addtogroup MeshProcess
  ///@{
  /**
   * \class ExtrudeFaces ProcessCreation.hpp <MeshProcessCreation.hpp>
   *
   * Extrude faces of a 2D mesh to create a 3D one.
   */
  class ExtrudeFaces : public Process
  {
  public:
    ExtrudeFaces(const Process &_proc) : Process(_proc) 
    {
      setName("Mesh/Creation/Extrude Faces");
      setDesc("Extrude the faces of cell complex to make 3D volumes");
      setIcon(QIcon(":/images/CCDiffusion.png"));

      addParm("Complex Name Out", "Name of output cell complex to create", "");
      addParm("Distance", "Distance to extrude", "1.0");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();

      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw(QString("No current cell complex"));
      CCStructure &csIn = mesh->ccStructure(ccName);
      if(csIn.faces().size() == 0) 
        throw(QString("Cell complex must have faces"));

      QString ccNameOut = parm("Complex Name Out");
      if(ccNameOut.isEmpty())
        ccNameOut = ccName + " Extruded";
      CCStructure &csOut = mesh->ccStructure(ccNameOut);

      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      bool result = run(csIn, csOut, indexAttr, parm("Distance").toDouble());

      mesh->drawParms(ccNameOut).setGroupVisible("Faces", false);
      mesh->drawParms(ccNameOut).setGroupVisible("Volumes", true);

      mesh->updateAll(ccNameOut);
      mesh->setCCName(ccNameOut);
      
      return result;
    }

    bool run(CCStructure &cs, CCStructure &csOut, CCIndexDataAttr &indexAttr, double Distance);
  };

  class ExtrudeTrianglesParallel : public Process
  {
  public:
    ExtrudeTrianglesParallel(const Process &_proc) : Process(_proc) 
    {
      setName("Mesh/Creation/Extrude Triangles Parallel");
      setDesc("Extrude the faces of cell complex to make 3D volumes where the top "
              "and bottom layer triangles are parallel.\n" 
              "The top layer is copied and translated along the normal of the "
              "selected vertex to create the bottom layer. Then a scaling factor "
              "is applied using the selected vertex as origin.\n"
              "Requires a selected vertex which represents the center of the mesh.");
      setIcon(QIcon(":/images/CCDiffusion.png"));

      addParm("Complex Name", "Name of cell complex to create; will be replaced if it already exists!", "");
      addParm("Complex Name Out", "Name of output cell complex to create", "");
      addParm("Distance", "Distance to extrude", "1.0");
      addParm("Scaling", "Scaling of bottom layer", "1.0");
    }

    bool initialize(QWidget *parent);
    bool run();

  private:
    Mesh *mesh;
    CCStructure *cs;

    QString ccName;
    QString ccNameOut;
    double Distance;
    double Scaling;
  };

  /**
   * \class MarchingCubesSurface ProcessCreation.hpp <MeshProcessCreation.hpp>
   *
   * Find the surface of a volume defined by an iso-surface on the image
   * intensity using a variation on the Marching Cube method.
   */
  class mdxBase_EXPORT MarchingCubesSurface : public Process, public ImplicitFunction 
  {
  public:
    MarchingCubesSurface(const Process& process) : Process(process) 
    {
      setName("Mesh/Creation/Marching Cubes Surface");
      setDesc("Extract surface mesh with marching cubes. "
              "A threshold to 0 will interpret the image as binary.");
      setIcon(QIcon(":/images/MarchCubes.png"));

      addParm("Cube Size", "Size for the marching cubes in microns.", "5.0");
      addParm("Threshold", "Minimal signal used for surface extraction.", "5000");
    }
  
    bool run()
    {
      Stack *stack = currentStack();
      if(!stack)
        throw QString("%1::run No current stack").arg(name());
      Store *store = stack->currentStore();
      if(!store or store->empty())
        throw QString("%1::run No current store").arg(name());
      Mesh* mesh = currentMesh();

      QString ccName = "Surface";
      bool result = run(*mesh, *store, ccName, parm("Cube Size").toDouble(), parm("Threshold").toInt());
      mesh->drawParms(ccName).setGroupVisible("Faces", true);
      mesh->updateAll(ccName);
      mesh->setCCName(ccName);
      return result;
    }
    bool run(Mesh &mesh, const Store &store, const QString &ccName, double cubeSize, int threshold);

    bool eval(Point3d p);
  
  private:
    int _threshold;
    const Stack* _stack;
    const Store* _store;
  };
  
  /**
   * \class CuttingSurfMesh ProcessCreation.hpp <MeshProcessCreation.hpp>
   *
   * Create a surface matching the current cutting surface, whether it's a
   * plane or a Bezier surface.
   */
  class mdxBase_EXPORT CuttingSurfMesh : public Process 
  {
  public:
    CuttingSurfMesh(const Process& process) : Process(process) 
    {
      setName("Mesh/Creation/Mesh Cutting Surface");
      setDesc("Make mesh from cutting surface");
      setIcon(QIcon(":/images/MakePlane.png"));
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw(QString("No current mesh"));
      CuttingSurface *cutSurf = cuttingSurface();
      if(!cutSurf)
        throw(QString("No active cutting surface"));

      QString ccName = "Surface";
      bool result = run(*mesh, *cutSurf, ccName);
      mesh->drawParms(ccName).setGroupVisible("Faces", true);
      mesh->updateAll(ccName);
      mesh->setCCName(ccName);
      if(result)
        cutSurf->hide();
      return result;
    }
    bool run(Mesh &mesh, CuttingSurface &cutSurf, const QString &ccName);
  };
//
//  /**
//   * \class VoxelFaceMesh ProcessCreation.hpp <MeshProcessCreation.hpp>
//   *
//   * Create a mesh by extracting the faces from the Voxels.
//   */
//  class mdxBase_EXPORT VoxelFaceMesh : public Process 
//  {
//  public:
//    VoxelFaceMesh(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms)
//    {
//      return run(currentStack(), currentStack()->currentStore(), currentMesh());
//    }
//  
//    bool run(Stack *stack, Store *store, Mesh *mesh);
//  
//      setName("Mesh/Creation/Voxel Face Mesh");
//      setDesc("Extract a mesh from the faces of the voxels.");
//    QStringList parmNames() const { return QStringList());
//    QStringList parmDescs() const { return QStringList());
//      setIcon(QIcon(":/images/MakePlane.png"));
//  };
  ///@}
}

#endif
