//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

// Surface class
#ifndef MDX_PROCESS_POLAR_SURFACE_HPP
#define MDX_PROCESS_POLAR_SURFACE_HPP

#include <Process.hpp>
#include <PolarSurface.hpp>
#include <Function.hpp>
#include <MDXProcessTissue.hpp>

namespace mdx
{
  // Process to hold polar surface parameters
  class mdxBase_EXPORT PolarSurface2DParms : public Process, public PolarSurface2D
  {
  public:
    // Parameters come from PolarSurface2D
    PolarSurface2DParms(const Process &process) : Process(process) 
    {
      setName("11 Polar Surface");
      setDesc("Parameters for Polar Surface");
      setIcon(QIcon(":/images/Parameters.png"));
    }

    // Functions for Gui

    // Make base initialize available, RSS maybe rename? 
    using PolarSurface2D::initialize;
    using Process::initialize;
    // Initialize
    bool initialize(QWidget *parent);

    // Don't do anything, process is only for parms
    bool step() { return false; }

    // Return surface
    PolarSurface2D &polarSurface() { return *this; }

    // Return tissue
    CellTissue &tissue() 
    {
      if(!cellTissueProcess)
       throw(QString("PolarSurface2DParms::tissue cellTissueProcess is null"));
      else
        return cellTissueProcess->tissue(); 
    }

  private:
    //PolarSurface2D polarSurface2D;    // Apex surface

    // Attributes
    CCIndexDataAttr *indexAttr;
    PolarSurface2D::PolarAttr *pData;

    // Tissue parms process
    CellTissueProcess *cellTissueProcess;
  };

  class mdxBase_EXPORT PolarSurface2DGrowth : public Process 
  {
  public:
    PolarSurface2DGrowth(const Process &process) : Process(process) 
    {
       setName("04 Polar Surface Growth");
       setDesc("Growing polar surface");
       setIcon(QIcon(":/images/CellApex.png"));

       addParm("Dt", "Growth timestep", "0.1");
       addParm("Cell Kill", "Distance from the tip to kill cell", "80.0");
       addParm("Surface Process","Process to hold surface parameters", "11 Polar Surface"); 
    }

    // Initialize the surface
    bool initialize(QWidget *parent = 0);

    // Run a step of the growth
    bool step();

    // Read parameters
		bool processParms();

    // Return surface
    PolarSurface2D &polarSurface() 
    { 
      if(!polarSurface2D)
        throw(QString(" PolarSurface2DGrowth::polarSurface Polar Surface is null"));
      else
        return *polarSurface2D; 
    }

    // Return tissue
    CellTissue &tissue() 
    {
      if(!surfaceTissue)
       throw(QString("PolarSurface2DGrowth::tissue Surface Tissue is null"));
      else
        return *surfaceTissue; 
    }

  private:
		// Mesh object
		Mesh *mesh = 0;         // Current mesh
    CellTissue *surfaceTissue = 0;    // Cellular tissue
    PolarSurface2D *polarSurface2D = 0;

    // Attributes
    CCIndexDataAttr *indexAttr;
    PolarSurface2D::PolarAttr *pData;

    // PolarSurface parms process
		QString SurfaceProcessName;
    PolarSurface2DParms *surfaceProcess;

		// Model parameters from GUI
    double Dt;                 // Timestep
    double CellKill;           // Arclength to kill cells at

    // Define all global data you want to save in the mesh in the attributes 
    double &time() 
    { 
      return mesh->attributes().attrMap<QString, double>("PolarSurface Time")["Time"]; 
    }
  };

  class mdxBase_EXPORT PolarSurface2DInitialCell : public Process 
  {
  public:
    PolarSurface2DInitialCell(const Process &process) : Process(process) 
    {
      setName("05 Initial Cell");
      setDesc("Create initial cell for the surface");
      setIcon(QIcon(":/images/InitialCell.png"));
      addParm("Initial Walls", "Initial number of cell walls", "6");
      addParm("Initial Size", "Size of initial cell", "10.0");
      addParm("Surface Process", "Process to hold surface process", "11 Polar Surface");
    }
    // Initialize
    bool initialize(QWidget *parent = 0);

    // Process parameters
    bool processParms();

    // Run a step of the growth
    bool step();

    // Return surface
    PolarSurface2D &polarSurface() 
    { 
      if(!polarSurface2D)
        throw(QString(" PolarSurface2DInitialCell::polarSurface Polar Surface is null"));
      else
        return *polarSurface2D; 
    }

  private:
		// Mesh object
		Mesh *mesh = 0;         // Current mesh
    CellTissue *tissue = 0;    // Cellular tissue
    PolarSurface2D *polarSurface2D = 0;

    // Attributes
    CCIndexDataAttr *indexAttr;
    PolarSurface2D::PolarAttr *pData;

    // PolarSurface parms process
		QString SurfaceProcessName;
    PolarSurface2DParms *surfaceProcess;

		// Parameters from GUI
    int cellInitWalls;         // Initial cell number of walls
    double cellInitSize;       // Initial cell size
  };
}  
#endif

