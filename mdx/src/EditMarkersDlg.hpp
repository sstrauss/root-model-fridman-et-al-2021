//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef EDITMARKERSDLG_HPP
#define EDITMARKERSDLG_HPP

#include <Config.hpp>

#include <TransferFunction.hpp>
#include <TransferMarkers.hpp>

#include <QDialog>
#include <vector>

#include <ui_EditMarkersDlg.h>

namespace mdx
{
  class mdx_EXPORT EditMarkersDlg : public QDialog {
    Q_OBJECT
  public:
    typedef TransferFunction::Colorf Colorf;
    typedef TransferFunction::value_list value_list;
    typedef TransferFunction::Interpolation Interpolation;
    EditMarkersDlg(const TransferFunction& fct, QWidget* parent = 0, Qt::WindowFlags f = 0);
  
    TransferFunction getFunction() const {
      return function;
    }
  
    value_list pointList() const;
  
  public slots:
    void on_addMarker_clicked();
    void on_removeMarker_clicked();
    void on_rgbaMode_toggled(bool on);
    void on_hsvaMode_toggled(bool on);
    void spreadMarkers();
  
  protected:
    TransferFunction function;
    Ui::EditMarkersDlg ui;
    TransferMarkerModel* model;
    MarkerColorDelegate* delegate;
    QPushButton* spread_button;
  };
}
#endif
