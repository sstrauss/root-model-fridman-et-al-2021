//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef MESH_PROCESS_MISC_HPP
#define MESH_PROCESS_MISC_HPP

/**
 * \file MeshProcessMisc.hpp
 *
 * Miscellaneous processes for cell complexes
 */

#include <Process.hpp>
#include <CCVerify.hpp>
#include <CCUtils.hpp>

namespace mdx
{
  // Process that creates a new cell complex to draw normals and neighborhoods
  class mdxBase_EXPORT DebugDraw : public Process 
  {
  public:
    enum ParmNames { pSourceCC, pOutputCC, pDrawNormals, pNormalSize, pDrawNhbds, pNhbdSize, pNhbdOffset, pNumParms };

    DebugDraw(const Process &process) : Process(process) 
    {
      setName("Mesh/Misc/Debug Draw");
      setDesc("Draw debugging information for cell complexes.");
      setIcon(QIcon(":/images/Debug.png"));

      addParm("Source CC", "Name of source cell complex", "");
      addParm("Output CC", "Name of output cell complex", "Debug Normals");
      addParm("Draw Normals", "Draw normal vectors", "Yes", booleanChoice());
      addParm("Normal Size", "Amount to scale normals", "1.0");
      addParm("Draw Nhbds", "Draw neighborhood vectors, with increasing length", "Yes", booleanChoice());
      addParm("Nhbd Size", "Amount to scale neighborhood vectors", "0.2");
      addParm("Nhbd Offset", "Offset for neighborhood vectors", "0.01");
    }
    bool initialize(QWidget* parent);
  
    bool run() { return run(currentMesh()); }
    bool run(Mesh *mesh);

  private:
    // Parameters
    QString SourceCC;
    QString OutputCC;
    bool DrawNormals;
    double NormalSize;
    bool DrawNhbds;
    double NhbdSize;
    double NhbdOffset;
  };

  // Process to calculate normals
  class mdxBase_EXPORT UpdateGeometry : public Process 
  {
  public:
    UpdateGeometry(const Process &process) : Process(process) 
    {
      setName("Mesh/Misc/Update Geometry");
      setDesc("Recalculate vertex and face normals, face measure, and position");
      setIcon(QIcon(":/images/Process.png"));
    }

    // Run the process
    bool run()
    { 
      Mesh *mesh = currentMesh();
      if(mesh->ccName().isEmpty())
        throw(QString("No current mesh"));

      CCStructure &cs = mesh->ccStructure(mesh->ccName());

      bool result = run(cs, mesh->indexAttr());
      mesh->updatePositions(mesh->ccName());

      return result;
    }
    bool run(CCStructure &cs, CCIndexDataAttr &indexAttr);
  };

  // Process to check cell complex integrity
  class mdxBase_EXPORT CellComplexVerify : public Process 
  {
  public:
    CellComplexVerify(const Process &process) : Process(process) 
    {
      setName("Mesh/Misc/Verify Cell Complex");
      setDesc("Run checks to verify a cell complex");
      setIcon(QIcon(":/images/Process.png"));
    }

    // Run the process
    bool run();
  };

  // Process to repair a cell complex's boundary.
  class mdxBase_EXPORT CellComplexRepair : public Process 
  {
  public:
    CellComplexRepair(const Process &process) : Process(process) 
    {
      setName("Mesh/Misc/Repair Cell Complex");
      setDesc("Rebuilds the boundary of a cell complex");
      setIcon(QIcon(":/images/Process.png"));
    }

    // Run the process
    bool run();
  };

  // Process to ensure that all cells of maximal dimension
  // are positively oriented with respect to TOP.
  class mdxBase_EXPORT CellComplexGlobalOrient : public Process
  {
  public:
    CellComplexGlobalOrient(const Process &process) : Process(process)
    {
      setName("Mesh/Misc/Fix Global Orientation");
      setDesc("Ensure that all cells have positive global orientation");
      setIcon(QIcon(":/images/Process.png"));
    }

    bool run();
  };

  // Process to output flip table
  class mdxBase_EXPORT PrintFlipTable : public Process 
  {
  public:
    PrintFlipTable(const Process &process) : Process(process) 
    {
      setName("Mesh/Misc/Print Flip Table");
      setDesc("Print a cell complex's flip table");
      setIcon(QIcon(":/images/Process.png"));

      addParm("File Name", "Name of file to write flip table", "FlipTable.txt");
    }

    // Run the process
    bool run();
  };

  /// Reduce dimension of a cell complex from 3 to 2
  class mdxBase_EXPORT ReduceDimension : public Process 
  {
  public:
    ReduceDimension(const Process &process) : Process(process) 
    {
      setName("Mesh/Misc/Reduce Dimension");
      setDesc("Reduce the dimension of a cell complex from 3 to 2");
      setIcon(QIcon(":/images/Process.png"));
    }

    // Run the process
    bool run();
  };
}

#endif

