/*
 * The Cell Complex Framework: UIntIndex and Manager
 * Brendan Lane 2016
 */

#include <CCF.hpp>

namespace mdx {
/* UIntIndex pseudocell definitions */
template<> const CCIndex CCIndex::UNDEF(0);
template<> const CCIndex CCIndex::Q(1);
template<> const CCIndex CCIndex::TOP(2);
template<> const CCIndex CCIndex::BOTTOM(3);
template<> const CCIndex CCIndex::INFTY(4);

CCIndexManager CCIndexFactory;
}
