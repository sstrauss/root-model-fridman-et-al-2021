//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_CANVAS_HPP
#define STACK_PROCESS_CANVAS_HPP

/**
 * \file StackProcessCanvas.hpp
 * Contains stack processes that manipulate the stack size
 */
#include <Process.hpp>

namespace mdx 
{
  ///\addtogroup StackProcess
  ///@{

  /**
   * \class ClipStack StackProcessCanvas.hpp <StackProcessCanvas.hpp>
   *
   * Apply the active clipping planes to the current stack
   */
  class mdxBase_EXPORT ClipStack : public Process 
  {
  public:
    ClipStack(const Process& process) : Process(process) 
    {
      setName("Stack/Canvas/Clip Stack");
      setDesc("Trim stack to clipping planes");
      setIcon(QIcon(":/images/ClipStack.png"));

      addParm("Trim Canvas", "Use AutoTrim to reduce canvas size after clipping the stack.", "No", booleanChoice());
      addParm("Labels", "Trim whole labels", "No", booleanChoice());
    }
    bool run();
    bool run(const Store* input, Store* output, bool trimStack, bool labels);
  };
  
  /**
   * \class ReverseStack StackProcessCanvas.hpp <StackProcessCanvas.hpp>
   *
   * Reverse the direction of the selected axes.
   */
  class mdxBase_EXPORT ReverseStack : public Process 
  {
  public:
    ReverseStack(const Process& process) : Process(process) 
    {
      setName("Stack/Canvas/Reverse Axes");
      setDesc("Reverse the direction of the selected axes.\nPress the A-key to display the stack axes.");
      setIcon(QIcon(":/images/Resize.png"));

      addParm("X", "Reverse the X axis", "No", booleanChoice());
      addParm("Y", "Reverse the Y axis", "No", booleanChoice());
      addParm("Z", "Reverse the Z axis", "Yes", booleanChoice());
    }
    bool run();
    bool run(Store* output, const Store* input, bool x, bool y, bool z);
  };
  
  /**
   * \class ChangeVoxelSize StackProcessCanvas.hpp <StackProcessCanvas.hpp>
   *
   * Change the size of the stack's voxel, without changing the data itself.
   */
  class mdxBase_EXPORT ChangeVoxelSize : public Process 
  {
  public:
    ChangeVoxelSize(const Process& process) : Process(process) 
    {
      setName("Stack/Canvas/Change Voxel Size");
      setDesc("Change the size of a voxel (i.e. doesn't change the data)");
      setIcon(QIcon(":/images/Resize.png"));

      addParm("X", "Size in X axis in microns", "1.0");
      addParm("Y", "Size in Y axis in microns", "1.0");
      addParm("Z", "Size in Z axis in microns", "1.0");
    }
    bool run();
    bool run(Stack* stack, Point3d nv);
  };
  
  /**
   * \class ResizeCanvas StackProcessCanvas.hpp <StackProcessCanvas.hpp>
   *
   * Resize the stack to add or remove voxels.
   */
  class mdxBase_EXPORT ResizeCanvas : public Process 
  {
  public:
    ResizeCanvas(const Process& process) : Process(process) 
    {
      setName("Stack/Canvas/Resize Canvas");
      setDesc("Resize the stack to add or remove voxels.\nMake sure BBox is checked on before running.");
      setIcon(QIcon(":/images/Resize.png"));

      addParm("Relative", "If false X, Y and Z are final size, otherwise will add/subtract\n" 
                          "0 means do not scale that dimension", "Yes", booleanChoice());
      addParm("Center", "New canvas centered as the old one, or else use the bottom\n"
                        "left corner as reference.", "Yes", booleanChoice());
      addParm("X", "Size in X direction", "0");
      addParm("Y", "Size in Y direction", "0");
      addParm("Z", "Size in Z direction", "0");
    }
    bool run();
    bool run(Stack* stack, bool isRelative, bool center, Point3i ds);
  };
  
  /**
   * \class ScaleStack StackProcessCanvas.hpp <StackProcessCanvas.hpp>
   *
   * Scale the stack.
   */
  class mdxBase_EXPORT ScaleStack : public Process 
  {
  public:
    ScaleStack(const Process& process) : Process(process) 
    {
      setName("Stack/Canvas/Scale Stack");
      setDesc("Scale the stack, 0 means don't scale that dimension.");
      setIcon(QIcon(":/images/Scale.png"));

      addParm("Percent", "Scale in percent", "Yes", booleanChoice());
      addParm("X", "Size in X direction", "0.0");
      addParm("Y", "Size in Y direction", "0.0");
      addParm("Z", "Size in Z direction", "0.0");
    }
    bool run();
    bool run(Stack* stack, Point3d newsize, bool percent);
  };
  
  /**
   * \class ShiftStack StackProcessCanvas.hpp <StackProcessCanvas.hpp>
   *
   * Shift main and work stores within the canvas (e.g. the voxels' values are
   * moved within the image)
   */
  class mdxBase_EXPORT ShiftStack : public Process 
  {
  public:
    ShiftStack(const Process& process) : Process(process) 
    {
      setName("Stack/Canvas/Shift Stack");
      setDesc("Shift both stores of the stack to within the canvas.");
      setIcon(QIcon(":/images/Shift.png"));

      addParm("Origin", "Relative to the origin", "No", booleanChoice());
      addParm("X", "Amount in X direction", "0");
      addParm("Y", "Amount in Y direction", "0");
      addParm("Z", "Amount in Z direction", "0");
    }
  
    bool run();
    bool run(Stack* stack, bool origin, Point3i ds);
  };

  /**
   * \class AutoTrim StackProcessCanvas.hpp <StackProcessCanvas.hpp>
   *
   * Trim stack boundary box (BBox) to keep only non-empty part. A voxel is
   * considered empty if its intensity is less or equal to the threshold.
   *
   * \ingroup StackProcess
   */
  class AutoTrim : public Process 
  {
  public:
    AutoTrim(const Process& process) : Process(process) 
    {
      setName("Stack/Canvas/AutoTrim");
      setDesc("Trim stack boundary box (BBox) to keep only non-empty part.\n"
             "A voxel is considered empty if its intensity is less or equal to the threshold.");
      setIcon(QIcon(":/images/AutoTrim.png"));

      addParm("Threshold", "Voxels below this threshold are considered empty.", "0");
    }
    bool run();
    bool run(Stack* stack, Store* store, int threshold);
  };

  ///@}
}

#endif
