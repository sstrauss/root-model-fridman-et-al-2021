//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <MDXProcessTissue.hpp>
#include <MDXProcessCellDivide.hpp>

// Process to perform cell division.
namespace mdx
{
  // Divide this so it can be called on any tissue
  bool CellTissueCell2dDivide::initialize(QWidget* parent)
  {
    Mesh *mesh = currentMesh();
    if(!mesh)
      throw(QString("CellTissueCell2dDivide::initialize No current mesh"));

    processParms();

    // Initialize the tissue
    if(!getProcess(TissueParmsProcessName, cellTissueProcess))
      throw(QString("CellTissueCell2dDivide::initialize: Unable to create CellTissueProcess: %1").
            arg(TissueParmsProcessName));
    cellTissueProcess->initialize(*mesh);
    tissue = &cellTissueProcess->tissue();
    if(!tissue)
      throw(QString("CellTissueCell2dDivide::initialize: Unable to get cell tissue"));

    TissueName = cellTissueProcess->parm("Tissue");
    TissueDualName = cellTissueProcess->parm("Tissue Dual");

    // Use the basic MDX data for subdivision, followed by the CellTissue subdivision.
    subdiv = MDXSubdivide(*mesh);
    subdiv.nextSubdivide = tissue->subdivider();

    return true;
  }

  bool CellTissueCell2dDivide::processParms()
  {
    // Process parameters
    CellMaxArea = parm("Cell Max Area").toDouble();
    Verbose = stringToBool(parm("Verbose"));
    TissueParmsProcessName = parm("Tissue Process");

    return true;
  }

  // Run a step of cell division
  bool CellTissueCell2dDivide::step(Mesh *mesh, Subdivide *subdiv)
  {
    if(!mesh)
      throw(QString("CellTissueCell2dDivide.step: Mesh not initialized"));
    if(!tissue)
      throw(QString("CellTissueCell2dDivide.step: Tissue not initialized"));

    CCIndexDataAttr &indexAttr = tissue->indexAttr();

    // Check if we need to use selection
    // implement later

    // Find cells to divide
    std::vector<CCIndex> toDivide;
    forall(CCIndex cell, tissue->cellStructure().faces()) {
      double area = indexAttr[cell].measure;
      if(area > CellMaxArea)
        toDivide.push_back(cell);
    }
    if(toDivide.empty())
      return false;
    
    for(const CCIndex &cell : toDivide) {
      if(Verbose)
        mdxInfo << "Dividing cell (" << to_string(cell).c_str() << "), with area "
                << indexAttr[cell].measure << endl;
      divideCell2d(tissue->cellStructure(), indexAttr, cell, *this,
                   subdiv, Cell2dDivideParms::SHORTEST_WALL_THROUGH_CENTROID);
    }
    tissue->createDual();
    tissue->updateGeometry();

    // Update mesh points, edges, surfaces
    mesh->updateAll(TissueName);
    mesh->updateAll(TissueDualName);   

    return true; 
  }
}

