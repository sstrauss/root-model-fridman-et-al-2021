//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <ToolsProcessSystem.hpp>
#include <MeshProcessSystem.hpp>
#include <StackProcessSystem.hpp>
#include <Dir.hpp>

#include <QFileDialog>

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
#  define FileDialogOptions 0
#endif
//
//using namespace qglviewer;
//
//#include <QTextStream>
//#include <stdio.h>

static QTextStream err(stderr);

namespace mdx 
{
  bool SaveViewFile::run() 
  {
    return run(parm("File Name"));
  }
  
  bool SaveViewFile::run(const QString& fileName)
  {
    if(fileName.isEmpty()) {
      setErrorMessage("Filename is empty. Cannot save view file.");
      return false;
    }
    actingFile(fileName, true);
    bool res = saveView(fileName);
    setStatus(QString("Saving view to file %1").arg(fileName));
    if(!res) {
      setErrorMessage("Error while saving view file.");
      return false;
    }
    return true;
  }
  
  bool SaveViewFile::initialize(QWidget* parent)
  {
    QString fileName(parm("File Name"));

    // Get the file name
    if(parent)
      fileName = QFileDialog::getSaveFileName(parent, QString("Select view file"), fileName,
                                     "MorphoDynamX View files (*.mdxv);;All files (*.*)", 0, FileDialogOptions);

    if(fileName.isEmpty())
      return false;
    if(fileName.endsWith(".mgxv", Qt::CaseInsensitive))
		  fileName[fileName.length() - 3] = 'd';
		else if(!fileName.endsWith(".mdxv", Qt::CaseInsensitive))
      fileName += ".mdxv";

    setParm("File Name", fileName);
    return true;
  }
  REGISTER_PROCESS(SaveViewFile);
  
  void QuickSaveFile::saveMesh(int meshNr)
  {

    QString fileName = getMesh(meshNr)->file();
    
    if(fileName != ""){
      MeshSave ms(*this);
      ms.run(getMesh(meshNr), fileName, false);
    }

  }

  void QuickSaveFile::saveStore(int stackNr, Store* store, int compressionLevel)
  {

    QString fileName = store->file();
    
    if(fileName != ""){
      StackSave ss(*this);
      ss.run(getStack(stackNr), store, fileName, compressionLevel);
    }

  }

  bool QuickSaveFile::run()
  {
    std::cout << "Quick Save!" << std::endl;

    SaveViewFile saveViewProc(*this);

    // save mdxv file
    saveView(saveViewProc.parm("File Name"));

    saveMesh(0);
    saveMesh(1);
    
    StackSave stackSaveProc(*this);
    int compressionLevel = stackSaveProc.parm("Compression").toInt();

    // RSS all of these pointers should be checked
    saveStore(0, getStack(0)->main(), compressionLevel);
    saveStore(0, getStack(0)->work(), compressionLevel);
    saveStore(1, getStack(1)->main(), compressionLevel);
    saveStore(1, getStack(1)->work(), compressionLevel);

    return true;
  }
  REGISTER_PROCESS(QuickSaveFile);

  class MeshListModel : public QAbstractTableModel {
  public:
    MeshListModel(Process* p, QObject* parent = 0)
      : QAbstractTableModel(parent)
      , proc(p)
    {
      for(int i = 0; i < proc->meshCount(); ++i) {
        names << QString("Mesh%1").arg(i + 1);
        Mesh* m = proc->getMesh(i);
        QString shortFile = stripCurrentDir(m->file());
        selected << not (shortFile.isEmpty());
        files << shortFile;
        transforms << false;
      }
    }

    int rowCount(const QModelIndex& parent = QModelIndex()) const
    {
      if(parent.isValid())
        return 0;
      return names.size();
    }

    int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
      return 4;
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const
    {
      if(role != Qt::DisplayRole)
        return QVariant();
      if(orientation == Qt::Vertical)
        return QVariant();
      switch(section) {
      case 0:
        return "Mesh";
      case 1:
        return "Filename";
      case 2:
        return "Transformed";
      default:
        return QVariant();
      }
    }

    Qt::ItemFlags flags(const QModelIndex& index) const
    {
      if(!index.isValid())
        return Qt::ItemIsEnabled;
      int i = index.row();
      if(i < 0 or i >= names.size())
        return Qt::ItemIsEnabled;
      switch(index.column()) {
      case 0:
        return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable;
      case 1:
        if(selected[i])
          return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
        else
          return 0;
      case 2:
        if(selected[i])
          return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable;
        else
          return 0;
      default:
        return Qt::ItemIsEnabled;
      }
    }

    QString properFile(const QString& fileName)
    {
      if(!fileName.endsWith(".mdxm", Qt::CaseInsensitive))
        return fileName + ".mdxm";
      return fileName;
    }

    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole)
    {
      if(!index.isValid() or index.column() > 3)
        return false;

      int i = index.row();

      if(i < 0 or i >= names.size())
        return false;

      switch(index.column()) {
      case 0:
        if(role != Qt::CheckStateRole)
          return false;
        selected[i] = value.toBool();
        emit dataChanged(this->index(i, 0), this->index(i, 4));
        break;
      case 1:
        if(role != Qt::EditRole)
          return false;
        files[i] = properFile(value.toString());
        emit dataChanged(index, index);
        break;
      case 2:
        if(role != Qt::CheckStateRole)
          return false;
        transforms[i] = value.toBool();
        emit dataChanged(index, index);
        break;
      default:
        return false;
      }
      return true;
    }

    QVariant data(const QModelIndex& index, int role) const
    {
      if(!index.isValid() or index.row() >= names.size() or index.column() > 3)
        return QVariant();
      int i = index.row();
      switch(role) {
      case Qt::CheckStateRole:
        switch(index.column()) {
        case 0:
          return selected[i] ? Qt::Checked : Qt::Unchecked;
        case 2:
          return transforms[i] ? Qt::Checked : Qt::Unchecked;
        case 1:
        default:
          return QVariant();
        }
      case Qt::EditRole:
        if(index.column() == 1)
          return files[i];
        else
          return QVariant();
      case Qt::DisplayRole:
        if(index.column() == 0)
          return names[i];
        else if(index.column() == 1)
          return files[i];
        else
          return QVariant();
      default:
        return QVariant();
      }
    }

    void changeMesh(const QString& name, const QString& file, bool trans)
    {
      int idx = names.indexOf(name);
      if(idx >= 0) {
        files[idx] = file;
        selected[idx] = true;
        transforms[idx] = trans;
        emit dataChanged(index(idx, 0), index(idx, 4));
      }
    }

    QStringList typeList;
    QStringList names;
    QStringList files;
    QList<bool> selected, transforms;

  protected:
    Process* proc;
  };
  
  class StackListModel : public QAbstractTableModel {
  public:
    StackListModel(Process* p, QObject* parent = 0)
      : QAbstractTableModel(parent)
      , proc(p)
    {
      for(int i = 0; i < proc->stackCount(); ++i) {
        Stack* s = proc->getStack(i);
        if(!s)
          continue;
        Store* main = s->main();
        if(main) {
          names << QString("MainStack%1").arg(i + 1);
          QString shortFile = stripCurrentDir(main->file());
          files << properFile(shortFile);
          selected << not shortFile.isEmpty();
        }
        Store* work = s->work();
        if(work) {
          names << QString("WorkStack%1").arg(i + 1);
          QString shortFile = stripCurrentDir(work->file());
          files << properFile(shortFile);
          selected << not shortFile.isEmpty();
        }
      }
    }

    QString properFile(const QString& fileName)
    {
      QString result = fileName;
      QFileInfo fi(fileName);
      QString suf = fi.suffix();
      if(suf == "txt")
        result = fileName.left(result.size() - 3) + "mdxs";
      else if(suf != "inr" and suf != "mdxs")
        result = fileName + ".mdxs";
      return result;
    }

    int rowCount(const QModelIndex& parent = QModelIndex()) const
    {
      if(parent.isValid())
        return 0;
      return names.size();
    }

    int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
      return 2;
    }

    QVariant data(const QModelIndex& index, int role) const
    {
      if(!index.isValid() or index.row() >= names.size() or index.column() > 2)
        return QVariant();
      int i = index.row();
      if(role == Qt::CheckStateRole and index.column() == 0)
        return selected[i] ? Qt::Checked : Qt::Unchecked;
      if(role == Qt::EditRole and index.column() == 1)
        return files[i];
      if(role == Qt::DisplayRole) {
        if(index.column() == 0)
          return names[i];
        if(index.column() == 1)
          return files[i];
      }
      return QVariant();
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const
    {
      if(role != Qt::DisplayRole)
        return QVariant();
      if(orientation == Qt::Vertical)
        return QVariant();
      switch(section) {
      case 0:
        return "Stack";
      case 1:
        return "Filename";
      default:
        return QVariant();
      }
    }

    Qt::ItemFlags flags(const QModelIndex& index) const
    {
      if(!index.isValid())
        return Qt::ItemIsEnabled;
      int i = index.row();
      if(i < 0 or i >= names.size())
        return Qt::ItemIsEnabled;
      switch(index.column()) {
      case 0:
        return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable;
      case 1:
        if(selected[i])
          return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
        else
          return 0;
      default:
        return Qt::ItemIsEnabled;
      }
    }

    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole)
    {
      if(!index.isValid() or index.column() > 1)
        return false;

      int i = index.row();

      if(i < 0 or i >= names.size())
        return false;

      if(index.column() == 0 and role == Qt::CheckStateRole) {
        selected[i] = value.toBool();
        emit dataChanged(this->index(i, 0), this->index(i, 1));
      } else if(index.column() == 1 and role == Qt::EditRole) {
        files[i] = properFile(value.toString());
        emit dataChanged(index, index);
      } else
        return false;

      return true;
    }

    void changeStack(const QString& name, const QString& file)
    {
      int idx = names.indexOf(name);
      if(idx >= 0) {
        files[idx] = file;
        selected[idx] = true;
        emit dataChanged(index(idx, 0), index(idx, 1));
      }
    }

    QStringList names;
    QStringList files;
    QList<bool> selected;

  protected:
    Process* proc;
  };
  
  bool TakeSnapshot::run(QString fileName, double overSampling, int width, int height, int quality,
                         bool expand_frustum)
  {
    return takeSnapshot(fileName, overSampling, width, height, quality, expand_frustum);
  }
  REGISTER_PROCESS(TakeSnapshot);

  bool LoadAllData::loadStore(Stack* stack, Store* store, QStringList& errors)
  {
    if(!store->file().isEmpty()) {
      if(store->file().endsWith(".txt", Qt::CaseInsensitive)) {
        StackImportSeries importStack(*this);
        Point3d step;
        double brightness = 1.f;
        if(!importStack.run(stack, store, step, brightness, store->file())) {
          errors << importStack.errorMessage();
          return false;
        }
      } else {
        StackOpen openStack(*this);
        if(!openStack.run(stack, store, store->file())) {
          errors << openStack.errorMessage();
          return false;
        }
      }
    } else
      store->reset();
    return true;
  }

  bool LoadAllData::run()
  {
    if(!runLoadAll())
      return false;
    for(int i = 0; i < stackCount(); ++i) {
      Stack* s = getStack(i);
      if(s->empty()) {
        s->main()->hide();
        s->work()->hide();
      }
    }
    return true;
  } 
  bool LoadAllData::runLoadAll()
  {
    QStringList errors;
    for(int i = 0; i < stackCount(); ++i) {
      Stack* s = getStack(i);
      Store* main = s->main();
      QString mainFilename = main->file();
      Store* work = s->work();
      QString workFilename = work->file();
      try {
        main->setFile(mainFilename);
        if(!loadStore(s, main, errors))
          main->reset();
      }
      catch(UserCancelException&) {
        main->reset();
        return true;
      }
      try {
        work->setFile(workFilename);
        if(!loadStore(s, work, errors))
          work->reset();
      }
      catch(UserCancelException&) {
        work->reset();
        return true;
      }
      if(main->file().isEmpty() and work->file().isEmpty()) {
        s->setSize(Point3u(0, 0, 0));
        main->reset();
        work->reset();
      }
    }
    MeshLoad loadMesh(*this);
    MeshImport importMesh(*this);
    for(int i = 0; i < meshCount(); ++i) {
      Mesh* mesh = getMesh(i);
      if(!mesh)
        throw(QString("MeshLoad::run Invalid Mesh Id").arg(i));
      if(!mesh->file().isEmpty()) {
        try {
          QString low_fn = mesh->file().toLower();
          if(low_fn.endsWith(".mdxm", Qt::CaseInsensitive) or low_fn.endsWith(".mgxm", Qt::CaseInsensitive)) {
            if(!loadMesh.run(*mesh, mesh->file(), mesh->transformed(), false)) {
              mesh->reset();
              errors << loadMesh.errorMessage();
            }
          } else if(!importMesh.run(*mesh, mesh->file(), "", mesh->transformed(), false)) {
            mesh->reset();
            errors << importMesh.errorMessage();
          }
        }
        catch(UserCancelException&) {
          return true;
        }
      }
    }
    if(!errors.empty())
      setErrorMessage(QString("Errors:\n  *%1").arg(errors.join("\n  *")));

    return true;
  }
  REGISTER_PROCESS(LoadAllData);
  
  bool LoadViewFile::run() 
  {
    return run(parm("File Name"));
  }
  
  bool LoadViewFile::run(QString fileName)
  {
    actingFile(fileName, true);
    bool res = loadView(fileName);
    if(!res) {
      setErrorMessage("Error while loading view file.");
      return false;
    }
    LoadAllData loaddata(*this);
    res = loaddata.run();
    if(!res)
      setErrorMessage(loaddata.errorMessage());
    else if(fileName.size() > 0)
      setStatus(QString("Loaded view file: %1.").arg(fileName));

    return res;
  }
  
  bool LoadViewFile::initialize(QWidget* parent)
  {
    QString fileName = parm("File Name");
  
    // Get the file name
    if(parent) {
      fileName = QFileDialog::getOpenFileName(parent, QString("Select view file"), fileName,
                    "MorphoDynamX View files (*.mdxv);;MorphoGraphX View files (*.mgxv);;All files (*.*)", 0, FileDialogOptions);
  
      if(fileName.isEmpty())
        return false;
  
      setParm("File Name", fileName);
    }
    return true;
  }
  REGISTER_PROCESS(LoadViewFile);
}
