//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "MorphoViewer.hpp"

#include "cuda/CudaExport.hpp"
#include "CutSurf.hpp"
#include "Dir.hpp"
#include "ImageData.hpp"
#include "Information.hpp"
#include "MorphoDynamX.hpp"
#include "Process.hpp"

#include <iostream>
#include <QDomElement>
#include <QFileInfo>
#include <QGLFormat>
#include <QGLFramebufferObjectFormat>
#include <QGLFramebufferObjectFormat>
#include <QGLContext>
#include <QImage>
#include <QShowEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <string>

#ifdef EPSILON
#  undef EPSILON
#endif
#define EPSILON 1e-3

using namespace mdx;
using namespace qglviewer;

MDXCameraFrame::MDXCameraFrame() : qglviewer::ManipulatedCameraFrame(), _zoom(0) {}

void MDXCameraFrame::wheelEvent(QWheelEvent* const event, Camera* const camera)
{
  const double wheelSensitivityCoef = 8e-4;
  switch(action_) {
  case QGLViewer::ZOOM:
    _zoom -= wheelSensitivity() * event->delta() * wheelSensitivityCoef;
    if(DEBUG)
      mdxInfo << "  zoom = " << _zoom << endl;
    emit manipulated();
    break;
  default:
    wheelEvent(event, camera);
    return;
  }

  if(previousConstraint_)
    setConstraint(previousConstraint_);

  const int finalDrawAfterWheelEventDelay = 400;

  QTimer::singleShot(finalDrawAfterWheelEventDelay, this, SLOT(flyUpdate()));

  // This could also be done *before* manipulated is emitted, so that isManipulated() returns false.
  // But then fastDraw would not be used with wheel.
  // Detecting the last wheel event and forcing a final draw() is done using the timer_.
  action_ = QGLViewer::NO_MOUSE_ACTION;
}

MDXCamera::MDXCamera() : qglviewer::Camera()
{
  setType(qglviewer::Camera::ORTHOGRAPHIC);
  _frame = new MDXCameraFrame();
  setFrame(_frame);
}

void MDXCamera::getOrthoWidthHeight(GLdouble &halfWidth, GLdouble &halfHeight) const
{
  qglviewer::Camera::getOrthoWidthHeight(halfWidth, halfHeight);
  double z = 0.5 * exp(_frame->zoom());
  halfWidth *= z;
  halfHeight *= z;
}

void MDXCamera::resetZoom() { _frame->setZoom(0); }

void MDXCamera::fitSphere(const Vec& center, double radius)
{
  _frame->setZoom(0.0);
  qglviewer::Camera::fitSphere(center, radius);
}

MorphoViewer::MorphoViewer(QWidget* parent) : QGLViewer(parent), _camera(new MDXCamera()) {}

MorphoViewer::~MorphoViewer()
{
  saveStateToFile();

  // Clean FBO
  glfuncs->glDeleteTextures(NB_FRAMES, colorTexId);
  glfuncs->glDeleteTextures(NB_FRAMES, depthTexId);
  for(int i = 0; i < NB_FRAMES; ++i) {
    colorTexId[i] = 0;
    depthTexId[i] = 0;
  }
  glfuncs->glDeleteRenderbuffers(1, &depthBuffer);
  depthBuffer = 0;

  if(fboId != 0)
    glfuncs->glDeleteFramebuffers(1, &fboId);
  fboId = 0;
  if(fboCopyId != 0)
    glfuncs->glDeleteFramebuffers(1, &fboCopyId);
  fboCopyId = 0;
}

void MorphoViewer::init()
{
  setCamera(_camera);

  // Check multi-sampling is de-activate, but in practice it seems useless ..
  if(format().sampleBuffers()) {
    QMessageBox::critical(this, 
      "There is a Problem with the OpenGL context", 
      "Multi-sampling is activated in the OpenGL context.\n"
      "Make sure your drivers are not setup to override application preferences.\n"
      "Note that as long as this persists, selection will not work properly in MorphoDynamX.");
  }

  QGLViewer::init();

  // Initialize OpenGL
  glfuncs = this->context()->contextHandle()->versionFunctions<MDXOpenGLFunctions>();
  if(!glfuncs || !(glfuncs->initializeOpenGLFunctions())) {
    mdxInfo << "Error initializing OpenGL, MorphoDynamX requires OpenGL "
                     << MDX_REQUIRED_OPENGL_VERSION << endl;
    QCoreApplication::exit(126);
    return;
  }
  mdxInfo << "Rendering OpenGL " << (char*)(glfuncs->glGetString(GL_VERSION))
                   << " on " << (char*)(glfuncs->glGetString(GL_RENDERER)) << endl;

  // Report texture size
  GLint maxTexSize;
  glfuncs->glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTexSize);
  mdxInfo << "OpenGL maximum texture size: " << maxTexSize << endl;

  initCamera();

  raycastingShader1.setVerbosity(0);
  raycastingShader1.init();

  raycastingShader1.addVertexShader(":/shaders/RayCasting.vert");
  raycastingShader1.addFragmentShader(":/shaders/Utils.frag");
  raycastingShader1.addFragmentShader(":/shaders/3DColoring.frag");
  raycastingShader1.addFragmentShaderCode("");   // To be replaced by the needed function
  raycastingShader1.addFragmentShader(":/shaders/RayCasting.frag");

  raycastingShader2.setVerbosity(0);
  raycastingShader2.init();

  raycastingShader2.addVertexShader(":/shaders/RayCasting.vert");
  raycastingShader2.addFragmentShader(":/shaders/Utils.frag");
  raycastingShader2.addFragmentShader(":/shaders/3DColoring.frag");
  raycastingShader2.addFragmentShaderCode("");   // To be replaced by the needed function
  raycastingShader2.addFragmentShader(":/shaders/RayCasting.frag");

  finalCombineShader.setVerbosity(0);
  finalCombineShader.init();

  finalCombineShader.addVertexShader(":/shaders/Combine.vert");
  finalCombineShader.addFragmentShader(":/shaders/Utils.frag");
  finalCombineShader.addFragmentShader(":/shaders/FinalCombine.frag");

  combineShader.setVerbosity(0);
  combineShader.init();

  combineShader.addVertexShader(":/shaders/Combine.vert");
  combineShader.addFragmentShader(":/shaders/Utils.frag");
  combineShader.addFragmentShader(":/shaders/Combine.frag");

  occlusionShader.setVerbosity(0);
  occlusionShader.init();

  occlusionShader.addVertexShader(":/shaders/PostProcess.vert");
  occlusionShader.addFragmentShader(":/shaders/Occlusion.frag");

  postProcessShader.setVerbosity(0);
  postProcessShader.init();

  postProcessShader.addVertexShader(":/shaders/PostProcess.vert");
  postProcessShader.addFragmentShader(":/shaders/PostProcess.frag");

  renderDepthShader.setVerbosity(0);
  renderDepthShader.init();

  renderDepthShader.addVertexShader(":/shaders/RenderDepth.vert");
  renderDepthShader.addFragmentShader(":/shaders/RenderDepth.frag");

  textureSurfShader.setVerbosity(0);
  textureSurfShader.init();

  textureSurfShader.addVertexShader(":/shaders/Light.vert");
  textureSurfShader.addVertexShader(":/shaders/TextureSurf.vert");
  textureSurfShader.addVertexShader(":/shaders/PeelingSurf.vert");
  textureSurfShader.addFragmentShader(":/shaders/Light.frag");
  textureSurfShader.addFragmentShader(":/shaders/TextureSurf.frag");
  textureSurfShader.addFragmentShader(":/shaders/PeelingSurf.frag");

  volumeSurfShader1.setVerbosity(0);
  volumeSurfShader1.init();

  volumeSurfShader1.addVertexShader(":/shaders/Light.vert");
  volumeSurfShader1.addVertexShader(":/shaders/VolumeSurf.vert");
  volumeSurfShader1.addVertexShader(":/shaders/PeelingSurf.vert");
  volumeSurfShader1.addFragmentShader(":/shaders/Light.frag");
  volumeSurfShader1.addFragmentShader(":/shaders/Utils.frag");
  volumeSurfShader1.addFragmentShader(":/shaders/3DColoring.frag");
  volumeSurfShader1.addFragmentShaderCode("");   // To be replaced by the needed function
  volumeSurfShader1.addFragmentShader(":/shaders/VolumeSurf.frag");
  volumeSurfShader1.addFragmentShader(":/shaders/PeelingSurf.frag");

  volumeSurfShader2.setVerbosity(0);
  volumeSurfShader2.init();

  volumeSurfShader2.addVertexShader(":/shaders/Light.vert");
  volumeSurfShader2.addVertexShader(":/shaders/VolumeSurf.vert");
  volumeSurfShader2.addVertexShader(":/shaders/PeelingSurf.vert");
  volumeSurfShader2.addFragmentShader(":/shaders/Light.frag");
  volumeSurfShader2.addFragmentShader(":/shaders/Utils.frag");
  volumeSurfShader2.addFragmentShader(":/shaders/3DColoring.frag");
  volumeSurfShader2.addFragmentShaderCode("");   // To be replaced by the needed function
  volumeSurfShader2.addFragmentShader(":/shaders/VolumeSurf.frag");
  volumeSurfShader2.addFragmentShader(":/shaders/PeelingSurf.frag");

  colorSurfShader.setVerbosity(0);
  colorSurfShader.init();

  colorSurfShader.addVertexShader(":/shaders/Light.vert");
  colorSurfShader.addVertexShader(":/shaders/ColorSurf.vert");
  colorSurfShader.addVertexShader(":/shaders/PeelingSurf.vert");
  colorSurfShader.addFragmentShader(":/shaders/Light.frag");
  colorSurfShader.addFragmentShader(":/shaders/ColorSurf.frag");
  colorSurfShader.addFragmentShader(":/shaders/PeelingSurf.frag");

  flatSurfShader.setVerbosity(0);
  flatSurfShader.init();

  flatSurfShader.addVertexShader(":/shaders/Light.vert");
  flatSurfShader.addVertexShader(":/shaders/FlatSurf.vert");
  flatSurfShader.addVertexShader(":/shaders/PeelingSurf.vert");
  flatSurfShader.addFragmentShader(":/shaders/Light.frag");
  flatSurfShader.addFragmentShader(":/shaders/FlatSurf.frag");
  flatSurfShader.addFragmentShader(":/shaders/PeelingSurf.frag");

  REPORT_GL_ERROR("OpenGL error initializing shaders");

  camera()->setType(qglviewer::Camera::ORTHOGRAPHIC);
  camera()->setSceneCenter(Vec(0, 0, 0));
  camera()->centerScene();
  camera()->showEntireScene();

  // Color3f clearColor = Colors::getColor(Colors::BackgroundColor);
  // glfuncs->glClearColor(clearColor.r(), clearColor.g(), clearColor.b(), 1.0);
  // glfuncs->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT);
  glfuncs->glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glfuncs->glShadeModel(GL_SMOOTH);
  // glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  // glfuncs->glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
  glfuncs->glEnable(GL_DEPTH_CLAMP_NV);   // Allow to zoom more, but cause reverse later. DO NOT REMOVE

  stack1->initTex();
  stack2->initTex();
  ImgData::scaleBar.init(this);

  // Init FBO
  glfuncs->glGenFramebuffers(1, &fboId);
  glfuncs->glGenFramebuffers(1, &fboCopyId);

  // Create textures for depth and color buffer
  depthBuffer = 0;

  for(int i = 0; i < NB_FRAMES; ++i) {
    colorTexId[i] = 0;
    depthTexId[i] = 0;
  }

  prevWidth = prevHeight = 0;
  // updateFBOTex(width(), height());

  // To start with, we always render on the screen
  baseFboId = 0;
  //  mdxInfo << "OpenGL initialized" << endl;

  setAnimationPeriod(0);


  // Remap movement to manipulated frame
  setMouseBinding(Qt::SHIFT + Qt::LeftButton, CAMERA, TRANSLATE);
  setMouseBinding(Qt::CTRL + Qt::SHIFT + Qt::LeftButton, FRAME, TRANSLATE);
  // Prevent manipulated frame zooming
  setWheelBinding(Qt::NoModifier, CAMERA, ZOOM);
  // Set that to move along the z axis
  setMouseBinding(Qt::CTRL + Qt::MidButton, FRAME, MOVE_FORWARD);

  // Include shift in selection
  setShortcut(SAVE_SCREENSHOT, Qt::SHIFT + Qt::Key_Print);

  // Remove some shortcuts
  setShortcut(CAMERA_MODE, 0);
  setShortcut(EXIT_VIEWER, 0);
  setShortcut(INCREASE_FLYSPEED, 0);
  setShortcut(DECREASE_FLYSPEED, 0);
  setContextMenuPolicy(Qt::ActionsContextMenu);

  // Make keyframe use Control instead of Alt
 	setAddKeyFrameKeyboardModifiers(Qt::ControlModifier);

  // Enable capture of mouse move events
  setMouseTracking(true);

  // Turn off buffer swap, we'll do it ourselves
  setAutoBufferSwap(false);
}

void MorphoViewer::postDraw()
{
  if(quitting)
    return;
  glfuncs->glEnable(GL_DEPTH_TEST);
  QGLViewer::postDraw();
  swapBuffers();
}

void MorphoViewer::initFromDOMElement(const QDomElement& element)
{
  // Restore standard state
  QGLViewer::initFromDOMElement(element);

  QGLFormat def = QGLFormat::defaultFormat();

  QDomElement child = element.firstChild().toElement();
  while(!child.isNull()) {
    if(child.tagName() == "GLFormat") {
      if(child.hasAttribute("alpha"))
        def.setAlpha(child.attribute("alpha").toLower() == "yes");
      if(child.hasAttribute("depth"))
        def.setDepth(child.attribute("depth").toLower() == "yes");
      if(child.hasAttribute("rgba"))
        def.setRgba(child.attribute("rgba").toLower() == "yes");
      if(child.hasAttribute("stereo"))
        def.setStereo(child.attribute("stereo").toLower() == "yes");
      if(child.hasAttribute("stencil"))
        def.setStencil(child.attribute("stencil").toLower() == "yes");
      if(child.hasAttribute("doubleBuffer"))
        def.setDoubleBuffer(child.attribute("doubleBuffer").toLower() == "yes");
      if(child.hasAttribute("sampleBuffers"))
        def.setSampleBuffers(child.attribute("sampleBuffers").toLower() == "yes");
      if(child.hasAttribute("directRendering"))
        def.setDirectRendering(child.attribute("directRendering").toLower() == "yes");
      if(child.hasAttribute("hasOverlay"))
        def.setOverlay(child.attribute("hasOverlay").toLower() == "yes");
      if(def.accum())
        if(child.hasAttribute("accumBufferSize"))
          def.setAccumBufferSize(child.attribute("accumBufferSize").toInt());
      if(def.alpha())
        if(child.hasAttribute("alphaBufferSize"))
          def.setAlphaBufferSize(child.attribute("alphaBufferSize").toInt());
      if(def.depth())
        if(child.hasAttribute("depthBufferSize"))
          def.setDepthBufferSize(child.attribute("depthBufferSize").toInt());
      if(def.sampleBuffers())
        if(child.hasAttribute("samples"))
          def.setSamples(child.attribute("samples").toInt());
      if(def.stencil())
        if(child.hasAttribute("stencilBufferSize"))
          def.setStencilBufferSize(child.attribute("stencilBufferSize").toInt());
      if(def.rgba()) {
        if(child.hasAttribute("redBufferSize"))
          def.setRedBufferSize(child.attribute("redBufferSize").toInt());
        if(child.hasAttribute("greenBufferSize"))
          def.setGreenBufferSize(child.attribute("greenBufferSize").toInt());
        if(child.hasAttribute("blueBufferSize"))
          def.setBlueBufferSize(child.attribute("blueBufferSize").toInt());
      }
    }
    child = child.nextSibling().toElement();
  }
  QGLFormat::setDefaultFormat(def);
}

QDomElement MorphoViewer::domElement(const QString& name, QDomDocument& document) const
{
  // Creates a custom node for a light
  QDomElement de = document.createElement("GLFormat");
  QGLFormat def = QGLFormat::defaultFormat();
  de.setAttribute("accum", (def.accum() ? "yes" : "no"));
  if(def.accum() && def.accumBufferSize() != -1)
    de.setAttribute("accumBufferSize", std::max(0, def.accumBufferSize()));
  de.setAttribute("alpha", (def.alpha() ? "yes" : "no"));
  if(def.alpha() && def.alphaBufferSize() != -1)
    de.setAttribute("alphaBufferSize", std::max(0, def.alphaBufferSize()));
  de.setAttribute("depth", (def.depth() ? "yes" : "no"));
  if(def.depth() && def.depthBufferSize() != -1)
    de.setAttribute("depthBufferSize", std::max(0, def.depthBufferSize()));
  de.setAttribute("doubleBuffer", (def.doubleBuffer() ? "yes" : "no"));
  de.setAttribute("directRendering", (def.directRendering() ? "yes" : "no"));
  de.setAttribute("hasOverlay", (def.hasOverlay() ? "yes" : "no"));
  de.setAttribute("rgba", (def.rgba() ? "yes" : "no"));
  if(def.rgba()) {
    if(def.redBufferSize() != -1)
      de.setAttribute("redBufferSize", std::max(0, def.redBufferSize()));
    if(def.greenBufferSize() != -1)
      de.setAttribute("greenBufferSize", std::max(0, def.greenBufferSize()));
    if(def.blueBufferSize() != -1)
      de.setAttribute("blueBufferSize", std::max(0, def.blueBufferSize()));
  }
  de.setAttribute("sampleBuffers", (def.sampleBuffers() ? "yes" : "no"));
  if(def.sampleBuffers() && def.samples() != -1)
    de.setAttribute("samples", std::max(0, def.samples()));
  de.setAttribute("stereo", (def.stereo() ? "yes" : "no"));
  de.setAttribute("stencil", (def.stencil() ? "yes" : "no"));
  if(def.stencil() && def.stencilBufferSize() != -1)
    de.setAttribute("stencilBufferSize", std::max(0, def.stencilBufferSize()));

  // Get default state domElement and append custom node
  QDomElement res = QGLViewer::domElement(name, document);
  res.appendChild(de);
  return res;
}

void MorphoViewer::initFormat()
{
  QGLFormat def = QGLFormat::defaultFormat();
  QFile file(".qglviewer.xml");
  if(file.open(QIODevice::ReadOnly)) {
    QDomDocument doc("QGLViewer");
    doc.setContent(&file);
    file.close();
    QDomElement root = doc.firstChildElement("QGLViewer");
    QDomElement child = root.firstChildElement("GLFormat");
    if(!child.isNull()) {
      if(child.hasAttribute("alpha"))
        def.setAlpha(child.attribute("alpha").toLower() == "yes");
      if(child.hasAttribute("depth"))
        def.setDepth(child.attribute("depth").toLower() == "yes");
      if(child.hasAttribute("rgba"))
        def.setRgba(child.attribute("rgba").toLower() == "yes");
      if(child.hasAttribute("stereo"))
        def.setStereo(child.attribute("stereo").toLower() == "yes");
      if(child.hasAttribute("stencil"))
        def.setStencil(child.attribute("stencil").toLower() == "yes");
      if(child.hasAttribute("doubleBuffer"))
        def.setDoubleBuffer(child.attribute("doubleBuffer").toLower() == "yes");
      if(child.hasAttribute("sampleBuffers"))
        def.setSampleBuffers(child.attribute("sampleBuffers").toLower() == "yes");
      if(child.hasAttribute("directRendering"))
        def.setDirectRendering(child.attribute("directRendering").toLower() == "yes");
      if(child.hasAttribute("hasOverlay"))
        def.setOverlay(child.attribute("hasOverlay").toLower() == "yes");
      if(def.accum())
        if(child.hasAttribute("accumBufferSize"))
          def.setAccumBufferSize(child.attribute("accumBufferSize").toInt());
      if(def.alpha())
        if(child.hasAttribute("alphaBufferSize"))
          def.setAlphaBufferSize(child.attribute("alphaBufferSize").toInt());
      if(def.depth())
        if(child.hasAttribute("depthBufferSize"))
          def.setDepthBufferSize(child.attribute("depthBufferSize").toInt());
      if(def.sampleBuffers())
        if(child.hasAttribute("samples"))
          def.setSamples(child.attribute("samples").toInt());
      if(def.stencil())
        if(child.hasAttribute("stencilBufferSize"))
          def.setStencilBufferSize(child.attribute("stencilBufferSize").toInt());
      if(def.rgba()) {
        if(child.hasAttribute("redBufferSize"))
          def.setRedBufferSize(child.attribute("redBufferSize").toInt());
        if(child.hasAttribute("greenBufferSize"))
          def.setGreenBufferSize(child.attribute("greenBufferSize").toInt());
        if(child.hasAttribute("blueBufferSize"))
          def.setBlueBufferSize(child.attribute("blueBufferSize").toInt());
      }
    }
  }
  QGLFormat::setDefaultFormat(def);
}

void MorphoViewer::showEvent(QShowEvent* event)
{
  // FIXME What was this supposed to do?
  return;
  QGLViewer::showEvent(event);
  if(!initialized) {
    restoreStateFromFile();
    lastInitCamera = camera();
    initialized = true;
  }
}

void MorphoViewer::initCamera()
{
  if(camera() != lastInitCamera) {
    restoreStateFromFile();
    lastInitCamera = camera();
  }
}

void MorphoViewer::preDraw()
{
  if(quitting)
    return;
  QGLViewer::preDraw();
  camera()->setSceneRadius(sceneRadius);

  glfuncs->glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  if(!finalCombineShader.initialized())
    if(!finalCombineShader.setupShaders())
      mdxWarning << "Error compiling combine shaders" << endl;
  if(!combineShader.initialized())
    if(!combineShader.setupShaders())
      mdxWarning << "Error compiling combine shaders" << endl;
  if(!occlusionShader.initialized())
    if(!occlusionShader.setupShaders())
      mdxWarning << "Error compiling combine shaders" << endl;
  if(!postProcessShader.initialized())
    if(!postProcessShader.setupShaders())
      mdxWarning << "Error compiling combine shaders" << endl;
  if(!renderDepthShader.initialized())
    if(!renderDepthShader.setupShaders())
      mdxWarning << "Error compiling render_depth shaders" << endl;
  bool hasClip = (c1->enabled() or c2->enabled() or c3->enabled());
  stack1->setupVolumeShader(raycastingShader1, 2, hasClip);
  if(!raycastingShader1.initialized())
    if(!raycastingShader1.setupShaders())
      mdxWarning << "Error compiling colormap shaders" << endl;
  stack2->setupVolumeShader(raycastingShader2, 2, hasClip);
  if(!raycastingShader2.initialized())
    if(!raycastingShader2.setupShaders())
      mdxWarning << "Error compiling colormap shaders" << endl;
  if(!textureSurfShader.initialized())
    if(!textureSurfShader.setupShaders())
      mdxWarning << "Error compiling texture_surf shaders" << endl;
  stack1->setupVolumeShader(volumeSurfShader1, 3, hasClip);
  if(!volumeSurfShader1.initialized())
    if(!volumeSurfShader1.setupShaders())
      mdxWarning << "Error compiling volume_surf shaders" << endl;
  stack2->setupVolumeShader(volumeSurfShader2, 3, hasClip);
  if(!volumeSurfShader2.initialized())
    if(!volumeSurfShader2.setupShaders())
      mdxWarning << "Error compiling volume_surf shaders" << endl;
  if(!colorSurfShader.initialized())
    if(!colorSurfShader.setupShaders())
      mdxWarning << "Error compiling color_surf shaders" << endl;
  if(!flatSurfShader.initialized())
    if(!flatSurfShader.setupShaders())
      mdxWarning << "Error compiling flat_surf shaders" << endl;
}

static bool checkFBO(const char* file, size_t ln)
{
  GLenum status = glfuncs->glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT);

  if(status == 0) {
    mdxWarning << "Error line " << ln << " creating framebuffer" << endl;
    return false;
  } else {
    if(status != GL_FRAMEBUFFER_COMPLETE_EXT) {
      mdxWarning << "Error file " << file << " on line " << ln << ": ";
      switch(status) {
      case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT:
        mdxWarning << "Framebuffer is incomplete: incomplete attachment" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
        mdxWarning << "Framebuffer is incomplete: missing attachment" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
        mdxWarning << "Framebuffer is incomplete: incomplete draw buffer" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
        mdxWarning << "Framebuffer is incomplete: incomplete read buffer" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
        mdxWarning << "Framebuffer is incomplete: incomplete formats" << endl;
        break;
      case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
        mdxWarning << "Framebuffer is incomplete: incomplete dimensions" << endl;
        break;
      case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
        mdxWarning << "Framebuffer unsupported" << endl;
        break;
      default:
        mdxWarning << "Framebuffer is incomplete: status = " << status << endl;
      }
      return false;
    }
  }
  return true;
}

void MorphoViewer::screenSampling(int val) 
{
  sampling = val / 10.0 + 1.0;
}

void MorphoViewer::updateFBOTex(int width, int height, bool fast_draw)
{
  if(DEBUG)
    mdxInfo << "updateFBOTex(" << width << ", " << height << ", " << fast_draw << ")" << endl;
  if(fast_draw) {
    texWidth = int(ceil(width / sampling));
    texHeight = int(ceil(height / sampling));
  } else {
    texWidth = width;
    texHeight = height;
  }
  if(texWidth == prevWidth and texHeight == prevHeight)
    return;
  // mdxWarning << "width = " << texWidth << " - height = " << texHeight << endl;

  prevWidth = texWidth;
  prevHeight = texHeight;

  if(colorTexId[0] == 0)
    glfuncs->glGenTextures(NB_FRAMES, colorTexId);
  if(depthTexId[0] == 0)
    glfuncs->glGenTextures(NB_FRAMES, depthTexId);
  if(depthBuffer == 0)
    glfuncs->glGenRenderbuffers(1, &depthBuffer);

  // Create textures
  for(int i = 0; i < NB_FRAMES; ++i) {
    glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[i]);

    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glfuncs->glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_SHORT, NULL);

    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[i]);

    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_ALPHA);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    glfuncs->glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, texWidth, texHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
                 NULL);
  }

  glfuncs->glBindTexture(GL_TEXTURE_2D, 0);

  // Bind textures to framebuffer

  glfuncs->glBindFramebuffer(GL_FRAMEBUFFER_EXT, fboId);

  glfuncs->glBindRenderbuffer(GL_RENDERBUFFER_EXT, depthBuffer);
  glfuncs->glRenderbufferStorage(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT32F, texWidth, texHeight);
  glfuncs->glBindRenderbuffer(GL_RENDERBUFFER_EXT, 0);

  glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, colorTexId[0], 0);
  glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, depthTexId[0], 0);
  REPORT_GL_ERROR("CREATE_FBO");

  checkFBO(__FILE__, __LINE__);
}

void MorphoViewer::resizeGL(int width, int height)
{
  drawWidth = width;
  drawHeight = height;
  QGLViewer::resizeGL(width, height);
}

void MorphoViewer::setupCopyFB(GLuint depth, GLint color)
{
  glfuncs->glBindFramebuffer(GL_FRAMEBUFFER_EXT, fboCopyId);
  if(color != 0)
    glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, color, 0);
  if(depth != 0)
    glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, depth, 0);
}

void MorphoViewer::setupFramebuffer(GLuint depth, GLuint color, GLbitfield clear)
{
  GLdouble proj[16], mv[16];
  glfuncs->glGetDoublev(GL_MODELVIEW_MATRIX, mv);
  glfuncs->glGetDoublev(GL_PROJECTION_MATRIX, proj);

  /*
   * if(DEBUG)
   *{
   *  mdxInfo << "GL_PROJECTION_MATRIX = " << endl;
   *  for(int i = 0 ; i < 4 ; ++i)
   *  {
   *    mdxInfo << "{ ";
   *    for(int j = 0 ; j < 4 ; ++j)
   *    {
   *      mdxInfo << proj[i+4*j] << " ";
   *    }
   *    mdxInfo << "}\n";
   *  }
   *  mdxInfo << endl;
   *}
   */

  glfuncs->glBindFramebuffer(GL_FRAMEBUFFER_EXT, fboId);
  glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, color, 0);
  if(depth != 0) {
    glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, depth, 0);
  } else {
    glfuncs->glFramebufferRenderbuffer(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthBuffer);
  }

  // checkFBO(__LINE__);

  glfuncs->glPushAttrib(GL_VIEWPORT_BIT);
  glfuncs->glViewport(0, 0, texWidth, texHeight);

  glfuncs->glClear(clear);

  glfuncs->glMatrixMode(GL_PROJECTION);
  glfuncs->glPushMatrix();
  glfuncs->glLoadIdentity();
  glfuncs->glMultMatrixd(proj);

  glfuncs->glMatrixMode(GL_MODELVIEW);
  glfuncs->glPushMatrix();
  glfuncs->glLoadIdentity();
  glfuncs->glMultMatrixd(mv);
}

void MorphoViewer::resetupFramebuffer(GLuint depth, GLuint color, GLbitfield clear)
{
  glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, color, 0);
  if(depth > 0) {
    glfuncs->glBindTexture(GL_TEXTURE_2D, depth);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    glfuncs->glBindTexture(GL_TEXTURE_2D, 0);
    glfuncs->glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, depth, 0);
  } else {
    glfuncs->glFramebufferRenderbuffer(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthBuffer);
  }

  glfuncs->glClear(clear);
}

void MorphoViewer::resetFramebuffer()
{
  glfuncs->glMatrixMode(GL_PROJECTION);
  glfuncs->glPopMatrix();
  glfuncs->glMatrixMode(GL_MODELVIEW);
  glfuncs->glPopMatrix();

  glfuncs->glPopAttrib();

  glfuncs->glBindFramebuffer(GL_FRAMEBUFFER_EXT, baseFboId);
}

void MorphoViewer::startScreenCoordinatesSystem(bool upward) const
{
  glfuncs->glMatrixMode(GL_PROJECTION);
  glfuncs->glPushMatrix();
  glfuncs->glLoadIdentity();
  if(upward)
    glfuncs->glOrtho(0, current_device->width(), 0, current_device->height(), 0.0, -1.0);
  else
    glfuncs->glOrtho(0, current_device->width(), current_device->height(), 0, 0.0, -1.0);

  glfuncs->glMatrixMode(GL_MODELVIEW);
  glfuncs->glPushMatrix();
  glfuncs->glLoadIdentity();
}

void MorphoViewer::drawColorTexture(int i, bool draw_depth)
{
  // Check the texture ...
  glfuncs->glPolygonMode(GL_FRONT, GL_FILL);
  glfuncs->glEnable(GL_TEXTURE_2D);
  glfuncs->glMatrixMode(GL_TEXTURE);
  glfuncs->glPushMatrix();
  glfuncs->glLoadIdentity();
  glfuncs->glMatrixMode(GL_MODELVIEW);

  if(i >= 0) {
    if(draw_depth)
      glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[i]);
    else
      glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[i]);
  }

  // For now, render the depth buffer instead of the color one
  startScreenCoordinatesSystem(true);

  glfuncs->glBegin(GL_QUADS);
  glfuncs->glColor4f(1, 1, 1, 1);
  glfuncs->glTexCoord2d(0, 0);
  glfuncs->glVertex2d(0, 0);
  glfuncs->glTexCoord2d(1, 0);
  glfuncs->glVertex2d(drawWidth, 0);
  glfuncs->glTexCoord2d(1, 1);
  glfuncs->glVertex2d(drawWidth, drawHeight);
  glfuncs->glTexCoord2d(0, 1);
  glfuncs->glVertex2d(0, drawHeight);
  glfuncs->glEnd();

  stopScreenCoordinatesSystem();

  glfuncs->glMatrixMode(GL_TEXTURE);
  glfuncs->glPopMatrix();
  glfuncs->glMatrixMode(GL_MODELVIEW);

  if(i >= 0)
    glfuncs->glBindTexture(GL_TEXTURE_2D, 0);
  glfuncs->glDisable(GL_TEXTURE_2D);
}

void MorphoViewer::alternatePeels(int& curPeelId, int& prevPeelId, int fullImgId)
{
  if(curPeelId == -1) {
    curPeelId = 3;
  } else if(curPeelId == 3) {
    prevPeelId = 3;
    curPeelId = 4;
  } else {
    prevPeelId = 4;
    curPeelId = 3;
  }

  if(fullImgId != -1) {
    Shader::activeTexture(Shader::AT_FRONT_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_NONE);
  }

  resetupFramebuffer(depthTexId[curPeelId], colorTexId[curPeelId]);
}

void MorphoViewer::combinePeels(int& fullImgId, int curPeelId, int volume1, int volume2)
{
  int newFinalId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
  resetupFramebuffer(depthTexId[newFinalId], colorTexId[newFinalId]);
  combineShader.setUniform("front", GLSLValue(1));
  Shader::activeTexture(1);
  glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);

  combineShader.setUniform("front_depth", GLSLValue(2));
  Shader::activeTexture(2);
  glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
  glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

  combineShader.setUniform("back", GLSLValue(3));
  Shader::activeTexture(3);
  glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[curPeelId]);

  combineShader.setUniform("back_depth", GLSLValue(4));
  Shader::activeTexture(4);
  glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[curPeelId]);
  glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

  combineShader.setUniform("volume1", GLSLValue(5));
  Shader::activeTexture(5);
  glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[volume1]);

  combineShader.setUniform("volume2", GLSLValue(6));
  Shader::activeTexture(6);
  glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[volume2]);

  Shader::activeTexture(Shader::AT_NONE);
  glfuncs->glBindTexture(GL_TEXTURE_2D, 0);

  combineShader.useShaders();
  combineShader.setupUniforms();

  drawColorTexture(-1);

  combineShader.stopUsingShaders();
  fullImgId = newFinalId;
}

void MorphoViewer::fastDraw()
{
  fast_draw = true;
  draw();
  fast_draw = false;
}

void MorphoViewer::draw() 
{
  draw(this);
}

void MorphoViewer::draw(QPaintDevice* device)
{
  if(quitting or !device)
    return;

  REPORT_GL_ERROR("draw: start");
  current_device = device;
  if(ImgData::MeshLineWidth == 0.0) {
    GLfloat range[2];
    glfuncs->glGetFloatv(GL_LINE_WIDTH_RANGE, range);
    ImgData::MeshLineWidth = range[0];
  }
  updateFBOTex(drawWidth, drawHeight, fast_draw);

  uint oldSlices = ImgData::Slices;
  if(fast_draw)
    ImgData::Slices /= sampling;

  stack1->loadLabelTex();
  stack2->loadLabelTex();
  REPORT_GL_ERROR("draw: loading textures");

  // Set the clipping region
  // Draw the stack
  if(!raycastingShader1.initialized() or !raycastingShader2.initialized() or !volumeSurfShader1.initialized()
     or !volumeSurfShader2.initialized() or !textureSurfShader.initialized()) {
    Information::setStatus("Error, shaders not correctly initialized. Cannot render objects");
    return;
  }

  raycastingShader1.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  raycastingShader1.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
  raycastingShader1.setUniform("secondTex", GLSLValue(Shader::AT_SECOND_TEX3D));
  raycastingShader1.setUniform("secondColormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  raycastingShader1.setUniform("front", GLSLValue(Shader::AT_FRONT_TEX));
  raycastingShader1.setUniform("frontColor", GLSLValue(Shader::AT_FRONT_COLOR_TEX));
  raycastingShader1.setUniform("occlusion", GLSLValue(Shader::AT_OCCLUSION_TEX));
  raycastingShader1.setUniform("back", GLSLValue(Shader::AT_BACK_TEX));
  raycastingShader1.setUniform("solid", GLSLValue(Shader::AT_DEPTH_TEX));
  raycastingShader1.setUniform("labelColormap", GLSLValue(Shader::AT_LABEL_TEX));
  raycastingShader1.setUniform("colorCount", GLSLValue(int(ImgData::LabelColors.size())));
  raycastingShader1.setUniform("labelCenters1", GLSLValue(Shader::AT_LABEL_CENTER_TEX1));
  raycastingShader1.setUniform("labelCenters2", GLSLValue(Shader::AT_LABEL_CENTER_TEX2));

  raycastingShader2.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  raycastingShader2.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
  raycastingShader2.setUniform("secondTex", GLSLValue(Shader::AT_SECOND_TEX3D));
  raycastingShader2.setUniform("secondColormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  raycastingShader2.setUniform("front", GLSLValue(Shader::AT_FRONT_TEX));
  raycastingShader2.setUniform("frontColor", GLSLValue(Shader::AT_FRONT_COLOR_TEX));
  raycastingShader2.setUniform("occlusion", GLSLValue(Shader::AT_OCCLUSION_TEX));
  raycastingShader2.setUniform("back", GLSLValue(Shader::AT_BACK_TEX));
  raycastingShader2.setUniform("solid", GLSLValue(Shader::AT_DEPTH_TEX));
  raycastingShader2.setUniform("labelColormap", GLSLValue(Shader::AT_LABEL_TEX));
  raycastingShader2.setUniform("colorCount", GLSLValue(int(ImgData::LabelColors.size())));
  raycastingShader2.setUniform("labelCenters1", GLSLValue(Shader::AT_LABEL_CENTER_TEX1));
  raycastingShader2.setUniform("labelCenters2", GLSLValue(Shader::AT_LABEL_CENTER_TEX2));

  volumeSurfShader1.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  volumeSurfShader1.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  volumeSurfShader1.setUniform("testFront", GLSLValue(false));
  volumeSurfShader1.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  volumeSurfShader1.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
//  volumeSurfShader1.setUniform("second_tex", GLSLValue(Shader::AT_SECOND_TEX3D));
//  volumeSurfShader1.setUniform("second_colormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
//  volumeSurfShader1.setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
//  volumeSurfShader1.setUniform("nb_colors", GLSLValue(int(ImgData::LabelColors.size())));
  volumeSurfShader1.setUniform("secondTex", GLSLValue(Shader::AT_SECOND_TEX3D));
  volumeSurfShader1.setUniform("secondColormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  volumeSurfShader1.setUniform("labelColormap", GLSLValue(Shader::AT_LABEL_TEX));
  volumeSurfShader1.setUniform("colorCount", GLSLValue(int(ImgData::LabelColors.size())));
  volumeSurfShader1.setUniform("labelCenters1", GLSLValue(Shader::AT_LABEL_CENTER_TEX1));
  volumeSurfShader1.setUniform("labelCenters2", GLSLValue(Shader::AT_LABEL_CENTER_TEX2));

  volumeSurfShader2.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  volumeSurfShader2.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  volumeSurfShader2.setUniform("testFront", GLSLValue(false));
  volumeSurfShader2.setUniform("tex", GLSLValue(Shader::AT_TEX3D));
  volumeSurfShader2.setUniform("colormap", GLSLValue(Shader::AT_CMAP_TEX));
//  volumeSurfShader2.setUniform("second_tex", GLSLValue(Shader::AT_SECOND_TEX3D));
//  volumeSurfShader2.setUniform("second_colormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
//  volumeSurfShader2.setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
//  volumeSurfShader2.setUniform("nb_colors", GLSLValue(int(ImgData::LabelColors.size())));
  volumeSurfShader2.setUniform("secondTex", GLSLValue(Shader::AT_SECOND_TEX3D));
  volumeSurfShader2.setUniform("secondColormap", GLSLValue(Shader::AT_SECOND_CMAP_TEX));
  volumeSurfShader2.setUniform("labelColormap", GLSLValue(Shader::AT_LABEL_TEX));
  volumeSurfShader2.setUniform("colorCount", GLSLValue(int(ImgData::LabelColors.size())));
  volumeSurfShader2.setUniform("labelCenters1", GLSLValue(Shader::AT_LABEL_CENTER_TEX1));
  volumeSurfShader2.setUniform("labelCenters2", GLSLValue(Shader::AT_LABEL_CENTER_TEX2));

  colorSurfShader.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  colorSurfShader.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  colorSurfShader.setUniform("testFront", GLSLValue(false));

  flatSurfShader.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  flatSurfShader.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  flatSurfShader.setUniform("testFront", GLSLValue(false));

  textureSurfShader.setUniform("backDepth", GLSLValue(Shader::AT_DEPTH_TEX));
  textureSurfShader.setUniform("frontDepth", GLSLValue(Shader::AT_FRONT_TEX));
  textureSurfShader.setUniform("testFront", GLSLValue(false));

  REPORT_GL_ERROR("draw: setting up shaders");

  // Save projection and modelview matrices
  Color3f clearColor = Colors::getColor(Colors::BackgroundColor);
  glfuncs->glClearColor(clearColor.r(), clearColor.g(), clearColor.b(), 1.0);
  REPORT_GL_ERROR("draw: Set ClearColor");

  setupFramebuffer(depthTexId[FI_BACKGROUND], colorTexId[FI_BACKGROUND]);
  REPORT_GL_ERROR("draw: Setting up FrameBuffer");

  textureSurfShader.setUniform("peeling", GLSLValue(false));
  colorSurfShader.setUniform("peeling", GLSLValue(false));
  flatSurfShader.setUniform("peeling", GLSLValue(false));
  volumeSurfShader1.setUniform("peeling", GLSLValue(false));
  volumeSurfShader2.setUniform("peeling", GLSLValue(false));
  REPORT_GL_ERROR("draw: Turn off surface shader peeling");

  // ### Begin draw opaque

  glfuncs->glDisable(GL_BLEND);

  clip1.drawGrid(sceneRadius);
  clip2.drawGrid(sceneRadius);
  clip3.drawGrid(sceneRadius);
  REPORT_GL_ERROR("draw: clipping plane grid");

  if(gridIsDrawn()) {
    glfuncs->glLineWidth(1.0);
    drawGrid(camera()->sceneRadius());
    REPORT_GL_ERROR("draw: grid");
  }
	if(axisIsDrawn()) {
    glfuncs->glLineWidth(2.0);
    drawAxis(camera()->sceneRadius());
    REPORT_GL_ERROR("draw: axis");
  }

  stack1->drawBBox();
  stack2->drawBBox();
  REPORT_GL_ERROR("draw: BBox");

  clip1.drawClip();
  clip2.drawClip();
  clip3.drawClip();
  REPORT_GL_ERROR("draw: clipping planes");

  glfuncs->glEnable(GL_LIGHTING);

  // If opaque, we only need to draw once
  if(stack1->mesh->opacity() == 1.0) {
    if(stack1->mesh->shading()) {
      setLighting(stack1);
      stack1->drawCCs(&colorSurfShader);
    } else 
      stack1->drawCCs(&flatSurfShader);
  }
  if(stack2->mesh->opacity() == 1.0) {
    if(stack2->mesh->shading()) {
      setLighting(stack2);
      stack2->drawCCs(&colorSurfShader);
    } else
      stack2->drawCCs(&flatSurfShader);
  }
  REPORT_GL_ERROR("draw: drawing surface");

  cutSurf->drawCutSurfGrid(*stack1);
  cutSurf->drawCutSurfGrid(*stack2);

  if(stack1->mesh->opacity() == 1.0) {
    setLighting(stack1);
    cutSurf->drawCutSurfPlane(*stack1, false, &volumeSurfShader1);
  }

  if(stack2->mesh->opacity() == 1.0) {
    setLighting(stack2);
    cutSurf->drawCutSurfPlane(*stack2, false, &volumeSurfShader2);
  }
  REPORT_GL_ERROR("draw: drawing cutting surface");

  // ### End draw opaque

  glfuncs->glClearColor(0, 0, 0, 0);

  Shader::activeTexture(Shader::AT_DEPTH_TEX);
  glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_BACKGROUND]);
  glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
  Shader::activeTexture(Shader::AT_NONE);

  glfuncs->glEnable(GL_DEPTH_TEST);
  REPORT_GL_ERROR("draw: setting 2D texture");

  int fullImgId = FI_FULL_IMG1;

  // First, clean the first peel and set it "full front"
  glfuncs->glClearDepth(0.0);
  glfuncs->glClearColor(0, 0, 0, 0);
  resetupFramebuffer(depthTexId[fullImgId], colorTexId[fullImgId]);

  glfuncs->glClearDepth(1.0);

  textureSurfShader.setUniform("peeling", GLSLValue(true));
  colorSurfShader.setUniform("peeling", GLSLValue(true));
  flatSurfShader.setUniform("peeling", GLSLValue(true));
  volumeSurfShader1.setUniform("peeling", GLSLValue(true));
  volumeSurfShader2.setUniform("peeling", GLSLValue(true));

  // Clear occlusion texture
  glfuncs->glClearColor(0, 1, 0, 1);
  resetupFramebuffer(0, colorTexId[FI_OCCLUSION]);

  glfuncs->glClearColor(0, 0, 0, 0);
  REPORT_GL_ERROR("draw: setting 3D draw shaders");

  // Create query
  GLuint occ_query, nb_pixels;
  glfuncs->glGenQueries(1, &occ_query);
  for(int current_pass = 0; current_pass < MaxNbPeels; ++current_pass) {
    // Setup the last full image as front texture

    // Set the framebuffer to write the current peel

    resetupFramebuffer(depthTexId[FI_CUR_PEEL], colorTexId[FI_CUR_PEEL]);

    if(current_pass == 0 and !fast_draw) {
      // Now, copy the depth buffer, only on the first pass, to get the correct
      // depth of the surface
      setupCopyFB(depthTexId[FI_BACKGROUND], colorTexId[FI_BACKGROUND]);

      glfuncs->glBindFramebuffer(GL_READ_FRAMEBUFFER_EXT, fboCopyId);
      glfuncs->glBindFramebuffer(GL_DRAW_FRAMEBUFFER_EXT, fboId);

      glfuncs->glBlitFramebuffer(0, 0, texWidth, texHeight, 0, 0, texWidth, texHeight, GL_DEPTH_BUFFER_BIT, GL_NEAREST);

      glfuncs->glBindFramebuffer(GL_FRAMEBUFFER_EXT, fboId);
    }

    Shader::activeTexture(Shader::AT_DEPTH_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_BACKGROUND]);
    Shader::activeTexture(Shader::AT_FRONT_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_NONE);

    // ### Begin draw transparent

    glfuncs->glEnable(GL_LIGHTING);
    if(stack1->mesh->opacity() < 1.0) {
      if(stack1->mesh->shading()) {
        setLighting(stack1);
        stack1->drawCCs(&colorSurfShader);
      } else 
        stack1->drawCCs(&flatSurfShader);
    }
    if(stack2->mesh->opacity() < 1.0) {
      if(stack2->mesh->shading()) {
        setLighting(stack2);
        stack2->drawCCs(&colorSurfShader);
      } else
        stack2->drawCCs(&flatSurfShader);
    }
    // Next, the cutting surface
    if(stack1->mesh->opacity() < 1.0) {
      setLighting(stack1);
      cutSurf->drawCutSurfPlane(*stack1, false, &volumeSurfShader1);
    }
    if(stack2->mesh->opacity() < 1.0) {
      setLighting(stack2);
      cutSurf->drawCutSurfPlane(*stack2, false, &volumeSurfShader2);
    }

    // ### End draw transparent

    // And the volume

    // Setup depth buffers
    Shader::activeTexture(Shader::AT_BACK_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_CUR_PEEL]);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_FRONT_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(Shader::AT_FRONT_COLOR_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);
    Shader::activeTexture(Shader::AT_OCCLUSION_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[FI_OCCLUSION]);
    Shader::activeTexture(Shader::AT_NONE);
    glfuncs->glBindTexture(GL_TEXTURE_2D, 0);

    resetupFramebuffer(0, colorTexId[FI_VOLUME1]);
    stack1->drawStack(&raycastingShader1);

    resetupFramebuffer(0, colorTexId[FI_VOLUME2]);
    stack2->drawStack(&raycastingShader2);

    combinePeels(fullImgId, FI_CUR_PEEL, FI_VOLUME1, FI_VOLUME2);

    // Check if there is anything else to be done

    resetupFramebuffer(0, colorTexId[FI_OCCLUSION]);

    Shader::activeTexture(1);
    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[fullImgId]);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    Shader::activeTexture(2);
    glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);
    Shader::activeTexture(3);
    glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_BACKGROUND]);
    Shader::activeTexture(Shader::AT_NONE);
    glfuncs->glBindTexture(GL_TEXTURE_2D, 0);

    occlusionShader.setUniform("depth", GLSLValue(1));
    occlusionShader.setUniform("color", GLSLValue(2));
    occlusionShader.setUniform("background", GLSLValue(3));

    occlusionShader.useShaders();
    occlusionShader.setupUniforms();

    glfuncs->glBeginQuery(GL_SAMPLES_PASSED_ARB, occ_query);

    drawColorTexture(-1);

    glfuncs->glEndQuery(GL_SAMPLES_PASSED_ARB);

    glfuncs->glGetQueryObjectuiv(occ_query, GL_QUERY_RESULT_ARB, &nb_pixels);

    occlusionShader.stopUsingShaders();

    if(DEBUG and current_pass == MaxNbPeels - 1 and nb_pixels > 0) {
      int prevFullImg = fullImgId;
      fullImgId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
      resetupFramebuffer(0, colorTexId[fullImgId]);
      glfuncs->glDisable(GL_DEPTH_TEST);
      drawColorTexture(prevFullImg);
      glfuncs->glEnable(GL_BLEND);
      drawColorTexture(FI_OCCLUSION);
      glfuncs->glDisable(GL_BLEND);
      glfuncs->glEnable(GL_DEPTH_TEST);
      Information::setStatus(QString("Number of left pixels on pass # %1 = %2.").arg(current_pass).arg(nb_pixels));
    }

    // End check

    if(current_pass == 0 and !fast_draw) {
      // Now, copy the depth buffer!
      glfuncs->glEnable(GL_TEXTURE_2D);
      glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_CUR_PEEL]);
      depthTexture.resize(texWidth * texHeight);
      glfuncs->glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_FLOAT, &depthTexture[0]);
      glfuncs->glBindTexture(GL_TEXTURE_2D, 0);
      glfuncs->glDisable(GL_TEXTURE_2D);
    }
    if(show_slice > -1) {
      if(current_pass == show_slice) {
        int prevFullImg = fullImgId;
        fullImgId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;

        glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[FI_CUR_PEEL]);
        glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[prevFullImg]);
        glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glfuncs->glBindTexture(GL_TEXTURE_2D, 0);

        // glfuncs->glEnable(GL_BLEND);
        // glfuncs->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glfuncs->glDisable(GL_DEPTH_TEST);

        glfuncs->glClearColor(0, 0, 0, 1);
        resetupFramebuffer(0, colorTexId[fullImgId]);
        switch(slice_type) {
        case 0:
          drawColorTexture(FI_CUR_PEEL);
          break;
        case 1:
          drawColorTexture(FI_CUR_PEEL, true);
          break;
        case 2:
          drawColorTexture(FI_VOLUME1);
          break;
        case 3:
          drawColorTexture(FI_VOLUME2);
          break;
        case 4:
          drawColorTexture(prevFullImg);
          break;
        case 5:
          drawColorTexture(prevFullImg, true);
          break;
        case 6:
          drawColorTexture(FI_OCCLUSION);
          break;
        default:
          mdxInfo << "Unknown slice type: " << slice_type << endl;
        }
        break;
      }
    }
    REPORT_GL_ERROR("draw");
    if(nb_pixels == 0)     // TODO: Find out why some pixels are never empty!
    {
      if(DEBUG)
        mdxInfo << "# peels = " << current_pass + 1 << endl;
      if(show_slice > current_pass) {
        int prevFullImg = fullImgId;
        fullImgId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
        glfuncs->glDisable(GL_DEPTH_TEST);

        glfuncs->glBindTexture(GL_TEXTURE_2D, depthTexId[prevFullImg]);
        glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);

        glfuncs->glClearColor(0, 0, 0, 1);
        resetupFramebuffer(0, colorTexId[fullImgId]);
        switch(slice_type) {
        case 0:
          drawColorTexture(FI_BACKGROUND);
          break;
        case 1:
          drawColorTexture(FI_BACKGROUND, true);
          break;
        case 5:
          drawColorTexture(prevFullImg, true);
          break;
        default:
          drawColorTexture(prevFullImg);
          break;
        }
        break;
      }
      break;
    } else {
      if(DEBUG)
        mdxInfo << "# pixels at step " << current_pass << " = " << nb_pixels << endl;
    }
  }
  REPORT_GL_ERROR("draw: drawing 3D volume");
  glfuncs->glDeleteQueries(1, &occ_query);

  int finalId = (fullImgId == FI_FULL_IMG1) ? FI_FULL_IMG2 : FI_FULL_IMG1;
  resetupFramebuffer(0, colorTexId[finalId]);
  REPORT_GL_ERROR("draw: reseting frame buffer");

  Shader::activeTexture(Shader::AT_DEPTH_TEX);
  glfuncs->glBindTexture(GL_TEXTURE_2D, 0);
  Shader::activeTexture(Shader::AT_NONE);
  REPORT_GL_ERROR("draw: reseting 2D texture");

  // Now, render on the final texture, before the screen

  clip1.disable();
  clip2.disable();
  clip3.disable();

  // drawColorTexture(0);

  {
    resetupFramebuffer(depthTexId[finalId], colorTexId[finalId]);
    finalCombineShader.setUniform("front", GLSLValue(1));
    Shader::activeTexture(1);
    glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[fullImgId]);

    finalCombineShader.setUniform("back", GLSLValue(3));
    Shader::activeTexture(3);
    glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[FI_BACKGROUND]);

    Shader::activeTexture(Shader::AT_NONE);
    glfuncs->glBindTexture(GL_TEXTURE_2D, 0);

    finalCombineShader.useShaders();
    finalCombineShader.setupUniforms();

    drawColorTexture(-1);

    finalCombineShader.stopUsingShaders();
  }
  REPORT_GL_ERROR("draw: rendering to final texture");

  /*
     glfuncs->glEnable(GL_BLEND);
     glfuncs->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
     glfuncs->glDisable(GL_DEPTH_TEST);

     if(show_slice == -1)
     drawColorTexture(FI_BACKGROUND);

     if(fullImgId >= 0)
     {
     drawColorTexture(fullImgId);
     }
   */

  resetFramebuffer();
  REPORT_GL_ERROR("draw");

  glfuncs->glDisable(GL_DEPTH_TEST);

  postProcessShader.activeTexture(Shader::AT_FINAL_VOLUME_TEX);
  glfuncs->glBindTexture(GL_TEXTURE_2D, colorTexId[finalId]);
  postProcessShader.activeTexture(Shader::AT_NONE);
  postProcessShader.setUniform("texId", GLSLValue(Shader::AT_FINAL_VOLUME_TEX));
  postProcessShader.setUniform("texSize", GLSLValue(Point2i(texWidth, texHeight)));
  postProcessShader.setUniform("brightness", GLSLValue(float(GlobalBrightness)));
  postProcessShader.setUniform("contrast", GLSLValue(float(GlobalContrast)));
  /*
   * if(not fast_draw and UnsharpStrength != 0)
   *{
   *  post_processShader.setUniform("unsharp", GLSLValue(true));
   *  post_processShader.setUniform("amount", GLSLValue(UnsharpStrength));
   *  post_processShader.setUniform("kernel", GLSLValue(unsharp_kernel, 9));
   *}
   * else
   */
  postProcessShader.setUniform("unsharp", GLSLValue(false));
  postProcessShader.useShaders();
  postProcessShader.setupUniforms();
  REPORT_GL_ERROR("draw: post processing shaders");

  drawColorTexture(-1);
  REPORT_GL_ERROR("draw: final draw");

  postProcessShader.stopUsingShaders();

  // ### Begin draw overlay

  drawSelectRect();
  drawSelectLasso();
  drawVoxelCursor();
  REPORT_GL_ERROR("draw: selection tools");

  // Draw the legend and scale bar
  drawScaleBar();
  REPORT_GL_ERROR("draw: scale bar");
  drawColorBar();
  REPORT_GL_ERROR("draw: color bar");

  // ### End draw overlay

  glfuncs->glEnable(GL_DEPTH_TEST);

  if(fast_draw)
    ImgData::Slices = oldSlices;

  REPORT_GL_ERROR("draw: final");
}

void MorphoViewer::clipEnable()
{
  // Turn on clipping planes
  clip1.drawClip();
  clip2.drawClip();
  clip3.drawClip();
}

void MorphoViewer::clipDisable()
{
  // Turn off clipping planes
  clip1.disable();
  clip2.disable();
  clip3.disable();
}

void MorphoViewer::drawScaleBar()
{
  try {
    ImgData::scaleBar.setScale(1e-6); // We work in microns
    ImgData::scaleBar.draw(this, current_device);
  }
  catch(const QString& s) {
    mdxInfo << "MorphoViewer::drawScaleBar() - " << s << endl;
  }
  catch(...) {
    mdxInfo << "MorphoViewer::drawScaleBar() - Unknown exception" << endl;
  }
}

void MorphoViewer::drawColorBar()
{
  MorphoDynamX* mdx = mainWindow();
  if(!mdx) {
    mdxInfo << "MorphoViewer::drawColorBar Could not find main window" << endl;
    return;
  }
  ImgData *stk;
  if(mdx->activeMesh() == 0)
    stk = stack1;
  else if(mdx->activeMesh() == 1)
    stk = stack2;
  else {
    mdxInfo << "MorphoViewer::drawColorBar Invalid stack" << endl;
    return;
  }
  Mesh &mesh = *stk->mesh;
  // Return if no mesh selected or mesh is not visible
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return;

  CCDrawParms &cdp = mesh.drawParms(ccName);
  if(!cdp.isVisible())
    return;

  // We go through the render groups to see which one to attach to the color bar
  for(const QString &groupName : cdp.groupList())
  {
    // The group must be visible and drawing something
    if(!cdp.isGroupVisible(groupName) or cdp.renderChoiceList(groupName).isEmpty())
      continue;
    const QString &choiceName = cdp.currentRenderChoice(groupName);
    const RenderChoice &choice = cdp.renderChoice(groupName,choiceName);
    // It must be drawing areas, not dots or lines
    if(choice.type != GL_TRIANGLES) 
      continue;

    const ColorMap &colorMap = cdp.colorMap(choice.colorMap);

    // Color map must be ranged
    if(!colorMap.isRangeMap())
      continue;

    // The first thing that fills these requirements is attached to the color bar
    for(uint channelId = 0 ; channelId < colorMap.numChannels() ; channelId++) {
      if(colorMap.channelMap(channelId).visible) {
        ImgData::colorBar.setBounds(colorMap.channelMap(channelId).bounds);
        ImgData::colorBar.setLabel(colorMap.channelMap(channelId).unit);
        ImgData::colorBar.draw(colorMap, channelId, current_device);
        break;
      }
    }
    break;
  }
}

void MorphoViewer::drawVoxelCursor()
{
  if(!voxelEditCursor)
    return;
  {
    startScreenCoordinatesSystem();
    glfuncs->glDisable(GL_LIGHTING);
    glfuncs->glDisable(GL_TEXTURE_1D);
    glfuncs->glDisable(GL_TEXTURE_2D);
    glfuncs->glDisable(GL_TEXTURE_3D);

    Color3f pixelColor = Colors::getColor(Colors::VoxelEditColor);

    double voxelEditRadius = ImgData::VoxelEditRadius;

    if(DEBUG) {
      mdxInfo << "Drawing voxelEditCursor around mouse position " 
         << mousePos.x() << "x" << mousePos.y() << endl;
      mdxInfo << "   Color: " << pixelColor << " - Radius: " << voxelEditRadius << endl;
    }

    glfuncs->glLineWidth(1.0);
    Point2f c(mousePos.x(), mousePos.y());
    glfuncs->glBegin(GL_LINE_LOOP);
    glfuncs->glColor4fv(pixelColor.c_data());
    glfuncs->glNormal3d(0, 0, 1);
    for(int i = 0; i < 24; ++i) {
      double a = 2 * M_PI * double(i) / 24;
      Point2f p = c + voxelEditRadius * Point2f(cos(a), sin(a));
      glfuncs->glVertex2fv(p.c_data());
    }
    glfuncs->glEnd();
    stopScreenCoordinatesSystem();
  }

  /*
     {
     QPainter paint(this);
     paint.setRenderHint(QPainter::HighQualityAntialiasing);
     QPen pen(Colors::getColor(Colors::VoxelEditColor));
     pen.setWidth(0);
     paint.setPen(pen);
     paint.setBrush(Qt::NoBrush);
     paint.drawEllipse(mousePos, ImgData::VoxelEditRadius, ImgData::VoxelEditRadius);
     }
   */

  if(DrawClipBox) {
    glfuncs->glDisable(GL_LIGHTING);
    glfuncs->glDisable(GL_TEXTURE_1D);
    glfuncs->glDisable(GL_TEXTURE_2D);
    glfuncs->glDisable(GL_TEXTURE_3D);

    ImgData* stk = findSelectStack();
    if(stk) {
      Point3d bBox[2] = { stk->imageToWorld(stk->bBoxTex[0]), stk->imageToWorld(stk->bBoxTex[1]) };
      if(bBox[0].x() < bBox[1].x() and bBox[0].y() < bBox[1].y() and bBox[0].z() < bBox[1].z()) {
        glfuncs->glMatrixMode(GL_MODELVIEW);
        glfuncs->glPushMatrix();
        glfuncs->glMultMatrixd(stk->getFrame().worldMatrix());
        glfuncs->glBegin(GL_LINES);
        for(int i = 0; i < 2; i++)
          for(int j = 0; j < 2; j++) {
            for(int k = 0; k < 2; k++)
              glfuncs->glVertex3d(bBox[k].x(), bBox[i].y(), bBox[j].z());
            for(int k = 0; k < 2; k++)
              glfuncs->glVertex3d(bBox[i].x(), bBox[k].y(), bBox[j].z());
            for(int k = 0; k < 2; k++)
              glfuncs->glVertex3d(bBox[i].x(), bBox[j].y(), bBox[k].z());
          }
        glfuncs->glEnd();

        glfuncs->glPopMatrix();
      }
    }
  }
}

void MorphoViewer::drawSelectRect()
{
  if(guiActionOn and (guiAction == MESH_RECT_SEL_VERTICES or guiAction == MESH_RECT_SEL_FACES) 
                            and selectRect.topLeft() == selectRect.bottomRight())
    return;

  startScreenCoordinatesSystem();
  glfuncs->glDisable(GL_LIGHTING);
  glfuncs->glDisable(GL_TEXTURE_1D);
  glfuncs->glDisable(GL_TEXTURE_2D);
  glfuncs->glDisable(GL_TEXTURE_3D);
  glfuncs->glEnable(GL_BLEND);
  glfuncs->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glfuncs->glDisable(GL_DEPTH_TEST);

  glfuncs->glColor4f(0.2, 0.2, 0.2f, 0.5f);
  glfuncs->glPolygonMode(GL_FRONT, GL_FILL);
  glfuncs->glBegin(GL_QUADS);
  glfuncs->glVertex2i(selectRect.left(), selectRect.top());
  glfuncs->glVertex2i(selectRect.right(), selectRect.top());
  glfuncs->glVertex2i(selectRect.right(), selectRect.bottom());
  glfuncs->glVertex2i(selectRect.left(), selectRect.bottom());
  glfuncs->glEnd();

  glfuncs->glLineWidth(2.0);
  glfuncs->glColor4f(0.5f, 0.5f, 0.5f, 0.5f);
  glfuncs->glBegin(GL_LINE_LOOP);
  glfuncs->glVertex2i(selectRect.left(), selectRect.top());
  glfuncs->glVertex2i(selectRect.right(), selectRect.top());
  glfuncs->glVertex2i(selectRect.right(), selectRect.bottom());
  glfuncs->glVertex2i(selectRect.left(), selectRect.bottom());
  glfuncs->glEnd();

  stopScreenCoordinatesSystem();
}

void MorphoViewer::drawSelectLasso()
{
  if(guiActionOn and (guiAction == MESH_RECT_SEL_VERTICES or guiAction == MESH_RECT_SEL_FACES)
                            and selectLasso.contour.size() == 0)
    return;

  startScreenCoordinatesSystem();
  glfuncs->glDisable(GL_LIGHTING);
  glfuncs->glDisable(GL_TEXTURE_1D);
  glfuncs->glDisable(GL_TEXTURE_2D);
  glfuncs->glDisable(GL_TEXTURE_3D);
  glfuncs->glEnable(GL_BLEND);
  glfuncs->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glfuncs->glDisable(GL_DEPTH_TEST);

  glfuncs->glLineWidth(3.0);
  glfuncs->glColor4f(0.7f, 0.7f, 0.7f, 0.7f);
  glfuncs->glBegin(GL_LINE_LOOP);
  for(uint i=0;i<selectLasso.contour.size();i++)
  glfuncs->glVertex2i(selectLasso.contour[i].x(),selectLasso.contour[i].y());
  glfuncs->glEnd();

  stopScreenCoordinatesSystem();
}


void MorphoViewer::setLighting(ImgData *stack)
{
  glfuncs->glEnable(GL_COLOR_MATERIAL);
  glfuncs->glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glfuncs->glMatrixMode(GL_MODELVIEW);
  glfuncs->glPushMatrix();
  glfuncs->glLoadIdentity();

  // Set lighting, 4 lights
  // GLfloat lightc[4] = {stack->mesh->brightness()/8.0f, stack->mesh->brightness()/8.0f,
  // stack->mesh->brightness()/8.0f, 1.0f };
  float br = stack->mesh->brightness();
  float lc = br / 8.0f;
  float la = br / 8.0f;
  float ls = GlobalSpecular * br;
  Colorf lightc(lc, lc, lc, 1.0f);
  Colorf lights(ls, ls, ls, 1.0f);
  Colorf lighta(la, la, la, 1.0f);

  for(int i = 0; i < 4; ++i) {
    glfuncs->glLightfv(GL_LIGHT0 + i, GL_AMBIENT, lighta.c_data());
    glfuncs->glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, lightc.c_data());
    glfuncs->glLightfv(GL_LIGHT0 + i, GL_SPECULAR, lights.c_data());
  }

  glfuncs->glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, GlobalShininess);
  glfuncs->glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, lights.c_data());

  Point4f lightp0(1.0f, -1.0f, 1.0f, 0.0f);
  Point4f lightp1(-1.0f, -1.0f, 1.0f, 0.0f);
  Point4f lightp2(1.0f, 1.0f, 1.0f, 0.0f);
  Point4f lightp3(-1.0f, 1.0f, 1.0f, 0.0f);

  glfuncs->glLightfv(GL_LIGHT0, GL_POSITION, lightp0.c_data());
  glfuncs->glLightfv(GL_LIGHT1, GL_POSITION, lightp1.c_data());
  glfuncs->glLightfv(GL_LIGHT2, GL_POSITION, lightp2.c_data());
  glfuncs->glLightfv(GL_LIGHT3, GL_POSITION, lightp3.c_data());

  glfuncs->glEnable(GL_LIGHT0);
  glfuncs->glEnable(GL_LIGHT1);
  glfuncs->glEnable(GL_LIGHT2);
  glfuncs->glEnable(GL_LIGHT3);

  glfuncs->glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  Colorf black(0, 0, 0, 1);
  glfuncs->glLightModelfv(GL_LIGHT_MODEL_AMBIENT, black.c_data());

  glfuncs->glPopMatrix();
}

// Read from parameter file
void MorphoViewer::readParms(Parms& parms, QString section)
{
  parms(section, "DrawClipBox", DrawClipBox, false);
  parms(section, "Spinning", Spinning, 10000.0);
  camera()->frame()->setSpinningSensitivity(Spinning);
  parms(section, "FlySpeed", FlySpeed, .005);
  parms(section, "Brightness", GlobalBrightness, .0);
  parms(section, "Contrast", GlobalContrast, 1.0);
  parms(section, "Shininess", GlobalShininess, 32.0);
  parms(section, "Specular", GlobalSpecular, 0.2);
  parms(section, "UnsharpStrength", UnsharpStrength, 1.0);
  parms(section, "MaxNbPeels", MaxNbPeels, 10);
  if(MaxNbPeels < 1)
    MaxNbPeels = 1;
  CameraFrame = Matrix4d::identity();;
  CameraFrame[3][2] = -1000;
  parms(section, "CameraFrame", CameraFrame, CameraFrame);
  camera()->setFromModelViewMatrix(CameraFrame.data());
  parms(section, "SceneRadius", SceneRadius, 500.0);
  sceneRadius = SceneRadius;

  double zoom;
  parms(section, "CameraZoom", zoom, 1.0);
  _camera->setZoom(zoom);

  clip1.readParms(parms, "Clip1");
  clip2.readParms(parms, "Clip2");
  clip3.readParms(parms, "Clip3");
}

// Write to parameter file
void MorphoViewer::writeParms(QTextStream& pout, QString section)
{
  pout << endl;
  pout << "[" << section << "]" << endl;
  pout << "DrawClipBox: " << (DrawClipBox ? "true" : "false") << endl;
  pout << "Spinning: " << Spinning << endl;
  pout << "FlySpeed: " << FlySpeed << endl;
  pout << "Brightness: " << GlobalBrightness << endl;
  pout << "Contrast: " << GlobalContrast << endl;
  pout << "Shininess: " << GlobalShininess << endl;
  pout << "Specular: " << GlobalSpecular << endl;
  pout << "UnsharpStrength: " << UnsharpStrength << endl;
  pout << "MaxNbPeels: " << MaxNbPeels << endl;
  camera()->getModelViewMatrix(CameraFrame.data());
  pout << "CameraFrame: " << CameraFrame << endl;
  pout << "SceneRadius: " << SceneRadius << endl;
  pout << "CameraZoom: " << _camera->zoom() << endl;
  pout << endl;

  clip1.writeParms(pout, "Clip1");
  clip2.writeParms(pout, "Clip2");
  clip3.writeParms(pout, "Clip3");
}

// Set the current label and color
void MorphoViewer::setLabel(int label)
{
  uint idx = label % ImgData::LabelColors.size();
  if(idx >= ImgData::LabelColors.size())
    return;

  QPixmap pix(24, 24);
  if(label == 0)
    pix.fill(QColor(0, 0, 0, 0));
  else {
    Point4f col = Point4f(ImgData::LabelColors[idx]);
    pix.fill(QColor(col.x() * 255, col.y() * 255, col.z() * 255));
  }
  QIcon icon = QIcon(pix);
  selectedLabel = label;
  emit selectLabelChanged(label);
  emit setLabelColor(icon);
}

MorphoDynamX* MorphoViewer::mainWindow()
{
  QWidget* p = this;
  MorphoDynamX* mdx = 0;
  do {
    p = p->parentWidget();
    mdx = dynamic_cast<MorphoDynamX*>(p);
  } while(p and !mdx);
  return mdx;
}

// Find which stack to use for pixel editing
ImgData* MorphoViewer::findSelectStack()
{
  if(processRunning())
    return 0;
  MorphoDynamX* mdx = mainWindow();
  if(!mdx) {
    mdxInfo << "Could not find main window" << endl;
    return 0;
  }
  if(mdx->activeStack() == 0 and stack1->stack->work()->isVisible() and stack1->valid())
    return stack1;
  if(mdx->activeStack() == 1 and stack2->stack->work()->isVisible() and stack2->valid())
    return stack2;
  return (0);
}

// Find which stack to use for label selection
ImgData* MorphoViewer::findSelectSurf()
{
  if(processRunning())
    return 0;
  MorphoDynamX* mdx = mainWindow();
  if(!mdx) {
    mdxInfo << "Could not find main window" << endl;
    return 0;
  }
  if(mdx->activeMesh() == 0)
    return stack1;
  if(mdx->activeMesh() == 1)
    return stack2;
  return (0);
}

// Find which mesh to use for mesh selection
ImgData* MorphoViewer::findSelectMesh()
{
  if(processRunning())
    return 0;
  MorphoDynamX* mdx = mainWindow();
  if(!mdx) {
    mdxInfo << "Could not find main window" << endl;
    return 0;
  }
  if(mdx->activeMesh() == 0) {
    if(!stack1->mesh->ccName().isEmpty())
      return stack1;
  }
  if(mdx->activeMesh() == 1) {
    if(!stack2->mesh->ccName().isEmpty())
      return stack2;
  }
  return (0);
}

// Find selected triangle, respect the clipping planes
CCIndex MorphoViewer::findSelectFace(ImgData* stk, int x, int y)
{
  clipEnable();
  CCIndex face = stk->findSelectFace(x, y);
  clipDisable();
  return face;
}

// Get interface variables
void MorphoViewer::getGUIFlags(QMouseEvent* e)
{
  mousePos = e->pos();

  shiftPressed = (e->modifiers() & Qt::ShiftModifier);
  altPressed = (e->modifiers() & Qt::AltModifier);
  controlPressed = (e->modifiers() & Qt::ControlModifier);

  leftButton = (e->button() == Qt::LeftButton or (e->buttons() & Qt::LeftButton));
  rightButton = (e->button() == Qt::RightButton or (e->buttons() & Qt::RightButton));
}

// Get interface variables
void MorphoViewer::getGUIFlags(QKeyEvent* e)
{
  shiftPressed = (e->modifiers() & Qt::ShiftModifier);
  altPressed = (e->modifiers() & Qt::AltModifier);
  controlPressed = (e->modifiers() & Qt::ControlModifier);
}

void MorphoViewer::keyReleaseEvent(QKeyEvent* e)
{
  getGUIFlags(e);
  if(guiAction == STACK_VOXEL_EDIT) {
    checkVoxelCursor(altPressed);
    updateAll();
  }
  // Update the cell edges (usually when the alt key is released)
  if(needCellEdgeUpdate) {
    ImgData *stk = findSelectSurf();
    if(stk) {
      Mesh &mesh = *stk->mesh;

      QString ccName = mesh.ccName();
      if(!ccName.isEmpty()) {
        mesh.updateCellEdges(ccName);
        mesh.updateCC();
        updateAll();
      }
    }
    needCellEdgeUpdate = false;
  }

  // Cannot depend on getting Qt::Key_Alt
  if(guiActionOn and not altPressed) {
    QMouseEvent ev(QEvent::MouseButtonRelease, QPoint(), Qt::LeftButton, Qt::LeftButton, Qt::AltModifier);
    callGuiAction(guiAction, &ev);
    guiActionOn = false;
  }
  QGLViewer::keyReleaseEvent(e);
}

void MorphoViewer::enterEvent(QEvent* e)
{
  checkVoxelCursor(false);
  setFocus(Qt::MouseFocusReason);
  updateAll();
  QGLViewer::enterEvent(e);
}

void MorphoViewer::leaveEvent(QEvent* e)
{
  checkVoxelCursor(false);
  updateAll();
  QGLViewer::leaveEvent(e);
}

// Keyboard actions, return true if handled, false to pass through
void MorphoViewer::keyPressEvent(QKeyEvent* e)
{
  int key = e->key();
  Qt::KeyboardModifiers mod = e->modifiers();
  double fly = FlySpeed;
  bool handled = false;
  getGUIFlags(e);

  // Grab x,y,z directions in correct frame
  Vec x(1.0, 0.0, 0.0), y(0.0, 1.0, 0.0), z(0.0, 0.0, 1.0), revpt(0, 0, 0);
  if(manipulatedFrame() and manipulatedFrame() != camera()->frame()) {
    x = manipulatedFrame()->transformOf(camera()->frame()->inverseTransformOf(x));
    y = manipulatedFrame()->transformOf(camera()->frame()->inverseTransformOf(y));
    z = manipulatedFrame()->transformOf(camera()->frame()->inverseTransformOf(z));
    fly *= -1;
    // Change revolve aroung pt or not?
  }
  if(key == Qt::Key_Alt and guiAction == STACK_VOXEL_EDIT) {
    checkVoxelCursor(true);
    handled = true;
  } else if(mod == Qt::NoModifier) {
    switch(key) {
    case Qt::Key_Minus:
      FlySpeed *= .9;
      FlySpeed = trim(FlySpeed, .001, M_PI);
      handled = true;
      break;
    case Qt::Key_Right:     // rotate left/right
      manipulatedFrame()->rotateAroundPoint(qglviewer::Quaternion(z, fly), revpt);
      handled = true;
      break;
    case Qt::Key_Left:
      manipulatedFrame()->rotateAroundPoint(qglviewer::Quaternion(z, -fly), revpt);
      handled = true;
      break;
    case Qt::Key_Up:     // move in/out, for translations always use camera frame
      manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, 0.0, -fly)));
      handled = true;
      break;
    case Qt::Key_Down:
      manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, 0.0, fly)));
      handled = true;
      break;
    case Qt::Key_Delete:
    case Qt::Key_Backspace:
      handled = true;
      emit deleteSelection();
      break;
    }
  } else if(mod == Qt::ShiftModifier) {
    switch(key) {
    case Qt::Key_Plus:
      FlySpeed *= 1.1;
      FlySpeed = trim(FlySpeed, .001, M_PI);
      handled = true;
      break;
    case Qt::Key_Right:     // the translations
      manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(-fly, 0.0, 0.0)));
      handled = true;
      break;
    case Qt::Key_Left:
      manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(fly, 0.0, 0.0)));
      handled = true;
      break;
    case Qt::Key_Up:
      manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, -fly, 0.0)));
      handled = true;
      break;
    case Qt::Key_Down:
      manipulatedFrame()->translate(camera()->frame()->inverseTransformOf(Vec(0.0, fly, 0.0)));
      handled = true;
      break;
    }
  } else if(mod == Qt::ControlModifier) {
    switch(key) {
    case Qt::Key_Right:     // the other rotations
      manipulatedFrame()->rotateAroundPoint(qglviewer::Quaternion(y, fly), revpt);
      handled = true;
      break;
    case Qt::Key_Left:
      manipulatedFrame()->rotateAroundPoint(qglviewer::Quaternion(y, -fly), revpt);
      handled = true;
      break;
    case Qt::Key_Up:
      manipulatedFrame()->rotateAroundPoint(qglviewer::Quaternion(x, fly), revpt);
      handled = true;
      break;
    case Qt::Key_Down:
      manipulatedFrame()->rotateAroundPoint(qglviewer::Quaternion(x, -fly), revpt);
      handled = true;
      break;
    }
  }
  if(handled)
    updateAll();
  else if(!altPressed)
    QGLViewer::keyPressEvent(e);
}

void MorphoViewer::checkVoxelCursor(bool altPressed)
{
  if(!voxelEditCursor and altPressed and guiAction == STACK_VOXEL_EDIT) {
    ImgData* stk = findSelectStack();
    if(stk and stk->stack->work()->isVisible()) {
      voxelEditCursor = true;
      qApp->setOverrideCursor(QCursor(Qt::BlankCursor));
    }
  } else if(voxelEditCursor and !(altPressed and guiAction == STACK_VOXEL_EDIT)) {
    voxelEditCursor = false;
    qApp->restoreOverrideCursor();
  }
}

int MorphoViewer::findLabel(const Store* store, Point3d start, Point3d dir) const
{
  const Stack* stk = store->stack();
  size_t loop_count = 0;
  start = Point3d(stk->getFrame().coordinatesOf(Vec(start)));
  dir = Point3d(stk->getFrame().transformOf(Vec(dir)));
  start = stk->worldToImaged(start);
  dir = stk->worldToImageVectorf(dir);
  Point3d end = start + dir;
  dir = normalized(dir);
  if(DEBUG) {
    mdxInfo << "findLabel() - stack coordinates" << endl;
    mdxInfo << "Start = " << start << " - End = " << end << " - dir = " << dir << endl;
  }
  int incx = dir.x() > 0 ? 1 : -1;
  int incy = dir.y() > 0 ? 1 : -1;
  int incz = dir.z() > 0 ? 1 : -1;
  Point3d incr_l(0, 0, 0);
  if(dir.x() > 0)
    incr_l.x() = 1;
  if(dir.y() > 0)
    incr_l.y() = 1;
  if(dir.z() > 0)
    incr_l.z() = 1;
  Point3u grid_size = stk->size();
  if(fabs(dir.x()) > EPSILON) {
    if(incx > 0) {
      start -= dir * (start.x() / dir.x());
      end -= dir * ((end.x() - grid_size.x()) / dir.x());
    } else {
      start -= dir * (start.x() - grid_size.x()) / dir.x();
      end -= dir * (end.x() / dir.x());
    }
    if(fabs(dir.y()) > EPSILON) {
      if(incy > 0) {
        if(start.y() < 0)
          start -= dir * (start.y() / dir.y());
        if(end.y() > grid_size.y())
          end -= dir * ((end.y() - grid_size.y()) / dir.y());
      } else {
        if(start.y() > grid_size.y())
          start -= dir * (start.y() - grid_size.y()) / dir.y();
        if(end.y() <= 0)
          end -= dir * (end.y() / dir.y());
      }
    }
    if(fabs(dir.z()) > EPSILON) {
      if(incz > 0) {
        if(start.z() < 0)
          start -= dir * (start.z() / dir.z());
        if(end.z() > grid_size.z())
          end -= dir * ((end.z() - grid_size.z()) / dir.z());
      } else {
        if(start.z() > grid_size.z())
          start -= dir * (start.z() - grid_size.z()) / dir.z();
        if(end.z() <= 0)
          end -= dir * (end.z() / dir.z());
      }
    }
  } else if(fabs(dir.y()) > EPSILON) {
    if(incy > 0) {
      start -= dir * (start.y() / dir.y());
      end -= dir * ((end.y() - grid_size.y()) / dir.y());
    } else {
      start -= dir * (start.y() - grid_size.y()) / dir.y();
      end -= dir * (end.y() / dir.y());
    }
    if(fabs(dir.z()) > EPSILON) {
      if(incz > 0) {
        if(start.z() < 0)
          start -= dir * (start.z() / dir.z());
        if(end.z() > grid_size.z())
          end -= dir * ((end.z() - grid_size.z()) / dir.z());
      } else {
        if(start.z() > grid_size.z())
          start -= dir * (start.z() - grid_size.z()) / dir.z();
        if(end.z() <= 0)
          end -= dir * (end.z() / dir.z());
      }
    }
  } else {
    if(incz > 0) {
      start -= dir * (start.z() / dir.z());
      end -= dir * ((end.z() - grid_size.z()) / dir.z());
    } else {
      start -= dir * (start.z() - grid_size.z()) / dir.z();
      end -= dir * (end.z() / dir.z());
    }
  }
  // Check if we cross the grid at all
  if((start.x() < 0 and end.x() < 0)or (start.x() > grid_size.x() and end.x() > grid_size.x())
    or (start.y() < 0 and end.y() < 0) or (start.y() > grid_size.y() and end.y() > grid_size.y())
    or (start.z() < 0 and end.z() < 0) or (start.z() > grid_size.z() and end.z() > grid_size.z())) {
    // No label possible
    return 0;
  }
  // Now, compute the intersection with the clipping planes
  Clip* clips[3] = { c1, c2, c3 };
  double smm[16];
  stk->getFrame().getMatrix(smm);
  Matrix4f sm(smm);
  Point4f hstart(stk->imageToWorld(start));
  hstart[3] = 1.f;
  Point4f hend(stk->imageToWorld(end));
  hend[3] = 1.f;
  for(int i = 0; i < 3; ++i) {
    Clip* c = clips[i];
    if(c->enabled()) {
      // Get the transform from clipping plane to stack frame
      double mm[16];
      c->frame().inverse().getMatrix(mm);
      Matrix4f cm(mm);
      Matrix4f m = sm * cm;
      Point4f c0 = m * c->normalFormPos();
      Point4f c1 = m * c->normalFormNeg();
      // Now perform the tests
      double testS0 = hstart * c0;
      double testE0 = hend * c0;
      double testS1 = hstart * c1;
      double testE1 = hend * c1;
      if((testS0 < 0 and testE0 < 0)or (testS1 < 0 and testE1 < 0)) {
        return 0;                                                     // This is completly clipped
      } else if((testS0 * testE0 < 0)or (testS1 * testE1 < 0))        // Partial clip
      {
        double lambda0 = testS0 / (testS0 - testE0);
        double lambda1 = testS1 / (testS1 - testE1);
        if(lambda0 > lambda1) {
          double a = lambda0;
          lambda0 = lambda1;
          lambda1 = a;
        }
        Point4f newStart = (lambda0 > 0) ? (1 - lambda0) * hstart + lambda0 * hend : hstart;
        Point4f newEnd = (lambda1 < 1) ? (1 - lambda1) * hstart + lambda1 * hend : hend;
        hstart = newStart;
        hend = newEnd;
      }
    }
  }

  hstart /= hstart[3];
  hend /= hend[3];

  start = stk->worldToImaged(Point3d(hstart));
  end = stk->worldToImaged(Point3d(hend));

  Point3d cp = min(Point3d(grid_size - 1u), max(Point3d(0, 0, 0), start));
  Point3i cell(map(floor, cp));
  if(DEBUG) {
    mdxInfo << "********* Start computeLine ***************" << endl;
    mdxInfo << "dir = " << dir << endl;
    mdxInfo << "start = " << start << " => " << cell << endl << endl;
  }
  // bool add_corners = false;
  size_t max_loop = grid_size.x() + grid_size.y() + grid_size.z();
  const HVecUS& data = store->data();
  while((end - start) * dir > EPSILON and loop_count++ < max_loop 
                                        and stk->offset(cell) < stk->storeSize()) {
    // Check label
    ushort label = data[stk->offset(cell)];
    if(label > 0) {
      if(DEBUG)
        mdxInfo << "Found label: " << label << endl;
      return label;
    }
    Point3d l(cell);
    Point3d dl(l - start + incr_l);
    if(DEBUG)
      mdxInfo << "l = " << l << endl;
    Point3d r = divide(dl, dir);
    if(fabs(dir.x()) < EPSILON)
      r.x() = HUGE_VAL;
    if(fabs(dir.y()) < EPSILON)
      r.y() = HUGE_VAL;
    if(fabs(dir.z()) < EPSILON)
      r.z() = HUGE_VAL;
    Point3d ar = fabs(r);
    if(DEBUG)
      mdxInfo << "dl = " << dl << endl;
    if(DEBUG)
      mdxInfo << "r = " << r << endl;
    if(ar.x() <= ar.y() and ar.x() <= ar.z()) {
      start += dir * r.x();
      cell.x() += incx;
      if(ar.y() == ar.x())
        cell.y() += incy;
      if(ar.z() == ar.x())
        cell.z() += incz;
    } else if(ar.y() <= ar.x() and ar.y() <= ar.z()) {
      start += dir * r.y();
      cell.y() += incy;
      if(ar.z() == ar.y())
        cell.z() += incz;
    } else {
      start += dir * r.z();
      cell.z() += incz;
    }
    if(DEBUG)
      mdxInfo << "New start point = " << start << " => " << cell << endl << endl;
  }
  if(DEBUG) {
    if(loop_count >= max_loop) {
      mdxWarning << "Error, too many loop count" << endl;
    }
    mdxInfo << "######### End computeLine ###############" << endl;
  }
  return 0;
}
Point3d MorphoViewer::pointUnderPixel(const QPoint &pos, bool &found)
{
  QPoint texPos = QPoint(pos.x(), drawHeight - pos.y() - 1);
  double depth = depthTexture[texPos.y() * drawWidth + texPos.x()];
  found = depth < 1.0;
  Vec point(pos.x(), pos.y(), depth);
  point = camera()->unprojectedCoordinatesOf(point);
  return Point3d(point);
}

void MorphoViewer::mouseDoubleClickEvent(QMouseEvent* e)
{
  if(e->button() == Qt::LeftButton and e->buttons() == (Qt::LeftButton | Qt::RightButton)
     and e->modifiers() == Qt::NoModifier) {
    bool found;
    Point3d pup = pointUnderPixel(e->pos(), found);
    if(found) {
      camera()->setPivotPoint(Vec(pup));
      setVisualHintsMask(1);
      updateAll();
    }
  } else
    QGLViewer::mouseDoubleClickEvent(e);
}

void MorphoViewer::labelColor()
{
  setLabel(0);
  updateAll();
}

void MorphoViewer::updateSceneRadius()
{
  //  bool wasEmpty = sceneIsEmpty;
  sceneRadius = 0;
  for(int i = 0; i < 3; i++) {
    if(sceneRadius < fabs(stack1->stack->size()[i] * stack1->stack->step()[i]))
      sceneRadius = fabs(stack1->stack->size()[i] * stack1->stack->step()[i]);
    if(sceneRadius < fabs(stack2->stack->size()[i] * stack2->stack->step()[i]))
      sceneRadius = fabs(stack2->stack->size()[i] * stack2->stack->step()[i]);
  }
  for(int i = 0; i < 2; i++)
    for(int j = 0; j < 3; j++) {
      if(sceneRadius < fabs(stack1->mesh->boundingBox()[i][j]))
        sceneRadius = fabs(stack1->mesh->boundingBox()[i][j]);
      if(sceneRadius < fabs(stack2->mesh->boundingBox()[i][j]))
        sceneRadius = fabs(stack2->mesh->boundingBox()[i][j]);
    }
  sceneIsEmpty = sceneRadius == 0;
  if(sceneIsEmpty)
    sceneRadius = SceneRadius;
  sceneRadius *= 5.0; // Add some room

  camera()->setSceneRadius(sceneRadius);
  cutSurf->setSceneBoundingBox(Point3d(sceneRadius, sceneRadius, sceneRadius));
// RSS This will reset the zoom and translation loaded from an mgxv file the first time
// Is it needed? 
//  if(wasEmpty) 
//    camera()->showEntireScene();
}

//  Takes care of GUI others
void MorphoViewer::resetView()
{
  Matrix4d m = Matrix4d::identity();
  stack1->getMainFrame().setFromMatrix(m.data());
  stack1->getTransFrame().setFromMatrix(m.data());
  stack2->getMainFrame().setFromMatrix(m.data());
  stack2->getTransFrame().setFromMatrix(m.data());
  c1->frame().setFromMatrix(m.data());
  c2->frame().setFromMatrix(m.data());
  c3->frame().setFromMatrix(m.data());
  cutSurf->cut->frame().setFromMatrix(m.data());

  camera()->setFromModelViewMatrix(m.data());
  camera()->setSceneCenter(Vec(0, 0, 0));
  updateSceneRadius();
  camera()->showEntireScene();
  _camera->resetZoom();

  updateAll();
}

void MorphoViewer::reloadShaders()
{
  textureSurfShader.invalidate();
  volumeSurfShader1.invalidate();
  volumeSurfShader2.invalidate();
  colorSurfShader.invalidate();
  flatSurfShader.invalidate();
  raycastingShader1.invalidate();
  raycastingShader2.invalidate();
  updateAll();
}

void MorphoViewer::updateLabels()
{
  stack1->LabelColorsChanged = true;
  stack2->LabelColorsChanged = true;
  updateAll();
}

void MorphoViewer::updateAll()
{
  update();
}

void MorphoViewer::loadFile(const QString& pth, bool loadStack2, bool loadWork)
{
  QFileInfo fi(pth);
  QString ext = fi.suffix();
  ImgData* stack = (loadStack2) ? stack2 : stack1;

  if(ext == "fct") {
    stack->setColorMap(pth, loadWork);
    updateAll();
  }
}

bool MorphoViewer::saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling, bool expand,
                                     bool hasGUI)
{
  return saveImageSnapshot(fileName, finalSize, oversampling, expand, hasGUI, 0);
}

bool MorphoViewer::saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling, bool,
                                     bool hasGUI, QString* error)
{
  if(!finalSize.isValid())
    finalSize = QSize(this->width(), this->height());
  QSize textureSize = oversampling * finalSize;
  if(oversampling != 1.0) {
    ImgData::scaleBar.scaleDrawing(oversampling);
    ImgData::colorBar.scaleDrawing(oversampling);
  }
  int savedWidth = drawWidth;
  int savedHeight = drawHeight;
  // drawWidth = textureSize.width();
  // drawHeight = textureSize.height();
  QGLFramebufferObjectFormat fboFormat;
  fboFormat.setAttachment(QGLFramebufferObject::Depth);
  QGLFramebufferObject fbo(textureSize, fboFormat);
  if(!fbo.bind()) {
    if(hasGUI)
      QMessageBox::critical(0, "Error creating FBO",
                            QString("Unable to create a FBO of size %1x%2").arg(drawWidth).arg(drawHeight));
    if(error)
      *error = QString("Unable to create a FBO of size %1x%2").arg(drawWidth).arg(drawHeight);
    resizeGL(savedWidth, savedHeight);
    return false;
  }
  resizeGL(textureSize.width(), textureSize.height());
  baseFboId = fbo.handle();

  preDraw();
  draw(&fbo);
  postDraw();

  baseFboId = 0;
  resizeGL(savedWidth, savedHeight);

  if(!fbo.release()) {
    if(hasGUI)
      QMessageBox::critical(0, "Error releasing FBO",
                            QString("Unable to release the FBO of size %1x%2").arg(drawWidth).arg(drawHeight));
    if(error)
      *error = QString("Unable to release the FBO of size %1x%2").arg(drawWidth).arg(drawHeight);
    return false;
  }

  QImage img = fbo.toImage();

  if(oversampling != 1.0) {
    img = img.scaled(finalSize, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    ImgData::scaleBar.restoreScale();
    ImgData::colorBar.restoreScale();
  }

  img.save(fileName);

  return true;
}

static const QString extToFilter[8][2] = { 
  { QString("jpeg"), QString("JPEG") }, { QString("jpg"), QString("JPEG") },
  { QString("png"), QString("PNG") }, { QString("bmp"), QString("BMP") },
  { QString("ppm"), QString("PPM") }, { QString("eps"), QString("EPS") },
  { QString("ps"), QString("PS") }, { QString("xfig"), QString("XFIG") } };

static const QString filters = QObject::tr("JPEG images") + " (*.jpeg *.jpg)" + ";;" 
  + QObject::tr("PNG images") + " (*.png)" + ";;" + QObject::tr("PPM images") 
  + " (*.ppm)" + ";;" + QObject::tr("BMP images") + " (*.bmp)";

void MorphoViewer::recordMovie(bool on)
{
  if(on) {
    QString filename, filetype;
    QString selectedFilter;
    filename = QFileDialog::getSaveFileName(this, "Record movie", "", filters, &selectedFilter);
    if(!filename.isEmpty()) {
      // Find out the type of the selected file
      QString filetype;
      for(int i = 0; i < 8; ++i) {
        QString ext = "." + extToFilter[i][0];
        if(filename.endsWith(ext, Qt::CaseInsensitive)) {
          filetype = extToFilter[i][1];
          filename = filename.left(filename.size() - filetype.size());
          break;
        }
      }
      if(filetype.isEmpty()) {
        int index = selectedFilter.indexOf(' ');
        filetype = selectedFilter.left(index);
      }
      setSnapshotCounter(0);
      setSnapshotFileName(filename);
      setSnapshotFormat(filetype);
      connect(this, SIGNAL(drawFinished(bool)), this, SLOT(saveScreenshot(bool)));
    } else {
      emit recordingMovie(false);
    }
  } else
    disconnect(this, SIGNAL(drawFinished(bool)), this, SLOT(saveScreenshot(bool)));
}

void MorphoViewer::clipEnable(Clip* c, bool _val)
{
  if(_val)
    c->enable();
  else
    c->disable();
  updateAll();
}

void MorphoViewer::clipGrid(Clip* c, bool _val)
{
  if(_val)
    c->showGrid();
  else
    c->hideGrid();
  updateAll();
}

void MorphoViewer::clipWidth(Clip* c, int _val)
{
  c->setWidth(0.5 * sceneRadius * exp(double(_val) / 2000.0));
  if(c->grid() or c->enabled())
    updateAll();
}

// Repeat is used from the label dialog to select a specific color.
// RSS this routine is used only by the label dialog
void MorphoViewer::selectMeshLabel(int label, int repeat, bool replace)
{
//  ImgData* stk = findSelectSurf();
//  if(mesh.showMesh()) {
//    if(label > 0 or repeat > 0) {
//      if(replace)
//        stk->clearMeshSelect();
//      if(mesh.useParents()) {
//        stk->selectParent(label, repeat);
//        Information::setStatus(QString("Selected parent %1.").arg(label));
//      } else {
//        stk->selectLabel(label, repeat);
//        Information::setStatus(QString("Selected label %1.").arg(label));
//      }
//    }
//  } else
//    Information::setStatus("Selection operations require a visible mesh");
}

/*
 * These are the user interaction routines.
 * It would be nice to create a plug-in mechanism for this, so do not change the uniform 
 * interface if possible
 */
// Label operations, selection, filling, deleting
bool MorphoViewer::stackLabel(QMouseEvent *e, GuiAction action)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Do nothing for release
  if(e->type() == QEvent::MouseButtonRelease or e->type() == QEvent::MouseMove)
    return true;

  ImgData *stk = findSelectStack();
  if(!stk) {
    Information::setStatus("Error finding stack.");
    stackPickFill = true;
    return false;
  }
  Store* store = stk->stack->currentStore();
  if(!store->isVisible() or !store->labels()) {
    Information::setStatus("Label operations require a labeled stack.");
    stackPickFill = true;
    return false;
  }
  if(action != STACK_PICK_LABEL and store != stk->stack->work()) {
    Information::setStatus("You can only edit the work store.");
    stackPickFill = true;
    return false;
  }

  Vec vorig, vdir;
  camera()->convertClickToLine(e->pos(), vorig, vdir);
  int label = findLabel(store, Point3d(vorig), Point3d(vdir));
  if(label <= 0)
    return false;
  if(action == STACK_PICK_LABEL) {
    setLabel(label);
    Information::setStatus(QString("Picked label %1.").arg(label));
  } else if(action == STACK_DEL_LABEL) {
    stk->fillLabel("Work", label, 0);
    Information::setStatus(QString("Cleared label %1.").arg(label));
  } else if(action == STACK_FILL_LABEL) {
    if(label == selectedLabel)
      return false;
    stk->fillLabel("Work", label, selectedLabel);
    Information::setStatus(QString("Filled label %1 with %2.").arg(label).arg(selectedLabel));
  } else if(action == STACK_PICK_FILL_LABEL) {
    if(stackPickFill) {
      setLabel(label);
      stackPickFill = false;
      Information::setStatus(QString("Picked label for %1 for pick-fill.").arg(label));
    } else {
      stackPickFill = true;
      if(label == selectedLabel)
        return false;
      stk->fillLabel("Work", label, selectedLabel);
      Information::setStatus(QString("Filled label %1 with %2.").arg(label).arg(selectedLabel));
    }
  } else {
    mdxInfo << "ImgData::stackLabel Unknown action." << endl;
    return false;
  }
  return true;
}

// Do pixel editing operations
bool MorphoViewer::stackVoxelEdit(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Find which surface selected
  ImgData* stk = findSelectStack();
  if(stk == 0)
    return false;

  // First process release
  if(e->type() == QEvent::MouseButtonRelease) {
    stk->voxelEditStop();
    return true;
  }
 
  // Start the editing process
  if(e->type() == QEvent::MouseButtonPress)
    stk->voxelEditStart(clip1, clip2, clip3);

  // Then edit current position
  if(e->type() == QEvent::MouseButtonPress or e->type() == QEvent::MouseMove) {
    Point3d p(0, 0, 0), x(1, 0, 0), y(0, 1, 0), z(0, 0, 1);
    // Find select point
    bool surf = false;
    if(stk->stack->work()->isVisible() and cutSurf->cut->isVisible()) {
      if(!stk->findSeedPoint(e->pos().x(), e->pos().y(), *cutSurf, p))
        return false;
      else
        surf = true;
    } else {
      p = (Point3d)camera()->unprojectedCoordinatesOf(Vec(e->pos().x(), e->pos().y(), 0.0), &stk->getFrame());
      x = (Point3d)camera()->unprojectedCoordinatesOf(Vec(e->pos().x() + 1, e->pos().y(), 0.0), &stk->getFrame());
      y = (Point3d)camera()->unprojectedCoordinatesOf(Vec(e->pos().x(), e->pos().y() - 1, 0.0), &stk->getFrame());
      z = (Point3d)camera()->unprojectedCoordinatesOf(Vec(e->pos().x(), e->pos().y(), 1.0), &stk->getFrame());
      x = (x - p).normalize();
      y = (y - p).normalize();
      z = (z - p).normalize();
    }
    pixelRadius = ImgData::VoxelEditRadius * camera()->pixelGLRatio(Vec(p));
    stk->voxelEdit(pixelRadius, p, x, y, z, surf, selectedLabel);
  } else
    return false;

  return true;
}

// Move points when right button is pressed
bool MorphoViewer::meshMovePoints(QMouseEvent *e)
{
  bool cutSurfActive = cutSurf->cut->drawGrid();
  ImgData* stk = findSelectMesh();
  if(!stk)
    return false;
  Mesh &mesh = *stk->mesh;

  // Cell complex stuff
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;
  CCIndexDataAttr &indexAttr = mesh.indexAttr();

  // First process release
  if(e->type() == QEvent::MouseButtonRelease) {
    mesh.updatePositions();
    mesh.updateCC();
  } else if(e->type() == QEvent::MouseButtonPress) {
    // Save old position
    oldPos = e->pos();
    if(cutSurfActive) {
      // Save reference point
      oldVPos = Point3d(camera()->unprojectedCoordinatesOf(
                           Vec(Point3d(oldPos.x(), oldPos.y(), 0)), cutSurf->getFrame()));
    } else if(stk) {   // Process mesh point select
      // Save reference point
      oldVPos = Point3d(camera()->unprojectedCoordinatesOf(
                           Vec(Point3d(oldPos.x(), oldPos.y(), 0)), &stk->getFrame()));
    } else
      return false;
  } else if(e->type() == QEvent::MouseButtonPress or e->type() == QEvent::MouseMove) {
    if(rightButton) {
      // handle the moving of points
      Point3d s(0,0,0);

      // If we hit the control key while moving it will cancel
      if(controlPressed) {
        s.x() = oldPos.x();
        s.y() = oldPos.y();
      } else {
        s.x() = e->pos().x();
        s.y() = e->pos().y();
      }

      Point3d newVPos;
      if(cutSurfActive) {
        // Move bezier points
        newVPos = Point3d(camera()->unprojectedCoordinatesOf(Vec(s), cutSurf->getFrame()));
        forall(const uint u, cutSurf->selectV)
          cutSurf->cut->bezier().bezierV()[u] += Point3d(newVPos - oldVPos);
        cutSurf->cut->hasChanged();
      } else {
        // Move mesh points
        newVPos = Point3d(camera()->unprojectedCoordinatesOf(Vec(s), &stk->getFrame()));
        Point3d deltaP(newVPos - oldVPos);

        // Update selected vertices
        CCDrawParms &cdp = mesh.drawParms(ccName);
        #pragma omp parallel for
        for(uint i = 0; i < cdp.vertexSelected.size(); i++)
          indexAttr[cdp.vertexSelected[i]].pos += deltaP;

        mesh.updateSelectPositions();
        mesh.updateCC();
      }
      oldVPos = newVPos;
    }
  }
  return true;
}

// Start mesh selecting/editing operations
bool MorphoViewer::meshSelect(QMouseEvent *e, GuiAction action)
{
  bool cutSurfActive = cutSurf->cut->drawGrid();
  ImgData* stk = findSelectMesh();
  if(!stk)
    return false;
  Mesh &mesh = *stk->mesh;

  // Cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;
  CCIndexDataAttr &indexAttr = mesh.indexAttr();
  bool doVertices = (action == MESH_RECT_SEL_VERTICES or action == MESH_LASSO_SEL_VERTICES);
  bool doFaces = (action == MESH_RECT_SEL_FACES or action == MESH_LASSO_SEL_FACES);
  bool doVolumes = (action == MESH_RECT_SEL_VOLUMES or action == MESH_LASSO_SEL_VOLUMES);
  bool doLasso = (action == MESH_LASSO_SEL_VERTICES or action == MESH_LASSO_SEL_FACES or action == MESH_LASSO_SEL_VOLUMES);
  if(!(doVertices or doFaces or doVolumes)) {
    mdxInfo << "MorphoViewer::meshSelect Bad action: " << action << endl;
    return false;
  }

  // First process release
  if(e->type() == QEvent::MouseButtonRelease) {
    if(cutSurfActive) {
      // Handle cutting surface
      if(leftButton) {
        for(uint i = 0; i < cutSurf->cut->bezier().bezPoints().x() * cutSurf->cut->bezier().bezPoints().y(); i++) {
          Point3d pos = Point3d(camera()->projectedCoordinatesOf(Vec(cutSurf->cut->bezier().bezierV()[i]), cutSurf->getFrame()));
          if((doLasso and selectLasso.LassoContains(pos)) or (!doLasso and selectRect.contains(QPoint(int(pos.x() + .5), int(pos.y() + .5)))))
            cutSurf->selectV.insert(i);
        }
      }
    } else if(stk) {
      // Handle mesh, add geometry to selected set
 
      // Respect clip planes, first find the position of the origin and the normal vector
      Point3d orig1 = Point3d(c1->frame().inverseCoordinatesOf(Vec(0, 0, 0)));
      Point3d orig2 = Point3d(c2->frame().inverseCoordinatesOf(Vec(0, 0, 0)));
      Point3d orig3 = Point3d(c3->frame().inverseCoordinatesOf(Vec(0, 0, 0)));
      // Then the normal
      Point3d norm1 = Point3d(c1->frame().inverseCoordinatesOf(Vec(c1->normal()))) - orig1;
      Point3d norm2 = Point3d(c2->frame().inverseCoordinatesOf(Vec(c2->normal()))) - orig2;
      Point3d norm3 = Point3d(c3->frame().inverseCoordinatesOf(Vec(c3->normal()))) - orig3;

      if(leftButton) {
        // Handle selection on cell complex
        if(!ccName.isEmpty()) {
          const CCStructure &cs = mesh.ccStructure(ccName);
          CCDrawParms &cdp = mesh.drawParms(ccName);
          const CCIndexVec &cells = doFaces ? cs.faces() : doVolumes ? cs.volumes() : cs.vertices();
          #pragma omp parallel for
          for(uint i = 0; i < cells.size(); i++) {
            CCIndex c = cells[i];
            CCIndexData &cIdx = indexAttr[c];
            Point3d cpos = multiply(Point3d(cIdx.pos), stk->stack->scale());
            Point3d ccpos = Point3d(stk->getFrame().inverseCoordinatesOf(Vec(cpos)));
    
            // Check if clipped, 
            if((!c1->enabled() or (fabs((ccpos - orig1) * norm1) < c1->width()))
               and (!c2->enabled() or (fabs((ccpos - orig2) * norm2) < c2->width()))
               and (!c3->enabled() or (fabs((ccpos - orig3) * norm3) < c3->width()))) {
              Point3d pos = Point3d(camera()->projectedCoordinatesOf(Vec(cpos), &stk->getFrame()));
              if((doLasso and selectLasso.LassoContains(pos)) or 
                       (!doLasso and selectRect.contains(QPoint(int(pos.x() + .5), int(pos.y() + .5))))) {
                if(controlPressed) {
                  if(cIdx.selected) {
                    cIdx.selected = false;
                    if(doVertices)
                      cdp.vertexChanged.push_back(c);
                    else if(doFaces)
                      cdp.faceChanged.push_back(c);
                    else if(doVolumes)
                      cdp.volumeChanged.push_back(c);
                  }
                } else {
                  if(!cIdx.selected) {
                    cIdx.selected = true;
                    if(doVertices)
                      cdp.vertexChanged.push_back(c);
                    else if(doFaces)
                      cdp.faceChanged.push_back(c);
                    else if(doVolumes)
                      cdp.volumeChanged.push_back(c);
                  }
                }
              }
            }
          }
          if(cdp.vertexChanged.size() > 0) {
            mesh.updateVertexSelect(ccName); 
            mesh.updateCC();
          }
          if(cdp.faceChanged.size() > 0) {
            mesh.updateFaceSelect(ccName); 
            mesh.updateCC();
          }
          if(cdp.volumeChanged.size() > 0) {
            mesh.updateVolumeSelect(ccName); 
            mesh.updateCC();
          }
        }  
      }
    } else
      return false;

    if(doLasso) {
      selectLasso.Clear();
      selectLasso.AddPoint(Point3d(oldPos.x(),oldPos.y(),0));
    } else {
      selectRect.setTopRight(oldPos);
      selectRect.setBottomLeft(oldPos);
    }

    return true;
  }
  if(e->type() == QEvent::MouseButtonPress) {
    oldPos = e->pos();
    if(doLasso) {
      selectLasso.Clear();
      selectLasso.AddPoint(Point3d(e->pos().x(),e->pos().y(),0));  
    } else {
      // Save position and reset selection rectangle
      selectRect.setBottomRight(e->pos());
      selectRect.setTopLeft(e->pos());
    }
    // Clear previous selection if required
    if(cutSurfActive) {
      // Save reference point
      oldVPos = Point3d(camera()->unprojectedCoordinatesOf(
                           Vec(Point3d(oldPos.x(), oldPos.y(), 0)), cutSurf->getFrame()));

      // If shift not pressed clear selection
      if(leftButton and !shiftPressed and !controlPressed)
        cutSurf->clearSelect();
    } else if(stk) {   // Process mesh point select
      // Save reference point
      oldVPos = Point3d(camera()->unprojectedCoordinatesOf(
                           Vec(Point3d(oldPos.x(), oldPos.y(), 0)), &stk->getFrame()));
      // If shift not pressed clear selection
      if(leftButton and !shiftPressed and !controlPressed) {
        if(doVertices)
          mesh.clearSelectVertices(ccName);
        else if(doFaces)
          mesh.clearSelectFaces(ccName);
        else if(doVolumes)
          mesh.clearSelectVolumes(ccName);
      }
    } else
      return false;
  }
  if(e->type() == QEvent::MouseButtonPress or e->type() == QEvent::MouseMove) {
    if(leftButton) {
      if(doLasso) {
        // Add point to Lasso 
        selectLasso.AddPoint(Point3d(e->pos().x(),e->pos().y(),0));
      } else {
        // Handle sizing of select rectangle
        selectRect.setBottomRight(oldPos);
        selectRect.setTopLeft(e->pos());
        selectRect = selectRect.normalized();
      }
    } else 
      return false;
  } else
    return false;

  return true;
}

// Pick the label color from the mesh
bool MorphoViewer::meshPickLabel(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Do nothing for release
  if(e->type() == QEvent::MouseButtonRelease)
    return true;

  // Find selected surface and triangle
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;
  int label = 0;

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;
  label = mesh.getLabel(mesh.indexAttr()[face].label);
	if(label < 0)
    return false;
  setLabel(label);
  Information::setStatus(QString("Picked %1, %2 %3").arg(label).arg(mesh.labeling()).arg(mesh.labelName()[label]));

  return true;
}

// Add a new seed for each click
bool MorphoViewer::meshAddSeed(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Do nothing for release
  if(e->type() == QEvent::MouseButtonRelease)
    return true;

  // Find selected surface and triangle
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Get cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;
  CCIndexDataAttr &indexAttr = mesh.indexAttr();

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;

  // Do not seed over an existing label
  auto &fIdx = indexAttr[face];
  int label = fIdx.label;
  if(label > 0)
    return false;

  // If shift pressed use current label
  if(shiftPressed or e->type() ==  QEvent::MouseMove)
    label = mesh.viewLabel();
  else
    label = mesh.nextLabel();

  // Add to labeling table if required
  if(mesh.labeling() != "Labels")
    mesh.labelMap(mesh.labeling())[label] = label;

  mesh.setFaceLabel(ccName, label, CCIndexVec({face})); 
  setLabel(label);
  needCellEdgeUpdate = true;

  if(mesh.useParents())
    Information::setStatus(QString("Label and parent %1 added.").arg(label));
  else
    Information::setStatus(QString("Label %1 added.").arg(label));

  return true;
}

// Add the current seed
bool MorphoViewer::meshCurrentSeed(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Do nothing for release
  if(e->type() == QEvent::MouseButtonRelease)
    return true;

  // Find selected surface and triangle
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Get cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;

  // Add to parent table if required
  if(mesh.labeling() != "Labels")
    mesh.labelMap(mesh.labeling())[selectedLabel] = selectedLabel;

  mesh.setFaceLabel(ccName, selectedLabel, CCIndexVec({face})); 
  needCellEdgeUpdate = true;

  Information::setStatus(QString("Label %1 seeded, labeling: %2").arg(selectedLabel).arg(mesh.labeling()));

  return true;
}

// Add the current seed
bool MorphoViewer::meshDrawSignal(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Do nothing for release
  if(e->type() == QEvent::MouseButtonRelease)
    return true;

  // Find selected surface and triangle
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Get cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;

  controlPressed = (e->modifiers() & Qt::ControlModifier);
  //shiftPressed = (e->modifiers() & Qt::ShiftModifier);

  double signal = controlPressed ? 0.0 : 65535.0;
  QString signalName = mesh.signal();
  if(signalName.isEmpty())
    signalName = "Signal";
  auto &signalAttr = mesh.signalAttr<double>(signalName);

  mesh.setFaceSignal(ccName, signalAttr, signal, {face});

  return true;
}

// Fill selected label with current label
bool MorphoViewer::meshFillLabel(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Find selected surface and triangle
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Get cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;
  CCIndexDataAttr &indexAttr = mesh.indexAttr();

  // On release do nothing
  if(e->type() == QEvent::MouseButtonRelease and needCellEdgeUpdate)
    return true;

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;

  auto fIdx = indexAttr[face];
  int label = fIdx.label;

  if(label <= 0)
    return false;

  if(mesh.labeling() != "Labels")
    mesh.labelMap(mesh.labeling())[selectedLabel] = selectedLabel;
  if(mesh.useParents())
    mesh.setLabelLabeling(ccName, mesh.labeling(), selectedLabel, IntSet({label}));
  else if(label != selectedLabel)
    mesh.fillLabel(ccName, selectedLabel, IntSet({label}));
  needCellEdgeUpdate = true;

  Information::setStatus(QString("Area with label %1 filled with label %2, labeling %3.")
                                           .arg(label).arg(selectedLabel).arg(mesh.labeling()));
  return true;
}

// Pick and fill labels 
bool MorphoViewer::meshPickFillLabel(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Find selected surface and triangle
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Get cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;
  CCIndexDataAttr &indexAttr = mesh.indexAttr();

  if(e->type() == QEvent::MouseButtonPress) {
    CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
    if(face == CCIndex::UNDEF) {
      meshPickFill = true;
      return false;
    }
    auto fIdx = indexAttr[face];
    int label = fIdx.label;

    if(label <= 0) {
      meshPickFill = true;
      return false;
    }
    if(meshPickFill) {
      setLabel(label);
      meshPickFill = false;
    } else {
      meshPickFill = true;
      if(mesh.labeling() != "Labels") {
        if(label <= 0)
          return false;
        mesh.setLabelLabeling(ccName, mesh.labeling(), selectedLabel, IntSet({label}));
      } else
        mesh.fillLabel(ccName, selectedLabel, IntSet({label}));
      needCellEdgeUpdate = true;
    }
  }
  return true;
}

// Add the current label to the selection
bool MorphoViewer::meshSelectLabel(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Find selected surface and triangle
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Get cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;
  CCIndexDataAttr &indexAttr = mesh.indexAttr();

  // Do nothing release
  if(e->type() == QEvent::MouseButtonRelease)
    return true;

  // Clear selection if shift not pressed
  if(e->type() == QEvent::MouseButtonPress and !shiftPressed)
    mesh.clearSelectFaces(ccName);

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;
  auto &fIdx = indexAttr[face];
  int label = fIdx.label;

  if(label <= 0)
    return false;

  if(controlPressed)
    mesh.removeSelectLabel(ccName, label);
  else
    mesh.addSelectLabel(ccName, label);

  label = mesh.getLabel(label); 

  Information::setStatus(QString("%1 %2, %3 %4").arg(controlPressed ? "De-selected" : "Selected").
                                            arg(label).arg(mesh.labeling()).arg(mesh.labelName()[label]));

  setLabel(label);

  return true;
}

// Add a connected region to the selection
bool MorphoViewer::meshSelectConnected(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Do nothing for release
  if(e->type() == QEvent::MouseButtonRelease)
    return true;

  // Find selected surface and triangle, print error msg only on press
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Get cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;

  // Select connected areas
  if(!shiftPressed and !controlPressed)
    mesh.clearSelectVertices(ccName);

  mesh.selectConnectedFaces(ccName, face, !controlPressed);

  return true;
}

// Add a triangle to the selection
bool MorphoViewer::meshSelectFace(QMouseEvent *e, GuiAction action)
{
  // Only use left button
  if(!leftButton or e->type() == QEvent::MouseButtonRelease)
    return false;

  // Grab the stack
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;

  // Gui action
  bool doFaces = (action == MESH_SEL_FACE);
  bool doVolumes = (action == MESH_SEL_VOLUME);
  if(!(doFaces or doVolumes)) {
    mdxInfo << "MorphoViewer::meshSelectFace Bad action: " << action << endl;
    return false;
  }

  // Clear existing selection if required
  if(e->type() == QEvent::MouseButtonPress)
    if(!shiftPressed and !controlPressed) {
      if(doFaces)
        mesh.clearSelectFaces(ccName);
      else if(doVolumes)
        mesh.clearSelectVolumes(ccName);
    }

  if(e->type() == QEvent::MouseButtonPress or e->type() == QEvent::MouseMove) {
    CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
    if(face == CCIndex::UNDEF)
      return false;

    if(doFaces) {
      std::vector<CCIndex> fVec;
      fVec.push_back(face);
      if(controlPressed)
        mesh.removeSelectFaces(ccName, fVec);
      else
        mesh.addSelectFaces(ccName, fVec);
    } else if(doVolumes) {
      const CCStructure &cs = mesh.ccStructure(ccName);
      CCDrawParms &cdp = mesh.drawParms(ccName);
      CCIndexData &fIdx = mesh.indexAttr()[face];
      for(CCIndex volume : cs.cobounds(face)) {
        bool front = (camera()->frame()->transformOf(stk->getFrame().inverseTransformOf(Vec(fIdx.nrml)))[2] > 0);
        bool ro = (cs.ro(face, volume) == ccf::POS);

        if(ro == front) {
          CCIndexData &lIdx = mesh.indexAttr()[volume];
          if(controlPressed and lIdx.selected)
            lIdx.selected = false;
          else if(!lIdx.selected)
            lIdx.selected = true;
          else
            break;
          cdp.volumeChanged.push_back(volume);
          break;
        }
      }
      if(cdp.volumeChanged.size() > 0) {
        mesh.updateVolumeSelect(ccName); 
        mesh.updateCC();
      }
    }
  }

  return true;
}

// Grab the label from the other mesh to use as seed
bool MorphoViewer::meshGrabSeed(QMouseEvent *e)
{
  // Only use left button
  if(!leftButton)
    return false;

  // Save the label from MousePress for MouseMove, on Release it is cleared
  static int olabel = 0;
  if(e->type() == QEvent::MouseButtonRelease) {
    olabel = 0;
    return true;
  }

  // Find selected surface and triangle, print error msg only on press
  ImgData *stk = findSelectSurf();
  if(stk == 0)
    return false;
  Mesh &mesh = *stk->mesh;

  // Cell complex
  QString ccName = mesh.ccName();
  if(ccName.isEmpty())
    return false;
  CCIndexDataAttr &indexAttr = mesh.indexAttr();

  CCIndex face = findSelectFace(stk, e->pos().x(), e->pos().y());
  if(face == CCIndex::UNDEF)
    return false;
  auto &fIdx = indexAttr[face];
  int label = fIdx.label;

  // Get other stack
  ImgData* ostk = stack1;
  if(stk == stack1)
    ostk = stack2;
  if(ostk->mesh->empty())
    return false;

  if(e->type() == QEvent::MouseButtonPress) {
    CCIndex face = findSelectFace(ostk, e->pos().x(), e->pos().y());
    if(face == CCIndex::UNDEF)
      return false;
    auto &fIdx = ostk->mesh->indexAttr()[face];
    olabel = fIdx.label;
  }

  // Return if unlabeled in other mesh
  if(olabel <= 0) {
    setLabel(0);
    return false;
  }

  // If grabbing parent add new seed (and map) or just map if already colored
  if(mesh.labeling() != "Labels") {
    if(label > 0)
      mesh.setLabelLabeling(ccName, mesh.labeling(), olabel, IntSet({label}));
  } else
    mesh.setFaceLabel(ccName, olabel, CCIndexVec({face})); 
  needCellEdgeUpdate = true;

  setLabel(olabel);

  if(mesh.useParents())
    Information::setStatus(QString("Parent label %1 grabbed.").arg(olabel));
  else if(label > 0)
    Information::setStatus(QString("Seed %1 grabbed.").arg(olabel));

  return true;
}

bool MorphoViewer::callGuiAction(GuiAction guiAction, QMouseEvent *e)
{
  // Right button moves points
  if(rightButton)
    return meshMovePoints(e);

  // Left button is used for editing operation
  switch(guiAction) {
    // Stack operations on labels
    case STACK_PICK_LABEL:
    case STACK_FILL_LABEL:
    case STACK_PICK_FILL_LABEL:
    case STACK_DEL_LABEL:
      return stackLabel(e, guiAction);

    // 3D voxel editing with magic wand
    case STACK_VOXEL_EDIT:
      return stackVoxelEdit(e);

    // Mesh selection tools with rectangle or lasso
    case MESH_RECT_SEL_VERTICES:
    case MESH_RECT_SEL_FACES:
    case MESH_RECT_SEL_VOLUMES:
    case MESH_LASSO_SEL_VERTICES:
    case MESH_LASSO_SEL_FACES:
    case MESH_LASSO_SEL_VOLUMES:
      return meshSelect(e, guiAction);

    // Pick the label color
    case MESH_PICK_LABEL:
      return meshPickLabel(e);

    // Add a new seed
    case MESH_ADD_SEED:
      return meshAddSeed(e);

    // Add the current seed
    case MESH_CURR_SEED:
      return meshCurrentSeed(e);

    // Draw the signal on the mesh
    case MESH_DRAW_SIGNAL:
      return meshDrawSignal(e);

    // Grab the seed from the other mesh
    case MESH_GRAB_SEED:
      return meshGrabSeed(e);

    // Fill with a label
    case MESH_FILL_LABEL:
      return meshFillLabel(e);

    // Pick and Fill the label
    case MESH_PICK_FILL_LABEL:
      return meshPickFillLabel(e);

    // Select by label
    case MESH_SEL_LABEL:
      return meshSelectLabel(e);

    // Select connected regions
    case MESH_SEL_CONN:
      return meshSelectConnected(e);

    // Select by clicking on face
    case MESH_SEL_FACE:
    case MESH_SEL_VOLUME:
      return meshSelectFace(e, guiAction);
  }
  return false;
}

void MorphoViewer::mousePressEvent(QMouseEvent *e)
{
  lastButtons = e->buttons();
  getGUIFlags(e);

  // Alt-left button for select/edit
  bool handled = false;
  if(altPressed) 
    handled = callGuiAction(guiAction, e);

  if(handled) {
    guiActionOn = true;
    updateAll();
  }

  //if(!altPressed)
  else
    QGLViewer::mousePressEvent(e);
}

void MorphoViewer::mouseMoveEvent(QMouseEvent* e)
{
  getGUIFlags(e);
  bool handled = false;

  // Check for missed events
  if(guiActionOn and (not altPressed or not (leftButton or rightButton))) {
    QMouseEvent ev(QEvent::MouseButtonRelease, e->pos(), Qt::LeftButton, 0, 0);
    callGuiAction(guiAction, &ev);
    guiActionOn = false;
  }

  // Check 3D editing cursor
  if(guiAction == STACK_VOXEL_EDIT) {
    checkVoxelCursor(altPressed);
    if(altPressed)
      handled = true;
  }

  // Alt left button for select/edit
  if(guiActionOn and altPressed)
    handled = callGuiAction(guiAction, e);

  if(handled)
    updateAll();
  //if(!altPressed) 
  else
    QGLViewer::mouseMoveEvent(e);
} 

void MorphoViewer::mouseReleaseEvent(QMouseEvent *e)
{
  lastButtons = e->buttons();
  getGUIFlags(e);

  bool handled = false;
  if(guiActionOn) {
    handled = callGuiAction(guiAction, e);
    guiActionOn = false;
  }

  if(handled) {
    updateAll();
  }

  // Always pass through
  QGLViewer::mouseReleaseEvent(e);
}


