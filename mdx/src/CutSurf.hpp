//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

// Draw the cutting surface. 

#ifndef CUT_SURF_H
#define CUT_SURF_H

#include <Config.hpp>
#include <GL.hpp>

#include <Color.hpp>
#include <CuttingSurface.hpp>
#include <Geometry.hpp>
#include <MDXViewer/qglviewer.h>
#include <Parms.hpp>

namespace mdx 
{
  typedef Color<double> Color3d;
  class Shader;
  class ImgData;
  class Stack;
  
  // Class to add GUI control on top of the cutting surface class
  class mdx_EXPORT CutSurf : public QObject 
  {
    Q_OBJECT
  public:
    CutSurf();
    ~CutSurf();
  
    CuttingSurface* cut;
  
    std::set<uint> selectV;   // List of selected vertices
  
  protected:
    uint BlendSlices;
    uint Material;
    double LineWidth;
  
    void drawSurface(ImgData& stk, bool select);
  
  public:
    // Read clipping plane parameters
    void readParms(Parms& parms, QString section);
  
    // write parms to file
    void writeParms(QTextStream& out, QString section);
  
    // Draw cutting plane
    void drawCutSurfPlane(ImgData& stk, bool select, Shader* shader = 0);
    void drawCutSurfGrid(ImgData& stk);
  
    // Get frame
    qglviewer::ManipulatedFrame* getFrame() { return &cut->frame(); }
  
    // Clear selection
    void clearSelect();
  
    // Return true if the cutting surface is visible
    bool isVisible();
  
    double getSceneRadius() {
      return SceneRadius;
    };
  
  protected:
    double SceneRadius;
    double getSize(int val);
  
  public slots:
    // Set sizes
    void drawCutSurf(bool val);
    void cutSurfThreeAxis(bool val);
    void cutSurfGrid(bool val);
    void cutSurfPlane(bool val);
    void cutSurfBezier(bool val);
    void sizeX(int val);
    void sizeY(int val);
    void sizeZ(int val);
    void reset(double sceneRadius);
    void setSceneBoundingBox(const Point3d& bbox);
  
  signals:
    void viewerUpdate();
  };
}
#endif
