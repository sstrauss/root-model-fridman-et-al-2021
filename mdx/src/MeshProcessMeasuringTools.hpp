//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_MEASURE_TOOLS_HPP
#define MESH_PROCESS_MEASURE_TOOLS_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <MeshUtils.hpp>
#include <MDXSubdivide.hpp>

namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{

  /**
   * \class VertexPairDistance <MeshProcessMeaseureTools.hpp>
   *
   * Compute the distance between two vertices. Subdivide (triangle) faces
   */
  class mdxBase_EXPORT VertexPairDistance : public Process
  {
  // RSS FIXME Does this also convert a mesh to triangles? If so, maybe it could be split
  public:
    VertexPairDistance(const Process& process) : Process(process) 
    {
      setName("Mesh/Measuring Tools/Vertex Pair Distance");
      setDesc("Find the distance between a pair of selected vertices");
      setIcon(QIcon(":/images/Ruler.png"));
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());
      QString ccName = mesh->ccName();
      if(ccName.isEmpty()) 
        throw QString("%1::run No cell complex").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      return run(*mesh, cs);
    }
    bool run(Mesh &mesh, const CCStructure &cs);
  };
}
#endif
