/*
 * The Cell Complex Framework
 * Brendan Lane 2018
 */
#ifndef CCF_CLASSES_HPP
#define CCF_CLASSES_HPP

#include <Config.hpp>

#include <algorithm>
#include <stdexcept>
#include <typeinfo>

#include <set>
#include <map>
#include <vector>

#include <string>
#include <iostream>

#include <Types.hpp>
#include <CCIndex.hpp>

#include <Information.hpp>

namespace mdx
{
  template<typename Index> struct CCStructureInterface;
  /**
   * Cell Complex library namespace
   */
  namespace ccf
  {
    template<typename Index> struct CellStructure;

    /***
     ***  Things to do with relative orientation: oriented objects, boundary chains, etc.
     ***/

    /// OrientedSet is a set of oriented objects, with some map-like operations.
    template<typename Index>
    struct OrientedSet
    {
      typedef OrientedObject<Index> OObj;
      typedef std::set<OObj> SetType;
      SetType sdata;

      /* OrientedSet reflected types */
      typedef OObj value_type;
      typedef typename SetType::size_type size_type;
      typedef typename SetType::difference_type difference_type;
      typedef typename SetType::reference reference;
      typedef typename SetType::const_reference const_reference;
      typedef typename SetType::pointer pointer;
      typedef typename SetType::const_pointer const_pointer;
      typedef typename SetType::const_iterator const_iterator;

      /* OrientedSet constructors */
      OrientedSet(void) : sdata() {}
      OrientedSet(const OrientedSet& os) : sdata(os.sdata) {}
      explicit OrientedSet(const Index& t) : sdata() { insert(t); }
      explicit OrientedSet(const OrientedObject<Index>& oo) : sdata() { insert(oo); }

      /* OrientedSet set methods */
      inline const_iterator begin() const { return sdata.begin(); }
      inline const_iterator end() const { return sdata.end(); }

      inline bool empty(void) const { return sdata.empty(); }
      inline size_type size(void) const { return sdata.size(); }
      inline size_type max_size() const { return sdata.max_size(); }

      inline void erase(const_iterator pos) { sdata.erase(pos); }
      inline void erase(const_iterator first,const_iterator last) { sdata.erase(first,last); }

      inline void clear() { sdata.clear(); }
      inline void swap(OrientedSet& other) { sdata.swap(other.sdata); }

      inline size_type erase(const value_type &oobj) { return sdata.erase(oobj); }
      inline size_type count(const value_type &oobj) const { return sdata.count(oobj); }
      inline const_iterator find(const value_type &oobj) const { return sdata.find(oobj); }

      void insert(const value_type &oobj)
      {
        const_iterator iter = find(~oobj);
        if(iter == end())
          sdata.insert(oobj);
        else if(iter->orientation() != oobj.orientation())
          sdata.erase(iter);
        else
          mdxWarning << "OrientedSet::insert: object " << to_string(oobj).c_str()
                     << " is already in set." << endl;
      }

      /* OrientedSet map methods */
      const_iterator find(const Index &index) const
      {
        const_iterator iter = sdata.lower_bound(OObj(index,POS));
        return (iter == end() || ~(*iter) != index) ? end() : iter;
      }
      size_type count(const Index &index) const
      {
        return (find(index) == end()) ? 0 : 1;
      }
      size_type erase(const Index &index)
      {
        const_iterator iter = find(index);
        return (iter == end()) ? 0 : sdata.erase(iter);
      }

      inline void insert(const Index &index) { insert(OObj(index,POS)); }

      RO operator[](const Index &index) const
      {
        const_iterator iter = find(index);
        return (iter == end()) ? POS : iter->orientation();
      }

      /* OrientedSet operations */
      /* OrientedSet assignment operators */
      OrientedSet& operator=(const OrientedSet& os)
      { sdata = os.sdata; return *this; }
      OrientedSet& operator=(const OrientedObject<Index>& oo)
      { clear(); insert(oo); return *this; }

      /* OrientedSet scalar multiplication operators */
      OrientedSet& operator*=(RO ro)
      {
        if(ro == NEG)
        {
          SetType newS;
          for(const_iterator iter = begin() ; iter != end() ; iter++)
            newS.insert(newS.end() , -(*iter));
          std::swap(sdata , newS);
        }
        return *this;
      }
      OrientedSet operator*(RO ro) const { OrientedSet ans(*this); return (ans *= ro); }
      OrientedSet operator-(void) const { return (*this) * NEG; }
      friend OrientedSet operator*(RO ro, const OrientedSet& os) { return os * ro; }

      /* OrientedSet +/- OrientedSet operators */
      OrientedSet operator+(const OrientedSet& os) const
      {
        OrientedSet ans;
        const_iterator it1 = begin() , it2 = os.begin();
        while(it1 != end() && it2 != os.end())
        {
          if(~(*it1) < ~(*it2)) ans.sdata.insert(ans.sdata.end(),*it1++);
          else if(~(*it2) < ~(*it1)) ans.sdata.insert(ans.sdata.end(),*it2++);
          else if(it1->orientation() == it2->orientation()) // key and orientation equal
          {
            mdxWarning << "OrientedSet::operator+: object " << to_string(*it1).c_str()
                       << " is in both sets." << endl;
            ans.sdata.insert(ans.sdata.end(),*it1);
            it1++; it2++;
          }
          else { it1++; it2++; } // key equal, orientation different
        }
        while(it1 != end()) ans.sdata.insert(ans.sdata.end(),*it1++);
        while(it2 != os.end()) ans.sdata.insert(ans.sdata.end(),*it2++);
        return ans;
      }
      OrientedSet& operator+=(const OrientedSet& os)
      {
        for(const_iterator iter = os.begin() ; iter != os.end() ; iter++)
          this->insert(*iter);
        return (*this);
      }
      OrientedSet operator-(const OrientedSet& os) const { return (*this) + (-os); }
      OrientedSet& operator-=(const OrientedSet& os) { return (*this += -os); }

      /* OrientedSet +/- OrientedObject operators */
      OrientedSet operator+(const OrientedObject<Index>& oo) const
      { OrientedSet ans(*this); ans.insert(oo); return ans; }
      OrientedSet operator-(const OrientedObject<Index>& oo) const
      { OrientedSet ans(*this); ans.insert(-oo); return ans; }
      OrientedSet& operator+=(const OrientedObject<Index>& oo)
      { this->insert(oo); return *this; }
      OrientedSet& operator-=(const OrientedObject<Index>& oo)
      { this->insert(-oo); return *this; }

      /* OrientedSet +/- Index operators */
      OrientedSet operator+(const Index& t) const
      { OrientedSet ans(*this); ans.insert(+t); return ans; }
      OrientedSet operator-(const Index& t) const
      { OrientedSet ans(*this); ans.insert(-t); return ans; }
      OrientedSet& operator+=(const Index& t) { this->insert(+t); return *this; }
      OrientedSet& operator-=(const Index& t) { this->insert(-t); return *this; }
    };
    /* Other operators which create an OrientedSet */
    template<typename Index>
    OrientedSet<Index> operator+(const OrientedObject<Index>& o1,
                                 const OrientedObject<Index>& o2)
    { OrientedSet<Index> ans; ans.insert(o1); ans.insert(o2); return ans; }

    template<typename Index>
    OrientedSet<Index> operator-(const OrientedObject<Index>& o1,
                                 const OrientedObject<Index>& o2)
    { OrientedSet<Index> ans; ans.insert(o1); ans.insert(-o2); return ans; }
    template<typename Index,typename U>
    OrientedSet<Index> operator+(const OrientedObject<Index>& o1, const U& t2)
    { OrientedSet<Index> ans; ans.insert(o1); ans.insert(+t2); return ans; }
    template<typename Index, typename U>
    OrientedSet<Index> operator-(const OrientedObject<Index>& o1, const U& t2)
    { OrientedSet<Index> ans; ans.insert(o1); ans.insert(-t2); return ans; }

    /**
     * Main storage for the cell complex. Implemented as a hash map of vectors.
     */
    template<typename Index>
    class mdx_EXPORT FlipTable
    {
    public:
      // Variables for debugging/timing
      int cmds[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
      int loops[12] = {0,0,0,0,0,0,0,0,0,0,0,0};

      // Allow numeric access to flip entries
      typedef Flip<Index> FlipI;
      typedef std::vector<FlipI> FlipVectorI;
      typedef tbb::concurrent_vector<FlipI, TBBALLOC<FlipI> > FlipVec;
      FlipVectorI flipVec;

      /* FlipTable reflected types */
      typedef typename FlipVectorI::value_type value_type;
      typedef typename FlipVectorI::const_iterator iterator;
      typedef typename FlipVectorI::const_iterator const_iterator;
      typedef typename FlipVectorI::const_reverse_iterator reverse_iterator;
      typedef typename FlipVectorI::const_reverse_iterator const_reverse_iterator;
      typedef typename FlipVectorI::size_type size_type;
      typedef typename FlipVectorI::reference reference;
      typedef typename FlipVectorI::pointer pointer;
      typedef typename FlipVectorI::const_reference const_reference;
      typedef typename FlipVectorI::const_pointer const_pointer;

      const_iterator begin(void) const { return flipVec.begin(); }
      const_iterator end(void) const { return flipVec.end(); }
      const_reverse_iterator rbegin(void) const { return flipVec.rbegin(); }
      const_reverse_iterator rend(void) const { return flipVec.rend(); }
      const_iterator find(const FlipI &flip);
      uint findIndex(const FlipI &flip);

      struct FlipArray
      {
        typedef UIntUIntTbbMap MapType;
        typedef UIntTbbVec VecType;

        VecType vec;
        MapType *map = 0;

        FlipArray() {}
        // Need a copy constructor to use in map
        FlipArray(const FlipArray &copy)
        {
          vec = copy.vec;
          if(copy.map)
            map = new MapType(*copy.map);
        }

        ~FlipArray()
        {
          if(map)
            delete map;
        }

        // Insert does not check for dups
        void insert(const FlipVectorI &flipVec, uint idx)
        {
          auto it = vec.push_back(idx);
          if(map)
            (*map)[idx] = it - vec.begin();

        }
        // Erase and return number erased
        uint erase(const FlipVectorI &flipVec, uint idx)
        {
          if(!map and vec.size() > 30)  {
            // Create map if the vector is too big
            map = new MapType;
            uint count = 0;
            for(auto idx : vec)
              (*map)[idx] = count++;
          }
          if(map) {
            auto it = map->find(idx);
            if(it != map->end()) {
              // swap and pop
              uint i = it->second;
              // swap in both map and vec if not at end
              if(i != vec.size() - 1) {
                std::swap(vec[i], vec.back());
                (*map)[vec[i]] = i;
              }
              vec.resize(vec.size() - 1);
              map->unsafe_erase(it);

              return 1;
            }
          } else {
            for(auto it = vec.begin(); it != vec.end(); ++it) {
              if(*it == idx) {
                // swap and pop
                if(it != vec.end() - 1)
                  std::swap(*it, vec.back());
                vec.resize(vec.size() - 1);
                return 1;
              }
            }
          }
          return 0;
  			}

        // Replace indices after pop and swap on main flipVec
        uint replace(const FlipVectorI &flipVec, uint backPos, uint delPos)
        {
          if(map) {
            auto it = map->find(backPos);
            if(it != map->end())  {
              vec[it->second] = delPos;
              (*map)[delPos] = it->second;
              map->unsafe_erase(it);
              return 1;
            }
          } else {
            for(auto it = vec.begin(); it != vec.end(); ++it)
              if(*it == backPos) {
                *it = delPos;
                return 1;
              }
          }
          return 0;
        }

        // Get the data
        const VecType &get() const { return vec; }

        // Number of elements
        size_t size() const { return vec.size(); }
        bool empty() const { return vec.size() == 0; }
      };

      // Lookup maps for the flips
      tbb::concurrent_unordered_map<Index, FlipArray, std::hash<Index>, std::equal_to<Index>,
                                  TBBALLOC<std::pair<const Index, FlipArray> > > flipMap;

      FlipTable()
      {
        flipMap.max_load_factor(1.0);
      }

      bool empty(void) const { return flipVec.empty(); }
      size_t size(void) const { return flipVec.size(); }

      void clear(void)
      {
        flipMap.clear();
        flipVec.clear();
      }

      // Erase a flip
      size_t erase(const FlipI &flip)
      {
        if(flipVec.empty())
          return 0;

        // See if flip exists and finds its location
        uint delPos = findIndex(flip);
        if(delPos == flipVec.size())
          return 0;

        // I
        auto iIt = flipMap.find(flip.interior);
        if(iIt != flipMap.end()) {
          if(!iIt->second.erase(flipVec, delPos))
            mdxInfo << "FlipTable:erase Flip missing, I Vec" << endl;
          if(iIt->second.empty())
            flipMap.unsafe_erase(iIt);
        } else
          mdxInfo << "FlipTable:erase Flip missing, I" << endl;

        // J
        auto jIt = flipMap.find(flip.joint);
        if(jIt != flipMap.end()) {
          if(!jIt->second.erase(flipVec, delPos))
            mdxInfo << "FlipTable:erase Flip missing, Vec, J" << endl;
          if(jIt->second.empty())
            flipMap.unsafe_erase(jIt);
        } else
          mdxInfo << "FlipTable:erase Flip missing, J" << endl;

        // L
        auto lIt = flipMap.find(flip.facet[0]);
        if(lIt != flipMap.end()) {
          if(!lIt->second.erase(flipVec, delPos))
            mdxInfo << "FlipTable:erase Flip missing, L Vec" << endl;
          if(lIt->second.empty())
            flipMap.unsafe_erase(lIt);
        } else
          mdxInfo << "FlipTable:erase Flip missing, L" << endl;

        // R
        auto rIt = flipMap.find(flip.facet[1]);
        if(rIt != flipMap.end()) {
          if(!rIt->second.erase(flipVec, delPos))
            mdxInfo << "FlipTable:erase Flip missing, R Vec" << endl;
          if(rIt->second.empty())
            flipMap.unsafe_erase(rIt);
        } else
          mdxInfo << "FlipTable:erase Flip missing, R" << endl;

        // Swap and pop and chase old moved back entry
        uint backPos = flipVec.size() - 1;
        if(delPos != backPos) {
          auto &back = flipVec.back();

          // I
          iIt = flipMap.find(back.interior);
          if(iIt == flipMap.end() or !iIt->second.replace(flipVec, backPos, delPos))
            mdxInfo << "FlipTable:erase Flip missing chasing I" << endl;

          // J
          jIt = flipMap.find(back.joint);
          if(jIt == flipMap.end() or !jIt->second.replace(flipVec, backPos, delPos))
            mdxInfo << "FlipTable:erase Flip missing chasing J" << endl;

          // L
          lIt = flipMap.find(back.facet[0]);
          if(lIt == flipMap.end() or !lIt->second.replace(flipVec, backPos, delPos))
            mdxInfo << "FlipTable:erase Flip missing chasing L, facet 0" << endl;

          // R
          rIt = flipMap.find(back.facet[1]);
          if(rIt == flipMap.end() or !rIt->second.replace(flipVec, backPos, delPos))
            mdxInfo << "FlipTable:erase Flip missing chasing R, face 1:" << endl;

          std::swap(flipVec[delPos], flipVec.back());
        }
        flipVec.pop_back();

        return 1;
      }

      // Insert into the flip table
      bool insert(const FlipI &flip)
      {
  	    // Check for undefined index
  			if(flip.interior == CCIndex::UNDEF or flip.joint == CCIndex::UNDEF or flip.facet[0] == CCIndex::UNDEF or flip.facet[1] == CCIndex::UNDEF) {
          mdxInfo << "FlipTable::insert Attempt to insert flip with undefined index:" << ccf::to_string(flip).c_str() << endl;
          return false;
        }
  			// Check for duplicate index
        if(flip.interior == flip.joint or flip.interior == flip.facet[0] or flip.interior == flip.facet[1] or
            flip.joint == flip.facet[0] or flip.joint == flip.facet[1] or flip.facet[0] == flip.facet[1]) {
          mdxInfo << "FlipTable::insert Attempt to insert flip with duplicate index:" << ccf::to_string(flip).c_str() << endl;
          return false;
        }

        // See if flip exists and finds its location
        uint pos = findIndex(flip);
        if(pos != flipVec.size())
          return false;

        // If we get here we'll assume flip does not exist in any map
        flipVec.push_back(flip);
        flipMap[flip.interior].insert(flipVec, pos);
        flipMap[flip.joint].insert(flipVec, pos);
        flipMap[flip.facet[0]].insert(flipVec, pos);
        flipMap[flip.facet[1]].insert(flipVec, pos);

        return true;
      }

      template<typename InputIterator>
      void insert(InputIterator first, InputIterator last)
      {
        while(first != last)
          insert(*(first++));
      }

      void reindex()
      {
        // TBB requires sizes in powers of 2 (really?)
        // Round down to nearest power of 2, shouldn't be more than half as many CCIndex as flips
        flipMap.clear();
        size_t sz = 1 << (size_t)__TBB_Log2(flipVec.size()/2);
        flipMap.rehash(sz);
        #pragma omp parallel for
        for(uint i = 0; i < flipVec.size(); i++) {
          flipMap[flipVec[i].interior].insert(flipVec, i);
          flipMap[flipVec[i].joint].insert(flipVec, i);
          flipMap[flipVec[i].facet[0]].insert(flipVec, i);
          flipMap[flipVec[i].facet[1]].insert(flipVec, i);
        }
      }

      inline bool hasCell(Index cell) const
      {
        return flipMap.find(cell) != flipMap.end();
      }

      /* FlipTable match method declarations */
      FlipVectorI matchV(FlipI query) const;

      const FlipI &matchFirst(FlipI query) const;

      size_t count(const FlipI &flip) const { return matchFirst(flip) == flip ? 1 : 0; }
    };


    /***
     *** Helper classes
     ***/
    /* DimensionMap class definition */
    /// A bidirectional map between dimensions and cells.
    template<typename Index>
    struct DimensionMapG
    {
      typedef unsigned short Dimension;

      /* DimensionMap members */
      std::vector< std::vector<Index> > cellsByDimension;
      typedef std::unordered_map<Index, std::pair<Dimension,unsigned int> > DimMap;
      DimMap dimensionByCell;

      /* DimensionMap constructor */
      DimensionMapG(Dimension _maxDim) : cellsByDimension(_maxDim+1) {}

      /* DimensionMap insert method */
      void insert(Index idx,Dimension dim)
      {
	if(dim < cellsByDimension.size())
          {
            cellsByDimension[dim].push_back(idx);
            dimensionByCell[idx] = std::make_pair(dim,cellsByDimension[dim].size() - 1);
          }
	else
	  throw std::domain_error
	    (std::string("Tried to insert cell ")+to_string(idx)+" of dimension "+
	     to_string(dim)+" in dimension map with max dimension "+
	     to_string(cellsByDimension.size()));
      }

      /* DimensionMap load method */
      void load(const std::vector<std::vector<Index> > &cells)
      {
	clear();
	size_t sz = 0;
	for(Dimension dim = 0; dim < cells.size(); dim++)
	  if(dim < cellsByDimension.size()) {
	    cellsByDimension[dim] = cells[dim];
	    sz += cells[dim].size();
	  } else
	    throw std::domain_error
	      (std::string("Tried to load cells of dimension ") +
	       to_string(dim) + " in dimension map with max dimension "+
	       to_string(cellsByDimension.size()));

	dimensionByCell.reserve(sz);
	for(Dimension dim = 0; dim < cells.size(); dim++)
	  for(uint i = 0; i < cells[dim].size(); i++)
	    dimensionByCell[cells[dim][i]] = std::make_pair(dim, i);
      }

      /* DimensionMap erase method */
      void erase(Index idx)
      {
	typename DimMap::iterator iter = dimensionByCell.find(idx);
	if(iter != dimensionByCell.end())
          {
            Dimension dim = iter->second.first;
            unsigned int i = iter->second.second;
            dimensionByCell[cellsByDimension[dim].back()].second = i;
            cellsByDimension[dim][i] = cellsByDimension[dim].back();
            cellsByDimension[dim].pop_back();
            dimensionByCell.erase(iter);
          }
      }

      /* Other DimensionMap methods */
      void clear(void)
      {
	for(unsigned int i = 0 ; i < cellsByDimension.size() ; i++)
	  cellsByDimension[i].clear();
	dimensionByCell.clear();
      }
      unsigned int size(void) const { return dimensionByCell.size(); }

      /* DimensionMap hasCell method */
      bool hasCell(Index idx) const
      { return dimensionByCell.find(idx) != dimensionByCell.end(); }

      /* DimensionMap dimensionOf method */
      Dimension dimensionOf(Index idx) const
      {
	typename DimMap::const_iterator iter = dimensionByCell.find(idx);
	if(iter == dimensionByCell.end())
	  throw std::domain_error
	    (std::string("Called dimensionOf on nonexistent cell ")+to_string(idx));
	else return iter->second.first;
      }

      /* DimensionMap cellsOfDimension method */
      const std::vector<Index>& cellsOfDimension(Dimension d) const
      {
	const static std::vector<Index> EmptyVector;
	if(d < cellsByDimension.size())
	  return cellsByDimension[d];
	else return EmptyVector;
      }
    };

    /* FlipTower class declaration */
    /// Helper structure used by CellTuple.
    template<typename Index>
    struct mdx_EXPORT FlipTowerG
    {
      /* FlipTower member variables */
      bool invalid;

      typedef unsigned short Dimension;
      typedef CCStructureInterface<Index> CStructure;
      typedef Flip<Index> FlipI;

      typedef std::vector<FlipI> FlipVectorI;
      typedef typename FlipVectorI::const_iterator FlipVectorIterator;

      typedef std::map<Dimension,Index> CellMap;
      typedef typename CellMap::const_iterator CellMapIterator;
      CellMap cellMap;

      std::vector<FlipI> flips;
      const CStructure *cs;

      /* FlipTower addCell method */
      void addCell(Index c);

      /* FlipTower constructors */
      FlipTowerG(void);
      FlipTowerG(const CStructure &_cs);
      FlipTowerG(const CStructure &_cs, Index c1);
      FlipTowerG(const CStructure &_cs, Index c1, Index c2);
      FlipTowerG(const CStructure &_cs, Index c1, Index c2, Index c3);
      FlipTowerG(const CStructure &_cs, Index c1, Index c2, Index c3, Index c4);
      FlipTowerG(const CStructure &_cs, const CellMap &_cellMap);

      /* FlipTower fillCell method declarations */
      bool fillCellsBelow(CellMapIterator itTo);
      bool fillCellsAbove(CellMapIterator itFrom);
      bool fillCellsBetween(CellMapIterator itFrom, CellMapIterator itTo);
      /* FlipTower fillAllCells method */
      bool fillAllCells(void)
      {
	if(invalid) return false;
	if(cellMap.empty())
	  throw std::invalid_argument("FlipTower: trying to fill empty cell tower");
	else
          {
            CellMapIterator itTo = cellMap.begin();
            if(!fillCellsBelow(itTo)) return false;
            CellMapIterator itFrom = (itTo++);
            while(itTo != cellMap.end())
              if(!fillCellsBetween(itFrom++,itTo++)) return false;
            return fillCellsAbove(itFrom);
          }
      }

      /* FlipTower fillInteriorCells method */
      bool fillInteriorCells(void)
      {
	if(invalid) return false;
	if(cellMap.size() < 2) return true;
	else return fillCellsBetween(cellMap.begin(),--(cellMap.end()));
      }

      /* FlipTower fillBelow method */
      bool fillBelow(Dimension dim)
      {
	if(invalid) return false;
	CellMapIterator itTo = cellMap.find(dim);
	if(itTo == cellMap.end())
	  throw std::invalid_argument
	    (std::string("FlipTower: trying to fill below unset cell of dimension ")+
	     to_string(dim));
	else return fillCellsBelow(itTo);
      }

      /* FlipTower fillAbove method */
      bool fillAbove(Dimension dim)
      {
	if(invalid) return false;
	CellMapIterator itFrom = cellMap.find(dim);
	if(itFrom == cellMap.end())
	  throw std::invalid_argument
	    (std::string("FlipTower: trying to fill above unset cell of dimension ")+
	     to_string(dim));
	else return fillCellsAbove(itFrom);
      }

      /* FlipTower fillAt method */
      bool fillAt(Dimension dim)
      {
	if(invalid) return false;
	if(cellMap.empty())
	  throw std::invalid_argument("FlipTower: trying to fill cell in empty cell tower");
	CellMapIterator itTo = cellMap.lower_bound(dim);
	if(itTo == cellMap.end()) return fillCellsAbove(--itTo);
	if(itTo->first == dim) return true;
	if(itTo == cellMap.begin()) return fillCellsBelow(itTo);
	CellMapIterator itFrom = itTo; itFrom--;
	return fillCellsBetween(itFrom,itTo);
      }

      /* FlipTower fillFlip methods */
      bool fillFlip(Dimension dim);
      bool fillDeterminedFlips(void);

      /* Other FlipTower methods */
      Index getCell(Dimension dim)
      {
	if(cellMap.empty())
	  throw std::runtime_error
	    (std::string("FlipTower: trying to access cell in empty cell sequence"));
	if(!fillAt(dim))
	  throw std::runtime_error
	    (std::string("FlipTower: trying to access cell of dimension ")+
	     to_string(dim)+" in invalid cell sequence");
	return cellMap[dim];
      }
      const FlipI& getFlip(Dimension dim)
      {
	if(!fillFlip(dim))
	  throw std::runtime_error
	    (std::string("FlipTower: attempt to access flip of dimension ")+
	     to_string(dim)+" in invalid flip tower");
	return flips[dim];
      }
      RO ro(Dimension dim)
      {
	fillBelow(dim);
	fillFlip(dim);
	if(!fillDeterminedFlips())
	  throw std::runtime_error("Call to ro in invalid cell tower");
	RO acc = POS;
	for(Dimension d = 0 ; d <= dim ; d++)
	  if(flips[d].facet[0] != cellMap[d])
	    acc *= NEG;
	return acc;
      }
      Dimension highestDimension(void) const
      {
	if(invalid)
	  throw std::runtime_error("Call to highestDimension in invalid cell tower");
	if(cellMap.empty())
	  throw std::runtime_error("Call to highestDimension in empty cell tower");
	return cellMap.rbegin()->first;
      }
      bool isValid(void)
      {
	if(invalid) return false;
	if(cellMap.size() < 2) return true;
	fillInteriorCells();
	fillDeterminedFlips();
	if(invalid) return false;
	if(cellMap.size() == 2 && cellMap.begin()->first > 0)
	  return fillFlip(cellMap.begin()->first);
	else return true;
      }
    };

    /* CellTuple class definition */
    /// A CellTuple is a tuple of adjacent cells, one of each dimension.
    /// They can be used as a cursor, to point at a position and orientation
    /// within the cell complex.
    template<typename Index>
    struct mdx_EXPORT CellTupleG
    {
      typedef unsigned short Dimension;
      typedef CCStructureInterface<Index> CStructure;
      typedef Flip<Index> FlipI;
      typedef FlipTowerG<Index> FlipTower;

      /* CellTuple member variables */
      const CStructure *cs;
      mutable FlipTower tower;
      std::vector<Index> tuple;

      /* CellTuple containing method declarations */
      /// Creates a CellTuple containing the given cells.
      void containing(const std::vector<Index>& cells);
      /// Creates a CellTuple containing the given cell.
      void containing(Index c1)
      { std::vector<Index> cells(1); cells[0] = c1; containing(cells); }
      /// Creates a CellTuple containing the given cells.
      void containing(Index c1,Index c2)
      { std::vector<Index> cells(2); cells[0] = c1; cells[1] = c2;
	containing(cells); }
      /// Creates a CellTuple containing the given cells.
      void containing(Index c1,Index c2,Index c3)
      { std::vector<Index> cells(3); cells[0] = c1; cells[1] = c2;
	cells[2] = c3; containing(cells); }
      /// Creates a CellTuple containing the given cells.
      void containing(Index c1,Index c2,Index c3,Index c4)
      { std::vector<Index> cells(4); cells[0] = c1; cells[1] = c2;
	cells[2] = c3; cells[3] = c4; containing(cells); }
      /// Creates a CellTuple containing arbitrary cells in the complex.
      void containing(void);

      /* CellTuple constructors */
      CellTupleG(void);
      CellTupleG(const CStructure &_cs);
      CellTupleG(const CStructure &_cs,Index c1);
      CellTupleG(const CStructure &_cs,Index c1,Index c2);
      CellTupleG(const CStructure &_cs,Index c1,Index c2,Index c3);
      CellTupleG(const CStructure &_cs,Index c1,Index c2,Index c3,Index c4);

      /* CellTuple comparison operators */
      bool operator==(const CellTupleG& ct) const { return tuple == ct.tuple; }
      bool operator!=(const CellTupleG& ct) const { return tuple != ct.tuple; }
      bool operator<(const CellTupleG& ct) const { return tuple < ct.tuple; }

      /* CellTuple flip operation declarations */
      /// Changes the CellTuple into the unique CellTuple which only
      /// differs in the given dimension.
      CellTupleG& flip(Dimension dim);
      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(const std::vector<Dimension> dims)
      {
	for(unsigned int i = 0 ; i < dims.size() ; i++) flip(dims[i]);
	return *this;
      }
      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(Dimension d1, Dimension d2)
      { std::vector<Dimension> dims(2); dims[0] = d1; dims[1] = d2; return flip(dims); }

      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(Dimension d1, Dimension d2, Dimension d3)
      { std::vector<Dimension> dims(3); dims[0] = d1; dims[1] = d2; dims[2] = d3;
	return flip(dims); }
      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(Dimension d1, Dimension d2, Dimension d3, Dimension d4)
      { std::vector<Dimension> dims(4); dims[0] = d1; dims[1] = d2; dims[2] = d3;
	dims[3] = d4; return flip(dims); }
      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(Dimension d1, Dimension d2, Dimension d3, Dimension d4, Dimension d5)
      { std::vector<Dimension> dims(5); dims[0] = d1; dims[1] = d2; dims[2] = d3;
	dims[3] = d4; dims[4] = d5; return flip(dims); }
      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(Dimension d1, Dimension d2, Dimension d3, Dimension d4,
		       Dimension d5, Dimension d6)
      { std::vector<Dimension> dims(6); dims[0] = d1; dims[1] = d2; dims[2] = d3;
	dims[3] = d4; dims[4] = d5; dims[5] = d6; return flip(dims); }
      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(Dimension d1, Dimension d2, Dimension d3, Dimension d4,
		       Dimension d5, Dimension d6, Dimension d7)
      { std::vector<Dimension> dims(7); dims[0] = d1; dims[1] = d2; dims[2] = d3;
	dims[3] = d4; dims[4] = d5; dims[5] = d6; dims[6] = d7; return flip(dims); }
      /// Carry out the given flips in sequence to change the CellTuple.
      CellTupleG& flip(Dimension d1, Dimension d2, Dimension d3, Dimension d4,
		       Dimension d5, Dimension d6, Dimension d7, Dimension d8)
      { std::vector<Dimension> dims(8); dims[0] = d1; dims[1] = d2; dims[2] = d3;
	dims[3] = d4; dims[4] = d5; dims[5] = d6; dims[6] = d7; dims[7] = d8;
	return flip(dims); }

      /// Returns the unique CellTuple which only
      /// differs in the given dimension.
      CellTupleG flipped(Dimension dim) const
      { CellTupleG tuple(*this); return tuple.flip(dim); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(const std::vector<Dimension> dims) const
      { CellTupleG tuple(*this); return tuple.flip(dims); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(Dimension d1, Dimension d2) const
      { CellTupleG tuple(*this); return tuple.flip(d1,d2); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(Dimension d1, Dimension d2, Dimension d3) const
      { CellTupleG tuple(*this); return tuple.flip(d1,d2,d3); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(Dimension d1, Dimension d2, Dimension d3, Dimension d4) const
      { CellTupleG tuple(*this); return tuple.flip(d1,d2,d3,d4); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(Dimension d1, Dimension d2, Dimension d3,
			 Dimension d4, Dimension d5) const
      { CellTupleG tuple(*this); return tuple.flip(d1,d2,d3,d4,d5); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(Dimension d1, Dimension d2, Dimension d3, Dimension d4,
			 Dimension d5, Dimension d6) const
      { CellTupleG tuple(*this); return tuple.flip(d1,d2,d3,d4,d5,d6); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(Dimension d1, Dimension d2, Dimension d3, Dimension d4,
			 Dimension d5, Dimension d6, Dimension d7) const
      { CellTupleG tuple(*this); return tuple.flip(d1,d2,d3,d4,d5,d6,d7); }
      /// Carry out the given flips in sequence and return the resulting CellTuple.
      CellTupleG flipped(Dimension d1, Dimension d2, Dimension d3, Dimension d4,
			 Dimension d5, Dimension d6, Dimension d7, Dimension d8) const
      { CellTupleG tuple(*this); return tuple.flip(d1,d2,d3,d4,d5,d6,d7,d8); }

      /// Returns the cell adjacent to the tuple in the given dimension.
      Index other(Dimension dim) const;

      /* Other CellTuple operations */
      void loadTuple(const std::vector<Index> &cells)
      {
	tuple = cells;
	if(cs == NULL)
	  tower = FlipTower();
	else
	  rebuildTower(cells);
      }

      void setCellStructure(const CStructure &newCS)
      {
	if(&newCS != cs)
          {
            cs = &newCS;
            rebuildTower(tuple);
          }
      }

      void rebuildTower(const std::vector<Index> &cells)
      {
	typename FlipTower::CellMap cellMap;
	for(size_t i = 0 ; i < cells.size() ; i++)
	  cellMap[i] = cells[i];
	tower = FlipTower(*cs,cellMap);

	if(!tower.fillAllCells() || !tower.isValid())
	  throw std::runtime_error("CellTuple: rebuilt FlipTower is not valid");
      }

      /// Access the cell of the given dimension in the tuple.
      Index operator[](Dimension dim) const { return tuple[dim]; }

      /// Returns the relative orientation of the cell of the given dimension
      /// with respect to the cell at the next dimension up.
      RO ro(Dimension dim) const
      { return tower.ro(dim); }

      /// Returns the product of all of the relative orientations in the cell.
      RO totalOrientation(void) const;

      friend std::ostream& operator<<(std::ostream& out, const CellTupleG& tuple)
      {
	out << "(" << tuple[0];
	for(unsigned int i = 1 ; i <= tuple.tuple.size() ; i++)
	  out << "," << tuple[i];
	out << ")";
	return out;
      }
    };

    /* SplitStruct class definition */
    /// Contains the four cells which define a split or merge operation.
    template<typename Index>
    struct SplitStructG {
      Index parent;
      Index membrane;
      Index childP, childN;

      /* SplitStruct constructors */
      SplitStructG(void)
	: parent(Index::UNDEF), membrane(Index::UNDEF),
	  childP(Index::UNDEF), childN(Index::UNDEF) {}

      SplitStructG(Index _parent)
	: parent(_parent), membrane(Index::UNDEF),
	  childP(Index::UNDEF), childN(Index::UNDEF) {}

      SplitStructG(Index _parent, Index _membrane)
	: parent(_parent), membrane(_membrane),
	  childP(Index::UNDEF), childN(Index::UNDEF) {}

      SplitStructG(Index _parent, Index _membrane, Index _childP, Index _childN)
	: parent(_parent), membrane(_membrane),
	  childP(_childP), childN(_childN) {}

      /* SplitStruct operators */
      inline Index& child(RO ro) {
	if(ro == POS) return childP;
	else return childN;
      }

      inline Index child(RO ro) const {
	if(ro == POS) return childP;
	else return childN;
      }
      friend std::ostream& operator<<(std::ostream& out, const SplitStructG& ss)
      {
	out << ss.parent << " --> (" << ss.childP << ","
	    << ss.membrane << "," << ss.childN << ")";
	return out;
      }
    };
  } // end namespace ccf

  // Operators for Information::
  inline QString toQString(const ccf::Flip<CCIndex> &flip)
  {
    return ccf::to_string(flip).c_str();
  }
  inline QDebug &operator<<(QDebug &out, const ccf::Flip<CCIndex> &flip)
  {
    return out << toQString(flip);
  }

  inline QString toQString(const ccf::OrientedSet<CCIndex> &chain)
  {
    QString result;
    for(auto iter = chain.begin() ; iter != chain.end() ; iter++) {
      if(iter != chain.begin())
        result.append(" ");
      result.append(toQString(*iter));
    }
    return result;
  }
  inline QDebug &operator<<(QDebug &out, const ccf::OrientedSet<CCIndex> &chain)
  {
    return out << toQString(chain);
  }
} // end namespace mdx

#endif // CCF_CLASSES_HPP
