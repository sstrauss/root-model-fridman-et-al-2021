//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef RESETABLESLIDER_H
#define RESETABLESLIDER_H

#include <Config.hpp>

#include <QSlider>

class QMouseEvent;

class ResetableSlider : public QSlider 
{
  Q_OBJECT
public:
  ResetableSlider(QWidget* parent = 0);
  ResetableSlider(Qt::Orientation orientation, QWidget* parent = 0);

  int defaultValue() {
    return default_value;
  }

public slots:
  void setValueAsDefault();
  void setDefaultValue(int val);
  void resetValue();

protected slots:
  void checkDefaultValue(int min, int max);

signals:
  void reset();

protected:
  void init();
  void mouseDoubleClickEvent(QMouseEvent* e);

private:
  int default_value;
};

#endif
