//
// This file is part of MorphoGraphX - http://www.MorphoGraphX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoGraphX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoGraphX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef VIS_FLAGS_HPP
#define VIS_FLAGS_HPP

struct VisClipFlags
{
  uchar enabled : 1;
  uchar grid : 1;
};

struct VisCutSurfFlags
{
  uchar enabled : 1;
  uchar grid : 1;
  uchar plane : 1;
  uchar axis : 1;
  uchar bezier : 1;
};

struct VisStackFlags
{
  uchar main : 1;
  uchar work : 1;
  uchar surface : 1;
  uchar mesh : 1;
  uchar lines : 1;
  uchar points : 1;
};

union VisFlags
{
  VisClipFlags clipFlags;
  VisCutSurfFlags cutSurfFlags;
  VisStackFlags stackFlags;
  ulong iFlags;
};

#endif
