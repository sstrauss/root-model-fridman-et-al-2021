//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESSS_AXIS_HPP
#define MESH_PROCESSS_AXIS_HPP

#include <Process.hpp>

namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{

  /**
   * \class SaveAxis <MeshProcessAxis.hpp>
   */

  class SaveAxisAttr : public Process 
  {
  public:
    SaveAxisAttr(const Process& process) : Process(process)
    {
      setName("Mesh/Axis/Save Axis Attribute");
      setDesc("Save an axis attribute to a file");
      setIcon(QIcon(":/images/CellAxisSave.png"));

      addParm("Axis Name", "Name of axis attribute to save, empty for current", "");
      addParm("Output File", "Path to output file", "");
    }

    bool initialize(QWidget* parent);
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No cell complex").arg(name());

      QString axisName = parm("Axis Name");
      if(axisName.isEmpty())
        throw QString("%1:run No axis attribute name").arg(name()); // We could grab the current one if available

      QString fileName = parm("Output File");
      if(fileName.isEmpty())
        throw QString("%1:run No output file").arg(name()); 

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      auto &axisAttr = mesh->axisAttr(ccName, axisName);

      return run(cs, indexAttr, axisAttr, fileName);
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, const CCIndexSymTensorAttr &axisAttr, const QString& fileName);
  };

  /**
   * \class LoadAxisAttr <MeshProcessAxis.hpp>
   */
  class LoadAxisAttr : public Process 
  {
  public:
    LoadAxisAttr(const Process& process) : Process(process)
    {
      setName("Mesh/Axis/Load Axis Attribute");
      setDesc("Load an axis attribute from a file");
      setIcon(QIcon(":/images/CellAxisOpen.png"));

      addParm("Axis Name", "Name of axis attribute to save, empty for current", "");
      addParm("Input File", "Path to input file", "");
    }

    bool initialize(QWidget* parent);
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No cell complex").arg(name());

      QString axisName = parm("Axis Name");
      if(axisName.isEmpty())
        throw QString("%1:run No axis attribute name").arg(name()); // We could grab the current one if available

      QString fileName = parm("Input File");
      if(fileName.isEmpty())
        throw QString("%1:run No input file").arg(name()); 

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      auto &axisAttr = mesh->axisAttr(ccName, axisName);

      return run(cs, indexAttr, axisAttr, fileName);
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexSymTensorAttr &axisAttr, const QString &fileName);
  };

  /**
   * \class ClearAxisAttr <MeshProcessAxis.hpp>
   */
  class ClearAxisAttr : public Process 
  {
  public:
    ClearAxisAttr(const Process& process) : Process(process)
    {
      setName("Mesh/Axis/Clear Axis Attribute");
      setDesc("Clear axis for selected cells");
      setIcon(QIcon(":/images/CellAxisOpen.png"));

      addParm("Axis Name", "Name of axis attribute to save, empty for current", "");
      addParm("Erase All", "Erase all, or just the selected cells", "All", booleanChoice());
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No cell complex").arg(name());

      QString axisName = parm("Axis Name");
      if(axisName.isEmpty())
        throw QString("%1:run No axis attribute name").arg(name()); // We could grab the current one if available
      if(!mesh->axisExists(ccName, axisName))
        throw QString("%1:run No axis named (%2) exists").arg(name()).arg(axisName);

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      auto &axisAttr = mesh->axisAttr(ccName, axisName);
      bool eraseAll = stringToBool("Erase All");
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, axisAttr, eraseAll);
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexSymTensorAttr &axisAttr, bool eraseAll);
  };

  /**
   * \class ClearAxisAttr <MeshProcessAxis.hpp>
   */
  class SetAxisLineWidth : public Process 
  {
  public:
    SetAxisLineWidth(const Process& process) : Process(process)
    {
      setName("Mesh/Axis/Set Axis Line Width");
      setDesc("Set the width of the axis lines");
      setIcon(QIcon(":/images/CellAxisOpen.png"));

      addParm("Axis Name", "Name of axis attribute to save, empty for current", "");
      addParm("Width", "The width to draw the liness", "2.0");
    }

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1:run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1:run No cell complex").arg(name());

      QString axisName = parm("Axis Name");
      if(axisName.isEmpty())
        throw QString("%1:run No axis attribute name").arg(name()); // We could grab the current one if available
      if(!mesh->axisExists(ccName, axisName))
        throw QString("%1:run No axis named (%2) exists").arg(name()).arg(axisName);

      double width = parm("Width").toDouble();
      if(width <= 0)
        throw QString("%1:run Width must be > 0").arg(name());

      mesh->updateProperties(ccName);
      return mesh->axisSetLineWidth(ccName, axisName, width);
    }
  };
  ///@}
}
#endif
