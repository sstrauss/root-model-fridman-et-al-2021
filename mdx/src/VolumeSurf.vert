varying vec3 texCoord;

void setTexCoord()
{
  vec4 tc = gl_TextureMatrix[0] * gl_MultiTexCoord0;
  texCoord = tc.xyz / tc.w;
}
