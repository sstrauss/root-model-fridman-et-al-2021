//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef DIR_HPP
#define DIR_HPP

#include <Config.hpp>

#include <QDir>

namespace mdx 
{
  /// Returns the path relative to the current directory
  mdx_EXPORT QString stripCurrentDir(QString file);
  
  /// Remove a directory from the file path
  mdx_EXPORT QString stripDir(const QString& dir, QString file);
  
  /// Get the directory containing a file (base of the name only)
  mdx_EXPORT QString getDir(QString file);
  
  /// Set the current path
  mdx_EXPORT bool setCurrentPath(const QString& path);
  
  /// Get the current path
  mdx_EXPORT QString currentPath();
  
  /// Get the current path as a directory
  mdx_EXPORT QDir currentDir();
  
  /// Return the absolute path to a file
  mdx_EXPORT QString absoluteFilePath(QString filename);
  
  /// Return the absolute file path, resolving things like environement variables and '~' for $HOME
  mdx_EXPORT QString resolvePath(QString path);
}

#endif
