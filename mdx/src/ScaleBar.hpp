//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef SCALEBAR_HPP
#define SCALEBAR_HPP

#include <Config.hpp>
#include <GL.hpp>

#include <Geometry.hpp>
#include <MDXViewer/qglviewer.h>
#include <Parms.hpp>

#include <cmath>
#include <QColor>
#include <QFont>
#include <QString>
#include <string>

class QTextStream;

namespace mdx 
{
  class mdx_EXPORT ScaleBar 
	{
  public:
    typedef Vector<2, size_t> Point2u;
    enum Position { Top, Bottom, Left, Right, TopLeft, BottomLeft, TopRight, BottomRight, Center };
  
    enum Direction { Horizontal, Vertical };
  
    enum TextPosition { In, Out };
  
    ScaleBar();
  
    void setWantedSize(double ws)
    {
      if(ws > 0)
        wantedSize = ws;
    }
  
    void setScale(double s)
    {
      if(s > 0)
        scale = s;
    }
  
    void setUnit(QString u)
    {
      unit = u;
      displayUnit = !u.isEmpty();
    }
  
    // Specified the screen coordinate only for user defined
    // sp is in normalized screen coordinates (i.e. between (0,0) and (1,1))
    void setPosition(Position p)   //, Point2d sp = Point2d())
    {
      pos = p;
    }
  
    void setThickness(int th)
    {
      if(th < 1)
        thickness = 1;
      else
        thickness = th;
    }
  
    void setShiftBorder(const Point2u& pt) {
      shiftBorder = pt;
    }
  
    void setFont(const QFont& fnt) {
      unit_font = fnt;
    }
  
    void setFontSize(int size) {
      fontSize = (size > 0) ? size : 0;
    }
  
    void init(QGLViewer* viewer);
    void draw(QGLViewer* viewer, QPaintDevice* device = 0);
  
    void readParms(Parms& parms, QString section);
    void writeParms(QTextStream& pout, QString section);
  
    void scaleDrawing(double s);
    void restoreScale();
  
  protected:
    void findScale(double unit_size);
    double wantedSize;
    double scale;
    QString unit;
    bool displayUnit;
    Position pos;
    Direction dir;
    QFont unit_font;
    double thickness;
    Point2u shiftBorder;
    TextPosition textPosition;
    int fontSize;
    bool autoScale, autoUnit;
    double minSize, maxSize;
  
    double globalScale;
  
    // Saved positions
    /*
     * double savedWantedSize;
     * double savedThickness;
     * Point2u savedShiftBorder;
     * int savedFontSize;
     * double savedMinSize;
     * double savedMaxSize;
     */
  };
}
#endif
