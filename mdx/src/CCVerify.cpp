//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <CCVerify.hpp>
#include <CCUtils.hpp>

namespace mdx
{
  /* Helper class DisjointSet (copied from CCF.cpp) */
  template <typename Index>
  struct DisjointSet
  {
    /* DisjointSet type definitions */
    typedef std::pair<Index,unsigned int> DisjointData;
    typedef std::map<Index,DisjointData> DisjointMap;
    typedef typename DisjointMap::iterator DisjointIterator;
  
    /* DisjointSet member variables */
    DisjointMap sets;
  
    /* DisjointSet FindSet of iterator */
    Index FindSet(DisjointIterator iter)
    {
      if(iter->second.first != iter->first)
        iter->second.first = FindSet(iter->second.first);
      return iter->second.first;
    }
  
    /* DisjointSet Link */
    void Link(DisjointIterator it1, DisjointIterator it2)
    {
      unsigned int &rank1 = it1->second.second, &rank2 = it2->second.second;
      if(rank1 > rank2) it2->second.first = it1->first;
      else if(rank1 < rank2) it1->second.first = it2->first;
      else { it1->second.first = it2->first; rank2++; }
    }
  
  public:
    /* DisjointSet MakeSet */
    void MakeSet(Index t)
    { if(sets.count(t) == 0) sets[t] = std::make_pair(t,0); }
  
    /* DisjointSet FindSet of Index */
    Index FindSet(Index t)
    {
      DisjointIterator iter = sets.find(t);
      if(iter == sets.end()) { MakeSet(t); return t; }
      else return FindSet(iter);
    }
  
    /* DisjointSet Union */
    void Union(Index t1, Index t2)
    { Link(sets.find(FindSet(t1)), sets.find(FindSet(t2))); }
  
    /* DisjointSet Components */
    std::set<Index> Components(void)
    {
      std::set<Index> components;
      for(DisjointIterator iter = sets.begin() ; iter != sets.end() ; iter++)
        components.insert(FindSet(iter));
      return components;
    }
  
    /* DisjointSet CountComponents */
    unsigned int CountComponents(void)
    {
      return Components().size();
    }
  };

  bool verifyCCStructure(const CCStructure &cs1, CCIndexDataAttr &indexAttr)
  {
    const ccf::CCStructure &cs = ensureCCF(cs1);

    typedef typename CCStructure::FlipI FlipI;
    typedef typename CCStructure::FlipTableIterator FlipTableIterator;
    typedef typename CCStructure::Dimension Dimension;
    typedef typename CCStructure::BoundaryChain BoundaryChain;
    typedef typename CCStructure::BoundaryIterator BoundaryIterator;
    typedef typename CCStructure::DimensionMap::DimMap::const_iterator DimMapIterator;
    typedef std::vector<FlipI> FlipVectorI;
    typedef FlipVectorI::const_iterator FlipVectorIterator;
  
    bool ccfOK = true;
  
    // 1. Check that the dimension map is self-consistent.
    mdxInfo << "Checking that the dimension map is self-consistent...." << endl;
    int maxDim = cs.maxDimension();
    // a. Read and validate the information in dimensionByCell
    std::vector<std::set<CCIndex> > cells(maxDim+1);
    for(DimMapIterator iter = cs.dimensionMap.dimensionByCell.begin() ;
        iter != cs.dimensionMap.dimensionByCell.end() ; iter++) {

      // Clear selection 
      indexAttr[iter->first].selected = false;

      if(iter->first.isPseudocell()) {
        mdxInfo << "Pseudocell " << iter->first << " in dimensionMap at dimension "
                        << iter->second.first << endl;
        ccfOK = false;
        continue;
      }
      if(iter->second.first > maxDim) {
        mdxInfo << "Cell " << iter->first << " has dimension " << iter->second.first
                        << " > maxDim (=" << maxDim << ")" << endl;
        ccfOK = false;
        continue;
      }
  
      cells[iter->second.first].insert(iter->first);
    }
    // b. Check the information from dimensionByCell against cellsByDimension
    for(Dimension d = 0 ; d <= maxDim ; d++) {
      if(cells[d].size() != cs.dimensionMap.cellsByDimension[d].size()) {
        mdxInfo << "dimensionMap sizes inconsistent at dimension " << d << endl;
        ccfOK = false;
        continue;
      }
      // copy and sort cellsByDimension into another set
      std::set<CCIndex> thisDimension(cs.dimensionMap.cellsByDimension[d].begin(),
                                    cs.dimensionMap.cellsByDimension[d].end());
      // check element by element against the information from dimensionByCell
      typename std::set<CCIndex>::const_iterator cIter = cells[d].begin(),
        dIter = thisDimension.begin();
      while(cIter != cells[d].end() && dIter != thisDimension.end()) {
        if(*cIter != *dIter) {
          mdxInfo << "dimensionByCell has cell " << (*cIter) << " at dimension " << d
                    << " but at that position in cellsByDimension[" << d << "] is "
                    << (*dIter) << endl;

          selectCell(cs, indexAttr, *cIter);
          ccfOK = false;
          continue;
        }
        cIter++;
        dIter++;
      }
    }
    // c. Check that there are no dimensions (except upper ones) with no cells at all
    int highestDim;
    for(highestDim = maxDim ; highestDim >= 0 ; highestDim--)
      if(cells[highestDim].size() > 0) break;
    if(highestDim < maxDim)
      mdxInfo << "maxDim is " << maxDim << " but the highest dimension cell is of dimension "
                << highestDim << endl;
    for(int i = 0 ; i <= highestDim ; i++) {
      if(cells[i].empty()) {
        mdxInfo << "No cells of dimension " << i << endl;
        ccfOK = false;
      }
    }
  
    if(ccfOK != true) 
      return false;
  
    // 2. Check boundaries and coboundaries
    mdxInfo << "Checking boundaries..." << endl;
    std::map<CCIndex,BoundaryChain> boundary, coboundary;
    std::set<CCIndex> badBoundary;
    for(int i = 0 ; i <= maxDim ; i++) {
      // a. Grab and check boundaries of all cells
      for(typename std::set<CCIndex>::iterator iter = cells[i].begin(); iter != cells[i].end(); iter++) {
        try {
          // Vertices have only boundary BOTTOM
          if(i == 0) {
            boundary[*iter] = +CCIndex::BOTTOM;
            coboundary[CCIndex::BOTTOM] += *iter;
          } else {
            BoundaryChain bd = boundary[*iter] = cs.boundary(+(*iter));
            for(BoundaryIterator bi = bd.begin() ; bi != bd.end() ; bi++) {
              CCIndex bdCell = ~(*bi);
              if(bdCell.isPseudocell()) {
                mdxInfo << "Cell " << (*iter) << " has pseudocell " << bdCell << " in its boundary." << endl;
                selectCell(cs, indexAttr, *iter);
                ccfOK = false;
              } else if(!cs.dimensionMap.hasCell(bdCell)) {
                mdxInfo << "Cell " << (*iter) << " has cell " << bdCell
                          << " in its boundary, but " << bdCell << " is not in the dimension map!" << endl;
  
                selectCells(cs, indexAttr, *iter, bdCell);
                ccfOK = false;
              } else if(cells[i-1].count(bdCell) != 1) {
                mdxInfo << "Cell " << (*iter) << " of dimension " << i
                          << " has cell " << bdCell << " of dimension " << (cs.dimensionOf(bdCell)) << " in its boundary!" << endl;
  
                selectCells(cs, indexAttr, *iter, bdCell);
                ccfOK = false;
              }
            }
          }
        } catch(...) {
          mdxInfo << "Exception examining boundary of cell " << *iter << endl;

          badBoundary.insert(*iter);
          selectCell(cs, indexAttr, *iter);
          ccfOK = false;
        }
      }
    }
    // b. Same checks for the boundary of TOP, if it exists
    if(cs.hasCell(CCIndex::TOP)) {
      boundary[CCIndex::TOP] = cs.boundary(+CCIndex::TOP);
      if(cs.hasCell(CCIndex::INFTY)) boundary[CCIndex::TOP] += CCIndex::INFTY;
      BoundaryChain bd = boundary[CCIndex::TOP];
      for(BoundaryIterator bi = bd.begin() ; bi != bd.end() ; bi++) {
        CCIndex bdCell = ~(*bi);
        try {
          if(bdCell.isPseudocell()) {
            if(bdCell != CCIndex::INFTY) {
              mdxInfo << "TOP has pseudocell " << bdCell
                        << " in its boundary." << endl;
  
              selectCell(cs, indexAttr, bdCell);
              ccfOK = false;
            }
          } else if(!cs.dimensionMap.hasCell(bdCell)) {
            mdxInfo << "TOP has cell " << bdCell
                      << " in its boundary, but " << bdCell
                      << " is not in the dimension map!" << endl;
  
            selectCell(cs, indexAttr, bdCell);
            ccfOK = false;
          } else if(cells[maxDim].count(bdCell) != 1) {
            mdxInfo << "TOP in cell complex of dimension " << maxDim
                      << " has cell " << bdCell << " of dimension " << (cs.dimensionOf(bdCell))
                      << " in its boundary!" << endl;
  
            selectCell(cs, indexAttr, bdCell);
            ccfOK = false;
          }
        } catch(...) {
          mdxInfo << "Exception examining boundary of cell " << bdCell << endl;

          selectCell(cs, indexAttr, bdCell);
          ccfOK = false;
        }
      }
    }
    // c. Same checks for the boundary of INFTY, if it exists
    if(cs.hasCell(CCIndex::INFTY)) {
      coboundary[CCIndex::INFTY] = +CCIndex::TOP;
      try {
        BoundaryChain bd = boundary[CCIndex::INFTY] = cs.boundary(+CCIndex::INFTY);

        for(BoundaryIterator bi = bd.begin() ; bi != bd.end() ; bi++) {
          CCIndex bdCell = ~(*bi);
          try {
            if(bdCell.isPseudocell()) {
              mdxInfo << "INFTY has pseudocell " << bdCell << " in its boundary." << endl;
    
              ccfOK = false;
            } else if(!cs.dimensionMap.hasCell(bdCell)) {
              mdxInfo << "INFTY has cell " << bdCell << " in its boundary, but " << bdCell << " is not in the dimension map!" << endl;
    
              selectCell(cs, indexAttr, bdCell);
              ccfOK = false;
            } else if(cells[maxDim-1].count(bdCell) != 1) {
              mdxInfo << "INFTY in cell complex of dimension " << maxDim
                        << " has cell " << bdCell << " of dimension " << (cs.dimensionOf(bdCell))
                        << " in its boundary!" << endl;
    
              selectCell(cs, indexAttr, bdCell);
              ccfOK = false;
            }
          } catch(...) {
            mdxInfo << "Exception examining boundary of cell " << bdCell << endl;
  
            selectCell(cs, indexAttr, bdCell);
            ccfOK = false;
          }
        }
      } catch(...) {
        mdxInfo << "Exception creating boundary chain for INFTY" << endl;

        badBoundary.insert(CCIndex::INFTY);
        ccfOK = false;
      }
    }
    // d. Get and check coboundaries
    mdxInfo << "Checking coboundaries..." << endl;
    for(int i = 0 ; i <= maxDim ; i++) {
      for(typename std::set<CCIndex>::iterator iter = cells[i].begin(); iter != cells[i].end(); iter++) {
        try {
          coboundary[*iter] = cs.coboundary(+(*iter));
          // coboundary of a cell of maximal dimension doesn't automatically include TOP
          if(i == maxDim) 
            coboundary[*iter] += cs.ro(*iter,CCIndex::TOP) * CCIndex::TOP;
          // or INFTY
          else if(i == (maxDim-1) && cs.matchFirst(CCIndex::Q,CCIndex::Q,*iter,CCIndex::INFTY).isValid())
            coboundary[*iter] += cs.ro(*iter,CCIndex::INFTY) * CCIndex::INFTY;
          BoundaryChain bd = coboundary[*iter];
          for(BoundaryIterator bi = bd.begin() ; bi != bd.end() ; bi++) {
            CCIndex bdCell = ~(*bi);
            if(bdCell.isPseudocell()) {
              if((i == maxDim && bdCell == CCIndex::TOP) || (i == maxDim-1 && bdCell == CCIndex::INFTY))
                ;
              else {
                mdxInfo << "Cell " << (*iter) << " has pseudocell " << bdCell
                          << " in its coboundary." << endl;

                selectCell(cs, indexAttr, *iter);
                ccfOK = false;
              }
            } else if(!cs.dimensionMap.hasCell(bdCell)) {
              mdxInfo << "Cell " << (*iter) << " has cell " << bdCell
                        << " in its coboundary, but " << bdCell
                        << " is not in the dimension map!" << endl;

              selectCells(cs, indexAttr, *iter, bdCell);
              ccfOK = false;
            } else if(i == maxDim) {
              mdxInfo << "Cell " << (*iter) << " of dimension " << i
                        << " has non-TOP cell " << bdCell << " in its coboundary!" << endl;

              selectCells(cs, indexAttr, *iter, bdCell);
              ccfOK = false;
            } else if(cells[i+1].count(bdCell) != 1) {
              mdxInfo << "Cell " << (*iter) << " of dimension " << i
                        << " has cell " << bdCell << " of dimension " << (cs.dimensionOf(bdCell))
                        << " in its coboundary!" << endl;

              selectCells(cs, indexAttr, *iter, bdCell);
              ccfOK = false;
            }
          }
        } catch(...) {
          mdxInfo << "Exception examining coboundary of cell " << *iter << endl;

          selectCell(cs, indexAttr, *iter);
          ccfOK = false;
        }
      }
    }
    // e. Check the coboundary of BOTTOM
    if(cs.hasCell(CCIndex::BOTTOM)) {
      BoundaryChain bd = coboundary[CCIndex::BOTTOM] = cs.coboundary(+CCIndex::BOTTOM);
      for(BoundaryIterator bi = bd.begin() ; bi != bd.end() ; bi++) {
        CCIndex bdCell = ~(*bi);
        if(bdCell.isPseudocell()) {
          mdxInfo << "BOTTOM has pseudocell " << bdCell << " in its coboundary." << endl;

          ccfOK = false;
        } else if(!cs.dimensionMap.hasCell(bdCell)) {
          mdxInfo << "BOTTOM has cell " << bdCell << " in its coboundary, but " << bdCell << " is not in the dimension map!" << endl;

          selectCell(cs, indexAttr, bdCell);
          ccfOK = false;
        } else if(cells[0].count(bdCell) != 1) {
          mdxInfo << "BOTTOM has cell " << bdCell << " of dimension " << (cs.dimensionOf(bdCell)) << " in its coboundary!" << endl;

          selectCell(cs, indexAttr, bdCell);
          ccfOK = false;
        }
      }
    }
  
    // f. Check boundary / coboundary consistency
    mdxInfo << "Checking boundary / coboundary consistency..." << endl;
    // Does every cell in the boundary have the original in its coboundary?
    for(typename std::map<CCIndex,BoundaryChain>::const_iterator iter = boundary.begin(); iter != boundary.end(); iter++) {
      for(BoundaryIterator bi = iter->second.begin() ; bi != iter->second.end() ; bi++) {
        BoundaryIterator ib = coboundary[~(*bi)].find(iter->first);
        if(ib == coboundary[~(*bi)].end()) {
          mdxInfo << "Boundary of " << iter->first << " contains " << (*bi)
                    << ", but coboundary of " << (~(*bi)) << " does not contain " << iter->first << endl;

          selectCells(cs, indexAttr, iter->first, ~(*bi));
          ccfOK = false;
        } else if((*bi).orientation() != (*ib).orientation()) {
          mdxInfo << "Boundary of " << iter->first << " contains " << (*bi)
                    << ", but coboundary of " << (~(*bi)) << " contains " << (*ib) << endl;

          selectCells(cs, indexAttr, iter->first, ~(*bi));
          ccfOK = false;
        }
      }
    }
  
    // Does every cell in the coboundary have the original in its boundary?
    for(typename std::map<CCIndex,BoundaryChain>::const_iterator iter = coboundary.begin(); iter != coboundary.end(); iter++) {
      for(BoundaryIterator bi = iter->second.begin(); bi != iter->second.end(); bi++) {
        if(badBoundary.count(~(*bi))) {
          mdxInfo << "Skipping boundary of cell " << (*bi) << endl;
          continue;
        }
          
        BoundaryIterator ib = boundary[~(*bi)].find(iter->first);
        if(ib == boundary[~(*bi)].end()) {
          mdxInfo << "Coboundary of " << iter->first << " contains " << (*bi)
                    << ", but boundary of " << (~(*bi)) << " does not contain "
                    << iter->first << endl;

          selectCells(cs, indexAttr, iter->first, ~(*bi));
          ccfOK = false;
        } else if((*bi).orientation() != (*ib).orientation()) {
          mdxInfo << "Coboundary of " << iter->first << " contains " << (*bi)
                    << ", but boundary of " << (~(*bi)) << " contains " << (*ib) << endl;

          selectCells(cs, indexAttr, iter->first, ~(*bi));
          ccfOK = false;
        }
      }
    }
  
    if(ccfOK != true) 
      return false;
  
    // 3. Local checks on flips.
    mdxInfo << "Local checks on flips..." << endl;
    std::map<std::pair<CCIndex,CCIndex>,FlipI> ijFlip;
    for(FlipTableIterator iter = cs.flips.begin() ; iter != cs.flips.end() ; iter++) {
      FlipI flip = *iter;
      // a. Check cell uniqueness
      if(flip.interior == flip.joint    ||
         flip.interior == flip.facet[0] ||
         flip.interior == flip.facet[1] ||
         flip.joint    == flip.facet[0] ||
         flip.joint    == flip.facet[1] ||
         flip.facet[0] == flip.facet[1])
      {
        mdxInfo << "Flip " << flip << " has one or more identical cells." << endl;
        
        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
        continue;
      }
  
      // b. Check IJ-uniqueness
      if(flip.interior != CCIndex::INFTY) {
        auto ijIter = ijFlip.find(std::make_pair(flip.interior,flip.joint));
        if(ijIter != ijFlip.end()) {
          mdxInfo << "Flip " << flip << " has same IJ cells as " << ijIter->second << "." << endl;

          selectFlip(cs, indexAttr, flip);
          selectFlip(cs, indexAttr, ijIter->second);
          ccfOK = false;
          continue;
        }
        ijFlip[std::make_pair(flip.interior,flip.joint)] = flip;
      }
  
      // c. Check dimensions: make sure that dimension of joint is one less than
      //    dimension of facets, and dimension of interior is one greater.
      int interiorDim;
      if(flip.interior == CCIndex::TOP) interiorDim = maxDim + 1;
      else if(flip.interior == CCIndex::INFTY) interiorDim = maxDim;
      else if(flip.interior.isPseudocell())
      {
        mdxInfo << "Flip " << flip << " has invalid pseudocell in interior position." << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
        continue;
      } else if(!cs.dimensionMap.hasCell(flip.interior)) {
        mdxInfo << "Flip " << flip << " has interior cell " << (flip.interior) << " not in dimension map." << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
        continue;
      } else 
        interiorDim = cs.dimensionOf(flip.interior);
  
      // joint cell
      if(flip.joint == CCIndex::BOTTOM) {
        if(interiorDim != 1) {
          mdxInfo << "Flip " << flip << " has interior cell of dimension "
                    << interiorDim << " but has BOTTOM as joint." << endl;

          selectFlip(cs, indexAttr, flip);
          ccfOK = false;
          continue;
        }
      } else if(flip.joint.isPseudocell()) {
        mdxInfo << "Flip " << flip << " has invalid pseudocell in joint position."
                  << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
        continue;
      } else if(!cs.dimensionMap.hasCell(flip.joint)) {
        mdxInfo << "Flip " << flip << " has joint cell " << (flip.joint)
                  << " not in dimension map." << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
        continue;
      } else if(cs.dimensionOf(flip.joint) != interiorDim - 2) {
        mdxInfo << "Flip " << flip << " has interior cell of dimension "
                  << interiorDim << " but joint of dimension " << cs.dimensionOf(flip.joint) << "." << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
        continue;
      }
  
      // facets
      for(int i = 0 ; i < 2 ; i++) {
        if(flip.facet[i] == CCIndex::INFTY) {
          if(interiorDim != maxDim + 1) {
            mdxInfo << "Flip " << flip << " has interior cell of dimension "
                      << interiorDim << " but has INFTY as facet." << endl;

            selectFlip(cs, indexAttr, flip);
            ccfOK = false;
            continue;
          }
        } else if(flip.facet[i].isPseudocell()) {
          mdxInfo << "Flip " << flip << " has invalid pseudocell in facet position." << endl;

          selectFlip(cs, indexAttr, flip);
          ccfOK = false;
          continue;
        } else if(!cs.dimensionMap.hasCell(flip.facet[i])) {
          mdxInfo << "Flip " << flip << " has facet " << (flip.facet[i]) << " not in dimension map." << endl;

          selectFlip(cs, indexAttr, flip);
          ccfOK = false;
          continue;
        } else if(cs.dimensionOf(flip.facet[i]) != interiorDim - 1) {
          mdxInfo << "Flip " << flip << " has interior cell of dimension "
                    << interiorDim << " but facet of dimension " << cs.dimensionOf(flip.facet[i]) << "." << endl;

          selectFlip(cs, indexAttr, flip);
          ccfOK = false;
          continue;
        }
      }
  
      // d. Check flip against computed boundaries.
      if(boundary[flip.interior].count(flip.facet[0]) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.facet[0] << " is not in the boundary of " << flip.interior << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
      if(boundary[flip.interior].count(flip.facet[1]) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.facet[1] << " is not in the boundary of " << flip.interior << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
      if(boundary[flip.facet[0]].count(flip.joint) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.joint << " is not in the boundary of " << flip.facet[0] << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
      if(boundary[flip.facet[1]].count(flip.joint) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.joint << " is not in the boundary of " << flip.facet[1] << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
      if(coboundary[flip.joint].count(flip.facet[0]) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.facet[0] << " is not in the coboundary of " << flip.joint << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
      if(coboundary[flip.joint].count(flip.facet[1]) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.facet[1] << " is not in the coboundary of " << flip.joint << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
      if(coboundary[flip.facet[0]].count(flip.interior) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.interior << " is not in the coboundary of " << flip.facet[0] << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
      if(coboundary[flip.facet[1]].count(flip.interior) != 1) {
        mdxInfo << "Flip " << flip << " is in the table, but " << flip.interior << " is not in the coboundary of " << flip.facet[1] << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
      }
  
      // e. Check that flip orientations are canonical and consistent.
      ccf::RO leftOrnt = boundary[flip.interior][flip.facet[0]] *
        boundary[flip.facet[0]][flip.joint],
        rightOrnt = boundary[flip.interior][flip.facet[1]] *
        boundary[flip.facet[1]][flip.joint];
      if(leftOrnt != ccf::POS || rightOrnt != ccf::NEG) {
        mdxInfo << "Flip " << flip << " has left ROs ("
                  << boundary[flip.facet[0]][flip.joint] << ","
                  << boundary[flip.interior][flip.facet[0]] << ") and right ROs ("
                  << boundary[flip.facet[1]][flip.joint] << ","
                  << boundary[flip.interior][flip.facet[1]] << ")" << endl;

        selectFlip(cs, indexAttr, flip);
        ccfOK = false;
        continue;
      }
    }
  
    if(ccfOK != true) 
      return false;
  
    // 4. Check that boundaries are simple and closed.
    mdxInfo << "Checking that boundaries are simple and closed..." << endl;
    for(int i = 1 ; i <= maxDim ; i++) {
      unsigned int simpNum = (i*(i+1))/2; // The number of cells in the boundary of a simplex of dimension i.
      for(typename std::set<CCIndex>::iterator iter = cells[i].begin(); iter != cells[i].end(); iter++) {
        FlipVectorI bflips = cs.matchV(CCIndex::Q,CCIndex::Q,CCIndex::Q,*iter);
  
        if(bflips.size() < simpNum)  // This is legal, just unusual.
          mdxInfo << "Boundary of cell " << (*iter) << " of dimension "
                    << i << " is subsimplicial; it contains " << (bflips.size()) << " flips." << endl;
  
        // a. Use a disjoint-set data structure to counting boundary components.
        DisjointSet<CCIndex> disj;
        std::map<CCIndex,FlipI> byJoint;
        for(FlipVectorIterator fi = bflips.begin() ; fi != bflips.end() ; fi++) {
          if(byJoint.find(fi->joint) == byJoint.end())
            byJoint[fi->joint] = *fi;
          else {
            mdxInfo << "Boundary of cell " << (*iter) << " contains flips "
                      << byJoint[fi->joint] << " and " << (*fi) << ", which share a joint" << endl;

            selectCell(cs, indexAttr, *iter);
            ccfOK = false;
          }
          disj.Union(fi->facet[0],fi->facet[1]);
        }
        if(disj.CountComponents() != 1) {
          mdxInfo << "Boundary of cell " << (*iter) << " contains " << disj.CountComponents() << " components." << endl;

          selectCell(cs, indexAttr, *iter);
          ccfOK = false;
        }
  
        // b. Check that the boundary of the boundary is zero
        BoundaryChain boundaryOfBoundary;
        BoundaryChain iBounds = boundary[*iter], bBounds;
        for(BoundaryIterator bi = iBounds.begin() ; bi != iBounds.end() ; bi++)
          bBounds += (*bi).orientation() * boundary[~(*bi)];
        if(!bBounds.empty()) {
          mdxInfo << "Boundary of boundary of cell " << (*iter) << " is nonempty: " << bBounds << endl;

          selectCell(cs, indexAttr, *iter);
          ccfOK = false;
        }
      }
    }
  
    if(ccfOK != true) 
      return false;
  
    // 5. Check border of the cell complex (INFTY).
    mdxInfo << "Checking the border of the cell complex..." << endl;
    if(cs.hasCell(CCIndex::INFTY)) {
      unsigned int simpNum = (maxDim*(maxDim+1))/2;
      FlipVectorI bflips = cs.matchV(CCIndex::Q,CCIndex::Q,CCIndex::Q,CCIndex::INFTY);
      if(maxDim == 1 && !(bflips.empty())) {
        mdxInfo << "There are " << bflips.size() << " flips with interior INFTY"
                  << " in a one-dimensional complex; there should be none." << endl;

        ccfOK = false;
      }
  
      // a. Disjoint-set will collect all boundary components.
      DisjointSet<CCIndex> disj;
      std::map<CCIndex,FlipI> byJoint;
      CCIndexSet sharedJoints;
      for(FlipVectorIterator fi = bflips.begin() ; fi != bflips.end() ; fi++) {
        if(byJoint.find(fi->joint) == byJoint.end())
          byJoint[fi->joint] = *fi;
        else {   // This is not an error, just strange.
          mdxInfo << "Border of cell complex contains flips " << byJoint[fi->joint] << " and " << (*fi) << ", which share a joint" << endl;
          sharedJoints.insert(fi->joint);
        }
        disj.Union(fi->facet[0],fi->facet[1]);
      }
      // Check each component.
      std::map<CCIndex,FlipVectorI> components;
      for(FlipVectorIterator fi = bflips.begin() ; fi != bflips.end() ; fi++)
        components[disj.FindSet(fi->facet[0])].push_back(*fi);
      for(typename std::map<CCIndex,FlipVectorI>::const_iterator iter = components.begin(); iter != components.end(); iter++) {
        if(iter->second.size() < simpNum)
          mdxInfo << "Border component is subsimplicial; it contains " << (iter->second.size()) << " flips." << endl;
      }
  
      // b. Check that the boundary of the boundary is zero
      BoundaryChain boundaryOfBoundary;
      BoundaryChain iBounds = boundary[CCIndex::INFTY], bBounds;
      for(BoundaryIterator bi = iBounds.begin() ; bi != iBounds.end() ; bi++)
        bBounds += (*bi).orientation() * boundary[~(*bi)];
      if(!bBounds.empty()) {
        mdxInfo << "Boundary of border is nonempty: " << bBounds << endl;
        for(CCSignedIndex c : bBounds)
          if(sharedJoints.count(~c) == 0) {
            selectCell(cs, indexAttr, ~c);
            ccfOK = false;
          }
      }
    }
  
    if(ccfOK != true) 
      return false;
  
    // 6. Check for the existence of flips for all incident cells.
    mdxInfo << "Checking the existence of flips for all incident cells..." << endl;
    for(typename std::map<CCIndex,BoundaryChain>::const_iterator cIter = boundary.begin(); cIter != boundary.end(); cIter++) {
      if(cIter->first == CCIndex::TOP) continue;
      for(typename BoundaryChain::const_iterator bIter = cIter->second.begin(); bIter != cIter->second.end(); bIter++) {
        if(~(*bIter) == CCIndex::BOTTOM) continue;
        // Check for flip with bIter in facet.
        FlipI qFlip(CCIndex::Q,~(*bIter),CCIndex::Q,cIter->first);
        if(!cs.matchFirst(qFlip).isValid()) {
          mdxInfo << toQString(~(*bIter)) << " is in the boundary of " << cIter->first
                  << " but no flip matching " << qFlip << " is in the flip table." << endl;

          selectCells(cs, indexAttr, ~(*bIter), cIter->first);
          ccfOK = false;
        }
        // Check for flip with bIter in joint.
        if(!(coboundary[cIter->first].empty())) {
          FlipI qFlip(~(*bIter),CCIndex::Q,cIter->first,CCIndex::Q);
          if(!cs.matchFirst(qFlip).isValid()) {
            mdxInfo << toQString(~(*bIter)) << " is in the boundary of " << cIter->first
                      << " but no flip matching " << qFlip << " is in the flip table." << endl;

            selectCells(cs, indexAttr, ~(*bIter), cIter->first);
            ccfOK = false;
          }
        }
      }
    }
    for(typename std::map<CCIndex,BoundaryChain>::const_iterator cIter = coboundary.begin(); cIter != coboundary.end(); cIter++) {
      if(cIter->first == CCIndex::BOTTOM) continue;
      for(typename BoundaryChain::const_iterator bIter = cIter->second.begin(); bIter != cIter->second.end(); bIter++) {
        // Check for flip with bIter in interior.
        if(~(*bIter) == CCIndex::TOP) 
          continue;
        FlipI qFlip(CCIndex::Q,CCIndex::Q,cIter->first,~(*bIter));
        if(!cs.matchFirst(qFlip).isValid()) {
          mdxInfo << toQString(~(*bIter)) << " is in the coboundary of " << cIter->first
                    << " but no flip matching " << qFlip << " is in the flip table." << endl;

          selectCells(cs, indexAttr, ~(*bIter), cIter->first);
          ccfOK = false;
        }
        if(!(coboundary[~(*bIter)].empty())) {
        // Check for flip with bIter in facet.
          FlipI qFlip(cIter->first,CCIndex::Q,~(*bIter),CCIndex::Q);
          if(!cs.matchFirst(qFlip).isValid()) {
            mdxInfo << toQString(~(*bIter)) << " is in the coboundary of " << cIter->first
                      << " but no flip matching " << qFlip << " is in the flip table." << endl;

            selectCells(cs, indexAttr, ~(*bIter), cIter->first);
            ccfOK = false;
          }
        }
      }
    }

    // 7. Check that all cells of maximal dimension are positively oriented wrt TOP.
    std::set<CCIndex> negativeOrnt;
    for(CCSignedIndex sx : cs.boundary(+CCIndex::TOP)) {
      if(sx.orientation() == ccf::NEG)
	negativeOrnt.insert(~sx);
    }
    if(!negativeOrnt.empty()) {
      mdxInfo << negativeOrnt.size() << " of " << cs.boundary(CCIndex::TOP).size()
	      << " " << maxDim << "-dimension cells are negatively oriented "
	      << "with respect to TOP." << endl;

      // This is just a warning: not exactly illegal, just not recommended.
      // We only mark these cells if we have no previous problems.
      if(ccfOK) {
	for(CCIndex c : negativeOrnt)
	  selectCell(cs, indexAttr, c);
      }
      ccfOK = false;
    }

    if(ccfOK != true) 
      return false;

    mdxInfo << "Cell complex verified OK." << endl;
  
    return ccfOK;
  }
}
