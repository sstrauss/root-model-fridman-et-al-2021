//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESSS_TRI_DATA_HPP
#define MESH_PROCESSS_TRI_DATA_HPP

#include <Process.hpp>


namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{
  /**
   * \class StatsTriangleValues <ProcessCellAxis.hpp>
   */
  class mdxBase_EXPORT StatsTriangleValues : public Process 
  {
  public:
    StatsTriangleValues(const Process& process) : Process(process) {}
		
		bool initialize(QStringList& parms);  // , QWidget* parent);

    bool run(const QStringList &parms)
    {
      Mesh *mesh = currentMesh();
      if(!mesh) throw(QString("No current mesh"));
      return run(mesh, stringToBool(parms[0]), parms[1], parms[2].toFloat());
    }
  
    bool run(Mesh* mesh, bool selection, QString stat, float prc);
  
    QString name() const { return "Mesh/Triangle Data/Average Tri Value"; }
    QString description() const {
      return "Compute average of triangle values."; }
    QStringList parmNames() const { return QStringList() 
		  << "Selection only"
			<< "Statistic"
			<< "Percentile"; }
    QStringList parmDescs() const { return QStringList()
		  << "Compute only for selected triangles"
			<< "Type of statistic computed"
			<< "Value for percentile"; }
    QStringList parmDefaults() const { return QStringList() 
		  << "Yes"
			<< "Percentile"
			<< "50"; }
    ParmChoiceMap parmChoice() const
    {
      ParmChoiceMap map;
      map[0] = booleanChoice();
			map[1] = QStringList() << "WeightedAverage" << "Percentile";
      return map;
    }
    QIcon icon() const { return QIcon(":/images/MakeHeatMap.png"); }
  };

}
#endif
