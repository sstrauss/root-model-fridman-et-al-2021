//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessSignal.hpp>
#include <Progress.hpp>
#include <CCUtils.hpp>

#include "ui_LoadHeatMap.h"
#include <omp.h>

namespace mdx 
{
//
//  bool ViewMeshProcess::run(const QStringList &parms)
//  {
//    QStringList warning;
//  
//    Mesh* m = currentMesh();
//  
//    if(not m)
//      return setErrorMessage("Error, you need an active mesh");
//  
//    if(not parms[0].isEmpty()) {
//      m->setShowSurface(stringToBool(parms[0]));
//    }
//    // RSS Should change "Normal" to "Signal" here
//    if(not parms[1].isEmpty()) {
//      if(parms[1] == "Normal")
//        m->setShowVertex("Signal");
//      else if(parms[1] == "Heat")
//        m->setShowLabel("Label Heat");
//      else if(parms[1] == "Label")
//        m->setShowLabel("Label");
//      else
//        warning << QString("Invalid surface type '%1'").arg(parms[1]);
//    }
//    if(not parms[2].isEmpty()) {
//      if(parms[2] == "Signal")
//        m->setShowVertex("Signal");
//      else if(parms[2] == "Texture")
//        m->setShowVertex("Stack Texture");
//      else if(parms[2] == "Image")
//        m->setShowVertex("Image Texture");
//      else
//        warning << QString("Invalid signal type '%1'").arg(parms[2]);
//    }
//    if(not parms[3].isEmpty())
//      m->setBlending(stringToBool(parms[3]));
//    if(not parms[4].isEmpty())
//      m->setCulling(stringToBool(parms[4]));
//    if(not parms[5].isEmpty()) {
//      m->setShowMesh(stringToBool(parms[5]));
//    }
//    if(not parms[6].isEmpty())
//      m->setMeshView(parms[6]);
//    if(not parms[7].isEmpty())
//      m->setShowMeshLines(stringToBool(parms[7]));
//    if(not parms[8].isEmpty())
//      m->setShowMeshPoints(stringToBool(parms[8]));
//    if(not parms[9].isEmpty())
//      m->setShowMeshCellMap(stringToBool(parms[9]));
//    if(not parms[10].isEmpty())
//      m->setScaled(stringToBool(parms[10]));
//    if(not parms[11].isEmpty())
//      m->setTransformed(stringToBool(parms[11]));
//    if(not parms[12].isEmpty())
//      m->setShowBBox(stringToBool(parms[12]));
//    if(parms[13].toDouble() >= 0)
//      m->setBrightness(parms[13].toDouble());
//    if(parms[14].toDouble() >= 0)
//      m->setOpacity(parms[14].toDouble());
//    return true;
//  }
//  REGISTER_PROCESS(ViewMeshProcess);
  
 
  bool MeshProjectSignal::run(const Store* store, const CCStructure &cs, const CCIndexDataAttr &indexAttr, 
                                             CCIndexDoubleAttr &signalAttr, double mindist, double maxdist)
  {
    const HVecUS& data = store->data();
    const Stack* stack = store->stack();
  
    auto faces = cs.faces();
    #pragma omp parallel for
    for(size_t i = 0; i < faces.size(); ++i) {
      CCIndex f = faces[i];
      auto &fIdx = indexAttr[f];
      Point3d fpos = fIdx.pos;
      Point3d fnrml = fIdx.nrml;
      // RSS Is there a nicer way to do this?
      if(stack->showScale() and norm(stack->scale()) != 1.0) {
        fpos = divide(fpos, Point3d(stack->scale()));
        fnrml = divide(fnrml, Point3d(stack->scale()));
      }
      Point3d pmin = (fpos - fnrml * mindist);
      Point3d pmax = (fpos - fnrml * maxdist);
      Point3i pimin(stack->worldToImagei(pmin));
      Point3i pimax(stack->worldToImagei(pmax));
      int steps = int((pimax - pimin).norm() * 2 + 3);
    
      double sum = 0;
      for(int i = 0; i <= steps; i++) {
        Point3i oi(stack->worldToImagei(pmax + fIdx.nrml * (maxdist - mindist) * double(i) / double(steps)));
        if(stack->boundsOK(oi.x(), oi.y(), oi.z()))
          sum += data[stack->offset(oi.x(), oi.y(), oi.z())];
      }
      signalAttr[f] = sum / double(steps);
    }
    faceAttrToVertices(cs, signalAttr, cs.vertices());

    return true;
  }
  REGISTER_PROCESS(MeshProjectSignal);
  
  bool MeshSmoothSignal::run(CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, uint passes)
  {
    auto faces = cs.faces();
    if(faces.empty())
      return true;

    AttrMap<CCIndex, double> signal;
    for(uint j = 0; j < passes; j++) {
      #pragma omp parallel for
      for(uint i = 0; i < faces.size(); i++) {
        CCIndex f = faces[i];
        double wt = indexAttr[f].measure;
        signal[f] = signalAttr[f] * wt;
        for(CCIndex n : cs.neighbors(f)) {
          signal[f] += signalAttr[n] * indexAttr[n].measure;
          wt += indexAttr[n].measure;
        }
        if(wt > 0)
          signal[f] /= wt;
      }
      #pragma omp parallel for
      for(uint i = 0; i < faces.size(); i++) {
        CCIndex f = faces[i];
        signalAttr[f] = signal[f];
      }
    }
    faceAttrToVertices(cs, signalAttr, activeVertices(cs, indexAttr));
          
    return true;
  }
  REGISTER_PROCESS(MeshSmoothSignal);

  bool MeshBrightness::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double amount)
  {
    CCIndexVec faces = activeFaces(cs, indexAttr);
    if(faces.empty())
      return true;

    for(CCIndex f : faces)
      signalAttr[f] *= amount;

    faceAttrToVertices(cs, signalAttr, activeVertices(cs, indexAttr));

    return true;
  }
  REGISTER_PROCESS(MeshBrightness);
 
  bool MeshSetSignal::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double signal)
  {
    for(uint dim = 0; dim <= cs.maxDimension(); dim++)
      if(dim == 1)
        continue;
      else
         for(CCIndex c : cs.cellsOfDimension(dim))
           if(indexAttr[c].selected)
             signalAttr[c] = signal;

    return true;
  }
  REGISTER_PROCESS(MeshSetSignal);
  
  MeshProjectCurvatureOp::MeshProjectCurvatureOp(const CCStructure &_cs, const CCIndexDataAttr &_indexAttr, 
     const CCIndexVec& _vertices, double _radius, bool _selectNhbd, Matrix2x3dVec &_eVecs, Point2dVec &_eVals)
    : cs(_cs), indexAttr(_indexAttr), vertices(_vertices), radius(_radius), selectNhbd(_selectNhbd), eVecs(_eVecs), eVals(_eVals) {}
  
  void MeshProjectCurvatureOp::operator()(int i)
  {
    CCIndex v = vertices[i];
    CCIndexSet nbs = neighbors(cs, indexAttr, v, radius);
  
    std::vector<Point3d> plist, nlist;
    
    auto &vIdx = indexAttr[v];
    plist.push_back(vIdx.pos);
    nlist.push_back(vIdx.nrml);
    for(CCIndex n : nbs) {
      if(n == v)
        continue;
      auto &nIdx = indexAttr[n];
      // Don't stray outside of the selection
      if(selectNhbd and !nIdx.selected)
        continue;
      plist.push_back(nIdx.pos);
      nlist.push_back(nIdx.nrml);
    }
    calcCurvature(plist, nlist, eVecs[i], eVals[i]);
  }
  
  bool MeshProjectCurvature::run(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexSymTensorAttr *curvatureAttr, 
          CCIndexDoubleAttr &signalAttr, const QString &outputFile, const QString &type, double radius, bool selectNhbd, bool updateNormals)
  {
    CCIndexVec vertices = activeVertices(cs, indexAttr);

    // Nothing to do
    if(vertices.empty())
      return true;
  
    Matrix2x3dVec eVecs(vertices.size());
    Point2dVec eVals(vertices.size());
  
    if(updateNormals)
      updateVertexNormals(cs, indexAttr);

    progressStart(QString("Computing curvature"), vertices.size() / omp_get_max_threads());
    MeshProjectCurvatureOp op(cs, indexAttr, vertices, radius, selectNhbd, eVecs, eVals);
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();

    DoubleVec values(vertices.size());
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); i++) {
      CCIndex v = vertices[i];
      if(curvatureAttr)
        (*curvatureAttr)[v] = SymmetricTensor(eVecs[i][0], eVecs[i][1], Point3d(eVals[i][0], eVals[i][1], 0));
      if(type == "Minimal")
        values[i] = signalAttr[v] = std::min(eVals[i][0], eVals[i][1]);
      else if (type == "Maximal")
        values[i] = signalAttr[vertices[i]] = std::max(eVals[i][0], eVals[i][1]);
      else if (type == "Gaussian")
        values[i] = signalAttr[vertices[i]] = eVals[i][0] * eVals[i][1];
      else if (type == "SumSquare")
        values[i] = signalAttr[vertices[i]] = eVals[i][0] * eVals[i][0] + eVals[i][1] * eVals[i][1];
      else if (type == "Average")
        values[i] = signalAttr[vertices[i]] = (eVals[i][0] + eVals[i][0])/2.0;
    }

    // Write curvatures to file if required
    // RSS This should be moved to a generic axis export
    if(!outputFile.isEmpty()) {
      QFile f(outputFile);
      if(f.open(QIODevice::WriteOnly)) {
        QTextStream ts(&f);
        ts << QString("X,Y,Z,X1,Label,Y1,Z1,X2,Y2,Z2,k1 (1/%1),k2 (1/%1)\n").arg(UM);
        for(uint i = 0; i < vertices.size(); i++) {
          auto &vIdx = indexAttr[vertices[i]];
          Point3d pos(vIdx.pos);
          Point3d e1 = eVecs[i][0], e2 = eVecs[i][1];
          double c1 = eVals[i][0], c2 = eVals[i][1];
          QStringList fields;
          fields << QString::number(pos.x(), 'g', 10) << QString::number(pos.y(), 'g', 10)
                 << QString::number(pos.z(), 'g', 10) << QString::number(vIdx.label)
                 << QString::number(e1.x(), 'g', 10) << QString::number(e1.y(), 'g', 10)
                 << QString::number(e1.z(), 'g', 10) << QString::number(e2.x(), 'g', 10)
                 << QString::number(e2.y(), 'g', 10) << QString::number(e2.z(), 'g', 10)
                 << QString::number(c1, 'g', 10) << QString::number(c2, 'g', 10);
          ts << fields.join(",") << "\n";
          ++i;
        }
      } else
        setErrorMessage(QString("Error, could not open file '%1' for writing. No output written").arg(outputFile));
    }

    return true;
  }
  REGISTER_PROCESS(MeshProjectCurvature);

  MeshSignalGradOp::MeshSignalGradOp(const CCStructure &_cs, const CCIndexDataAttr &_indexAttr, const CCIndexDoubleAttr &_signalAttr,
                                          const CCIndexVec& _vertices, CCIndexDoubleAttr & _result)
    : cs(_cs), indexAttr(_indexAttr), signalAttr(_signalAttr), vertices(_vertices), result(_result) {}
  
  void MeshSignalGradOp::operator()(int i)
  {
    CCIndex v = vertices[i];
    auto &vIdx = indexAttr[v];

    double num = 0,  wt = 0;
    Point3d gradient(0,0,0);

    // Find the signal gradient
    // RSS Fix, this doesn't check if on boundary
    auto nbs = vertexNeighbors(cs, v);
    for(uint i = 0; i < nbs.size(); i++) {
    	CCIndex r = nbs[i];
    	CCIndex n = nbs[(i+1)%nbs.size()];
      auto &rIdx = indexAttr[r];
      auto &nIdx = indexAttr[n];
	    num++;  
	    wt++;
	    Point3d tgrad = triangleGradient(vIdx.pos,rIdx.pos,nIdx.pos,signalAttr[v],signalAttr[r],signalAttr[n]);
	    gradient+= tgrad;

// RSS What is this for? probably should be separate process
//	    Point3d bary = 1.0/3.0*(vIdx.pos+rIdx.pos+nIdx.pos);
//	    Point3d p1 = bary+0.00001*tgrad;
//	    Point3d p2 = bary-0.00001*tgrad;

//      CCIndex vC = CCIndexFactory.getIndex();
//      cs.addCell(vC);
//      idxData[vC].pos = p1;
//
//      CCIndex nC = CCIndexFactory.getIndex();
//      cs.addCell(nC);
//      idxData[nC].pos = p2;
//
//      CCIndex eC = CCIndexFactory.getIndex();
//      cs.addCell(eC, +vC -nC);
    }
    result[v] = norm(gradient)/wt; 
  }
  
  bool MeshSignalGrad::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr)
  {
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
    progressStart(QString("Mesh signal gradient"), vertices.size() / omp_get_max_threads());
    // Allocate space for result, need to initialize or not thread safe
    CCIndexDoubleAttr result;
  
// Cell complex for debugging
//    // Get a 2D cell complex by name
//    QString ccName("Signal gradient");
//
//    CCStructure &cs = mesh.ccStructure(ccName);
//    cs = CCStructure(2);
//
//    // We also need the attribute map
//    CCIndexDataAttr &idxData = mesh.indexAttr();
//
//    std::cout<<"Initializing op (sig grad)"<<std::endl;

    MeshSignalGradOp op(cs, indexAttr, signalAttr, vertices, result);

    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();

    // Update the Signal
    for(CCIndex v : vertices)
      signalAttr[v] = result[v];

//    // Turn visualization on in GUI
//    mesh.drawParms(ccName).setGroupVisible("Vertices", true);
//    mesh.drawParms(ccName).setGroupVisible("Edges", true);
//
//    // Tell the mesh to regenerate the VBOs
//    mesh.updateAll(ccName);
//
//    std::cout<<"Updating tri (sig grad)"<<std::endl;
// 
//    mesh->updateTriangles();
//
//    std::cout<<"Completed sig grad"<<std::endl;
//
    // Copy signal to faces
    faceAttrFromVertices(cs, signalAttr, cs.faces());

    return true;
  }
  REGISTER_PROCESS(MeshSignalGrad);
 
  MeshGaussianBlurOp::MeshGaussianBlurOp(const CCStructure &_cs, const CCIndexDataAttr &_indexAttr, const CCIndexDoubleAttr &_signalAttr, 
     const CCIndexVec& _vertices, double _radius, CCIndexDoubleAttr &_result)
    : cs(_cs), indexAttr(_indexAttr), signalAttr(_signalAttr), vertices(_vertices), radius(_radius), result(_result) {}
  
  void MeshGaussianBlurOp::operator()(int i)
  {
    CCIndex v = vertices[i];
    auto &vIdx = indexAttr[v];
    double sigma = radius / 2;
    double den = 0, num = 0, g = 0, ar = 0, wt = 0;
    auto VSet = neighbors(cs, indexAttr, v, radius);   // Neighborhood processing
  
    // Gaussian blur over the cell
    for(CCIndex r : VSet) {
      auto &rIdx = indexAttr[r];
      if(norm(rIdx.pos - vIdx.pos) < radius) {
        den = sqrt(6.28 * (pow(sigma, 2)));
        num = exp(-(pow((norm(rIdx.pos - vIdx.pos)), 2) / (2 * pow(sigma, 2))));
        g = num / den;
        ar = ar + g * signalAttr[r];
        wt = wt + g;
      }
    }
    result[v] = ar / wt;   // Blurring the signal
  }
  
  bool MeshGaussianBlur::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius)
  {
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
  
    progressStart(QString("Gaussian Blur"), vertices.size() / omp_get_max_threads());
    // Allocate space for result, need to initialize or not thread safe
    CCIndexDoubleAttr result;

    MeshGaussianBlurOp op(cs, indexAttr, signalAttr, vertices, radius, result);
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();
  
    // Update the Signal
    for(CCIndex v : vertices)
      signalAttr[v] = result[v];

    // Copy signal to faces
    faceAttrFromVertices(cs, signalAttr, cs.faces());
  
    return true;
  }
  REGISTER_PROCESS(MeshGaussianBlur);
  
  bool MeshDiffGaussians::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius1, double radius2)
  {
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
  
    // Find first Gaussian, allocate space for result, need to initialize or not thread safe
    CCIndexDoubleAttr firstGauss;
    progressStart(QString("Calculating first Gaussian"), vertices.size() / omp_get_max_threads());
    if(radius1 > 0) {
      MeshGaussianBlurOp op1(cs, indexAttr, signalAttr, vertices, radius1, firstGauss);
      #pragma omp parallel for
      for(size_t i = 0; i < vertices.size(); ++i) {
        if(!progressAdvance(i))
          continue;
        op1(i);
      }
      if(!progressAdvance(vertices.size()))
        userCancel();
    }
  
    progressStart(QString("Calculating second Gaussian"), vertices.size() / omp_get_max_threads());
    // Find second Gaussian, allocate space for result, need to initialize or not thread safe
    CCIndexDoubleAttr secondGauss;
    MeshGaussianBlurOp op2(cs, indexAttr, signalAttr, vertices, radius2, secondGauss);
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op2(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();
  
    // Update the Signal
    for(CCIndex v : vertices)
      signalAttr[v] = firstGauss[v] - secondGauss[v];
  
    // Copy signal to faces
    faceAttrFromVertices(cs, signalAttr, cs.faces());

    return true;
  }
  REGISTER_PROCESS(MeshDiffGaussians);
  
  MeshLocalMinimaOp::MeshLocalMinimaOp(const CCStructure &_cs, CCIndexDataAttr &_indexAttr, const CCIndexDoubleAttr &_signalAttr, 
     const CCIndexVec& _vertices, double _radius, int _label)
    : cs(_cs), indexAttr(_indexAttr), signalAttr(_signalAttr), vertices(_vertices) , radius(_radius) , label(_label) {}
  
  void MeshLocalMinimaOp::operator()(int i)
  {
    CCIndex v = vertices[i];
    auto &vIdx = indexAttr[v];
    auto VSet = neighbors(cs, indexAttr, v, radius);
  
    // Finding the lowest v->signal in the set of vertices respective to a cell
    double lowest = DBL_MAX;
    for(CCIndex r : VSet) {
      double signal = signalAttr[r];
      if(signal < lowest)
        lowest = signal;
    }
  
    // Label the vertex at the local minima
    if(signalAttr[v] == lowest)
      vIdx.label = label;
  }
  
  bool MeshLocalMinima::run(Mesh &mesh, const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, double radius)
  {
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
  
    progressStart(QString("Finding Local Minima"), vertices.size() / omp_get_max_threads());

    int startLabel = mesh.nextLabel();
    MeshLocalMinimaOp op(cs, indexAttr, signalAttr, vertices, radius, startLabel);
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();
    for(size_t i = 0; i < vertices.size(); ++i) {
      CCIndex v = vertices[i];
      auto &vIdx = indexAttr[v];
      if(vIdx.label == startLabel) {
        int label = mesh.nextLabel();
        vIdx.label = label;
        for(CCIndex n : cs.incidentCells(v,2))
          indexAttr[n].label = label;
      }
    }
    // Copy label to faces
    faceLabelsFromVertices(cs, indexAttr, cs.faces());

    return true;
  }
  REGISTER_PROCESS(MeshLocalMinima);
  
  MeshNormalizeOp::MeshNormalizeOp(const CCStructure &_cs, const CCIndexDataAttr &_indexAttr, CCIndexDoubleAttr &_signalAttr, 
     const CCIndexVec& _vertices, double _radius, CCIndexDoubleAttr & _result)
    : cs(_cs), indexAttr(_indexAttr), signalAttr(_signalAttr), vertices(_vertices), radius(_radius), result(_result) {}
  
  void MeshNormalizeOp::operator()(int i)
  {
    CCIndex v = vertices[i];
    double min = DBL_MAX;
    double max = 0;
    auto nbs = neighbors(cs, indexAttr, v, radius);
  
    // Normalizing the signal of mesh
    for(CCIndex n : nbs) {
      double signal = signalAttr[n];
      if(min > signal)
        min = signal;
      if(max < signal)
        max = signal;
    }
    result[v] = (signalAttr[v] - min) / (max - min);
  }
  
  bool MeshNormalize::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius)
  {
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);

    progressStart(QString("Normalizing Signal"), vertices.size() / omp_get_max_threads());

    CCIndexDoubleAttr result;
    MeshNormalizeOp op(cs, indexAttr, signalAttr, vertices, radius, result);
  
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();
  
    // Update the Signal
    for(CCIndex v : vertices)
      signalAttr[v] = result[v] * 65535.0;

    // Copy signal to faces
    faceAttrFromVertices(cs, signalAttr, cs.faces());
  
    return true;
  }
  REGISTER_PROCESS(MeshNormalize);
  
  MeshDilationOp::MeshDilationOp(const CCStructure &_cs, const CCIndexDataAttr &_indexAttr, CCIndexDoubleAttr &_signalAttr, 
                                 const CCIndexVec& _vertices, double _radius, CCIndexDoubleAttr &_result)
    : cs(_cs), indexAttr(_indexAttr), signalAttr(_signalAttr), vertices(_vertices), radius(_radius), result(_result) {}
  
  void MeshDilationOp::operator()(int i)
  {
    CCIndex v = vertices[i];
    double max = -DBL_MAX;
    auto nbs = neighbors(cs, indexAttr, v, radius);

    // Dilation
    for(CCIndex n : nbs) {
      if(max < signalAttr[n])
        max = signalAttr[n];
    }
    result[v] = max;
  };
  
  bool MeshDilation::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius)
  {
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
    progressStart(QString("Dilating Signal"), vertices.size() / omp_get_max_threads());

    // Allocate space for result, need to initialize or not thread safe
    CCIndexDoubleAttr result;
    MeshDilationOp op(cs, indexAttr, signalAttr, vertices, radius, result);
  
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();
  
    // Update the Signal
    for(CCIndex v : vertices)
      signalAttr[v] = result[v];

    // Copy signal to faces
    faceAttrFromVertices(cs, signalAttr, cs.faces());
  
    return true;
  }
  REGISTER_PROCESS(MeshDilation);
  
  MeshErosionOp::MeshErosionOp(const CCStructure &_cs, const CCIndexDataAttr &_indexAttr, CCIndexDoubleAttr &_signalAttr, 
                               const CCIndexVec& _vertices, double _radius, CCIndexDoubleAttr &_result)
    : cs(_cs), indexAttr(_indexAttr), signalAttr(_signalAttr), vertices(_vertices), radius(_radius), result(_result) {}
  
  void MeshErosionOp::operator()(int i)
  {
    CCIndex v = vertices[i];
    double min = DBL_MAX;
    auto nbs = neighbors(cs, indexAttr, v, radius);
  
    // Erosion
    for(CCIndex n : nbs) {
      if(min > signalAttr[n])
        min = signalAttr[n];
    }
    result[v] = min;
  };
  
  bool MeshErosion::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius)
  {
    const CCIndexVec &vertices = activeVertices(cs, indexAttr);
    progressStart(QString("Eroding Signal"), vertices.size() / omp_get_max_threads());

    // Allocate space for result, need to initialize or not thread safe
    CCIndexDoubleAttr result;
    MeshErosionOp op(cs, indexAttr, signalAttr, vertices, radius, result);
  
    #pragma omp parallel for
    for(size_t i = 0; i < vertices.size(); ++i) {
      if(!progressAdvance(i))
        continue;
      op(i);
    }
    if(!progressAdvance(vertices.size()))
      userCancel();
  
    // Update the Signal
    for(CCIndex v : vertices)
      signalAttr[v] = result[v];

    // Copy signal to faces
    faceAttrFromVertices(cs, signalAttr, cs.faces());
  
    return true;
  }
  REGISTER_PROCESS(MeshErosion);
  
  bool MeshClosing::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius)
  {
    MeshDilation md(*this);
    md.run(cs, indexAttr, signalAttr, radius);

    MeshErosion me(*this);
    me.run(cs, indexAttr, signalAttr, radius);

    return true;
  }
  REGISTER_PROCESS(MeshClosing);
  
  bool MeshOpening::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius)
  {
    MeshErosion me(*this);
    me.run(cs, indexAttr, signalAttr, radius);

    MeshDilation md(*this);
    md.run(cs, indexAttr, signalAttr, radius);

    return true;
  }
  REGISTER_PROCESS(MeshOpening);
  
  bool MeshRescaleSignal::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, 
             bool zeroRef, double percentile, double minimum, double maximum, Point2d &signalBounds)
  {
    if(percentile > 0) {
      const CCIndexVec &faces = cs.faces();
      std::vector<double> values(faces.size());
      int i = 0;
      for(CCIndex f : faces)
        values[i++] = signalAttr[f];
      std::sort(values.begin(), values.end());
  
      int rankmax = values.size() - 1;
      int rankmin = 0;
      if(percentile < 100) {
        rankmax = (values.size() * percentile) / 100;
        rankmin = values.size() - rankmax;
      }
      minimum = values[rankmin];
      maximum = values[rankmax];
      if(zeroRef) {
        if(minimum * maximum < 0) {
          maximum = std::max(fabs(maximum), fabs(minimum));
          minimum = -maximum;
        } else if(maximum > 0)
          minimum = 0;
        else if(minimum < 0)
          maximum = 0;
        else {
          minimum = 0;
          maximum = 1;
        }
      }
    }
    signalBounds = Point2d(minimum, maximum);

    return true;
  }
  REGISTER_PROCESS(MeshRescaleSignal);

  bool MeshCombineSignals::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, 
                                                      CCIndexDoubleAttr &signalAttr2, CCIndexDoubleAttr &signalResAttr)
  {
    
    signalResAttr.clear();

    for(CCIndex f : cs.faces()){
      if(parm("Operation") == "Addition")
        signalResAttr[f] = signalAttr[f] + signalAttr2[f];
      else if(parm("Operation") == "Addition")
        signalResAttr[f] = signalAttr[f] + signalAttr2[f];
      else if(parm("Operation") == "Multiplication")
        signalResAttr[f] = signalAttr[f] * signalAttr2[f];
      else if(parm("Operation") == "Ratio")
        if(signalAttr2[f] != 0) signalResAttr[f] = signalAttr[f] / signalAttr2[f];
    }

    return true;
  }
  REGISTER_PROCESS(MeshCombineSignals);

  bool MeshTransformSignal::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, 
                                                        CCIndexDoubleAttr &signalAttr, CCIndexDoubleAttr &signalResAttr)
  {
    signalResAttr.clear();

    QString op = parm("Operation");
    if(op == "Inverse") {
      for(const auto &pr : signalAttr)
        if(pr.second != 0) 
          signalResAttr[pr.first] = 1.0 / pr.second;
    } else if(op == "Invert") {
      for(const auto &pr : signalAttr)
        signalResAttr[pr.first] = -1.0 * pr.second;
    } else
      throw QString("%1: Invalid operation: %2").arg(name()).arg(op);

    return true;
  }
  REGISTER_PROCESS(MeshTransformSignal);

  bool MeshTrimSignal::run(Mesh &mesh, const QString &signalName, double lowerThreshold, double upperThreshold)
  {
    if(lowerThreshold > upperThreshold)
      throw QString("%1::run Lower threshold must be <= upper threhsold").arg(name());
    if(!mesh.signalExists(signalName))
      throw QString("%1::run Signal '%2' does not exist").arg(name()).arg(signalName);
    auto &signalAttr = mesh.signalAttr<double>(signalName);
    CCIndexSet toTrim;
    for(auto &pr : signalAttr)
      if(pr.second < lowerThreshold or pr.second > upperThreshold)
        toTrim.insert(pr.first);
    for(CCIndex c : toTrim)
      signalAttr.erase(c);

    return true;
  }
  REGISTER_PROCESS(MeshTrimSignal);

  bool MeshEraseSignal::run(Mesh &mesh, const QString &signalName)
  {
    if(!mesh.signalExists(signalName))
      throw QString("%1::run Signal '%2' does not exist").arg(name()).arg(signalName);
    auto &signalAttr = mesh.signalAttr<double>(signalName);
    auto &indexAttr = mesh.indexAttr();
    CCIndexSet toErase;
    for(auto &pr : signalAttr)
      if(indexAttr[pr.first].selected)
        toErase.insert(pr.first);
    for(CCIndex c : toErase)
      signalAttr.erase(c);

    return true;
  }
  REGISTER_PROCESS(MeshEraseSignal);

  bool MeshRenameSignal::run(Mesh &mesh, const QString &signalName)
  {
    if(!mesh.signalExists(signalName))
      throw QString("%1::run Signal '%2' does not exist").arg(name()).arg(signalName);
    mesh.signalErase(signalName);

    return true;
  }
  REGISTER_PROCESS(MeshRenameSignal);

  bool MeshDeleteSignal::run(Mesh &mesh, const QString &signalName)
  {
    if(!mesh.signalExists(signalName))
      throw QString("%1::run Signal '%2' does not exist").arg(name()).arg(signalName);
    mesh.signalErase(signalName);

    return true;
  }
  REGISTER_PROCESS(MeshDeleteSignal);
}
