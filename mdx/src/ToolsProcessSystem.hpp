//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2017 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef TOOLS_PROCESS_SYSTEM_HPP
#define TOOLS_PROCESS_SYSTEM_HPP

#include <Process.hpp>
//#include <QFileDialog>
//#include <QTreeWidget>

/**
 * \file ToolsProcessSystem.hpp
 * This file contains miscellaneous system processes
 */

//class QDialog;
//class Ui_LoadStackDialog;
//class Ui_LoadMeshDialog;
//class Ui_ImportMeshDialog;
//class Ui_SaveMeshDialog;
//class Ui_ExportMeshDialog;
//class Ui_PlyCellGraphDlg;
//class QFile;
//class QIODevice;

namespace mdx 
{
   /**
   * \class SaveViewFile ToolsProcessSystem.hpp <ToolsProcessSystem.hpp>
   *
   * Save the view file
   *
   * \ingroup ToolsProcess
   */
  class mdxBase_EXPORT SaveViewFile : public Process {
  public:
    SaveViewFile(const Process& proc) : Process(proc) 
    {
      setName("Tools/System/Save View");
      setDesc("Save the view file for the current configuration.");
      setIcon(QIcon(":/images/save.png"));

      addParm("File Name", "Name of view file to save", "");
    }
    bool initialize(QWidget* parent);

    bool run();
    /**
     * Save the view file
     * \param fileName File to save the parameters in
     */
    bool run(const QString& fileName);
  };
  
  /**
   * \class QuickSaveFile ToolsProcessSystem.hpp <ToolsProcessSystem.hpp>
   *
   * Save all active files
   *
   * \ingroup ToolsProcess
   */
  class mdxBase_EXPORT QuickSaveFile : public Process {
  public:
    QuickSaveFile(const Process& proc) : Process(proc) 
    {
      setName("Tools/System/Quick Save");
      setDesc("Save all active mesh, stack and view files.");
      setIcon(QIcon(":/images/save.png"));
    }
    bool run();

    void saveStore(int stackNr, Store* store, int compressionLevel);
    void saveMesh(int meshNr);
  };

  /**
   * \class TakeSnapshot ToolsProcessSystem.hpp <ToolsProcessSystem.hpp>
   *
   * This process allows scripts and other processes to take snapshots of the current result.
   *
   * Parameter       | Description
   * ----------------|------------
   * Filename        | Name of the output file. The extension will define will file format is used.
   * Expend Frustrum | If true and the widht/height ratio is not the same as the screen, the frustrum will be expended
   * Width           | Width of the image
   * Height          | Height of the image
   * Oversampling    | Oversampling factor (see Notes)
   * Quality         | For lossy file formats, quality of the image on a scale from 0 to 100.
   *
   * Note on oversampling
   * ====================
   *
   * If oversampling is not 1, then the scene is rendered on an image with width and height multiplied by the
   * oversampling. The result is then scaled back to the wanted size, using bilinear filtering.
   *
   * \ingroup ToolsProcess
   */
  class mdxBase_EXPORT TakeSnapshot : public Process 
  {
  public:
    TakeSnapshot(const Process& proc) : Process(proc) 
    {
      setName("Tools/System/Snapshot");
      setDesc("Take a snapshot of the current view");
      setIcon(QIcon(":/images/SaveScreenshot.png"));

      addParm("File Name", "Name of file to save", "");
      addParm("Expand Frustum", "Expand frustrum if required", "No", booleanChoice());
      addParm("Width", "Width of screen buffer", "0");
      addParm("Height", "Height of screen buffer", "0");
      addParm("Oversampling", "Amount of oversampling", "1.0");
      addParm("Quality", "Compression quality in percent", "95");
    }

    bool run()
    {
      QString fileName = parm("File Name");
      if(fileName.isEmpty())
        return setErrorMessage("Error, no file name specified.");
      bool expand_frustum = stringToBool(parm("Expand Frustum"));
      int width = parm("Width").toInt();
      int height = parm("Height").toInt();
      double overSampling = parm("Oversampling").toDouble();
      int quality = parm("Quality").toInt();
      return run(fileName, overSampling, width, height, quality, expand_frustum);
    }
    bool run(QString fileName, double overSampling, int width, int height, int quality, bool expand_frustum);
  };

  /**
   * \class LoadAllData ToolsProcessSystem.hpp <ToolsProcessSystem.hpp>
   *
   * Load all the files as specified in the various objects (fileNames).
   *
   * Note that currently, it will load only the main store of the stacks as
   * only they can have any data.
   *
   * \ingroup ToolsProcess
   */
  class mdxBase_EXPORT LoadAllData : public Process
  {
  public:
    LoadAllData(const Process& proc) : Process(proc) 
    {
      setName("Tools/System/Load All");
      setDesc("Load the data for all existing objects, using the fileName and properties set in them.");
      setIcon(QIcon(":/images/open.png"));
    }
    bool run();
    bool runLoadAll();
  
  protected:
    bool loadStore(Stack* stack, Store* store, QStringList& errors);
  };
  
  /**
   * \class LoadViewFile ToolsProcessSystem.hpp <ToolsProcessSystem.hpp>
   *
   * Load a view file and all the associated files.
   *
   * \ingroup ToolsProcess
   */
  class mdxBase_EXPORT LoadViewFile : public Process 
  {
  public:
    LoadViewFile(const Process& proc) : Process(proc) 
    {
      setName("Tools/System/Load View");
      setDesc("Load a view file and set all the fields and interface.");
      setIcon(QIcon(":/images/open.png"));

      addParm("File Name", "File name for view file", "");
    }
  
    bool run();
    /**
     * Load the file \c fileName
     */
    bool run(QString fileName);
  
    bool initialize(QWidget* parent);
  };
}
#endif

