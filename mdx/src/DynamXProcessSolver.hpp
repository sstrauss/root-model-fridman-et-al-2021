//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef DYNAMX_PROCESS_SOLVER_HPP
#define DYNAMX_PROCESS_SOLVER_HPP

#include <Process.hpp>
#include <Solver.hpp>

namespace mdx
{
  // Class to hold FEM parameters
  class mdxBase_EXPORT SolverParms : public Process 
  {
  public:
    FemParms(const Process &process) : Process(process) {}

    // Don't do anything, process is only for parms
    bool run(const QStringList &) { return true; }

    // Functions for Gui
    QString description() const { return "Parameters for ODE Solver"; }

    QStringList parmNames() const { return Solver::parmNames(); }
    QStringList parmDescs() const { return Solver::parmDescs(); }
    QStringList parmDefaults() const { return Solver::parmDefaults(); }

    // Icon file
    QIcon icon() const { return QIcon(":/images/Parameters.png"); }
  };
}
#endif
