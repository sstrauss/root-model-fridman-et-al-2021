//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2021 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <MeshProcessLineage.hpp>
#include <MeshUtils.hpp>

#include <QFileDialog>

namespace mdx 
{
  bool SaveParents::initialize(QWidget* parent)
  {
    QString fileName(parm("File Name"));
    if(parent)
      fileName = QFileDialog::getSaveFileName(parent, 
               "Select file to save labels in", parm("File Name"), "CSV files (*.csv)");
    // check fileName
    if(fileName.isEmpty())
      return setErrorMessage("File name is empty");
    // check ending, add suffix if required
    QString suffix = ".csv";
    if(fileName.right(suffix.size()) != suffix)
      fileName.append(suffix);
  
    setParm("File Name", fileName);
    return true;
  }

  bool SaveParents::run()
  {
    Mesh *mesh = currentMesh();
    if(!currentMesh())
      throw(QString("SaveParents - No current mesh"));

    return run(*mesh, parm("File Name"), stringToBool(parm("Existing Only")));
  } 

  bool SaveParents::run(Mesh &mesh, const QString& fileName, bool saveOnlyExisting)
  {
    IntIntAttr& parentLabel = mesh.labelMap("Parents");

    if(parm("Labeling") != "Parents")
      parentLabel = mesh.labelMap(parm("Labeling"));

    if(parentLabel.size() == 0) {
      setErrorMessage(QString("There are no parent labels").arg(fileName));
      return false;
    }
  
    // Open output file
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
      setErrorMessage(QString("File '%1' cannot be opened for writing").arg(fileName));
      return false;
    }
    QTextStream out(&file);
    // Write header
    out << "Label, Parent Label" << endl;
  
    std::unordered_set<int> allExistingLabels;
    for(auto &cs : mesh.ccs()){
      CCIndexVec ccv = cs.second.faces();
      if(parm("Dimension") == "Volume") ccv = cs.second.volumes();
      else if(parm("Dimension") == "Vertex") ccv = cs.second.vertices();
      for(CCIndex f : ccv) {
        auto &fIdx = mesh.indexAttr()[f];
        if(fIdx.label > 0)
          allExistingLabels.insert(fIdx.label);
      }
    }
        
    // Write data
    forall(const IntIntPair& p, parentLabel) {
      int label = p.first;
      if(saveOnlyExisting and allExistingLabels.find(label) == allExistingLabels.end()) continue;
      int parent = p.second;
      out << label << "," << parent << endl;
    }
    file.close();

    setStatus(QString("Storing %1 parent labels to file %2").arg(parentLabel.size()).arg(fileName));

    return true;
  }
  REGISTER_PROCESS(SaveParents);
  
  bool LoadParents::initialize(QWidget* parent)
  {
    QString fileName(parm("File Name"));
    if(parent)
      fileName = QFileDialog::getOpenFileName(parent, "Choose parent label file to load", parm("File Name"), 
           "CSV files (*.csv);; MARS files (*.txt);; All files (*.*)");
  
    // check file
    if(fileName.isEmpty())
      return setErrorMessage("File name is empty");

    // Get the type
    QString fileType = fileName.split(".").last().toUpper();
    if(fileType != "CSV" and fileType != "TXT")
      return setErrorMessage(QString("Invalid file type '%1', must be .csv(label, parent)"
                                               " or .txt(parent: label, label ...)").arg(fileType));
  
    setParm("File Name", fileName);
    setParm("File Type", fileType);

    return true;
  }

  bool LoadParents::run(Mesh &mesh, const QString &fileName, const QString &fileType, QString labeling, bool keep)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly))
      throw QString("%1::run File '%2' cannot be opened for reading").arg(name()).arg(fileName);

    if(labeling.isEmpty())
      labeling = mesh.labeling();
    if(labeling.isEmpty() or labeling == "Labels")
      labeling = "Parents";

    // Start with an empty map
    IntIntAttr parentLabel;

    // Add existing if required
    if(keep)
      parentLabel = mesh.labelMap(labeling);

    // Read the file
    QTextStream ss(&file);
    if(fileType == "CSV") {
      QString line = ss.readLine();
      QStringList fields = line.split(",");
      if(fields.size() != 2)
        throw(QString("File '%1' should be in the format 'label, parent'").arg(fileName));

      int lineNum = 1;
      while(ss.status() == QTextStream::Ok) {
        ++lineNum;

        // Get the line and split it
        fields = ss.readLine().split(",");
        if(fields.size() != 2)
          break;

        // Get the data
        bool ok;
        int label = fields[0].toInt(&ok);
        if(!ok)
          throw(QString("Error line %1: the first column is not an integer number but '%2'").arg(fileName).arg(lineNum));

        int parent = fields[1].toInt(&ok);
        if(!ok)
          throw(QString("Error line %1: the second column is not an integer number but '%2'").arg(fileName).arg(lineNum));

        parentLabel[label] = parent;
      }
    } else if(fileType == "TXT") {

      // No header in MARS-ALT format
      int lineNum = 1;
      while(ss.status() == QTextStream::Ok) {
        ++lineNum;

        // Get the line and split it
        QString line = ss.readLine();
        QStringList fields = line.split(":");
        if(fields.size() != 2)
          break; // End of file?

        // Get the data
        bool ok;
        int parent = fields[0].toInt(&ok);
        if(!ok)
          throw(QString("File %1, line %2: should be in the format 'parent: label, label ...'").arg(fileName).arg(lineNum));

        QStringList labels = fields[1].split(",");
        for(int i = 0; i < labels.size(); i++) {
          int label = labels[i].toInt(&ok);
          if(!ok)
            throw(QString("File %1, line %2, label %3: should be in the format 'parent: label, label ...'")
                .arg(fileName).arg(lineNum).arg(i));

          if(label == 0 or parent == 0)
            continue;
          parentLabel[label] = parent;
        }
      }
    } else
      throw QString("%1::Run Invalid file type %2").arg(name()).arg(fileType);
  
    // update the parent map
    mesh.labelMap(labeling) = parentLabel;
    mesh.setLabeling(labeling);
    mesh.updateProperties();

    setStatus(QString("%1 parent labels loaded from file %2").arg(parentLabel.size()).arg(fileName));

    return true;
  }
  REGISTER_PROCESS(LoadParents);
 
  bool ResetParents::run(Mesh &mesh, const QString &labeling)
  {
    if(labeling.isEmpty())
      return false;

    // Clear the parent map
    mesh.labelMap(labeling).clear();
    // Clear any heat maps
    for(QString s : mesh.heatAttrList(labeling))
      mesh.heatErase(s, labeling);

    return true;
  }
  REGISTER_PROCESS(ResetParents);

  bool SetParent::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, const QString &labeling, int label)
  {
    if(labeling.isEmpty())
      return false;

    // Start with an empty map
    IntIntAttr &parentLabel = mesh.labelMap(labeling);

    if(cs.maxDimension() == 3) {
      // if mesh 3D -> check selected volumes
      for(CCIndex l : cs.volumes())
        if(indexAttr[l].selected) 
          parentLabel[indexAttr[l].label] = label;
    } else if(cs.maxDimension() == 2) {
      // if mesh 2D -> check selected faces
      for(CCIndex f : cs.faces())
        if(indexAttr[f].selected) 
          parentLabel[indexAttr[f].label] = label;
    }

    return true;
  }
  REGISTER_PROCESS(SetParent);

  bool SelectParents::run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, const QString &labeling, int label)
  {
    if(labeling.isEmpty())
      return false;

    // Start with an empty map
    IntIntAttr& parentLabel = mesh.labelMap(labeling);

    // if mesh 3D -> check selected volumes
    forall(CCIndex v, cs.volumes()){
      if(parentLabel[indexAttr[v].label] == label) indexAttr[v].selected = true;
    }

    // if mesh 2D -> check selected faces
    // TODO ADD


    mesh.updateProperties();
    return true;
  }
  REGISTER_PROCESS(SelectParents);

  //------------------------------
//  // Eliminate from parents labels which are not in mesh1 or mesh2
//  bool CorrectParents::run(Mesh* mesh1, Mesh* mesh2)
//  {
//    IntIntAttr &daughterParentMap = mesh2->labelMap("Parents");
//    if(daughterParentMap.size() == 0) {
//      setErrorMessage(QString("No parent labels on second mesh"));
//      return false;
//    }
//
//    const vvGraph &S1 = mesh1->graph();
//    const vvGraph &S2 = mesh2->graph();
//
//    // Check the parents for cells which are not in the mesh1 or mesh2
//    IntIntAttr daughterParentMaptemp;
//    std::set<int> labelsS1;
//    std::set<int> labelsS2;
//    std::set<int> problemsS1;
//    std::set<int> problemsS2;
//    forall(vertex c, S1)
//      if(c->label > 0)
//        labelsS1.insert(c->label);
//    forall(vertex c, S2)
//      if(c->label > 0)
//        labelsS2.insert(c->label);
//    for(IntIntAttr::iterator it = daughterParentMap.begin(); it != daughterParentMap.end(); it++) {
//      int lParent = it->second;
//      int lDaughter = it->first;
//      if(labelsS1.count(lParent) == 0)
//        problemsS1.insert(lParent);
//      if(labelsS2.count(lDaughter) == 0)
//        problemsS2.insert(lDaughter);
//      if(labelsS1.count(lParent) != 0 and labelsS2.count(lDaughter) != 0)
//        daughterParentMaptemp[lDaughter] = lParent;
//    }
//    daughterParentMap = daughterParentMaptemp;
//
//    if(problemsS1.size() > 0 or problemsS2.size() > 0) {
//      Information::out << "problems with parents. Some labels in the map are not present in the mesh:\n"
//                       << "in mesh1, the following labels do not exist:\n";
//      forall(int l, problemsS1)
//        Information::out << l << ", ";
//      Information::out << "\nin mesh2, the following labels do not exist:\n";
//      forall(int l, problemsS2)
//        Information::out << l << ", ";
//      Information::out << endl;
//    }
//    return true;
//  }
//  REGISTER_PROCESS(CorrectParents);
//
//  // Make a heat map based on how many daughter cells a parent cell has
//  bool HeatMapProliferation::run(Mesh* mesh, Mesh* m2, bool onOtherMesh)
//  {
//    vvGraph& S = mesh->graph();
//
//    IntIntAttr parents = mesh->labelMap("Parents");
//    if(onOtherMesh) parents = m2->labelMap("Parents");
//    IntSet labels;
//
//    // get list of labels
//    forall(const vertex& v, S)
//      if(v->label > 0)
//        labels.insert(v->label);
//
//    IntFloatAttr &labelHeat = mesh->labelHeat();
//    labelHeat.clear();
//
//    // find how many daughters each parent has
//    forall(IntIntPair p, parents){
//      if(p.first > 0 and p.second > 0)
//        labelHeat[p.second] += 1;
//    }
//
//    mesh->setUseParents(!onOtherMesh);
//
//    // Set the map bounds.
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    // Unit of the heat map
//    mesh->heatMapUnit() = QString("#");
//    // Tell the system the triangles have changed (here, the heat map)
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(HeatMapProliferation);
//
//  // Select all the vertices on newly divided walls
//  bool MeshSelectNewWalls::run(Mesh* m)
//  {
//    auto parents = m->labelMap("Parents");
//    auto &S = m->graph();
//
//    for(const vertex &v : S) {
//      if(v->label != -1)
//        continue;
//      IntSet labelSet, parentSet;
//      forall(const vertex &n, S.neighbors(v)) { // Looks like neighors() is not compatible with C++11 for()
//        if(n->label > 0) {
//          labelSet.insert(n->label);
//          if(parents[n->label] > 0)
//            parentSet.insert(parents[n->label]);
//        }
//      }
//      if(labelSet.size() > 1 and parentSet.size() > 0 and labelSet.size() > parentSet.size())
//        v->selected = true;
//    }
//
//    return true;
//  }
//  REGISTER_PROCESS(MeshSelectNewWalls);
//
//  // Make a heat map based on how many daughter cells a parent cell has
//  bool HeatMapAreaAsymmetry::run(Mesh* mesh)
//  {
//    vvGraph& S = mesh->graph();
//
//    IntIntAttr parents = mesh->labelMap("Parents");
//    IntSet labels;
//
//    // get list of labels
//    forall(const vertex& v, S)
//      if(v->label > 0)
//        labels.insert(v->label);
//
//    IntFloatAttr labelCount;
//    IntFloatAttr labelMean;
//    IntFloatAttr labelMeanAccumulate;
//    IntFloatAttr labelSTDAccumulate;
//    IntFloatAttr labelSTD;
//    IntFloatAttr cellArea;
//    MeasureArea AreaProc(*this);
//    mesh->setUseParents(false);
//    if(!(AreaProc.run(mesh,cellArea)))
//        Information::out<<"Could not calculate cell area"<<endl;
//    mesh->setUseParents(true);
//    IntFloatAttr &labelHeat = mesh->labelHeat();
//    labelHeat.clear();
//
//    // find how daughters each parent has
//    forall(IntIntPair p, parents)
//      if(p.first > 0 and p.second > 0){
//        labelMeanAccumulate[p.second] += cellArea[p.first];
//        labelCount[p.second] += 1;
//        labelMean[p.second] = labelMeanAccumulate[p.second]/labelCount[p.second]; //Recalculated every time, could be done more efficiently
//    }
//
//    forall(IntIntPair p, parents)
//      if(p.first > 0 and p.second > 0 and labelCount[p.second]>1 ){
//        labelSTDAccumulate[p.second] += pow(labelMean[p.second] - cellArea[p.first],2);
//        labelSTD[p.second] = sqrt(labelSTDAccumulate[p.second]/labelCount[p.second]/labelMean[p.second]/labelMean[p.second]);
//        labelHeat[p.second] = labelSTD[p.second];
//        //std::cout<<labelSTDAccumulate[p.second]<<" : "<<labelMean[p.second]<<" : "<<labelCount[p.second]<<std::endl;
//        //std::cout<<cellArea[p.second]<<std::endl;
//        //std::cout<<labelHeat[p.second]<<std::endl;
//      }
//
//    //labelHeat = labelSTD;
//
//    // Set the map bounds.
//    mesh->heatMapBounds() = mesh->calcHeatMapBounds();
//    // Unit of the heat map
//    mesh->heatMapUnit() = QString("#");
//    // Tell the system the triangles have changed (here, the heat map)
//    mesh->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(HeatMapAreaAsymmetry);
//
//  // Copy parent labels overtop of labels
//  bool CopyParentsToLabels::run(Mesh* mesh)
//  {
//    vvGraph& S = mesh->graph();
//    IntIntAttr& parents = mesh->labelMap("Parents");
//    forall(const vertex& v, S)
//      if(v->label > 0) {
//        if(parents[v->label] > 0)
//          v->label = parents[v->label];
//        else
//          v->label = 0;
//      }
//
//    parents.clear();
//
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(CopyParentsToLabels);
//
//  // Copy labels to parents
//  bool CopyLabelsToParents::run(Mesh* mesh)
//  {
//    vvGraph& S = mesh->graph();
//    IntIntAttr& parents = mesh->labelMap("Parents");
//    parents.clear();
//    forall(const vertex& v, S)
//      if(v->label > 0)
//        parents[v->label] = v->label;
//
//    mesh->updateAll();
//    return true;
//  }
//  REGISTER_PROCESS(CopyLabelsToParents);
//
//
//
// struct CompareNeighSize
// {
//    bool operator()(const std::pair<int,std::set<int> > & left, const std::pair<int,std::set<int> > & right){
//      return left.second.size() < right.second.size();
//    }
//
// };
//
//
// bool CorrectLabeling::run(Mesh* mesh, QString numColorString, QString colormapSizeString, QString balanceDistributionString, bool mesh3D)
//  {
//    int num_color = numColorString.toInt();// 16; //PASS IN PARAMETER;
//    int colormap_size = colormapSizeString.toInt();
//    bool balance_distribution = stringToBool(balanceDistributionString);
//    std::cout<<num_color<<"  "<<colormap_size<<" "<<balance_distribution<<std::endl;
//    if(num_color>colormap_size){
//      std::cout<<"Number of colors is greater than color map size, decreased to match"<<std::endl;
//      num_color = colormap_size;
//    }
//    if(num_color<8){
//      std::cout<<"CorrectLabeling: number of colors is too small, increased to 7"<<std::endl;
//      num_color=8;
//    }
//
//    vvGraph& S = mesh->graph();
//    std::map<IntIntPair, double> neighPair;
//    std::map<int, std::set<int> > neighMap;
//    std::map<int, std::set<int> > neighColor;
//    std::map<int, int> reLabel;
//
//    if(mesh3D){
//      NhbdGraphInfo info;
//      neighborhoodGraph(S, 0.001, info);
//      neighPair=info.sharedWallArea;
//    } else {
//      neighborhood2D(S,neighPair);
//    }
//
//    //Store neighborhoods
//    std::map<IntIntPair, double>::iterator it = neighPair.begin();
//    while(it!= neighPair.end()){
//      IntIntPair cur_pair = it->first;
//      neighMap[cur_pair.first].insert(cur_pair.second);
//      it++;
//    }
//
//
//    //neighColor = neighMap;
//    //std::map<int, std::vector<int> >::iterator it = neighColor.begin();
//    //while(it!= neighColor.end()){
//    //  for(unsigned int i=0;i<it->second.size();i++)
//    //    it->second[i] = -1;
//    //}
//
//    std::vector<int> color_histo;
//    color_histo.resize(num_color);
//    for(int i=0;i<num_color;i++)
//      color_histo[i]=0;
//
//    int max_color = -1;
//    while(neighMap.size()){
//      std::pair<int, std::set<int> > min_deg = *min_element(neighMap.begin(),neighMap.end(),CompareNeighSize());
//      int cur_color = -1;
//      //Find minimum available color
//      if(!balance_distribution){
//        for(int i=0;i<num_color;i++)
//            if(!neighColor[min_deg.first].count(i)){
//              cur_color = i;
//              break;
//            }
//      }
//      else{
//            int cur_freq = 1e5;
//          for(int i=0;i<num_color;i++){
//              if(!neighColor[min_deg.first].count(i) && color_histo[i]<cur_freq){
//                cur_color = i;
//                cur_freq = color_histo[i];
//
//              }
//               //std::cout<<cur_freq<<":"<<color_histo[i]<<std::endl;
//          }
//      }
//      if(cur_color == -1){
//        setErrorMessage("Number of colors is too small, some adjacent cells may have the same color. Rerun with a larger number of colors.");
//        cur_color=0;
//      }
//      reLabel[min_deg.first] = cur_color+1+color_histo[cur_color]*colormap_size;
//      color_histo[cur_color]++;
//      if(cur_color>max_color)
//        max_color = cur_color;
//
//      //Erase from neighbors & add color to neighbors
//      std::set<int>::iterator it_neigh;
//      for(it_neigh = min_deg.second.begin();it_neigh != min_deg.second.end();it_neigh++){
//        int cur_index = *it_neigh;
//        neighColor[cur_index].insert(cur_color);
//        neighMap[cur_index].erase(min_deg.first);
//      }
//
//      neighMap.erase(min_deg.first);
//    }
//
//
//      //Relabel based on color and color-map size
//
//
//
////    forall(const IntIntPair& p, parentMap)
//
//
//    IntIntAttr& parents = mesh->labelMap("Parents");
//    parents.clear();
//    forall(const vertex& v, S)
//      if(v->label > 0)
//        parents[v->label] = reLabel[v->label];
//
//    mesh->updateAll();
//    std::cout<<"Relabeled with the folowing number of colors: "<<max_color+1<<std::endl;
//
//    return true;
//  }
//  REGISTER_PROCESS(CorrectLabeling);
//
//
//
//
//   bool CorrectLabelingStack::run(Store* store, QString numColorString, QString colormapSizeString, QString balanceDistributionString)
//    {
//      int num_color = numColorString.toInt();// 16; //PASS IN PARAMETER;
//      int colormap_size = colormapSizeString.toInt();
//      bool balance_distribution = stringToBool(balanceDistributionString);
//      std::cout<<num_color<<"  "<<colormap_size<<" "<<balance_distribution<<std::endl;
//      if(num_color>colormap_size){
//        std::cout<<"Number of colors is greater than color map size, decreased to match"<<std::endl;
//        num_color = colormap_size;
//      }
//      if(num_color<8){
//        std::cout<<"CorrectLabeling: number of colors is too small, increased to 7"<<std::endl;
//        num_color=8;
//      }
//
//      //vvGraph& S = mesh->graph();
//      std::map<IntIntPair, double> neighPair;
//      std::map<int, std::set<int> > neighMap;
//      std::map<int, std::set<int> > neighColor;
//      std::map<int, int> reLabel;
//
//      HVecUS& dataV = store->data();
//      const Stack* stack = store->stack();
//      Point3i imgSize(stack->size());
//
//      int xIdx = 0;
//      #pragma omp parallel for schedule(guided)
//      for(int z = 0; z < imgSize.z(); ++z)
//        for(int y = 0; y < imgSize.y(); ++y)
//          for(int x = 0; x < imgSize.x(); ++x) {
//            Point3i p(x,y,z);
//            int label = dataV[stack->offset(p)];
//            if(label == 0) continue;
//            std::set<int> n = findNeighborsLabel(store, imgSize, p);
//            forall(int l, n){
//
//              if(l != label and l != 0){
//                #pragma omp critical
//                {
//                  neighMap[label].insert(l);
//                }
//              }
//            }
//
//          }
//
//      // forall(auto p, neighMap){
//      //   std::cout << "l  " << p.first << " /// ";
//      //   forall(auto n, p.second){
//      //     std::cout << n << " / ";
//      //   }
//      //   std::cout << std::endl;
//      // }
//
//      std::vector<int> color_histo;
//      color_histo.resize(num_color);
//      for(int i=0;i<num_color;i++)
//        color_histo[i]=0;
//
//      int max_color = -1;
//      while(neighMap.size()){
//        std::pair<int, std::set<int> > min_deg = *min_element(neighMap.begin(),neighMap.end(),CompareNeighSize());
//        int cur_color = -1;
//        //Find minimum available color
//        if(!balance_distribution){
//          for(int i=0;i<num_color;i++)
//              if(!neighColor[min_deg.first].count(i)){
//                cur_color = i;
//                break;
//              }
//        }
//        else{
//              int cur_freq = 1e5;
//            for(int i=0;i<num_color;i++){
//                if(!neighColor[min_deg.first].count(i) && color_histo[i]<cur_freq){
//                  cur_color = i;
//                  cur_freq = color_histo[i];
//
//                }
//                 //std::cout<<cur_freq<<":"<<color_histo[i]<<std::endl;
//            }
//        }
//        if(cur_color == -1){
//          setErrorMessage("Number of colors is too small, some adjacent cells may have the same color. Rerun with a larger number of colors.");
//          cur_color=0;
//        }
//        reLabel[min_deg.first] = cur_color+1+color_histo[cur_color]*colormap_size;
//        color_histo[cur_color]++;
//        if(cur_color>max_color)
//          max_color = cur_color;
//
//        //Erase from neighbors & add color to neighbors
//        std::set<int>::iterator it_neigh;
//        for(it_neigh = min_deg.second.begin();it_neigh != min_deg.second.end();it_neigh++){
//          int cur_index = *it_neigh;
//          neighColor[cur_index].insert(cur_color);
//          neighMap[cur_index].erase(min_deg.first);
//        }
//
//        neighMap.erase(min_deg.first);
//      }
//
//
//      xIdx = 0;
//      for(int z = 0; z < imgSize.z(); ++z)
//        for(int y = 0; y < imgSize.y(); ++y)
//          for(int x = 0; x < imgSize.x(); ++x, ++xIdx) {
//            Point3i p(x,y,z);
//            dataV[stack->offset(p)] = reLabel[dataV[stack->offset(p)]];
//          }
//
//
//      std::cout<<"Relabeled with the folowing number of colors: "<<max_color+1<<std::endl;
//      store->changed();
//
//      return true;
//    }
//    REGISTER_PROCESS(CorrectLabelingStack);
//  bool readParentFile(QString fileName, std::unordered_map<int,int>& labelParentMap)
//  {
//    labelParentMap.clear();
//
//    QFile file(fileName);
//    if(!file.open(QIODevice::ReadOnly))
//      throw(QString("File '%1' cannot be opened for reading").arg(fileName));
//
//    QTextStream ss(&file);
//    QString line = ss.readLine();
//
//
//    QStringList fields = line.split(",");
//    if(fields.size() != 2)
//      throw(QString("File '%1' should be in the format 'label, parent'").arg(fileName));
//
//    int lineNum = 1;
//    while(ss.status() == QTextStream::Ok) {
//      ++lineNum;
//
//      // Get the line and split it
//      fields = ss.readLine().split(",");
//      if(fields.size() != 2)
//        break;
//
//      // Get the data
//      bool ok;
//      int label = fields[0].toInt(&ok);
//      if(!ok)
//        throw(QString("Error line %1: the first column is not an integer number but '%2'").arg(fileName).arg(lineNum));
//
//      int parent = fields[1].toInt(&ok);
//      if(!ok)
//        throw(QString("Error line %1: the second column is not an integer number but '%2'").arg(fileName).arg(lineNum));
//
//      labelParentMap[label] = parent;
//    }
//
//
//    return true;
//  }
//
//  bool saveParentFile(QString filename, std::unordered_map<int, int>& parentMap)
//  {
//    // Open output file
//    QFile file(filename);
//    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
//      throw(QString("File '%1' cannot be opened for writing").arg(filename));
//      return false;
//    }
//    QTextStream out(&file);
//    // Write header
//    out << "Label,Parent Label" << endl;
//
//    // Write data
//    forall(const IntIntPair& p, parentMap) {
//      int label = p.first;
//      int parent = p.second;
//      out << label << "," << parent << endl;
//    }
//    file.close();
//
//    return true;
//  }
//
//  bool MergeParentFiles::initialize(QWidget* parent)
//  {
//    QString suffix = ".csv";
//
//    QString fileName1(parm("File Open T1T0"));
//    if(parent)
//      fileName1 = QFileDialog::getOpenFileName(parent, "Choose parent label file to load", parm("File Open T1T0"),
//           "CSV files (*.csv)");
//
//    // check file
//    if(fileName1.isEmpty())
//      throw(QString("File name is empty"));
//
//    // check ending, add suffix if required
//    if(fileName1.right(suffix.size()) != suffix)
//      fileName1.append(suffix);
//
//    setParm("File Open T1T0", fileName1);
//
//    QString fileName2(parm("File Open T2T1"));
//    if(parent)
//      fileName2 = QFileDialog::getOpenFileName(parent, "Choose parent label file to load", parm("File Open T2T1"),
//           "CSV files (*.csv)");
//
//    // check file
//    if(fileName2.isEmpty())
//      throw(QString("File name is empty"));
//
//    // check ending, add suffix if required
//    if(fileName2.right(suffix.size()) != suffix)
//      fileName2.append(suffix);
//
//    setParm("File Open T2T1", fileName2);
//
//
//    QString filenameSave(parm("File Save T2T0"));
//    if(parent)
//      filenameSave = QFileDialog::getSaveFileName(parent, "Select file to save labels in", parm("File Save T2T0"), "CSV files (*.csv)");
//    // check filename
//    if(filenameSave.isEmpty())
//      return false;
//    // check ending, add suffix if required
//    if(filenameSave.right(suffix.size()) != suffix)
//      filenameSave.append(suffix);
//
//    setParm("File Save T2T0", filenameSave);
//
//    return true;
//  }
//
//  bool MergeParentFiles::run(QString fileName1, QString fileName2, QString fileNameSave)
//  {
//    std::unordered_map<int, int> labelParentT1T0, labelParentT2T1, labelParentT2T0;
//    readParentFile(fileName1, labelParentT1T0);
//    readParentFile(fileName2, labelParentT2T1);
//
//    forall(IntIntPair p, labelParentT2T1){
//      if(labelParentT1T0.find(p.second) == labelParentT1T0.end()) continue;
//      labelParentT2T0[p.first] = labelParentT1T0[p.second];
//    }
//
//    saveParentFile(fileNameSave, labelParentT2T0);
//
//    return true;
//  }
//  REGISTER_PROCESS(MergeParentFiles);
//
//
//  bool CreateParentAttr::run(Mesh* m, QString prefix, QString name)
//  {
//
//    AttrMap<int, int>& attrData = m->attributes().attrMap<int, int>(prefix + " " + name);
//    attrData.clear();
//
//    forall(auto p, m->labelMap("Parents")){
//      attrData[p.first] = p.second;
//    }
//
//    return true;
//  }
//  REGISTER_PROCESS(CreateParentAttr);
//
//  bool UniqueParentsFromAttr::run(Mesh* m, QString prefix, QString name1, QString name2, bool mult10)
//  {
//    AttrMap<int, int>& attrData1 = m->attributes().attrMap<int, int>(prefix + " " + name1);
//    if(attrData1.empty()) return setErrorMessage("Attr Map 1 doesn't exist.");
//
//    AttrMap<int, int>& attrData2 = m->attributes().attrMap<int, int>(prefix + " " + name2);
//    if(attrData2.empty()) return setErrorMessage("Attr Map 2 doesn't exist.");
//
//    int maxP1 = 0;
//    int maxP2 = 0;
//
//    std::set<int> setP1, setP2;
//    std::vector<int> vecP1, vecP2;
//    std::map<int, int> valueKeyP1, valueKeyP2;
//
//    forall(auto p, attrData1){
//      if(p.second > maxP1) maxP1 = p.second;
//      setP1.insert(p.second);
//    }
//    forall(auto p, attrData2){
//      if(p.second > maxP2) maxP2 = p.second;
//      setP2.insert(p.second);
//    }
//
//    forall(int l, setP1){
//      vecP1.push_back(l);
//      valueKeyP1[l] = vecP1.size();
//    }
//
//    forall(int l, setP2){
//      vecP2.push_back(l);
//      valueKeyP2[l] = vecP2.size();
//    }
//
//    std::set<int> allCells = findAllLabelsSet(m->graph());
//
//    forall(int l, allCells){
//      int p1 = attrData1[l];
//      int p2 = attrData2[l];
//      std::cout << "cell " << l << "/" << p1 << "/" << p2 << "/" << valueKeyP1[p1] << "/" << valueKeyP2[p2] 
//                << "/./" << vecP1.size() << "/./" << vecP2.size() << "/" << valueKeyP1[p1] + (vecP1.size()+1) * valueKeyP2[p2] << std::endl;
//      m->labelMap("Parents")[l] = valueKeyP1[p1] + (vecP1.size()+1) * valueKeyP2[p2];
//      if(mult10) m->labelMap("Parents")[l] = valueKeyP1[p1] + pow(10,((int)(log10(vecP1.size())+1))) * valueKeyP2[p2];
//      if(p1 == 0 and p2 == 0) m->labelMap("Parents")[l] = 0;
//    }
//
//    m->updateTriangles();
//
//    return true;
//  }
//  REGISTER_PROCESS(UniqueParentsFromAttr);
//
//
//
//  bool ImportParentAttr::run(Mesh* m, Mesh* m2, QString prefix, QString name, QString importMesh)
//  {
//    AttrMap<int, int>& attrData = m->attributes().attrMap<int, int>(prefix + " " + name);
//
//    if(attrData.empty()){
//      return setErrorMessage("Attribute Map empty!");
//    }
//
//    if(importMesh == "Active Mesh"){
//      m->labelMap("Parents").clear();
//      forall(auto p, attrData){
//        m->labelMap("Parents")[p.first] = p.second;
//      }
//      m->setUseParents(true);
//      m->updateTriangles();
//    } else if(importMesh == "Other Mesh (T2)"){
//      AttrMap<int,int> newParents;
//      forall(auto p, m2->labelMap("Parents")){
//        newParents[p.first] = m->labelMap("Parents")[p.second];
//      }
//      m2->labelMap("Parents") = newParents;
//      m2->setUseParents(true);
//      m2->updateTriangles();
//    } else {
//      m2->labelMap("Parents").clear();
//      forall(auto p, attrData){
//        m2->labelMap("Parents")[m->labelMap("Parents")[p.first]] = p.second;
//      }
//      m2->setUseParents(true);
//      m2->updateTriangles();
//    }
//    return true;
//  }
//  REGISTER_PROCESS(ImportParentAttr);
//
//  bool SelectParentLabel::run(Mesh *m, int labelToSelect, bool keepSelection)
//  {
//
//    forall(const vertex& v, m->graph()){
//      if(!keepSelection) v->selected = false;
//      if(m->labelMap("Parents")[v->label] == labelToSelect){
//        v->selected = true;
//      }
//    }
//
//    m->correctSelection(true);
//    m->updateSelection();
//
//    return true;
//
//  }
//  REGISTER_PROCESS(SelectParentLabel);
//
//  bool SelectParentLabelsT1::run(Mesh *m, Mesh *m2)
//  {
//
//    std::set<int> parentLabelsT2;
//
//    forall(auto p, m2->labelMap("Parents")){
//      parentLabelsT2.insert(p.second);
//    }
//
//    vvGraph& S = m->graph();
//
//    forall(const vertex& v, S){
//      v->selected = false;
//      if(v->label < 1) continue;
//      if(parentLabelsT2.find(v->label)==parentLabelsT2.end()) continue;
//      v->selected = true;
//    }
//
//    forall(const vertex& v, S){
//      if(v->selected) continue;
//      if(v->label > -1) continue;
//      forall(const vertex& n, S.neighbors(v)){
//        if(n->selected) v->selected = true;
//      }
//    }
//
//
//    m->updateSelection();
//
//    return true;
//
//  }
//  REGISTER_PROCESS(SelectParentLabelsT1);
//
//  REGISTER_PROCESS(SetCellType);
//  REGISTER_PROCESS(ResetCellTypes);
//  REGISTER_PROCESS(LoadCellTypes);
//  REGISTER_PROCESS(SaveCellTypes);
//  REGISTER_PROCESS(SelectCellType);
}
