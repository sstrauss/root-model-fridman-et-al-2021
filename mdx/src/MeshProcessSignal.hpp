//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef MESH_PROCESS_SIGNAL_HPP
#define MESH_PROCESS_SIGNAL_HPP

#include <Process.hpp>
#include <Curvature.hpp>
#include <MeshUtils.hpp>

class Ui_LoadHeatMap;

namespace mdx
{
  typedef AttrMap<CCIndex, double> CCIndexDoubleAttr;

  typedef std::map<int, HVec3U> IntHVec3UMap;
  
  ///\addtogroup MeshProcess
  ///@{
//  /**
//   * \class ViewProcess MeshProcessSignal.hpp <MeshProcessSignal.hpp>
//   *
//   * This process is used to change how the mesh is seen. Mostly useful to
//   * write scripts taking screenshots.
//   */
//  class mdxBase_EXPORT ViewMeshProcess : public Process 
//  {
//  public:
//    ViewMeshProcess(const Process& process) : Process(process) {}
//  
//    bool run(const QStringList &parms);
//  
//      setName("Mesh/System/View");
//    QString description() const { return 
//      "Modify how the current mesh is viewed. Useful for scripts.");
//    QStringList parmNames() const { return QStringList() 
//      << "Show Surface" << "Surface Type" << "Signal Type" << "Blend" << "Cull"
//      << "Show Mesh" << "Mesh View" << "Show Lines" << "Show Points" << "Show Map"
//      << "Scale" << "Transform" << "BBox" << "Brightness" << "Opacity";
//    }
//    QStringList parmDescs() const { return QStringList() 
//      << "Draw mesh as a continuous surface."
//      << "Nrml: show projected signal, Labels: color triangles according to assigned "
//         "labels, Parents: color triangles with parent labels, Heat: color triangles "
//         "with computed heat map (e.g. growth)"
//      << "Signal: show projected signal, Tex: Use 3D stack as texture, Img: Use 2D "
//         "texture (height map)."
//      << "Semi-transparent mesh, for example to superimpose to meshes or view the "
//         "stack through the mesh."
//      << "Color the triangles (with signal or labels) only on the top of the mesh."
//      << "Draw triangle edges and nodes"
//      << "All: draw all triangles. Border: draw outside edge of the mesh only. "
//         "Cells: draw cell outlines only. Selected: draw selected nodes only. "
//      << "Show connecting lines between nodes in the mesh."
//      << "Show mesh nodes."
//      << "Mapping of text on the labels (e.g. label number)"
//      << "Change scaling of mesh/stacks, independently in 3 directions (x,y,z). NB: "
//      << "a stack saved with 'Scale' turned on will have a modified voxel size, "
//         "while saved meshes are unaffected. "
//      << "Apply rotation and translation to the mesh/stack."
//      << "Display the bounding box (i.e. total size) of a stack."
//      << "Brightness of signal, labels or heat"
//      << "Opacity of signal, labels or heat";
//    }
//  
//    QStringList parmDefaults() const { return QStringList() 
//      << "" << "" << "" << "" << "" << "" << "" << "" << "" << ""
//      << "" << "" << "" << "-1" << "-1");
//  
//    ParmChoiceMap parmChoice() const
//    {
//      QStringList bools = QStringList() << "" << booleanChoice();
//      ParmChoiceMap map;
//      map[0] = bools;
//      map[1] = QStringList() << "" << "Normal" << "Heat" << "Label";
//      map[2] = QStringList() << "" << "Signal" << "Texture" << "Image";
//      map[3] = map[4] = map[5] = bools;
//      map[6] = QStringList() << "" << "All" << "Border" << "Cells" << "Selected";
//      map[7] = map[8] = map[9] = map[10] = map[11] = map[12] = bools;
//      return map;
//    }
//      setIcon(QIcon(":/images/Palette.png"));
//  };
  
  /**
   * \class MeshProjectSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Project stack signal onto the current mesh.
   */
  class mdxBase_EXPORT MeshProjectSignal : public Process 
  {
  public:
    MeshProjectSignal(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Project Signal");
      setDesc("Project signal onto mesh, normal to its curved surface.");
      setIcon(QIcon(":/images/ProjectColor.png"));

      addParm("Min Dist", "Min distance for signal projection in microns.", "2.0");
      addParm("Max Dist", "Max distance for signal projection in microns.", "6.0");
      addParm("Use Absolute", "Use absolute values of signal, instead of normalizing it,"
        "often used for signal quantification of multiple samples.", "No", booleanChoice());
      addParm("Min Signal", "Lower bound of signal value if 'Use absolute' is chosen", "0.0");
      addParm("Max Signal", "Upper bound of projected signal value.", "60000.0");
      addParm("Signal", "Name of the signal to project", "Signal");
    }

    bool run()
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = "Signal";
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());
  
      CCStructure &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      bool result = run(currentStack()->currentStore(), cs, indexAttr, signalAttr,
                                   parm("Min Dist").toDouble(), parm("Max Dist").toDouble());
  
      bool useAbsSignal = stringToBool(parm("Use Absolute")); 
      double absSignalMin = parm("Min Signal").toDouble();
      double absSignalMax = parm("Max Signal").toDouble();
      if(useAbsSignal)
        mesh->setSignalBounds(Point2d(absSignalMin, absSignalMax), signalName);
      else
        mesh->setSignalBounds(calcBounds(signalAttr), signalName);
      mesh->setSignalUnit("AUI");
  
      setStatus(QString("Signal projection, min: %1 max: %2")
                      .arg(mesh->signalBounds(signalName).x()).arg(mesh->signalBounds(signalName).y()));
      return result;
    }
    bool run(const Store* store, const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, 
                                                                                          double minDist, double maxDist);

    // Since we are creating a signal, make it visible
    bool finalize(QWidget *parent)
    {
      if(parent and mesh and !signalName.isEmpty()) {
        mesh->updateProperties(ccName);
        mesh->drawParms(ccName).setGroupVisible("Faces", true);
        mesh->drawParms(ccName).setRenderChoice("Faces", signalName);
        mesh->setSignal(signalName);
      }
      return true;
    }

  private:
    Mesh *mesh = 0;
    QString ccName, signalName;
  };
  
  /**
   * \class MeshSmoothSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Smooth the mesh signal using local averaging.
   */
  class mdxBase_EXPORT MeshSmoothSignal : public Process 
  {
  public:
    MeshSmoothSignal(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Smooth Mesh Signal");
      setDesc("Averages the signal of each node, based on its immediate neighbors.");
      setIcon(QIcon(":/images/SmoothColor.png"));

      addParm("Passes", "Number of smoothing iterations.", "3");
      addParm("Signal", "Name of the signal to smooth, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());
  
      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);
  
      return run(cs, indexAttr, signalAttr, parm("Passes").toUInt());
    }
    bool run(CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, uint passes);
  };

  /**
   * \class BrightenMesh MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Change the brightness of the signal.
   */
  class mdxBase_EXPORT MeshBrightness : public Process {
  public:
    MeshBrightness(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Mesh Brighness");
      setDesc("Changes the brightness of the signal on a mesh.");
      setIcon(QIcon(":/images/Brightness.png"));

      addParm("Amount", "Amount to multiply the signal, >1 is brighter", "2.0");
      addParm("Signal", "Name of the signal to brighten, empty for current", "");
      
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name() );

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name() );

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name() );

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, signalAttr, parm("Amount").toFloat());
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double amount);
  }; 

  /**
   * \class MeshRescaleSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Rescale the mesh signal
   */ 
  class mdxBase_EXPORT MeshRescaleSignal : public Process 
  {
  public:
    MeshRescaleSignal(const Process& process) : Process(process)
    {
      setName("Mesh/Signal/Rescale Signal");
      setDesc("Change the colorbar of the signal.\n"
              "If percentile is set to 0, it uses the minimum and maximum arguments.\n"
              "Only the visualization is affected, the signal projection remains unchanged");
      setIcon(QIcon(":/images/Scale.png"));

      addParm("Zero Ref", "If true, 0 will be used as a reference.\n"
              "If the signal is all positive (resp. negative), 0 will be added "
              "as a minimum (resp. maximum).\n"
              "If the signal is both positive and negative, 0 will be place "
              "at the center of the range", "No", booleanChoice());
      addParm("Percentile", "Keep only this percentage of the signal to compute the range.", "95.0");
      addParm("Minimum", "If the percentile specified is 0, uses this as the minimum value for the range", "0.0");
      addParm("Maximum", "If the percentile specified is 0, uses this as the maximum value for the range", "1.0");
      addParm("Signal", "Name of the signal to rescale, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);
      Point2d signalBounds;

      bool result = run(cs, indexAttr, signalAttr, stringToBool(parm("Zero Ref")), parm("Percentile").toDouble(), 
          parm("Minimum").toDouble(), parm("Maximum").toDouble(), signalBounds);
      if(result)
        mesh->setSignalBounds(signalBounds, signalName);
      return result;
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, 
         bool zeroRef, double percentile, double minimum, double maximum, Point2d &signalBounds);
  };

  /**
   * \class MeshSetSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Set the mesh signal to a defined value.
   */
  class mdxBase_EXPORT MeshSetSignal : public Process 
  {
  public:
    MeshSetSignal(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Set Mesh Signal");
      setDesc("Set the mesh signal");
      setIcon(QIcon(":/images/ClearSignal.png"));

      addParm("Value", "Set (or clear) the mesh to a specific value.", "50000");
      addParm("Nan", "Fill with Nan", "No", booleanChoice());
      addParm("Signal", "Name of the signal to brighten, empty for current", "");
    }
  
    bool run()
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      double signal = parm("Signal").toDouble();
      if(stringToBool(parm("Nan")))
        signal = std::numeric_limits<double>::quiet_NaN();

      auto &cs = mesh->ccStructure(ccName);
      auto &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      return run(cs, indexAttr, signalAttr, signal);
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double signal);

    // Since we are creating a signal, make it visible
    bool finalize(QWidget *parent)
    {
      if(parent and mesh and !signalName.isEmpty()) {
        mesh->updateProperties(ccName);
        mesh->drawParms(ccName).setGroupVisible("Faces", true);
        mesh->drawParms(ccName).setRenderChoice("Faces", signalName);
        mesh->setSignal(signalName);
      }
      return true;
    }

  private:
    Mesh *mesh = 0;
    QString ccName, signalName;
  };

  class mdxBase_EXPORT MeshProjectCurvatureOp 
  {
  public:
    MeshProjectCurvatureOp(const CCStructure &cs, const CCIndexDataAttr &indexAttr, 
        const CCIndexVec &vertices, double radius, bool selectNhbd, Matrix2x3dVec &eVecs, Point2dVec &eVals);
    void operator()(int i);
  
  private:
    const CCStructure &cs;
    const CCIndexDataAttr &indexAttr;
    const CCIndexVec &vertices;
    double radius;
    bool selectNhbd;
    Matrix2x3dVec &eVecs;
    Point2dVec &eVals;
  };

  /**
   * \class MeshProjectCurvature MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Set the mesh signal to be the curvature of the mesh. The process can also
   * output a CSV file with the full curvature tensor for each vertex.
   */
  class mdxBase_EXPORT MeshProjectCurvature : public Process 
  {
  public:
    MeshProjectCurvature(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Project Mesh Curvature");
      setDesc("Compute curvature at each node of the mesh, for a given neighborhood size. "
              "Curvature values are stored as signal.\n"
              "Implemented from Goldfeather and Interrante (2004)");
      setIcon(QIcon(":/images/Curvature.png"));

      addParm("Type", "Minimal = minCurv, Maximal = maxCurv, Gaussian = maxCurv * minCurv, "
         "SumSquare = maxCurv^2 + minCurv^2, Average = (maxCurv + minCurv)/2, "
         "SignedAverageAbs = sign(max or min) x (abs(maxCurv) + abs(minCurv))/2", "Gaussian",
          QStringList() << "Minimal" << "Maximal" << "Gaussian" << "SumSquare" << "Average" << "SignedAverageAbs");
      addParm("Neighborhood", "Neighborhood in microns", "5.0");
      addParm("Auto Scale", "Clip max and min signal range according to curvature distribution", "Yes", booleanChoice());
      addParm("Min Curv", "Minimal curvature value displayed", "-50.0");
      addParm("Max Curv", "Maximal curvature value displayed", "50.0");
      addParm("Percentile", "Auto-scale signal range based on curvature percentile", "85.0");
      addParm("Output", "Name of output file, if desired.", "");
      addParm("Signal", "Name of the signal to project curvature", "");
      addParm("Select Nhbd", "Only use selected vertices in neighborhood", "No");
      addParm("Do Axis", "Create axis lines", "Yes");
      addParm("Update Normals", "Update the normals before running", "Yes");
    }
  
    bool run()
    {
      mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      const QString &type = parm("Type");
      signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = QString("%1 Curvature").arg(type);
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      const QString &output = parm("Output");
      double neighborhood = parm("Neighborhood").toFloat();
      bool autoScale = stringToBool(parm("Auto Scale"));
      double mincurv = parm("Min Curv").toFloat();
      double maxcurv = parm("Max Curv").toFloat();
      double percentile = parm("Percentile").toFloat();
      bool selectNhbd = stringToBool(parm("Select Nhbd"));
      bool updateNormals = stringToBool(parm("Update Normals"));

      // Get the name for the curvature attribute map
      bool setColorMap = !mesh->signalExists(signalName);

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      CCIndexSymTensorAttr *curvatureAttr = 0;
      bool doAxis = stringToBool(parm("Do Axis"));
      if(doAxis)
        curvatureAttr = &mesh->axisAttr(ccName, signalName);
      bool result = run(cs, indexAttr, curvatureAttr, signalAttr, output, type, neighborhood, selectNhbd, updateNormals);

      if(result) {
        // Copy signal to faces
        faceAttrFromVertices(cs, signalAttr, cs.faces());

        if(setColorMap)
          mesh->signalColorMap(signalName).setColors("Uniform Jet");

        if(type == "Gaussian" or type == "SumSquare")
          mesh->setSignalUnit(UM_2);
        else if(type == "Anisotropy")
          mesh->setSignalUnit("");
        else
          mesh->setSignalUnit(UM_1);
     
        if(autoScale) {
          Point2d signalBounds;
          MeshRescaleSignal mrs(*this);
          mrs.run(cs, indexAttr, signalAttr, 0, percentile, mincurv, maxcurv, signalBounds);
          mesh->setSignalBounds(signalBounds, signalName);
        } else
          mesh->setSignalBounds(calcBounds(signalAttr), signalName);

      }
      return result;
    }
    bool run(const CCStructure &cs, CCIndexDataAttr &indexAttr, CCIndexSymTensorAttr *curvatureAttr, CCIndexDoubleAttr &signalAttr, 
                            const QString &outputFle, const QString &type, double neighborhood, bool selectNhbd, bool updateNormals);

    // Since we are creating a signal, make it visible
    bool finalize(QWidget *parent)
    {
      if(parent and mesh and !signalName.isEmpty()) {
        mesh->updateProperties(ccName);
        mesh->drawParms(ccName).setGroupVisible("Faces", true);
        mesh->drawParms(ccName).setRenderChoice("Faces", signalName);
        mesh->setSignal(signalName);
      }
      return true;
    }

  private:
    Mesh *mesh = 0;
    QString ccName, signalName;
  };

  class mdxBase_EXPORT MeshSignalGradOp 
  {
  public:
    MeshSignalGradOp(const CCStructure &cs, const CCIndexDataAttr &indexAttr, 
                                                const CCIndexDoubleAttr &signalAttr, const CCIndexVec &vertices, CCIndexDoubleAttr &result);
    void operator()(int i);
  
  private:
    const CCStructure &cs;
    const CCIndexDataAttr &indexAttr;
    const CCIndexDoubleAttr &signalAttr;
    const CCIndexVec &vertices;
    double radius;
    CCIndexDoubleAttr& result;
  };

  /**
   * \class MeshSignalGrad ProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Smooth the mesh signal using a gaussian kernel.
   */
  class mdxBase_EXPORT MeshSignalGrad : public Process 
  {
  public:
    MeshSignalGrad(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Signal Gradient");
      setDesc("Gradient of mesh signal");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Signal", "Name of the signal to calculate signal gradient, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, signalAttr);
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr);
  };

  class mdxBase_EXPORT MeshGaussianBlurOp 
  {
  public:
    MeshGaussianBlurOp(const CCStructure &cs, const CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr,
                          const CCIndexVec &vertices, double radius, CCIndexDoubleAttr &result);
    void operator()(int i);
  
  private:
    const CCStructure &cs;
    const CCIndexDataAttr &indexAttr;
    const CCIndexDoubleAttr &signalAttr;
    const CCIndexVec &vertices;
    double radius;
    CCIndexDoubleAttr& result;
  };

  /**
   * \class MeshGaussianBlur ProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Smooth the mesh signal using a gaussian kernel.
   */
  class mdxBase_EXPORT MeshGaussianBlur : public Process 
  {
  public:
    MeshGaussianBlur(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Gaussian Blur");
      setDesc("Apply Gaussian Blur to mesh signal");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Radius", "Size of neighborhood in microns used for Gaussian blur.\n"
              "The blur function standard deviation is given by sigma = radius/2.", "2.0");
      addParm("Signal", "Name of the signal to Gaussian blur, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      mesh->updateProperties(ccName);
      auto &signalAttr = mesh->signalAttr<double>(signalName);

      faceAttrToVertices(*mesh, cs, cs.vertices());
      

      return run(cs, indexAttr, signalAttr, parm("Radius").toFloat());
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius);
  };
  
  /**
   * \class MeshDiffGaussians ProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Calculate a difference of Gaussians on mesh.
   */
  class mdxBase_EXPORT MeshDiffGaussians : public Process 
  {
  public:
    MeshDiffGaussians(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Difference of Gaussians");
      setDesc("Calculate a difference of Gaussians for the mesh signal");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Radius 1", "Size of neighborhood in microns used for first Gaussian blur.", "1.0");
      addParm("Radius 2", "Size of neighborhood in microns used for second Gaussian blur.", "5.0");
      addParm("Signal", "Name of the signal to calculate difference of Gaussians, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      bool result = run(cs, indexAttr, signalAttr, parm("Radius 1").toFloat(), parm("Radius 2").toFloat());
      mesh->setSignalBounds(calcBounds(signalAttr));
      return result;
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius1, double radius2);
  };
  
  class mdxBase_EXPORT MeshLocalMinimaOp 
  {
  public:
    MeshLocalMinimaOp(const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, 
                                             const CCIndexVec &vertices, double radius, int startLabel);
    void operator()(int i);
  
  private:
    const CCStructure &cs;
    CCIndexDataAttr &indexAttr;
    const CCIndexDoubleAttr &signalAttr;
    const CCIndexVec &vertices;
    double radius;
    int label;
  };
   
  /**
   * \class MeshLocalMinima ProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Find the local minima in the current mesh.
   */
  class mdxBase_EXPORT MeshLocalMinima : public Process 
  {
  public:
    MeshLocalMinima(const Process& process) : Process(process) 
    {
      setName("Mesh/Segmentation/Auto Seeding");
      setDesc("Put a seed at local minima of mesh signal.");
      setIcon(QIcon(":/images/LocalMinima.png"));

      addParm("Radius", "Size of neighborhood in microns used for local minima.", "3.0");
      addParm("Signal", "Name of the signal to find local minima, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      return run(*mesh, cs, indexAttr, signalAttr, parm("Radius").toFloat());
    }
    bool run(Mesh &mesh, const CCStructure &cs, CCIndexDataAttr &indexAttr, const CCIndexDoubleAttr &signalAttr, double radius);
  };
   
  class mdxBase_EXPORT MeshNormalizeOp 
  {
  public:
    MeshNormalizeOp(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr,
                                      const CCIndexVec &vertices, double radius, CCIndexDoubleAttr &result);
    void operator()(int i);
  
  private:
    const CCStructure &cs;
    const CCIndexDataAttr &indexAttr;
    CCIndexDoubleAttr &signalAttr;
    const CCIndexVec &vertices;
    double radius;
    CCIndexDoubleAttr& result;
  }; 

  /**
   * \class MeshNormalize ProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Local normalization of the mesh signal.
   */
  class mdxBase_EXPORT MeshNormalize : public Process 
  {
  public:
    MeshNormalize(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Normalize Signal");
      setDesc("Normalize mesh signal locally.");
      setIcon(QIcon(":/images/Normalize.png"));

      addParm("Radius", "Size of neighborhood in microns used for normalization", "5.0");
      addParm("Signal", "Name of the signal to normalize, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      bool result = run(cs, indexAttr, signalAttr, parm("Radius").toFloat());
      mesh->setSignalBounds(calcBounds(signalAttr));
      return result;
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius);
  };

  class MeshDilationOp 
  {
  public:
    MeshDilationOp(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr,  
                   const CCIndexVec &vertices, double radius, CCIndexDoubleAttr &result);
    void operator()(int vi);
  
  private:
    const CCStructure &cs;
    const CCIndexDataAttr &indexAttr;
    CCIndexDoubleAttr &signalAttr;
    const CCIndexVec &vertices;
    double radius;
    CCIndexDoubleAttr& result;
  }; 
  /**
   * \class MeshDilation MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Morphological dilation on the mesh, using a spherical kernel. This process
   * replace the signal intensity on each vertex with the maximum within a
   * given distance.
   */
  class MeshDilation : public Process 
  {
  public:
    MeshDilation(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Dilate Signal");
      setDesc("Morphological dilation of the signal on the mesh.");
      setIcon(QIcon(":/images/Dilate.png"));

      addParm("Radius", "Size of neighborhood in microns", "1.0");
      addParm("Signal", "Name of the signal to dilate, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, signalAttr, parm("Radius").toDouble());
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius);
  };
 
  class MeshErosionOp 
  {
  public:
    MeshErosionOp(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr,  
                  const CCIndexVec &vertices, double radius, CCIndexDoubleAttr &result);
    void operator()(int vi);
  
  private:
    const CCStructure &cs;
    const CCIndexDataAttr &indexAttr;
    CCIndexDoubleAttr &signalAttr;
    const CCIndexVec &vertices;
    double radius;
    CCIndexDoubleAttr& result;
  };  

  /**
   * \class MeshErosion MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Morphological erosion on the mesh, using a spherical kernel. This process
   * replace the signal intensity on each vertex with the minimum within a
   * given distance.
   */
  class MeshErosion : public Process 
  {
  public:
    MeshErosion(const Process& process) : Process(process)
    {
      setName("Mesh/Signal/Erode Signal");
      setDesc("Morphological erosion of the signal on the mesh.");
      setIcon(QIcon(":/images/Erode.png"));

      addParm("Radius", "Size of neighborhood in microns", "1.0");
      addParm("Signal", "Name of the signal to erode, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, signalAttr, parm("Radius").toDouble());
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius);
  };
  
  /**
   * \class MeshClosing MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Morphological closing on the mesh. This process is equivalent to call the
   * MeshDilation followed by MeshErosion with the same size.
   */
  class MeshClosing : public Process 
  {
  public:
    MeshClosing(const Process& process) : Process(process)
    {
      setName("Mesh/Signal/Close Signal");
      setDesc("Morphological closing of the signal on the mesh.");
      setIcon(QIcon(":/images/Closing.png"));

      addParm("Radius", "Size of neighborhood in microns", "1.0");
      addParm("Signal", "Name of the signal to close, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, signalAttr, parm("Radius").toDouble());
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius);
  };
  
  /**
   * \class MeshOpening MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Morphological opening on the mesh. This process is equivalent to call the
   * MeshErosion followed by MeshDilation with the same size.
   */
  class MeshOpening : public Process 
  {
  public:
    MeshOpening(const Process& process) : Process(process)
    {
      setName("Mesh/Signal/Open Signal");
      setDesc("Morphological opening of the signal on the mesh.");
      setIcon(QIcon(":/images/Opening.png"));

      addParm("Radius", "Size of neighborhood in microns", "1.0");
      addParm("Signal", "Name of the signal to open, empty for current", "");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      mesh->updateProperties(ccName);

      return run(cs, indexAttr, signalAttr, parm("Radius").toDouble());
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, double radius);  
  };

  /**
   * \class MeshCombineSignals MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Combine the signals on a mesh.
   */
  class mdxBase_EXPORT MeshCombineSignals : public Process 
  {
  public:
    MeshCombineSignals(const Process& process) : Process(process)
    {
      setName("Mesh/Signal/Combine Signals");
      setDesc("Combine Signals");
      setIcon(QIcon(":/images/Scale.png"));

      addParm("Signal 1", "Signal 1", "");
      addParm("Signal 2", "Signal 2", "");
      addParm("Operation", "Operation", "Addition", QStringList() << "Addition" << "Multiplication" << "Ratio");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal 1");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      QString signalName2 = parm("Signal 2");
      if(signalName2.isEmpty())
        signalName2 = mesh->signal();
      if(signalName2.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      auto &signalAttr2 = mesh->signalAttr<double>(signalName2);

      // RSS Should this be the norm for all filters? ie. create a new signal rather than replacing the old? 
      QString signalResName = signalName + "-" + parm("Operation") + "-" + signalName2;
      auto &signalResAttr = mesh->signalAttr<double>(signalResName);

      mesh->updateProperties(ccName);

      bool result = run(cs, indexAttr, signalAttr, signalAttr2, signalResAttr);
      if(result) {
        mesh->setSignalBounds(calcBounds(signalResAttr), signalResName); // How to update units?
        mesh->setSignal(signalResName);
      }
      return result;
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, CCIndexDoubleAttr &signalAttr2, CCIndexDoubleAttr &signalResAttr);
  };

  /**
   * \class MeshTransformSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Transform the signal on a mesh.
   */
  class mdxBase_EXPORT MeshTransformSignal : public Process 
  {
  public:
    MeshTransformSignal(const Process& process) : Process(process)
    {
      setName("Mesh/Signal/Transform Signal");
      setDesc("Transform the mesh signal");
      setIcon(QIcon(":/images/Invert.png"));

      addParm("Signal", "Signal", "");
      addParm("Operation", "Operation", "Invert", QStringList() << "Invert" << "Inverse");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No mesh selected").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No cell complex").arg(name());

      QString signalName = parm("Signal");
      if(signalName.isEmpty())
        signalName = mesh->signal();
      if(signalName.isEmpty())
        throw QString("%1::run No signal name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();
      auto &signalAttr = mesh->signalAttr<double>(signalName);
      QString signalResName = signalName + "-" + parm("Operation");
      auto &signalResAttr = mesh->signalAttr<double>(signalResName);

      mesh->updateProperties(ccName);

      bool result = run(cs, indexAttr, signalAttr, signalResAttr);
      if(result) {
        mesh->setSignalBounds(calcBounds(signalResAttr), signalResName); // How to update units?
        mesh->setSignal(signalResName);
      }
      
      return result;
    }
    bool run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexDoubleAttr &signalAttr, CCIndexDoubleAttr &signalResAttr);
  };

  /**
   * \class MeshTrimSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Trim the signal on a mesh, erasing values outside lower and upper thresholds.
   */
  class MeshTrimSignal : public Process
  {
  public:
    MeshTrimSignal(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Trim Signal");
      setDesc("Remove out of range signal values.");
      setIcon(QIcon(":/images/MakeHeatMap.png"));

      addParm("Lower Threshold", "Lower threshold of signal to trim", "0.0");
      addParm("Upper Threshold", "Upper threshold of signal to trim", "1.0");
    }
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh)
        throw QString("%1::run No current mesh").arg(name());
      return run(*mesh, mesh->signal(), parm("Lower Threshold").toDouble(), parm("Upper Threshold").toDouble());
    }
    bool run(Mesh &mesh, const QString &signalName, double lowerThreshold, double upperThreshold);
  };

  /**
   * \class MeshEraseSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Erase the signal in selected protions of a mesh
   */
  class MeshEraseSignal : public Process
  {
  public:
    MeshEraseSignal(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Erase Signal");
      setDesc("Erase signal from selection.");
      setIcon(QIcon(":/images/EraseLabel.png"));
    }
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());
      return run(*mesh, mesh->signal());
    }
    bool run(Mesh &mesh, const QString &signalName);
  };

  /**
   * \class MeshRenameSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Rename a mesh signal attribute
   */
  class MeshRenameSignal : public Process
  {
  public:
    MeshRenameSignal(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Rename Signal");
      setDesc("Delete the entire signal attribute.");
      setIcon(QIcon(":/images/Process.png"));

      addParm("Signal Name", "Signal name, blank for current", "");
      addParm("Mesh Id", "Mesh, blank for current", "");
    }
//    bool initalize(QWidget *parent)
//    {
//      if(
//    }
    bool run()
    {
      Mesh *mesh = 0;
      if(parm("Mesh").isEmpty())
        mesh = currentMesh();
      else
        mesh = getMesh(parm("Mesh Id").toInt());

      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());
      return run(*mesh, mesh->signal());
    }
    bool run(Mesh &mesh, const QString &signalName);
  };

  /**
   * \class MeshDeleteSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
   *
   * Delete an entire mesh signal attribute
   */
  class MeshDeleteSignal : public Process
  {
  public:
    MeshDeleteSignal(const Process& process) : Process(process) 
    {
      setName("Mesh/Signal/Delete Signal");
      setDesc("Delete the entire signal attribute.");
      setIcon(QIcon(":/images/DeleteLabel.png"));

      addParm("Signal Name", "Signal name, blank for current", "");
      addParm("Mesh Id", "Mesh, blank for current", "");
    }
    bool run()
    {
      Mesh *mesh = 0;
      if(parm("Mesh").isEmpty())
        mesh = currentMesh();
      else
        mesh = getMesh(parm("Mesh Id").toInt());

      if(!mesh) 
        throw QString("%1::run No current mesh").arg(name());
      return run(*mesh, mesh->signal());
    }
    bool run(Mesh &mesh, const QString &signalName);
  };
  ///@}
} 

#endif
