# Linux Makefile for MorphoDynamX

# Directories
MDX_BASE = $(shell mdx --dir)
MDX_INCLUDE = $(shell mdx --include)
MDX_P_INCLUDE = $(shell mdx --process)/include
MDX_PROCESS = $(shell mdx --user-process)
# Flags for compilation
INCLUDES = ${QT_INCLUDE} \
  -I$(MDX_INCLUDE) -I$(MDX_P_INCLUDE) -I/usr/local/cuda/include \
  -I/usr/X11R6/include -I/usr/include/eigen3 -I.
DEFINES = ${QT_DEFINES} -Dcimg_display=0 -DQT_NO_DEBUG -DQT_MESSAGELOGCONTEXT
CFLAGS = -m64 -pipe -O3 -D_REENTRANT -Wall -W -fPIC $(DEFINES)
CXXFLAGS = -m64 -pipe -Wno-unused-local-typedefs -Wno-system-headers -fopenmp -O3 \
  -D_REENTRANT -Wall -W -fPIC -std=c++14 $(DEFINES)
LD_FLAGS = -m64 -Wl,-O1 -shared
LIBS = -L/usr/lib/x86_64-linux-gnu -L/usr/X11R6/lib64 -fopenmp -L/usr/local/lib \
  ${QT_LIBS} -lmdx -lGL -lpthread

#LD_SO_FLAGS?=  -pipe -mfpmath=sse -msse3 -O3 -rdynamic 
#LD_EXE_FLAGS?=  -pipe -mfpmath=sse -msse3 -O3 -rdynamic 

# Command definitions
CXX = g++
CC = gcc
RM = rm -f
CP = cp -f

# Qt
export QT_SELECT = qt5
MOC = moc
RCC = rcc
UIC = uic

# Compile rules
%.so:
	$(CXX) -o $@ $^ -shared -Wl,-soname=$@ $(LD_FLAGS) $(LIBS) 

%.o: %.cpp
	$(CXX) -c -o $@ $< -fpic $(CXXFLAGS) $(INCLUDES)

%.moc: %.hpp
	$(MOC) -o $@ $<

%.moc: %.cpp $(wildcard %.hpp)
	$(MOC) -o $@ $<

qrc_%.cpp: %.qrc
	$(RCC) -name $(basename $<) -o $@ $<

ui_%.h: %.ui
	$(UIC) -o $@ $<

clean:
	$(RM) *.o *.moc qrc_*.cpp ui_*.h

install:
	$(CP) *.so $(MDX_PROCESS)
