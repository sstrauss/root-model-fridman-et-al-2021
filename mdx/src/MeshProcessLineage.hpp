//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2021 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef MESH_PROCESS_LINEAGE_HPP
#define MESH_PROCESS_LINEAGE_HPP

#include <Process.hpp>

namespace mdx
{
  ///\addtogroup MeshProcess
  ///@{
  /**
   * \class SaveParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
   *
   * Save mapping from labels to parents
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT SaveParents : public Process
  {
  public:
    SaveParents(const Process& process) : Process(process) 
    {
      setName("Mesh/Lineage Tracking/Save Parents");
      setDesc("Save map of labels to parents labels to a file");
      setIcon(QIcon(":/images/ParentsSave.png"));

      addParm("File Name", "Name of spreadsheet file.", ""); 
      addParm("Existing Only", "Save only existing Labels", "Yes", booleanChoice());
      addParm("Labeling", "Labeling", "Parents");
      addParm("Dimension", "Dimension", "Face", QStringList() << "Vertex" << "Face" << "Volume");
    }
    bool initialize(QWidget* parent);
    bool run();
    bool run(Mesh &mesh, const QString& filename, bool saveOnlyExisting);
  };
  
  /**
   * \class LoadParents ProcessHeatMap.hpp <MeshProcessHeatMap.hpp>
   *
   * Load mapping from each label to a label in a different mesh (parent).
   */
  class mdxBase_EXPORT LoadParents : public Process
  {
  public:
    LoadParents(const Process& process) : Process(process) 
    {
      setName("Mesh/Lineage Tracking/Load Parents");
      setDesc("Load map of labels to parents from a file");
      setIcon(QIcon(":/images/Parents.png"));

      addParm("File Name", "Path to label parents file.", "");
      addParm("File Type","File type, CSV(label, parent) or TXT(parent: label, label ...)", "CSV");
      addParm("Keep","Keep existing parents", "No", booleanChoice());
      addParm("Labeling","Labeling to load, empty for current", "");
    }
    bool initialize(QWidget* parent);

    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!currentMesh())
        throw QString("%1::run No current mesh").arg(name());
  
      return run(*mesh, parm("File Name"), parm("File Type"), parm("Labeling"), stringToBool(parm("Keep")));
    } 
    bool run(Mesh &mesh, const QString& filename, const QString &fileType, QString labeling, bool keep);
  };
  
  /**
   * \class ResetParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
   *
   * Clear label to parent mapping
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT ResetParents : public Process
  {
  public:
    ResetParents(const Process& process) : Process(process) 
    {
      setName("Mesh/Lineage Tracking/Reset Parents");
      setDesc("Clear mapping from parents to labels");
      setIcon(QIcon(":/images/ParentsClear.png"));

      addParm("Labeling","Labeling to reset", "Parents");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!currentMesh())
        throw(QString("ResetParents:run No current mesh"));

      return run(*mesh, parm("Labeling"));
    }
    bool run(Mesh &mesh, const QString &labeling);
  };

  /**
   * \class SetParent MeshProcessLineage.hpp <MeshProcessLineage.hpp>
   *
   * Set label to parent mapping
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT SetParent : public Process
  {
  public:
    SetParent(const Process& process) : Process(process) 
    {
      setName("Mesh/Lineage Tracking/Set Parent");
      setDesc("Set parent labels");
      setIcon(QIcon(":/images/Parents.png"));

      addParm("Labeling","Labeling to set", "Parents");
      addParm("Parent Label","Labeling to set", "0");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!currentMesh())
        throw QString("%1::run No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No CC Name").arg(name());

      QString labeling = parm("Labeling");
      if(labeling.isEmpty())
        throw QString("%1::run No labeling").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      if(mesh->labeling() == labeling)
        mesh->updateProperties();

      return run(*mesh, cs, indexAttr, labeling, parm("Parent Label").toInt());
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, const QString &labeling, int label);
  };

  /**
   * \class SelectParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
   *
   * Set label to parent mapping
   *
   * \ingroup MeshProcess
   */
  class mdxBase_EXPORT SelectParents : public Process
  {
  public:
    SelectParents(const Process& process) : Process(process) 
    {
      setName("Mesh/Lineage Tracking/Select Parents");
      setDesc("Select parent labels");
      setIcon(QIcon(":/images/Parents.png"));

      addParm("Labeling","Labeling to select", "Parents");
      addParm("Parent Label","Labeling to select", "0");
    }
  
    bool run()
    {
      Mesh *mesh = currentMesh();
      if(!currentMesh())
        throw QString("%1::run No current mesh").arg(name());

      QString ccName = mesh->ccName();
      if(ccName.isEmpty())
        throw QString("%1::run No CC Name").arg(name());

      CCStructure &cs = mesh->ccStructure(ccName);
      CCIndexDataAttr &indexAttr = mesh->indexAttr();

      return run(*mesh, cs, indexAttr, parm("Labeling"), parm("Parent Label").toInt());
    }
    bool run(Mesh &mesh, CCStructure &cs, CCIndexDataAttr &indexAttr, const QString &labeling, int label);
  };

// -------------------------- 
//  /**
//   * \class CorrectParents ProcessLineage.hpp <MeshProcessLineage.hpp>
//   *
//   * Take out non-existing labels from parent map.
//   */
//  class mdxBase_EXPORT CorrectParents : public Process
//  {
//  public:
//    CorrectParents(const Process& process) : Process(process)
//    {
//      setName("Mesh/Deformation/Restore Correct Parents");
//      setDesc("Restores the correct parent labelling of the other mesh from the attribute map");
//      setIcon(QIcon(":/images/ParentsCheck.png"));
//  }
//
//    bool run()
//    {
//      if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
//        return false;
//      Mesh* mesh1, *mesh2;
//      if(currentMesh() == mesh(0)) {
//        mesh2 = mesh(0);
//        mesh1 = mesh(1);
//      } else if(currentMesh() == mesh(1)) {
//        mesh2 = mesh(1);
//        mesh1 = mesh(0);
//      } else
//        return false;
//
//      bool res = run(mesh1, mesh2);
//      return res;
//    }
//
//    bool run(Mesh* mesh1, Mesh* mesh2);
//
//  };
//
//  /**
//   * \class HeatMapProliferation MeshProcessLineage.hpp <MeshProcessLineage.hpp>
//   *
//   * Compute the heap map that shows how many daughter cells a parent cell has.
//   */
//  class mdxBase_EXPORT HeatMapProliferation : public Process
//  {
//  public:
//    HeatMapProliferation(const Process& process) : Process(process)
//    {
//      setName("Mesh/Lineage Tracking/Heat Map Proliferation");
//      setDesc("Compute a heat map that shows the cell proliferation rate, (how may daughter cells a parent cell has).");
//      setIcon(QIcon(":/images/HeatMapProliferation.png"));
//
//      addParm("Use Parents On Other Mesh","Use Parents On Other Mesh","No", booleanChoice()); // RSS shouldn't this be decided in the GUI?
//    }
//
//    bool run()
//    {
//      Mesh* m = currentMesh();
//      if(!m)
//        throw QString("%1:run No current mesh").arg(name());
//      Mesh* m2 = otherMesh();
//        throw QString("%1:run No other mesh").arg(name());
//
//      bool res = run(m,m2, stringToBool(parm("Use Parents On Other Mesh")));
//      if(res)
//        m->setShowLabel("Label Heat");
//      return res;
//    }
//
//    bool run(Mesh* m, Mesh* m2, bool onOtherMesh);
//  };
//
// /**
//   * \class MeshSelectNewWalls <MeshSelectNewWalls.hpp>
//   */
//  class MeshSelectNewWalls : public Process
//  {
//  public:
//    MeshSelectNewWalls(const Process &process) : Process(process) 
//    {
//      setName("Mesh/Lineage Tracking/Select New Walls");
//      setDesc("Select Newly Divided Walls");
//      setIcon(QIcon(":/images/NewWalls.png"));
//    }
//
//    bool run()
//    {
//      Mesh* m = currentMesh();
//      if(!m)
//        throw QString("%1:run No current mesh").arg(name());
//
//      m->updateSelection();
//      return run(m);
//    }
//
//    bool run(Mesh* m);
//  };
//
//  class mdxBase_EXPORT HeatMapAreaAsymmetry : public Process
//  {
//  public:
//    HeatMapAreaAsymmetry(const Process& process) : Process(process)
//    {
//      setName("Mesh/Lineage Tracking/Daughter Area asymmetry");
//      setDesc("Measures the asymmetry in cell-area in daughter cells. Uses standard-deviation of daughter area over mean daughter area (division by mean provides scale invariance)");
//      setIcon(QIcon(":/images/HeatMapProliferation.png"));
//  }
//
//    bool run()
//    {
//      if(!checkState().mesh(MESH_USE_PARENTS)) {
//        setErrorMessage("The current mesh must have show parents selected.");
//        return false;
//      }
//      Mesh* m = currentMesh();
//
//      bool res = run(m);
//      if(res)
//        m->setShowLabel("Label Heat");
//      return res;
//    }
//
//    bool run(Mesh* m);
//
//  };
//
//  /**
//   * \class CopyParentsToLabels ProcessLineage.hpp <MeshProcessLineage.hpp>
//   *
//   * Copy parents to labels, and clear parent table.
//   */
//  class mdxBase_EXPORT CopyParentsToLabels : public Process
//  {
//  public:
//    CopyParentsToLabels(const Process& process) : Process(process)
//    {
//      setName("Mesh/Lineage Tracking/Copy Parents to Labels");
//      setDesc("Copy parents to labels, and clear parent table.");
//      setIcon(QIcon(":/images/ParentsCopyToLabel.png"));
//  }
//
//    bool run()
//    {
//      return run(currentMesh());
//    }
//
//    bool run(Mesh* m);
//
//  };
//
//  /**
//   * \class CopyLabelsToParents ProcessLineage.hpp <MeshProcessLineage.hpp>
//   *
//   * Copy labels to parents
//   */
//  class mdxBase_EXPORT CopyLabelsToParents : public Process
//  {
//  public:
//    CopyLabelsToParents(const Process& process) : Process(process)
//    {
//      setName("Mesh/Lineage Tracking/Copy Labels to Parents");
//      setDesc("Copy labels to parents.");
//      setIcon(QIcon(":/images/ParentsCopyToLabel.png"));
//    }
//
//    bool run()
//    {
//      Mesh* m = currentMesh();
//      return run(m);
//    }
//
//    bool run(Mesh* m);
//
//  };
//
//  /**
//   * \class CorrectLabeling <MeshProcessLineage.hpp>
//   *
//   * Creates a new labeling of the mesh that avoids common colors between neighbors
//   */
//
//  class mdxBase_EXPORT CorrectLabeling : public Process
//  {
//  public:
//    CorrectLabeling(const Process& process) : Process(process)
//    {
//    setName("Mesh/Lineage Tracking/Correct Labeling Colors");
//    setDesc("Relabels cells to avoid neighboring cells with the same color.");
//      setIcon(QIcon(":/images/ParentsCopyToLabel.png"));
//
//    addParm("Number of colors","Number of colors used to color","7");
//    addParm("Colormap size","Size of the labels colormap","16");
//    addParm("Balance color distribution","Try to balance the number cells of each color","No",booleanChoice());
//    addParm("3D Mesh","Set to Yes if the mesh has volumetric cells","No",booleanChoice());
//    }
//
//    bool run()
//    {
//      Mesh* m = currentMesh();
//      return run(m,parm("Number of colors"),parm("Colormap size"),parm("Balance color distribution"), stringToBool(parm("3D Mesh")));
//    }
//
//    bool run(Mesh* mesh, QString numColorString, QString colormapSizeString, QString balanceDistributionString, bool mesh3D);
//
//  };
//
//
//    /**
//     * \class CorrectLabeling <MeshProcessLineage.hpp>
//     *
//     * Creates a new labeling of the mesh that avoids common colors between neighbors
//     */
//
//    class mdxBase_EXPORT CorrectLabelingStack : public Process
//    {
//    public:
//      CorrectLabelingStack(const Process& process) : Process(process)
//      {
//      setName("Stack/Segmentation/Correct Labeling Colors");
//      setDesc("Relabels cells to avoid neighboring cells with the same color.");
//        setIcon(QIcon(":/images/ParentsCopyToLabel.png"));
//
//      addParm("Number of colors","Number of colors used to color","7");
//      addParm("Colormap size","Size of the labels colormap","16");
//      addParm("Balance color distribution","Try to balance the number cells of each color","No",booleanChoice());
//      }
//
//      bool run()
//      {
//        Stack* stack = currentStack();
//        Store* store = stack->currentStore();
//        return run(store,parm("Number of colors"),parm("Colormap size"),parm("Balance color distribution"));
//      }
//
//      bool run(Store* store, QString numColorString, QString colormapSizeString, QString balanceDistributionString);
//
//    };
//
//
//  /**
//   * Process that resembles Aleksandra's Phython script to merge several Label-Parent files
//   *
//   * \addtogroup MeshProcess
//   */
//  class mdxBase_EXPORT MergeParentFiles : public Process
//  {
//  public:
//    MergeParentFiles(const Process& process) : Process(process)
//    {
//    setName("Mesh/Lineage Tracking/Merge Parent Files");
//    setDesc("Set the parent for selected cells");
//      setIcon(QIcon(":/images/Parents.png"));
//
//    addParm("File Open T1T0","File Open T1T0","");
//    addParm("File Open T2T1","File Open T2T1","");
//    addParm("File Save T2T0","File Save T2T0","");
//  }
//
//    bool initialize(QWidget* parent);
//
//    bool run()
//    {
//      return run(parm("File Open T1T0"), parm("File Open T2T1"), parm("File Save T2T0"));
//    }
//
//    bool run(QString file1, QString file2, QString fileNameSave);
//
//  };
//
//  class mdxBase_EXPORT CreateParentAttr : public Process
//  {
//  public:
//    CreateParentAttr(const Process& process) : Process(process)
//    {
//    setName("Mesh/Lineage Tracking/Parent Export to Attr Map");
//    setDesc("Creates an Attribute map of the chosen name (preceding Measure Label Int) using the current parent labels.");
//      setIcon(QIcon(":/images/MakeHeatMap.png"));
//
//    addParm("Prefix","Prefix","Measure Label Int");
//    addParm("Attr Name","Attr Name","Parents");
//    }
//    //bool processParms();
//
//    bool run()
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *m = currentMesh();
//      return run(m, parm("Prefix"), parm("Attr Name"));
//    }
//
//    bool run(Mesh* m, QString prefix, QString name);
//
//  };
//
//  class mdxBase_EXPORT UniqueParentsFromAttr : public Process
//  {
//  public:
//    UniqueParentsFromAttr(const Process& process) : Process(process)
//    {
//    setName("Mesh/Lineage Tracking/Unique Parents from Attrs");
//    setDesc("Unique Parents from Attrs");
//      setIcon(QIcon(":/images/MakeHeatMap.png"));
//
//    addParm("Prefix","Prefix","Measure Label Int");
//    addParm("Parent Attr 1","Parent Attr 1","P1");
//    addParm("Parent Attr 2","Parent Attr 2","P2");
//    addParm("Multiplier 10","Multiplier 10","No", booleanChoice());
//    }
//
//    bool run()
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *m = currentMesh();
//      return run(m, parm("Prefix"), parm("Parent Attr 1"), parm("Parent Attr 2"), stringToBool(parm("Multiplier 10")));
//    }
//
//    bool run(Mesh* m, QString prefix, QString name1, QString name2, bool mult10);
//
//  };
//
//
//  class mdxBase_EXPORT ImportParentAttr : public Process
//  {
//  public:
//    ImportParentAttr(const Process& process) : Process(process)
//    {
//    setName("Mesh/Lineage Tracking/Parent Import from Attr Map");
//    setDesc("Imports parent labels from an Attribute map of the chosen name (preceding Measure Label Int). Overwrites the current parents!");
//      setIcon(QIcon(":/images/MakeHeatMap.png"));
//
//    addParm("Prefix","Prefix","Measure Label Int");
//    addParm("Attr Name","Attr Name","Parents");
//    addParm("Import To Mesh","Import To Mesh","Active Mesh", QStringList() << "Active Mesh" << "Other Mesh (T1)"<< "Other Mesh (T2)");
//    }
//    bool processParms();
//
//    bool run()
//    {
//      if(!checkState().mesh(MESH_ANY))
//        return false;
//      Mesh *m = currentMesh();
//      Mesh *m2 = otherMesh();
//      return run(m, m2, parm("Prefix"), parm("Attr Name"), parm("Import To Mesh"));
//    }
//
//    bool run(Mesh* m, Mesh* m2, QString prefix, QString name, QString importMesh);
//
//  };
//
//  class mdxBase_EXPORT SelectParentLabel : public Process
//  {
//   public:
//     SelectParentLabel(const Process& process) : Process(process)
//     {
//     setName("Mesh/Lineage Tracking/Select Parents");
//     setDesc("Select Parents with a given label");
//     setIcon(QIcon(":/images/CellAtlas.png"));
//
//     addParm("Label","Label","0");
//     addParm("Keep Selection","Keep Selection","No",booleanChoice());
//   }
//
//     bool run(){
//       Mesh *m = currentMesh();
//       return run(m, parm("Label").toInt(), stringToBool(parm("Keep Selection")));
//     }
//     bool run(Mesh *m, int labelToSelect, bool keepSelection);
//
//  };
//
//  class mdxBase_EXPORT SelectParentLabelsT1 : public Process
//  {
//   public:
//     SelectParentLabelsT1(const Process& process) : Process(process)
//     {
//     setName("Mesh/Lineage Tracking/Select Parents in Parent Mesh");
//     setDesc("Select cells in the earlier mesh that have a parents label in the later mesh.");
//     setIcon(QIcon(":/images/CellAtlas.png"));
//
//   }
//
//     bool run(){
//       Mesh *m = currentMesh();
//       Mesh *m2 = otherMesh();
//       return run(m, m2);
//     }
//     bool run(Mesh *m, Mesh *m2);
//
//  };
//
//  /**
//   * Set cell type
//   *
//   * \addtogroup MeshProcess
//   */
//  class mdxBase_EXPORT SetCellType : public SetParent
//  {
//  public:
//    SetCellType(const Process& process) : SetParent(process)
//    {
//    setName("Mesh/Cell Types/Set Cell Type");
//    setDesc("Set the cell type for selected cells. Overwrite the parent label with the cell type number.");
//    setIcon(QIcon(":/images/Parents.png"));
//
//  }
//
//  };
//
//  /**
//   * \class ResetParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
//   *
//   * Clear label to parent mapping
//   *
//   * \ingroup MeshProcess
//   */
//  class mdxBase_EXPORT ResetCellTypes : public ResetParents {
//  public:
//    ResetCellTypes(const Process& process) : ResetParents(process)
//    {
//      setName("Mesh/Cell Types/Reset Cell Types");
//      setDesc("Clear mapping from parents to labels");
//      setIcon(QIcon(":/images/ParentsClear.png"));
//    }
//  
//  };
//
//  /**
//   * \class ResetParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
//   *
//   * Clear label to parent mapping
//   *
//   * \ingroup MeshProcess
//   */
//  class mdxBase_EXPORT LoadCellTypes : public LoadParents {
//  public:
//    LoadCellTypes(const Process& process) : LoadParents(process)
//    {
//      setName("Mesh/Cell Types/Load Cell Types");
//      setDesc("Clear mapping from parents to labels");
//    }
//  
//  };
//
//  /**
//   * \class ResetParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
//   *
//   * Clear label to parent mapping
//   *
//   * \ingroup MeshProcess
//   */
//  class mdxBase_EXPORT SaveCellTypes : public SaveParents {
//  public:
//    SaveCellTypes(const Process& process) : SaveParents(process)
//    {
//      setName("Mesh/Cell Types/Save Cell Types");
//      setDesc("Clear mapping from parents to labels");
//    }
//  
//  };
//
//
//  class mdxBase_EXPORT SelectCellType : public Process
//  {
//   public:
//     SelectCellType(const Process& process) : Process(process)
//     {
//       setName("Mesh/Cell Types/Select Cell Type");
//       setDesc("Select Cell Types with a given label");
//     }
//  };



  ///@}
}

#endif
