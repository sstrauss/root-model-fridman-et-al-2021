//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef DEBUGDLG_HPP
#define DEBUGDLG_HPP

#include <Config.hpp>

#include <MorphoViewer.hpp>

#include <ui_DebugDlg.h>

class DebugDlg : public QDialog 
{
  Q_OBJECT
public:
  DebugDlg(MorphoViewer* viewer, QWidget* parent = 0, Qt::WindowFlags f = 0);

protected slots:
  void on_peeling_toggled(bool on);
  void on_peelingSlice_valueChanged(int val);
  void on_peelingType_currentIndexChanged(int val);

protected:
  MorphoViewer* viewer;
  Ui::DebugDlg ui;
};

#endif
