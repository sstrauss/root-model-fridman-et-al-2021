//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <Mesh.hpp>
#include <ImageData.hpp>
#include <Stack.hpp>
#include <Information.hpp>
#include <Thrust.hpp>
#include <Dir.hpp>
#include <CCUtils.hpp>

#include <algorithm>
#include <iterator>
#include <limits>
#include <QFileInfo>

#include <boost/variant.hpp>
#include <boost/variant/static_visitor.hpp>

namespace mdx 
{
  // From SystemProcessLoad
  QList<int> extractVersion(QIODevice &file); 

  Mesh::Mesh(int id, Stack* stack) : _id(id), _stack(stack)
  {
    init();
  }
  
  void Mesh::init()
  {
//    changed_surf_function = false;
//    changed_heat_function = false;
    _changed = 0;
    _culling = true;
    _blending = true;
    _opacity = 1.0f;
    _brightness = 1.0f;
    _useParents = false;
    _imgtex = false;
    _scaled = false;
    _transformed = false;
    _showBBox = false;

    ccUpdateTimer.setInterval(1);
    ccUpdateTimer.setSingleShot(true);
  }
  
  Mesh::~Mesh() {}

  // Get the label from a cell
  int Mesh::getLabel(CCIndex c, const IntIntAttr *parents)
  {
    int label = indexAttr()[c].label;
    if(parents)
      label = (*parents)[label];
    return label;
  }

  int Mesh::getLabel(int label, const IntIntAttr &parents)
  {
    if(useParents())
      return parents[label];

    return label;
  }

  /*
   * Labeling methods 
   */

  // Get the current labeling
  const QString &Mesh::labeling() 
  {
    QString &labeling = _attributes.attrMap<QString, QString>("#LabelingChoice#")["Labeling"];
    if(labeling.isEmpty())
      labeling = "Labels";
    return labeling;
  }

  //* Set the current labeling
  bool Mesh::setLabeling(const QString &labeling)
  {
    _attributes.attrMap<QString, QString>("#LabelingChoice#")["Labeling"] = labeling;
    if(labeling == "Labels")
      _labelMap = 0;
    else
      _labelMap = &labelMap(labeling);
 
    return true;
  }

  // See if a labeling exists
  bool Mesh::labelingExists(const QString &labeling)
  {
    if(labeling == "Labels")
      labelMap("Labels");
    else if(labeling == "Parents")
      labelMap("Parents");

    const QStringList &attrList = _attributes.attrList(QString("#Labeling#%1#").arg(labeling));
    if(attrList.size() > 1)
      mdxInfo << "labelingExists: Warning, multiple labelings named " << labeling << endl;

    return !attrList.empty();
  }

  // Get a labeling map
  IntIntAttr &Mesh::labelMap(const QString &labeling)
  {
    return _attributes.attrMap<int, int>(QString("#Labeling#%1#").arg(labeling));
  }

  // Get a label, taking into account the current labeling map
  int Mesh::getLabel(int label)
  {
    if(!_labelMap)
      return label;
    else {
      auto it = _labelMap->find(label);
      if(it == _labelMap->end())
        return -1;
      else
        return it->second;
    }
  }

  // Set the labeling map for some labels
  bool Mesh::setLabelLabeling(const QString &ccName, const QString &labeling, int label, const IntSet &labels)
  {
    if(labeling == "Labels") {
      mdxInfo << "Mesh::setLabelLabeling Labeling cannot be 'Labels'" << endl;
      return false;
    }
    IntIntAttr &_labelMap = labelMap(labeling);
    for(int l : labels)
      if(label == 0)
        _labelMap.erase(l);
      else if(_labelMap[l] != label)
        _labelMap[l] = label;

    CCDrawParms &cdp = drawParms(ccName);
    CCStructure &cs = ccStructure(ccName);
    const CCIndexVec &faces = cs.faces();
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(labels.count(fIdx.label) > 0)
        cdp.faceChanged.push_back(f);
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceLabel(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
    return true;
  }  

  // Get the labeling choice list, should at least contain "Labels" and "Parents"
  QStringList Mesh::labelingAttrList()
  {
    const QStringList &attrList = _attributes.attrList("#Labeling#.*");
    QStringList result;
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      if(s.size() > 2 and s[2] != "Labels" and s[2] != "Parents")
          result << s[2];
    }

    return QStringList() << "Labels" << "Parents" << result;
  }

  IntQStringAttr &Mesh::labelName(const QString &labeling)
  {
    return _attributes.attrMap<int, QString>(QString("#LabelNames#%1#").arg(labeling));
  }

  /*
   * Signal attribute methods
   */

  // Helper function to verify names
  bool validAttrName(const QString &name)
  {
    if(name.isEmpty() or name.contains('#') or name.contains('$'))
      return false;
    return true;
  }

  // The current signal map
  const QString &Mesh::signal()
  {
    return _attributes.attrMap<QString, QString>("#SignalChoice#")["Signal"];
  }

  // Set the current signal
  bool Mesh::setSignal(const QString &signal)
  {
    _attributes.attrMap<QString, QString>("#SignalChoice#")["Signal"] = signal;
    return true;
  }

  // See if a signal exists
  bool Mesh::signalExists(const QString &signal)
  {
    if(signal.isEmpty())
      return false;

    const QStringList &attrList = _attributes.attrList(QString("#Signal#.*#%1$").arg(signal));
    if(attrList.size() > 1)
      mdxInfo << "signalExists: Warning, multiple signals named " << signal << endl;

    return !attrList.empty();
  }

  // Get type of signal attribute
  QString Mesh::signalType(const QString &signal)
  {
    if(signal.isEmpty())
      return QString();

    const QStringList &attrList = _attributes.attrList(QString("#Signal#.*#%1$").arg(signal));
    if(attrList.size() > 1)
      mdxInfo << "signalType: Warning, multiple signals named " << signal << endl;

    for(const QString &attrName : attrList) {
      QStringList s = attrName.split("#");
      return s[2];
    }

    return QString();
  }

  // Helper function to make sure each signal name has only one associated attribute
  template<typename T>
  AttrMap<CCIndex,T> &sigAttr(const QString &signal, const QString &typeName,
                              Attributes &attributes)
  {
    if(!validAttrName(signal))
      throw QString("Mesh::signalAttr Signal name invalid (%1)").arg(signal);
    if(!validAttrName(typeName))
      throw QString("Mesh::signalAttr Type name invalid (%1)").arg(typeName);

    const QStringList &attrList = attributes.attrList(QString("#Signal#.*#%1$").arg(signal));
    for(const QString &attrName : attrList) {
      QStringList s = attrName.split("#");
      if(s[2] != typeName) {
        mdxInfo << "Warning, erasing existing signal " << signal
                         << " of type " << s[2] << endl;
        attributes.erase(attrName);
      }
    }

    return attributes.attrMap<CCIndex,T>(QString("#Signal#"+typeName+"#"+signal));
  }

  // Get the signal maps for various types
  template<> CCIndexIntAttr &Mesh::signalAttr<int>(const QString &signal)
  {
    auto &attr = sigAttr<int>(signal,"Int",_attributes);
    signalColorMap(signal).makeIndexMap();
    return attr;
  }
  template<> CCIndexDoubleAttr &Mesh::signalAttr<double>(const QString &signal)
  {
    auto &attr = sigAttr<double>(signal,"Double",_attributes);
    signalColorMap(signal).makeRangeMap();
    return attr;
  }
  template<> CCIndexPoint2dAttr &Mesh::signalAttr<Point2d>(const QString &signal)
  {
    auto &attr = sigAttr<Point2d>(signal,"Point2d",_attributes);
    signalColorMap(signal).makeRangeMap();
    return attr;
  }
  template<> CCIndexPoint3dAttr &Mesh::signalAttr<Point3d>(const QString &signal)
  {
    auto &attr = sigAttr<Point3d>(signal,"Point3d",_attributes);
    signalColorMap(signal).makeRangeMap();
    return attr;
  }
  template<> CCIndexColorbAttr &Mesh::signalAttr<Colorb>(const QString &signal)
  {
    auto &attr = sigAttr<Colorb>(signal,"Colorb",_attributes);
    signalColorMap(signal).makePassthroughMap();
    return attr;
  }

  // Get the signal bounds
  const Point2d &Mesh::signalBounds(const QString &signal)
  {
    if(!validAttrName(signal))
      throw QString("Mesh::signalBounds Signal name invalid (%1)").arg(signal);

     return signalColorMap(signal).channelMap(0).bounds;
  }
  // Set the signal bounds
  bool Mesh::setSignalBounds(const Point2d &signalBounds, const QString &signal)
  {
    if(signal.isEmpty())
      return false;

    signalColorMap(signal).channelMap(0).bounds = signalBounds;
    return true;
  }

  // Get the signal unit
  const QString &Mesh::signalUnit(const QString &signal)
  {
    if(!validAttrName(signal))
      throw QString("Mesh::signalUnit Signal name invalid (%1)").arg(signal);

    return signalColorMap(signal).channelMap(0).unit;
  }
  // Set the signal unit
  bool Mesh::setSignalUnit(const QString &signalUnit, const QString &signal)
  {
    if(signal.isEmpty())
      return false;

    signalColorMap(signal).channelMap(0).unit = signalUnit;
    return true;
  }

  // Get the signal map choice list
  QStringList Mesh::signalAttrList()
  {
    const QStringList &attrList = _attributes.attrList("#Signal#.*");
    QStringList result;
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      if(s.size() > 3)
          result << s[3];
    }
    result.sort();
    return  result;
  }

  // Get the signal colormap name
  QString Mesh::signalColorMapName(const QString &signal) 
  {
    if(signal.isEmpty())
      return QString();
    return QString("#SignalColorMap#%1").arg(signal);
  }
  // Get the signal colorMap
  ColorMap &Mesh::signalColorMap(const QString &signal)
  {
    if(!validAttrName(signal))
      throw QString("Mesh::signalColorMap Signal name invalid (%1)").arg(signal);

    ColorMap &colorMap = _attributes.attrMap<QString, ColorMap>("#ColorMaps#")[signalColorMapName(signal)];
    if(colorMap.size() == 0)
      colorMap.setColors("Default");
    return colorMap;
  }

  // Get the signal as a VizAttribute
  VizAttribute<CCIndex> Mesh::signalVizAttribute(const QString &signal)
  {
    if(!validAttrName(signal))
      throw QString("Mesh::signalVizAttribute Signal name invalid (%1)").arg(signal);

    const ColorMap &colorMap = signalColorMap(signal);
    const QStringList &attrList = _attributes.attrList(QString("#Signal#.*#%1$").arg(signal));
    if(attrList.size() > 1)
      mdxInfo << "signalVizAttribute: Warning, multiple signals named " << signal << endl;

    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      QString signalType = s[2];
      if(signalType == "Double")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,double>(attr), colorMap);
      else if(signalType == "Point2d")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,Point2d>(attr), colorMap);
      else if(signalType == "Point3d")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,Point3d>(attr), colorMap);
      else if(signalType == "Colorb")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,Colorb>(attr), colorMap);
      else if(signalType == "Int")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,int>(attr), colorMap);
    }

    throw(QString("signalVizAttribute: Could not find signal %1 with appropriate type").arg(signal));
  }

  // Clear a signal
  bool Mesh::signalErase(const QString &signal)
  {
    if(!signalExists(signal))
      return false;
    // Erase all matches, there should only be one
    for(const QString &attrName : _attributes.attrList(QString("#Signal#.*#%1$").arg(signal)))
      _attributes.erase(attrName);
    // Erase the colorMap
    _attributes.attrMap<QString, ColorMap>("#ColorMaps#").erase(signalColorMapName(signal));

    return true;
  }

  /*
   * Heat map methods
   */
  
  // The current heat map
  const QString &Mesh::heat()
  {
    return _attributes.attrMap<QString, QString>("#HeatChoice#")["Heat"];
  }

  // Set the current heat map
  bool Mesh::setHeat(const QString &heat)
  {
    _attributes.attrMap<QString, QString>("#HeatChoice#")["Heat"] = heat;
    return true;
  }

  // See if heat map exists
  bool Mesh::heatExists(const QString &heat, const QString &labeling)
  {
    if(heat.isEmpty() or labeling.isEmpty())
      return false;

    const QStringList &attrList = _attributes.attrList(QString("#Heat#%1#.*#%2$").arg(labeling).arg(heat));
    if(attrList.size() > 1)
      mdxInfo << "heatExists: Warning, multiple heat maps " << labeling << "/" << heat << endl;
    return(!attrList.empty());
  }

  // Get type of heat map
  QString Mesh::heatType(const QString &heat, const QString &labeling)
  {
    if(heat.isEmpty() or labeling.isEmpty())
      return QString();

    const QStringList &attrList = _attributes.attrList(QString("#Heat#%1#.*#%2$").arg(labeling).arg(heat));
    if(attrList.size() > 1)
      mdxInfo << "heatType: Warning, multiple heat maps " << labeling << "/" << heat << endl;

    for(const QString &attrName : attrList) {
      QStringList s = attrName.split("#");
      return s[3];
    }

    return QString();
  }

  // Helper function to make sure each heat/labeling combination has only one associated attribute
  template<typename T>
  AttrMap<int,T> &htAttr(const QString &heat, const QString &labeling,
                         const QString &typeName, Attributes &attributes)
  {
    if(!validAttrName(heat))
      throw QString("Mesh::heatAttr Heat name invalid (%1)").arg(heat);
    if(!validAttrName(labeling))
      throw QString("Mesh::heatAttr Labeling name invalid (%1)").arg(labeling);
    if(!validAttrName(typeName))
      throw QString("Mesh::heatAttr Type name invalid (%1)").arg(typeName);

    const QStringList &attrList = attributes.attrList(QString("#Heat#%1#.*#%2$").arg(labeling).arg(heat));
    for(const QString &attrName : attrList) {
      QStringList s = attrName.split("#");
      if(s[3] != typeName) {
        mdxInfo << "Warning, erasing existing heat " << labeling << "/" << heat
                         << " of type " << s[3] << endl;
        attributes.erase(attrName);
      }
    }

    return attributes.attrMap<int,T>(QString("#Heat#"+labeling+"#"+typeName+"#"+heat));
  }

  // Get the heat map for various data types
  template<> IntIntAttr &Mesh::heatAttr<int>(const QString &heat, const QString &labeling)
  {
    auto &attr = htAttr<int>(heat,labeling,"Int",_attributes);
    heatColorMap(heat,labeling).makeIndexMap();
    return attr;
  }
  template<> IntDoubleAttr &Mesh::heatAttr<double>(const QString &heat, const QString &labeling)
  {
    auto &attr = htAttr<double>(heat,labeling,"Double",_attributes);
    heatColorMap(heat,labeling).makeRangeMap();
    return attr;
  }
  template<> IntPoint2dAttr &Mesh::heatAttr<Point2d>(const QString &heat, const QString &labeling)
  {
    auto &attr = htAttr<Point2d>(heat,labeling,"Point2d",_attributes);
    heatColorMap(heat,labeling).makeRangeMap();
    return attr;
  }
  template<> IntPoint3dAttr &Mesh::heatAttr<Point3d>(const QString &heat, const QString &labeling)
  {
    auto &attr = htAttr<Point3d>(heat,labeling,"Point3d",_attributes);
    heatColorMap(heat,labeling).makeRangeMap();
    return attr;
  }
  template<> IntColorbAttr &Mesh::heatAttr<Colorb>(const QString &heat, const QString &labeling)
  {
    auto &attr = htAttr<Colorb>(heat,labeling,"Colorb",_attributes);
    heatColorMap(heat,labeling).makePassthroughMap();
    return attr;
  }

  // Get the heat bounds
  const Point2d &Mesh::heatBounds(const QString &heat, const QString &labeling)
  {
    if(!validAttrName(heat))
      throw QString("Mesh::heatBounds Heat name invalid (%1)").arg(heat);
    if(!validAttrName(labeling))
      throw QString("Mesh::heatBounds Labeling name invalid (%1)").arg(labeling);

    return heatColorMap(heat, labeling).channelMap(0).bounds;
  }
  // Set the heat bounds
  bool Mesh::setHeatBounds(const Point2d &heatBounds, const QString &heat, const QString &labeling)
  {
    if(heat.isEmpty() or labeling.isEmpty())
      return false;

    heatColorMap(heat, labeling).channelMap(0).bounds = heatBounds;
    return true;
  }

  // Get the heat unit
  const QString &Mesh::heatUnit(const QString &heat, const QString &labeling)
  {
    if(!validAttrName(heat))
      throw QString("Mesh::heatUnit Heat name invalid (%1)").arg(heat);
    if(!validAttrName(labeling))
      throw QString("Mesh::heatUnit Labeling name invalid (%1)").arg(labeling);

    return heatColorMap(heat, labeling).channelMap(0).unit;
  }
  // Set the heat unit
  bool Mesh::setHeatUnit(const QString &heatUnit, const QString &heat, const QString &labeling)
  {
    if(heat.isEmpty() or labeling.isEmpty())
      return false;

    heatColorMap(heat, labeling).channelMap(0).unit = heatUnit;
    return true;
  }

  // Get the heat colormap name
  QString Mesh::heatColorMapName(const QString &heat, const QString &labeling) 
  {
    if(heat.isEmpty() or labeling.isEmpty())
      return QString();

    return QString("#HeatColorMap#%1#%2").arg(labeling).arg(heat);
  }
  // Get the heat colorMap
  ColorMap &Mesh::heatColorMap(const QString &heat, const QString &labeling)
  {
    if(!validAttrName(heat))
      throw QString("Mesh::heatColorMap Heat name invalid (%1)").arg(heat);
    if(!validAttrName(labeling))
      throw QString("Mesh::heatColorMap Labeling name invalid (%1)").arg(labeling);

    ColorMap &colorMap = _attributes.attrMap<QString, ColorMap>("#ColorMaps#")[heatColorMapName(heat, labeling)];
    if(colorMap.size() == 0)
      colorMap.setColors("Jet");
    return colorMap;
  }

  // Get the heat as a VizAttribute
  VizAttribute<int> Mesh::heatVizAttribute(const QString &heat, const QString &labeling)
  {
    const QStringList &attrList = _attributes.attrList(QString("#Heat#%1#.*#%2$").arg(labeling).arg(heat));
    const ColorMap &colorMap = heatColorMap(heat,labeling);
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      QString heatType = s[3];
      if(heatType == "Double")
        return VizAttribute<int>(_attributes.attrMap<int,double>(attr), colorMap);
      else if(heatType == "Point2d")
        return VizAttribute<int>(_attributes.attrMap<int,Point2d>(attr), colorMap);
      else if(heatType == "Point3d")
        return VizAttribute<int>(_attributes.attrMap<int,Point3d>(attr), colorMap);
      else if(heatType == "Colorb")
        return VizAttribute<int>(_attributes.attrMap<int,Colorb>(attr), colorMap);
      else if(heatType == "int")
        return VizAttribute<int>(_attributes.attrMap<int,int>(attr), colorMap);
    }

    throw(QString("heatVizAttribute: Could not find heat %1/%2 with appropriate type")
          .arg(labeling).arg(heat));
  }


  // Get the heat map choice list
  QStringList Mesh::heatAttrList(const QString &labeling, const QString &type)
  { 
    if(labeling.isEmpty())
      return QStringList();

    const QStringList &attrList = _attributes.attrList(QString("#Heat#%1#.*").arg(labeling));
    QStringList result;
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      if(s.size() > 4 and s[2] == labeling and s[3] == type)
        result << s[4];
    }
    result.sort();
    return result;
  }

  // Get the heat map choice list
  QStringList Mesh::heatAttrList(const QString &labeling)
  { 
    if(labeling.isEmpty())
      return QStringList();

    const QStringList &attrList = _attributes.attrList(QString("#Heat#%1#.*").arg(labeling));
    QStringList result;
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      if(s.size() > 4 and s[2] == labeling)
        result << s[4];
    }
    result.sort();
    return result;
  }

  // Clear a heat map
  bool Mesh::heatErase(const QString &heat, const QString &labeling)
  {
    if(!heatExists(heat, labeling))
      return false;
    // Erase the attribute
    _attributes.erase(QString("#Heat#%1#Double#%2").arg(labeling).arg(heat));
    // Erase the colorMap
    _attributes.attrMap<QString, ColorMap>("#ColorMaps#").erase(heatColorMapName(heat, labeling));

    return true;
  }

  /*
   * Methods for custom attributes
   */

  // See if a custom vertex attribute exists
  bool Mesh::customVertexExists(const QString &ccName, const QString &attrName)
  {
    const QStringList &attrList = _attributes.attrList(QString("#CustomVertex#.*#%1#%2$").arg(ccName).arg(attrName));
    if(attrList.size() > 1)
      mdxInfo << "Mesh::customVertexExists: Warning, multiple signals named " << attrName << " for cc:" << ccName << endl;
    return(!attrList.empty());
  }

  // Get type of custom vertex attribute
  QString Mesh::customVertexType(const QString &ccName, const QString &attrName)
  {
    const QStringList &attrList = _attributes.attrList(QString("#CustomVertex#.*#%1#%2$").arg(ccName).arg(attrName));
    if(attrList.size() > 1)
      mdxInfo << "Mesh::customVertexType: Warning, multiple attributes named:" << attrName << " for cc:" << ccName << endl;

    for(const QString &attrName : attrList) {
      QStringList s = attrName.split("#");
      return s[2];
    }

    throw QString("Mesh::customVertexType: Could not find any attribute called %1").arg(attrName);
  }

  // Helper function to make sure each custom vertex attribute name has only one associated attribute
  template<typename T>
  AttrMap<CCIndex, T> &custVertAttr(const QString &ccName, const QString &attrName, const QString &typeName, Attributes &attributes)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::custVertexAttr Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::custVertexAttr Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(typeName))
      throw QString("Mesh::custVertexAttr Type name invalid (%1)").arg(typeName);

    const QStringList &attrList = attributes.attrList(QString("#CustomVertex#.*#%1#%2$").arg(ccName).arg(attrName));
    for(const QString &attrName : attrList) {
      QStringList s = attrName.split("#");
      if(s.size() < 5)
        continue;
      if(s[2] != typeName) {
        mdxInfo << "Warning, erasing existing attribute " << attrName  << " of type " << s[2] << endl;
        attributes.erase(attrName);
      }
    }

    return attributes.attrMap<CCIndex, T>(QString("#CustomVertex#%1#%2#%3").arg(typeName).arg(ccName).arg(attrName));
  }

  // Get the signal maps for various types
  template<> CCIndexIntAttr &Mesh::customVertexAttr<int>(const QString &ccName, const QString &attrName)
  {
    customVertexColorMap(ccName, attrName).makeIndexMap();
    return custVertAttr<int>(ccName, attrName, "Int", _attributes);
  }
  template<> CCIndexDoubleAttr &Mesh::customVertexAttr<double>(const QString &ccName, const QString &attrName)
  {
    customVertexColorMap(ccName, attrName).makeRangeMap();
    return custVertAttr<double>(ccName, attrName, "Double", _attributes);
  }
  template<> CCIndexPoint2dAttr &Mesh::customVertexAttr<Point2d>(const QString &ccName, const QString &attrName)
  {
    customVertexColorMap(ccName, attrName).makeRangeMap();
    return custVertAttr<Point2d>(ccName, attrName, "Point2d", _attributes);
  }
  template<> CCIndexPoint3dAttr &Mesh::customVertexAttr<Point3d>(const QString &ccName, const QString &attrName)
  {
    customVertexColorMap(ccName, attrName).makeRangeMap();
    return custVertAttr<Point3d>(ccName, attrName, "Point3d", _attributes);
  }
  template<> CCIndexColorbAttr &Mesh::customVertexAttr<Colorb>(const QString &ccName, const QString &attrName)
  {
    customVertexColorMap(ccName, attrName).makePassthroughMap();
    return custVertAttr<Colorb>(ccName, attrName, "Colorb", _attributes);
  }

  // Get the list of custom vertex attributes
  QStringList Mesh::customVertexAttrList(const QString &ccName)
  {
    const QStringList &attrList = _attributes.attrList(QString("#CustomVertex#.*#%1#.*").arg(ccName));
    QStringList result;
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      if(s.size() >= 5)
        result << s[4];
    }

    return QStringList() << result;
  }

  // Get the customVertex colormap name
  QString Mesh::customVertexColorMapName(const QString &ccName, const QString &attrName) 
  {
    return QString("#CustomVertex#%1#%2").arg(ccName).arg(attrName);
  }
  // Get the customVertex colorMap
  ColorMap &Mesh::customVertexColorMap(const QString &ccName, const QString &attrName)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::custVertexiColorMap Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::custVertexColorMap Attribute name invalid (%1)").arg(attrName);

    return _attributes.attrMap<QString, ColorMap>("#ColorMaps#")[customVertexColorMapName(ccName, attrName)];
  }

  // Get the customVertex as a VizAttribute
  VizAttribute<CCIndex> Mesh::customVertexVizAttribute(const QString &ccName, const QString &attrName)
  {
    const ColorMap &colorMap = customVertexColorMap(ccName, attrName);
    const QStringList &attrList = _attributes.attrList(QString("#CustomVertex#.*#%1#%2$").arg(ccName).arg(attrName));
    if(attrList.size() > 1)
      mdxInfo << "customVertexVizAttribute: Warning, multiple customVertexs named " << attrName << endl;

    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      QString customVertexType = s[2];
      if(customVertexType == "Double")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,double>(attr), colorMap);
      else if(customVertexType == "Point2d")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,Point2d>(attr), colorMap);
      else if(customVertexType == "Point3d")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,Point3d>(attr), colorMap);
      else if(customVertexType == "Colorb")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,Colorb>(attr), colorMap);
      else if(customVertexType == "Int")
        return VizAttribute<CCIndex>(_attributes.attrMap<CCIndex,int>(attr), colorMap);
    }

    throw(QString("customVertexVizAttribute: Could not find customVertex %1 with appropriate type").arg(attrName));
  }

  /// Set the custom vertex attribute point size
  bool Mesh::customVertexSetPointSize(const QString &ccName, const QString &attrName, double pointSize)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::customVertexSetPointSize Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::customVertexSetPointSize Attribute name invalid (%1)").arg(attrName);

    _attributes.attrMap<QString, double>(QString("#CustomVertex#%1#%2").arg(ccName).arg(attrName))["PointSize"] = pointSize;
    return true;
  }

  /// Get the custom vertex attribute point size
  double Mesh::customVertexPointSize(const QString &ccName, const QString &attrName)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::customVertexPointSize Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::customVertexPointSize Attribute name invalid (%1)").arg(attrName);

    return _attributes.attrMap<QString, double>(QString("#CustomVertex#%1#%2").arg(ccName).arg(attrName))["PointSize"];
  }

  // Clear a custom vertex attribute
  bool Mesh::customVertexErase(const QString &ccName, const QString &attrName)
  {
    if(!customVertexExists(ccName, attrName))
      return false;
    // Get the attribute and erase it
    const QStringList &attrList = _attributes.attrList(QString("#CustomVertex#.*#%1#%2$").arg(ccName).arg(attrName));
    for(auto attr : attrList)
      _attributes.erase(attr);
    // Erase the properties (point size, etc)
    const QStringList &propAttrList = _attributes.attrList(QString("#CustomVertex#%1#%2$").arg(ccName).arg(attrName));
    for(auto attr : propAttrList)
      _attributes.erase(attr);
    // Erase the colorMap
    _attributes.attrMap<QString, ColorMap>("#ColorMaps#").erase(customVertexColorMapName(ccName, attrName));

    return true;
  }

  // See if a cell axis exists
  bool Mesh::cellAxisExists(const QString &ccName, const QString &attrName, const QString &labeling)
  {
    const QStringList &attrList = _attributes.attrList(QString("#CellAxis#SymmetricTensor#%1#%2#%3$").arg(ccName).arg(labeling).arg(attrName));
    return(!attrList.empty());
  }

  // Get a list of the cell axes
  QStringList Mesh::cellAxisAttrList(const QString &ccName, const QString &labeling)
  {
    const QStringList &attrList = _attributes.attrList(QString("#CellAxis#SymmetricTensor#%1#%2#.*$").arg(ccName).arg(labeling));
    QStringList result;
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      if(s.size() >= 6)
        result << s[5];
    }
    return QStringList() << result;;
  }

  // Get the signal map for various data types
  // Matrix is stored as 1st two eigenvectors and then values
  AttrMap<int, SymmetricTensor> &Mesh::cellAxisAttr(const QString &ccName, const QString &attrName, const QString &labeling)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisAttr Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisAttr Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisAttr Labeling name invalid (%1)").arg(labeling);

    // Trigger colormap defaults if required
    cellAxisColorMap(ccName, attrName, labeling);
    return _attributes.attrMap<int, SymmetricTensor>(QString("#CellAxis#SymmetricTensor#%1#%2#%3").arg(ccName).arg(labeling).arg(attrName));
  }

  // Get the cell axis attribute's colormap name
  QString Mesh::cellAxisColorMapName(const QString &ccName, const QString &attrName, const QString &labeling)
  {
    return QString("#CellAxis#%1#%2#%3").arg(ccName).arg(attrName).arg(labeling);
  }

  // Get the cell axis attribute's colormap name
  ColorMap &Mesh::cellAxisColorMap(const QString &ccName, const QString &attrName, const QString &labeling)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisColorMap Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisColorMap Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisColorMap Labeling name invalid (%1)").arg(labeling);
  
    ColorMap &colorMap = _attributes.attrMap<QString, ColorMap>("#ColorMaps#")[cellAxisColorMapName(ccName, attrName, labeling)];
    if(colorMap.size() == 0 or colorMap.numChannels() < 3) {
      colorMap.makeRangeMap(3);
      colorMap.setColors("Axis");
      colorMap.setBounds(0, Point2d(1.0, 1.0));
      colorMap.setIndices(0, Point2i(0, 1));
      colorMap.setBounds(1, Point2d(1.0, 1.0));
      colorMap.setIndices(1, Point2i(2, 3));
      colorMap.setBounds(2, Point2d(1.0, 1.0));
      colorMap.setIndices(2, Point2i(4, 5));
      cellAxisSetLineWidth(ccName, attrName, 1.0);
    }
    return colorMap;
  }

  // Erase a cell axis
  bool Mesh::cellAxisErase(const QString &ccName, const QString &attrName, const QString &labeling)
  {
    if(!cellAxisExists(ccName, attrName, labeling))
      return false;
    // Erase the attribute
    _attributes.erase(QString("#CellAxis#SymmetricTensor#%1#%2#%3").arg(ccName).arg(labeling).arg(attrName));
    // Erase the double parameters (line width)
    _attributes.erase(QString("#CellAxis#DoubleParms#%1#%2#%3").arg(ccName).arg(labeling).arg(attrName));
    // Erase the colorMap
    _attributes.attrMap<QString, ColorMap>("#ColorMaps#").erase(cellAxisColorMapName(ccName, attrName, labeling));

    return true;
  }

  // Get the cell axis line width
  double Mesh::cellAxisLineWidth(const QString &ccName, const QString &attrName, const QString &labeling)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisLineWidth Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisLineWidth Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisLineWidth Labeling name invalid (%1)").arg(labeling);

    return _attributes.attrMap<QString, double>(QString("#CellAxis#DoubleParms#%1#%2#%3").arg(ccName).arg(labeling).arg(attrName))["LineWidth"];
  }

  // Set the cell axis line width
  bool Mesh::cellAxisSetLineWidth(const QString &ccName, const QString &attrName, const QString &labeling, const double lineWidth)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisSetLineWidth Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisSetLineWidth Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisSetLineWidth Labeling name invalid (%1)").arg(labeling);

    _attributes.attrMap<QString, double>(QString("#CellAxis#DoubleParms#%1#%2#%3").arg(ccName).arg(labeling).arg(attrName))["LineWidth"] = lineWidth;
    return true;
  }

  // Get the cell axis bounds
  const Point2d &Mesh::cellAxisBounds(const QString &ccName, const QString &attrName, const QString &labeling, uint channel)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisBounds Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisBounds Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisBounds Labeling name invalid (%1)").arg(labeling);

    return cellAxisColorMap(ccName, attrName, labeling).channelMap(channel).bounds;
  }

  // Set the cell axis bounds
  bool Mesh::cellAxisSetBounds(const QString &ccName, const QString &attrName, const QString &labeling, uint channel, const Point2d &bounds)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisSetBounds Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisSetBounds Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisSetBounds Labeling name invalid (%1)").arg(labeling);

    cellAxisColorMap(ccName, attrName, labeling).channelMap(channel).bounds = bounds;
    return true;
  }

  // Get the cell axis unit
  const QString &Mesh::cellAxisUnit(const QString &ccName, const QString &attrName, const QString &labeling)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisUnit Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisUnit Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisUnit Labeling name invalid (%1)").arg(labeling);

    return cellAxisColorMap(ccName, attrName, labeling).channelMap(0).unit;
  }

  // Set the cell axis uint
  bool Mesh::cellAxisSetUnit(const QString &ccName, const QString &attrName, const QString &labeling, const QString &unit)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisSetUnit Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::cellAxisSetUnit Attribute name invalid (%1)").arg(attrName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisSetUnit Labeling name invalid (%1)").arg(labeling);

    cellAxisColorMap(ccName, attrName, labeling).channelMap(0).unit = unit;
    return true;
  }

  // The centers and normals by label for the axis
  IntMatrix2x3dAttr &Mesh::cellAxisCentersNormals(const QString &ccName, const QString &labeling)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::cellAxisAttr Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(labeling))
      throw QString("Mesh::cellAxisAttr Labeling name invalid (%1)").arg(labeling);

    return _attributes.attrMap<int, Matrix2x3d>(QString("#CellAxis#CentersNormals#%1#%2").arg(ccName).arg(labeling));
  }

  // See if a axis exists
  bool Mesh::axisExists(const QString &ccName, const QString &attrName)
  {
    const QStringList &attrList = _attributes.attrList(QString("#Axis#SymmetricTensor#%1#%2$").arg(ccName).arg(attrName));
    return(!attrList.empty());
  }

  // Get a list of the axes
  QStringList Mesh::axisAttrList(const QString &ccName)
  {
    const QStringList &attrList = _attributes.attrList(QString("#Axis#SymmetricTensor#%1#.*$").arg(ccName));
    QStringList result;
    for(const QString &attr : attrList) {
      QStringList s = attr.split("#");
      if(s.size() >= 5)
        result << s[4];
    }
    return QStringList() << result;;
  }

  // Get the signal map for various data types
  // Matrix is stored as 1st two eigenvectors and then values
  AttrMap<CCIndex, SymmetricTensor> &Mesh::axisAttr(const QString &ccName, const QString &attrName)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisAttr Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisAttr Attribute name invalid (%1)").arg(attrName);

    // Use a range map so we can use the bounds for the size
    axisColorMap(ccName, attrName);
    return _attributes.attrMap<CCIndex, SymmetricTensor>(QString("#Axis#SymmetricTensor#%1#%2").arg(ccName).arg(attrName));
  }

  // Get the axis attribute's colormap name
  QString Mesh::axisColorMapName(const QString &ccName, const QString &attrName)
  {
    return QString("#Axis#%1#%2").arg(ccName).arg(attrName);
  }

  // Get the axis attribute's colormap name
  ColorMap &Mesh::axisColorMap(const QString &ccName, const QString &attrName)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisColorMap Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisColorMap Attribute name invalid (%1)").arg(attrName);
  
    ColorMap &colorMap = _attributes.attrMap<QString, ColorMap>("#ColorMaps#")[axisColorMapName(ccName, attrName)];
    if(colorMap.size() == 0 or colorMap.numChannels() < 3) {
      colorMap.makeRangeMap(3);
      colorMap.setColors("Axis");
      colorMap.setBounds(0, Point2d(1.0, 1.0));
      colorMap.setIndices(0, Point2i(0, 1));
      colorMap.setBounds(1, Point2d(1.0, 1.0));
      colorMap.setIndices(1, Point2i(2, 3));
      colorMap.setBounds(2, Point2d(1.0, 1.0));
      colorMap.setIndices(2, Point2i(4, 5));
      axisSetLineWidth(ccName, attrName, 1.0);
    }
    return colorMap;
  }

  // Erase an axis
  bool Mesh::axisErase(const QString &ccName, const QString &attrName)
  {
    if(!axisExists(ccName, attrName))
      return false;
    // Erase the attribute
    _attributes.erase(QString("#Axis#SymTensot#%1#%2").arg(ccName).arg(attrName));
    // Erase the double parameters (line width)
    _attributes.erase(QString("#Axis#DoubleParms#%1#%2").arg(ccName).arg(attrName));
    // Erase the colorMap
    _attributes.attrMap<QString, ColorMap>("#ColorMaps#").erase(axisColorMapName(ccName, attrName));

    return true;
  }

  // Get the axis line width
  double Mesh::axisLineWidth(const QString &ccName, const QString &attrName)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisLineWidth Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisLineWidth Attribute name invalid (%1)").arg(attrName);

    return _attributes.attrMap<QString, double>(QString("#Axis#DoubleParms#%1#%2").arg(ccName).arg(attrName))["LineWidth"];
  }

  // Set the axis line width
  bool Mesh::axisSetLineWidth(const QString &ccName, const QString &attrName, const double lineWidth)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisSetLineWidth Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisSetLineWidth Attribute name invalid (%1)").arg(attrName);

    _attributes.attrMap<QString, double>(QString("#Axis#DoubleParms#%1#%2").arg(ccName).arg(attrName))["LineWidth"] = lineWidth;
    return true;
  }

  // Get the axis bounds
  const Point2d &Mesh::axisBounds(const QString &ccName, const QString &attrName, uint channel)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisBounds Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisBounds Attribute name invalid (%1)").arg(attrName);

    return axisColorMap(ccName, attrName).channelMap(channel).bounds;
  }

  // Set the axis bounds
  bool Mesh::axisSetBounds(const QString &ccName, const QString &attrName, uint channel, const Point2d &bounds)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisSetBounds Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisSetBounds Attribute name invalid (%1)").arg(attrName);

    axisColorMap(ccName, attrName).channelMap(channel).bounds = bounds;
    return true;
  }

  // Get the axis unit
  const QString &Mesh::axisUnit(const QString &ccName, const QString &attrName)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisUnit Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisUnit Attribute name invalid (%1)").arg(attrName);

    return axisColorMap(ccName, attrName).channelMap(0).unit;
  }

  // Set the axis unit
  bool Mesh::axisSetUnit(const QString &ccName, const QString &attrName, const QString &unit)
  {
    if(!validAttrName(ccName))
      throw QString("Mesh::axisSetUnit Cell complex name invalid (%1)").arg(ccName);
    if(!validAttrName(attrName))
      throw QString("Mesh::axisSetUnit Attribute name invalid (%1)").arg(attrName);

    axisColorMap(ccName, attrName).channelMap(0).unit = unit;
    return true;
  }

  // Reset the mesh
  // FIXME is there still an issue with this? (it seems to sometimes crashe)
  void Mesh::reset()
  {
    // First clear free openGL buffers (should be done in GUI thread)
    emit clearBuffers(id());

    // Clear the render groups
    for(auto &name : ccNames())
      drawParms(name).clearGroups();

    // Erase the draw parms
    for(auto &name : ccNames())
      erase(name);

    // Clear the cell complexes
    ccs().clear();

    // Reset the index factory
    CCIndexFactory.reset();

    // Clear the drawParms attributes
    drawParmsAttr().clear();

    _attributes.clear();
    _indexAttr = 0;
    _labelMap = 0;
    setCCName(QString());

    _VVCorrespondence.clear();
  
    setFile();
    clearImgTex();
    setScaled(true);
    setTransformed(false);
  }
  
  // Set the mesh filename
  void Mesh::setFile(const QString& file)
  {
    if(file.isEmpty()) {
      _file = file;
    } else {
      _file = absoluteFilePath(file);
    }
  }

  void Mesh::resetModified()
  {
//    changed_surf_function = false;
//    changed_heat_function = false;
    _changed = 0;
  }
  
  void Mesh::updateBBox()
  {
    _bbox = BoundingBox3d(Point3d(0,0,0), Point3d(0,0,0));
    for(auto &cs : ccs())
      for(auto v : cs.second.vertices()) {
        auto &vIdx = indexAttr()[v];
        _bbox |= Point3d(vIdx.pos);
      }
  }

  ParmsAttr &Mesh::drawParmsAttr()
  {
    return attributes().attrMap<QString, CCDrawParms>("CCDrawParms");
  }
  CCDrawParms &Mesh::drawParms(const QString &name)
  {
    CCDrawParms &cdp = drawParmsAttr()[name];
    cdp.setAttributes(&_attributes);
    return cdp;
  }

  // Update calls
  void Mesh::updateAll(const QString &name)
  {
    drawParms(name).changed |=
      CCDrawParms::TopologyChanged |
      CCDrawParms::PositionsChanged |
      CCDrawParms::PropertiesChanged |
      CCDrawParms::DrawChoicesChanged;
  }
  void Mesh::updatePositions(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::PositionsChanged;
  }
  void Mesh::updateProperties(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::PropertiesChanged;
  }
  void Mesh::updateVertexSelect(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::VertexSelectChanged;
  }
  void Mesh::updateFaceSelect(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::FaceSelectChanged;
  }
  void Mesh::updateVolumeSelect(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::VolumeSelectChanged;
  }
  void Mesh::updateSelect(const QString &name)
  {
    // update everything except edges
    updateVertexSelect(name);
    updateFaceSelect(name);
    updateVolumeSelect(name);
  }
  void Mesh::updateSelectPositions(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::SelectPositionsChanged;
  }
  void Mesh::updateFaceLabel(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::FaceLabelChanged;
  }
  void Mesh::updateCellEdges(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::CellEdgesChanged;
  }
  void Mesh::updateDrawChoices(const QString &name)
  {
    drawParms(name).changed |= CCDrawParms::DrawChoicesChanged;
  }

  void Mesh::updateAll()
  {
    for(auto &pr : ccs())
      updateAll(pr.first);
  }
  void Mesh::updatePositions()
  {
    for(auto &pr : ccs())
      updatePositions(pr.first);
  }
  void Mesh::updateProperties()
  {
    for(auto &pr : ccs())
      updateProperties(pr.first);
  }
  void Mesh::updateVertexSelect()
  {
    for(auto &pr : ccs())
      updateVertexSelect(pr.first);
  }
  void Mesh::updateFaceSelect()
  {
    for(auto &pr : ccs())
      updateFaceSelect(pr.first);
  }
  void Mesh::updateVolumeSelect()
  {
    for(auto &pr : ccs())
      updateVolumeSelect(pr.first);
  }
  void Mesh::updateSelect()
  {
    for(auto &pr : ccs())
      updateSelect(pr.first);
  }
  void Mesh::updateSelectPositions()
  {
    for(auto &pr : ccs())
      updateSelectPositions(pr.first);
  }
  void Mesh::updateDrawChoices()
  {
    for(auto &pr : ccs())
      updateDrawChoices(pr.first);
  }

  // Read/write cell complexes
  bool Mesh::writeCCStructures(QIODevice &file)
  {
    // write number of cell complexes
    writeFile(file, ccs().size());
    if(ccs().empty()) 
      return true;

    // Clean up the attribute map
    std::unordered_set<CCIndex> allCells;
    for(auto &pr : ccs()) {
      const CCStructure &cs = pr.second;
      for(unsigned int dim = 0; dim <= cs.maxDimension(); dim++)
        allCells.insert(cs.cellsOfDimension(dim).begin(), cs.cellsOfDimension(dim).end());
    }

    // Now erase hanging attributes or ones with default values
    CCIndexData defaultVal;
    for(auto iter = indexAttr().begin(); iter != indexAttr().end();)
      if(allCells.find(iter->first) == allCells.end() or iter->second == defaultVal)
        indexAttr().erase((*iter++).first);
      else
        ++iter;


    // Write cell complexes
    for(auto iter = ccs().begin() ; iter != ccs().end() ; iter++) {
      // Write name
      writeFile(file, iter->first);

      // Write complex
      writeCCStructure(file,iter->second);
    }

    return true;
  }

  bool Mesh::writeCCIndex(QIODevice &file, CCIndex index)
  {
    uint writeId = index.value;
    writeFile(file, writeId);
    return true;
  }

  // CC writing helper
  struct WriteCCStructureVisitor : public boost::static_visitor<bool>
  {
    Mesh &mesh;
    QIODevice &file;
    WriteCCStructureVisitor(Mesh &_mesh, QIODevice &_file)
      : mesh(_mesh), file(_file) {}

    bool operator()(const ccf::CCStructure& cs) {
      // write maxDimension
      uint maxDim = cs.maxDimension();
      writeFile(file, maxDim);

      // write cells at each dimension
      for(uint d = 0 ; d <= maxDim ; d++) {
	std::vector<CCIndex> dimCells = cs.cellsOfDimension(d);
	uint numCells = dimCells.size();
	writeFile(file, numCells);
	for(std::vector<CCIndex>::iterator iter = dimCells.begin() ;
	    iter != dimCells.end() ; iter++)
	  mesh.writeCCIndex(file, *iter);
      }

      // write flip table
      uint numFlips = cs.flips.size();
      writeFile(file, numFlips);
      for(auto &flip : cs.flips.flipVec) {
	mesh.writeCCIndex(file, flip.joint);
	mesh.writeCCIndex(file, flip.facet[0]);
	mesh.writeCCIndex(file, flip.facet[1]);
	mesh.writeCCIndex(file, flip.interior);
      }

      return true;
    }
  };

  bool Mesh::writeCCStructure(QIODevice &file, const CCStructure &cs)
  {
    // write CC type
    QString ccType = cs.csType();
    writeFile(file, ccType);

    // write individual CC based on type
    WriteCCStructureVisitor visitor(*this,file);
    return cs.varCC.apply_visitor(visitor);
  }

  bool Mesh::readCCStructures(QIODevice &file)
  {
    // clear existing cell complexes
    ccs().clear();

    // read number of complexes
    size_t numComplexes;
    readFile(file, numComplexes);

    uint maxIndex = 0;
    uint indexCount = 0;
    if(numComplexes > 0) {
      // read in all complexes
      for(uint cidx = 0 ; cidx < numComplexes ; cidx++) {
        // Read name
        QString name;
        readFile(file,name);

        // Create and read complex with name
        CCStructure &cs = ccs()[name];
        readCCStructure(file, cs);

        for(int i = 0; i <= cs.maxDimension(); i++)
          for(CCIndex c : cs.cellsOfDimension(i)) {
            if(c.value > maxIndex)
              maxIndex = c.value;
            indexCount++;
          }
        
        mdxInfo << QString("Read cell complex: vertices %1, edges %2, faces %3, volumes %4")
          .arg(cs.vertices().size()).arg(cs.edges().size()).arg(cs.faces().size()).arg(cs.volumes().size()) << endl;
        updateAll(name);
      }
      CCIndexFactory.increaseHighestIndex(maxIndex);
      mdxInfo << QString("Read cell %1 cell complexes, with %2 n-cells, max index %3.")
        .arg(numComplexes).arg(indexCount).arg(maxIndex) << endl;
    }

    return numComplexes > 0;
  }

  // read in index, creating if necessary
  bool Mesh::readCCIndex(QIODevice &file, CCIndex &index)
  {
    // read saved value
    uint uIndex;
    readFile(file, uIndex);
    index = CCIndex(uIndex);
    return true;
  }

  bool Mesh::readCCStructure(QIODevice &file, CCStructure &cs)
  {
#ifdef DEBUG_CC_TIMING
    QTime ccTimer;
    float ccSec = 0;
    ccTimer.start(); 
#endif

// RSS Just using the scalable allocator seems enough
//    size_t tbbSoftLimit = 3;
//    if(scalable_allocation_mode(TBBMALLOC_SET_SOFT_HEAP_LIMIT, 0x40000000 * tbbSoftLimit) != TBBMALLOC_OK)
//      mdxInfo << "TBB soft Limit enabling has failed" << endl;
//    else
//      mdxInfo << "TBB soft Limit enabling set to " << tbbSoftLimit << "Gb" << endl;

    // Flag if we need to read in a CC after the first block
    bool ccfCC = true;

    // switch for original-flavour or variant CCs
    // variants first appear with v2.5
    if(Attributes::majorVersion > 2 || Attributes::minorVersion >= 5)
    {
      // read cc type
      QString ccType;
      readFile(file, ccType);

      if(ccType == ccf::CCStructure::csTypeName)
	ccfCC = true;
      else
	throw(QString("Mesh::read: Unknown cell complex type %1.").arg(ccType));
    }

    // Read in an original CCF complex
    if(ccfCC)
    {
      // read maxDimension
      uint maxDim;
      readFile(file, maxDim);

      // create new ccf::CCStructure of the given dimension
      cs = CCStructure(maxDim);
      ccf::CCStructure &csX = ensureCCF(cs);

      CCIndexVecVec cells(maxDim+1);
      // read in cells at each dimension
      for(uint d = 0 ; d <= maxDim ; d++) {
	uint numCells;
	readFile(file, numCells);
	cells[d].resize(numCells);
	for(uint i = 0 ; i < numCells ; i++)
	  readCCIndex(file, cells[d][i]);
      }
#ifdef DEBUG_CC_TIMING
      ccSec = ccTimer.elapsed()/1000.0;
      mdxInfo << "Read cells time:" << ccSec << " seconds" << endl;
      ccTimer.restart();
#endif

      // read in flip table
      uint numFlips;
      readFile(file, numFlips);
      csX.flips.flipVec.resize(numFlips);
      for(uint i = 0 ; i < numFlips ; i++) {
	CCStructure::FlipI &flip = csX.flips.flipVec[i];
	readCCIndex(file, flip.joint);
	readCCIndex(file, flip.facet[0]);
	readCCIndex(file, flip.facet[1]);
	readCCIndex(file, flip.interior);
      }
#ifdef DEBUG_CC_TIMING
      ccSec = ccTimer.elapsed()/1000.0;
      mdxInfo << "Read flip table time:" << ccSec << " seconds" << endl;
      ccTimer.restart();
#endif

      // Now do the insert, skips the duplicate/orientation checking
      csX.flips.reindex();
#ifdef DEBUG_CC_TIMING
      ccSec = ccTimer.elapsed()/1000.0;
      mdxInfo << "Insertion flip table time:" << ccSec << " seconds" << endl;
      ccTimer.restart();
#endif
      csX.dimensionMap.load(cells);

#ifdef DEBUG_CC_TIMING
      ccSec = ccTimer.elapsed()/1000.0;
      mdxInfo << "Insertion dimension map time:" << ccSec << " seconds" << endl;
      ccTimer.restart();

      mdxInfo << "FlipVec size: " << csX.flips.flipVec.size() << endl;
      mdxInfo << "FlipMap size: " << csX.flips.flipMap.size() << endl;

      std::vector<CCIndex> temp;
      mdxInfo << "Vector size: " << sizeof(temp) << endl;
      temp.push_back(CCIndex());
      mdxInfo << "Vector size: " << sizeof(temp) << endl;
#endif
    }

    return true;
  }

  // Access and creation
  QStringList Mesh::ccNames(void) const
  {
    QStringList list;
		for(auto const &pr : _ccs)
			list << pr.first;
    return list;
  }

  // Clear OpenGL render buffers, must be called from GUI
  void Mesh::clearBuffers(void)
  {
    for(auto &name : ccNames())
      drawParms(name).clearBuffers();
  }

  CCStructure& Mesh::ccStructure(const QString &name)
  {
    if(name.isEmpty())
      throw(QString("Mesh::ccStructure Cannot have cell complex with empty name"));

    if(ccs().count(name) == 0) {
      // Create complex 
      ccs()[name];
      // Default processes for rendering and deletion
      ccAttr(name, "RenderProcess") = QString("Mesh/System/Render");
      ccAttr(name, "DeleteProcess") = QString("Mesh/System/Delete Selection");

      updateAll(name);
    }

    return ccs()[name];
  }

  bool Mesh::exists(const QString &name)
  {
    return ccs().count(name) > 0;
  }

  void Mesh::erase(const QString &name)
  {
    if(ccName() == name)
      setCCName(QString());
    drawParmsAttr().erase(name);
    attributes().erase(QString("#CC#%1").arg(name));
    ccs().erase(name);
  }

  void Mesh::refreshNames(void)
  {
    // If no complex selected, set it to the first in the list
    if(ccName().isEmpty() and ccNames().size() > 0)
      setCCName(ccNames()[0]);
    emit refreshWidget(ccName());
  }

  // Methods for selection
  
  // Clear selection
  bool Mesh::clearSelectVertices(const QString &ccName)
  {
    CCStructure &cs = ccs()[ccName];
    std::vector<CCIndex> const &vertices = cs.vertices();
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); i++) {
      CCIndex v = vertices[i];
      CCIndexData &vIdx = indexAttr()[v];
      if(vIdx.selected) {
        vIdx.selected = false;
        cdp.vertexChanged.push_back(v);
      }
    }
    if(cdp.vertexChanged.size() > 0) {
      updateVertexSelect(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Remove points from selection
  bool Mesh::removeSelectVertices(const QString &ccName, const std::vector<CCIndex> &vVec)
  {
    CCDrawParms &cdp = drawParms(ccName);

    for(uint i = 0; i < vVec.size(); i++) {
      CCIndex v = vVec[i];
      CCIndexData &vIdx = indexAttr()[v];
      if(vIdx.selected) {
        vIdx.selected = false;
        cdp.vertexChanged.push_back(v);
      }
    }
    if(cdp.vertexChanged.size() > 0) {
      updateVertexSelect(ccName); 
      emit updateCCRequired();
      return true;
    }

    return false;
  }

  // Add points to selection
  bool Mesh::addSelectVertices(const QString &ccName, const std::vector<CCIndex> &vVec)
  {
    CCDrawParms &cdp = drawParms(ccName);

    for(uint i = 0; i < vVec.size(); i++) {
      CCIndex v = vVec[i];
      CCIndexData &vIdx = indexAttr()[v];
      if(!vIdx.selected) {
        vIdx.selected = true;
        cdp.vertexChanged.push_back(v);
      }
    }
    if(cdp.vertexChanged.size() > 0) {
      updateVertexSelect(ccName); 
      emit updateCCRequired();
      return true;
    }
    return false;

    // Print some useful info
//    if(selectedVec.size() == 1) {
//      CCIndexData &V = selectedVec[0]->*indexAttr();
//      Information::setStatus(QString("Vertex label: %1").arg(V.label));
//    } else if(selectedVec.size() == 2) {
//      CCIndexData &V0 = selectedVec[0]->*indexAttr();
//      CCIndexData &V1 = selectedVec[1]->*indexAttr();
//      Information::setStatus(QString("Distance between vertices: %1um").arg((V0.pos - V1.pos).norm())); 
//    } else
//      Information::setStatus(QString("%1 vertex(es) selected").arg(selectedVec.size()));
  }

  // Clear selection
  bool Mesh::clearSelectFaces(const QString &ccName)
  {
    CCStructure &cs = ccs()[ccName];
    const CCIndexVec &faces = cs.faces();
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(fIdx.selected) {
        fIdx.selected = false;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceSelect(ccName); 
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Remove faces from selection
  bool Mesh::removeSelectFaces(const QString &ccName, const std::vector<CCIndex> &fVec)
  {
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < fVec.size(); i++) {
      CCIndex f = fVec[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(fIdx.selected) {
        fIdx.selected = false;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceSelect(ccName); 
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Add faces to selection
  bool Mesh::addSelectFaces(const QString &ccName, const std::vector<CCIndex> &fVec)
  {
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < fVec.size(); i++) {
      CCIndex f = fVec[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(!fIdx.selected) {
        fIdx.selected = true;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceSelect(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Add a label to the selection
  bool Mesh::addSelectLabel(const QString &ccName, int label)
  {
    CCStructure &cs = ccs()[ccName];
    std::vector<CCIndex> const &faces = cs.cellsOfDimension(2);
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(!fIdx.selected and getLabel(fIdx.label) == getLabel(label)) {
        fIdx.selected = true;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceSelect(ccName); 
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Remove a label from the selection
  bool Mesh::removeSelectLabel(const QString &ccName, int label)
  {
    CCStructure &cs = ccs()[ccName];
    std::vector<CCIndex> const &faces = cs.cellsOfDimension(2);
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(fIdx.selected and getLabel(fIdx.label) == getLabel(label)) {
        fIdx.selected = false;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceSelect(ccName); 
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Update label on faces
  bool Mesh::setFaceLabel(const QString &ccName, int label, const CCIndexVec &fVec)
  {
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < fVec.size(); i++) {
      CCIndex f = fVec[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(fIdx.label != label) {
        fIdx.label = label;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceLabel(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Update label on faces that match a label list
  bool Mesh::fillLabel(const QString &ccName, int label, IntSet labels)
  {
    CCDrawParms &cdp = drawParms(ccName);
    CCStructure &cs = ccStructure(ccName);
    const CCIndexVec &faces = cs.faces();
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i];
      CCIndexData &fIdx = indexAttr()[f];
      if(labels.count(fIdx.label) > 0 and fIdx.label != label) {
        fIdx.label = label;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceLabel(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Update label on faces that match a label list
  bool Mesh::fillSelect(const QString &ccName, int label)
  {
    CCDrawParms &cdp = drawParms(ccName);
    CCStructure &cs = ccStructure(ccName);
    const CCIndexVec &faces = cs.faces();
    #pragma omp parallel for
    for(uint i = 0; i < faces.size(); i++) {
      CCIndex f = faces[i];
      CCIndexData &fIdx = indexAttr()[f];
      // RSS if parents is on, we should just change the parent label
      //   parents[fIdx.label] = label;
      if(fIdx.selected and fIdx.label != label) {
        fIdx.label = label;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceLabel(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Update signal on faces
  bool Mesh::setFaceSignal(const QString &ccName, CCIndexDoubleAttr &signalAttr, double signal, const CCIndexVec &fVec)
  {
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < fVec.size(); i++) {
      CCIndex f = fVec[i];
      auto &fSignal = signalAttr[f];
      if(fSignal != signal) {
        fSignal = signal;
        cdp.faceChanged.push_back(f);
      }
    }
    if(cdp.faceChanged.size() > 0) {
      updateFaceLabel(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  bool Mesh::selectConnectedFaces(const QString &ccName, CCIndex f, bool select)
  {
    CCDrawParms &cdp = drawParms(ccName);
    CCStructure &cs = ccStructure(ccName);

    CCIndexSet faces;
    faces.insert(f);
    if(getConnectedSurface(cs, faces)) {
      for(CCIndex c : faces) {
        auto &cIdx = indexAttr()[c];
        if(cIdx.selected != select) {
          cIdx.selected = select;
          cdp.faceChanged.push_back(c);
        }
      }
      if(cdp.faceChanged.size() > 0) {
        updateFaceSelect();
        emit updateCCRequired();
        return true;
      }
    }
    return false;
  }

  // Selection for volumes
 
  // Add volumes to selection
  bool Mesh::addSelectVolumes(const QString &ccName, const std::vector<CCIndex> &lVec)
  {
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < lVec.size(); i++) {
      CCIndex l = lVec[i];
      CCIndexData &lIdx = indexAttr()[l];
      if(!lIdx.selected) {
        lIdx.selected = true;
        cdp.volumeChanged.push_back(l);
      }
    }
    if(cdp.volumeChanged.size() > 0) {
      updateVolumeSelect(ccName);
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Remove faces from selection
  bool Mesh::removeSelectVolumes(const QString &ccName, const std::vector<CCIndex> &lVec)
  {
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < lVec.size(); i++) {
      CCIndex l = lVec[i];
      CCIndexData &fIdx = indexAttr()[l];
      if(fIdx.selected) {
        fIdx.selected = false;
        cdp.volumeChanged.push_back(l);
      }
    }
    if(cdp.volumeChanged.size() > 0) {
      updateVolumeSelect(ccName); 
      emit updateCCRequired();
      return true;
    }
    return false;
  }

  // Clear volume selection
  bool Mesh::clearSelectVolumes(const QString &ccName)
  {
    CCStructure &cs = ccs()[ccName];
    const CCIndexVec &volumes = cs.volumes();
    CCDrawParms &cdp = drawParms(ccName);
    #pragma omp parallel for
    for(uint i = 0; i < volumes.size(); i++) {
      CCIndex l = volumes[i];
      CCIndexData &lIdx = indexAttr()[l];
      if(lIdx.selected) {
        lIdx.selected = false;
        cdp.volumeChanged.push_back(l);
      }
    }
    if(cdp.volumeChanged.size() > 0) {
      updateFaceSelect(ccName); 
      emit updateCCRequired();
      return true;
    }
    return false;
  }


  // Data associated with every CCIndex
  CCIndexDataAttr &Mesh::indexAttr(void)
  {
    if(!_indexAttr)
      _indexAttr = &attributes().attrMap<CCIndex,CCIndexData>("CCIndexData");
    return *_indexAttr;
  }
  // Data associated will every CCSignedIndex
  CCSignedIndexDataAttr &Mesh::signedIndexAttr(void)
  {
    return attributes().attrMap<CCSignedIndex,CCIndexData>("CCSignedIndexData");
  }
  // Data associated with every CCStructure
  QString &Mesh::ccAttr(const QString &ccName, const QString &propName)
  {
    return attributes().attrMap<QString, QString>(QString("#CC#%1").arg(ccName))[propName];
  }

  bool Mesh::read(QIODevice &file, bool transform)
  {
    // Read the header
    char magic[5];
    file.read(magic, 5);
    if(strncmp(magic, "MDXM", 4) and strncmp(magic, "MGXM", 4))
      throw(QString("Mesh::read: Error, bad header"));

    // Get the revision numbers, these will change when the structure changes
    QList<int> version = extractVersion(file);
    Attributes::majorVersion = version[0];
    Attributes::minorVersion= version[1];
    if(Attributes::majorVersion < 2 or Attributes::minorVersion < 2)
      throw(QString("readCellMesh: Error, bad mesh version %1.%2").arg(Attributes::majorVersion).arg(Attributes::minorVersion));
  
    // Start progress bar, we'll allow cancel so to catch errors and clean up
    progressStart(QString("Reading Mesh %1").arg(userId()), 0, true);
    try {
      // Read in the mesh type
      QString mType;
      readFile(file, mType);

      // Should not have an old VV mesh
      int tmpInt = 0;
      size_t tmpSize = 0;
      readFile(file, tmpInt);
      if(tmpInt > 0)
        throw(QString("Mesh has old graph data"));
      readFile(file, tmpInt);
      if(tmpInt > 0)
        throw(QString("Mesh has old graph data"));
      readFile(file, tmpSize);
      if(tmpSize > 0)
        throw(QString("Mesh has old graph data"));
      readFile(file, tmpInt);
      if(tmpInt > 0)
        throw(QString("Mesh has old graph data"));
      readFile(file, tmpInt);
      if(tmpInt > 0)
        throw(QString("Mesh has old graph data"));

      // Read the cell complexes
      readCCStructures(file);
      // Read the attributes
      attributes().read(file);
			// Trigger indexAttr parse
			indexAttr();
      // Labeling is stored in the attributes
      setLabeling(labeling());
      updateAll();

      // Get the image texture if there is one (i.e. keyence mesh)
      if(hasImgTex()) {
        QByteArray ba =
          attributes().attrMap<QString, QByteArray>("MeshQByteArray")["ImageTex"];
        if(ba.size() == 0)
          clearImgTex();
        else {
          QImage image;
          image.loadFromData(ba);
          mdxInfo << QString("Loaded image texture size %1 x %2.")
                                                .arg(image.width()).arg(image.height()) << endl;
          if(image.width() * image.height() > 0)
            setImgTex(image);
          else
            clearImgTex();
        } 
      }

    } catch(const QString &s) {
      reset();
      throw(s);
    } catch(UserCancelException &e) { 
      reset();
      throw(e);
    } catch(...) {
      reset();
      throw("Unknown exception loading mesh");
    }
    return true;
  }

  bool Mesh::write(QIODevice &file, bool transform)
  {
    // Start progress bar, we'll allow cancel so to catch errors and clean up
    progressStart(QString("Saving Mesh %1").arg(userId()), 0, true);
    mdxInfo << QString("Saving Mesh %1") .arg(userId()) << endl;

    try {
      // Write the header
      file.write("MDXM 2.5 ", 9);

      // Write the mesh type
      writeFile(file, QString("MDXM"));

      // Write zeros for counts for the old VV graphs
      int tmpInt = 0;
      size_t tmpSize = 0;
      writeFile(file, tmpInt);
      writeFile(file, tmpInt);
      writeFile(file, tmpSize);
      writeFile(file, tmpInt);
      writeFile(file, tmpInt);

      // Write out the cell complexes
      writeCCStructures(file);

      // Add any attributes which are not live
      if(hasImgTex()) {
        QByteArray ba;
        QBuffer buffer(&ba);
        buffer.open(QIODevice::WriteOnly);
        imgTex().save(&buffer, "TIFF");  
        attributes().attrMap<QString, QByteArray>("MeshQByteArray")["ImageTex"] = ba; 
      }

      // Now write the attributes
      attributes().write(file);

    } catch(const QString &s) {
      // try to remove half-written file
      file.close();
      file.open(QIODevice::Truncate);
      // also need to restore All graph for 3D cells
      file.close();
      throw(s);
    } catch(UserCancelException &e) {
      file.close();
      file.open(QIODevice::Truncate);
      throw(e);
    } catch(...) {
      file.close();
      file.open(QIODevice::Truncate);
      throw("Unknown exception writing mesh");
    }
    return true;
  }

  void Mesh::updateCCRequired()
  {
    ccUpdateTimer.start();
  }
  void Mesh::updateCC()
  {
    emit updateCCData(id());
  }
}
