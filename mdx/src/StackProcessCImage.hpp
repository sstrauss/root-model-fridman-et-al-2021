//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef STACK_PROCESS_CIMAGE_HPP
#define STACK_PROCESS_CIMAGE_HPP

#include <Process.hpp>

namespace mdx 
{
  ///\addtogroup Process
  ///@{
  
  /**
   * \class CImgGaussianBlurStack StackProcessCImage.hpp <StackProcessCImage.hpp>
   *
   * CImg implementation of the gaussian blur
   */
  class mdxBase_EXPORT CImgGaussianBlurStack : public Process 
  {
  public:
    CImgGaussianBlurStack(const Process& process) : Process(process) 
    {
      setName("Stack/CImage/Gaussian Blur");
      setDesc("CImage Gaussian Blur");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Radius", "Radius for Gaussian blur in voxels", "5"); 
    }

    bool run()
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output, parm("Radius").toUInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, uint radius);
  };
	
  /**
   * \class CImgMedianBlurStack StackProcessCImage.hpp <StackProcessCImage.hpp>
   *
   * CImg implementation of the median blur
   */
  class mdxBase_EXPORT CImgMedianBlurStack : public Process 
  {
  public:
    CImgMedianBlurStack(const Process& process) : Process(process) 
    {
      setName("Stack/CImage/Median Blur");
      setDesc("CImage Median Blur");
      setIcon(QIcon(":/images/Blur.png"));

      addParm("Radius", "Size of the median filter in voxels", "3"); 
    }
  
    bool run(const QStringList &parms)
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output, parms[0].toUInt());
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output, uint radius);
  };

  /**
   * \class CImgLaplaceStack StackProcessCImage.hpp <StackProcessCImage.hpp>
   *
   * Compute the Laplacian of the stack (using CImg).
   */
  class mdxBase_EXPORT CImgLaplaceStack : public Process 
  {
  public:
    CImgLaplaceStack(const Process& process) : Process(process)
    {
      setName("Stack/CImage/Laplace Transform");
      setDesc("CImage Laplace transform of stack");
      setIcon(QIcon(":/images/Laplace.png"));
    }
  
    bool run(const QStringList &parms)
    {
      Stack* s = currentStack();
      Store* input = s->currentStore();
      Store* output = s->work();
      bool res = run(input, output);
      if(res) {
        input->hide();
        output->show();
      }
      return res;
    }
    bool run(const Store* input, Store* output);
  };

  ///@}
}

#endif 
