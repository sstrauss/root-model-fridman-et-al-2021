//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <ImageData.hpp>

#include <ClipRegion.hpp>
#include <CutSurf.hpp>
#include <Dir.hpp>
#include <Geometry.hpp>
#include <Information.hpp>
#include <Mesh.hpp>
#include <Misc.hpp>
#include <Progress.hpp>
#include <Vector.hpp>
#include <ColorMap.hpp>
#include <TransferFunctionDlg.hpp>

#include <limits>
#include <cmath>

namespace mdx 
{
  typedef Vector<16, double> Point16d;
  
  //using namespace std;
  
  // Static class varaibles
  int ImgData::VoxelEditRadius = 25;
  int ImgData::VoxelEditMaxPix = 0;
  int ImgData::ClearColor = 0;
  uint ImgData::Slices = 250u;
  uint ImgData::TileCount = 20u;
  Point3u ImgData::MaxTexSize = Point3u(2048u, 2048u, 2048u);
  double ImgData::SurfOffset = .01f;
  bool ImgData::SeedStack = false;
  
  double ImgData::DrawNormals = 0.0f;
  double ImgData::DrawZeroLabels = 0.0f;
  double ImgData::DrawNhbds = 0.0f;
  double ImgData::DrawOffset = 2.0f;
  bool ImgData::DeleteBadVertex = false;
  bool ImgData::FillWorkData = false;
  std::vector<Colorf> ImgData::LabelColors; // = std::vector<Point3d>(1);
  
  bool ImgData::MeshSelect;
  ScaleBar ImgData::scaleBar;
  Colorbar ImgData::colorBar;
  
  double ImgData::MeshPointSize = 1.0f;
  double ImgData::MeshLineWidth = 1.0f;
  
  #ifdef WIN32
  #  define FileDialogOptions QFileDialog::DontUseNativeDialog
  #else
  const int FileDialogOptions = 0;
  #endif
  
  ImgData::ImgData(int id, QWidget* _parent)
    : StackId(id), Main16Bit(false), Work16Bit(false), 
      MainClipVoxels(true), WorkClipVoxels(true), MainClipLabels(false), WorkClipLabels(false), MainShading(false), WorkShading(false),
      triangleColorEditDlg(0), newMainColorMap(true), newWorkColorMap(true), dataTexColor(0), colorMapTexId(0),
      imgTexId(0), mcmapTexId(0), wcmapTexId(0), labelTexId(0),
      selFboId(0), selFboColor(0), selFboDepth(0),
      texMap(0), parent(_parent), min(0), max(1),
      labelWallBordMin(0), labelWallBordMax(1), pixelRadius(1),
      meshShift(0), marchData(0), changed(false), pixelsChanged(false)
  {
    parent = _parent;
    workTransferDlg = new TransferFunctionDlg(parent);
    mainTransferDlg = new TransferFunctionDlg(parent);
    dataTexIds["Main"] = dataTexIds["Work"] = 0;
    labelCenterTexIds["Main"] = labelCenterTexIds["Work"] = 0;
  }
  
  void ImgData::init(Stack* s, Mesh* m)
  {
    stack = s;
    mesh = m;
    mainTransferDlg->setDefaultTransferFunction(stack->main()->transferFct());
    workTransferDlg->setDefaultTransferFunction(stack->work()->transferFct());
    updateMainColorMap();
    updateWorkColorMap();
  }
  
  ImgData::~ImgData()
  {
    // Delete textures
    if(dataTexIds["Main"])
      glfuncs->glDeleteTextures(1, &dataTexIds["Main"]);
    if(dataTexIds["Work"])
      glfuncs->glDeleteTextures(1, &dataTexIds["Work"]);
    if(colorMapTexId)
      glfuncs->glDeleteTextures(1, &colorMapTexId);
    if(imgTexId)
      glfuncs->glDeleteTextures(1, &imgTexId);
  
    delete mainTransferDlg;
    delete workTransferDlg;
  }
  
  // Set parameters for function
  void ImgData::readParms(Parms& parms, QString section)
  {
    Section = section;
  
    QString StackFile, WorkStackFile, MeshFile, ParentFile;
    parms(section, "StackFile", StackFile, QString(""));
    if(!StackFile.isEmpty())
      StackFile = absoluteFilePath(StackFile);
    stack->main()->setFile(StackFile);
    parms(section, "WorkStackFile", WorkStackFile, QString(""));
    if(!WorkStackFile.isEmpty())
      WorkStackFile = absoluteFilePath(WorkStackFile);
    stack->work()->setFile(WorkStackFile);
    parms(section, "MeshFile", MeshFile, QString(""));
    if(!MeshFile.isEmpty())
      MeshFile = absoluteFilePath(MeshFile);
    mesh->setFile(MeshFile);

    bool MainShow;
    parms(section, "MainShow", MainShow, true);
    if(MainShow)
      stack->main()->show();
    else
      stack->main()->hide();
    double MainBright;
    parms(section, "MainBright", MainBright, 1.0);
    stack->main()->setBrightness(MainBright);
    double MainOpacity;
    parms(section, "MainOpacity", MainOpacity, 0.8);
    stack->main()->setOpacity(MainOpacity);
    parms(section, "Main16Bit", Main16Bit, false);
    parms(section, "MainClipVoxels", MainClipVoxels, true);
    parms(section, "MainClipLabels", MainClipLabels, false);
    parms(section, "MainShading", MainShading, false);
    bool MainLabels;
    parms(section, "MainLabels", MainLabels, false);
    if(MainLabels)
      Main16Bit = true;
    stack->main()->setLabels(MainLabels);
  
    bool WorkShow;
    parms(section, "WorkShow", WorkShow, false);
    if(WorkShow)
      stack->work()->show();
    else
      stack->work()->hide();
    double WorkBright;
    parms(section, "WorkBright", WorkBright, 1.0);
    stack->work()->setBrightness(WorkBright);
    double WorkOpacity;
    parms(section, "WorkOpacity", WorkOpacity, 0.8);
    stack->work()->setOpacity(WorkOpacity);
    parms(section, "Work16Bit", Work16Bit, false);
    parms(section, "WorkClipVoxels", WorkClipVoxels, true);
    parms(section, "WorkClipLabels", WorkClipLabels, false);
    parms(section, "WorkShading", WorkShading, false);
    bool WorkLabels;
    parms(section, "WorkLabels", WorkLabels, false);
    if(WorkLabels)
      Work16Bit = true;
    stack->work()->setLabels(WorkLabels);
  
    if(WorkStackFile == StackFile) {
      stack->work()->setFile(QString());
      if(WorkShow) {
        stack->main()->show();
        stack->work()->hide();
      }
    }
  
    double SurfBright;
    parms(section, "SurfBright", SurfBright, 1.0);
    mesh->setBrightness(SurfBright);
    double SurfOpacity;
    parms(section, "SurfOpacity", SurfOpacity, 1.0);
    mesh->setOpacity(SurfOpacity);
    bool SurfCull, SurfBlend, SurfShade;
    parms(section, "SurfCull", SurfCull, true);
    mesh->setCulling(SurfCull);
    parms(section, "SurfBlend", SurfBlend, false);
    mesh->setBlending(SurfBlend);
    parms(section, "SurfShade", SurfShade, true);
    mesh->setShading(SurfShade);

    bool ShowTrans, ShowBBox;
    Point3d Scale = Point3d(1.0, 1.0, 1.0);
    // double Scale;
    parms(section, "ShowTrans", ShowTrans, false);
    stack->setShowTrans(ShowTrans);
    parms(section, "ShowBBox", ShowBBox, false);
    stack->setShowBBox(ShowBBox);
    // Show scaling is turned off by default
    parms(section, "ScaleX", Scale.x(), 1.0);
    parms(section, "ScaleY", Scale.y(), 1.0);
    parms(section, "ScaleZ", Scale.z(), 1.0);

    bool LoadMeshScaleUm, LoadMeshTransform;
    parms(section, "LoadMeshScaleUm", LoadMeshScaleUm, true);
    mesh->setScaled(LoadMeshScaleUm);
    parms(section, "LoadMeshTransform", LoadMeshTransform, false);
    mesh->setTransformed(LoadMeshTransform);
    parms(section, "MeshShift", meshShift, 0.0);

    // Cell complexes
    QString ccName;
    parms(section, "CCName", ccName, QString());
    mesh->setCCName(ccName);

    QString sf;
    parms(section, "MainColorMap", sf, QString());
    TransferFunction mainTransferFct = stack->main()->transferFct();
    mainTransferFct.clear();
    if(not sf.isEmpty()) {
      QString qsf = unshield(sf);
      mainTransferFct = TransferFunction::load(qsf);
    }
    if(mainTransferFct.empty())
      mainTransferFct = TransferFunction::scaleGreen();
    mainTransferDlg->setTransferFunction(mainTransferFct);
    stack->main()->setTransferFct(mainTransferFct);
    updateMainColorMap();
  
    QString sf2;
    parms(section, "WorkColorMap", sf2, QString());
    TransferFunction workTransferFct = stack->work()->transferFct();
    workTransferFct.clear();
    if(not sf2.isEmpty()) {
      QString qsf = unshield(sf2);
      workTransferFct = TransferFunction::load(qsf);
    }
    if(workTransferFct.empty())
      workTransferFct = TransferFunction::scaleCyan();
    workTransferDlg->setTransferFunction(workTransferFct);
    stack->work()->setTransferFct(workTransferFct);
    updateWorkColorMap();
  
    Matrix4d m = 1;
    parms(section, "Frame", m, m);
    stack->frame().setFromMatrix(m.c_data());
    m = 1;
    parms(section, "Trans", m, m);
    stack->trans().setFromMatrix(m.c_data());
  
    if(section == "Stack1") {
      MeshColor = Colors::Mesh1Color;
      MeshBorderColor = Colors::Mesh1BorderColor;
      // MeshPointColor = Colors::Mesh1PointColor;
      MeshSelectColor = Colors::Mesh1SelectColor;
    } else {
      MeshColor = Colors::Mesh2Color;
      MeshBorderColor = Colors::Mesh2BorderColor;
      // MeshPointColor = Colors::Mesh2PointColor;
      MeshSelectColor = Colors::Mesh2SelectColor;
    }
  }
  
  void ImgData::setWorkColorMap(const TransferFunction& fct) {
    workTransferDlg->setTransferFunction(fct);
  }
  
  void ImgData::setMainColorMap(const TransferFunction& fct) {
    mainTransferDlg->setTransferFunction(fct);
  }
  
  // Set parameters for function
  void ImgData::writeParms(QTextStream& pout, QString section)
  {
    Section = section;
  
    pout << endl;
    pout << "[" << section << "]" << endl;
  
    pout << "StackFile: " << stripCurrentDir(stack->main()->file()) << endl;
    pout << "WorkStackFile: " << stripCurrentDir(stack->work()->file()) << endl;
    pout << "MeshFile: " << stripCurrentDir(mesh->file()) << endl;
  
    pout << "MainShow: " << (stack->main()->isVisible() ? "true" : "false") << endl;
    pout << "MainBright:" << stack->main()->brightness() << endl;
    pout << "MainOpacity:" << stack->main()->opacity() << endl;
    pout << "Main16Bit:" << (Main16Bit ? "true" : "false") << endl;
    pout << "MainClipVoxels:" << (MainClipVoxels ? "true" : "false") << endl;
    pout << "MainClipLabels:" << (MainClipLabels ? "true" : "false") << endl;
    pout << "MainShading:" << (MainShading ? "true" : "false") << endl;
    pout << "MainLabels:" << (stack->main()->labels() ? "true" : "false") << endl;
  
    pout << "WorkShow: " << (stack->work()->isVisible() ? "true" : "false") << endl;
    pout << "WorkBright:" << stack->work()->brightness() << endl;
    pout << "WorkOpacity:" << stack->work()->opacity() << endl;
    pout << "Work16Bit:" << (Work16Bit ? "true" : "false") << endl;
    pout << "WorkClipVoxels:" << (WorkClipVoxels ? "true" : "false") << endl;
    pout << "WorkClipLabels:" << (WorkClipLabels ? "true" : "false") << endl;
    pout << "WorkShading:" << (WorkShading ? "true" : "false") << endl;
    pout << "WorkLabels:" << (stack->work()->labels() ? "true" : "false") << endl;
  
    pout << "SurfBright:" << mesh->brightness() << endl;
    pout << "SurfOpacity:" << mesh->opacity() << endl;
    pout << "SurfBlend:" << (mesh->blending() ? "true" : "false") << endl;
    pout << "SurfCull:" << (mesh->culling() ? "true" : "false") << endl;
    pout << "SurfShade:" << (mesh->shading() ? "true" : "false") << endl;
  
    pout << "ShowTrans:" << (stack->showTrans() ? "true" : "false") << endl;
    pout << "ShowBBox:" << (stack->showBBox() ? "true" : "false") << endl;
    pout << "ShowScale:" << (stack->showScale() ? "true" : "false") << endl;
    pout << "TieScales:" << (stack->tieScales() ? "true" : "false") << endl;
    pout << "ScaleX:" << stack->scale().x() << endl;
    pout << "ScaleY:" << stack->scale().y() << endl;
    pout << "ScaleZ:" << stack->scale().z() << endl;
    pout << "Scale:" << norm(stack->scale()) << endl;

    pout << "LoadMeshScaleUm: " << (mesh->scaled() ? "true" : "false") << endl;
    pout << "LoadMeshTransform: " << (mesh->transformed() ? "true" : "false") << endl;
    pout << "MeshShift: " << meshShift << endl;  

    pout << "CCName: " << mesh->ccName() << endl;  

    QString qsf = shield(stack->main()->transferFct().dump());
    pout << "MainColorMap: " << qsf << endl;
    qsf = shield(stack->work()->transferFct().dump());
    pout << "WorkColorMap: " << qsf << endl;
  
    pout << "Frame: " << Point16d(stack->frame().matrix()) << endl;
    pout << "Trans: " << Point16d(stack->trans().matrix()) << endl;
    pout << endl;
  }
  
  // Set stack sizes
  void ImgData::updateStackSize()
  {
    if(stack->empty())
      unloadTex();
  
    emit changeSize(stack->size(), stack->step(), stack->origin());
  }
  
  static void drawPlane(Shader* shader, Point3d imageSize, Point3u texSize, Point3d Origin, double scaling, int Slices,
                        GLboolean hasClip1, GLboolean hasClip2, GLboolean hasClip3, Point3d c1, Point3d c2, Point3d c3, Point3d c4)
  {
    shader->setUniform("origin", GLSLValue(vec3(Origin)));
    shader->setUniform("imageSize", GLSLValue(vec3(imageSize)));
    shader->setUniform("scaling", GLSLValue(float(scaling)));
    shader->setUniform("texSize", GLSLValue(ivec3(texSize)));
    shader->setUniform("slices", GLSLValue(float(Slices) / 10000.f));
    shader->setUniform("hasClipped", GLSLValue(ivec3(hasClip1, hasClip2, hasClip3)));
    shader->useShaders();
    shader->setupUniforms();
  
    glfuncs->glBegin(GL_QUADS);
    glfuncs->glColor4f(1, 1, 1, 1);
    glfuncs->glVertex3dv(c4.c_data());
    glfuncs->glVertex3dv(c3.c_data());
    glfuncs->glVertex3dv(c2.c_data());
    glfuncs->glVertex3dv(c1.c_data());
    glfuncs->glEnd();
  
    shader->stopUsingShaders();
  }
  
  void ImgData::setupVolumeShader(Shader& shader, int pos, bool hasClip)
  {
    auto &main = *stack->main();
    auto &work = *stack->work();

    // Are we using normal clipping planes on both stores? If so we can just clip the ray.
    bool normalClip = true;
    if(hasClip) {
      if(isMainVisible() and (MainClipLabels or !MainClipVoxels))
        normalClip = false;
      if(isWorkVisible() and (WorkClipLabels or !WorkClipVoxels))
        normalClip = false;
    }

    // Is label clipping on?
    bool mainClipLabels = isMainVisible() and main.labels() and MainClipLabels;
    bool workClipLabels = isWorkVisible() and work.labels() and WorkClipLabels;

    // Is voxel clipping required
    bool voxelClip = !normalClip and ((isWorkVisible() and WorkClipVoxels) or (isMainVisible() and MainClipVoxels));

    // Do we need shading? both shading and fancy clipping require center and/or vertex positions calcs
    bool mainShading = isMainVisible() and main.labels() and MainShading;
    bool workShading = isWorkVisible() and work.labels() and WorkShading;

    // Define the color function
    QString shaderCode = "vec4 color(vec3 texCoord)\n"
                         "{\n"
                         "  vec4 c1 = vec4(0,0,0,1);\n"
                         "  vec4 c2 = vec4(0,0,0,1);\n"
                         "  bool vClipped = false;\n"
                         "  bool c1Clipped = false;\n"
                         "  bool c2Clipped = false;\n"
                         "  vec4 vPos;\n"
                         "  vec4 c1Pos;\n"
                         "  vec4 c2Pos;\n"
                         "  int label1 = 0;\n"
                         "  int label2 = 0;\n";

    // Calculate vertex position if required
    if(voxelClip or mainShading or workShading)
      shaderCode +=      "  vPos = texToRealPos(texCoord);\n";
    if(voxelClip)
      shaderCode +=      "  vClipped = isClipped(vPos);\n";

    if(mainShading or mainClipLabels) {
      shaderCode +=      "  label1 = getLabel1(texCoord);\n"
                         "  if(label1 == 0)\n"
                         "    c1Clipped = true;\n"
                         "  else {\n"
                         "    float index = float(label1)/float(labelCount1 - 1);\n"
                         "    c1Pos = texToRealPos((texture1D(labelCenters1, index).rgb * 65535.0)/vec3(texSize));\n";
      if(mainClipLabels)
        shaderCode +=    "    c1Clipped = isClipped(c1Pos);\n";
      shaderCode +=      "  }\n";
    }

    if(workShading or workClipLabels) {
      shaderCode +=      "  label2 = getLabel2(texCoord);\n"
                         "  if(label2 == 0)\n"
                         "    c2Clipped = true;\n"
                         "  else {\n"
                         "    float index = float(label2)/float(labelCount2 - 1);\n"
                         "    c2Pos = texToRealPos((texture1D(labelCenters2, index).rgb * 65535.0)/vec3(texSize));\n";
      if(workClipLabels)
        shaderCode +=    "    c2Clipped = isClipped(c2Pos);\n";
      shaderCode +=      "  }\n";
    }

    if(isMainVisible()) {
      shaderCode +=      "  bool mainClipped = false;\n";
      if(!normalClip) {
        if(MainClipVoxels)
          shaderCode +=  "  if(vClipped) mainClipped = true;\n";
        if(MainClipLabels)
          shaderCode +=  "  if(c1Clipped) mainClipped = true;\n";
        shaderCode +=    "  if(!mainClipped)\n";
      }
      if(main.labels()) {
        if(MainShading)
          shaderCode +=  "    c1 = doShading(vPos, c1Pos) * premulColor(indexColor1(texCoord));\n";
        else
          shaderCode +=  "    c1 = premulColor(indexColor1(texCoord));\n";
      } else
        shaderCode +=    "    c1 = premulColor(colormapColor1(texCoord));\n";
    }
    if(isWorkVisible()) {
      shaderCode +=        "bool workClipped = false;\n";
      if(!normalClip) {
        if(WorkClipVoxels)
          shaderCode +=  "  if(vClipped) workClipped = true;\n";
        if(WorkClipLabels)
          shaderCode +=  "  if(c2Clipped) workClipped = true;\n";
        shaderCode +=    "  if(!workClipped)\n";
      }
      if(work.labels()) {
        if(WorkShading)
          shaderCode +=  "    c2 = doShading(vPos, c2Pos) * premulColor(indexColor2(texCoord));\n";
        else
          shaderCode +=  "    c2 = premulColor(indexColor2(texCoord));\n";
      } else
        shaderCode +=    "    c2 = premulColor(colormapColor2(texCoord));\n";
    }
    shaderCode +=        "  return mixColors(c1, c2);\n"
                         "}\n";

    // Ray clipping function
    shaderCode +=        "void rayClip(inout vec4 rayStart, inout vec4 rayEnd)\n"
                         "{\n";
    if(normalClip)
      shaderCode +=      "  doRayClip(rayStart, rayEnd);\n";
    shaderCode +=        "  return;\n"
                         "}\n";

    shader.changeFragmentShaderCode(pos, shaderCode);
  }

  void ImgData::setup3DRenderingData(Shader* shader)
  {
    // Setup the main textures and uniforms
    if(stack->main()->isVisible() and dataTexIds["Main"]) {
      bind3DTex(dataTexIds["Main"]);
      setupMainColorMap();
      double brightness = powf(stack->main()->brightness(), 2.0f);
      double opacity = powf(stack->main()->opacity(), 2.0f);
      shader->setUniform("brightness", GLSLValue(float(brightness)));
      shader->setUniform("opacity", GLSLValue(float(opacity)));

      if(stack->main()->labels()) {
        setupLabelCenters("Main");
        shader->setUniform("labelCount1", GLSLValue(int(labelCenters["Main"].size())));
      }
    }

    // Setup the work textures and uniforms
    if(stack->work()->isVisible() and dataTexIds["Work"]) {
      bind3DTex(dataTexIds["Work"], Shader::AT_SECOND_TEX3D);
      setupWorkColorMap2();
      double brightness = powf(stack->work()->brightness(), 2.0);
      double opacity = powf(stack->work()->opacity(), 2.0);
      shader->setUniform("secondBrightness", GLSLValue(float(brightness)));
      shader->setUniform("secondOpacity", GLSLValue(float(opacity)));

      if(stack->work()->labels()) {
        setupLabelCenters("Work");
        shader->setUniform("labelCount2", GLSLValue(int(labelCenters["Work"].size())));
      }
    }

    REPORT_GL_ERROR("setup3DRendering");
  }
  
  void ImgData::drawStack(Shader* shader)
  {
    if(!isVisible() or !(dataTexIds["Main"] or dataTexIds["Work"])
       or ((stack->main()->opacity() == 0 or !isMainVisible()) and (stack->work()->opacity() == 0 or !isWorkVisible())))
      return;
  
    glfuncs->glEnable(GL_AUTO_NORMAL);
    glfuncs->glDisable(GL_LIGHTING);
    glfuncs->glDisable(GL_BLEND);
  
    glfuncs->glFrontFace(GL_CCW);
    glfuncs->glPolygonMode(GL_FRONT, GL_FILL);
  
    glfuncs->glDisable(GL_TEXTURE_1D);
    glfuncs->glDisable(GL_TEXTURE_2D);
    glfuncs->glDisable(GL_TEXTURE_3D);
  
    // Choose blending function
    glfuncs->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
    // Set matrix
    glfuncs->glMatrixMode(GL_MODELVIEW);
    glfuncs->glPushMatrix();
    glfuncs->glMultMatrixd(getFrame().worldMatrix());
  
    GLdouble mv[16], proj[16];
    GLint viewport[4];
    glfuncs->glGetDoublev(GL_MODELVIEW_MATRIX, mv);
    glfuncs->glGetDoublev(GL_PROJECTION_MATRIX, proj);
    glfuncs->glGetIntegerv(GL_VIEWPORT, viewport);
  
    // Get unit vectors in x,y,z direction
    Point3d x(mv[0], mv[4], mv[8]);
    Point3d y(mv[1], mv[5], mv[9]);
    Point3d z(mv[2], mv[6], mv[10]);
    // Vertex array for quads
    std::vector<Point3d> vtxVA;
  
    // Render object aligned or view aligned
    const Point3d& Size = stack->worldSize();
    double x2 = Size.x() * Size.x() / 4;
    double y2 = Size.y() * Size.y() / 4;
    double z2 = Size.z() * Size.z() / 4;
    double dis = sqrt(x2 + y2 + z2);
    z *= dis;
    x *= dis;
    y *= dis;
  
    Shader::activeTexture(Shader::AT_LABEL_TEX);
    glfuncs->glBindTexture(GL_TEXTURE_1D, labelTexId);
    Shader::activeTexture(Shader::AT_NONE);

    GLdouble winX = viewport[2];
    GLdouble winY = viewport[3];
  
    typedef Vector<3, GLdouble> Point3GLd;
    Point3GLd c1, c2, c3, c4;
    gluUnProject(0, 0, 0, mv, proj, viewport, &c1.x(), &c1.y(), &c1.z());
    gluUnProject(0, winY, 0, mv, proj, viewport, &c2.x(), &c2.y(), &c2.z());
    gluUnProject(winX, winY, 0, mv, proj, viewport, &c3.x(), &c3.y(), &c3.z());
    gluUnProject(winX, 0, 0, mv, proj, viewport, &c4.x(), &c4.y(), &c4.z());
  
    GLboolean clip1, clip2, clip3;
    glfuncs->glGetBooleanv(GL_CLIP_PLANE0, &clip1);
    glfuncs->glGetBooleanv(GL_CLIP_PLANE2, &clip2);
    glfuncs->glGetBooleanv(GL_CLIP_PLANE4, &clip3);
  
    setup3DRenderingData(shader);
  
    drawPlane(shader, Size, stack->size(), stack->origin(), stack->scale().x(),   // ALR: not sure what to do here
              Slices, clip1, clip2, clip3, Point3d(c1), Point3d(c2), Point3d(c3), Point3d(c4));
  
    // drawPlane(shader, Size, stack->size(), stack->origin(), stack->scale(),
    //          Slices,
    //          clip1, clip2, clip3,
    //          Point3d(c1), Point3d(c2), Point3d(c3), Point3d(c4));
    unbind3DTex();
  
    // glfuncs->glBlendEquation(GL_FUNC_ADD);
    glfuncs->glDisable(GL_BLEND);
    glfuncs->glMatrixMode(GL_MODELVIEW);
    glfuncs->glPopMatrix();
    REPORT_GL_ERROR("drawStack");
  }
  
  void ImgData::drawBBox()
  {
    if(!stack->showBBox() or !(dataTexIds["Main"] or dataTexIds["Work"]))
      return;
    glfuncs->glDisable(GL_LIGHTING);
    glfuncs->glDisable(GL_TEXTURE_1D);
    glfuncs->glDisable(GL_TEXTURE_2D);
    glfuncs->glDisable(GL_TEXTURE_3D);
    glfuncs->glDisable(GL_BLEND);
    glfuncs->glDisable(GL_ALPHA_TEST);
    glfuncs->glDisable(GL_AUTO_NORMAL);
    glfuncs->glEnable(GL_DEPTH_TEST);
  
    glfuncs->glLineWidth(MeshLineWidth);
  
    glfuncs->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  
    glfuncs->glMatrixMode(GL_MODELVIEW);
    glfuncs->glPushMatrix();
    glfuncs->glMultMatrixd(getFrame().worldMatrix());
  
    Colorf c = Colors::getColor((StackId == 0) ? Colors::Stack1BBoxColor : Colors::Stack2BBoxColor);
  
    glfuncs->glColor4fv(c.c_data());
  
    const Point3d& Origin = stack->origin();
    Point3d c1 = Origin;
    Point3d c2 = Origin + stack->worldSize();
  
    glfuncs->glBegin(GL_QUADS);
    glfuncs->glVertex3d(c1.x(), c1.y(), c1.z());
    glfuncs->glVertex3d(c1.x(), c2.y(), c1.z());
    glfuncs->glVertex3d(c2.x(), c2.y(), c1.z());
    glfuncs->glVertex3d(c2.x(), c1.y(), c1.z());
  
    glfuncs->glVertex3d(c1.x(), c1.y(), c2.z());
    glfuncs->glVertex3d(c1.x(), c2.y(), c2.z());
    glfuncs->glVertex3d(c2.x(), c2.y(), c2.z());
    glfuncs->glVertex3d(c2.x(), c1.y(), c2.z());
    glfuncs->glEnd();
  
    glfuncs->glBegin(GL_LINES);
    glfuncs->glVertex3d(c1.x(), c1.y(), c1.z());
    glfuncs->glVertex3d(c1.x(), c1.y(), c2.z());
  
    glfuncs->glVertex3d(c1.x(), c2.y(), c1.z());
    glfuncs->glVertex3d(c1.x(), c2.y(), c2.z());
  
    glfuncs->glVertex3d(c2.x(), c2.y(), c1.z());
    glfuncs->glVertex3d(c2.x(), c2.y(), c2.z());
  
    glfuncs->glVertex3d(c2.x(), c1.y(), c1.z());
    glfuncs->glVertex3d(c2.x(), c1.y(), c2.z());
    glfuncs->glEnd();
  
    glfuncs->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glfuncs->glMatrixMode(GL_MODELVIEW);
    glfuncs->glPopMatrix();
    REPORT_GL_ERROR("drawBBox");
  }
  
  // Draw CC
  void ImgData::drawCCs(Shader *colorShader)
  {
    for(const QString &name : mesh->ccNames())
      drawCC(name, false, colorShader);
  }
  
  void ImgData::drawCC(const QString &ccName, bool select, Shader *colorShader)
  {
    CCDrawParms &cdp = mesh->drawParms(ccName);
    if(!cdp.isVisible())
      return;

    REPORT_GL_ERROR("drawCC: start");

    // Bind select framebuffer
    if(select)
      startSelectFbo();

    glfuncs->glDisable(GL_TEXTURE_1D);
    glfuncs->glDisable(GL_TEXTURE_2D);
    glfuncs->glDisable(GL_TEXTURE_3D);
    glfuncs->glDisable(GL_BLEND);
    glfuncs->glDisable(GL_ALPHA_TEST);
    glfuncs->glDisable(GL_AUTO_NORMAL);
    glfuncs->glEnable(GL_DEPTH_TEST);

    glfuncs->glMatrixMode(GL_MODELVIEW);
    glfuncs->glPushMatrix();
    glfuncs->glMultMatrixd(getFrame().worldMatrix());
    if(stack->scale() != Point3d(1.0, 1.0, 1.0))
      glfuncs->glScaled(stack->scale().x(), stack->scale().y(), stack->scale().z());

    // In select mode, we draw from uniqueVertex &c.
    if(select) {
      if(!cdp.uniqueColorVA.empty()) {
        if(glfuncs->glIsBuffer(cdp.uniqueColorVAid) and
           glfuncs->glIsBuffer(cdp.uniqueVertexVAid) and
           cdp.uniqueVertexVA) {
          glfuncs->glDisable(GL_LIGHTING);
          glfuncs->glFrontFace(GL_CCW);
          glfuncs->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

          glfuncs->glDisable(GL_CULL_FACE);
          //if(mesh->culling() and cs.maxDimension() < 3)
            //glfuncs->glEnable(GL_CULL_FACE);

          glfuncs->glEnableClientState(GL_VERTEX_ARRAY);
          glfuncs->glEnableClientState(GL_COLOR_ARRAY);

          glfuncs->glClearColor(0, 0, 0, 1);
          glfuncs->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
          glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, cdp.uniqueVertexVAid);
          glfuncs->glVertexPointer(3, GL_FLOAT, 0, cdp.uniqueVertexVAptr);
          glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, cdp.uniqueColorVAid);
          glfuncs->glColorPointer(3, GL_UNSIGNED_BYTE, 0, 0);
          glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);
    
          glfuncs->glDrawArrays(GL_TRIANGLES, 0, cdp.uniqueColorVA.size()); 

          glfuncs->glDisableClientState(GL_VERTEX_ARRAY);
          glfuncs->glDisableClientState(GL_COLOR_ARRAY);
        } else
          mdxInfo << "Bad buffers passed to drawSelectFace." << " ccName:" << ccName
                           << " uniqueVertexVAid:" << cdp.uniqueVertexVAid 
                           << " uniqueColorVAid:" << cdp.uniqueColorVAid
                           << " uniqueVertexVA" << cdp.uniqueVertexVA  
                           << " uniqueColorVA size:" << cdp.uniqueColorVA.size() << endl;
      }
      REPORT_GL_ERROR("drawCC: draw for select");
      stopSelectFbo();
    } else {
      // !select 
      for(QString &group : cdp.groupList()) {
        if(cdp.isGroupVisible(group) && !cdp.renderChoiceList(group).isEmpty()) {
          RenderChoice &choice = cdp.renderChoice(group, cdp.currentRenderChoice(group));
          auto &ea = cdp.elementEA(choice.elements);
          if(!ea.empty()) {
            // Confirm that we have vertices
            uint vertexVAid = cdp.vertexVAid(choice.vertices);
            std::vector<Point3f> &vertexVA = cdp.vertexVA(choice.vertices);
            if(!(glfuncs->glIsBuffer(vertexVAid)) || vertexVA.empty()) {
              mdxInfo << "Bad vertex buffers in drawCC:"
                               << " ccName:" << ccName
                               << " group:" << group
                               << " drawChoice:" << cdp.currentRenderChoice(group)
                               << " EA size:" << ea.size() 
                               << " vertexVAid:" << vertexVAid
                               << " vertexVA size:" << vertexVA.size() << endl;
              continue;
            }

            // Confirm that we have the right number of colours
            uint colorVAid = cdp.colorVAid(choice.colors);
            std::vector<Colorb> &colorVA = cdp.colorVA(choice.colors);
            if(!(glfuncs->glIsBuffer(colorVAid)) || colorVA.size() != vertexVA.size()) {
              mdxInfo << "Bad color buffers in drawCC:"
                               << " ccName:" << ccName
                               << " group:" << group
                               << " drawChoice:" << cdp.currentRenderChoice(group)
                               << " EA size:" << ea.size() 
                               << " vertexVA size:" << vertexVA.size()
                               << " colorVAid:" << colorVAid
                               << " colorVA size:" << colorVA.size() << endl;
              continue;
            }

            // Confirm that we have the right number of normals, if we're drawing faces
            uint normalVAid = cdp.normalVAid(choice.normals);
            std::vector<Point3f> &normalVA = cdp.normalVA(choice.normals);
            if(choice.type == GL_TRIANGLES) {
              if(!(glfuncs->glIsBuffer(normalVAid)) || normalVA.size() != vertexVA.size()) {
                mdxInfo << "Bad normal buffers in drawCC:"
                                 << " ccName:" << ccName
                                 << " group:" << group
                                 << " drawChoice:" << cdp.currentRenderChoice(group)
                                 << " EA size:" << ea.size() 
                                 << " vertexVA size:" << vertexVA.size()
                                 << " normalVAid:" << normalVAid
                                 << " normalVA size:" << normalVA.size() << endl;
                continue;
              }
            }
            uint clipVAid = cdp.clipVAid(choice.clip);
            std::vector<Point3f> &clipVA = cdp.clipVA(choice.clip);

            // Now we shouldn't be able to fail.  Bind those arrays!
            glfuncs->glEnableClientState(GL_VERTEX_ARRAY);
            glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, vertexVAid);
            glfuncs->glVertexPointer(3, GL_FLOAT, 0, 0);

            glfuncs->glEnableClientState(GL_COLOR_ARRAY);
            glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, colorVAid);
            glfuncs->glColorPointer(4, GL_UNSIGNED_BYTE, 0, 0);

            if(choice.type == GL_TRIANGLES) {
              glfuncs->glEnableClientState(GL_NORMAL_ARRAY);
              glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, normalVAid);
              glfuncs->glNormalPointer(GL_FLOAT, 0, 0);
            }

            if(clipVAid) {
              glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, clipVAid);
              glfuncs->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
              glfuncs->glEnableVertexAttribArray(1);
            }
            glfuncs->glBindBuffer(GL_ARRAY_BUFFER_ARB, 0);

            // Set line / point sizes or polygon offset
            if(choice.type == GL_POINTS) {
              if(choice.plSize > 0)
                glfuncs->glPointSize(choice.plSize);
              else
                glfuncs->glPointSize(MeshPointSize);
            } else if(choice.type == GL_LINES) {
              if(choice.plSize > 0)
                glfuncs->glLineWidth(choice.plSize);
              else
                glfuncs->glLineWidth(MeshLineWidth);
            } else if(choice.type == GL_TRIANGLES) {
              if(choice.polygonOffset != Point2f(0.0,0.0)) {
                glfuncs->glEnable(GL_POLYGON_OFFSET_FILL);
                glfuncs->glPolygonOffset(choice.polygonOffset[0], choice.polygonOffset[1]);
              }
              else
                glfuncs->glDisable(GL_POLYGON_OFFSET_FILL);
            } else {
              mdxInfo << "ImageData;:drawCC Invalid draw choice" << endl;
              continue;
            }

            // Faces and lines / points are rendered differently
            if(choice.type == GL_TRIANGLES) {
              glfuncs->glFrontFace(GL_CCW);
              glfuncs->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

              if(mesh->culling())
                glfuncs->glEnable(GL_CULL_FACE);
              else
                glfuncs->glDisable(GL_CULL_FACE);

              glfuncs->glEnable(GL_LIGHTING);

              Shader::activeTexture(Shader::AT_NONE);
              colorShader->init();
              colorShader->setUniform("brightness", GLSLValue(mesh->brightness()));
              colorShader->setUniform("opacity", GLSLValue(mesh->opacity()));
              //const BoundingBox3d& bbox = mesh->boundingBox();
              //double meshSize = norm(bbox[1] - bbox[0]);
              //colorShader->setUniform("shift", GLSLValue(meshShift * meshSize));
              if(mesh->cellClipping())
                colorShader->setUniform("cellClipping", GLSLValue(true));
              else
                colorShader->setUniform("cellClipping", GLSLValue(false));

              colorShader->useShaders();
              colorShader->setupUniforms();
            } else
              glfuncs->glDisable(GL_LIGHTING);

            // Draw the elements
            glfuncs->glDrawElements(choice.type, ea.size(), GL_UNSIGNED_INT, &ea[0]);

            // Disable what we enabled
            if(choice.type == GL_TRIANGLES) {
              colorShader->stopUsingShaders();
              glfuncs->glDisableClientState(GL_NORMAL_ARRAY);
            }
            if(clipVAid)
              glfuncs->glDisableVertexAttribArray(1);

            glfuncs->glDisableClientState(GL_COLOR_ARRAY);
            glfuncs->glDisableClientState(GL_VERTEX_ARRAY);
          }
          REPORT_GL_ERROR(QString("drawCC: draw ").append(group));
        }
      }
    }

    glfuncs->glMatrixMode(GL_MODELVIEW);
    glfuncs->glPopMatrix();
    glfuncs->glDisable(GL_LIGHTING);
    glfuncs->glDisable(GL_POLYGON_OFFSET_FILL);
    glfuncs->glDisable(GL_POLYGON_OFFSET_LINE);

    REPORT_GL_ERROR("drawCC: end");
  }

  // Reset mesh
  void ImgData::resetMesh()
  {
    mesh->reset();
  }

  // Reset stack data
  void ImgData::resetStack()
  {
    stack->reset();
    unloadTex();
    emit stackUnloaded();
  }
  
  // Initialize controls
  void ImgData::initControls(QWidget* viewer)
  {
    // Transfer functions
    connect(workTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction &)), this,
            SLOT(updateWorkColorMap(const TransferFunction &)));
    connect(mainTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction &)), this,
            SLOT(updateMainColorMap(const TransferFunction &)));
  
    connect(workTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction &)), viewer, SLOT(updateViewer()));
    connect(mainTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction &)), viewer, SLOT(updateViewer()));
  }
  
  void ImgData::unloadTex()
  {
    if(dataTexIds["Main"])
      glfuncs->glDeleteTextures(1, &dataTexIds["Main"]);
    if(dataTexIds["Work"])
      glfuncs->glDeleteTextures(1, &dataTexIds["Work"]);
    dataTexIds["Main"] = dataTexIds["Work"] = 0;

    if(mcmapTexId)
      glfuncs->glDeleteTextures(1, &mcmapTexId);
    if(wcmapTexId)
      glfuncs->glDeleteTextures(1, &wcmapTexId);
    if(colorMapTexId)
      glfuncs->glDeleteTextures(1, &colorMapTexId);

    if(labelCenterTexIds["Main"])
      glfuncs->glDeleteTextures(1, &labelCenterTexIds["Main"]);
    if(labelCenterTexIds["Work"])
      glfuncs->glDeleteTextures(1, &labelCenterTexIds["Work"]);
    labelCenterTexIds["Main"] = labelCenterTexIds["Work"] = 0;
  
    //surfTexId = heatTexId = 
    colorMapTexId = wcmapTexId = mcmapTexId = 0;
  
    // Histograms require update
    invalidateHistogram(MainHist);
    invalidateHistogram(WorkHist);
  }
  
  // Get decimation factors for each dimension
  Point3u ImgData::getTexStep()
  {
    Point3u step;
    for(uint i = 0; i < 3; i++) {
      step[i] = stack->size()[i] / MaxTexSize[i];
      if(stack->size()[i] % MaxTexSize[i] > 0)
        step[i]++;
    }
    return (step);
  }
  
  void ImgData::loadTex(const QString &storeName, const BoundingBox3i &bbox)
  {
    // Check the sizes
    if(stack->storeSize() == 0
        or (storeName == "Main" and stack->main()->data().size() != stack->storeSize())
        or (storeName == "Work" and stack->work()->data().size() != stack->storeSize())) {
      unloadTex();
      return;
    }

    // Get the texId or create a new texture
    bool newTex = false;
    auto &texId = dataTexIds[storeName];
    if(texId == 0) {
      mdxInfo << "Creating new texture of size " << stack->size() << endl;
      glfuncs->glGenTextures(1, &texId);
      newTex = true;
    }

    bool updateCenters = false;
    auto &data = storeName == "Main" ? stack->main()->data() : stack->work()->data();
    // Decide if loading the whole texture or updating
    if(newTex or bbox.empty() or bbox.size() == Point3i(stack->size())) {  // FIXME maybe also check if bbox is bigger?
      updateCenters = true;
      glfuncs->glBindTexture(GL_TEXTURE_3D, texId);
      const auto &size = stack->size();
      if(swapTextureBytes(texId))
        glfuncs->glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_TRUE);
      // Find if any dimension need to be decimated
      Point3u step = getTexStep();
      if(step == Point3u(1u, 1u, 1u)) {
        GLsizei width = size.x();
    
        if(width % 4 == 0)
          glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 8);
        else if(width % 2 != 0)
          glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
    
        glfuncs->glTexImage3D(GL_TEXTURE_3D, 0, internalFormat(texId), size.x(), size.y(), size.z(),
                              0, GL_ALPHA, GL_UNSIGNED_SHORT, data.data());
      } else { 
        Point3u sz = Point3u(size.x() / step.x(), size.y() / step.y(), size.z() / step.z());
    
        HVecUS tpix(size_t(sz.x()) * sz.y() * sz.z());
        ushort* p = tpix.data();
    
        // Decimate array
        for(uint z = step.z() / 2; z < sz.z() * step.z(); z += step.z())
          for(uint y = step.y() / 2; y < sz.y() * step.y(); y += step.y())
            for(uint x = step.x() / 2; x < sz.x() * step.x(); x += step.x())
              *p++ = data[offset(x, y, z)];
    
        GLsizei width = sz.x();
    
        if(width % 4 == 0)
          glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 8);
        else if(width % 2 != 0)
          glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
    
        // Load smaller texture
        glfuncs->glTexImage3D(GL_TEXTURE_3D, 0, internalFormat(texId), sz.x(), sz.y(), sz.z(),
                              0, GL_ALPHA, GL_UNSIGNED_SHORT, tpix.data());
      }
      glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 4);   // Restore default
      if(swapTextureBytes(texId))
        glfuncs->glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_FALSE);
    
      glfuncs->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, interpolation(texId));
      glfuncs->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, interpolation(texId));
      glfuncs->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
      glfuncs->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
      glfuncs->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
      double anisotropy;
      glfuncs->glGetDoublev(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisotropy);
      glfuncs->glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);

    } else { // Update a subsection of the texture

      Point3i base(bbox[0].x(), bbox[0].y(), bbox[0].z());
      Point3i bsz(bbox[1].x() - bbox[0].x(), bbox[1].y() - bbox[0].y(), bbox[1].z() - bbox[0].z());
  
      if(swapTextureBytes(texId))
        glfuncs->glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_TRUE);
      // Get factor to decimate texture if required
      Point3u step = getTexStep();
      // Enlarge bounding box to multiples of steps
      if(step != Point3u(1u, 1u, 1u)) {
        base.x() -= base.x() % step.x();
        base.y() -= base.y() % step.y();
        base.z() -= base.z() % step.z();
        bsz.x() += base.x() % step.x();
        bsz.y() += base.y() % step.y();
        bsz.z() += base.z() % step.z();
        if(bsz.x() % step.x() > 0)
          bsz.x() = bsz.x() / step.x() + 1;
        else
          bsz.x() /= step.x();
        if(bsz.y() % step.y() > 0)
          bsz.y() = bsz.y() / step.y() + 1;
        else
          bsz.y() /= step.y();
        if(bsz.z() % step.z() > 0)
          bsz.z() = bsz.z() / step.z() + 1;
        else
          bsz.z() /= step.z();
  
        HVecUS tpix(size_t(bsz.x()) * bsz.y() * bsz.z());
  
        ushort* p = tpix.data();
        for(uint z = base.z() + step.z() / 2; z < base.z() + bsz.z() * step.z(); z += step.z())
          for(uint y = base.y() + step.y() / 2; y < base.y() + bsz.y() * step.y(); y += step.y())
            for(uint x = base.x() + step.x() / 2; x < base.x() + bsz.x() * step.x(); x += step.x())
              *p++ = data[offset(x, y, z)];
  
        glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
  
        // Load texture subimage
        glfuncs->glBindTexture(GL_TEXTURE_3D, texId);
        glfuncs->glTexSubImage3D(GL_TEXTURE_3D, 0,
                                 base.x() / step.x(), base.y() / step.y(), base.z() / step.z(), bsz.x(),
                                 bsz.y(), bsz.z(), GL_ALPHA, GL_UNSIGNED_SHORT, tpix.data());
        glfuncs->glBindTexture(GL_TEXTURE_3D, 0);
  
        glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 4);     // Restore default
      } else {
        glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
  
        glfuncs->glBindTexture(GL_TEXTURE_3D, texId);
        glfuncs->glPixelStorei(GL_UNPACK_ROW_LENGTH, stack->size().x());
        glfuncs->glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, stack->size().y());
        glfuncs->glTexSubImage3D(GL_TEXTURE_3D, 0, base.x(), base.y(), base.z(), bsz.x(), bsz.y(), bsz.z(),
                                 GL_ALPHA, GL_UNSIGNED_SHORT, &data[offset(base.x(), base.y(), base.z())]);
        glfuncs->glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        glfuncs->glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, 0);
        glfuncs->glBindTexture(GL_TEXTURE_3D, 0);
  
        glfuncs->glPixelStorei(GL_UNPACK_ALIGNMENT, 4);     // Restore default
      }
      if(swapTextureBytes(texId))
        glfuncs->glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_FALSE);
    }
    // Histograms require update
    if(storeName == "Main")
      invalidateHistogram(MainHist);
    else if(storeName == "Work")
      invalidateHistogram(WorkHist);

    REPORT_GL_ERROR("loadTex");
    
    // If labeled recalc centers
    if(updateCenters and ((storeName == "Main" and stack->main()->labels()) or (storeName == "Work" and stack->work()->labels())))
      loadLabelCenterTex(storeName);
  }
  
  // Bind the stack texture
  void ImgData::bind3DTex(GLuint texId, Shader::ActiveTextures atexId)
  {
    if(texId) {
      glfuncs->glMatrixMode(GL_TEXTURE);
      glfuncs->glLoadIdentity();
      const Point3d& Size = multiply(stack->step(), Point3d(stack->size()));
      glfuncs->glScaled(1 / Size.x(), 1 / Size.y(), 1 / Size.z());
      Point3d Origin = stack->origin();
      glfuncs->glTranslatef(-Origin.x(), -Origin.y(), -Origin.z());
      Shader::activeTexture(atexId);
      glfuncs->glBindTexture(GL_TEXTURE_3D, texId);
      glfuncs->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, interpolation(texId));
      glfuncs->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, interpolation(texId));
      Shader::activeTexture(Shader::AT_NONE);
      glfuncs->glBindTexture(GL_TEXTURE_3D, 0);
      glfuncs->glMatrixMode(GL_MODELVIEW);
    }
  }
  
  // Bind the stack texture
  void ImgData::bind2DTex(GLuint texId)
  {
    if(texId) {
      glfuncs->glMatrixMode(GL_TEXTURE);
      glfuncs->glLoadIdentity();
      Shader::activeTexture(Shader::AT_TEX2D);
      glfuncs->glBindTexture(GL_TEXTURE_2D, texId);
      glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, interpolation(texId));
      glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, interpolation(texId));
      Shader::activeTexture(Shader::AT_NONE);
      glfuncs->glBindTexture(GL_TEXTURE_2D, 0);
      glfuncs->glMatrixMode(GL_MODELVIEW);
    }
  }
  
  void ImgData::unbind3DTex()
  {
    glfuncs->glMatrixMode(GL_TEXTURE);
    glfuncs->glLoadIdentity();
    Shader::activeTexture(Shader::AT_TEX3D);
    glfuncs->glBindTexture(GL_TEXTURE_3D, 0);
    Shader::activeTexture(Shader::AT_SECOND_TEX3D);
    glfuncs->glBindTexture(GL_TEXTURE_3D, 0);
    Shader::activeTexture(Shader::AT_NONE);
    glfuncs->glMatrixMode(GL_MODELVIEW);
  }
  
  void ImgData::unbind2DTex()
  {
    glfuncs->glMatrixMode(GL_TEXTURE);
    glfuncs->glLoadIdentity();
    Shader::activeTexture(Shader::AT_TEX2D);
    glfuncs->glBindTexture(GL_TEXTURE_2D, 0);
    Shader::activeTexture(Shader::AT_NONE);
    glfuncs->glMatrixMode(GL_MODELVIEW);
  }
  
  // Load a texture image (for Keyence)
  void ImgData::loadImgTex(const QImage& image)
  {
    if(image.isNull() or image.width() <= 0 or image.height() <= 0)
      return;
    if(imgTexId)
      glfuncs->glDeleteTextures(1, &imgTexId);
    glfuncs->glGenTextures(1, &imgTexId);
    glfuncs->glBindTexture(GL_TEXTURE_2D, imgTexId);
  
    std::vector<double> pix(image.width() * image.height() * 3);
    double* px = &pix[0];
    for(int y = image.height() - 1; y >= 0; y--)
      for(int x = 0; x < image.width(); x++) {
        *px++ = double(qRed(image.pixel(x, y))) / 255.0;
        *px++ = double(qGreen(image.pixel(x, y))) / 255.0;
        *px++ = double(qBlue(image.pixel(x, y))) / 255.0;
      }
  
    glfuncs->glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.width(), image.height(), 0, 
                          GL_RGB, GL_FLOAT, &pix[0]);
  
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glfuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
  }
  
  void ImgData::initTex()
  {
    // Initialize 1D index texture
    LabelColorsChanged = true;
    loadLabelTex();
  }
  
  void ImgData::loadLabelTex()
  {
    if(LabelColorsChanged) {
      if(!labelTexId)
        glfuncs->glGenTextures(1, &labelTexId);
      glfuncs->glBindTexture(GL_TEXTURE_1D, labelTexId);
  
      if(!LabelColors.empty())
        glfuncs->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, LabelColors.size(), 0, GL_RGBA, GL_FLOAT, &LabelColors[0]);
      else
        glfuncs->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 0, 0, GL_RGB, GL_FLOAT, 0);
      glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glfuncs->glBindTexture(GL_TEXTURE_1D, 0);
      LabelColorsChanged = false;
    }
  }

  void ImgData::loadLabelCenterTex(const QString &storeName, int startLabel, int endLabel)
  {
    IntVec counts(65536, 0);
    Point3ulVec centers(65536);

    if(storeName == "Main" and !stack->main()->labels())
      unloadLabelCenterTex(storeName);
    else if(storeName == "Work" and !stack->work()->labels())
      unloadLabelCenterTex(storeName);
    else { // if(labelCentersChanged[storeName]) {
      // Default, do the whole thing
      // When updating all, do the centers and bBoxes as well
      if(startLabel == 0 and endLabel == 0) { 
        // Find centers of labels and bounding boxes
        auto &bBox = labelBBox[storeName];
        bBox.resize(65536);
        std::fill(bBox.begin(), bBox.end(), BoundingBox3i());
        Point3u sz = stack->size();
        const auto &data = (storeName == "Main" ? stack->main()->data() : stack->work()->data());
        ushort maxLabel = 0;
        for(uint z = 0; z < sz.z(); z++)
          for(uint y = 0; y < sz.y(); y++)
            for(uint x = 0; x < sz.x(); x++) {
              ushort label = data[stack->offset(x, y, z)];
              if(label > 0) {
                if(label > maxLabel)
                  maxLabel = label;
                counts[label]++;
                centers[label] += Point3ul(x, y, z);
                bBox[label] |= Point3i(x, y, z);
              }
            }
  
        // Calculate label centers
        auto &lCenters = labelCenters[storeName];
        lCenters.resize(maxLabel+1);
        bBox.resize(maxLabel+1);
        if(maxLabel > 0) {
          for(int label = 0; label <= maxLabel; label++) {
            int count = counts[label];
            if(count <= 0)
              continue;
            Point3ul center = centers[label];
            lCenters[label] = Point3us(center.x()/count, center.y()/count, center.z()/count);
          }
        }
        // Write centers to graphics card
        auto &texId = labelCenterTexIds[storeName];
        if(!texId)
          glfuncs->glGenTextures(1, &texId);
  
        glfuncs->glBindTexture(GL_TEXTURE_1D, texId);
        if(lCenters.size() > 0)
          glfuncs->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB16, lCenters.size(), 0, GL_RGB, GL_UNSIGNED_SHORT, &lCenters[0]);
        else
          glfuncs->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB16, 0, 0, GL_RGB, GL_UNSIGNED_SHORT, 0);
  
        glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glfuncs->glBindTexture(GL_TEXTURE_1D, 0);
  
        labelCentersChanged[storeName] = false;
      } else {
        // For update, assume calling procedure has calculated the centers and just update the tex
        // Check the labels
        if(startLabel < 0 or endLabel < 0)
          return;
        if(startLabel > endLabel)
          return;
        if(startLabel == 0 and endLabel > 0)
          startLabel = 1;
        auto &lCenters = labelCenters[storeName];
        if(ulong(startLabel) >= lCenters.size())
          return;
        if(ulong(endLabel) >= lCenters.size())
          endLabel = labelCenters.size() - 1;

        // Get the texture
        auto &texId = labelCenterTexIds[storeName];
        if(!texId) {
          mdxInfo << "Error, loadLabelCenterTex update called with no tex" << endl;
          return;
        }

        // Write the subsection required
        glfuncs->glBindTexture(GL_TEXTURE_1D, texId);
        glfuncs->glTexSubImage1D(GL_TEXTURE_1D, 0, startLabel, endLabel - startLabel + 1, GL_RGB, GL_UNSIGNED_SHORT, &lCenters[startLabel]);
        glfuncs->glBindTexture(GL_TEXTURE_1D, 0);
      }
    }
    REPORT_GL_ERROR("loadLabelCenterTex");
  }

  void ImgData::unloadLabelCenterTex(const QString &storeName)
  {
    auto &lCenters = labelCenters[storeName];
    lCenters.clear();
    auto &texId = labelCenterTexIds[storeName];
    if(texId)
      glfuncs->glDeleteTextures(1, &texId);
    texId = 0;
    labelCentersChanged[storeName] = false;
  }
  
  // Select vertices by parent
//  void ImgData::selectParent(int parent, int repeat)
//  {
//    vvGraph& S = mesh->graph();
//    if(S.empty())
//      return;
//  
//    bool changed = false;
//    if(repeat > 0) {
//      const IntIntAttr& parentMap = mesh->labelMap("Parents");
//      #pragma omp parallel for
//      for(uint i = 0; i < S.size(); i++) {
//        vertex v = S[i];
//        if(v->selected)
//          continue;
//        IntIntAttr::const_iterator it = parentMap.find(v->label);
//        if(it != parentMap.end() and it->second % repeat == parent) {
//          changed = true;
//          v->selected = true;
//        }
//      }
//    } else {
//      const IntIntAttr& parentMap = mesh->labelMap("Parents");
//      #pragma omp parallel for
//      for(uint i = 0; i < S.size(); i++) {
//        vertex v = S[i];
//        if(v->selected)
//          continue;
//        IntIntAttr::const_iterator it = parentMap.find(v->label);
//        if(it != parentMap.end() and it->second == parent) {
//          changed = true;
//          v->selected = true;
//        }
//      }
//    }
//    if(changed) {
//      mesh->correctSelection(true);
//      updateSelection();
//    }
//  }
//
//  // Select vertices by parent
//  void ImgData::selectParent(const IntSet &parents)
//  {
//    vvGraph& S = mesh->graph();
//    if(parents.empty() or S.empty())
//      return;
//  
//    bool changed = false;
//    const IntIntAttr& parentMap = mesh->labelMap("Parents");
//    #pragma omp parallel for
//    for(uint i = 0; i < S.size(); i++) {
//      vertex v = S[i];
//      if(v->selected)
//        continue;
//      IntIntAttr::const_iterator it = parentMap.find(v->label);
//      if(it != parentMap.end() and parents.count(it->second) > 0) {
//        changed = true;
//        v->selected = true;
//      }
//    }
//    if(changed) {
//      mesh->correctSelection(true);
//      updateSelection();
//    }
//  } 
//
//  // Unselect vertices by
//  void ImgData::unselectParent(const IntSet &parents)
//  {
//    vvGraph& S = mesh->graph();
//    if(parents.empty() or S.empty())
//      return;
//  
//    bool changed = false;
//    const IntIntAttr& parentMap = mesh->labelMap("Parents");
//    #pragma omp parallel for
//    for(uint i = 0; i < S.size(); i++) {
//      vertex v = S[i];
//      if(!v->selected)
//        continue;
//      IntIntAttr::const_iterator it = parentMap.find(v->label);
//      if(it != parentMap.end() and parents.count(it->second) > 0) {
//        changed = true;
//        v->selected = false;
//      }
//    }
//    if(changed) {
//      mesh->correctSelection(true);
//      updateSelection();
//    }
//  }
  
  // Setup framebuffer for selection
  bool ImgData::startSelectFbo()
  {
    // Size of renderbuffers
    static GLint w = 0, h = 0;

    // Get viewport size
    GLint dims[4] = {0};
    glfuncs->glGetIntegerv(GL_VIEWPORT, dims);
    GLint width = dims[2];
    GLint height = dims[3];

    // If there is no frame buffer create one
    if(!selFboId)
      glfuncs->glGenFramebuffers(1, &selFboId);
    glfuncs->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, selFboId);

    // (re-)create if not created or changed in size
    bool recreate = w != width or h != height or !selFboColor or !selFboDepth;
    if(recreate) {
      if(selFboColor)
        glfuncs->glDeleteRenderbuffers(1, &selFboColor);

      glfuncs->glGenRenderbuffers(1, &selFboColor);
      glfuncs->glBindRenderbuffer(GL_RENDERBUFFER_EXT, selFboColor);
      glfuncs->glRenderbufferStorage(GL_RENDERBUFFER_EXT, GL_RGB8, width, height);

      if(selFboDepth)
        glfuncs->glDeleteRenderbuffers(1, &selFboDepth);
      glfuncs->glGenRenderbuffers(1, &selFboDepth);
      glfuncs->glBindRenderbuffer(GL_RENDERBUFFER, selFboDepth);
      glfuncs->glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
      glfuncs->glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, selFboColor);
      glfuncs->glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, selFboDepth);
      // Check frame buffer created properly
      if(glfuncs->glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        mdxInfo << "Warning: Unable to create framebuffer, selection will not work." << endl;
        return false;
      }
      glfuncs->glViewport(0, 0, width, height);
      w = width;
      h = height;
    }

    REPORT_GL_ERROR("startSelectFbo");

    return true;
  }

  bool ImgData::stopSelectFbo()
  {
    glfuncs->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glfuncs->glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

    REPORT_GL_ERROR("stopSelectFbo");

    return true;
  }

  bool ImgData::readSelectFbo()
  {
    if(!selFboId)
       startSelectFbo();

    glfuncs->glBindFramebuffer(GL_READ_FRAMEBUFFER, selFboId);
    glfuncs->glReadBuffer( GL_COLOR_ATTACHMENT0 );

    REPORT_GL_ERROR("readSelectFboUnbind");

    return true;
  }

  // Find selected face in a cell complex
  CCIndex ImgData::findSelectFace(int x, int y)
  {
    if(!mesh or mesh->ccName().isEmpty())
      return CCIndex::UNDEF;

    if(x < 0 or y < 0)
      return CCIndex::UNDEF;

    drawCC(mesh->ccName(), true);
 
    // Bind select framebuffer for reading
    readSelectFbo();
  
    // Find the color of the pixels drawn in BACK buffer
    Point3GLub pix;
    GLint viewport[4];
    glfuncs->glGetIntegerv(GL_VIEWPORT, viewport);
    if(x >= viewport[2] or y > viewport[3])
      return CCIndex::UNDEF;
    glfuncs->glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &pix);

    // Unbind select framebuffer
    stopSelectFbo();

    uint u = vMapColor(pix);

    // Return if nothing selected
    if(u == UINT_MAX)
      return CCIndex::UNDEF;

    CCStructure &cs = mesh->ccStructure(mesh->ccName());
    const std::vector<CCIndex> &faces = cs.faces();
    if(u > faces.size()) {
      mdxInfo << "findSelectFace:Error Index too large:" << u << " number of faces:" << faces.size() << endl;
      return CCIndex::UNDEF;
    }
    return faces[u];
  }
   
  // Trim bounding box with a clip plane in point normal form
  void ImgData::bBoxClip(BoundingBox3d& bBox, Point3d p, Point3d n)
  {
    int x, y, z;
    Point3d u1, u2, pt;
    double dir, s;
  
    // check x direction, find which line to test
    dir = n * Point3d(1.0f, 0.0f, 0.0f);
    y = (n.y() > 0 ? 1 : 0);
    z = (n.z() > 0 ? 1 : 0);
    u1 = Point3d(bBox[0].x(), bBox[y].y(), bBox[z].z());
    u2 = Point3d(bBox[1].x(), bBox[y].y(), bBox[z].z());
    if(planeLineIntersect(p, n, u1, u2, s, pt)) {
      if(dir > 0) {     // outside points in positive x dir
        if(pt.x() > bBox[0].x())
          bBox[0].x() = pt.x();
      } else {
        if(pt.x() < bBox[1].x())
          bBox[1].x() = pt.x();
      }
    }
    // check y direction, find which line to test
    dir = n * Point3d(0.0f, 1.0f, 0.0f);
    x = (n.x() > 0 ? 1 : 0);
    z = (n.z() > 0 ? 1 : 0);
    u1 = Point3d(bBox[x].x(), bBox[0].y(), bBox[z].z());
    u2 = Point3d(bBox[x].x(), bBox[1].y(), bBox[z].z());
    if(planeLineIntersect(p, n, u1, u2, s, pt)) {
      if(dir > 0) {
        if(pt.y() > bBox[0].y())
          bBox[0].y() = pt.y();
      } else {
        if(pt.y() < bBox[1].y())
          bBox[1].y() = pt.y();
      }
    }
    // check z direction, find which line to test
    dir = n * Point3d(0.0f, 0.0f, 1.0f);
    x = (n.x() > 0 ? 1 : 0);
    y = (n.y() > 0 ? 1 : 0);
    u1 = Point3d(bBox[x].x(), bBox[y].y(), bBox[0].z());
    u2 = Point3d(bBox[x].x(), bBox[y].y(), bBox[1].z());
    if(planeLineIntersect(p, n, u1, u2, s, pt)) {
      if(dir > 0) {
        if(pt.z() > bBox[0].z())
          bBox[0].z() = pt.z();
      } else {
        if(pt.z() < bBox[1].z())
          bBox[1].z() = pt.z();
      }
    }
  }
  
  //  Calculate bounding box from clipping planes (in point normal form)
  void ImgData::bBoxFromClip()
  {
    // clear, set to entire texture
    Point3d origin = stack->origin();
    const Point3d size(stack->size());
    bBox[0] = origin;
    bBox[1] = origin + size;
  
    // Test all 6 planes (twice)
    for(int i = 0; i < 2; i++)
      for(int pln = 0; pln < 6; pln++) {
        if(!clipDo[pln / 2])
          continue;
  
        Point3d n(pn[pln].x(), pn[pln].y(), pn[pln].z());
        double d = pn[pln][3];
        bBoxClip(bBox, n * -d, n);
      }
  }
  
  // Setup data for clipping test
  void ImgData::getClipTestData(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3)
  {
    Clip* c1 = clip1.clip;
    Clip* c2 = clip2.clip;
    Clip* c3 = clip3.clip;
    // Indicator if in use
    clipDo[0] = (c1->enabled() or c1->grid() ? 1 : 0);
    clipDo[1] = (c2->enabled() or c2->grid() ? 1 : 0);
    clipDo[2] = (c3->enabled() or c3->grid() ? 1 : 0);
  
    // Frame matrix
    frm = Matrix4d(getFrame().worldMatrix());
  
    // Clip plane matrices
    clm[0] = Matrix4d(c1->frame().inverse().worldMatrix());
    clm[1] = Matrix4d(c2->frame().inverse().worldMatrix());
    clm[2] = Matrix4d(c3->frame().inverse().worldMatrix());
  
    // Clipping planes, point normal form
    pn[0] = Point4f(frm * clm[0] * 
                 Point4d(c1->normal().x(), c1->normal().y(), c1->normal().z(), c1->width()));
    pn[1] = Point4f(frm * clm[0] * 
                 Point4d(-c1->normal().x(), -c1->normal().y(), -c1->normal().z(), c1->width()));
    pn[2] = Point4f(frm * clm[1] * 
                 Point4d(c2->normal().x(), c2->normal().y(), c2->normal().z(), c2->width()));
    pn[3] = Point4f(frm * clm[1] * 
                 Point4d(-c2->normal().x(), -c2->normal().y(), -c2->normal().z(), c2->width()));
    pn[4] = Point4f(frm * clm[2] * 
                 Point4d(c3->normal().x(), c3->normal().y(), c3->normal().z(), c3->width()));
    pn[5] = Point4f(frm * clm[2] * 
                 Point4d(-c3->normal().x(), -c3->normal().y(), -c3->normal().z(), c3->width()));
  
    // Put in host vector
    Hpn.resize(6);
    for(int i = 0; i < 6; i++)
      Hpn[i] = pn[i];
  }
  
  void ImgData::voxelEditStart(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3)
  {
    if(stack->work()->isVisible()) {
      // Setup matrices and planes for clip test
      getClipTestData(clip1, clip2, clip3);
  
      // If 3D seeding, increment label number
      if(FillWorkData and stack->work()->isVisible() and SeedStack and stack->work()->labels()) {
        int lab = stack->nextLabel();
        Information::setStatus(QString("Adding seed %1").arg(lab));
      }
  
      // Setup texture update area
      bBoxTex[0] = Point3i(stack->size().x(), stack->size().y(), stack->size().z());
      bBoxTex[1] = Point3i(0, 0, 0);
    }
  }
 
  // Edit the 3D voxel data 
  void ImgData::voxelEditStop()
  {
    // Reload the texture to see the edit result
    if(stack->work()->isVisible() and pixelsChanged)
      loadTex("Work", bBoxTex);
    pixelsChanged = false;
  }
  
  void ImgData::voxelEdit(double pixelRadius, const Point3d& p, const Point3d& px, 
                       const Point3d& py, const Point3d& pz, bool doCut, int currentLabel)
  {
    Point3d Origin = stack->origin();
    if(stack->work()->isVisible()) {
      // Clear bounding box, set to entire texture
      bBox[0] = Origin;
      bBox[1] = Origin + multiply(Point3d(stack->size()), stack->step());
      // Trim bounding box to clip planes
      for(int i = 0; i < 2; i++) {
        // Test all 6 planes (twice)
        for(int pln = 0; pln < 6; pln++) {
          if(!clipDo[pln / 2])
            continue;
  
          Point3d n(pn[pln].x(), pn[pln].y(), pn[pln].z());
          double d = pn[pln][3];
          bBoxClip(bBox, n * -d, n);
        }
        bBoxClip(bBox, p - pixelRadius * px, px);
        bBoxClip(bBox, p + pixelRadius * px, -px);
        bBoxClip(bBox, p - pixelRadius * py, py);
        bBoxClip(bBox, p + pixelRadius * py, -py);
        if(doCut) {
          bBoxClip(bBox, p - pixelRadius * pz, pz);
          bBoxClip(bBox, p + pixelRadius * pz, -pz);
        }
      }
  
      // Get bounding box in image coordinates and pad a pixel
      Point3i imgSize(stack->size().x(), stack->size().y(), stack->size().z());
      Point3i base = worldToImagei(bBox[0]) - Point3i(1, 1, 1);
      Point3i end = worldToImagei(bBox[1]) + Point3i(1, 1, 1);
  
      // Check bounds and update bound box for texture update
      for(int i = 0; i < 3; i++) {
        base[i] = std::max(base[i], 0);
        end[i] = std::min(end[i], imgSize[i]);
        bBoxTex[0][i] = std::min(bBoxTex[0][i], base[i]);
        bBoxTex[1][i] = std::max(bBoxTex[1][i], end[i]);
      }
      Point3i size = end - base;
  
      if(size.x() > 0 and size.y() > 0 and size.z() > 0) {
        ushort pixval = 0x0;
        if(FillWorkData) { // Decide if clear, fill or seel
          if(stack->work()->labels()) {
            if(SeedStack)
              pixval = stack->viewLabel();
            else
              pixval = currentLabel;
          } else
            pixval = 0xFFFF; // FIXME Should we be able to set this?
        }
        mdx::voxelEditGPU(base, size, imgSize, Point3f(stack->step()), Point3f(Origin), Point3f(p), Point3f(pz), pixelRadius, 
                                     pixval, clipDo, Hpn, stack->work()->data());
        pixelsChanged = true;
      }
      if(pixelsChanged and (VoxelEditMaxPix == 0 or size_t(bBoxTex[1].x() - bBoxTex[0].x()) * (bBoxTex[1].y() - bBoxTex[0].y())
         * (bBoxTex[1].z() - bBoxTex[0].z()) < size_t(VoxelEditMaxPix))) {
        loadTex("Work", bBoxTex);
        pixelsChanged = false;
        bBoxTex[0] = Point3i(stack->size().x(), stack->size().y(), stack->size().z());
        bBoxTex[1] = Point3i(0, 0, 0);
      }
    }
  }
  
	// Find the seed point for 3D voxel editing
	bool ImgData::findSeedPoint(uint x, uint y, CutSurf& cutSurf, Point3d& p)
  {
    if(!SeedStack)
      return (false); 
  
    p = Point3d(0, 0, 0);
    // Use the cutting surface if visible to locate the seed in Z
    if(cutSurf.cut->isVisible()) {
      // Bind select frame buffer for reading, do we need to clear?
      readSelectFbo();
      cutSurf.drawCutSurfPlane(*this, true);

      // Pixel color is x,y,z
      GLint viewport[4];
      glfuncs->glGetIntegerv(GL_VIEWPORT, viewport);
      glfuncs->glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_FLOAT, &p);

      // Unbind select frame buffer
      stopSelectFbo();
      p -= Point3d(.5f, .5f, .5f);
    } else if(!mesh->ccName().isEmpty()) {
      CCIndex face = findSelectFace(x, y);
      if(face == CCIndex::UNDEF)
        return false;
      p = Point3d(mesh->indexAttr()[face].pos);
    } 
    return (true);
  }
  
  void ImgData::updateHistogram(std::vector<double>& hist, const HVecUS& data, 
                          std::pair<double, double>& minMaxValues, int max_data, int size)
  {
    hist.clear();
    if(data.empty())
      return;
    hist.resize(size, 0);
    uint min = 65536;
    uint max = 0;
    #pragma omp parallel for reduction(min:min) reduction(max:max)
    for(uint i = 0; i < data.size(); i++) {
      ushort d = data[i];
      int val = (d * size) / max_data;
      hist[val]++;
      if(d < min)
        min = d;
      if(d > max)
        max = d;
    }
    minMaxValues.first = min / double(max_data);
    minMaxValues.second = max / double(max_data);
  }
  
  void ImgData::updateWorkHistogram()
  {
    if(WorkHist.empty())
      updateHistogram(WorkHist, stack->work()->data(), workBounds);
  }
  
  void ImgData::updateMainHistogram()
  {
    if(MainHist.empty())
      updateHistogram(MainHist, stack->main()->data(), mainBounds);
  }
  
  void ImgData::editMainTransferFunction()
  {
    updateMainHistogram();
    mainTransferDlg->changeHistogram(MainHist);
    mainTransferDlg->changeBounds(mainBounds);
    mainTransferDlg->setTransferFunction(stack->main()->transferFct());
    if(mainTransferDlg->exec() == QDialog::Accepted) {
      emit changedInterface();
      stack->main()->setTransferFct(mainTransferDlg->transferFunction());
      updateMainColorMap();
    }
  }
  
  void ImgData::editWorkTransferFunction()
  {
    updateWorkHistogram();
    workTransferDlg->changeHistogram(WorkHist);
    workTransferDlg->changeBounds(workBounds);
    workTransferDlg->setTransferFunction(stack->work()->transferFct());
    if(workTransferDlg->exec() == QDialog::Accepted) {
      emit changedInterface();
      stack->work()->setTransferFct(workTransferDlg->transferFunction());
      updateWorkColorMap();
    }
  }
   
  void ImgData::updateColorMap(bool& newColorMap, std::vector<TransferFunction::Colorf>& ColorMap,
                               const TransferFunction& transferFct)
  {
    static const int n = 4096;
    ColorMap.resize(n);
    double dc = 1.0 / (n - 1);
    for(int i = 0; i < n; ++i) {
      ColorMap[i] = transferFct.rgba(i * dc);
    }
    newColorMap = true;
  }

  bool ImgData::setupTexFromColorMap(GLuint &texId, const ColorbVec &colorVec)
  {
    if(!texId)
      glfuncs->glGenTextures(1, &texId);
    glfuncs->glBindTexture(GL_TEXTURE_1D, texId);
    glfuncs->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA8, colorVec.size(),
                          0, GL_RGBA, GL_UNSIGNED_BYTE, &colorVec[0][0]);
    glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glfuncs->glBindTexture(GL_TEXTURE_1D, 0);

    return true;
  };
  
  void ImgData::setupColorMap(bool& newColorMap, GLuint &texId, const std::vector<TransferFunction::Colorf>& ColorMap,
                              Shader::ActiveTextures activeTex)
  {
    if(newColorMap or !texId) {
      newColorMap = false;
      if(!texId)
        glfuncs->glGenTextures(1, &texId);
      glfuncs->glBindTexture(GL_TEXTURE_1D, texId);
      glfuncs->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA16, ColorMap.size(), 0, GL_RGBA, GL_FLOAT, &ColorMap[0]);
      glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glfuncs->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glfuncs->glBindTexture(GL_TEXTURE_1D, 0);
    }
    Shader::activeTexture(activeTex);
    glfuncs->glBindTexture(GL_TEXTURE_1D, texId);
    Shader::activeTexture(Shader::AT_NONE);
  }

  void ImgData::setupLabelCenters(const QString &storeName)
  {
    auto &texId = labelCenterTexIds[storeName];
    if(!texId)
      loadLabelCenterTex(storeName);
    if(!texId)
      return;
    if(labelCenters[storeName].size() == 0)
      return;

    if(storeName == "Main")
      Shader::activeTexture(Shader::AT_LABEL_CENTER_TEX1);
    else
      Shader::activeTexture(Shader::AT_LABEL_CENTER_TEX2);

    glfuncs->glBindTexture(GL_TEXTURE_1D, texId);
    Shader::activeTexture(Shader::AT_NONE);

    REPORT_GL_ERROR("setupLabelCenters");
  }

  void ImgData::updateWorkColorMap(const TransferFunction& fct)
  {
    stack->work()->setTransferFct(fct);
    updateWorkColorMap();
  }
  
  void ImgData::updateMainColorMap(const TransferFunction& fct)
  {
    stack->main()->setTransferFct(fct);
    updateMainColorMap();
  }
  
  void ImgData::setColorMap(const QString& pth, bool work)
  {
    QFile f(pth);
    if(!f.open(QIODevice::ReadOnly)) {
      mdxWarning << "Error, cannot open file '" << pth << "' for reading" << endl;
      return;
    }
    QTextStream ts(&f);
    QString content = ts.readAll();
    TransferFunction fct = TransferFunction::load(content);
    if(fct.empty()) {
      mdxWarning << "Error, file '" << pth << "' doesn't contain a valid transfer function" << endl;
      return;
    }
    if(work)
      workTransferDlg->setTransferFunction(fct);
    else
      mainTransferDlg->setTransferFunction(fct);
  }
   
  void ImgData::setMain16Bit(bool val)
  {
    if(val != Main16Bit) {
      Main16Bit = val;
      loadTex("Main");
    }
  }

  void ImgData::setWork16Bit(bool val)
  {
    if(val != Work16Bit) {
      Work16Bit = val;
      loadTex("Work");
    }
  }

  void ImgData::setMainClipVoxels(bool val)
  {
    if(val != MainClipVoxels)
      MainClipVoxels = val;
  }

  void ImgData::setWorkClipVoxels(bool val)
  {
    if(val != WorkClipVoxels)
      WorkClipVoxels = val;
  }

  void ImgData::setMainClipLabels(bool val)
  {
    if(val != MainClipLabels)
      MainClipVoxels = val;
  }

  void ImgData::setWorkClipLabels(bool val)
  {
    if(val != WorkClipLabels)
      WorkClipLabels = val;
  }

  // Slots for stack controls
  // Main stack
  void ImgData::mainShow(bool val)
  {
    if(val != stack->main()->isVisible()) {
      if(val)
        stack->main()->show();
      else
        stack->main()->hide();
      emit changedInterface();
      emit viewerUpdate();
    }
  }
  
  void ImgData::mainBright(int val)
  {
    double v = double(val) / 10000.0;
    if(stack->main()->brightness() != v) {
      stack->main()->setBrightness(v);
      emit changedInterface();
      emit viewerUpdate();
    }
  }
  
  void ImgData::mainOpacity(int val)
  {
    double v = double(val) / 10000.0;
    if(stack->main()->opacity() != v) {
      stack->main()->setOpacity(v);
      emit changedInterface();
      emit viewerUpdate();
    }
  }
  
  void ImgData::mainLabels(bool val)
  {
    if(val != stack->main()->labels()) {
      emit changedInterface();
      // Labels requires 16 bit
      stack->main()->setLabels(val);
      loadTex("Main");
      emit viewerUpdate();
    }
  }
  
  void ImgData::main16Bit(bool val)
  {
    if(val != Main16Bit) {
      emit changedInterface();
      Main16Bit = val;
      if(!stack->work()->labels())
        loadTex("Main");
      emit viewerUpdate();
    }
  }

  void ImgData::mainClipVoxels(bool val)
  {
    if(val != MainClipVoxels) {
      emit changedInterface();
      MainClipVoxels = val;
      emit viewerUpdate();
    }
  }

  void ImgData::mainClipLabels(bool val)
  {
    if(val != MainClipLabels) {
      emit changedInterface();
      MainClipLabels = val;
      emit viewerUpdate();
    }
  }    

  void ImgData::mainShading(bool val)
  {
    if(val != MainShading) {
      emit changedInterface();
      MainShading = val;
      emit viewerUpdate();
    }
  }    

  // Work stack
  void ImgData::workShow(bool val)
  {
    if(val != stack->work()->isVisible()) {
      emit changedInterface();
      if(val)
        stack->work()->show();
      else
        stack->work()->hide();
      emit viewerUpdate();
    }
  }
  
  void ImgData::workBright(int val)
  {
    double v = double(val) / 10000.0;
    if(stack->work()->brightness() != v) {
      emit changedInterface();
      stack->work()->setBrightness(v);
      emit viewerUpdate();
    }
  }
  
  void ImgData::workOpacity(int val)
  {
    double v = double(val) / 10000.0;
    if(stack->work()->opacity() != v) {
      emit changedInterface();
      stack->work()->setOpacity(v);
      emit viewerUpdate();
    }
  }
  
  void ImgData::workLabels(bool val)
  {
    if(val != stack->work()->labels()) {
      emit changedInterface();
      stack->work()->setLabels(val);
      loadTex("Work");
      emit viewerUpdate();
    }
  }
  
  void ImgData::work16Bit(bool val)
  {
    if(val != Work16Bit) {
      emit changedInterface();
      Work16Bit = val;
      if(!stack->work()->labels())
        loadTex("Work");
      emit viewerUpdate();
    }
  }

  void ImgData::workClipVoxels(bool val)
  {
    if(val != WorkClipVoxels) {
      emit changedInterface();
      WorkClipVoxels = val;
      emit viewerUpdate();
    }
  }

  void ImgData::workClipLabels(bool val)
  {
    if(val != WorkClipLabels) {
      emit changedInterface();
      WorkClipLabels = val;
      emit viewerUpdate();
    }
  }  

  void ImgData::workShading(bool val)
  {
    if(val != WorkShading) {
      emit changedInterface();
      WorkShading = val;
      emit viewerUpdate();
    }
  }      

  void ImgData::mainColorMap()
  {
    editMainTransferFunction();
    emit viewerUpdate();
  }

  void ImgData::workColorMap()
  {
    editWorkTransferFunction();
    emit viewerUpdate();
  }

  // Mesh
  void ImgData::surfBright(int val)
  {
    double v = double(val) / 10000.0;
    if(mesh->brightness() != v) {
      emit changedInterface();
      mesh->setBrightness(v);
      emit viewerUpdate();
    }
  }
  
  void ImgData::surfOpacity(int val)
  {
    double v = double(val) / 10000.0;
    if(mesh->opacity() != v) {
      emit changedInterface();
      mesh->setOpacity(v);
      emit viewerUpdate();
    }
  }
  
  void ImgData::surfBlend(bool val)
  {
    if(val != mesh->blending()) {
      emit changedInterface();
      mesh->setBlending(val);
      mesh->updateProperties();
      mesh->updateCC();
      emit viewerUpdate();
    }
  }
 
  // RSS FIXME Is there a better way to do this? 
  // Both Labeling and Heat can trigger a updateCC()
  static bool ccCalled;
  void ImgData::surfLabeling(const QString &labeling)
  {
    if(labeling != mesh->labeling()) {
      emit changedInterface();
      ccCalled = false;
      mesh->setLabeling(labeling);
      emit loadHeatChoices(mesh->id());
      if(!ccCalled) {
        mesh->updateProperties();
        mesh->updateCC();
        emit viewerUpdate();
      }
    }
  }

  void ImgData::surfSignal(const QString &signal)
  {
    if(signal != mesh->signal()) {
      emit changedInterface();
      mesh->setSignal(signal);
      mesh->updateProperties();
      mesh->updateCC();
      emit viewerUpdate();
    }
  } 

  void ImgData::surfHeat(const QString &heat)
  {
    if(heat != mesh->heat()) {
      emit changedInterface();
      mesh->setHeat(heat);
      mesh->updateProperties();
      ccCalled = true;
      mesh->updateCC();
      emit viewerUpdate();
    }
  }

  void ImgData::surfCull(bool val)
  {
    if(val != mesh->culling()) {
      emit changedInterface();
      mesh->setCulling(val);
      emit viewerUpdate();
    }
  }

  void ImgData::surfCellClipping(bool val)
  {
    if(val != mesh->cellClipping()) {
      emit changedInterface();
      mesh->setCellClipping(val);
      emit viewerUpdate();
    }
  } 

  void ImgData::surfShade(bool val)
  {
    if(val != mesh->shading()) {
      emit changedInterface();
      mesh->setShading(val);
      emit viewerUpdate();
    }
  }

  void ImgData::showTrans(bool val)
  {
    if(val != stack->showTrans()) {
      emit changedInterface();
      stack->setShowTrans(val);
      emit viewerUpdate();
    }
  }
  
  void ImgData::showBBox(bool val)
  {
    if(val != stack->showBBox()) {
      emit changedInterface();
      stack->setShowBBox(val);
      emit viewerUpdate();
    }
  }
  
  void ImgData::showScale(bool val)
  {
    if(val != stack->showScale()) {
      emit changedInterface();
      stack->setShowScale(val);
      updateStackSize();
      emit viewerUpdate();
    }
  }
  
  void ImgData::tieScales(bool val)
  {
    if(val != stack->tieScales()) {
      emit changedInterface();
      stack->setTieScales(val);
      emit viewerUpdate();
    }
  }

  void ImgData::ccUpdate(void)
  {
    emit viewerUpdate();
  }

  void ImgData::updateViewer(void)
  {
    emit viewerUpdate();
  } 

  void ImgData::updateColorMap(void)
  {
    emit viewerUpdate();
  } 

  // Map back and forth between slider and scale
  int ImgData::toSliderScale(double s) 
  {
    return int((s - 1.0f) * 15000.0f);
  }
  
  double ImgData::fromSliderScale(int i) 
  {
    return 1.0f + double(i) / 15000.0f;
  }
  
  // Scale slots in x,y,z
  void ImgData::scaleX(int val)
  {
    double newScale = fromSliderScale(val);
    if(newScale != stack->scale().x()) {
      emit changedInterface();
      if(!stack->tieScales())
        stack->setScale(Point3d(newScale, stack->scale().y(), stack->scale().z()));
      else
        stack->setScale(Point3d(newScale, newScale, newScale));
      updateStackSize();
      emit updateSliderScale();
      emit viewerUpdate();
    }
  }

  void ImgData::scaleY(int val)
  {
    double newScale = fromSliderScale(val);
    if(newScale != stack->scale().y()) {
      emit changedInterface();
      if(!stack->tieScales())
        stack->setScale(Point3d(stack->scale().x(), newScale, stack->scale().z()));
      else
        stack->setScale(Point3d(newScale, newScale, newScale));
      updateStackSize();
      emit updateSliderScale();
      emit viewerUpdate();
    }
  }

  void ImgData::scaleZ(int val)
  {
    double newScale = fromSliderScale(val);
    if(newScale != stack->scale().z()) {
      emit changedInterface();
      if(!stack->tieScales())
        stack->setScale(Point3d(stack->scale().x(), stack->scale().y(), newScale));
      else
        stack->setScale(Point3d(newScale, newScale, newScale));
      updateStackSize();
      emit updateSliderScale();
      emit viewerUpdate();
    }
  }

//  Point3d ImgData::imageGradientI(Point3i ipos)
//  {
//    uint ix = ipos.x();
//    uint iy = ipos.y();
//    uint iz = ipos.z();
//    Point3d grad(0, 0, 0);
//    const Point3d size(stack->size());
//    double xScale = (size.x() / stack->size().x()) * 2.0;
//    double yScale = (size.y() / stack->size().y()) * 2.0;
//    double zScale = (size.z() / stack->size().z()) * 2.0;
//    HVecUS& Data = currentData();
//    grad[0] = (Data[offset(ix, iy, iz) + 1] - Data[offset(ix, iy, iz) - 1]) / xScale;
//    grad[1] = (Data[offset(ix, iy, iz) + stack->size().x()] - Data[offset(ix, iy, iz) - stack->size().x()]) / yScale;
//    grad[2] = (Data[offset(ix, iy, iz) + stack->size().x() * stack->size().y()]
//               - Data[offset(ix, iy, iz) - stack->size().x() * stack->size().y()]) / zScale;
//    return grad;
//  }
//  
//  Point3d ImgData::imageGradientW(Point3d worldpos)
//  {
//    Point3i ipos(worldToImagei(Point3d(worldpos)));
//    return imageGradientI(ipos);
//  }
  
  uint ImgData::imageLevel(Point3d worldpos)
  {
    HVecUS& Data = currentData();
    Point3i ipos = worldToImagei(Point3d(worldpos));
    return Data[offset(ipos)];
  }
  
  Point2i ImgData::imageMinMax()
  {
    HVecUS& Data = currentData();
    uint min = 65535;
    uint max = 0;
    for(size_t i = 0; i < stack->storeSize(); ++i) {
      uint val = Data[i];
      if(val > max)
        max = val;
      if(val < min)
        min = val;
    }
    return Point2i(min, max);
  }

  bool ImgData::fillLabel(const QString &storeName, int oldLabel, int newLabel)
  {
    if(oldLabel < 0 or newLabel < 0 or oldLabel > 65535 or newLabel > 65535) {
      mdxInfo << QString("ImgData::fillLabel Bad label, old %1, new %2").arg(oldLabel).arg(newLabel) << endl;
      return false;
    }
    if(oldLabel == newLabel)
      return false;

    auto *store = stack->getStore(storeName);
    if(!store)
      return false;
    auto &data = store->data();
    auto &bBox = labelBBox[storeName];
    auto &centers = labelCenters[storeName];
    // Resize if new label FIXME use labelCenters size() as maxLabel
    if(uint(newLabel) > centers.size()) {
      centers.resize(newLabel+1);
      bBox.resize(newLabel+1);
    }
    // Fill old label with new
    auto &oldBBox = bBox[oldLabel];
    auto &newBBox = bBox[newLabel];
    if(oldBBox.empty()) {
      mdxInfo << "ImgData::fillLabel Old bBox empty" << endl;
      return false;
    }
    if(newLabel > 0 and newBBox.empty()) {
      mdxInfo << "ImgData::fillLabel New bBox empty" << endl;
      return false;
    }

    for(int z = oldBBox.pmin().z(); z < oldBBox.pmax().z(); z++)
      for(int y = oldBBox.pmin().y(); y < oldBBox.pmax().y(); y++)
        for(int x = oldBBox.pmin().x(); x < oldBBox.pmax().x(); x++)
          if(data[offset(x,y,z)] == oldLabel)
            data[offset(x,y,z)] = newLabel;

    // If new not 0, calc new center & bbox
    if(newLabel > 0) {
      newBBox |= oldBBox;
      long count = 0;
      Point3l center(0,0,0);
      for(int z = newBBox.pmin().z(); z < newBBox.pmax().z(); z++)
        for(int y = newBBox.pmin().y(); y < newBBox.pmax().y(); y++)
          for(int x = newBBox.pmin().x(); x < newBBox.pmax().x(); x++)
            if(data[offset(x,y,z)] == newLabel) {
              center += Point3l(x,y,z);
              count++;
            }
      if(count > 0)
        centers[newLabel] = Point3us(center.x()/count, center.y()/count, center.z()/count);
      else
        centers[newLabel] = Point3us(0,0,0);
    }
    // Update voxels
    loadTex(storeName, oldBBox);
    // Update label centers tex
    loadLabelCenterTex(storeName, newLabel, newLabel);
    loadLabelCenterTex(storeName, oldLabel, oldLabel);

    // Clear old center and bbox
    oldBBox.reset();
    centers[oldLabel] = Point3us(0,0,0);

    return true;
  }
}
