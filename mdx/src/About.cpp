//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "About.hpp"
#include "Version.hpp"
#include <QThread>
#include <QString>

QString aboutText()
{
  return QString(
    "<h2>MorphoDynamX " VERSION " r" REVISION "</h2>"
    "A tool for computational morphodynamics, powered by cell complexes.<br><br>"
    "Copyright 2012-2018 Richard S. Smith and collaborators.<br><br>"
    "MorphoDynamX is licensed under the "
    "<html><style type='text/css'></style><a href='http://www.gnu.org/licenses/gpl-2.0.html'>GNU "
    "GPLv2.</a></html><br><br>"
    "Please visit "
    "<html><style type='text/css'></style><a href='http://www.MorphoDynamX.org'>MorphoDynamX.org</a></html>"
    " for more information.<br><br>"
    "If you use MorphoDynamX in your research, please"
    " <html><style type='text/css'></style><a href='http://dx.doi.org/10.7554/eLife.05864'>reference</a></html>"
    " us. MorphoDynamX uses open source libraries from many contributors. Please see the tooltips"
    " for the individual processes to find references to specific algorithms"
    " implemented in MorphoDynamX.<br><br>"
    "Funding support:<dl>"
    "- SystemsX.ch & the Swiss National Science Foundation (SNF)<br>"
    "- Max Planck Institute for Plant Breeding Research (MPIPZ)<br>"
    "- German Federal Ministry of Education and Research (BMBF)<br>"
    "- Human Frontiers Science Program (HFSP)<br>"
    "- German Research Foundation (DFG)<br>");
}

QString introText()
{
  return QString("MorphoDynamX version: " VERSION " revision: " REVISION "\n   created in thread: 0x%1")
         .arg(quintptr(QThread::currentThread()), -16);
}
