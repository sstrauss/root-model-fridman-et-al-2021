//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#ifndef PCA_HPP
#define PCA_HPP

#include <Process.hpp>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>

namespace mdx 
{
  // Class to calculate PCA
  class mdx_EXPORT PCA
  {
    public:

    Point3d p1, p2, p3;
    Point3d ev;
    gsl_matrix *mat;
    gsl_vector *eval;
    gsl_matrix *evec;
    gsl_eigen_symmv_workspace *w;

    PCA() : mat(gsl_matrix_alloc(3, 3)), eval(gsl_vector_alloc(3)),
            evec(gsl_matrix_alloc(3, 3)), w(gsl_eigen_symmv_alloc(3)) {} 

    ~PCA()
    {
      gsl_eigen_symmv_free(w);
      gsl_vector_free(eval);
      gsl_matrix_free(evec);
      gsl_matrix_free(mat);
    }
    bool operator()(const Matrix3d& corr);
  };
}
#endif
