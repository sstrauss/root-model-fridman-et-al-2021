//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2019 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include "MeshProcessAxis.hpp"

#include <QFileDialog>

namespace mdx 
{
  
  bool SaveAxisAttr::initialize(QWidget* parent)
  {
    QString fileName = parm("Output File");
    if(fileName.isEmpty() and parent)
      fileName = QFileDialog::getSaveFileName(parent, "Choose spreadsheet file to save", QDir::currentPath(),
                                              "CSV files (*.csv)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".csv", Qt::CaseInsensitive))
      fileName += ".csv";
    setParm("Output File", fileName);
    return true;
  }
  
  bool SaveAxisAttr::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, const CCIndexSymTensorAttr &axisAttr, const QString &fileName)
  {
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
      throw QString("%1: Unable to write to output file").arg(name());

    QTextStream ts(&file);
   
    if(axisAttr.size() == 0)
      throw QString("%1: Axis is empty").arg(name());

    ts << "CCIndex, Label, PosX, PosY, PosZ, eVec1X, eVec1Y, eVec1Z, eVec2X, eVec2Y, eVec2Z, eVal1, eVal2, eVal3" << endl;


    for(const auto &pr : axisAttr) {
      auto cIdx = indexAttr[pr.first];
      ts << pr.first.value << "," << cIdx.label << "," << cIdx.pos.x() << "," <<  cIdx.pos.y() << "," << cIdx.pos.z() << ",";
      ts << pr.second.ev1().x() << "," << pr.second.ev1().y() << ","<< pr.second.ev1().z() << ",";
      ts << pr.second.ev2().x() << "," << pr.second.ev2().y() << ","<< pr.second.ev2().z() << ",";
      ts << pr.second.evals().x() << "," << pr.second.evals().y() << ","<< pr.second.evals().z() << endl;
    }
  
    return true;
  }
  REGISTER_PROCESS(SaveAxisAttr);
  
  bool LoadAxisAttr::initialize(QWidget* parent)
  {
    QString fileName = parm("Input File");
    if(fileName.isEmpty() and parent)
      fileName = QFileDialog::getSaveFileName(parent, "Choose spreadsheet file to load", QDir::currentPath(),
                                              "CSV files (*.csv)");
    if(fileName.isEmpty())
      return false;
    if(!fileName.endsWith(".csv", Qt::CaseInsensitive))
      fileName += ".csv";
    setParm("Input File", fileName);
    return true;
  }
  
  bool LoadAxisAttr::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexSymTensorAttr &axisAttr, const QString &filename)
  {
//     QFile file(filename);
//     if(!file.open(QIODevice::ReadOnly)) {
//       setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
//       return false;
//     }
//     QTextStream ts(&file);
//     QString line = ts.readLine();
//     QStringList fields = line.split(",");
//     QHash<QString, int> fieldMap;
//     int field_xMax, field_yMax, field_zMax;
//     int field_xMin, field_yMin, field_zMin;
//     int field_k1, field_k2, field_k3;
//     int field_label;
//     if(fields.size() < 10) {
//       setErrorMessage(QString("File '%1' should be a CSV file with at least 10 columns").arg(filename));
//       return false;
//     }
//     // First 7 columns of csv files are the same for all cell axis types: 
// 		// Label, first direction (x,y,z), second direction (x,y,z)
//     field_label = fields.indexOf("Label");
//     if(field_label < 0)
//       return setErrorMessage("Error, no field 'Label' in the CSV file");
//     field_xMax = max(fields.indexOf("xMax"), fields.indexOf("x1"));
//     if(field_xMax < 0)
//       return setErrorMessage("Error, no field 'xMax' nor 'x1' in the CSV file");
//     field_yMax = max(fields.indexOf("yMax"), fields.indexOf("y1"));
//     if(field_yMax < 0)
//       return setErrorMessage("Error, no field 'yMax' nor 'y1' in the CSV file");
//     field_zMax = max(fields.indexOf("zMax"), fields.indexOf("z1"));
//     if(field_zMax < 0)
//       return setErrorMessage("Error, no field 'zMax' nor 'z1' in the CSV file");
//     field_xMin = max(fields.indexOf("xMin"), fields.indexOf("x2"));
//     if(field_xMin < 0)
//       return setErrorMessage("Error, no field 'xMin' nor 'x2' in the CSV file");
//     field_yMin = max(fields.indexOf("yMin"), fields.indexOf("y2"));
//     if(field_yMin < 0)
//       return setErrorMessage("Error, no field 'yMin' nor 'y2' in the CSV file");
//     field_zMin = max(fields.indexOf("zMin"), fields.indexOf("z2"));
//     if(field_zMin < 0)
//       return setErrorMessage("Error, no field 'zMin' nor 'z2' in the CSV file");

//     // Next columns of csv files are called differently depending on cell axis types.  
// 		// norm of first vector, norm of second vector, norm of third vector (= 0 for 2D cell axis) 
//     if(type == QString("PDG")) {
//       field_k1 = max(fields.indexOf("stretchRatioMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'stretchRatioMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("stretchRatioMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'stretchRatioMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("stretchRatioNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'stretchRatioNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("curvature")) {
//       field_k1 = max(fields.indexOf("1/radiusCurvMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field '1/radiusCurvMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("1/radiusCurvMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field '1/radiusCurvMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("1/radiusCurvNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field '1/radiusCurvNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("fibril")) {
//       field_k1 = max(fields.indexOf("alignmentMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'alignmentMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("alignmentMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'alignmentMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("alignmentNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'alignmentNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("polarization")) {
//       field_k1 = max(fields.indexOf("signalMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'signalMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("signalMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'signalMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("signalNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'signalNormal' nor 'k3' in the CSV file");
//     } else if(type == QString("PCA")) {
//       field_k1 = max(fields.indexOf("pcaMax"), fields.indexOf("k1"));
//       if(field_k1 < 0)
//         return setErrorMessage("Error, no field 'pcaMax' nor 'k1' in the CSV file");
//       field_k2 = max(fields.indexOf("pcaMin"), fields.indexOf("k2"));
//       if(field_k2 < 0)
//         return setErrorMessage("Error, no field 'pcaMin' nor 'k2' in the CSV file");
//       field_k3 = max(fields.indexOf("pcaNormal"), fields.indexOf("k3"));
//       if(field_k3 < 0)
//         return setErrorMessage("Error, no field 'pcaNormal' nor 'k3' in the CSV file");
//     }	else {
// 		  setErrorMessage(QString("Unknown cell axis type"));
//       return false;
//     }
 
//     int nb_fields = fields.size();
  
//     mesh->clearCellAxis();
//     IntSymTensorAttr& cellAxis = mesh->cellAxis();
  
//     int linenum = 1;
//     while(ts.status() == QTextStream::Ok) {
//       ++linenum;
//       fields = ts.readLine().split(",");
//       if(fields.empty() or fields.size() == 1)
//         break;
  
//       if(fields.size() < nb_fields)
//         return setErrorMessage(
//           QString("Error on line %1: not enough fields (Expected: %2, Read: %3)").arg(linenum).arg(nb_fields).arg(
//             fields.size()));
  
//       bool ok;
//       int label = fields[field_label].toInt(&ok);
//       if(!ok) {
//         setErrorMessage(
//           QString("Error line %1: invalid label '%2'").arg(linenum).arg(fields[field_label].trimmed()));
//         return false;
//       }
  
//       Point3f ev1, ev2, evals;
//       ev1.x() = fields[field_xMax].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid xMax '%2'").arg(linenum).arg(fields[field_xMax]));
//       ev1.y() = fields[field_yMax].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid yMax '%2'").arg(linenum).arg(fields[field_yMax]));
//       ev1.z() = fields[field_zMax].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid zMax '%2'").arg(linenum).arg(fields[field_zMax]));
  
//       ev2.x() = fields[field_xMin].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid xMin '%2'").arg(linenum).arg(fields[field_xMin]));
//       ev2.y() = fields[field_yMin].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid yMin '%2'").arg(linenum).arg(fields[field_yMin]));
//       ev2.z() = fields[field_zMin].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid zMin '%2'").arg(linenum).arg(fields[field_zMin]));
  
//       evals.x() = fields[field_k1].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid k1 '%2'").arg(linenum).arg(fields[field_k1]));
//       evals.y() = fields[field_k2].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid k2 '%2'").arg(linenum).arg(fields[field_k2]));
//       evals.z() = fields[field_k3].toFloat(&ok);
//       if(!ok)
//         return setErrorMessage(QString("Error line %1: invalid k3 '%2'").arg(linenum).arg(fields[field_k3]));
  
//       // make sure ev1 and ev2 are unit vectors before storing in cellAxis. It is not the case in the older csv files
//       ev1 = ev1 / norm(ev1);
//       ev2 = ev2 / norm(ev2);
  
//       cellAxis[label] = SymmetricTensor(ev1, ev2, evals);
//     }
  
//     mesh->setCellAxisType(type);

//     // Display cell axis according to their type, with the parameters from the GUI
//     QStringList parms;
//     if(type == "curvature") {
// 			DisplayTissueCurvature *proc = getProcessParms<DisplayTissueCurvature>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "PDG") {
// 			DisplayPDGs *proc = getProcessParms<DisplayPDGs>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
// 			// DisplayPDGs::initialize() changes the display parms in case they commend to draw the PDGs projected 
// 			// on a Bezier and cellAxisBezier doesn't exist yet. 
//   		// ALR: I don't know how to update the GUI with the new parms. 
//       proc->initialize(parms, 0);
//       return proc->run(parms);
//     } else if(type == "fibril") {
//       DisplayFibrilOrientations *proc = getProcessParms<DisplayFibrilOrientations>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display tissue curvature process"));
//       return proc->run(parms);
//     } else if(type == "polarization") {
//       QStringList parms;
//       DisplayWallSignalOrientation *proc = getProcessParms<DisplayWallSignalOrientation>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display signal orientation process"));
//       return proc->run(parms);
//     } else if(type == "PCA") {
//       DisplayPCA *proc = getProcessParms<DisplayPCA>(this, parms);
// 	  	if(!proc)
// 	  	  throw(QString("Error, unable to create display PCA process"));
//       return proc->run(parms);
// 		}

//     mesh->setShowAxis("Cell Axis");
  
//     setStatus(QString("Loaded directions for %1 cells").arg(cellAxis.size()));
    return true;
  }
  REGISTER_PROCESS(LoadAxisAttr);

  bool ClearAxisAttr::run(const CCStructure &cs, const CCIndexDataAttr &indexAttr, CCIndexSymTensorAttr &axisAttr, bool eraseAll)
  {
    if(eraseAll) {
      axisAttr.clear();
      return true;
    }
    for(uint dim = 0; dim <= cs.maxDimension(); dim++)
      if(dim == 1)
        continue;
      else
        for(CCIndex c : cs.cellsOfDimension(dim))
          if(indexAttr[c].selected)
            axisAttr.erase(c);
  
    return true;
  }
  REGISTER_PROCESS(ClearAxisAttr);

  REGISTER_PROCESS(SetAxisLineWidth);
}
