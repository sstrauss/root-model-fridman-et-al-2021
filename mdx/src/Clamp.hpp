//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#ifndef CLAMP_H
#define CLAMP_H

#include <Config.hpp>

/**
 * \file Clamp.hpp
 *
 * Defines the clamp function
 */

//#include <config.h>
namespace mdx 
{
  /**
   * \brief A function to clamp a value to a range.
   * \param val The start value.
   * \param min The minimum value of the range.
   * \param max The maximum value of the range.
   *
   *  If \c min is more than \c max, the function returns \c max.
   */
  template <class T> T clamp(const T& val, const T& min, const T& max)
  {
    if(min >= max)
      return max;
    else if(val < min)
      return min;
    else if(val > max)
      return max;
    else
      return val;
  }
}

#endif
