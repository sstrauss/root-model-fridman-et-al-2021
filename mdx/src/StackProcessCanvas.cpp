//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2015 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
// 
#include <QFileDialog>

#include <StackProcessCanvas.hpp>
#include <Image.hpp>

#include <algorithm>
#include <cfloat>

#include <CImg.h>

using namespace cimg_library;

typedef CImg<ushort> CImgUS;

#ifdef _MSC_VER
static float roundf(float v) // Defined to work only for positive values
{
  return floor(v + 0.5);
}
#endif

namespace mdx 
{
  bool ClipStack::run()
  {
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool labels = stringToBool(parm("Labels"));
    if(labels and !input->labels())
      throw QString("%1::run Only labeled stacks can trim by label").arg(name());

    bool res = run(input, output, stringToBool(parm("Trim Canvas")), labels);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool ClipStack::run(const Store* input, Store* output, bool trimStack, bool labels)
  {
    // Setup matrices and planes for clip test
    Point3u clipDo;
    const Stack* stack = output->stack();
  
    // Indicator if in use
    clipDo[0] = (clip1()->enabled() or clip1()->grid() ? 1 : 0);
    clipDo[1] = (clip2()->enabled() or clip2()->grid() ? 1 : 0);
    clipDo[2] = (clip3()->enabled() or clip3()->grid() ? 1 : 0);
  
    // Frame matrix
    Matrix4d frm(stack->getFrame().worldMatrix());
  
    // Clip plane matrices
    Matrix4d clm[3];
    clm[0] = Matrix4d(clip1()->frame().inverse().worldMatrix());
    clm[1] = Matrix4d(clip2()->frame().inverse().worldMatrix());
    clm[2] = Matrix4d(clip3()->frame().inverse().worldMatrix());
  
    // Clipping planes, point normal form
    Point4f pn[6];
    pn[0] = Point4f(frm * clm[0]
                    * Point4d(clip1()->normal().x(), clip1()->normal().y(), clip1()->normal().z(), clip1()->width()));
    pn[1] = Point4f(frm * clm[0] 
                    * Point4d(-clip1()->normal().x(), -clip1()->normal().y(), -clip1()->normal().z(), clip1()->width()));
    pn[2] = Point4f(frm * clm[1]
                    * Point4d(clip2()->normal().x(), clip2()->normal().y(), clip2()->normal().z(), clip2()->width()));
    pn[3] = Point4f(frm * clm[1] 
                    * Point4d(-clip2()->normal().x(), -clip2()->normal().y(), -clip2()->normal().z(), clip2()->width()));
    pn[4] = Point4f(frm * clm[2]
                    * Point4d(clip3()->normal().x(), clip3()->normal().y(), clip3()->normal().z(), clip3()->width()));
    pn[5] = Point4f(frm * clm[2] 
                    * Point4d(-clip3()->normal().x(), -clip3()->normal().y(), -clip3()->normal().z(), clip3()->width()));

    if(labels) {
      IntTbbSet deleteLabels;

      auto sz = stack->size();
      const auto &inData = input->data();
      auto &outData = output->data();

      #pragma omp parallel for
      for(uint z = 0; z < sz.z(); z++)
        for(uint y = 0; y < sz.y(); y++)
          for(uint x = 0; x < sz.x(); x++)
            for(uint w = 0; w < 6; w++)
              if(clipDo[w/2]) {
                Point3d p = stack->imageToWorld(Point3i(x,y,z));
                if(pn[w] * Point4f(p.x(), p.y(), p.z(), 1.0) < 0.0) {
                  deleteLabels.insert(inData[stack->offset(x, y, z)]);
                  break;
                }
              }
      #pragma omp parallel for
      for(uint z = 0; z < sz.z(); z++)
        for(uint y = 0; y < sz.y(); y++)
          for(uint x = 0; x < sz.x(); x++) {
            size_t idx = stack->offset(x, y, z);
            if(deleteLabels.count(inData[idx]) > 0)
              outData[idx] = 0;
            else
              outData[idx] = inData[idx];
          }
    } else {
      // Put in host vector
      HVec4F Hpn;
      Hpn.resize(6);
      for(int i = 0; i < 6; i++)
        Hpn[i] = pn[i];
    
      // Call cuda to clip
      clipStackGPU(Point3i(stack->size()), Point3f(stack->step()), Point3f(stack->origin()), clipDo, Hpn, input->data(), output->data());
    }

    // Call autotrim
    if(trimStack) {
      AutoTrim trim(*this);
      bool result = trim.run(currentStack(), currentStack()->currentStore(), 0);
      if(!result)
        return false;
    }
  
    setStatus(QString("Stack %1 clipped to intersection of clipping planes").arg(stack->userId()));
    output->copyMetaData(input);
    output->changed();
    return true;
  }
  REGISTER_PROCESS(ClipStack);

  bool ReverseStack::run()
  {
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool reverse_x = stringToBool(parm("X"));
    bool reverse_y = stringToBool(parm("Y"));
    bool reverse_z = stringToBool(parm("Z"));
    if(run(output, input, reverse_x, reverse_y, reverse_z)) {
      input->hide();
      output->show();
      return true;
    }
    return false;
  }
  bool ReverseStack::run(Store* output, const Store* input, bool rx, bool ry, bool rz)
  {
    const Stack* stack = output->stack();
    Point3u size = stack->size();
    if(input == output) {
      HVecUS& data = output->data();
      if(rx and ry and rz) {
        ushort* s = &data[0];
        ushort* e = s + data.size() - 1;
        uint nb_iter = data.size() >> 1;
        for(uint i = 0; i < nb_iter; ++i) {
          std::swap(*e--, *s++);
        }
        output->changed();
      } else if(rx xor ry xor rz)     // Only one if true
      {
        int dx = 1, dy = 1, dz = 1;
        Point3u start;
        if(rx) {
          dx = -1;
          start.x() = size.x() - 1;
          size.x() >>= 1;
        } else if(ry) {
          dy = -1;
          start.y() = size.y() - 1;
          size.y() >>= 1;
        } else {
          dz = -1;
          start.z() = size.z() - 1;
          size.z() >>= 1;
        }
        for(uint z = 0, z1 = start.z(); z < size.z(); ++z, z1 += dz)
          for(uint y = 0, y1 = start.y(); y < size.y(); ++y, y1 += dy)
            for(uint x = 0, x1 = start.x(); x < size.x(); ++x, x1 += dx) {
              std::swap(data[stack->offset(x, y, z)], data[stack->offset(x1, y1, z1)]);
            }
        output->changed();
      } else if(rx and ry) {
        uint size_xy = size.x() * size.y();
        uint to_shift = size_xy >> 1;
        for(uint z = 0; z < size.z(); ++z) {
          ushort* s = &data[stack->offset(0, 0, z)];
          ushort* e = s + size_xy - 1;
          for(uint i = 0; i < to_shift; ++i)
            std::swap(*e--, *s++);
        }
        output->changed();
      } else if(rz)     // and rx or ry
      {
        Point3u start;
        int dx = 1, dy = 1;
        start.z() = size.z() - 1;
        if(rx) {
          start.x() = size.x() - 1;
          dx = -1;
        } else       // if(ry)
        {
          start.y() = size.y() - 1;
          dy = -1;
        }
        uint half_z = size.z() >> 1;
        for(uint z = 0, z1 = start.z(); z < half_z; ++z, --z1)
          for(uint y = 0, y1 = start.y(); y < size.y(); ++y, y1 += dy)
            for(uint x = 0, x1 = start.x(); x < size.x(); ++x, x1 += dx) {
              std::swap(data[stack->offset(x, y, z)], data[stack->offset(x1, y1, z1)]);
            }
        if(size.z() & 1)       // If there is an off number of planes
        {
          uint z = half_z;
          uint end_y = (ry ? size.y() >> 1 : size.y());
          uint end_x = (rx ? size.x() >> 1 : size.x());
          for(uint y = 0, y1 = start.y(); y < end_y; ++y, y1 += dy)
            for(uint x = 0, x1 = start.x(); x < end_x; ++x, x1 += dx) {
              std::swap(data[stack->offset(x, y, z)], data[stack->offset(x1, y1, z)]);
            }
        }
        output->changed();
      }
    } else if(rx or ry or rz) {
      const HVecUS& data = input->data();
      HVecUS& out = output->data();
      Point3u size = stack->size();
      int dx = (rx ? -1 : 1), dy = (ry ? -1 : 1), dz = (rz ? -1 : 1);
      uint startx = (rx ? size.x() - 1 : 0);
      uint starty = (ry ? size.y() - 1 : 0);
      uint startz = (rz ? size.z() - 1 : 0);
      for(uint z1 = 0, z2 = startz; z1 < size.z(); ++z1, z2 += dz)
        for(uint y1 = 0, y2 = starty; y1 < size.y(); ++y1, y2 += dy)
          for(uint x1 = 0, x2 = startx; x1 < size.x(); ++x1, x2 += dx)
            out[stack->offset(x2, y2, z2)] = data[stack->offset(x1, y1, z1)];
      output->copyMetaData(input);
      output->changed();
    }
    return true;
  }
  REGISTER_PROCESS(ReverseStack);

  bool ChangeVoxelSize::run()
  {
    Stack* s = currentStack();
    Point3d nv(parm("X").toFloat(), parm("Y").toFloat(), parm("Z").toFloat());
    return run(s, nv);
  } 
  bool ChangeVoxelSize::run(Stack* stack, Point3d nv)
  {
    if(nv.x() <= 0)
      nv.x() = stack->step().x();
    if(nv.y() <= 0)
      nv.y() = stack->step().y();
    if(nv.z() <= 0)
      nv.z() = stack->step().z();
    Point3d ratio = nv / stack->step();
    stack->setStep(nv);
    stack->setOrigin(multiply(stack->origin(), ratio));
    stack->main()->changed();
    stack->work()->changed();
    return true;
  }
  REGISTER_PROCESS(ChangeVoxelSize);

  bool ResizeCanvas::run()
  {
    Stack* s = currentStack();
    Point3i ds(parm("X").toInt(), parm("Y").toInt(), parm("Z").toInt());
    return run(s, stringToBool(parm("Relative")), stringToBool(parm("Center")), ds);
  }
  bool ResizeCanvas::run(Stack* stack, bool isRelative, bool center, Point3i ds)
  {
    Point3i oldSize(stack->size());
    Point3i newSize(stack->size());
  
    HVecUS& MainData = stack->main()->data();
    HVecUS& WorkData = stack->work()->data();
  
    if(isRelative)
      newSize += ds;
    else
      for(int i = 0; i < 3; ++i)
        if(ds[i] > 0)
          newSize[i] = ds[i];

    if(newSize == oldSize)
      return true;

    // Check new size
    for(int i = 0; i < 3; ++i)
      if(newSize[i] < 0)
        throw(QString("Negative size"));
  
    {
      HVecUS mi = resize(MainData, oldSize, newSize, center);
      MainData.swap(mi);
    }
    {
      HVecUS wi = resize(WorkData, oldSize, newSize, center);
      WorkData.swap(wi);
    }
    stack->setSize(Point3u(newSize));
  
    stack->main()->changed();
    stack->work()->changed();
  
    return true;
  }
  REGISTER_PROCESS(ResizeCanvas);

  bool ScaleStack::run()
  {
    Stack* s = currentStack();
    Point3d newsize(parm("X").toFloat(), parm("Y").toFloat(), parm("Z").toFloat());
    return run(s, newsize, stringToBool(parm("Percent")));
  } 
  bool ScaleStack::run(Stack* stack, Point3d newsize, bool percent)
  {
    if(newsize.x() <= 0 and newsize.y() <= 0.0 and newsize.z() <= 0) {
      setErrorMessage("Error, no rescale specified.");
      return false;
    }
    Point3u size = stack->size();
    Point3d step = stack->step();
  
    HVecUS& MainData = stack->main()->data();
    HVecUS& WorkData = stack->work()->data();
  
    CImgUS mainImage(MainData.data(), size.x(), size.y(), size.z(), 1, false);
    CImgUS workImage(WorkData.data(), size.x(), size.y(), size.z(), 1, false);
  
    if(percent)
      for(int i = 0; i < 3; i++)
        newsize[i] *= size[i] / 100.0;
  
    for(int i = 0; i < 3; i++)
      if(newsize[i] > 0) {
        step[i] *= size[i] / newsize[i];
        size[i] = newsize[i];
      }
  
    mainImage.resize(size.x(), size.y(), size.z(), 1, (stack->main()->labels() ? 1 : 5));
    workImage.resize(size.x(), size.y(), size.z(), 1, (stack->work()->labels() ? 1 : 5));
  
    stack->setSize(size);
    stack->setStep(step);
  
    memcpy(MainData.data(), mainImage.data(), MainData.size() * 2);
    memcpy(WorkData.data(), workImage.data(), WorkData.size() * 2);
  
    stack->main()->changed();
    stack->work()->changed();
  
    return true;
  }
  REGISTER_PROCESS(ScaleStack);

  bool ShiftStack::run()
  {
    Stack* s = currentStack();
    Point3i ds(parm("X").toInt(), parm("Y").toInt(), parm("Z").toInt());
    return run(s, stringToBool(parm("Origin")), ds);
  } 
  bool ShiftStack::run(Stack* stack, bool origin, Point3i ds)
  {
    Point3u size = stack->size();
  
    Store* main = stack->main();
    Store* work = stack->work();
  
    HVecUS& MainData = main->data();
    HVecUS& WorkData = work->data();
  
    if(origin)
      stack->setOrigin(stack->origin() + Point3d(ds)); // RSS why is ds Point3i?
    else {
      CImgUS mainImage(MainData.data(), size.x(), size.y(), size.z(), 1, true);
      mainImage.shift(ds.x(), ds.y(), ds.z(), 0, 0);
  
      CImgUS workImage(WorkData.data(), size.x(), size.y(), size.z(), 1, true);
      workImage.shift(ds.x(), ds.y(), ds.z(), 0, 0);
    }
  
    main->changed();
    work->changed();
    return true;
  }
  REGISTER_PROCESS(ShiftStack);

  bool AutoTrim::run()
  {
    Stack *stack = currentStack();
    if(!stack) throw(QString("No current stack"));

    Store *store = stack->currentStore();
    if(!store or store->empty()) throw(QString("No current store"));
  
    return run(stack, store, parm("Threshold").toInt());
  } 
  bool AutoTrim::run(Stack* stack, Store* store, int threshold)
  {
    Point3u size = stack->size();
    Point3u pmin(stack->size().x() + stack->size().y() + stack->size().z()), pmax(0u);
    progressStart("Auto-trim stack", 2 * size.y() * size.z());
    int adv = 0, adv_step = 2 * size.y() * size.z() / 100;
    const HVecUS& data = store->data();
    setStatus("Search bounds.");
    for(uint z = 0, k = 0; z < size.z(); ++z)
      for(uint y = 0; y < size.y(); ++y, ++adv) {
        for(uint x = 0; x < size.x(); ++x, ++k) {
          if(data[k] > threshold) {
            if(x < pmin.x())
              pmin.x() = x;
            if(y < pmin.y())
              pmin.y() = y;
            if(z < pmin.z())
              pmin.z() = z;
            if(x > pmax.x())
              pmax.x() = x;
            if(y > pmax.y())
              pmax.y() = y;
            if(z > pmax.z())
              pmax.z() = z;
          }
        }
        if((adv % adv_step == 0)and !progressAdvance(adv))
          userCancel();
      }
    Point3u newSize = (pmax - pmin) + 1u;
    if(newSize == size) {
      setStatus("No change done, the volume already occupies the whole space.");
      return true;
    }
    setStatus("Copy data.");
    unsigned int totSize = newSize.x() * newSize.y() * newSize.z();
    if(totSize == 0)
      throw QString("Error, found a volume of 0, is the image empty?");
    HVecUS mainData(totSize);
    HVecUS workData(totSize);
    unsigned int stride_y = (size.y() - newSize.y()) * size.x();
    ushort* srcMain = &stack->main()->data()[stack->offset(Point3i(pmin))];
    ushort* dstMain = &mainData[0];
    ushort* srcWork = &stack->work()->data()[stack->offset(Point3i(pmin))];
    ushort* dstWork = &workData[0];
    for(uint z = 0; z < newSize.z(); ++z) {
      for(uint y = 0; y < newSize.y(); ++y, ++adv) {
        memcpy(dstMain, srcMain, sizeof(ushort) * newSize.x());
        dstMain += newSize.x();
        srcMain += size.x();
  
        memcpy(dstWork, srcWork, sizeof(ushort) * newSize.x());
        dstWork += newSize.x();
        srcWork += size.x();
      }
      srcMain += stride_y;
      srcWork += stride_y;
      if((adv % adv_step == 0)and !progressAdvance(adv))
        userCancel();
    }
  
    if(!progressAdvance(2 * size.y() * size.z()))
      userCancel();
    setStatus("Resize actual stack.");
  
    // Compute new origin
    stack->setSize(newSize);
  
    stack->setOrigin(stack->imageToWorld(Point3d(pmin) - Point3d(.5, .5, .5)));
  
    stack->main()->data() = mainData;
    stack->work()->data() = workData;
  
    stack->main()->changed();
    stack->work()->changed();
    return true;
  }
  REGISTER_PROCESS(AutoTrim);
}
