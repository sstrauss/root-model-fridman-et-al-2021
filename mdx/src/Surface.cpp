//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//

#include <iostream>
#include <cstdio>
#include <cmath>

#include <Function.hpp>
#include <Contour.hpp>
#include <Information.hpp>

#include "Surface.hpp"

namespace mdx
{
  bool Surface::initialCell(CellTissue &T, double size, int sides) 
  { 
    // By default just crease a cell at the origin
    std::vector<vertex> poly;
    // Center
    vertex c; 
    c->pos = Point3d(0,0,0);
    c->nrml = Point3d(0,0,1);
    poly.push_back(c);

    // Cell corners
    for(int i = 0; i < sides; i++) {
      vertex v;
      double a = double(i)/sides * M_PI * 2;
      v->pos = Point3d(size * cos(a), size * sin(a), 0);
      v->nrml = Point3d(0,0,1);
      poly.push_back(v);
    }
    return T.addCell(poly);
  }
}
