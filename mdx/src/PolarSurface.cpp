//
// This file is part of MorphoDynamX - http://www.MorphoDynamX.org
// Copyright (C) 2012-2016 Richard S. Smith and collaborators.
//
// If you use MorphoDynamX in your work, please cite:
//   http://dx.doi.org/10.7554/eLife.05864
//
// MorphoDynamX is free software, and is licensed under under the terms of the 
// GNU General (GPL) Public License version 2.0, http://www.gnu.org/licenses.
//
#include <PolarSurface.hpp>

namespace mdx
{
  bool PolarSurface2D::processParms()
  {
    // scalar parameters
    surfaceScale = parm("Surface Scale").toDouble();
    growthScaleOutMult = parm("Growth Scale Out Mult").toDouble();
    growthScaleInMult = parm("Growth Scale In Mult").toDouble();
    rootSearchMaxSteps = parm("Root Search Max Steps").toInt();

    // surface contour and shape
    surfaceContourFile = parm("Surface Contour");
    samplePositions(surfaceContourFile, sampledPos);

    // growth function of position
    growthType = parm("Growth Type").toInt();
    if(growthType == ARCLENGTH)
    {
      arcLengthGrowthFile = parm("ArcLength Growth");
      sampleIntegral(arcLengthGrowthFile, sampledGrowth);
    }

    // growth function of time
    growthScaleFile = parm("Growth Scale Func");
    growthScaleFunc.setFile(growthScaleFile);

    return true;
  }

  void PolarSurface2D::samplePositions(QString &contourFile, Point3d *pos)
  {
    Contour contour(contourFile);
    Point3d curr, prev;

    // Calculate total arclength of contour
    double totalArcLength = 0.;
    prev = surfaceScale * contour(0.0);
    for(int i = 1 ; i <= SAMPLES ; i++)
    {
      curr = surfaceScale * contour(double(i) / double(SAMPLES));
      totalArcLength += norm(prev - curr);
      prev = curr;
    }

    // Re-parameterize by arclength
    dr = totalArcLength / SAMPLES;
    pos[0] = prev = curr = surfaceScale * contour(0.0);
    int i = 1, j = 1;
    for( ; i <= SAMPLES ; i++)
    {
      // We want to place pos[i] at a position on the contour
      // a distance of dr from pos[i-1].
      // We approximate this by stepping along the contour until we find a position
      // that is _further_ away than this, then linearly interpolating between
      // the previous position and this one.
      // (Why isn't this handled with something like a binary search?)
      double dist;
      while((dist = norm(pos[i-1] - curr)) < dr && j <= SAMPLES)
        curr = surfaceScale * contour(double(j++) / double(SAMPLES));
      if(j >= SAMPLES) break;
      pos[i] = ((dist - dr) * pos[i-1] + dr * curr) / dist;
    }
    // Fill in the rest of the positions by extrapolation
    Point3d final = pos[i-1];
    maxTangent = final - pos[i-2];
    j = i-1;
    for( ; i <= SAMPLES ; i++)
      pos[i] = final + double(i - j) * maxTangent;
    maxTangent /= dr;

    // Translate all of the positions so the start of the curve is at the origin
    Point3d v0 = pos[0];
    for(int i = 0 ; i <= SAMPLES ; i++)
      pos[i].set(pos[i].x() - v0.x(), pos[i].y() - v0.y(), pos[i].z());
  }

  void PolarSurface2D::sampleIntegral(QString &functionFile, double *v)
  {
    Function func(functionFile);
    double dx = 1.0 / SAMPLES, x = 0, sum = 0;
    bool negy = false;

    for(int i = 0 ; i <= SAMPLES ; i++)
    {
      v[i] = sum;
      x += dx;
      double y = func(x);
      if(y < 0.0) negy = true;
      sum += dx * y;
    }

    if(negy)
      mdxInfo << "Surface::sampleIntegral: Warning: " << functionFile
                       << " goes negative." << endl;
  }

  Point3d PolarSurface2D::polarToCartesian(double r, double theta) const
  {
    // r must be nonnegative, but throw the origin in here
    if(r <= 0.0) return sampledPos[0];

    // Otherwise, we must interpolate / extrapolate on sampledPos
    int rIdx(r / dr);
    if(rIdx > SAMPLES - 1) rIdx = SAMPLES - 1;
    double rFrac = r - double(rIdx) * dr;

    // Get position at r for theta = 0
    Point3d cartesian;
    if(rIdx == SAMPLES - 1)    // past the end of the curve, extrapolate
      cartesian = sampledPos[rIdx] + rFrac * maxTangent;
    else                       // on the curve, interpolate
      cartesian = ((dr - rFrac) * sampledPos[rIdx] + rFrac * sampledPos[rIdx+1]) / dr;

    // Rotate based on theta
    cartesian.set(cartesian.x() * cos(theta), cartesian.y(), -cartesian.x() * sin(theta));

    return cartesian;
  }

  Point3d PolarSurface2D::polarToCartesian(const PolarData &pd) const
  { 
    return polarToCartesian(pd.r,pd.theta); 
  }

  // Move vertex vp to closest point on a polar surface to a given cartesian point (cp),
  // starting from a given vertex (vsp)
  bool PolarSurface2D::setPoint(CCIndex vp, CCIndex vsp, Point3d cp)
  {
    PolarData &p = (*pAttr)[vp], &sp = (*pAttr)[vsp];
    CCIndexData &vIdx = (*indexAttr)[vp];

    // Return if already there
    computePos(vp);
    if(norm(vIdx.pos - cp) < DX)
      return true;
    
    bool found = true;
    if(norm(cp) <= 0.0)
      p.r = p.theta = 0.0;
    else {
      // Zip close point around to same angle as given point and start from there
      p = sp;
      p.theta = std::arg(std::complex<double>(cp.x(),-cp.z()));
      Point3d zp(cp.x(), 0.0, cp.y());
      if(norm(zp) <= DX) {
        p.r = norm(zp);
        computePos(vp);
      } else {
        int max = rootSearchMaxSteps;
        computePos(vp);
        Point3d pp;
        do {
          // Save previous position
          pp = vIdx.pos;
  
          // calculate normal
					double nr,na, sr,sa;
          na = p.r - DX;
          if(nr < 0.0) 
            nr = 0.0;
          sr = p.r + DX;
          na = sa = p.theta;
          Point3d n = polarToCartesian(nr,na), s = polarToCartesian(sr,sa);
          Point3d pn = s - n;
          pn /= norm(pn);
  
          // Project onto normal
          double dr = pn * (cp - pp);
  
          // Exit if small enough
          if(fabs(dr) < DX * 10) 
            break;
  
          // Set new arc length
          p.r += dr;
          if(p.r <= 0.0) {
            p.r = p.theta = 0.0;
            break;
          }

          computePos(vp);
        } while(--max > 0);
  
        // Print error if hit max iterations
        if(max <= 0) {
          mdxInfo << "Surface::SetPoint:Error failed to find root " << cp << endl;
          found = false;
        }
      }
      computeNormal(vp);
    }
    return found;
  }

  void PolarSurface2D::computePos(CCIndex v)
  {
    (*indexAttr)[v].pos = polarToCartesian((*pAttr)[v]);
  }

  void PolarSurface2D::computeNormal(CCIndex v)
  {
    PolarData &pd = (*pAttr)[v];
    Point3d normal;
    if(pd.r <= DX) normal.set(0,1,0);
    else
    {
      // compute from cross product of (approximate) tangent vectors
      PolarData north(pd.r - DX, pd.theta);
      PolarData south(pd.r + DX, pd.theta);
      PolarData east(pd.r, pd.theta + DX);
      PolarData west(pd.r, pd.theta - DX);

      Point3d ns = polarToCartesian(north) - polarToCartesian(south);
      Point3d we = polarToCartesian(west) - polarToCartesian(east);
      normal = ns ^ we;
      normal /= norm(normal);
    }
    (*indexAttr)[v].nrml = normal;
  }

  // * The rest of the stuff
  bool PolarSurface2D::initialize(CellTissue &tissue, PolarAttr *_pAttr, CCIndexDataAttr *_indexAttr)
  {
    pAttr = _pAttr;
    indexAttr = _indexAttr;
    // Do point inversion for the points
    const CCIndexVec &vertices = tissue.cellStructure().vertices(); 
    #pragma omp parallel for
    for(uint i = 0; i < vertices.size(); i++) {
      CCIndex v = vertices[i];
      CCIndexData &vIdx = (*indexAttr)[v];
      // project it to the surface., choose closest as guess

      // How to find all neighbor vertices to pick the closest
      // Grab neighbor vertices
      //CCIndex vP, vN;
      //{
        //CCStructure::CellTuple tuple(tissue.cs,vtx);
        //vP = tuple.other(0);
        //tuple.flip(1);
        //vN = tuple.other(0);
      //}
      //CCIndexData &dN = (*indexAttr)[vN];
      //CCIndexData &dP = (*indexAttr)[vP];
      //setPoint(vtx, (norm(dN.pos - dV.pos) < norm(dP.pos - dV.pos) ? vN : vP), dV.pos);
      setPoint(v, v, vIdx.pos);
//mdxInfo << __FILE__ << " " << __LINE__ << endl;
    }
    // Set positions in dual graph (centers)
    const CCIndexVec &cells = tissue.cellStructure().vertices(); 
    #pragma omp parallel for
    for(uint i = 0; i < cells.size(); i++) {
      CCIndex c = cells[i];
      CCIndexData &cIdx = (*indexAttr)[c];
      setPoint(c, c, cIdx.pos);
    } 
    return true;
  }

  bool PolarSurface2D::initialCell(CellTissue &tissue, double radius, int sides)
  {
    // The initial cell is a single cell in the xy plane at the origin
    // with the specified (outer) radius and number of sides.
 
    // Clear current data
    tissue.clear();

    // Create the face
    CCIndex face = CCIndexFactory.getIndex();
    PolarData &fdata = (*pAttr)[face];
    fdata.r = 0;
    fdata.theta = 0;
    computePos(face);
    computeNormal(face);

    // Create the vertices
    std::vector<CCIndex> polygon;
    double dth = 2.*M_PI/sides;
    for(int i = 0; i < sides; ++i)
    {
      CCIndex v = CCIndexFactory.getIndex();
      polygon.push_back(v);
      PolarData &vdata = (*pAttr)[v];
      vdata.r = radius;
      vdata.theta = i * dth;
      computePos(v);
      computeNormal(v);
    }

    CCStructure &cs = tissue.cellStructure();

    // Add vertices to complex
    for(uint i = 0 ; i < polygon.size() ; i++)
      if(!cs.hasCell(polygon[i]))
        if(!cs.addCell(polygon[i]))
          throw(QString("PolarSurface2D::initialCell: Unable to add vertex"));

    // Add edges to complex and accumulate face boundary chain
    CCStructure::BoundaryChain faceBoundary;
    for(uint i = 0 ; i < polygon.size() ; i++) {
      uint i1 = (i+1) % polygon.size();
      CCIndex v1 = polygon[i];
      CCIndex v2 = polygon[i1];
      CCSignedIndex edge = cs.orientedEdge(v1,v2);
      if(~edge == CCIndex::UNDEF) {
        CCIndex e = CCIndexFactory.getIndex();
        if(!cs.addCell(e, +polygon[i] -polygon[i1])) 
          throw(QString("PolarSurface2D::initialCell: Unable to add edge"));
        faceBoundary += e;
      } else
        faceBoundary += edge;
    }

    // Add face to complex
    if(!cs.addCell(face, faceBoundary)) 
      throw(QString("PolarSurface2D::initialCell: Unable to add face"));

    // Add to the cell tissue
    return tissue.addCell(face);
  }

  void PolarSurface2D::growPoint(CCIndex v, double dt, double time)
  {
    PolarData &pdata = (*pAttr)[v];
    if(pdata.r > 0.0)
    {
      int rIdx = int(pdata.r / dr);
      if(rIdx > SAMPLES) 
        rIdx = SAMPLES;
      pdata.r += sampledGrowth[rIdx] * dt * growthScaleOutMult *
        growthScaleFunc(time * growthScaleInMult);
    }
    computePos(v);
    computeNormal(v);
  }

  void PolarSurface2D::Subdivide::splitCellUpdate(Dimension dim, const CCStructure &cs, const CCStructure::SplitStruct& ss, 
                                                                               CCIndex otherP, CCIndex otherN, double interpPos)
  {
    if(!surf) 
      return;

    if(dim == 1) {
      // The division algorithm has set the location: we just
      // project it to the surface.
      CCIndexData &dV = (*surf->indexAttr)[ss.membrane];
      surf->setPoint(ss.membrane, (interpPos < 0.5) ? otherP : otherN , dV.pos);
    } else if(dim == 2) {
      // Remove the old cell data from the polar attributes
      surf->pAttr->erase(ss.parent);
  
      // Compute each child's centroid in polar coordinates
      ccf::RO ro = ccf::POS;
      for(int i = 0 ; i < 2 ; i++, ro = -ro) {
        CCIndex child = ss.child(ro);
  
        // Iterate around the face and compute the centroid
        CCStructure::CellTuple tuple(cs,child);
        std::complex<double> centroid = 0;
        double signedArea = 0;
        CCIndex firstV = tuple[0];
        do {
          PolarData &pd = (*(surf->pAttr))[tuple[0]];
          std::complex<double> p = std::polar(pd.r,pd.theta);
          tuple.flip(0);
          PolarData &qd = (*(surf->pAttr))[tuple[0]];
          std::complex<double> q = std::polar(qd.r,qd.theta);
          tuple.flip(1);
          double a = std::imag(p * std::conj(q));
          signedArea += a;
          centroid += a * (p + q);
        } while(tuple[0] != firstV);
        centroid /= (3.0 * signedArea);
  
        // update the polar position data
        PolarData &pd = (*(surf->pAttr))[child];
        pd.r = std::abs(centroid);
        pd.theta = std::arg(centroid);
        surf->computePos(child);
        surf->computeNormal(child);
      }
    }
  }
}
