MorphoDynamX
============

Installation
------------

### Requirements

- NVIDIA CUDA (tested 7.5, 8.0, 9.0RC)
- libtiff
- CImg >= 1.4.6
- GLEW
- GSL
- CMake >= 3.0
- TBB
- qt5
- gcc 4.8 to <= 5.4 (requirement of CUDA up to 8.0) or 6.4 (CUDA 9.0RC)
- boost
- `lsb_release`
- NVIDIA GPU and driver (optional, for GPU acceleration of some functions)
- git (optional, for version number detection)
- doxygen (optional, for documentation and packaging)
- libboost-test-dev for tests

### OS status

Because of the CUDA gcc version constraint, ability to use any given OS is limited. Because of that, depending on OS some packages may need to be built from source.

Supported:

- Debian 8

Recommended:

- Ubuntu 16.04 LTS
- Mint 18.1 (tested)

Some packages need to be obtained separately:

- Mint 17.3: cmake-3, libqcustomplot-1.2 for MorphoRobotX, others (?)

Some packages need to be built from source, epel repository contains others:

- CentOS 7: CImg for base; InsightToolKit, CGAL, comedi for options

Not recommended - only OMP backend works:

- Fedora 24+: ships with GCC 6
- Debian 9: ships with GCC 6

**WARNING**: Distribution-provided InsightToolkit seems to be old in all cases. It will not work if any C++11 code is used. As a workaround, install InsightToolkit from official repository (version 4.9 or higher).

### Building

The simplest way to build the source with CPU computation support, assuming it's in `$SRC`:

```
# mkdir build
# cd build
# cmake $SRC -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr/local -DTHRUST_BACKEND_CUDA=OFF -DTHRUST_BACKEND_OMP=ON
# make
# sudo make install
```

To build a version leveraging GPU computations, replace the `cmake` line above with the following (note `CUDA=ON`):

```
# cmake $SRC -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr/local -DTHRUST_BACKEND_CUDA=ON -DTHRUST_BACKEND_OMP=OFF
```

The commands together will install MorphoDynamX to `/usr/local` and create an executable file `/usr/local/bin/MorphoDynamX`.

#### Documentation

Documentation will be created during `make install` whenever the user has doxygen available. Alternatively, it can be created with:

```
make doc
```

### Optional portions

Some parts of MorphoDynamX can be enabled/disabled at build time. They are controlled by CMake build flags in the form of `BUILD_*=ON|OFF`, e.g. for ITK the cmake command would look similar to this:

```
# cmake $SRC -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_ITK=ON
```

These portions may have additional requirements:

#### CCFemLib

Does not build with CUDA, builds with OMP.

#### CellMaker

Requires:

- CGAL

#### ICP

*Deprecated*, does not build.

#### ITK

Requires:

- ITK >= 4.0
- VTK < 7.0 as a dependency

#### MorphoRobotX

Requires:

- QCustomPlot >= 1.2

#### MorphoRobotX_Robot

Requires:

- comedi
- libmcscontrol (not publicly available)

#### VTK

*Deprecated*, does not expose any functionality.

Required to build:

- VTK < 7.0

### Advanced build options

The CMake option `CUDA_SEPARABLE_COMPILATION` function can bring benefits (TODO: which?) but is unstable.

Packaging
---------

MorphoDynamX supports being packaged as a Debian package (`.deb`).

### Requirements

XDG programs must be installed on the building system:

- `xdg-icon-resource`
- `xdg-desktop-menu`
- `xdg-mime`

On Debian, they are found in the `xdg-utils` package.

The package `nvidia-cuda-toolkit` from (*jessie-backports*)[https://wiki.debian.org/Backports] is required.

**Note:** Debian's CUDA package is not versioned. MorphoDynamX package needs a specific CUDA version. It's possible that a mismatch occurs after installing the resulting package when one or the other is old. Solution: always keep your systems and builds up to date.

### Creating

Proceed with building as described earlier, with 2 exceptions:

The `cmake` command MUST contain `-DMDX_PACKAGE="ON"`, and instead of `sudo make install`, issue `make package`.

For example, to build the `MorphoRobotX` addon:

```
mkdir -p build_package
rm -rf build_package/.* # Remove cached variables from earlier builds
cd build_package
cmake /mnt/mdx/branches/MorphoDynamX/ -DCMAKE_BUILD_TYPE=Release -DMDX_PACKAGE=ON -DBUILD_MorphoRobotX=ON -DMDX_REVISION=MyTestBuild
```

**WARNING** Don't use the same directory for `make install` and `make package`! These two commands use different installation paths and the installation can be broken. User a separate `build_package` directory for packaging.

This will create a `MDX-....deb` file in `build_package` directory. To find it, look at the last line of output. It should look similar to:

```
Run CPack packaging tool...
CPack: Create package using DEB
CPack: Install projects
CPack: - Run preinstall target for: MorphoDynamX
CPack: - Install project: MorphoDynamX
CPack: Create package
CPack: - package: /mnt/builds/dcz/debian-8.7/MDX-2.0.unknown-Debian8.7-Cuda-MorphoRobotX.deb generated.
```

In this example, the file is called `MDX-2.0.unknown-Debian8.7-Cuda-MorphoRobotX_Robot.deb` and it's placed in `/mnt/builds/dcz/debian-8.7/`.

This `.deb` file is suitable for distributing to other systems of the same version. In this case, only Debian-8.7 is guaranteed to work.

**NOTE** the package does not spedcify dependencies on *CUDA* or *libmcscontrol*. If you are using, respectively, CUDA or MorphoRobotX_Robot, you will need to make sure the appropriate software is installed.

Supported configuration
-----------------------

The supported set of addons is always defined in `.gitlab-ci.yml`. Currently, these are *MorphoRobotX*, *MorphoRobotX_Simulator*, *CellMaker*, *CellAtlas* and *DivisionAnalysis*.

Make sure to enable them before testing and committing your changes.

```
cmake -DBUILD_MorphoRobotX=ON -DBUILD_MorphoRobotX_Simulator=ON -DBUILD_CellMaker=ON -DBUILD_CellAtlas=ON -DBUILD_DivisionAnalysis=ON [...]
```

Testing
-------

The basic test command is:

```
make check_all
```

It will run all tests after installing.

If building a package:

```
make check
make package
# install the package
make plugin_test
```

**WARNING** before `check_all` or any other plugin API test, clear the installation directory (usually `CMAKE_INSTALL_PREFIX`).

### Unit tests

To run unit tests, build the project and do the folowing:

```
make check
```

If any of the tests failed, then it's possible to identify a particular test by running the binary directly.

```
cd $WHERE_CMAKE_WAS_EXECUTED
./tests/$THE_TEST_NAME
```

**Note**: tests are not using *make* but *ctest* and will not be parallelized with `-jnum`. Instead, use the variable `CTEST_PARALLEL_LEVEL` like this:

```
CTEST_PARALLEL_LEVEL=8 make check
```

### Public API tests

To run public API tests, run CMake as usual, and issue:

```
CTEST_PARALLEL_LEVEL=8 make plugin_check
```

**Note**: This issues `make install`. If you're building a package, install it and then run `make plugin_test` instead.

### Targets

There are several test-related `make` *targets* (run them with `make $MY_TARGET`):

- *test name* - builds only the specified unit test
- `build_tests` - builds all unit tests
- `test` - only tries to run unit tests
- `check` - builds if necessary and then runs unit tests
- `plugin_test` - only runs plugin API tests
- `plugin_check` - installs API files and runs plugin API tests
- `check_all` - builds and runs unit tests, then installs API files and runs plugin API tests

Continuous integration
----------------------

CI is using *gl\_multi\_ci* for starting jobs on different OSs. See instructions for setting it up.
